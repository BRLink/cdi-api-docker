module CDI
  module V1
    module PageStructure
      class LearningCyclePagePresenter < BasePresenter

        attr_reader :learning_cycle, :categories

        def initialize
          @learning_cycle = ::LearningCycle.new
          @categories     = ::Category.all
        end

      end
    end
  end
end
