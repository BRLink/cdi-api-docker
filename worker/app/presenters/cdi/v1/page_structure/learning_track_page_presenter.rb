module CDI
  module V1
    module PageStructure
      class LearningTrackPagePresenter < BasePresenter

        attr_reader :learning_track, :questions_templates, :categories, :skills

        def initialize
          @learning_track      = ::LearningTrack.new
          @questions_templates = ::QuestionTemplate.questions_for(@learning_track.class)

          @categories     = ::Category.all
          @skills         = ::Skill.all
        end

      end
    end
  end
end
