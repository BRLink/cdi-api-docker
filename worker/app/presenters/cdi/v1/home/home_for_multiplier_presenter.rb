module CDI
  module V1
    module Home
      class HomeForMultiplierPresenter < BaseHomePresenter

        def recent_library_items
          Library.query('resource' => 0, 'owner' => @user).slice 0, 5
        end

        def learning_tracks_recommendations
          @user.multiplier_tracks_recommendations(3)
        end

        def learning_tracks_reports
          LearningTrackMultiplierReportPresenter.new(@user, { limit: 3 }).report
        end
      end
    end
  end
end
