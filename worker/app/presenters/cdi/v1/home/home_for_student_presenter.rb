module CDI
  module V1
    module Home
      class HomeForStudentPresenter < BaseHomePresenter

        def learning_tracks
          @user.enrolled_tracks.limit(3)
        end
      end
    end
  end
end
