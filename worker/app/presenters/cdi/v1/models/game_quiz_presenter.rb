module CDI
  module V1
    module Models
      class GameQuizPresenter < BaseGamePresenter

        def serializer_name
          'game_quiz'
        end

        def game_class
          GameQuiz
        end

      end
    end
  end
end
