module CDI
  class BaseService

    include ServiceConcerns::BaseConcern

    attr_reader :options, :response_status, :errors

    CALLBACKS = [
      :after_initialize,
      :before_error,
      :after_error,
      :after_success,
      :before_success
    ]

    ERROR_RESPONSE_METHODS = {
      :bad_request_error     => 400,
      :not_authorized_error  => 401,
      :forbidden_error       => 403,
      :not_found_error       => 404,
      :internal_server_error => 500
    }

    ERRORS_I18N_NAMESPACE = 'cdi.errors'

    CALLBACKS.each do |callback|
      define_method "#{callback}" do
        nil
      end
    end

    def initialize(options={}, initial_response_status=400)
      @options         = set_default_options(options)
      @response_status = initial_response_status
      @errors          = []

      call_callback(:after_initialize)
    end

    def valid?
      return @errors.blank?
    end

    def success?
      @success == true && valid?
    end

    def fail?
      !success?
    end

    def response_status
      @response_status ||= 400
    end

    def changed?
      changed_attributes_array.any?
    end

    def changed_attributes_array
      []
    end

    def callback_fired?(callback_name)
      return @_fired_callbacks && @_fired_callbacks[callback_name.to_sym].present?
    end

    alias :callback_called? :callback_fired?

    def origin_params(params={})
      origin_data = (params.is_a?(Hash) && params.present? ? params : @options)[:origin] || {}
      origin_data = origin_data.slice(:provider, :locale, :user_agent, :ip)
    end

    def option_enabled?(key)
      options_var = @options
      options_var && options_var.key?(key) && options_var[key] == true
    end

    def option_disabled?(key)
      !option_enabled?(key)
    end

    private

    def create_origin(originable, params={})
      @origin ||= originable.create_origin(origin_params(params))
    end

    def create_origin_async(originable, params={})
      origin_params = origin_params(params)
      origin_params.merge!({
        originable_type: originable.class.to_s,
        originable_id: originable.id,
        fetch_originable: true
      })

      CDI::V1::OriginCreateWorker.perform_async(origin_params)
    end

    ERROR_RESPONSE_METHODS.each do |error_method, status_code|
      define_method error_method do |message_key, options={}|
        error(status_code, message_key, options)
      end

      define_method "#{error_method}!" do |message_key, options={}|
        error!(status_code, message_key, options)
      end
    end

    def error(status, message_key, options = {})
      call_callback(:before_error)

      options = options.reverse_merge(no_i18n: false)

      @response_status = status
      error_message    = process_error_message_for_key(message_key, options)

      error = add_error(error_message)

      call_callback(:after_error)

      error_message
    end

    def error!(status, message_key, options = {})
      error(status, message_key, options)

      # TODO:
      # maybe throw a Exception making bang(!) semantic
      # raise "CDI::V1::Exceptions::#{status.titleize}".constantize
      return false
    end

    def process_error_message_for_key(message_key, options)
      if message_key.is_a?(ActiveModel::Errors)
        message = message_key.full_messages.to_sentence # .messages
      elsif message_key.is_a?(Array) && message_key.first.is_a?(Hash)
        message = message_key.map(&:values).flatten.to_sentence
      elsif message_key.is_a?(Array)
        message = message_key.flatten.to_sentence
      else
        if options[:no_i18n].present?
          message = message_key
        else
          message = I18n.t("#{ERRORS_I18N_NAMESPACE}.#{message_key}", options)
        end
      end

      message
    end

    def success_response(status=200)
      call_callback(:before_success)

      @response_status = status
      @success         = true

      call_callback(:after_success)
    end

    def success_created_response
      success_response(201)
    end

    def call_callback(callback_name)
      @_fired_callbacks ||= {}
      callback_name = callback_name.to_s.underscore.to_sym

      if self.respond_to?(callback_name, true)
        @_fired_callbacks[callback_name] = true
        self.send(callback_name)
      end
    end

    def valid_object?(record, expected_class)
      record.present? && record.is_a?(expected_class)
    end

    def valid_user?
      valid_object?(@user, User)
    end

    def filter_hash(hash, whitelist_keys = [])
      (hash || {}).symbolize_keys.slice(*whitelist_keys.map(&:to_sym))
    end

    def changes(old, current, attributes = {})
      changes = []

      return changes if old.blank? || current.blank?

      old_attributes = old.attributes.slice(*attributes.map(&:to_s))
      new_attributes = current.attributes.slice(*attributes.map(&:to_s))

      new_attributes.each do |attribute, value|
        if old_attributes[attribute] != value
          changes << attribute
        end
      end

      changes
    end

    def add_error(error)
      @errors ||= []
      _method = error.is_a?(Array) ? :concat : :push
      @errors.send(_method, error)
    end

    def create_system_notification(**args)
      return false unless create_system_notification?

      return false unless user_can_receive_notification?(args[:receiver_user],
                                                         args[:notification_type])

      receiver = args.delete :receiver_user
      service = ::CDI::V1::Notifications::CreateService.new(receiver, args)
      service.execute

    end

    def user_can_receive_notification?(user, type)
      notification_config = user.notification_config(type)

      return false if notification_config.nil?

      return notification_config.can_notification
    end

    def user_can_receive_email?(*args)
      return false if args.size < 2

      user = args[0]
      type = args[1]

      notification_config = user.notification_config(type)

      return notification_config.can_email
    end

    def create_system_notification_async(**args)
      return false unless user_can_receive_notification?(args[:receiver_user],
                                                         args[:notification_type])

      if CDI::Config.enabled?(:create_system_notification_async)
        ::CDI::V1::NotificationCreateWorker.perform_async(args[:receiver_user].id, args)
      else
        create_system_notification(args)
      end
    end

    def delivery_sync_email(mailer, mail_method, *args)
      delivery_email(mailer, mail_method, :deliver_now, *args)
    end

    def delivery_async_email(mailer, mail_method, *args)
      delivery_email(mailer, mail_method, :deliver_later, *args)
    end

    def create_mail_message_for_mailer(mailer, mail_method, *args)
      mailer.to_s.constantize.send(mail_method, *args)
    end

    def delivery_email(mailer, mail_method, method=:deliver_later, *args)
      return false unless delivery_email?

      if mail_method == :notification
        return false unless user_can_receive_email?(*args)
      end

      mail = create_mail_message_for_mailer(mailer, mail_method, *args)

      mail.send(method)
    end

    def set_default_options(options={})
      {
        delivery_email: true,
        create_system_notification: true
      }.merge(options.symbolize_keys).deep_symbolize_keys
    end

    def create_system_notification?
      option_enabled?(:create_system_notification)
    end

    def delivery_email?
      option_enabled?(:delivery_email)
    end

    protected
    def not_implemented_exception(method_name)
      raise NotImplementedError, "#{method_name} must be implemented in subclass"
    end
  end
end
