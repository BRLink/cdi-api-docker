module CDI
  module V1
    module GameQuizzes
      class CreateService < Games::BaseCreateService

        include V1::ServiceConcerns::GameQuizzesParams

        record_type ::GameQuiz

        private
        def base_options_key
          :quiz
        end

        def game_params
          game_quiz_params
        end

        def game_user_scope
          @user.game_quizzes
        end

        def game_with_questions?
          true
        end

      end
    end
  end
end
