# encoding: UTF-8

module CDI
  module V1
    module SupportingCourses
      class CloneService < Libraries::CloneService
        record_type ::SupportingCourse
      end
    end
  end
end
