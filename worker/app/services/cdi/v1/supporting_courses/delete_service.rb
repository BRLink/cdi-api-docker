# encoding: UTF-8

module CDI
  module V1
    module SupportingCourses
      class DeleteService < BaseDeleteService
        record_type ::SupportingCourse

        def after_success
          # nothing to do
        end
      end
    end
  end
end
