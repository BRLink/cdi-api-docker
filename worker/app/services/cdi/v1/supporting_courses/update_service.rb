# encoding: UTF-8

module CDI
  module V1
    module SupportingCourses
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::SupportingCoursesParams

        record_type ::SupportingCourse

        def record_params
          supporting_course_params.keep_if { |_, v| v.present? }
        end

      end
    end
  end
end
