module CDI
  module V1
    module SupportingCourses
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::SupportingCoursesParams

        record_type ::SupportingCourse

        private
        def can_create_record?
          return false unless valid_user?

          return @user.admin?
        end

        def build_record
          @supporting_course = @user.supporting_courses.build(supporting_course_params)
        end
      end
    end
  end
end
