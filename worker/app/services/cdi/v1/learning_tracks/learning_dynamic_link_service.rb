# encoding: UTF-8

module CDI
  module V1
    module LearningTracks
      class LearningDynamicLinkService < BaseLinkResourceService

        action_name :link_learning_dynamic

        def user_can_associated_resource?
          unless valid_learning_dynamic?
            return not_found_error!('learning_dynamics.not_found')
          end

          unless has_dynamic_in_learning_track?
            return forbidden_error!('learning_tracks.yet_has_learning_dynamic')
          end

          unless can_link_dynamic_to_learning_track?
            return forbidden_error!('learning_dynamics.no_slides_found')
          end

          return @user.backoffice_profile?
        end

        def create_presentation_slide_with_service
          if overwrite? && learning_track.has_learning_dynamic?
            slide = learning_track.learning_dynamic_slide
            if slide
              update_slide(slide)
            else
              super
            end
          else
            super
          end
        end

        def has_dynamic_in_learning_track?
          return true if overwrite?
          return !learning_track.has_learning_dynamic?
        end

        def can_link_dynamic_to_learning_track?
          @resource.multiplier_presentation.slides.size > 0 && @resource.student_presentation.slides.size > 0

        end

        def valid_learning_dynamic?
          valid_object?(@resource, ::LearningDynamic)
        end

        def record_error_key
          :learning_dynamics
        end

        def slide_metadata_for_resource(resource = nil)
          {
            student_thumbnail_url: resource.try(:student_thumbnail_url),
            multiplier_thumbnail_url: resource.try(:multiplier_thumbnail_url)
          }
        end

      end
    end
  end
end
