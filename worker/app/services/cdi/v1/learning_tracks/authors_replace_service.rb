# encoding: UTF-8

module CDI
  module V1
    module LearningTracks
      class AuthorsReplaceService < BaseActionService

        MAX_MODERATORS_PER_BATCH = 20

        attr_reader :added_moderators

        action_name :track_replace_authors

        def initialize(learning_track, user, options = {})
          @added_moderators = []
          @learning_track = learning_track
          @user = user
          super(options)
        end

        private

        def success_runned_action?
          @added_moderators.any? || valid_record?
        end

        def execute_action
          clear_current_moderators
          add_new_moderators
        end

        def add_new_moderators
          @users = find_moderators

          @users.each do |user|
            if user_eligible_for_moderation?(user)
              add_user_as_moderator(user)
            end
          end

          # not the owner updating
          add_user_as_moderator(@user) if @learning_track.user_id != @user.id
        end

        def add_user_as_moderator(user)
          if user.add_as_track_moderator @learning_track
            @added_moderators << user
          end
        end

        def find_moderators
          @users ||= User.where(id: moderator_ids)
                         .limit(MAX_MODERATORS_PER_BATCH)
                         .select(&:multiplier?)
        end

        def clear_current_moderators
          # clear all
          @learning_track.moderators = []
        end

        def user_eligible_for_moderation?(user)
          return user.present? && user.id != @user.id
        end

        def valid_record?
          valid_object?(@learning_track, ::LearningTrack)
        end

        def user_can_execute_action?
          @learning_track.user_id == @user.id || @user.moderator_of_track?(@learning_track)
        end

        def record_error_key
          :learning_tracks
        end

        def moderator_ids
          array_values_from_params(@options, :users_ids) ||
          array_values_from_params(@options, :moderator_ids)
        end

        def after_success
          notify_users
          grant_achievement_to_new_authors
          grant_achievement_to_creator
        end

        def grant_achievement_to_creator
          @user.user_achievements.create(
            resource: @learning_track,
            achievement: Achievement.find_by(action: :add_author)
          )
        end

        def grant_achievement_to_new_authors
          @added_moderators.each do |moderator|
            moderator.user_achievements.create(
              resource: @learning_track,
              achievement: Achievement.find_by(action: :become_co_author)
            )
          end
        end

        def notify_users
          @added_moderators.each do |moderator|
            create_system_notification_async(
              sender_user: @user,
              receiver_user: moderator,
              notification_type: :added_as_track_author,
              notificable: @learning_track
            )
          end
        end
      end
    end
  end
end
