# encoding: UTF-8

module CDI
  module V1
    module LearningTracks
      class DeleteService < BaseDeleteService
        record_type ::LearningTrack

        def after_success
          # delete presentation + presentation sldies + associated resources (only games + questions)
          # + enrolled_learning_tracks + student_classes_tracks

          #sync cycle status
          @record.learning_cycle.refresh_status
        end

      end
    end
  end
end
