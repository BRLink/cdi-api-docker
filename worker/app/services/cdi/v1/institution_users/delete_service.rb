# encoding: UTF-8

module CDI
  module V1
    module InstitutionUsers
      class DeleteService < BaseDeleteService

        record_type ::InstitutionUser


        private
        def can_execute_action?
          if user_permanent_member?
            return forbidden_error!("#{record_error_key}.status_cant_be_altered")
          end
          super
        end

        def user_can_delete?
          @record.user == @user
        end

        def user_permanent_member?
          if @record.present?
            return institution.admin_user == @record.user
          end
          false
        end
        def institution
          @institution ||= EducationalInstitution.find_by(id: options[:id])
        end
      end
    end
  end
end
