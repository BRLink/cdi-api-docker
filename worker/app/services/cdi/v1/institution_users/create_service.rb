# encoding: UTF-8

module CDI
  module V1
    module InstitutionUsers
      class CreateService < BaseCreateService

        record_type ::InstitutionUser


        private
        def can_create_record?
          valid_user?
        end

        def build_record

          institution_user = institution.all_institution_users.find_by(user_id: @user.id)
          # this request can update status if use was rejected
          if institution_user.present?
            institution_user.pending! if  institution_user.rejected?
          else
            institution_user = institution.pending_institution_users.build(user_id: @user.id)
          end

          institution_user
        end

        def institution
          @institution ||= EducationalInstitution.find_by(id: options[:id])
        end
      end
    end
  end
end
