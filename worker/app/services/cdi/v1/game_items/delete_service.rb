# encoding: UTF-8

module CDI
  module V1
    module GameItems
      class DeleteService < BaseDeleteService

        record_type ::GameItem

        def after_success
        end

        def user_can_delete?
          @record.game_related_item.user_can_delete?(@user)
        end

        def game_related_item
          @record.try(:game_related_item)
        end

      end
    end
  end
end
