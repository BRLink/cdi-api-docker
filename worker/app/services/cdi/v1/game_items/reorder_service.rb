# encoding: UTF-8

module CDI
  module V1
    module GameItems
      class ReorderService < BaseReorderService

        record_type ::GameItem

        def record_error_key
          :game_items
        end

      end
    end
  end
end
