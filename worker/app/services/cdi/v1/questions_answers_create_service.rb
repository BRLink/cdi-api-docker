# encoding: UTF-8

module CDI
  module V1
    class QuestionsAnswersCreateService < BaseService

      def initialize(user, questionable, options={})
        @user = user
        @questionable = questionable
        super(options)
      end

      def execute
        if can_execute_action?
          @questions ||= load_questions

          create_or_update_answers_for_questions

          if @created_answers.any?
            success_created_response
          elsif @updated_answers.any?
            success_response
          else
            @response_status = 204
          end
        end

        success?
      end

      def can_execute_action?
        valid_user? && valid_questionable?
      end

      def create_or_update_answers_for_questions
        @answers = (@options[:answers] || {})
        @created_answers, @updated_answers = [], []

        @answers.each do |question_template_id, answer|
          question_template_id = question_template_id.to_s.to_i

          question = find_or_create_question(question_template_id)

          if question.question_multiple_choices?
            process_answers_for_multiple_choices_question(question, answer)
          else
            process_answer_for_simple_question(question, answer)
          end
        end
      end

      def process_answer_for_simple_question(question, answer)
        answer_data = question.format_data_for_answer(answer)

        if user_answer = find_answer(question)
          update_question_answer(user_answer, answer_data)
        else
          create_user_answer_for_question(question, answer)
        end
      end

      def create_user_answer_for_question(question, answer)
        @created_answers << question.create_answer_for(@user, answer)
      end

      def process_answers_for_multiple_choices_question(question, answer)
        answer_data   = question.format_data_for_answer(answer)

        system_anwers = find_answers(question)
        answers_index = answer_data.map {|a| a[:answer_index] }

        answers_to_remove = system_anwers.select {|a| answers_index.exclude?(a.answer_index) }

        answer_data.each do |answer_data|
          user_answer = system_anwers.find {|a| a.answer_index == answer_data[:answer_index] }

          if user_answer
            update_question_answer(user_answer, answer_data)
          else
            create_user_answer_for_question(question, answer_data[:answer_index])
          end
        end

        answers_to_remove.map(&:delete)
      end

      def update_question_answer(user_answer, answer_data)
        if (user_answer.answer_text != answer_data[:answer_text]) ||
           (user_answer.answer_index != answer_data[:answer_index])
            @updated_answers << user_answer.update(answer_data)
        end

        user_answer
      end

      def find_or_create_question(question_template_id)
        @questions ||= load_questions

        question = @questions.find { |q| q.question_template_id == question_template_id }

        if question.blank?
          question = @questionable.questions.create(question_template_id: question_template_id)
        end

        question
      end

      def valid_questionable?
        valid_object?(@questionable, ::LearningTrack) ||
        valid_object?(@questionable, ::CyclePlanning)
      end

      def load_questions
        @questionable.questions.includes(:question_template, :template_questionable, :answers)
      end

      def find_answer(question)
        question.answers.find {|q| q.user_id ==  @user.id }
      end

      def find_answers(question)
        question.answers.select {|q| q.user_id ==  @user.id }
      end

    end
  end
end
