module CDI
  module V1
    module PresentationSlides
      class ThumbUploadService < BaseActionService

        action_name :thumb_upload

        attr_reader :presentation_slide, :thumb_url, :asset

        def initialize(presentation_slide, user, options={})
          @presentation_slide = presentation_slide
          @user = user
          super(options)
        end

        def execute_action
          @upload_service = upload_with_service
          if @upload_service.success? && @asset = @upload_service.asset
            @thumb_url     = @asset.image_url
            @last_metadata = @presentation_slide.metadata
            @last_thumb    = @presentation_slide.thumbnail_url

            [:cover, :other].each do |version|
              image_url = @asset.image.try(version).try(:url)
              @presentation_slide.save_thumb_url(image_url, false, "#{version}_url")
            end

            @presentation_slide.save_thumb_url(@thumb_url)
          end
        end

        private
        def after_success
          delete_last_thumbnail
        end

        def delete_last_thumbnail
          CDI::V1::AssetsDeleteWorker.perform_async @last_thumb, visible: false
        end

        def user_can_execute_action?
          @presentation_slide.user_can_update?(@user)
        end

        def valid_record?
          valid_object?(@presentation_slide, ::PresentationSlide)
        end

        def record_error_key
          :presentation_slides
        end

        def success_runned_action?
          @thumb_url.present?
        end

        def action_errors
          @upload_service.try(:errors) || []
        end

        def upload_with_service
          upload_options = @options.dup.deep_merge(upload: {
            visible: false,
            create_image_versions: false,
            slide_thumb_upload: true
          })

          upload_service = UserGalleries::AssetUploadCreateService.new(@user, upload_options)
          upload_service.execute

          upload_service
        end
      end
    end
  end
end
