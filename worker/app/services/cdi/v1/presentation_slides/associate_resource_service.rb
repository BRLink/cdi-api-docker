module CDI
  module V1
    module PresentationSlides
      class AssociateResourceService < BaseActionService

        action_name :associate_resource

        attr_reader :resource, :presentation_slide

        def initialize(resource, user, options={})
          @resource = resource
          @user = user
          super(options)
        end

        def execute_action
          @presentation_slide = create_presentation_slide_with_service
        end

        private
        def success_runned_action?
          return false unless @presentation_slide.try(:valid?)

          valid_object?(@presentation_slide, ::PresentationSlide) &&
          @presentation_slide.associated_resource_id.present?
        end

        def user_can_execute_action?
          return false unless valid_user?
          return false unless @user.backoffice_profile?

          return true
        end

        def create_presentation_slide_with_service
          options = @options.dup.merge!(
            slide: {
              presentation_id: presentation.try(:id),
              slide_type: slide_type_for_resource,
              content: slide_content_for_resource,
              metadata: slide_metadata_for_resource,
            }
          )

          service = PresentationSlides::CreateService.new(@user, options)
          service.execute

          update_slide(service.presentation_slide)
        end

        def update_slide(slide)
          resource = associated_resource

          if resource.is_a?(LearningDynamic)
            resource = clone_learning_dynamic
          elsif resource.is_a?(SupportingCourse)
            resource = clone_supporting_course
          end

          slide.associated_resource = resource
          slide.metadata = slide_metadata_for_resource(resource)
          slide.save

          slide
        end

        def clone_learning_dynamic
          multiplier_slides = associated_resource.multiplier_slides.map(&:amoeba_dup)
          student_slides = associated_resource.student_slides.map(&:amoeba_dup)
          resource = associated_resource.amoeba_dup
          resource.category_ids = associated_resource.category_ids
          resource.user = @user
          resource.save

          # slides are't cloning together...
          resource.multiplier_presentation.slides = multiplier_slides if resource.multiplier_presentation
          resource.student_presentation.slides    = student_slides if resource.student_presentation

          resource
        end

        def clone_supporting_course
          associated_resource.clone!(@user)
        end

        def associated_resource
          @resource
        end

        def valid_resource?
          @resource.present?
        end

        def presentation
          return nil unless valid_resource?
          @resource.presentation || @resource.create_presentation
        end

        ####
        def record_error_key
          @record.class.to_s.underscore.pluralize
        end

        ####
        def slide_type_for_resource
          PresentationSlide::SLIDE_TYPES[:dynamic]
        end

        ###
        def slide_content_for_resource
          {}
        end

        ###
        def slide_metadata_for_resource(resource = nil)
          {}
        end
      end
    end
  end
end
