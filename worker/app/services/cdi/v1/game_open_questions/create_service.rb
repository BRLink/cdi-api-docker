# encoding: UTF-8

module CDI
  module V1
    module GameOpenQuestions
      class CreateService < Games::BaseCreateService

        include V1::ServiceConcerns::GameOpenQuestionsParams

        record_type ::GameOpenQuestion

        private
        def base_options_key
          :game
        end

        def game_params
          game_open_question_params
        end

        def game_user_scope
          @user.game_open_questions
        end

        # force empty options
        def normalize_data_for_question(question_data)
          question_data[:options] = []

          question_data
        end

        def game_with_questions?
          true
        end

      end
    end
  end
end
