# encoding: UTF-8

module CDI
  module V1
    module GameOpenQuestions
      class UpdateService < Games::BaseUpdateService

        include V1::ServiceConcerns::GameOpenQuestionsParams

        record_type ::GameOpenQuestion

        private
        def whitelist_attributes_to_clear
          [:video_link, :image]
        end

        def game_params
          game_open_question_params
        end

        def base_options_key
          :game
        end

      end
    end
  end
end
