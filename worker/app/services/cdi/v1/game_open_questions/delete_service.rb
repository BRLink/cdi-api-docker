# encoding: UTF-8

module CDI
  module V1
    module GameOpenQuestions
      class DeleteService < Games::BaseDeleteService

        record_type ::GameOpenQuestion

      end
    end
  end
end
