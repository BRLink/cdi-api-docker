# encoding: UTF-8

module CDI
  module V1
    module TrackingEvents
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::TrackingEventParams

        record_type ::TrackingEvent, alias_name: :event

        EVENTS_TYPE_FOR_SLIDE_TYPE = {
          :content => [1],
          :game    => [1,2],
          :course  => [1],
          :dynamic => [1,3],
          :dinamic => [1,3] # alias to keep code compatibility
        }

        delegate :watched_slides_ids_for_track, :watched_slides_for_track,
                 :played_slides_for_track, :not_watched_slides_for_track,
                 :not_played_slides_for_track, :not_watched_slides_for_track_count,
                 to: :enrolled_learning_track, allow_nil: true

        private
          def build_record
            enrolled_learning_track.events.build(tracking_event_params)
          end

          def save_record
            begin
              @record.save
            rescue ActiveRecord::RecordNotUnique, PG::UniqueViolation => e
              @record.errors.add(:presentation_slide_id, :yet_tracked)
              false
            end
          end

          def can_create_record?
            unless valid_learning_track?
              return not_found_error!('learning_tracks.not_found')
            end

            unless valid_slide_for_track?
              return not_found_error!('slides.not_found')
            end

            unless can_create_event?
              return forbidden_error!("cant_skip_to_slide_#{slide.id}, must be #{next_slide_id}")
            end

            unless valid_event_type_for_slide_type?
              return forbidden_error!("tracking_events.invalid_event_type_for_slide_type_#{slide.slide_type}")
            end

            return true
          end

          def can_create_event?
            if CDI::Config.enabled?(:validate_slide_id_on_tracking_event)
              last_slide = last_slide_for_track

              return (last_slide.blank? && slide.position == 1) || (last_slide.presence && last_slide.position.next == slide.position)
            end

            return true
          end

          def valid_event_type_for_slide_type?
            event_type = tracking_event_params[:event_type]
            slide_type = slide.slide_type

            events = EVENTS_TYPE_FOR_SLIDE_TYPE[slide_type.to_sym]

            return events.present? && events.member?(event_type)
          end

          def after_success
            update_user_enroll_status
            update_user_enroll_progress
          end

          def update_user_enroll_status
            if finished_track?
              migrate_to_complete
            elsif started_track?
              migrate_to_coursing
            end
          end

          def started_track?
            total_watched_slides > 1
          end

          def cache_keys_to_expire_after_success
            %w(current_user.show) # /users/me
          end

          def needs_cache_expiration?
            return finished_track?
          end

          def update_user_enroll_progress
            CDI::V1::TrackEnrollProgressUpdateWorker.perform_async(enrolled_learning_track.id)
          end

          def finished_track?
            watched_all_slides_of_track? && played_all_games_of_track?
          end

          def watched_all_slides_of_track?
            total_watched_slides >= total_track_slides_count
          end

          def played_all_games_of_track?
            total_played_slides >= total_track_games_count
          end

          def total_track_slides_count
            @track_slides_count ||= slide.presentation.slides_count
          end

          def total_track_games_count
            @track_games_count ||= learning_track.slides.game.count
          end

          def total_watched_slides
            total_events_count_by_type(:watched)
          end

          def total_played_slides
            total_events_count_by_type(:played)
          end

          def total_events_count_by_type(type)
            events_by_type[::TrackingEvent::EVENT_TYPES[type.to_sym]]
          end
      end
    end
  end
end
