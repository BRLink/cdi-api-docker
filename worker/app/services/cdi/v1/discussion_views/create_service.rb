module CDI
  module V1
    module DiscussionViews
      class CreateService < BaseCreateService

        record_type ::DiscussionView

        def initialize discussion, user, options={}
          super user, options
          @discussion = discussion
        end

        def can_create_record?
          @discussion.present?
        end

        def build_record
          @discussion.views.build(user: @user)
        end

        def after_success
          @discussion.views_count += 1
        end

      end
    end
  end
end
