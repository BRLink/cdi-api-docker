module CDI
  module V1
    module Discussions
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::DiscussionParams
        include V1::ServiceConcerns::SurveyAttachmentParams
        include V1::ServiceConcerns::ResourceVisibilityParams

        record_type ::Discussion

        def initialize publisher, user, options={}
          super user, options
          @publisher = publisher
        end

        def can_create_record?
          unless has_visibilities?
            return forbidden_error!("#{record_error_key}.supply_at_least_one_visibility")
          end

          unless valid_publisher?
            return forbidden_error!("#{record_error_key}.publisher_invalid_or_not_found")
          end

          if has_cover? && !valid_cover?
            return forbidden_error!("#{record_error_key}.cover_invalid_or_not_found")
          end

          true
        end

        def build_record
          @user.discussions.build(create_discussion_params)
        end

        def after_success
          create_options
          create_image
          publish_discussion_to_subscribers_async
        end

        def create_options
          if discussion_type_poll?
            attributes_hash[:options].each {|option| create_option_with_service(option) }
          end
        end

        def create_option_with_service(option_hash)
          service = DiscussionOptions::CreateService.new(@record, @user, { option: option_hash })
          service.execute
          service.success?
        end

        def publish_async?
          CDI::Config.enabled?(:publish_discussion_to_subscribers_async)
        end

        def publish_discussion_to_subscribers_async
          if publish_async?
            ::CDI::V1::PostDiscussionToSubscribersWorker.perform_async(@record.id)
          else
            publish_discussion_to_subscribers
          end
        end

        def publish_discussion_to_subscribers
          service = ::CDI::V1::Discussions::PostDiscussionToSubscribersService.new(@record)
          service.execute
        end
      end
    end
  end
end
