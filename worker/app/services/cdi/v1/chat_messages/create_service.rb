# encoding: UTF-8

module CDI
  module V1
    module ChatMessages
      class CreateService < BaseCreateService

        record_type ::ChatMessage

        attr_reader :chat_room

        WHITELIST_ATTRIBUTES = [:content]

        def initialize chat_room, user, options={}
          super user, options
          @chat_room = chat_room
        end

        def can_create_record?
          # user must be a memeber of the room
          @chat_room.all_users.include?(@user)
        end

        def build_record
          @user.chat_messages.build(chat_message_params)
        end

        def after_success
          @chat_room.all_users.each do |u|
            if (u.id != @record.user_id)
              Pusher.trigger("private-user-#{u.id}", "chat_message", ChatMessageSerializer.new(@record))
            end
          end
        end

        def chat_message_params
          if @chat_message_params.present?
            return @chat_message_params
          end
          @chat_message_params = filter_hash(@options[:chat_message], WHITELIST_ATTRIBUTES)
          @chat_message_params[:chat_room] = @chat_room
          @chat_message_params
        end
      end
    end
  end
end
