# encoding: UTF-8

module CDI
  module V1
    module ChatMessages
      class UpdateService < BaseUpdateService

        record_type ::ChatMessage

        def record_params
          filter_hash(@options[:chat_message], [:content])
        end
      end
    end
  end
end
