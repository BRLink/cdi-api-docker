# encoding: UTF-8

module CDI
  module V1
    module GameWordSearches
      class UpdateService < Games::BaseUpdateService

         include V1::ServiceConcerns::GameWordSearchesParams

        record_type ::GameWordSearch

        def game_params
          game_word_search_params
        end

        def base_options_key
          :game
        end

      end
    end
  end
end
