# encoding: UTF-8

module CDI
  module V1
    module GameWordSearches
      class DeleteService < Games::BaseDeleteService

        record_type ::GameWordSearch

      end
    end
  end
end
