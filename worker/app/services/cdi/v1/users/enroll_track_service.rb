module CDI
  module V1
    module Users
      class EnrollTrackService < BaseActionService

        attr_reader :user, :student_class, :learning_track

        action_name :user_enroll_learning_track

        def initialize(user, student_class, options = {})
          @user, @student_class = user, student_class
          super(options)
        end

        def execute_action
          begin
            @user.enrolled_learning_tracks.create(enrolled_track_params)
          rescue ActiveRecord::RecordNotUnique, PG::UniqueViolation => e
            if enroll = @user.enrolled_learning_tracks.not_enrolled.find_by(enrolled_track_params)
              enroll.migrate_to_enrolled
              return true
            end

            return false
          end
        end

        private
        def after_success
          learning_track.touch
        end

        def action_errors
          @errors
        end

        def success_runned_action?
          valid?
        end

        def user_can_execute_action?
          return true if @options[:validate] == false

          unless valid_student_class?
            return not_found_error!('student_classes.not_found')
          end

          unless valid_learning_track?
            return not_found_error!('learning_tracks.not_found')
          end

          if yet_coursing_track?
            return forbidden_error!('learning_tracks.yet_enrolled_for_this_class')
          end

          return true
        end

        def enrolled_track_params
          params = {
            learning_track_id: learning_track_id,
            student_class_id: student_class.id
          }

          params.merge!(status: @options[:status]) if @options[:allow_status_change].present?

          params
        end

        def yet_coursing_track?
          return false unless valid_learning_track?

          enrolled_track = @user.enrolled_learning_tracks.real_enrolled.find_by(enrolled_track_params)

          valid_object?(enrolled_track, ::EnrolledLearningTrack)
        end

        def learning_track
          return nil unless valid_student_class?

          @learning_track ||= student_class.learning_tracks.find_by(id: learning_track_id)
        end

        def learning_track_id
          @options[:learning_track_id]
        end

        def valid_student_class?
          valid_object?(@student_class, ::StudentClass)
        end

        def valid_learning_track?
          valid_object?(learning_track, ::LearningTrack)
        end

        def valid_record?
          valid_user?
        end

        def record_error_key
          :users
        end
      end
    end
  end
end
