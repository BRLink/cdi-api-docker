module CDI
  module V1
    module Users
      class FacebookLinkService < BaseActionService

        include V1::ServiceConcerns::OauthParams

        attr_reader :user

        action_name :facebook_link_account

        def initialize(user, options = {})
          @user = user
          super(options)
        end

        def execute_action
          begin
            @user.oauth_provider_uid = facebook_data[:oauth_provider_uid]
            @user.oauth_provider     = facebook_data[:oauth_provider]
            @user.save
          rescue Exception => e
            bad_request_error(e.message)
            Rollbar.error(e)
          end
        end

        def after_success
          @record = @user
          fetch_avatar_from_provider
        end

        def action_errors
          @facebook_auth_provider.try(:errors) || @errors
        end

        def success_runned_action?
          [@user.oauth_provider_uid, @user.oauth_provider_uid].all?(&:present?) && valid?
        end

        def user_can_execute_action?
          if using_facebook_data? && invalid_facebook_access_token?
            return bad_request_error!('users.invalid_facebook_access_token')
          end

          if another_user_linked?
            return forbidden_error!('users.another_user_is_linked_with_given_facebook_profile')
          end

          return true
        end

        def another_user_linked?
          user = facebook_auth_provider.fetch_user
          facebook_auth_provider.existent_user? && (user and user.id != @user.id)
        end

        def valid_record?
          valid_user?
        end

        def record_error_key
          :users
        end

        def cache_keys_to_expire_after_success
          %w(current_user.show) # /users/me
        end

        def replace_data_for_cache_key(key)
          {
            user_id: @user.id
          }
        end

      end
    end
  end
end
