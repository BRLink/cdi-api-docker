require 'open-uri'

module CDI
  module V1
    module Users
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::UserParams

        record_type ::User

        def record_params
          user_params
        end

        def user_can_update?
          if user_params[:profile_type].present?
            if @record.admin?
              return forbidden_error!('users.cant_change_admin_profile_type')
            end

            if @record.multiplier? && user_params[:profile_type].to_s == 'student'
              return forbidden_error!('users.multiplier_cant_update_profile_to_student')
            end

            if @record.student? && user_params[:profile_type].to_s != 'student'
              return forbidden_error!('users.student_cant_change_profile_type')
            end
          end

          if user_updating_address?
            unless valid_cep?
              return not_found_error!('addresses.invalid_cep')
            end

            if !valid_city? && !replace_cep_data?
              return not_found_error!('cities.not_found')
            end

          end

          @record.user_can_update?(@user)
        end

        private
        def changing_zipcode?
          user_updating_address? && address_attributes[:zipcode].present?
        end

        def valid_cep?
          return true unless CDI::Config.enabled?(:validate_user_cep_on_update)
          return true unless changing_zipcode?

          current_zipcode_data.present?
        end

        def current_zipcode_data
          return @zipcode unless @zipcode.nil?

          @zipcode = zipcode_data(address_attributes[:zipcode]) || false
        end

        def zipcode_data(zipcode)
          zipcode = zipcode.to_s.parameterize.gsub(/\s?|\-/, '')
          data = nil
          cep_endpoint = POSTMON_ENDPOINT % zipcode

          begin
            zipcode_data = open(cep_endpoint)
            data = JSON.parse(zipcode_data.read).symbolize_keys rescue nil
          rescue
            data = nil
          end

          data
        end

        def after_success
          if user_updating_address?
            old_address = user.address
            update_user_address

            if old_address
              address_changes = changes(old_address, @user.address, address_attributes.keys).map {|a| "address_#{a}"}
            else
              address_changes = address_attributes.keys
            end

            if address_changes.any?
              changed_attributes_array.concat(address_changes)
              changed_attributes.concat(address_changes) # to for changed? return true
            end


            @record.reload if changed?
          end
        end

        def cache_keys_to_expire_after_success
          %w(current_user.show) # /users/me
        end

        def user_updating_address?
          address_attributes && address_attributes.any? {|_, v| v.present? }
        end

        def address_attributes
          attrs = (@options[:user] && @options[:user] && @options[:user][:address]) || {}
          filter_hash(attrs, ADDRESS_WHITELIST_ATTRIBUTES)
        end

        def valid_city?
          valid_object?(city, ::City)
        end

        def city
          return nil unless user_updating_address?
          @city ||= City.find_by(id: address_attributes[:city_id])
        end

        def address_attributes_from_zipcode
          city_name = current_zipcode_data[:cidade]
          city_uf   = current_zipcode_data[:estado]
          city_id = ::City.joins(:state)
                      .find_by(name: city_name, states: { acronym: city_uf })
                      .try(:id)

          address_attributes.dup.merge!(
            street: current_zipcode_data[:logradouro],
            district: current_zipcode_data[:bairro],
            city_id: city_id || address_attributes[:city_id]
          )
        end

        def replace_cep_data?
          CDI::Config.enabled?(:validate_user_cep_on_update) &&
          CDI::Config.enabled?(:replace_address_data_with_cep_data) &&
          current_zipcode_data.present?
        end

        def update_user_address
          zipcode = address_attributes[:zipcode]
          address_attrs = replace_cep_data? ?
                          address_attributes_from_zipcode :
                          address_attributes

          if @user.address.blank?
            @user.create_address(address_attrs)
          else
            @user.address.update(address_attrs)
          end

          @user.address
        end

        def needs_cache_expiration?
          return true if changed_attributes.member?(:password)

          super
        end
      end
    end
  end
end
