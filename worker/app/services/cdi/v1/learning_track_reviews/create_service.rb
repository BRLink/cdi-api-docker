# encoding: UTF-8

module CDI
  module V1
    module LearningTrackReviews

      class CreateService < BaseCreateService

        include V1::ServiceConcerns::LearningTrackReviewsParams

        record_type ::LearningTrackReview, alias_name: :review

        private
        def can_create_record?
          unless valid_learning_track?
            return not_found_error!('learning_tracks.not_found')
          end

          true
        end

        def build_record
          @user.sent_tracks_reviews.build(track_review_params)
        end

        def valid_learning_track?
          valid_object?(learning_track, ::LearningTrack)
        end

        def learning_track
          track_id = attributes_hash.fetch(:learning_track_id, nil)

          return nil if track_id.blank?

          if @user.backoffice_profile?
            @learning_track ||= @user.find_editable_learning_track(track_id)
          else
            @learning_track ||= @user.enrolled_tracks.find_by(id: track_id)
          end
        end

        def after_success
          notify_user
        end

        def notify_user
          teacher = learning_track.user

          create_system_notification_async(
            sender_user: @user,
            receiver_user: teacher,
            notification_type: :learning_track_review,
            notificable: learning_track
          )
        end
      end
    end
  end
end
