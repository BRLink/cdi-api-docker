# encoding: UTF-8

module CDI
  module V1
    module Posts
      class PostToSubscribersService < BaseService

        def initialize post, options={}
          super options
          @post = post
        end

        def execute
          if can_execute?
            users.each { |user| create_subscriber_post(user) }
            create_subscriber_post @post.user
          end
        end

        def can_execute?
          !@post.nil?
        end

        def users
          all_users = []
          @post.visibilities.each { |v| all_users += users_by_visibilities(v) }
          all_users - [@post.user]
        end

        def users_by_visibilities visibility
          if visibility.students?
            @post.user.subscribers_users.student
          elsif visibility.multipliers?
            @post.user.subscribers_users.multiplier
          elsif visibility.all?
            @post.user.subscribers_users
          else
            obj = visibility.visibility_object
            @post.user.subscribers_users.reduce_by_visibility(obj)
          end
        end

        def create_subscriber_post user
          subscription = user.user_subscriptions.find_by(publisher: @post.publisher)
          @post.subscriber_posts.build(user_subscription: subscription).save

          notify_user user
        end

        def notify_user user
          unless user == @post.user
            create_system_notification_async(
              sender_user: @post.user,
              receiver_user: user,
              notification_type: :new_post,
              notificable: @post
            )
          end
        end
      end
    end
  end
end
