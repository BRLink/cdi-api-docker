# encoding: UTF-8

module CDI
  module V1
    module Posts
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::PostParams
        include V1::ServiceConcerns::ResourceVisibilityParams

        record_type ::Post

        def initialize resource, user, options={}
          super user, options
          @resource = resource
        end

        def can_create_record?
          if @resource.nil?
            return not_found_error!("#{record_error_key}.resource_not_found")
          end
          unless has_visibilities?
            return forbidden_error!("#{record_error_key}.supply_at_least_one_visibility")
          end
          true
        end

        def build_record
          @user.posts.build(post_params)
        end

        def after_success
          build_photos
          build_multimedias
          build_albums
          build_challenge
          build_discussion
          publish_post_to_subscribers_async
        end

        def build_photos
          @record.attachments << photos_as_attachments
        end

        def build_multimedias
          @record.attachments << multimedias_as_attachments
        end

        def build_albums
          @record.attachments << albums_as_attachments
        end

        def build_discussion
          if attributes_hash[:discussion]
            discussion_id = attributes_hash[:discussion]
            discussion = Discussion.find_by(id: discussion_id)
            return nil unless discussion
            @record.attachments.create(attachment: discussion)
          end
        end

        def build_challenge
          if attributes_hash[:challenge]
            challenge_id = attributes_hash[:challenge]
            challenge = Challenge.published.find_by(id: challenge_id)
            return nil unless challenge
            @record.attachments.create(attachment: challenge)
          end
        end

        def photos_as_attachments
          return [] unless attributes_hash[:photos]
          attributes_hash[:photos].map do |photo_id|
            photo = @user.photos.find_by(id: photo_id)
            next unless photo
            @record.attachments.build(attachment: photo)
          end
        end

        def multimedias_as_attachments
          return [] unless attributes_hash[:multimedias]
          attributes_hash[:multimedias].map do |media_id|
            media = @user.multimedias.find_by(id: media_id)
            next unless media
            @record.attachments.build(attachment: media)
          end
        end

        def albums_as_attachments
          return [] unless attributes_hash[:albums]
          attributes_hash[:albums].map do |album_id|
            album = @user.albums.visible.find_by(id: album_id)
            next unless album
            @record.attachments.build(attachment: album)
          end
        end

        def post_params
          @post_params ||= filter_hash(attributes_hash, [:content]).tap do |params|
            params[:publisher] = @resource
            params[:category_ids] = array_values_from_params(@options, :category_ids, :post)
            params[:visibilities_attributes] = nested_visibilities_attrs
          end
          @post_params
        end
      end
    end
  end
end
