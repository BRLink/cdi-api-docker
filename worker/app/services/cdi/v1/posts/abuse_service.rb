# encoding: UTF-8

module CDI
  module V1
    module Posts
      class AbuseService < BaseCreateService

        record_type ::PostAbuse

        def initialize post, user, options={}
          super user, options
          @post = post
        end

        def can_create_record?
          if @post.nil?
            return not_found_error!('posts.resource_not_found')
          end
          if @post.abuses.where(user: @user).count > 0
            return not_found_error!('posts.abuse.duplicate')
          end
          @post.user_can_read? @user
        end

        def duplicate_record?
          false
        end

        def build_record
          @post.abuses.build(user: @user)
        end
      end
    end
  end
end
