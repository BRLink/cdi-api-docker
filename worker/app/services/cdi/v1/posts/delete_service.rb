# encoding: UTF-8

module CDI
  module V1
    module Posts
      class DeleteService < BaseDeleteService

        record_type ::Post

        def destroy_record
          @photos = @record.photos.map { |photo| photo }
          super
        end

        def after_success
          @photos.each { |p| p.destroy }
        end
      end
    end
  end
end
