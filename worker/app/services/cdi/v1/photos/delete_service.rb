# encoding: UTF-8

module CDI
  module V1
    module Photos
      class DeleteService < BaseDeleteService

        record_type ::Photo

      end
    end
  end
end
