# encoding: UTF-8

module CDI
  module V1
    module Photos
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::PhotoParams

        record_type ::Photo

        private
        def build_record
          current_photo_params = create_photo_params.dup
          album_id = current_photo_params.delete(:album_id)

          @album = @user.find_album_or_use_default(id: album_id)

          @album.photos.build(current_photo_params)
        end

        def can_create_record?
          return true
        end
      end
    end
  end
end
