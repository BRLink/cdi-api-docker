module CDI
  module V1
    module ResourcesFeatured
      class DeleteService < BaseDeleteService

        record_type ::ResourceFeatured
      end
    end
  end
end
