# encoding: UTF-8

module CDI
  module V1
    module Multimedia
      class CloneService < Libraries::CloneService
        record_type ::Multimedia
      end
    end
  end
end
