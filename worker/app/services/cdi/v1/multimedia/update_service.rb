# encoding: UTF-8

module CDI
  module V1
    module Multimedia
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::MultimediaParams

        record_type ::Multimedia

        def record_params
          multimedia_params.keep_if { |_, v| v.present? }
        end
        def update_record
          params = record_params
          @record.update(params)
          @record
        end

      end
    end
  end
end
