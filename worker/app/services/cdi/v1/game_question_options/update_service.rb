# encoding: UTF-8

module CDI
  module V1
    module GameQuestionOptions
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::GameQuestionOptionsParams

        record_type ::GameQuestionOption

        def record_params
          params = game_question_option_params
          params.delete(:text) if @record.question.questionable_type == 'GameTrueFalse'

          params
        end

        def update_record
          game_question_option_params.each {|k, v| @record.send("#{k}=", v) }

          if @record.changed?
            @record.save
          end

          @record
        end

        def user_can_update?
          @record.question.questionable.user_can_update?(@user)
        end

      end
    end
  end
end
