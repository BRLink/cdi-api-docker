# encoding: UTF-8

module CDI
  module V1
    module GameQuestionOptions

      class ReorderService < BaseReorderService

        record_type ::GameQuestionOption

        def record_error_key
          :game_question_options
        end

        def game_question
          @record.try(:question)
        end

      end
    end
  end
end
