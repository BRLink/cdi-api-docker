# encoding: UTF-8

module CDI
  module V1
    module GameTrueFalses
      class UpdateService < Games::BaseUpdateService

        include V1::ServiceConcerns::GameTrueFalseParams

        record_type ::GameTrueFalse

        private
        def game_params
          game_true_false_params
        end

        def base_options_key
          :game
        end

      end
    end
  end
end
