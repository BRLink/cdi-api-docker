module CDI
  module V1
    module SurveyPolls
      class DeleteService < BaseDeleteService

        record_type ::SurveyPoll
      end
    end
  end
end
