module CDI
  module V1
    module SurveyOpenQuestions
      class DeleteService < BaseDeleteService

        record_type ::SurveyOpenQuestion
      end
    end
  end
end
