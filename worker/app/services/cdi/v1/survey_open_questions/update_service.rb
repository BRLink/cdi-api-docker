module CDI
  module V1
    module SurveyOpenQuestions
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::SurveyParams
        include V1::ServiceConcerns::SurveyAttachmentParams

        record_type ::SurveyOpenQuestion

        def record_params
          survey_open_question_params
        end

        def whitelist_attributes_to_clear
          ['video_link']
        end

        def after_success
          update_image
        end
      end
    end
  end
end
