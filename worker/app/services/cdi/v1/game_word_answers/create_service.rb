# encoding: UTF-8

module CDI
  module V1
    module GameWordAnswers
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::GameWordAnswersParams
        include V1::Games::GameProgressTrackMethods

        record_type ::GameWordAnswer, alias_name: :answer

        alias :answers :answer

        attr_reader :game

        def initialize(game, user, options = {})
          @game = game
          super(user, options)
        end

        private
        def can_create_record?
          return false unless valid_user?

          unless valid_game?
            return not_found_error!('game.not_found')
          end

          return can_create_answer?
        end

        def can_create_answer?
          return student_user_can_access_game? if @user.student?

          return @user.backoffice_profile?
        end

        def student_user_can_access_game?
          return false unless valid_game?

          @enroll_track ||= @user.enrolled_tracks.joins(:presentation_slides).where(presentation_slides: {
            associated_resource: game.is_a?(GameGroup) ? game.game_group_item : game
          })

          @enroll_track.present?
        end

        def build_record
          game.answers.build(answer_params.merge(user_id: @user.id))
        end

        def after_success
          register_user_progress_for_track
        end

        def save_record
          begin
            @record.save
          rescue PG::UniqueViolation, ActiveRecord::RecordNotUnique => e
            Rollbar.error(e)
            forbidden_error("#{game.class.to_s.underscore.pluralize}.yet_answered")
            false
          end
        end

      end
    end
  end
end
