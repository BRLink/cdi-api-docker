# encoding: UTF-8

module CDI
  module V1
    module GameQuestions
      class CreateService < BaseCreateService

      include V1::ServiceConcerns::GameQuestionParams

        record_type ::GameQuestion

        private
        # def validate_ip_on_create?
        #   @options[:validate_ip_on_create] != false
        # end

        def can_create_record?
          return false unless valid_user?

          unless valid_game?
            return not_found_error!("#{error_record_key}.not_found")
          end

          return can_create_question?
        end

        def can_create_question?
          if game.is_a?(GamePoll) && game.question.present?
            return forbidden_error!("game_polls.only_one_question_allowed_per_poll")
          end

          return @user.backoffice_profile?
        end

        def build_record
          if game.respond_to?(:questions)
            game.questions.build(game_question_params)
          elsif game.respond_to?(:question)
            game.build_question(game_question_params)
          end
        end

        def after_success
          set_options_data_for_question

          if has_options?
            service_options = game_options_hash.merge(@options.slice(:origin))

            service = GameQuestionOptions::BatchCreateService.new(@record, @user, service_options)
            service.execute
          end
        end

        def error_record_key
          (game_type.presence && game_type.underscore.pluralize) || 'games'
        end

        def game_options_hash
          return {} if game.is_a?(::GameOpenQuestion)

          @options[:question].slice(:options)
        end

        def has_options?
          return false if @options[:without_options].present?

          @options[:question].key?(:options) && game_options_hash.any?
        end
      end
    end
  end
end
