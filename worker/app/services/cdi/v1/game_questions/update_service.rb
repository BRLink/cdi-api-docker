# encoding: UTF-8

module CDI
  module V1
    module GameQuestions
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::GameQuestionParams

        record_type ::GameQuestion

        # for serializers
        alias :game_quiz :game
        alias :game_poll :game
        alias :game_open_question :game
        alias :game_true_false :game

        def record_params
          game_question_params
        end

        def after_success
          set_true_false_game_questions_correct

          if game_options = @options[:question].fetch(:options, nil)
            # Hash is composed of { id => option_data }

            if game_options.is_a?(Hash)
              game_options.each do |option_id, option_data|
                next if option_id.blank?

                update_question_option(option_id, option_data)
              end
            end

            if game_options.is_a?(Array)
              game_options.each_with_index do |option_data, index|
                next if option_data.blank?

                # needs convertion to string if key is a :symbol
                option_id = option_data.keys.shift.to_s.to_i
                option_data = option_data.values.shift

                update_question_option(option_id, option_data)
              end
            end

            @record.reload
            game.reload
          end
        end

        def set_true_false_game_questions_correct
          return nil unless game.is_a?(GameTrueFalse)

          set_options_data_for_question_game_true_false
          options = game_question.options
          coptions = @options[:question][:options].dup

          game_options = options.map do |option|
            {
              option.id => coptions.shift.slice(:correct)
            }
          end

          @options[:question][:options] = game_options
        end

        def update_question_option(option_id, option_data)
          option = game.options.find_by(id: option_id.to_s)

          return nil unless option

          service = ::CDI::V1::GameQuestionOptions::UpdateService.new(option, @user, option: option_data)
          service.execute
        end

        def user_can_update?
          unless valid_game?
            return not_found_error!("#{game_type || 'games'}.not_found")
          end

          return false unless @user.backoffice_profile?
          super
        end
      end

    end
  end
end
