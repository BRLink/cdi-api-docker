# encoding: UTF-8

module CDI
  module V1
    module GameQuestions
      class DeleteService < BaseDeleteService

        record_type ::GameQuestion

        def destroy_record
          ActiveRecord::Base.transaction do
            @record.options.delete_all
            @record.destroy
          end
        end

        def after_success
        end

        def user_can_delete?
          @record.questionable.user_can_update?(@user)
        end

      end
    end
  end
end
