# encoding: UTF-8

module CDI
  module V1
    module GameQuestions
      class ReplaceService < BaseActionService

        include V1::ServiceConcerns::GameQuestionParams

        action_name :game_questions_replace

        attr_reader :game

        alias :game_quiz :game
        alias :game_poll :game
        alias :game_open_question :game

        def initialize(game, user, options={})
          @game = game
          @user = user
          super(options)
        end

        def user_can_execute_action?
          unless can_update_game?
            return forbidden_error!("#{record_error_key}.cant_update_published")
          end

          unless valid_questions?
            return bad_request_error!("#{record_error_key}.invalid_questions_for_replace")
          end

          return true
        end

        def can_update_game?
          return false unless valid_game?

          return game.status_draft?
        end

        def success_runned_action?
          @success
        end

        def execute_action
          # dependent destroy not working properly
          ActiveRecord::Base.transaction do
            delete_questions_options
            delete_questions
          end

          create_questions
        end

        def delete_questions
          ::GameQuestion.where(id: game.question_ids).delete_all
        end

        def delete_questions_options
          return false unless game.respond_to?(:options)

          option_ids = game.options.pluck(:id)
          ::GameQuestionOption.where(id: option_ids).delete_all
        end

        def valid_record?
          valid_game?
        end

        def record_error_key
          (game && game.class.to_s.underscore.pluralize) || 'games'
        end

        def valid_questions?
          return false if @options[:questions].blank?
          return false unless @options[:questions].is_a?(Array)

          # validate if received question data is valid before
          # replacing the current game questiosn
          @options[:questions].all? do |question_hash|
            valid_question_object?(question_hash)
          end
        end

        def valid_question_object?(question_hash)
          return false unless question_hash.respond_to?(:keys)

          keys = question_hash.keys

          return false unless question_hash.values.all?

          return true if game_type == 'GameOpenQuestion'

          return false unless keys.member?(:title) && keys.member?(:options)
          return false if question_hash[:options].blank?

          return false unless question_hash[:options].all? {|opt| opt[:text].present? }

          return true
        end

        def create_questions
          @success = @options[:questions].all? do |question_hash|
            create_question_with_service(question_hash)
          end
          game.reload if @success

          @success
        end

        def create_question_with_service(question_hash, options = {})
          question_hash = question_hash.last if question_hash.is_a?(Array)

          options = {
            question: question_hash.merge(game_id: game.id, game_type: game.class.to_s),
            validate_ip_on_create: false
          }.deep_merge(options)

          service = GameQuestions::CreateService.new(@user, options)
          service.execute

          service.success?
        end

      end
    end
  end
end
