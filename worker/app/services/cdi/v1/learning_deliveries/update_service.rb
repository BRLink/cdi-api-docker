# encoding: UTF-8

module CDI
  module V1
    module LearningDeliveries
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::LearningDeliveriesParams

        record_type ::LearningDelivery

        def record_params
          learning_delivery_params
        end
      end

    end
  end
end
