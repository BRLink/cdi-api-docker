module CDI
  module V1
    module StudentDeliveries
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::StudentDeliveriesParams

        record_type ::StudentDelivery

        private

          def can_create_record?
            return false unless valid_user?

            unless valid_learning_delivery?
              return not_found_error!('learning_deliveries.not_found')
            end

            unless valid_student_class?
              return not_found_error!('student_classes.not_found')
            end

            unless valid_students?
              return forbidden_error!('learning_deliveries.invalid_students')
            end

            return true
          end

          def valid_students?
            return true unless group_delivery?

            delivery_students.any?
          end

          def build_record
            params = {
              learning_track_id: learning_track.try(:id),
              learning_delivery_id: learning_delivery.try(:id),
            }.merge(student_delivery_params)

            @user.student_deliveries.build(params)
          end

          def delivery_students
            return [] if students.blank?

            max_students = learning_delivery.group_size

            students.select {|user| user.student_class_ids.member?(student_class_id) }[0, max_students]
          end

          def delivery_student_ids
            delivery_students.map { |u| u.id }.concat([@user.id]).uniq
          end

          def after_success
            @record.student_ids = delivery_student_ids
            @record.learning_track.touch
            update_enrollment_progress
            convert_presentation if is_presentation?
            send_notification_to_multiplier
            send_notification_to_students
          end

        protected

          def update_enrollment_progress
            search_query = {
              student_class_id: student_class.try(:id),
              learning_track: learning_track.try(:id)
            }
            enrollment = @user.enrolled_learning_tracks.find_by(search_query)
            update_async = CDI::Config.enabled?(:update_enrollment_progress_after_student_delivery_async)
            method = update_async ? :update_progress_async : :update_progress
            enrollment.try(method)
          end

          def is_presentation?
            return false unless @record.file.present?
            LearningDelivery::VALID_FORMAT_TYPES[:presentation].include?(@record.format_type.to_sym)
          end

          def presentation_id
            if @record.presentation.nil?
              p = Presentation.create(user_id: @user.id, file: @record.file)
              @record.presentation_id = p.id
              @record.save
            end

            @record.presentation_id
          end

          def convert_presentation
            Shoryuken::Client.queues("#{Rails.env}_converter").send_message({
              user_id: @user.id,
              presentation_id: presentation_id,
              start_position: 0
            }.to_json)
          end

          def send_notification_to_multiplier
            multiplier = @record.student_class.admin_user
            create_system_notification_async(
              sender_user: @user,
              receiver_user: multiplier,
              notification_type: :new_student_delivery,
              notificable: @record
            )
          end

          def send_notification_to_students
            @record.students.each do |student|
              create_system_notification_async(
                sender_user: @user,
                receiver_user: student,
                notification_type: :new_delivery_for_student,
                notificable: @record
              )
            end
          end
      end
    end
  end
end
