# encoding: UTF-8

module CDI
  module V1
    module Presentations
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::PresentationParams

        record_type ::Presentation

        private

        def can_create_record?
          error_key = (presentation_for_class || 'presentation_for').to_s.underscore.pluralize

          unless valid_presentation_for?
            return not_found_error!("#{error_key}.not_found")
          end

          unless valid_presentation_for_options?
            return bad_request_error("presentation_for.invalid_params")
          end

          if existent_presentation?
            return forbidden_error!("#{error_key}.presentation_yet_created")
          end

          return @user.backoffice_profile?
        end

        def valid_presentation_for_options?
          return true if presentation_for.blank? && presentation_for_class.blank?

          [presentation_for.present?, presentation_for_class.present?].all?
        end

        def build_record
          presentation_attributes = presentation_params.merge!(user_id: @user.id, presentation_type: presentation_type)

          if valid_presentation_for_record?
            build_presentation_method = "build_#{presentation_association_method}"
            presentation_for.send(build_presentation_method, presentation_attributes)
          else
            p = ::Presentation.new(presentation_attributes)
          end
          if @options[:uploaded]
            convert p
          end
          p
        end

        def valid_presentation_for?
          return true if presentation_for.blank? && presentation_for_class.blank?
          valid_presentation_for_record?
		    end

        def convert p
          p.create_conversion_status(status: :pending)
          CDI::V1::PresentationConvertWorker.perform_async(p.id)
        end

        def learning_track
          @learning_track ||= @user.learning_tracks.find_by(id: presentation_params[:learning_track_id])
        end

        def valid_presentation_for_record?
          valid_object?(presentation_for, presentation_for_class)
        end

        def presentation_for_class
          @options[:presentation_for_class]
        end

        def presentation_for
          @options[:presentation_for]
        end

        def presentation_association_method
          @options[:presentation_association_method] || :presentation
        end

        def presentation_type
          ( (@options[:presentation_type]) ||
          (@options[:presentation] && @options[:presentation].fetch(:presentation_type, nil)) )
        end

        def existent_presentation?
          if valid_presentation_for_record?
            return presentation_for.send(presentation_association_method).present?
          end

          return false
        end

        def after_success
          # save presentation.id on associated column
          if valid_presentation_for_record?
            presentation_for.save!
          end
        end
      end

    end
  end
end
