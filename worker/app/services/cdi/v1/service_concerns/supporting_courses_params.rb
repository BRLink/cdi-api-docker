module CDI
  module V1
    module ServiceConcerns
      module SupportingCoursesParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :title,
          # :presentation_id,
          :category_ids,
          :tags
        ]

        included do
          def supporting_course_params
            @supporting_course_params ||= filter_hash(@options[:supportingcourse], WHITELIST_ATTRIBUTES)
            @supporting_course_params[:tags] = tags_from_params
            @supporting_course_params[:skill_ids] = skills_from_params
            @supporting_course_params[:category_ids] = categories_from_params

            @supporting_course_params
          end

          def categories_from_params
            array_values_from_params(@options, :category_ids, :supportingcourse)
          end

          def tags_from_params
            array_values_from_params(@options, :tags, :supportingcourse)
          end

          def skills_from_params
            array_values_from_params(@options, :skills, :supportingcourse)
          end

        end
      end
    end
  end
end
