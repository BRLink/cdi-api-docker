module CDI
  module V1
    module ServiceConcerns
      module SurveyQuestionParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :title
        ]

        VALID_SURVEYS = [
          ::SurveyOpenQuestion,
          ::SurveyPoll
        ]

        included do

          def attributes_hash
            @options[:question]
          end

          def valid_survey?
            VALID_SURVEYS.any? { |survey_class| valid_object?(@survey, survey_class) }
          end

          def survey_poll?
            @survey.is_a?(::SurveyPoll)
          end

          def survey_question_params
            @survey_question_params ||= filter_hash(@options[:question], [:title])
          end
        end
      end
    end
  end
end
