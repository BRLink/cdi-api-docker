module CDI
  module V1
    module ServiceConcerns
      module GameItemsParams
        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :word,
          :related_word,
          :image,
          :related_image
        ]

        included do
          def game_item_params
            @game_item_params ||= filter_hash(attributes_hash, WHITELIST_ATTRIBUTES)
          end

          def attributes_hash
            @options[:item]
          end

        end
      end
    end
  end
end
