module CDI
  module V1
    module ServiceConcerns
      module LearningCycleParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :name,
          :category_ids,
          :objective_text,
          :tags
        ]

        included do
          def learning_cycle_params
            @learning_cycle_params ||= filter_hash(attributes_hash, WHITELIST_ATTRIBUTES).tap do |params|
              params[:tags] = tags_from_params
              params[:category_ids] = categories_from_params
            end
          end

          def categories_from_params
            array_values_from_params(attributes_hash, :category_ids)
          end

          def tags_from_params
            array_values_from_params(attributes_hash, :tags)
          end

          def attributes_hash
            @options[:learning_cycle]
          end
        end
      end
    end
  end
end
