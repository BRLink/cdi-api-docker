module CDI
  module V1
    module ServiceConcerns
      module StudentDeliveriesParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :format_type,
          :learning_delivery_id,
          :student_class_id,
          :file,
          :link
        ]

        included do
          def student_delivery_params
            @student_delivery_params ||= filter_hash(@options[:delivery], WHITELIST_ATTRIBUTES)
          end

          def student_ids
            array_values_from_params(@options[:delivery], :students_ids)
          end

          def students
            return [] if student_ids.blank?

            @students ||= User.where(id: student_ids)
          end

          def group_delivery?
            return false if learning_delivery.blank?

            learning_delivery.group_delivery?
          end

          def learning_delivery
            @learning_delivery ||= if learning_track_id.present? && valid_learning_track?
              if learning_track.has_learning_dynamic?
                slide = learning_track.learning_dynamic_slide
                begin
                  slide.associated_resource.learning_delivery
                rescue => e
                  Rollbar.error(e)
                  nil
                end
              end
            else
               LearningDelivery.find_by(id: learning_delivery_id)
            end
          end

          def learning_track
            @learning_track ||= @user.enrolled_tracks.find_by(id: learning_track_id)
          end

          def student_class
            @student_class ||= @user.student_classes.find_by(id: student_class_id)
          end

          def valid_student_class?
            valid_object?(student_class, ::StudentClass)
          end

          def valid_learning_delivery?
            valid_object?(learning_delivery, ::LearningDelivery)
          end

          def valid_learning_track?
            valid_object?(learning_track, ::LearningTrack)
          end

          def learning_delivery_id
            @options[:delivery] && @options[:delivery][:learning_delivery_id]
          end

          def learning_track_id
            @options[:delivery] && @options[:delivery][:learning_track_id]
          end

          def student_class_id
            @options[:delivery] && @options[:delivery][:student_class_id]
          end

        end
      end
    end
  end
end
