module CDI
  module V1
    module ServiceConcerns
      module SurveyAttachmentParams
        extend ActiveSupport::Concern

        included do

          def create_image
            if has_image?
              @record.attachments.create(attachment: image)
            end
          end

          def update_image
            attrs_to_clear = array_values_from_string(@options[:_clear_attrs] || '').map(&:to_sym)
            if has_image? || attributes_hash[:video_link] || attrs_to_clear.member?(:image)
              @record.attachments.where(attachment_type: 'Photo').destroy_all
            end
            if has_image?
              @record.attachments.create(attachment: image)
            end
          end

          def has_image?
            image.present?
          end

          def image
            if attributes_hash[:image]
              @image ||= @user.photos.find_by(id: attributes_hash[:image])
            end
          end
        end
      end
    end
  end
end
