module CDI
  module V1
    module ServiceConcerns
      module LearningDeliveriesParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :format_type,
          :delivery_date,
          :delivery_kind,
          :description,
          :group_size
        ]

        included do
          def learning_delivery_params
            @learning_delivery_params ||= filter_hash(@options[:delivery], WHITELIST_ATTRIBUTES)
          end
        end

      end
    end
  end
end
