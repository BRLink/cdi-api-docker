module CDI
  module V1
    module ServiceConcerns
      module ResourceVisibilityParams

        extend ActiveSupport::Concern

        def has_visibilities?
          type, id = attributes_hash[:visibility_type], attributes_hash[:visibility_id]
          unless Visibility::PRESET_VISIBILITY_TYPES.include?(type)
            return type.present? && id.present?
          end
          type.present?
        end

        def create_visibilities
          # select_visibilities.each { |v| create_visibility(v) }
          create_visibility({ type: attributes_hash[:visibility_type], id: attributes_hash[:visibility_id] })
        end

        def create_visibility(v)
          if v[:id].present?
            visibility = @record.visibilities.build(visibility_type: v[:type], visibility_id: v[:id])
          else
            visibility = @record.visibilities.build(visibility_type: v[:type])
          end
          visibility.save
        end

        def select_visibilities
          attr_hash = attributes_hash[:visibilities]
          preset_types = Visibility::PRESET_VISIBILITY_TYPES
          if attr_hash.any? { |v| preset_types.include?(v[:type]) }
            [attr_hash.select { |v| preset_types.include?(v[:type]) }.first]
          else
            attr_hash
          end
        end

        def nested_visibility_attrs
          filter_hash(attributes_hash, [:visibility_type, :visibility_id])
        end

        def nested_visibilities_attrs
          [nested_visibility_attrs]
        end
      end
    end
  end
end
