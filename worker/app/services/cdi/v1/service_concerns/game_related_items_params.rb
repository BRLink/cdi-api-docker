module CDI
  module V1
    module ServiceConcerns
      module GameRelatedItemsParams
        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = []

        included do
          def game_related_items_params
            @game_related_items_params ||= filter_hash(attributes_hash, WHITELIST_ATTRIBUTES)
          end
        end
      end
    end
  end
end
