module CDI
  module V1
    module ServiceConcerns
      module GameGroupParams
        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :title,
          :items
        ]

        included do
          def game_group_params
            @game_group_params ||= filter_hash(attributes_hash, WHITELIST_ATTRIBUTES)
          end

          def attributes_hash
            @options[:group]
          end

          def valid_game?
            valid_object?(game, ::GameGroupItem)
          end
        end
      end
    end
  end
end
