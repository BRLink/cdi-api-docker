module CDI
  module V1
    module ServiceConcerns
      module GameTrueFalseParams
        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = []

        included do
          def game_true_false_params
            @game_true_false_params ||= filter_hash(attributes_hash, WHITELIST_ATTRIBUTES)
          end
        end
      end
    end
  end
end
