module CDI
  module V1
    module ServiceConcerns
      module DiscussionParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [:title, :content, :video_link, :cover_id, :category_ids, :best_comment_id]

        included do

          def create_discussion_params
            @create_discussion_params ||= discussion_params.tap do |params|
              params[:discussion_type]         = discussion_type_poll? ? 'poll' : 'simple'
              params[:publisher]               = @publisher
              params[:visibilities_attributes] = nested_visibilities_attrs
            end
          end

          def discussion_params
            @discussion_params ||= filter_hash(attributes_hash, WHITELIST_ATTRIBUTES).tap do |params|
              params[:video_link] = nil if has_image?
              params[:cover_id] = nil if attributes_hash[:cover_id] == 0
              params[:tags] = tags_from_params if attributes_hash[:tags].present?
            end
          end

          def attributes_hash
            @options[:discussion] || {}
          end

          def has_cover?
            attributes_hash[:cover_id].present?
          end

          def valid_cover?
            return Photo.exists?(id: attributes_hash[:cover_id])
          end

          def has_best_comment?
            attributes_hash[:best_comment_id].present?
          end

          def valid_best_comment?
            return @record.comments.exists?(parent_id: nil, id: attributes_hash[:best_comment_id])
          end

          def valid_publisher?
            @publisher.present? && ::Discussion::VALID_PUBLISHER_TYPES.include?(@publisher.class.to_s)
          end

          def discussion_type_poll?
            attributes_hash[:options].present? &&
              attributes_hash[:options].size > 0
          end

          def tags_from_params
            array_values_from_params(@options, :tags, :discussion)
          end
        end
      end
    end
  end
end
