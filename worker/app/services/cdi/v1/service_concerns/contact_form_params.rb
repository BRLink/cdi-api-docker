module CDI
  module V1
    module ServiceConcerns
      module ContactFormParams
        extend ActiveSupport::Concern

        CONTACT_WHITELIST_ATTRIBUTES = [
          :name,
          :email,
          :phone_area_code,
          :phone_number,
          :subject,
          :message_body
        ]

        FEEDBACK_WHITELIST_ATTRIBUTES = [
          :name,
          :email,
          :description,
          :image,
          :type,
          :message_body
        ]

        included do
          def contact_form_params
            @contact_form_params ||= filter_hash(attributes_hash, CONTACT_WHITELIST_ATTRIBUTES)
          end

          def attributes_hash
            (@options[:contact] || {}).merge(data_from_user).symbolize_keys
          end

          def data_from_user
            return {} unless valid_user?

            @user.as_json(only: [:email], methods: [:name])
          end

          def feedback_contact_form_params
            @feedback_form_params ||= filter_hash(attributes_hash, FEEDBACK_WHITELIST_ATTRIBUTES)
          end
        end

      end
    end
  end
end
