# encoding: UTF-8

module CDI
  module V1
    module InstitutionManagers
      class DeleteService < BaseDeleteService

        record_type ::InstitutionManager
        include V1::ServiceConcerns::EducationalInstitutionsParams

      end
    end
  end
end
