# encoding: UTF-8

module CDI
  module V1
    module GameGroupItems
      class UpdateService < Games::BaseUpdateService

        include V1::ServiceConcerns::GameGroupItemsParams

        record_type ::GameGroupItem

        def changed_attributes
          attrs = super
          attrs.push(:groups) if updating_groups?

          attrs
        end

        private
        def game_params
          game_group_items_params
        end

        def base_options_key
          :game
        end

        def after_success
          if updating_groups?
            groups.each do |group_id, group_hash|
              group = find_group_for_game(group_id)

              next unless group

              service = update_group_with_service(group, group_hash)

              bad_request_error(service.errors) if service.fail? && @errors.blank?
            end

            @record.reload
          end
        end

        def update_group_with_service(group, group_hash, options = {})
          options = {
                      group: group_hash
                    }.deep_merge(options)


          service = GameGroups::UpdateService.new(group, @user, options)
          service.execute

          service
        end

        def find_group_for_game(group_id)
          return nil if group_id.blank?

          @game_groups ||= @record.groups

          @game_groups.find {|group| group.id == group_id.to_s.to_i }
        end

      end
    end
  end
end
