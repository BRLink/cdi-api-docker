module CDI
  module V1
    module SurveyQuestionOptions
      class CreateService < BaseCreateService

        record_type ::SurveyQuestionOption

        def initialize(question, user, options)
          super user, options
          @question = question
        end

        def can_create_record?
          unless valid_question?
            return forbidden_error('survey_question.not_found_or_invalid')
          end
          @question.user_id == @user.id
        end

        def valid_question?
          @question.present? && @question.question_poll?
        end

        def build_record
          @question.options.build(option_params)
        end

        def attributes_hash
          @options[:option]
        end

        def option_params
          filter_hash(attributes_hash, [:text])
        end
      end
    end
  end
end
