module CDI
  module V1
    module SurveyQuestionOptions
      class UpdateService < BaseUpdateService

        record_type ::SurveyQuestionOption

        def record_params
          filter_hash(@options[:option], [:text])
        end
      end
    end
  end
end
