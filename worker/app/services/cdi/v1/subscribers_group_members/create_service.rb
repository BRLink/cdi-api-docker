module CDI
  module V1
    module SubscribersGroupMembers
      class CreateService < BaseCreateService

        record_type ::SubscribersGroupMember

        def initialize group, user, options={}
          super user, options
          @group = group
          @created_members = []
        end

        def execute
          subscriptions.each do |subscription|
            member = @group.members.build(user_subscription: subscription)
            @created_members << member if member.save
          end
          success_response(201)
        end

        def created_members
          @created_members
        end

        def subscriptions
          @subscriptions ||= @user.subscribers.where(user_id: @options[:members]).select { |subscription| !@group.has_subscription?(subscription) }
        end
      end
    end
  end
end
