module CDI
  module V1
    module Challenges
      class PublishService < BaseActionService

        action_name :publish_challenge

        def initialize(challenge, user, options={})
          @challenge = challenge
          @user = user
          super options
        end

        def execute_action
          @challenge.set_status(:published, true)
        end

        def can_execute_action?
          can_execute = super
          if can_execute
            if Challenge.open.exists?
              return forbidden_error!("#{record_error_key}.other_challenge_open")
            end

            unless @challenge.draft?
              return forbidden_error!("#{record_error_key}.challenge_isnt_draft")
            end
          end

          can_execute
        end

        def success_runned_action?
          @challenge.valid?
        end

        def action_errors
          @challenge.errors
        end

        def user_can_execute_action?
          @user.admin?
        end

        def valid_record?
          valid_object?(@challenge, ::Challenge)
        end

        def record_error_key
          :challenge
        end
      end
    end
  end
end
