# encoding: UTF-8

module CDI
  module V1
    module GameQuestionsAnswers
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::GameQuestionsAnswersParams

        record_type ::GameQuestionAnswer, alias_name: :answer

        def record_params
          params = filter_hash(attributes_hash, [:correct])
          params.merge!(corrected_at: DateTime.now) if params[:correct] != @record.correct

          params
        end

        def attributes_hash
          @options[:answer] || @options
        end

        def after_success
          update_track_report
        end

        def update_track_report
          if update_track_report?
            if learning_track_id = attributes_hash.fetch(:learning_track_id, nil)
              learning_track = @user.learning_tracks.find_by(id: learning_track_id)

              if learning_track && student_class_id = attributes_hash.fetch(:student_class_id, nil)
                report = learning_track.reports.find_by(student_class_id: student_class_id)
                report.try(:update_student_row, @record.user, @record.game)
              end

            end
          end
        end

        def update_track_report?
          @record.corrected? && @old_record.correct != @record.correct
        end

      end
    end
  end
end
