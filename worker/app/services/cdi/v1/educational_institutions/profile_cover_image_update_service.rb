# encoding: UTF-8

module CDI
  module V1
    module EducationalInstitutions
      class ProfileCoverImageUpdateService < BaseUpdateService

        record_type ::EducationalInstitution

        include V1::ServiceConcerns::UserParams

        private

        def record_params
          @record_params ||= set_image_crop_params(filter_hash(@options[:institution], [:profile_cover_image]), :profile_cover_image)
        end

        def attributes_hash
          @options[:institution]
        end
        def educational_institution
          @record
        end
        def updating_profile_image?
          attributes_hash && attributes_hash[:profile_cover_image].present?
        end
      end
    end
  end
end
