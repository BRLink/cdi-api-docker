module CDI
  module V1
    module StudentClasses
      class AddStudentService < BaseUpdateService

        record_type ::StudentClass

        attr_reader :added_students

        def changed_attributes_array
          return [] if fail?
          [:users]
        end

        private
        def record_params
          @_params ||= { user_ids: user_id_from_options }
        end

        def user_id_from_options
          @options[:user_ids] || @options[:users_ids] || @options[:student_ids]
        end

        def users_ids
          # if student user is trying to join the class
          # (make sure that student user can only add yourself to a class)
          @user.student? ? [@user.id] : array_values_from_params(record_params, :user_ids)
        end

        def update_record
          @users = User.where(id: users_ids)
          @added_students = []

          @users.each do |user|
            if user_eligible_for_class?(user)
              @added_students << user
              @record.users << user
            end
          end

          @record
        end

        def update_errors
          @users.map do |user|
            { user.id => process_error_message_for_key('users.yet_student_of_class', {}) }
          end
        end

        def user_eligible_for_class?(user)
          return false if user.blank?

          # TODO: Allow any kind of user to join StudentClass
          # return false if (user.id == @user.id && !@user.student?)
          # only check user.student? in staging/production
          # user.student? && !user.in_class?(@record)

          !user.in_class?(@record)
        end

        def success_updated?
          @added_students.any?
        end

        def user_can_update?
          return false if @record.status_closed?
          # return super if @user.multiplier? # @record.user_can_update?(@user)

          return user_eligible_for_class?(@user)
        end

        def after_success
          notify_user
        end

        def notify_user
          create_system_notification_async(
            sender_user: @user,
            receiver_user: @record.admin_user,
            notification_type: :new_student_on_class,
            notificable: @record
          )
        end
      end
    end
  end
end
