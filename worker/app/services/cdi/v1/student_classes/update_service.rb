# encoding: UTF-8

module CDI
  module V1
    module StudentClasses
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::StudentClassParams

        record_type ::StudentClass

        def record_params
          student_class_params
        end

      end
    end
  end
end
