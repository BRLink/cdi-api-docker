module CDI
  module V1
    module StudentClasses
      class StatusChangeService < BaseUpdateService

        record_type ::StudentClass

        private
        def record_params
          @_params ||= @record.slice(:status, :status_changed_at, :code_expiration_date)
        end

        def user_can_update?
          unless valid_status?
            return forbidden_error!("#{record_error_key}.invalid_status")
          end

          super
        end

        def update_record
          @record.send(migrate_method)
          @record
        end

        def changes(old, current, attributes = {})
          if record_params[:status] != current.status
            return super.concat(record_params.keys)
          end

          super(old, current, attributes)
        end

        def migrate_method
          "migrate_to_#{new_status}"
        end

        def new_status
          @options[:new_status]
        end

        def valid_status?
          new_status.present? && StudentClass::STATUS.values.member?(new_status.to_s)
        end

      end
    end
  end
end
