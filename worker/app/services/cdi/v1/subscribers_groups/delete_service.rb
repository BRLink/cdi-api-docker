# encoding: UTF-8

module CDI
  module V1
    module SubscribersGroups
      class DeleteService < BaseDeleteService

        record_type ::SubscribersGroup
      end
    end
  end
end
