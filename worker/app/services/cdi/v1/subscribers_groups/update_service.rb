# encoding: UTF-8

module CDI
  module V1
    module SubscribersGroups
      class UpdateService < BaseUpdateService

        record_type ::SubscribersGroup

        def record_params
          filter_hash(@options[:subscribers_group], [:name])
        end
      end
    end
  end
end
