module CDI
  module V1
    module LearningCycles
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::LearningCycleParams

        record_type ::LearningCycle

        def can_execute_action?
          can_execute = super
          if can_execute
            if need_update_categories? && will_remove_used_categories?
              return forbidden_error!("#{record_error_key}.cant_remove_categories_in_use")
            end
          end

          can_execute
        end

        def record_params
          learning_cycle_params
        end

        def need_update_categories?
          return false if learning_cycle_params[:category_ids].blank?

          @categories_for_remove = @record.category_ids - learning_cycle_params[:category_ids]
          @categories_for_remove.any?
        end

        def will_remove_used_categories?
          @categories_in_use = CategoryResource.where(
              resource_type: 'LearningTrack',
              resource_id: @record.tracks.pluck(:id),
              category_id: @categories_for_remove
            )
            .pluck(:category_id)
            .uniq

          @categories_in_use.any?
        end

        def categories_in_use
          @categories_in_use
        end
      end

    end
  end
end
