# encoding: UTF-8

module CDI
  module V1
    module UsersPreferences
      class UpdateService < BaseUpdateService

        record_type ::UserPreference

        def record_params
          filter_hash(@options[:preference], [:title, :author, :link])
        end
      end
    end
  end
end
