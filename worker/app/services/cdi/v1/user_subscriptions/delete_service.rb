# encoding: UTF-8

module CDI
  module V1
    module UserSubscriptions
      class DeleteService < BaseDeleteService

        record_type ::UserSubscription, alias_name: :subscription

        def initialize(resource, user, options={})
          subscription = user.subscriptions.find_by(publisher: resource)
          super subscription, user, options
        end
      end
    end
  end
end
