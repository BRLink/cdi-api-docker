# encoding: UTF-8

module CDI
  module V1
    module UserSubscriptions
      class CreateService < BaseCreateService

        record_type ::UserSubscription, alias_name: :subscription

        def initialize(resource, user, options={})
          super user, options
          @resource = resource
        end

        def can_create_record?
          if @resource.nil?
            return not_found_error!("user_subscriptions.resource_not_found")
          end

          if @resource.followed_by? @user
            return forbidden_error!("user_subscriptions.already_following")
          end

          true
        end

        def build_record
          @user.subscriptions.build(publisher: @resource)
        end

        def after_success
          copy_discussions_to_subscriber_async
        end

        def copy_discussions_to_subscriber_async
          service = Discussions::CopyDiscussionsToSubscriberService.new(@record)
          service.execute
        end
      end
    end
  end
end
