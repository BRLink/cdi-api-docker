# encoding: UTF-8
class CyclePlanning < ActiveRecord::Base

  include Accessable
  include Historiable
  include TemplateQuestionable

  belongs_to :learning_cycle

  has_one :user, through: :learning_cycle

end
