class Discussion < ActiveRecord::Base

  include Accessable
  include Attachable
  include ResourceVisibility
  include Commentable
  include Likeable
  include Categorizable
  include Taggable
  include Featurable
  include Searchable
  include AchievementConcerns::DiscussionAchievementable
  include BadgeConcerns::DiscussionBadgeable

  VALID_DISCUSSION_TYPES = ['simple', 'poll']
  MAX_OPTIONS_BY_DISCUSSION = 10
  VALID_PUBLISHER_TYPES = ['User', 'Challenge']

  belongs_to :user
  belongs_to :publisher, polymorphic: true
  belongs_to :best_comment, class_name: 'Comment'

  has_many :options, class_name: 'DiscussionOption'
  has_many :answers, class_name: 'DiscussionAnswer'
  has_many :subscriber_discussions
  has_many :photos, through: :attachments, source: :attachment, source_type: 'Photo'
  has_many :views, class_name: 'DiscussionView'

  searchable_for :title, :content, :tags, :categories_names, if: ->(discussion) { discussion.visible_to_all? }

  validates :user, :title, :publisher, presence: true

  validates :discussion_type, presence: true, inclusion: VALID_DISCUSSION_TYPES

  validates :publisher_type, inclusion: VALID_PUBLISHER_TYPES

  validates :video_link, presence: true, format: { with: URI.regexp }, if: -> { video_link.present? }

  scope :only_user, ->(user) {
    where(user: user)
  }

  def image_url
    photos.with_deleted.first.image.url if photos.with_deleted.exists?
  end

  def cover_url
    _cover ||= Photo.with_deleted.find_by(id: cover_id)
    return _cover.image.url if _cover.present?
  end

  def poll?
    discussion_type == 'poll'
  end

  # @override from commentable
  def nested_comments
    if best_comment_id.present?
      comments.where("parent_id IS NULL AND id != ?", best_comment_id)
    else
      super
    end
  end
end
