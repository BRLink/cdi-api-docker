class UserBadge < ActiveRecord::Base

  belongs_to :user
  belongs_to :badge

  validates :user_id, :badge_id, presence: true
  validate  :valid_user_type

  scope :history_for, -> (user) {
    where(user_id: user.id).includes(:badge).order(created_at: :desc)
  }

  def valid_user_type
    unless badge.user_type.any? { |type| type == "all" or user.send(:"#{type}?") }
      errors.add(:user, "#{user.profile_type} cannot receive badge #{badge.action}")
    end
  end
end
