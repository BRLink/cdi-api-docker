class DiscussionView < ActiveRecord::Base

  belongs_to :user
  belongs_to :discussion, counter_cache: :views_count

  validates :user, :discussion, presence: true
  validates :discussion_id, uniqueness: {scope: [:user_id]}
end
