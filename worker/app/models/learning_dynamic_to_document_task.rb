# encoding: UTF-8
require "net/http"
require "uri"
require "json"

class LearningDynamicToDocumentTask < ActiveRecord::Base

  STATUS = {
    pending: 0,
    downloading_slides: 35,
    generating_pdf: 50,
    uploading_pdf: 70,
    generating_ppt: 80,
    uploading_ppt: 90,
    done: 100,
    unknown_error: -1,
    downloading_slides_fail: -10,
    generating_pdf_fail: -20,
    uploading_pdf_fail: -30,
    invalid_request: -200
  }

  MAX_RETRY = 6

  BASE_URL = "http://%s.s3.amazonaws.com/uploads/%s/learning_dynamics/%s/%s.pdf"

  enum status: STATUS

  belongs_to :learning_dynamic

  validates :learning_dynamic, presence: true

  validate :validate_slides_have_thumbs

  before_validation :update_slides

  def request
    if self['status'] < 0
      was_failed
    elsif self['status'] < 100
      was_running
    else
      was_completed
    end
  end

  # statuses
  def run_new_task
    assign_task(force: true)
  end

  def was_running
    # currently ignore this situation for performance:
    # a user request for pdf and the owner update the track in the middle.
    get_status
  end

  def was_failed
    assign_task if retries < MAX_RETRY
  end

  def was_completed
    assign_task if check_presentation_dirty
  end

  # actions
  def check_presentation_dirty
    update_slides
    self.ins_changed?
  end

  def update_slides
    self.ins = slides.map{ |item| item.metadata['thumb_url'] }
  end

  def slides
    learning_dynamic.multiplier_slides + learning_dynamic.student_slides
  end

  def out
    @out ||= BASE_URL % [CDI::Config.aws_bucket_name, Rails.env, learning_dynamic.id, file_name]
  end

  alias :public_link :out

  def file_name
    return @file_name if @file_name.present?
    # title can be blank on create-->
    if learning_dynamic.title.present?
      @file_name = plain_string(learning_dynamic.title)
    else
      @file_name = 'index'
    end
    return @file_name
  end

  def plain_string(str)
    # this method should make sure that the string can be used as file name.
    str.parameterize
  end

  # http requests
  def assign_task(options = {})
    url = "#{CDI::Config.images_to_pdf_url}assign"
    uri = URI.parse(url)

    request_parameters = {
      token: CDI::Config.converter_token,
      out: out,
      ins: JSON.generate(ins)
    }.merge(options)

    reply = Net::HTTP.post_form(uri, request_parameters)  # if is down?

    body = JSON.parse(reply.body) # if it's not a valid json? (if server is down)

    save_and_return_status(body)

  end

  def get_status
    url = "#{CDI::Config.images_to_pdf_url}status"
    uri = URI.parse(url)
    uri.query = URI.encode_www_form(token: CDI::Config.converter_token, out: out)

    reply = Net::HTTP.get_response(uri) # if is down?
    body = JSON.parse(reply.body)

    save_and_return_status(body)
  end

  private
  def save_and_return_status(request_body)
    self['status'] = ((request_body && request_body['indicator']) || -200).to_i
    self.save
    self.status
  end

  def validate_slides_have_thumbs
    unless self.ins.any?
      errors.add(:slides, "have slides without thumb_url")
    end
  end
end
