class EnrolledLearningTrack < ActiveRecord::Base

  include Accessable
  include Statusable
  include AchievementConcerns::EnrolledLearningTrackAchievementable
  include BadgeConcerns::EnrolledLearningTrackBadgeable

  # TODO: Enum
  STATUS = {
    :not_enrolled => 'not_enrolled',
    :enrolled     => 'enrolled',
    :coursing     => 'coursing',
    :completed    => 'completed'
  }

  scope :real_enrolled, -> { where.not(status: STATUS[:not_enrolled] )}

  scope :pending, -> { where.not(status: STATUS[:completed] ) }

  status_configure default_status: :enrolled,
                   status_hash: STATUS,
                   validate_status_inclusion: false


  validates :status, inclusion: STATUS.values

  has_one :origin, as: :originable

  belongs_to :user

  alias :student :user

  belongs_to :learning_track

  alias :track :learning_track

  belongs_to :student_class

  has_many :tracking_events

  has_many :slides, through: :tracking_events, source: :presentation_slide

  alias :events :tracking_events

  validates :user, :learning_track, :student_class, presence: true

  # unique constraint in database
  # validates :learning_track, uniqueness: { scope: [:user_id] }

  def watched_slides_ids_for_track
    @watched_slide_ids ||= self.events.pluck('DISTINCT presentation_slide_id')
  end

  def not_watched_slides_ids_for_track
    @not_watched_slide_ids ||= not_watched_slides_for_track.map(&:id)
  end

  def played_slides_ids_for_track
    @played_slide_ids ||= played_slides_for_track.map(&:id)
  end

  def not_played_slides_ids_for_track
    @not_played_slide_ids ||= not_played_slides_for_track.map(&:id)
  end

  def watched_slides_for_track
    @watched_slides ||= self.slides
  end

  def update_progress
    percent_base = 2

    total_slides = learning_track.visible_slides.count
    total_watched_slides = watched_slides_ids_for_track.size

    watched_percent = total_slides.zero? ? 0 : (total_watched_slides * 100) / total_slides

    total_game_slides = learning_track.visible_slides.game.count
    total_played_slides = played_slides_ids_for_track.size

    played_percent = total_game_slides.zero? ? 100 : (total_played_slides * 100) / total_game_slides

    if track.has_learning_dynamic?
      student_delivery = track.student_deliveries.find_by(student_class_id: student_class_id, user_id: user_id)
      delivery_percent = student_delivery.present? ? 100 : 0
      percent_base = 3
    end

    total = (((watched_percent || 0) + (played_percent || 0) + (delivery_percent || 0)) * 100)

    total_percent = (total / (percent_base * 100)).round(2)

    self.update_attribute(:progress, total_percent)
  end

  def update_progress_async
    ::CDI::V1::TrackEnrollProgressUpdateWorker.perform_async(self.id)
  end

  def played_slides_for_track
    @played_slides ||= self.slides
                       .where(tracking_events: {
                          event_type: ::TrackingEvent::EVENT_TYPES[:played]
                        }) # only game play events
                       .game # scope on slide class
  end

  def not_watched_slides_for_track
    @not_watched_slides ||= watched_slides_ids_for_track.any? ?
                            learning_track.slides.where('presentation_slides.id NOT IN (?)', watched_slides_ids_for_track) :
                            learning_track.slides
  end

  def not_played_slides_for_track
    @played_slide_ids ||= self.events.played.pluck(:presentation_slide_id)

    @not_played_slides ||= @played_slide_ids.any? ?
                            learning_track.slides.game
                            .where('presentation_slides.id NOT IN (?)', @played_slide_ids) :
                            # all games
                            learning_track.slides.game
  end

  def not_watched_slides_for_track_count
    not_watched_slides_for_track.try(:size)
  end

end
