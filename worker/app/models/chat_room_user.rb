# encoding: UTF-8

class ChatRoomUser < ActiveRecord::Base

  ROLES = {
    admin: 100,
    member: 0
  }
  enum role: ROLES

  belongs_to :user
  belongs_to :chat_room

  validates :user, :chat_room, presence: true
  validates :user_id, uniqueness: {scope: [:chat_room_id]}, on: :create

end
