class GameGroup < ActiveRecord::Base

  include Accessable
  include Historiable

  amoeba do
    exclude_association [:origin, :answers, :edition_histories]
  end

  attr_accessor :skip_father_game_validation

  PARAMETERIZE_WORDS = true

  MAX_ITEMS = 10

  has_one :origin, as: :originable

  belongs_to :game_group_item

  has_many :answers, as: :game_wordable, class_name: 'GameWordAnswer'

  validates :game_group_item, presence: true, unless: -> {
    skip_father_game_validation.present?
  }

  validates :title, :items, presence: true

  validates :title, uniqueness: { scope: [:game_group_item_id] }, unless: -> {
    skip_father_game_validation.present?
  }

  validate :validate_min_items_data

  validate :item_dont_exist_in_other_group, unless: -> {
    skip_father_game_validation.present?
  }

  delegate :user_id, to: :game_group_item, allow_nil: true

  before_save :set_items

  def words
    items
  end

  def parent_groups
    game_group_item.groups.where('id != ?', self.id)
  end

  def correct?(items)
    group_items = self.items.map(&:to_s)
    correct_items = correct_items(items)

    return false if group_items.size != correct_items.size
    return group_items.map(&:downcase).sort == correct_items.map(&:downcase).sort
  end

  def correct_items(items)
    self.items.map(&:to_s) & items.map(&:to_s)
  end

  def wrong_items(items)
    self.items.map(&:to_s) - items.map(&:to_s)
  end

  def correct_percent_for_user(user)
    answer = self.answers.find_by(user_id: user.id)

    return 0 if answer.blank?

    ((correct_items(answer.words).size * 100) / self.items.size).round(2)
  end

  alias :correct_words_by_position :correct_items
  alias :wrong_words_by_position :wrong_items

  private
  def set_items
    self.items = self.items[0, MAX_ITEMS] if self.items
  end

  def validate_min_items_data
    self.items ||= []

    unless self.items.any?
      self.errors.add(:items, I18n.t('cdi.errors.game_groups.suply_at_least_one_item'))
    end
  end

  def item_dont_exist_in_other_group
    items = self.parent_groups.map {|g| { g.id => g.items } }
    # force conversion to checking below
    flatten_items = items.map(&:values).flatten.map(&:to_s)

    duplicated_items = self.items.select {|item| flatten_items.member?(item.to_s) }

    if duplicated_items.any?
      self.errors.add(:items, "'#{duplicated_items.to_sentence}' yet exists for another group")
    end
  end

end
