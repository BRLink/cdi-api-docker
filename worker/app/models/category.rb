# encoding: UTF-8

class Category < ActiveRecord::Base

  include Positionable
  include Historiable

  validates :name, presence: true, uniqueness: true

  has_many :category_resources

  has_many :learning_cycles

  alias :cycles :learning_cycles
  alias :cycles= :learning_cycles=

  has_many :multimedia
  has_many :supporting_courses

  has_and_belongs_to_many :posts

  acts_as_list :category_resources

  private
  def custom_data_for_histories
    self.changes.symbolize_keys.slice(:name, :position)
  end
end
