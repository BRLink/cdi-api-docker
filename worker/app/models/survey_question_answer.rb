class SurveyQuestionAnswer < ActiveRecord::Base

  include Accessable
  include AchievementConcerns::SurveyQuestionAnswerAchievementable

  belongs_to :user
  belongs_to :survey_question, counter_cache: :answers_count
  belongs_to :option, class_name: 'SurveyQuestionOption', counter_cache: :answers_count, dependent: :destroy

  has_many :question_options, through: :survey_question, source: :options

  validates :survey_question, :user, presence: true

  validates :answer_body, presence: true, if: :open_question_answer?

  validate :multiples_answers_for_question
  validate :option_belongs_to_question

  def open_question_answer?
    answer_body.present?
  end

  def user_can_update?(user)
    return false unless survey_question.try(:questionable_type) == 'SurveyOpenQuestion'
    user.id == survey_question.try(:user_id) || super(user)
  end

  def multiples_answers_for_question
    if survey_question.answers.exists?(user_id: self.user_id)
      self.errors.add(:base, I18n.t('cdi.errors.survey_questions.cant_answer_question_twice'))
    end
  end

  def option_belongs_to_question
    return self if open_question_answer?
    unless question_options.exists?(id: self.option_id)
      self.errors.add(:option_id, I18n.t('cdi.errors.survey_questions.option_not_found_in_question'))
    end
  end

  def survey
    survey_question.questionable
  end
end
