class UserSubscription < ActiveRecord::Base

  include Accessable

  VALID_PUBLISHER_TYPES = {
    :users => 'User',
    :institutions => 'EducationalInstitution'
  }

  belongs_to :user
  belongs_to :publisher, polymorphic: true

  has_many :subscriber_posts
  has_many :subscriber_surveys
  has_many :subscriber_discussions
  has_many :subscribers_group_members
  has_many :user_subscribers_groups, through: :subscribers_group_members

  validates :publisher_type, presence: true, inclusion: VALID_PUBLISHER_TYPES.values
  validates :user_id, uniqueness: { scope: [:publisher_type, :publisher_id] }

  delegate :name, to: :publisher, prefix: 'publisher'

  def self.publisher_types
    VALID_PUBLISHER_TYPES
  end

  def user_can_access?(user)
    self.user_id == user.id
  end
end
