# encoding: UTF-8
class QuestionTemplate < ActiveRecord::Base

  include Historiable

  QUESTIONS_TYPES = {
    :open => 'open',
    :single_choice => 'single_choice',
    :multiple_choices => 'multiple_choices',
    :comma_separated_open => 'comma_separated_open'
  }

  QUESTIONS_TYPES.each do |key, value|
    scope key, -> { where(question_type: value) }

    define_method "question_#{key}?" do
      self.question_type == value
    end
  end

  # cant use 'type' as column name
  validates :question_type, presence: true, inclusion: QUESTIONS_TYPES.values
  validates :title, :question_for, presence: true

  # Allow only ONE UNIQUE question per type and class
  validates :title, uniqueness: { scope: [:question_for, :question_type] }

  has_many :questions, class_name: 'Question', foreign_key: :question_template_id

  alias :children_questions :questions

  scope :questions_for, -> (question_for) {
    where(question_for: question_for)
  }

  before_validation :clear_choices

  def with_choice?
    QUESTIONS_TYPES.slice(:single_choice, :multiple_choices).values.member?(self.question_type.to_s)
  end

  private

  def clear_choices
    unless with_choice?
      self.choices = nil
    end
  end
end
