class LearningDynamic < ActiveRecord::Base

  include Accessable
  include Statusable
  include Taggable
  include Categorizable
  include Searchable
  include LibrarySearchable

  # Suporte a busca global
  searchable_for :title, :tags, :categories_names, if: ->(dynamic) { dynamic.status_published? }

  # Suporte a busca interna
  search_extra_fields :title

  amoeba do
    nullify [:user_id, :status, :status_changed_at]
    include_association [:skills, :learning_delivery]
    through :become_child
  end

  acts_as_tree order: :id

  has_and_belongs_to_many :skills

  has_one :learning_delivery

  alias :delivery :learning_delivery
  alias :delivery= :learning_delivery=

  belongs_to :user

  belongs_to :student_presentation, class_name: 'Presentation', foreign_key: :student_presentation_id
  belongs_to :multiplier_presentation, class_name: 'Presentation', foreign_key: :multiplier_presentation_id

  has_many :student_slides, through: :student_presentation, source: :presentation_slides
  has_many :multiplier_slides, through: :multiplier_presentation, source: :presentation_slides

  has_one :origin, as: :originable
  has_one :learning_dynamic_to_document_task

  after_create :create_presentations

  before_save :set_perspectives, :set_skills

  validates :title, presence: true, on: [:update]
  validates :user, presence: true

  MAX_SKILLS_COUNT = 5

  PERSPECTIVES = I18n.t('app.perspectives_texts')

  def clone! owner_or_id, options={}
    duped_object = self.amoeba_dup
    duped_object.user_id = owner_or_id.respond_to?(:id) ? owner_or_id.id : owner_or_id
    duped_object.save

    clone_presentation_for(duped_object, owner_or_id)
    clone_presentation_for(duped_object, owner_or_id, :multiplier)

    duped_object
  end

  alias :duplicate :clone!

  def perspectives_texts
    self.perspectives ||= []
    self.perspectives.map {|p|
      {
        p => PERSPECTIVES[p.to_i]
      }
    }
  end

  def become_child
    duped_object = self.dup
    duped_object.parent_id = self.id

    duped_object
  end

  def thumbnail_url(presentation_type = :student)
    self.send("#{presentation_type}_slides").first.try(:thumbnail_url)
  end

  [:student, :multiplier].each do |presentation_type|
    define_method "#{presentation_type}_thumbnail_url" do
      thumbnail_url(presentation_type)
    end
  end


  private
  def clone_presentation_for(cloned_object, owner, type = :student, validate = false)
    presentation_method = "#{type}_presentation"
    presentation = self.send(presentation_method)

    if presentation.present?
      cloned_presentation = cloned_object.send("#{presentation_method}=", presentation.clone!(owner))

      if cloned_presentation.errors.any?
        cloned_presentation.errors.each do |name, detail|
          cloned_object.errors.add("#{type}_presentation_#{name}".to_sym, detail)
        end
      end
    end
  end

  def create_presentations
    [:student_presentation, :multiplier_presentation].each do |presentation|
      return if self.send("#{presentation}").present?
      self.send("create_#{presentation}",
        user_id: self.user_id,
        presentation_type: Presentation::PRESENTATION_TYPES[:dynamic_content]
      )
      self.save
    end
  end

  def set_perspectives
    self.perspectives ||= []
    self.perspectives = self.perspectives.map(&:to_i) & PERSPECTIVES.keys.map(&:to_i)
  end

  def set_skills
    self.skill_ids = self.skill_ids[0, MAX_SKILLS_COUNT] if self.skill_ids.any?
  end

  def max_categories
    4
  end
end
