class GameQuestion < ActiveRecord::Base

  include Accessable
  include Positionable
  include Commentable

  amoeba do
    exclude_association [:answers, :comments, :likes, :options]

    # include_association [:options]
  end

  VALID_INPUT_TYPES = {
    :text        => "text",
    :radio       => "radio",
    :checkbox    => "checkboxes",
    :select      => "select",
    :multiselect => "multiselect"
  }

  scope :quiz, -> { where(questionable_type: 'GameQuiz') }
  scope :poll, -> { where(questionable_type: 'GamePoll') }
  scope :open_question, -> { where(questionable_type: 'GameOpenQuestion') }

  # TODO: Fix this
  acts_as_list scope: :questionable

  has_one :origin, as: :originable

  belongs_to :questionable, polymorphic: true

  has_many :answers, foreign_key: :game_question_id, class_name: 'GameQuestionAnswer'
  has_many :users_answers, through: :answers, source: :user

  has_many :options, foreign_key: :game_question_id, class_name: 'GameQuestionOption'

  validates :questionable_id, :questionable_type,
            :input_type, :title, presence: true

  validates :input_type, inclusion: VALID_INPUT_TYPES.values

  validates :title, uniqueness: { scope: [:questionable_type, :questionable_id] }

  validate :validate_game_reached_questions_limit, on: [:create]

  delegate :user_id, to: :questionable, allow_nil: true

  after_initialize :set_defaults

  COMMENTABLE_GAMES = [
    'GameOpenQuestion'.freeze
  ]

  def options_answers_percent(ndigits = 2)

    # load count options
    subquery = "SELECT (COUNT(option_id) * 100)::numeric"

    # total answers for question
    subquery2 = GameQuestionAnswer.where(game_question_id: id).select("COUNT(*)").to_sql

    GameQuestionAnswer
      .where(game_question_id: id)
      .group("option_id")
      .order("answers_count DESC")
      .select([
        # Total Answers Count
        "COUNT(option_id) as answers_count",
        "round( (#{subquery}) / (#{subquery2}), #{ndigits}) as total_percent",
        "option_id"
      ].join(", ")).to_a
  end

  def game
    questionable
  end

  def user_can_create_comment?(user)
    return false unless COMMENTABLE_GAMES.member?(self.questionable_type)
    return user_can_manage?(user)
  end

  private
  def validate_game_reached_questions_limit
    if questionable
      if questionable.is_a?(GamePoll)
        max_questions = GamePoll::MAX_QUESTIONS_BY_GAME
        current_questions_count = questionable.question ? 1 : 0
      elsif questionable.is_a?(GameOpenQuestion)
        max_questions = GameOpenQuestion::MAX_QUESTIONS_BY_GAME
        current_questions_count = questionable.questions.count
      elsif questionable.is_a?(GameTrueFalse)
        max_questions = GameTrueFalse::MAX_QUESTIONS_BY_GAME
        current_questions_count = questionable.questions.count
      elsif questionable.is_a?(GameQuiz)
        max_questions = GameQuiz::MAX_QUESTIONS_BY_GAME
        current_questions_count = questionable.questions.count
      end
      if current_questions_count >= max_questions
        self.errors.add(:base, I18n.t('cdi.errors.games.max_questions_for_game_reached'))
      end
    end
  end

  def set_defaults
    self.input_type ||= VALID_INPUT_TYPES[:text]
  end

  def set_position_before_save?
    false
  end
end
