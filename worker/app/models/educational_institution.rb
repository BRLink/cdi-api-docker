# encoding: UTF-8
class EducationalInstitution < ActiveRecord::Base

  include Accessable
  include Historiable
  include Usernamed
  include ProfileImageable
  include ProfileCoverImageable
  include PubsubConcerns::Publisher
  include ResourceVisibilitable
  include Searchable

  attr_accessor :relation_with_me, :can_edit

  VALID_CONTACTS = [:site, :email, :telephone]

  store_accessor :contacts, *VALID_CONTACTS

  searchable_for :name

  validates :name, presence: true
  validates :username, uniqueness: true, on: :create
  validates :admin_user, presence: true


  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }, if: -> {
    self.email.present?
  }
  validates :site, format: URI::regexp(%w(http https)),
            allow_blank: true


  validate :admin_user_must_be_staff, on: :create

  after_save :set_permanent_member

  belongs_to :admin_user, class_name: 'User'

  has_many :student_classes

  has_many :learning_tracks, through: :student_classes

  has_many :students, through: :student_classes, source: :users

  #accepted(default) scope for institution member
  has_many :institution_users, ->{ accepted }
  has_many :users, through: :institution_users

  has_many :pending_institution_users, -> { pending }, class_name: 'InstitutionUser'
  has_many :pending_users, through: :pending_institution_users, source: :user

  has_many :rejected_institution_users, -> { rejected }, class_name: 'InstitutionUser'
  has_many :rejected_users, through: :rejected_institution_users, source: :user

  has_many :all_institution_users, -> { all }, class_name: 'InstitutionUser'
  has_many :all_users, through: :all_institution_users, source: :user

  has_one :origin, as: :originable

  has_one :address, as: :addressable

  has_one :city, through: :address

  has_one :state, through: :address

  delegate :city_id, :state_id, to: :address, allow_nil: true

  has_many :institution_managers, foreign_key: :managed_institution_id
  has_many :managers, through: :institution_managers, class_name: 'User'

  # access control
  def user_can_update?(user)
    all_managers.index(user).present?
  end

  def user_can_delete?(user)
    user == admin_user
  end

  def user_can_view_post? u
    admin_user_id == u.id || managers.exists?(id: u.id) || users.exists?(id: u.id)
  end

  def all_managers
    [admin_user] + managers
  end

  private

  def set_permanent_member
    if admin_user.present? and not users.include?(admin_user)
      self.users << admin_user
    end
  end

  def custom_data_for_histories
    self.changes.symbolize_keys.slice(:name, :username, :about, :admin_user_id)
  end

  def admin_user_must_be_staff
    if self.admin_user && !self.admin_user.backoffice_profile?
      self.errors.add(:admin_user_id, 'must be a staff/admin user')
    end
  end
end
