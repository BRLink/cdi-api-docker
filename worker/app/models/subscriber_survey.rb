class SubscriberSurvey < ActiveRecord::Base

  VALID_SURVEY_TYPES = [
    'SurveyPoll',
    'SurveyOpenQuestion'
  ]

  belongs_to :user_subscription
  belongs_to :survey, polymorphic: true

  validates :user_subscription, :survey, presence: true
  validates :user_subscription_id, uniqueness: { scope: [:survey_type, :survey_id] }
  validates :survey_type, inclusion: VALID_SURVEY_TYPES

  delegate :title, :image, :video_link, :questions, :question, :user, :visibilities, to: :survey

  default_scope { order('created_at DESC') }

  scope :polls, ->{ where(survey_type: 'SurveyPoll') }
  scope :open_questions, ->{ where(survey_type: 'SurveyOpenQuestion') }

end
