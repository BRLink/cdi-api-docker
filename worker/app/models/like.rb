class Like < ActiveRecord::Base

  include Accessable
  include AchievementConcerns::LikeAchievementable

  VALID_RESOURCE_TYPES = {
    :tracks => 'LearningTrack',
    :posts => 'Post',
    :comments => 'Comment',
    :photos => 'Photo',
    :discussions => 'Discussion',
    :blog_articles => 'BlogArticle',
    :challenges => 'Challenge',
    :challenge_deliveries => 'ChallengeDelivery',
    :survey_questions => 'SurveyQuestion'
  }

  belongs_to :user
  belongs_to :resource, polymorphic: true

  validates :resource_type, presence: true, inclusion: VALID_RESOURCE_TYPES.values
  validates :user_id, uniqueness: { scope: [:resource_type, :resource_id] }

  def self.resource_types
    VALID_RESOURCE_TYPES
  end

  def user_can_manage?(user)
    # TODO: Admins can edit/delete others users likes?
    self.user_id == user.id
  end
end
