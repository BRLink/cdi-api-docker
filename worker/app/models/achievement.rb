class Achievement < ActiveRecord::Base

  NOTIFIABLE_ACHIEVEMENTS = [
    :user_post_selected_by_institution,
    :become_part_of_institution,
    :have_answer_selected_as_best_answer,
    :receive_like_on_delivery,
    :receive_share_on_delivery,
    :receive_like_on_post,
    :receive_comment_on_post,
    :receive_share_on_post,
    :receive_like_on_project,
    :receive_comment_on_project,
    :receive_share_on_project,
    :become_co_author,
    :receive_like_on_learning_track,
    :receive_comment_on_learning_track,
    :student_takes_learning_track,
    :learning_track_cloned_by_multiplier,
    :learning_track_recommended,
    :receive_like_on_library_content,
    :receive_comment_on_library_content,
    :receive_share_on_library_content,
    :have_library_content_cloned,
    :mentor_recommended
  ]

  has_many :user_achievements, dependent: :destroy
  has_many :users, through: :user_achievements

  has_many :points, dependent: :destroy

  alias_method :custom_scores, :points

  validates :action, :area, :default_score, presence: true
  validate  :valid_user_type

  def valid_user_type
    unless user_type.all? { |type| ["all", "multiplier"].include? type or User::VALID_PROFILES_TYPES.values.include? type }
      errors.add(:user_type, "Invalid user types: #{user_type}")
    end
  end

  def name
    I18n.t :name, scope: "cdi.achievement.#{action}"
  end

  def notifiable?
    NOTIFIABLE_ACHIEVEMENTS.include? action.to_sym
  end
end
