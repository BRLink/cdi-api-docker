# encoding: UTF-8
require "net/http"
require "uri"
require "json"

class PresentationToDocumentTask < ActiveRecord::Base

  STATUS = {
    pending: 0,
    downloading_slides: 35,
    generating_pdf: 50,
    uploading_pdf: 70,
    generating_ppt: 80,
    uploading_ppt: 90,
    done: 100,
    unknown_error: -1,
    downloading_slides_fail: -10,
    generating_pdf_fail: -20,
    uploading_pdf_fail: -30,
    invalid_request: -200
  }

  BASE_URL = "http://%s.s3.amazonaws.com/uploads/%s/presentations/%s/%s.pdf"

  MAX_RETRY = 6

  enum status: STATUS

  belongs_to :presentation

  validates :presentation, presence: true

  before_validation :update_slides
  validate :validate_have_slides_that_have_thumbs

  def request
    if self['status'] < 0
      was_failed
    elsif self['status'] < 100
      was_running
    else
      was_completed
    end
  end

  # statuses
  def run_new_task
    assign_task(force: true)
  end

  def was_running
    # currently ignore this situation for performance:
    # a user request for pdf and the owner update the track in the middle.
    get_status
  end

  def was_failed
    assign_task if retries < MAX_RETRY
  end

  def was_completed
    assign_task if check_presentation_dirty
  end

  # actions
  def check_presentation_dirty
    update_slides
    self.ins_changed?
  end

  def update_slides
    all_thumbnails = []
    presentation.slides.each do |item|
      if item.associated_resource_type.to_s == 'SupportingCourse'
        all_thumbnails += supporting_course_thumbnails(item.associated_resource)
      elsif item.associated_resource_type.to_s == 'LearningDynamic'
        all_thumbnails += learning_dynamic_thumbnails(item.associated_resource)
      else
        thumbnail = complete_url(item.try(:thumbnail_url))
        if thumbnail.present?
          all_thumbnails.push(thumbnail)
        end
      end
    end
    self.ins = all_thumbnails.select(&:present?)
  end

  def supporting_course_thumbnails(supporting_course)
    supporting_course.slides.map do |item|
      complete_url(item.try(:thumbnail_url))
    end
  end

  def learning_dynamic_thumbnails(learning_dynamic)
    thumb_nails = learning_dynamic.student_slides.map do |item|
      complete_url(item.try(:thumbnail_url))
    end
    thumb_nails += learning_dynamic.multiplier_slides.map do |item|
      complete_url(item.try(:thumbnail_url))
    end
    return thumb_nails
  end

  def complete_url url
    return nil unless url.present?
    url.start_with?('http') ? url :
        "http://#{CDI::Config.aws_bucket_name}.s3.amazonaws.com/#{url}"
  end

  def out
    @out ||= BASE_URL % [CDI::Config.aws_bucket_name, Rails.env, presentation.id, file_name]
  end

  alias :public_link :out

  def file_name
    return @file_name if @file_name.present?
    if presentation.learning_track.present?
      @file_name = plain_string(presentation.learning_track.name)
    else
      @file_name = 'index'
    end
    return @file_name
  end

  def plain_string(str)
    # this method should make sure that the string can be used as file name.
    str.parameterize
  end

  # http requests
  def assign_task(options = {})
    url = "#{CDI::Config.images_to_pdf_url}assign"
    uri = URI.parse(url)

    request_parameters = {
      token: CDI::Config.converter_token,
      out: out,
      ins: JSON.generate(ins)
    }.merge(options)

    reply = Net::HTTP.post_form(uri, request_parameters)
    body = JSON.parse(reply.body)

    save_and_return_status(body)
  end

  def get_status
    url = "#{CDI::Config.images_to_pdf_url}status"
    token = CDI::Config.converter_token
    uri = URI.parse(url)
    uri.query = URI.encode_www_form({token: token, out: out})

    reply = Net::HTTP.get_response(uri)
    body = JSON.parse(reply.body)

    save_and_return_status(body)
  end

  private

  def save_and_return_status(request_body)
    self['status'] = request_body['indicator'].present? ? request_body['indicator'].to_i : -200
    self.save
    self.status
  end

  def validate_have_slides_that_have_thumbs
    if self.ins.size == 0
      errors.add(:slides, "no slides have thumb_url")
    end
  end

end
