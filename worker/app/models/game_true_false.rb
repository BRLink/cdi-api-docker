class GameTrueFalse < ActiveRecord::Base

  include Accessable
  include Historiable
  include Statusable
  include GameScorable
  include GameQuestionableClonable

  amoeba do
    nullify [:user_id, :status, :status_changed_at]

    exclude_association [:origin, :edition_histories, :answers, :user_scores, :questions]

    # include_association [:questions]
  end

  MAX_QUESTIONS_BY_GAME = (CDI::Config.game_true_false_max_questions_limit || 10).to_i

  QUESTION_OPTIONS = {
    false => {
      :text => I18n.t('app.games.true_false.false_text')
    },
    true => {
      :text => I18n.t('app.games.true_false.true_text')
    }
  }

  belongs_to :user

  has_one :origin, as: :originable

  has_many :questions, as: :questionable, class_name: 'GameQuestion'
  has_many :options, through: :questions
  has_many :answers, through: :questions

  validates :user_id, presence: true

  after_initialize :set_defaults

  alias :owner_user :user
  alias :owner_user= :user=

  private
  def custom_data_for_histories
    self.changes.symbolize_keys.slice(:user_id, :status, :status_changed_at)
  end

  def set_defaults
  end
end
