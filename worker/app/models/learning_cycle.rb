class LearningCycle < ActiveRecord::Base

  include Accessable
  include Statusable
  include Taggable
  include Categorizable
  include AchievementConcerns::LearningCycleAchievementable
  include FullTextSearchable

  belongs_to :user

  has_one :origin, as: :originable

  has_one :cycle_planning

  alias :planning :cycle_planning
  alias :planning= :cycle_planning=

  has_many :learning_tracks
  has_many :tracks_categories, through: :learning_tracks, source: :categories

  has_many :published_learning_tracks, -> { published }, class_name: 'LearningTrack'

  has_many :student_class_published_tracks, -> {
    published_in_classes
  }, class_name: 'LearningTrack'

  has_many :student_classes, through: :learning_tracks

  # Alias
  alias :tracks :learning_tracks

  validates :name, :user_id, :objective_text, presence: true

  scope :learning_track_search, -> (content) {
    joins(learning_tracks: [:search_document])
    .search(content, use_joins: false)
  }

  def user_can_read?(user)
    return true if user.backoffice_profile?

    super || user.student? && (user.classes_cycles.exists?(id: self.id))
  end

  def user_can_delete?(user)
    return false if self.learning_tracks.size > 0
    super(user)
  end

  def refresh_status(save_now = true)
    self.status = published_learning_tracks.any? ? LearningCycle.status_map[:published] : LearningCycle.status_map[:draft]
    self.save if save_now && self.status_changed?

    self
  end

  private

  def max_categories
    4
  end
end
