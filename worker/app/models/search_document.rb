class SearchDocument < ActiveRecord::Base

  include SearchRebuilder
  include FullTextSearchable

  self.table_name = 'pg_search_documents'

  VALID_SEARCHABLE_TYPES = {
    posts: 'Post',
    learning_tracks: 'LearningTrack',
    users: 'User',
    blog_articles: 'BlogArticle',
    educational_institutions: 'EducationalInstitution',
    challenges: 'Challenge',
    multimedia: 'Multimedia',
    supporting_courses: 'SupportingCourse',
    learning_dynamics: 'LearningDynamic',
    discussions: 'Discussion'
  }

  belongs_to :searchable, polymorphic: true

  VALID_SEARCHABLE_TYPES.keys.each do |type|
    model_str = VALID_SEARCHABLE_TYPES[type]
    scope type, ->{ where(searchable_type: model_str) }
    belongs_to model_str.underscore.to_sym, foreign_key: :searchable_id, class_name: model_str
  end

  attr_readonly :content

  # overrides
  def save(validation=true)
    if content_changed? && valid?
      new_record? ? execute_create_query : execute_update_query
    end
  end

  def sync_content
    if valid?
      self.content = searchable.searchable_content
      @content_changed = true
    end
  end

  private
    def execute_create_query
      date_now = DateTime.now
      prefix   = 'INSERT INTO "%s" ("searchable_type", "searchable_id", "created_at", "updated_at", "content")' % [self.class.table_name]
      query    = "VALUES ('%s', %d, '%s', '%s', to_tsvector(unaccent('%s')))" % [
        searchable.class.to_s,
        searchable.id,
        date_now,
        date_now,
        self.class.connection.quote_string(content)
      ]
      suffix   = 'RETURNING "id"'
      result   = self.class.connection.execute([prefix, query, suffix].join(' '))
      self.id  = result.getvalue(0, 0)
      @content_changed = false

      self.id
    end

    def execute_update_query
      date_now = DateTime.now
      prefix   = 'UPDATE "%s"' % [self.class.table_name]
      query    = "SET \"updated_at\" = '%s', \"content\" = to_tsvector(unaccent('%s')) WHERE \"id\" = %d" % [
        date_now,
        self.class.connection.quote_string(content),
        self.id
      ]
      self.class.connection.execute([prefix, query].join(' '))
      @content_changed = false
    end

    def content_changed?
      return true if new_record?

      @content_changed == true
    end
end
