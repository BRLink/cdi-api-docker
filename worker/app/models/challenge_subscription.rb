class ChallengeSubscription < ActiveRecord::Base
  include Accessable
  include AchievementConcerns::ChallengeSubscriptionAchievementable

  belongs_to :challenge
  belongs_to :user

  validates :challenge, :user, presence: true
  validates :challenge_id, uniqueness: { scope: [:user_id] }

  def user_can_access? user
    self.user_id == user.id
  end
end
