class SubscribersGroup < ActiveRecord::Base

  belongs_to :user
  has_many :subscribers_group_members
  has_many :subscribers, through: :subscribers_group_members, source: :user_subscription
  has_many :users_members, through: :subscribers, source: :user

  alias :members :subscribers_group_members

  include ResourceVisibilitable
  include Accessable

  def user_can_access? user
    user.id == self.user_id
  end

  def user_can_view_post? u
    users_members.exists?(id: u.id)
  end

  def has_subscription? subscription
    subscribers.where(id: subscription.id).count > 0
  end

  def total_members
    members.count
  end
end
