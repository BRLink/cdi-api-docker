class ResourceHistory < ActiveRecord::Base

  belongs_to :user
  belongs_to :historable, polymorphic: true

  ACTION_TYPES = {
    create: 'create',
    update: 'update',
    delete: 'delete',
    status_change: 'status_change'
  }

  validates :action_type, presence: true, inclusion: ACTION_TYPES.values

  validates :historable_type, :historable_id, presence: true
end
