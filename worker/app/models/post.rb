class Post < ActiveRecord::Base

  VALID_PUBLISHER_TYPES = {
    users: "User",
    institutions: "EducationalInstitution"
  }

  include Likeable
  include Commentable
  include Statusable
  include Accessable
  include Attachable
  include ResourceVisibility
  include Categorizable
  include Featurable
  include Searchable
  include AchievementConcerns::PostAchievementable
  include BadgeConcerns::PostBadgeable

  searchable_for :content, :categories_names, if: ->(post) { post.visible_to_all? }

  belongs_to :user
  belongs_to :parent, class_name: 'Post'
  belongs_to :publisher, polymorphic: true

  has_many :photos, through: :attachments, source: :attachment, source_type: 'Photo'
  has_many :multimedias, through: :attachments, source: :attachment, source_type: 'Multimedia'
  has_many :albums, through: :attachments, source: :attachment, source_type: 'Album'
  has_many :challenges, through: :attachments, source: :attachment, source_type: 'Challenge'
  has_many :discussions, through: :attachments, source: :attachment, source_type: 'Discussion'
  has_many :post_abuses, class_name: 'PostAbuse'
  has_many :subscriber_posts
  has_many :users_subscribed, through: :subscriber_posts, source: :user_subscription

  alias :subscribers :subscriber_posts
  alias :abuses :post_abuses

  validates :publisher_type, presence: true, inclusion: VALID_PUBLISHER_TYPES.values

  status_configure default_status: :published

  scope :only_user, ->(user) {
    where(user: user)
  }

  def user_can_access? user
    self.user_id == user.id
  end

  def total_abuses
    post_abuses.count
  end

  def self.publisher_types
    VALID_PUBLISHER_TYPES
  end

  def shared_through_attachments?
    self.attachments.any? do |attachment|
      attachment.attachment_type == "Discussion"
    end
  end

  def shared?
    !self.parent.nil?
  end
end
