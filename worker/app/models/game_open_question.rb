class GameOpenQuestion < ActiveRecord::Base

  include Accessable
#  include Imageable
  include Historiable
  include Statusable
  include GameScorable
  include GameQuestionableClonable

  amoeba do
    nullify [:user_id, :status, :status_changed_at]

    exclude_association [:origin, :edition_histories, :answers, :user_scores, :questions]

    # include_association [:questions]
  end


  MAX_QUESTIONS_BY_GAME = (CDI::Config.game_open_question_max_questions_limit || 5).to_i

  belongs_to :user

  has_one :origin, as: :originable

  has_many :questions, as: :questionable, class_name: 'GameQuestion'

  has_many :answers, through: :questions

  has_many :corrected_answers, -> { corrected }, through: :questions, source: :answers
  has_many :pending_correction_answers, -> { pending_correction }, through: :questions, source: :answers

  validates :user_id, :title, presence: true

  # validate :validate_min_questions_for_game

  after_initialize :set_defaults

  alias :owner_user :user
  alias :owner_user= :user=

  validates :video_link, presence: true, format: { with: URI.regexp }, if: -> {
    self.video_link.present?
  }

  validates :image, presence: true, format: { with: URI.regexp }, if: -> {
    self.image.present?
  }

  def has_pending_correction_answers?
    pending_correction_answers.any?
  end

  def has_pending_correction_answers_for_user?(user)
    pending_correction_answers.where(game_question_answers: { user_id: user.id }).any?
  end

  private

  def validate_min_questions_for_game
    # if self.questions.count.zero?
    #   self.errors.add(:questions, 'must suply at least one question')
    # end
  end

  def custom_data_for_histories
    self.changes.symbolize_keys.slice(:user_id, :title, :status, :status_changed_at)
  end

  def set_defaults
  end
end
