class TrackingEvent < ActiveRecord::Base

  include Accessable

  belongs_to :enrolled_learning_track
  belongs_to :presentation_slide

  alias :slide :presentation_slide

  # REVIEW: This will create a huge impact in database
  # has_one :origin, as: :originable

  has_one :user, through: :enrolled_learning_track
  has_one :learning_track, through: :enrolled_learning_track
  has_one :student_class, through: :enrolled_learning_track

  has_one :presentation, through: :presentation_slide

  delegate :user_id, to: :enrolled_learning_track

  EVENT_TYPES = {
    :watched => 1,
    :played  => 2,
    :dynamic_delivered => 3
  }

  EVENT_TYPES.each do |type, value|
    scope type, -> { where(event_type: value) }
  end

  enum event_type: EVENT_TYPES

  validates :enrolled_learning_track_id, :presentation_slide_id, presence: true

  validates :presentation_slide_id, uniqueness: {
    scope: [:enrolled_learning_track_id, :event_type],
    message: 'yet tracked'
  }

  validate :slide_belongs_to_track

  private
  def slide_belongs_to_track
    if (presentation_slide_id && learning_track) and (presentation_slide.presentation_id != learning_track.presentation_id)
      self.errors.add(:presentation_slide_id, 'presentation dont belongs to track')
    end
  end

end
