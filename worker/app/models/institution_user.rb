class InstitutionUser < ActiveRecord::Base

  include AchievementConcerns::InstitutionUserAchievementable

  self.table_name = "educational_institutions_users"

  STATUS = {
    pending: 0,
    accepted: 100,
    rejected: -100
  }

  enum status: STATUS

  belongs_to :educational_institution
  belongs_to :user

  validates :educational_institution, :user, presence: true
  validates :user_id, uniqueness: { scope: [:educational_institution_id] }, on: :create

  # deveras, qualquer usuário pode ser membro de uma instituição
  # validate :validates_user_as_backoffice, on: :create

  private
  def validates_user_as_backoffice
    unless self.user.backoffice_profile?
      self.errors.add(:user_id, 'must has backoffice profile.')
    end
  end

end
