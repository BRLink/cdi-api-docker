module Statusable
  extend ActiveSupport::Concern

  DEFAULT_STATUS = {
    draft: 'draft',
    waiting_approval: 'waiting_approval',
    published: 'published',
    archived: 'archived'
  }

  included do

    def self.status_configure(options = {})

      options = {
        default_status: :draft,
        status_hash: nil,
        validate_status_inclusion: true
      }.merge(options)

      define_singleton_method :status_map do
        status = options[:status_hash].present? && options[:status_hash].is_a?(Hash) ?
          options[:status_hash].symbolize_keys :
          DEFAULT_STATUS

        status.each do |key, value|
          scope key, -> { where(status: value) }
        end
        status
      end

      scope :published_or_owned, ->(owner) {
        where('status = ? OR user_id = ?', 'published', owner.id).where('parent_id IS NULL')
      }

      scope :multiplier_visible, ->(owner) {
        where('status = ? OR user_id = ?', 'published', owner.id)
      }

      self.status_map.each do |key, value|
        define_method("status_#{key}?") do
          self.status == value
        end

        define_method("before_migrate_to_#{key}") do
          nil
        end

        define_method("after_migrate_to_#{key}") do
          nil
        end

        define_method "migrate_to_#{key}" do
          self.send("before_migrate_to_#{key}")
          set_status(value)
          self.send("after_migrate_to_#{key}")
        end

        # FIXME Este alias está quebrando a aplicação
        # alias :"moderation_status_#{key}" :"status_#{key}?"
      end

      define_method :default_status_value do
        options[:default_status]
      end

      define_method :validate_status_inclusion? do
        options[:validate_status_inclusion].present?
      end
    end

    status_configure

    validates :status, presence: true
    validates :status, inclusion: self.status_map.values, if: :validate_status_inclusion?

    before_validation :set_moderation_defaults
  end

  def set_status(new_status, save=true)
    return false if new_status.to_s == self.status.to_s

    self.status            = new_status
    self.status_changed_at = Time.zone.now

    save ? self.save : self
  end

  def set_moderation_defaults
    self.status ||= self.class.status_map[default_status_value]
  end
end
