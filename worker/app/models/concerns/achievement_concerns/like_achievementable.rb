module AchievementConcerns
  module LikeAchievementable
    extend ActiveSupport::Concern
    include AchievementConcerns::SimpleAchievementable

    private
    def like_post
      grant_on_create self.resource.publisher_type == "EducationalInstitution" ?
        :like_institution_post : :like_post
      grant_on_receive :receive_like_on_post
    end

    def like_blog_article
      grant_on_create :like_blog_article
    end

    def like_question
      grant_on_create :like_question
    end

    def like_challenge_delivery
      self.resource.users.each do |user|
        grant_to user, :receive_like_on_delivery
      end
    end

    def like_learning_track
      grant_on_receive :receive_like_on_learning_track
    end
  end
end

