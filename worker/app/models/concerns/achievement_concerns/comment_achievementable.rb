module AchievementConcerns
  module CommentAchievementable
    extend ActiveSupport::Concern
    include AchievementConcerns::SimpleAchievementable

    private
    def comment_post
      grant_on_create self.resource.publisher_type == "EducationalInstitution" ?
        :comment_institution_post : :comment_post
      grant_on_receive :receive_comment_on_post
    end

    def comment_learning_track
      grant_on_create :comment_learning_track
      grant_on_receive :receive_comment_on_learning_track
    end

    def comment_blog_article
      grant_on_create :comment_blog_article
    end

    def answer_discussion
      grant_on_create :answer_question
    end

    def comment_challenge_delivery
      self.resource.users.each do |user|
        grant_to user, :receive_comment_on_delivery
      end
    end
  end
end

