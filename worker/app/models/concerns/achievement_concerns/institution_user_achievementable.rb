module AchievementConcerns
  module InstitutionUserAchievementable
    extend ActiveSupport::Concern
    include AchievementConcerns::SimpleAchievementable

    included do
      after_update do
        accepted_as_institution_user
      end
    end

    private
    def accepted_as_institution_user
      if self.changed.include?("status") and self.status == "accepted"
        grant_to self.user, :become_part_of_institution, self.educational_institution
      end
    end

  end
end
