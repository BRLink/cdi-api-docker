module AchievementConcerns
  module UserAchievementable
    extend ActiveSupport::Concern
    include AchievementConcerns::SimpleAchievementable

    included do
      after_update :complete_profile
    end

    private
    def complete_sign_up
      grant_to self, :complete_sign_up
      complete_profile
    end

    def complete_profile
      grant_to(self, :complete_profile) if profile_complete?
    end
  end
end


