module AchievementConcerns
  module ChallengeSubscriptionAchievementable
    extend ActiveSupport::Concern
    include AchievementConcerns::SimpleAchievementable

    private
    def create_challenge_subscription
      grant_on_create :accept_challenge
    end

  end
end


