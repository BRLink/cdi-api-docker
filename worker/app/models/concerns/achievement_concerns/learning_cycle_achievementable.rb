module AchievementConcerns
  module LearningCycleAchievementable
    extend ActiveSupport::Concern
    include AchievementConcerns::SimpleAchievementable

    private
    def create_learning_cycle
      grant_on_create :create_learning_cycle
    end

  end
end



