module Multisearchable
  extend ActiveSupport::Concern

  included do
    # Integrating with pg_search
    include PgSearch

    def self.rebuild_pg_search_documents
      find_each { |record| record.update_pg_search_document }
    end

    # Workaround
    multisearchable against: [:clear_search_posts]
  end

  module ClassMethods

    def clear_multisearchable *attrs
      class_attribute :multisearchable_attrs
      self.multisearchable_attrs = attrs
    end
  end

  def clear_search_posts
    replacements = {
      ':\s' => ' ',
      ':'   => '',
      ';\s' => ' ',
      ';'   => ''
    }

    cleared_text = self.class.multisearchable_attrs.map { |key| send(key) }.join(' ')

    replacements.each do |key, val|
      r = Regexp.new(key)
      cleared_text.gsub!(r, val)
    end

    cleared_text
  end
end
