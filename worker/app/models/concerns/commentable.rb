module Commentable
  extend ActiveSupport::Concern

  included do
    has_many :comments, as: :resource, dependent: :destroy
  end

  def nested_comments
    @nested_comments ||= comments.where(parent_id: nil)
  end

  def total_comments
    comments.count
  end

  def total_nested_comments
    nested_comments.count
  end

  def user_can_create_comment?(user)
    # Todo mundo pode criar comentário
    true
  end
end
