module UserConcerns
  module Chat
    extend ActiveSupport::Concern

    included do
      has_many :chat_rooms
      has_many :chat_messages
      has_and_belongs_to_many :chat_rooms_joined, join_table: :chat_room_users, class_name: 'ChatRoom'
      has_and_belongs_to_many :chat_messages_read, join_table: :chat_message_reads, class_name: 'ChatMessage'
    end

    def chat_messages_accessible
      ChatMessage.where(chat_room_id: chat_rooms_joined).
        where.not(user_id: self)
    end

    def chat_messages_to_read
      chat_messages_accessible.where.
        not(id: ChatMessageRead.where(user: self).
                  select('distinct chat_message_id'))
    end

    def chat_rooms_to_read
      ChatRoom.where(id: chat_messages_to_read.select('distinct chat_room_id'))
    end
  end
end
