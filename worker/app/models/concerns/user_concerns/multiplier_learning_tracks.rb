module UserConcerns
  module MultiplierLearningTracks

    extend ActiveSupport::Concern

    included do
      has_many :learning_cycles
      has_many :learning_tracks, through: :learning_cycles

      alias :cycles :learning_cycles
      alias :tracks :learning_tracks

      has_and_belongs_to_many :moderate_learning_tracks,
                               join_table: :learning_track_moderators,
                               class_name: 'LearningTrack'

      has_many :moderate_learning_cycles,
               through: :moderate_learning_tracks,
               source: :learning_cycle

      has_many :moderate_presentations,
               through: :moderate_learning_tracks,
               source: :presentation

      has_many :moderate_slides,
               through: :moderate_presentations,
               source: :presentation_slides

      # learning track content
      has_many :presentations

      has_many :slides,
               through: :presentations,
               source: :presentation_slides

      has_many :learning_dynamics
      has_many :learning_deliveries, through: :learning_dynamics

      has_many :received_student_deliveries, through: :learning_deliveries, source: :student_deliveries

      has_many :multimedia
      has_many :supporting_courses

      # users learning cycles and tracks questions & answers
      has_many :questions_answers,
               class_name: 'QuestionUserAnswer',
               foreign_key: :user_id

      has_many :answered_questions,
               through: :questions_answers,
               source: :question

      has_many :categories, through: :learning_tracks
    end

    def all_slides
      slides | moderate_slides
    end

    def find_editable_slide id
      slides.find_by(id: id) || moderate_slides.find_by(id: id)
    end

    def find_editable_learning_track id
      learning_tracks.find_by(id: id) || moderate_learning_tracks.find_by(id: id)
    end

    def all_learning_tracks
      learning_tracks | moderate_learning_tracks
    end

    def add_as_track_moderator(track)
      track.moderators << self
    end

    def remove_track_moderation(track)
      self.moderate_learning_tracks.delete(track)
    end

    def moderator_of_track?(track)
      self.moderate_learning_tracks.exists?(id: track.id)
    end

    def multiplier_tracks_recommendations(limit=4)
      retval = []
      [
        :tracks_recommend_by_category,
        :tracks_recommend_by_institution,
        :tracks_recommend_by_following
      ].each do |action|
        q = self.send(action).limit(limit)
        q = q.where.not(id: retval.map(&:id)) if retval.size > 0
        tracks = q.to_ary
        retval.concat tracks
        limit -= tracks.size

        break if limit == 0
      end

      retval
    end


    def tracks_recommend_by_category
      LearningTrack.published
        .joins(:learning_cycle)
        .where.not(learning_cycles: {user_id: id})
        .where.not(id: moderate_learning_tracks)
        .where(categories: categories)
        .order('created_at DESC')
        .distinct
    end

    def tracks_recommend_by_institution
      institution_tracks.published
        .joins(:learning_cycle)
        .where.not(learning_cycles: {user_id: id})
        .where.not(id: moderate_learning_tracks)
        .order('created_at DESC')
        .distinct
    end

    def tracks_recommend_by_following
      LearningTrack.published
        .joins(:learning_cycle)
        .where(learning_cycles: {user_id: following_users})
        .where.not(id: moderate_learning_tracks)
        .order('created_at DESC')
    end
  end
end
