module UserConcerns
  module Badges
    extend ActiveSupport::Concern

    included do
      has_many :user_badges, dependent: :destroy
      has_many :badges, through: :user_badges
    end

  end
end
