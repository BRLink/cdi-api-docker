module UserConcerns
  module Multimedias

    extend ActiveSupport::Concern

    included do
      has_many :multimedias
    end
  end
end
