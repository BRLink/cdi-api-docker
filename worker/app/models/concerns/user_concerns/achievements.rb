module UserConcerns
  module Achievements
    extend ActiveSupport::Concern

#     SCORE_BY_INSTITUTION_QUERY = <<-SQL
#       SELECT sum(COALESCE(p.score, a.default_score))
#         FROM user_achievements ua
#   INNER JOIN achievements a
#           ON ua.achievement_id = a.id
#    LEFT JOIN points p
#           ON p.achievement_id = a.id
#          AND p.educational_institution_id = %{institution_id}
#        WHERE ua.user_id = %{user_id};
#     SQL
#
#     DEFAULT_SCORE = <<-SQL
#       SELECT sum(a.default_score)
#         FROM user_achievements ua
#   INNER JOIN achievements a
#           ON ua.achievement_id = a.id
#        WHERE ua.user_id = %{user_id};
#     SQL

    included do
      has_many :user_achievements, dependent: :destroy
      has_many :achievements, through: :user_achievements
      has_many :points, through: :achievements
      has_one  :general_ranking, class_name: "MaterializedViews::GeneralUserRanking", foreign_key: :user_id
    end

    # def query_score_by_institution institution
    #   query = SCORE_BY_INSTITUTION_QUERY % {
    #     user_id: self.id,
    #     institution_id: institution.id
    #   }

    #   self.class.connection.execute(query, :skip_logging).first.fetch("sum", nil) || 0
    # end
  end
end
