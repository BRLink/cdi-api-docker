module UserConcerns
  module Comments
    extend ActiveSupport::Concern

    included do
      has_many :comments
    end
  end
end
