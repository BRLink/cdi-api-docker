module Positionable
  extend ActiveSupport::Concern

  included do
    default_scope -> { order(position: :asc) }

    before_save :set_position, if: -> { self.send(:set_position_before_save?) }
  end

  private

  def set_position_before_save?
    true
  end

  def set_position
    self.position ||= (self.class.maximum(:position) || 0).next
  end

end
