module PubsubConcerns
  module Subscriber
    extend ActiveSupport::Concern

    included do
      has_many :user_subscriptions
      alias :subscriptions :user_subscriptions

      has_many :institutions_subscribed, through: :user_subscriptions, source: :publisher, source_type: 'EducationalInstitution'
      has_many :users_subscribed, through: :user_subscriptions, source: :publisher, source_type: 'User'

      has_many :subscriber_posts, through: :user_subscriptions
      has_many :feeds, through: :subscriber_posts, source: :post

      has_many :subscriber_surveys, through: :user_subscriptions
      has_many :subscriber_discussions, through: :user_subscriptions
      has_many :discussion_feeds, through: :subscriber_discussions, source: :discussion

      has_many :following_users,
        through: :user_subscriptions,
        source: :publisher,
        source_type: 'User'
    end

    class ActiveRecord::Associations::CollectionProxy
      def reduce_by_visibility visibility
        if visibility.is_a? User
          where(id: visibility.id)
        elsif visibility.is_a? SubscribersGroup
          joins(:subscribers_groups)
          .where(subscribers_groups: { id: visibility.id })
        elsif visibility.is_a? StudentClass
          to_ary.select { |u| visibility.admin_user_id == u.id || visibility.students.where(id: u.id).count > 0 }
        elsif visibility.is_a? EducationalInstitution
          to_ary.select { |u| u.id == visibility.admin_user_id || visibility.managers.where(id: u.id).count > 0 || visibility.students.where(id: u.id).count > 0 }
        else
          self
        end
      end
    end

    def follow? resource
      subscriptions.exists?(publisher: resource)
    end

    def subscription_for resource
      subscriptions.find_by(publisher: resource)
    end

    def find_publisher options
      if options[:type] == :institutions
        find_institution options
      elsif options[:type] == :classes
        find_student_class options
      elsif options[:type] == :users && options[:id] == id
        self
      end
    end

    def find_institution options
      if backoffice_profile?
        all_educational_institutions.find_by(id: options[:id])
      else
        student_classes_institutions.find_by(id: options[:id])
      end
    end

    def find_student_class options
      if backoffice_profile?
        ret = administered_student_classes.find_by(id: options[:id])
        return ret if ret
      end
      classes.find_by(id: options[:id])
    end

    def self_user_subscription_id
      @self_user_subscription_id ||= subscriptions.find_by(publisher: self).try(:id)
    end
  end
end
