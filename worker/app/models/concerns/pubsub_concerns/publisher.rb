module PubsubConcerns
  module Publisher
    extend ActiveSupport::Concern

    included do
      has_many :subscribers, class_name: 'UserSubscription', as: :publisher
      has_many :subscribers_users, through: :subscribers, source: :user
      has_many :published_posts, class_name: 'Post', as: :publisher
      has_many :published_discussions, class_name: 'Discussion', as: :publisher
    end

    def followed_by? user
      user.follow? self
    end
  end
end
