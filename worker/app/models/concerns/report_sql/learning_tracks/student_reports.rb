module ReportSQL
  module LearningTracks
    module StudentReports

      extend ActiveSupport::Concern

      REPORTABLE_ASSOCIATED_RESOURCES = [
        'GameCompleteSentence'.freeze,
        'GameQuiz'.freeze,
        'GamePoll'.freeze,
        'GameOpenQuestion'.freeze,
        'GameTrueFalse'.freeze,
        'GameWordSearch'.freeze,
        'GameRelatedItem'.freeze,
        'GameGroupItem'.freeze,
        # 'LearningDynamic'# out for now
      ]

      included do

        # TODO: SQL VERSION
        # TODO: Rewrite this method (DRY code (GameUserScore implement the same code in GameTrackMethods))
        def refresh_students_reports_column
          enrolled = track.enrolled_learning_tracks
                          .where(student_class_id: student_class_id)
                          .joins(:user) # only include valid enrollments
                          .includes(:user, :student_class)

          track_slides = track.visible_slides
                              .order(position: :asc, associated_resource_type: :desc)
                              .includes(:associated_resource)

          delivery = track.learning_delivery

          students_data = enrolled.map do |enroll|

            student = enroll.student
            student_class = enroll.student_class
            student_delivery = student_delivery_for_student(student, delivery)

            data = {
              student_id: student.id,
              student_name: student.name,
              content_progress: enroll.progress,
              student_delivery_id: student_delivery.try(:id),
              student_delivery: student_delivery_as_json(student_delivery),
              associated_resources: []
            }

            track_slides.each do |slide|
              if reportable_associated_resource?(slide)
                associated_data = associated_data_row(slide.associated_resource, student, student_class)
                data[:associated_resources] << associated_data
              end
            end

            # data[:associated_resources].sort_by! {|a| a['associated_resource_type'] }

            data
          end

          data = {
            students_data: students_data,
            updated_at: Time.zone.now
          }

          self.migrate_to_generated
          self.update_attribute(:students_reports, report_data: data.to_json)
        end

        def student_delivery_as_json(student_delivery)
          student_delivery.try(:as_json, only: [:id, :correct, :corrected_at])
        end

        def update_student_row(user, resource, is_associated_resource = true)
          report_data = self.students_reports['report_data']
          json_report_data = begin
            if report_data.is_a?(Hash)
              report_data
            else
              JSON.parse report_data
            end
          rescue => e
            Rollbar.error(e) #?
            {}
          end

          students_data = json_report_data.fetch('students_data', [])
          old_students_data = json_report_data.fetch('students_data', []).dup

          ar_t, ar_i = resource.class.to_s, resource.id

          students_data.each do |student_data|
            next unless student_data['student_id'] == user.id

            if is_associated_resource
              associated_data = student_data.fetch('associated_resources', [])

               associated_data.each do |data|
                if data['associated_resource_id'] == ar_i && data['associated_resource_type'] == ar_t

                  new_data = associated_data_row(resource, user, self.student_class)
                  data.merge!(new_data.stringify_keys)
                end
              end
            else
              if resource.is_a?(StudentDelivery)
                student_data['student_delivery'] = student_delivery_as_json(resource)
              end
            end
          end

          data = {
            students_data: students_data,
            updated_at: Time.zone.now
          }

          self.update_attribute(:students_reports, report_data: data)
        end

        protected
        def reportable_associated_resource?(slide)
          return false if slide.blank? || slide.associated_resource_id.blank?

          REPORTABLE_ASSOCIATED_RESOURCES.member?(slide.associated_resource_type)
        end

        def student_delivery_for_student(student, delivery)
          return nil if delivery.blank?

          student_delivery = student
                            .student_deliveries
                            .find_by(student_class_id: student_class_id, learning_delivery_id: delivery.id)

          student_delivery ||= student
                                .group_students_deliveries
                                .find_by(student_class_id: student_class_id, learning_delivery_id: delivery.id)
        end


        def associated_data_row(associated_resource, student, student_class)
          return {} if associated_resource.blank?

          associated_data = {
            associated_resource_id: associated_resource.id,
            associated_resource_type: associated_resource.class.to_s
          }

          score = GameUserScore.score_for(student, student_class, associated_resource)

          associated_data.merge!(
            student_score: score.score_percent
          )

          if associated_resource.respond_to?(:has_pending_correction_answers_for_user?)
            flag = associated_resource.has_pending_correction_answers_for_user?(student)
            associated_data.merge!(
              has_pending_correction_answers: flag,
              corrected_answers_count: student.all_responses_for_game(associated_resource).corrected.size
            )
          end

          associated_data
        end

      end

    end
  end
end
