module ReportSQL
  module LearningTracks
    module Reviews

      extend ActiveSupport::Concern

      # TODO: Review this query (is the same from MATERIALIZED VIEW)
      REVIEW_REPORT_QUERY = <<-QUERY
        SELECT ltr.learning_track_id,
               ltr.review_type,
               ltr.student_class_id,
        -- Total by review_type column
        COUNT(*) as total_reviews_by_type,
        -- Percentage for class by review_type
        round(
        -- Amount
        ((COUNT(*) * 100)::numeric)/
        (SELECT COUNT(*)
          FROM learning_track_reviews
          WHERE learning_track_id = ltr.learning_track_id AND
                student_class_id = ltr.student_class_id
        ), 2) as total_percent
        FROM learning_track_reviews ltr
        WHERE ltr.learning_track_id = %s AND ltr.student_class_id = %s
        GROUP BY ltr.review_type, ltr.learning_track_id, ltr.student_class_id
        ORDER BY learning_track_id, review_type, student_class_id;
      QUERY

      included do

        def refresh_reviews_reports_column
          query = ActiveRecord::Base.send(:sanitize_sql, [
            REVIEW_REPORT_QUERY, self.learning_track_id, self.student_class_id
          ], '')

          query_result = ActiveRecord::Base.connection.execute(query)
          result_data  = query_result.to_a

          review_types = ::LearningTrackReview::REVIEWS_TYPES.invert

          review_types.each do |type, _|
            unless result_data.any? {|d| d['review_type'].to_i == type.to_i }

              review_data = {
                learning_track_id: learning_track_id,
                review_type: type,
                student_class_id: student_class_id,
                total_percent: '0',
                total_reviews_by_type: '0'
              }

              result_data.push(review_data.stringify_keys)
            end
          end

          report_data = Hash[result_data.map {|d| [ review_types[d['review_type'].to_i], d['total_percent']] }] || {}
          report_data.merge!(updated_at: Time.zone.now)

          column_data = {
            data: result_data.to_json,
            report_data: report_data.to_json
          }

          self.migrate_to_generated
          self.update_attribute(:reviews_reports, column_data)
        end
      end

    end
  end
end
