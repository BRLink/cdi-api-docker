module ProfileImageable

  extend ActiveSupport::Concern

  included do
    mount_uploader :profile_image, ProfileImageUploader
    mount_base64_uploader :profile_image, ProfileImageUploader, base_filename: :profile_picture

    # create dinamic methods to handle cropping
    crop_uploaded :profile_image

    process_in_background :profile_image if CDI::Config.enabled?(:process_avatar_upload_in_background) ||
                                            CDI::Config.enabled?(:process_upload_in_background)
  end

  def profile_images
    profile_image_versions = self.profile_image.versions
    versions = profile_image_versions.map(&:first)
    images   = profile_image_versions.map(&:last).map(&:url)

    Hash[versions.zip(images)].presence || [self.profile_image_url]
  end

  def has_uploaded_image?
    self.profile_image_url.present? &&
    !self.profile_image_url.match(/(fallback|default)\.(png|jpg|jpeg)$/)
  end
end
