module BadgeConcerns
  module CommentBadgeable
    extend ActiveSupport::Concern
    include BadgeConcerns::SimpleBadgeable

    private
    def award_on_comment_created
      quantity = self.user.comments.count
      award_on_create "created_%d_comments", quantity
    end
  end
end

