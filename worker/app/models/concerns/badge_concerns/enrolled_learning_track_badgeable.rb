module BadgeConcerns
  module EnrolledLearningTrackBadgeable
    extend ActiveSupport::Concern
    include BadgeConcerns::SimpleBadgeable

    included do
      after_update :award_on_complete_learning_track
    end

    private
    def award_on_complete_learning_track
      quantity = self.user.enrolled_learning_tracks.where(
        status: EnrolledLearningTrack::STATUS[:completed]
      ).count
      award_on_create "completed_%d_tracks", quantity
    end

  end
end




