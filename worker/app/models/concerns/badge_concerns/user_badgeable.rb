module BadgeConcerns
  module UserBadgeable
    extend ActiveSupport::Concern
    include BadgeConcerns::SimpleBadgeable

    included do
      after_update :award_on_profile_complete
    end

    private
    def award_on_profile_complete
      award_to(self, :complete_profile) if profile_complete?
    end
  end
end

