module GameScorable
  extend ActiveSupport::Concern

  included do
    has_many :user_scores, as: :game, class_name: 'GameUserScore'
  end

end
