# code from plataformatec/mail_form

require 'active_model'

module ActiveModelable

  extend ActiveSupport::Concern

  included do

    @@class_attributes = Hash.new {|k,v| k[v] = [] }

    extend ActiveModel::Naming
    extend ActiveModel::Translation
    extend ActiveModel::Callbacks
    include ActiveModel::Validations
    include ActiveModel::Conversion
    include ActiveModel::Serializers::JSON

    define_model_callbacks :deliver

    def self.attributes(*accessors)
      options = accessors.extract_options!

      # TODO: make this not depend on column_names
      columns_methods = self.respond_to?(:column_names) ? column_names.map(&:to_sym) : []

      attrs = (accessors - instance_methods.map(&:to_sym) - columns_methods)
      attr_accessor(*(accessors - instance_methods.map(&:to_sym) - columns_methods))

      (@@class_attributes[self.model_name.collection].concat(attrs)).uniq
    end
  end

  def attributes
    attrs = @@class_attributes[self.model_name.collection]
    values = attrs.map {|_attr| self.send(_attr) }

    Hash[attrs.zip(values)]
  end

  def initialize(params={})
    params.each_pair do |attr, value|
      send("#{attr}=", value) if respond_to?("#{attr}=", true)
    end unless params.blank?
  end

  # Always return true so when using form_for, the default method will be post.
  def new_record?
    true
  end

  def persisted?
    false
  end

  # Always return nil so when using form_for, the default method will be post.
  def id
    nil
  end

  # Create just check validity, and if so, trigger callbacks.
  def deliver
    if valid?
      run_callbacks :deliver
    else
      false
    end
  end

  alias :save :deliver

  def i18n_scope
    :active_modelable
  end

end
