module FullTextSearchable

  extend ActiveSupport::Concern

  included do
    scope :search, -> (content, options={}) { FullTextSearchQuery.new(all, content, options).collection }
  end

  class FullTextSearchQuery
    def initialize(collection, content, options={})
      @collection = collection
      @content    = collection.connection.quote_string(content)
      @options    = {
        use_joins: true
      }.merge(options)
    end

    ## SEMANTIC BUG:
    ## Há um bug conhecido no AR#count. Se o `select` statement for
    ## sobrescrito, o count do activerecord para de funcionar.
    ## Workaround:
    ## > SearchDocument.search('fuzzbuzz').count('*')
    ## Um dos mantedores do rails fechou o ticket e disse que o problema
    ## é de SQL e não irá corrigir.
    ## https://github.com/rails/rails/issues/2541#issuecomment-13884438
    def collection
      query_collection = @collection
        .from("plainto_tsquery(unaccent('%s')) query, %s" % [@content, @collection.table_name])
        .select('*, ts_rank_cd("%s"."content", query) AS rank' % [SearchDocument.table_name])
        .order('rank DESC')
        .where('"%s"."content" @@ query' % [SearchDocument.table_name])

      if @options[:use_joins] && SearchDocument.table_name != @collection.table_name
        query_collection = query_collection.joins(SearchDocument.to_s.underscore.to_sym)
      end

      query_collection
    end
  end
end
