module LibrarySearchable

  extend ActiveSupport::Concern

  included do
    include PgSearch
  end

  module ClassMethods
    def search_extra_fields fields
      pg_search_scope :multi_field_search,
        associated_against: { categories: :name },
        against: [:tags] << fields,
        using: {tsearch: {prefix: true}},
        ignoring: :accents
    end
  end
end
