module Accessable
  extend ActiveSupport::Concern

  def user_can_access?(user)
    user.admin? || self.user_id == user.id
  end

  def user_can_read?(user)
    user_can_access?(user)
  end

  def user_can_manage?(user)
    user_can_access?(user)
  end

  def user_can_update?(user)
    user_can_manage?(user)
  end

  def user_can_delete?(user)
    user_can_manage?(user)
  end

end
