module Likeable
  extend ActiveSupport::Concern

  included do
    has_many :likes, as: :resource, dependent: :destroy
  end

  def total_likes
    likes.count
  end
end
