class UserAchievement < ActiveRecord::Base

  belongs_to :user
  belongs_to :achievement
  belongs_to :resource, polymorphic: true

  delegate :default_score, to: :achievement

  VALID_RESOURCES_TYPES = ["Like",
                           "Comment",
                           "Post",
                           "Question",
                           "User",
                           "Discussion",
                           "LearningTrack",
                           "LearningCycle",
                           "EnrolledLearningTrack",
                           "LearningTrackReview",
                           "EducationalInstitution",
                           "ChallengeSubscription",
                           "ChallengeDelivery",
                           "Challenge",
                           "StudentClass",
                           "SurveyQuestion",
                           "SurveyQuestionAnswer"]

  validates :user_id, :achievement_id, :resource_id, :resource_type, presence: true
  validates :resource_type, inclusion: VALID_RESOURCES_TYPES
  validates :user_id, uniqueness: { scope: [:achievement_id, :resource_id] }

  validate  :valid_user_type

  after_commit :notify_user, if: -> { achievement.notifiable? }

  scope :history_for, -> (user) {
    where(user_id: user.id).includes(:achievement).order(created_at: :desc)
  }

  def valid_user_type
    unless achievement.user_type.any? { |type| type == "all" or user.send(:"#{type}?") }
      errors.add(:user, "#{user.profile_type} cannot receive achievement #{achievement.action}")
    end
  end

  def history_message
    message = I18n.t :message, scope: "cdi.achievement.#{achievement.action}"
    message % { points: achievement.default_score }
  end

  def notify_user
    options = {
      notification_type: "achievement_granted",
      notificable: self,
      receiver_user: user
    }
    notification = Notification.create(options)
    notification_config = user.notification_config "achievement_granted"
    if notification_config.can_email?
      UsersMailer.achievement_notification(self).deliver_later
    end
    if notification_config.can_notification?
      Pusher.trigger("private-user-#{user.id}",
                      :notification,
                      CDI::V1::NotificationSerializer.new(notification))
    end
  end
end
