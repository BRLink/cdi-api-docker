class DiscussionOption < ActiveRecord::Base

  include Accessable
  include Positionable

  belongs_to :discussion

  has_many :answers, class_name: 'DiscussionAnswer', foreign_key: :option_id, dependent: :destroy
  has_many :answers_users, through: :answers, source: :user

  validates :text, :discussion, presence: true
  validates :text, uniqueness: { scope: [:discussion_id] }
  validate :discussion_reached_options_limit, on: [:create]

  def user_can_access?(user)
    discussion.user_id == user.id
  end

  def discussion_reached_options_limit
    if discussion.options.count >= Discussion::MAX_OPTIONS_BY_DISCUSSION
      self.errors.add(:base, I18n.t('cdi.errors.discussions.max_options_for_discussion_reached'))
    end
  end

  def set_position_before_save?
    false
  end
end
