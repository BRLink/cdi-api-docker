module API
  module Helpers
    module V1
      module SupportingCoursesHelpers

        extend Grape::API::Helpers

        params :new_supporting_course do
          requires :supportingcourse, type: Hash do
            requires :title, :type => String
            requires :category_ids
            # optional :presentation_id, :type =>Integer
            optional :tags
            optional :skills
          end
        end

        params :update_supporting_course do
          requires :supportingcourse, type: Hash do
            optional :title, :type => String
            optional :category_ids
            # optional :presentation_id, :type =>Integer
            optional :tags
            optional :skills
          end
        end

        def paginated_serialized_supporting_courses(supporting_courses, options = {})
          options = {
            serializer: :supporting_course,
            root: :supporting_course
          }.merge(options)
          collection = paginate(supporting_courses)
          paginated_serialized_array(collection, options)
        end

        def serialized_supporting_course(supporting_course, options = {})
          options = { serializer: :supporting_course }.merge(options)
          serialized_object(supporting_course, options)
        end

        def get_supporting_course_slide
          supporting_course = SupportingCourse.find_by(id: params[:id])
          if supporting_course.nil?
            not_found_error_response(:supporting_courses)
          else
            slide = supporting_course.slides.find_by(id: params[:slide_id])
            if slide.nil?
              not_found_error_response(:supporting_courses_slides)
            else
              return slide
            end
          end
          return nil
        end

        def user_can_write
          unless current_user.backoffice_profile?
            response = {
              error: true,
              status_code: 404,
              errors: "action not allowed for user"
            }
            error!(response, response[:status_code])
          end
        end

      end
    end
  end
end
