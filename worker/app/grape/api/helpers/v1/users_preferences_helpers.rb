# encoding: UTF-8

module API
  module Helpers
    module V1
      module UsersPreferencesHelpers

        extend Grape::API::Helpers

        def serialized_user_preferences user
          presenter = presenter(:user_preferences).new(user)
          serialized_object(presenter, serializer: :user_preferences, root: :preferences)
        end
      end
    end
  end
end
