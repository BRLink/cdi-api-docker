# encoding: UTF-8

module API
  module Helpers
    module V1
      module PresentationUploadHelpers

        extend Grape::API::Helpers

        params :new_presentation do
          requires :presentation, type: Hash do
            requires :file, :type => Rack::Multipart::UploadedFile
          end
          optional :start_position
        end

        def presentation_update(options)
          presentation = options.fetch :presentation
          slide_type = options.fetch :slide_type, nil
          presentation.update file: params[:presentation][:file]

          Shoryuken::Client.queues("#{Rails.env}_converter").send_message({
            user_id: current_user.id,
            presentation_id: presentation.id,
            start_position: params[:start_position],
            slide_type: slide_type
          }.to_json)
        end

        def presentation_existence_check(model)
          error!('The model does not exist.', 400) if model.nil?
          presentation = model.presentation
          error!('The model does not have a presentation', 400) if presentation.nil?          
          return presentation
        end

      end
    end
  end
end
