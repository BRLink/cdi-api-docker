# encoding: UTF-8

module API
  module Helpers
    module V1
      module SharedParamsHelpers

        extend Grape::API::Helpers

        params :new_learning_track do
          requires :learning_track, type: Hash do
            requires :name, type: String
            requires :category_ids
            requires :skills
            requires :questions_answers
            optional :tags
          end
        end

        params :new_learning_cycle do
          requires :learning_cycle, type: Hash do
            requires :name, type: String
            requires :category_ids
            requires :objective_text, type: String
            optional :tags
          end
        end

        params :new_cycle_planning do
          requires :cycle_planning, type: Hash do
            requires :questions_answers, type: Hash
          end
        end

        params :new_presentation do
          # requires :presentation, type: Hash do
          # end
        end

        params :new_presentation_slide do
          requires :slide, type: Hash do
            requires :title
            requires :layout_type
            requires :content
            requires :slide_type
            optional :metadata
            optional :hidden
          end
        end

        params :slide_update do
          requires :slide, type: Hash do
          end
        end

        params :slide_reorder do
          requires :position
        end

        params :new_file_upload do
          requires :upload, type: Hash do
            requires :file #, :type => Rack::Multipart::UploadedFile
            optional :caption
            optional :tags
          end
        end

        params :new_learning_dynamics do
          requires :dynamic, type: Hash do
            requires :title
            requires :category_ids
            requires :perspectives
            requires :skills
          end
        end

        params :update_learning_dynamics do
          requires :dynamic, type: Hash do
            optional :title
            optional :category_ids
            optional :perspectives
            optional :skills
          end
        end

      end
    end
  end
end
