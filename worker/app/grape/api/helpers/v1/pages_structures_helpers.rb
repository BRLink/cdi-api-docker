# encoding: UTF-8

module API
  module Helpers
    module V1
      module PagesStructuresHelpers

        extend Grape::API::Helpers

        def serialized_page_structure_presenter(object_name)

          object_page_name = [object_name.to_s, 'page'].join('_')

          presenter_name = File.join('page_structure', object_page_name)
          serializer_name = File.join('page_structure', object_page_name)

          serializer = serializer(serializer_name)
          presenter  = presenter(presenter_name)

          serializer_object = serializer.new(presenter.new)

          serializer_object.as_json
        end

        def cached_page_structure_response(page_name)
          respond_with_cacheable("page_structures.#{page_name}") do
            serialized_page_structure_presenter(page_name.to_sym)
          end
        end

      end
    end
  end
end
