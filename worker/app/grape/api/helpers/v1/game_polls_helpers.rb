# encoding: UTF-8

module API
  module Helpers
    module V1
      module GamePollsHelpers

        extend Grape::API::Helpers

        def serialized_game_poll(game_poll, options = {})
          options = { serializer: :game_poll }.merge(options)
          serialized_object(game_poll, options)
        end

      end
    end
  end
end
