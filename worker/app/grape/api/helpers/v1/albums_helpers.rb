# encoding: UTF-8

module API
  module Helpers
    module V1
      module AlbumsHelpers

        extend Grape::API::Helpers

        def serialized_album(user, options={})
          options = { serializer: :album }.merge(options)
          serialized_object(user, options)
        end
      end
    end
  end
end
