module API
  module Helpers
    module V1
      module ChannelsHelpers
        extend Grape::API::Helpers

        def current_user_is_enrolled?(channel)
          tracks = current_user.enrolled_learning_tracks.joins(:student_class).where(
            { student_class: channel.student_class, learning_track: channel.learning_track }
          )
          return tracks.present?
        end

        def current_user_have_administered_class_for_this_learning_track?(class_id, track_id)
          student_class = current_user.administered_student_classes.joins(:learning_tracks)
            .where({id: class_id, learning_tracks: {id: track_id}})
          return student_class.present?
        end

        def current_user_is_admin?(channel)
          return current_user_have_administered_class_for_this_learning_track?(
            channel.student_class_id, channel.learning_track_id
          )
        end

      end
    end
  end
end
