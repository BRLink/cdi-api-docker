# encoding: UTF-8

module API
  module Helpers
    module V1
      module ErrorsHelpers

        extend ActiveSupport::Concern

        included do

          rescue_from :all do |e|
            status_code = e.try(:status) || 500

            response = {
             error: true,
             status_code: status_code,
             errors: [
               e.message,
             ],
             backtrace: %w(development staging).member?(Rails.env.to_s) ? e.backtrace : []
            }.to_json

            Rollbar.error(e)

            Rack::Response.new(response, status_code, { 'Content-Type' => 'application/json' } )
          end
        end
      end
    end
  end
end
