# encoding: UTF-8

module API
  module Helpers
    module V1
      module SharedServiceActionsHelpers

        extend Grape::API::Helpers

        # TODO: Verify where `simple_serializer` can be used as response

        def new_learning_track_service_response(user, params, options = {})
          service = execute_service('LearningTracks::CreateService', user, params)
          response_for_create_service(service, :learning_track, options)
        end

        def new_learning_cycle_service_response(user, params, options = {})
          service = execute_service('LearningCycles::CreateService', user, params)
          response_for_create_service(service, :learning_cycle, options)
        end

        def new_learning_track_presentation_service_response(user, params, options = {})
          service = execute_service('LearningTracks::PresentationCreateService', user, params)
          response_for_create_service(service, :presentation, options)
        end

        def new_presentation_service_response(user, params, options = {})
          service = execute_service('Presentations::CreateService', user, params)
          response_for_create_service(service, :presentation, options)
        end

        def new_presentation_slide_thumb_upload_service_response(user, slide, params, options = {})
          service = execute_service('PresentationSlides::ThumbUploadService', slide, user, params)

          images_url = {
            original: service.asset.try(:image_url)
          }.merge((service.asset && service.asset.images_url.slice(:cover, :other)) || {})

          service_response = { images_url: images_url }

          response_for_service(service, service_response, options)
        end

        def new_presentation_slide_service_response(user, params, options = {})
          service = execute_service('PresentationSlides::CreateService', user, params)
          response_for_create_service(service, :presentation_slide, options)
        end

        def slide_reorder_service_response(user, slide, params, options = {})
          service = execute_service('PresentationSlides::ReorderService', slide, user, params)
          response_for_update_service(service, :presentation_slide, options)
        end

        def user_gallery_new_file_upload_service_response(user, params, options = {})
          service = execute_service('UserGalleries::AssetUploadCreateService', user, params)
          options = { serializer: :simple_user_gallery_asset }.merge(options)

          response_for_create_service(service, :asset, options)
        end

      end
    end
  end
end
