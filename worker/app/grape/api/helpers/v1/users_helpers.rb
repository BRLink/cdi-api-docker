# encoding: UTF-8

module API
  module Helpers
    module V1
      module UsersHelpers

        extend Grape::API::Helpers

        # match `2username`, `2329username`, `username434`
        USERNAME_REGEXP = /\A\w+(?=[a-z])/i

        def find_user_by_identifier(identifier)
          # searching by username
          if identifier.to_s.match(USERNAME_REGEXP).present?
            User.normal.find_by(username: identifier)
          else
            User.normal.find_by(id: identifier.to_i)
          end
        end

        def user_provider_auth_response(service)
          if service.success?
            response = user_success_response_for_service(service, new_user: service.new_user?)
          else
            status service.response_status
            response = error_response_for_service(service)
          end

          response
        end

        def service_name_for_provider_auth(options = {})
          create_user_enabled = CDI::Config.enabled?(:allow_user_creation_in_facebook_auth_endpoint)

          options[:create_user] == "true" && create_user_enabled ?
            'FromProviderCreateService' :
            'ProviderAuthenticationCreateService'
        end

        def user_success_response_for_service(service, merge_response = {})
          status service.response_status

          response = {
            success: true,
            status_code: service.response_status,
            user_data: service_user_as_json(service.user),
            auth_data: service_auth_as_json(service.auth_token)
          }.merge(merge_response)
        end

        def service_auth_as_json(auth)
          {
            auth_token: auth.token,
            provider: auth.provider,
            expires_at: auth.expires_at
          }
        end

        def service_user_as_json(user)
          user.as_json(only: [:id, :username, :profile_type],
                       methods: [:tof_accepted])
        end

        def serialized_user(user, options = {})
          options = { serializer: :user }.merge(options)
          serialized_object(user, options)
        end

        def serialized_current_user(user, options = {})
          serialized_user(user, options.merge(serializer: :current_user))
        end

        def paginated_notifications_for_user(user, options = {})
          notifications = user.notifications
          paginate(notifications).includes(:sender_user, :receiver_user, :notificable)
        end

        def paginated_serialized_notifications(notifications)
          paginated_serialized_array(notifications, serializer: :notification, root: :notifications)
        end

        def paginated_serialized_gallery(gallery, options = {})
          assets = gallery.photos.visible

          serialized_assets  = paginated_serialized_gallery_assets(assets, options[:asset_options] || {}).as_json
          serialized_gallery = serialized_gallery(gallery, options[:gallery_options] || {}).as_json

          meta_data = serialized_assets.delete(:meta)

          response = {
            gallery: serialized_gallery.merge(serialized_assets),
            meta: meta_data
          }.as_json

        end

        def serialized_gallery(gallery, options = {})
          options = {
            serializer: :simple_user_gallery
          }.merge(options)

          serialized_object(gallery, options)
        end

        def paginated_serialized_gallery_assets(assets, options = {})
          options = {
            serializer: :simple_user_gallery_asset,
            root: :assets
          }.merge(options)

          paginated_assets = paginate(assets)

          paginated_serialized_array(paginated_assets, options)
        end

      end
    end
  end
end
