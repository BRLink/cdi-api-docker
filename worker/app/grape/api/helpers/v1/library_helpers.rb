# encoding: UTF-8

module API
  module Helpers
    module V1
      module LibraryHelpers

        extend Grape::API::Helpers

        def paginated_serialized_library(library, options = {})
          options = {
            serializer: :library,
            root: :library,
            paginate: true
          }.merge(options)
          paginated_serialized_array(library,options)
        end
      end
    end
  end
end
