# encoding: UTF-8

module API
  module Helpers
    module V1
      module TrackingEventsHelpers

        extend Grape::API::Helpers

        def event_params
          params[:event] = {
            learning_track_id: params[:id],
            event_type: params[:type],
            presentation_slide_id: params[:slide_id]
          }

          params
        end

      end
    end
  end
end
