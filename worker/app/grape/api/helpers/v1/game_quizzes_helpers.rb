# encoding: UTF-8

module API
  module Helpers
    module V1
      module GameQuizzesHelpers

        extend Grape::API::Helpers

        def serialized_game_quiz(game_quiz, options = {})
          options = { serializer: :game_quiz }.merge(options)
          serialized_object(game_quiz, options)
        end

      end
    end
  end
end
