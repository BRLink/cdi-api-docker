# encoding: UTF-8

module API
  module Helpers
    module V1
      module ChatMessagesHelpers

        extend Grape::API::Helpers

        def room_messages_response(room)
          messages = paginate(room.messages.order('created_at DESC'))
          options = {
            root: :chat_messages,
            serializer: :chat_message,
            each_serializer_meta: {
              current_user_id: current_user.id
            }
          }
          paginated_serialized_array(messages, options)
        end

        def rooms_list_response(rooms)
          options = {
            root: :chat_rooms,
            serializer: :chat_room,
            each_serializer_meta: {
              current_user_id: current_user.id
            }
          }
          paginated_serialized_array(rooms, options)
        end

        def get_chat_room(me, target, create_if_not_found=true)
          # if room exists for the two users, then they can talk.
          # if room does not exist, then current_user must meet certain conditions.
          if me.id == target.id
            return nil
          end
          room = me.chat_rooms_joined.joins(:chat_room_users).
                  where(chat_room_users: {user: target})

          room = me.chat_rooms_joined.where(id: ChatRoom.rooms_with_user(target)).first

          if room.blank? and create_if_not_found and can_create_room?(me, target)
            room = create_room(me, target)
          end

          return room
        end

        def can_create_room?(me, target)
          # handle chat directions here. whether student can init chat with multiplier etc.
          if me.backoffice_profile?
            return true
          end
          return user_is_student_of_target?(me, target)
        end

        def user_is_student_of_target?(me,target)
          # allow student of a teacher to send message to the teacher.
          # condition: study / teach class intersection; class must be active
          me.student_classes.where(id: target.administered_student_classes).active.present?
        end

        def create_room(me, target)
          # try let multiplier be host of the room
          # even when student initiate the chat
          host = me
          client = target
          if me.student? and target.backoffice_profile?
            host = target
            client = me
          end
          room = ChatRoom.create(user: host)
          room.members << client
          return room
        end

      end
    end
  end
end
