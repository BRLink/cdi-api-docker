module API
  module V1
    class UsersMeMultiplierCacheable < API::V1::Base

      helpers API::Helpers::V1::LearningCyclesHelpers
      helpers API::Helpers::V1::LearningTracksHelpers

      with_cacheable_endpoints :users do
        namespace :me do

          before do
            authenticate_user
          end

          namespace :administered_classes do
            desc 'Get all administrated classes for current user'
            get do
              respond_with_cacheable 'users.current_user_administrated_classes', current_user.id do
                student_classes = current_user.administered_student_classes.includes(:users)

                options = {
                  serializer: :student_class,
                  root: :classes,
                  each_serializer_meta: {
                    limit_students: true
                  }
                }

                serialized_array(student_classes, options).as_json
              end
            end
          end

          namespace :learning_cycles do

            paginated_endpoint do

              desc 'Get paginated learning cycles for current user'
              get do
                categories_ids = categories_ids_from_params

                categories_key = { categories: (categories_ids.any? ? categories_ids.join('_') : 'no_filter') }

                cache_data = pagination_cache_replace_data(current_user.id).merge(categories_key)

                response_for_paginated_endpoint 'current_user.learning_cycles', cache_data do
                  if categories_ids.any?
                    cycles = current_user.learning_cycles
                                         .joins(:learning_tracks)
                                         .where(learning_tracks: { category_ids: categories_ids })
                                         .group('learning_cycles.id').uniq
                  else
                    cycles = current_user.learning_cycles.group('learning_cycles.id').uniq
                  end

                  options = {
                    each_serializer_meta: {
                      include_cover_image_url: true,
                      category_ids: categories_ids,
                      learning_tracks_ids: current_user.learning_track_ids
                    }
                  }

                  learning_tracks_by_cycles_feed_response(cycles, false, options)
                end
              end

            end
          end

          namespace :learning_tracks do

            paginated_endpoint do

              desc 'Get paginated learning tracks for current user'
              get do
                categories_ids = categories_ids_from_params
                options = {}

                if categories_ids.any?
                  options[:conditions] = { category_ids: categories_ids }
                end

                response_for_paginated_endpoint 'users.current_user_learning_tracks' do
                  paginated_scoped_serialized_learning_tracks(current_user, :learning_tracks, options).as_json
                end
              end
            end
          end

          paginated_endpoint do
            desc 'Get moderated tracks feed for multiplier user'

            get :moderate_learning_tracks do
              if current_user.backoffice_profile?
                response_for_paginated_endpoint 'users.current_user_moderate_learning_tracks' do

                  categories_ids = categories_ids_from_params
                  if categories_ids.any?
                    cycles = current_user.moderate_learning_cycles
                                         .joins(:learning_tracks)
                                         .where(learning_tracks: { category_ids: categories_ids })
                                         .group('learning_cycles.id').uniq
                  else
                    cycles = current_user.moderate_learning_cycles.group('learning_cycles.id').uniq
                  end

                  options = {
                    each_serializer_meta: {
                      include_cover_image_url: true,
                      only_moderate_tracks: true,
                      categories_ids: categories_ids,
                      learning_tracks_ids: current_user.moderate_learning_track_ids
                    }
                  }

                  learning_tracks_by_cycles_feed_response(cycles, false, options)
                end
              else
                forbidden_error_response(:multiplier_learning_cycles_feed)
              end
            end
          end
        end
      end
    end
  end
end
