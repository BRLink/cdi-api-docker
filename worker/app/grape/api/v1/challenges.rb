module API
  module V1
    class Challenges < API::V1::Base

      helpers API::Helpers::V1::SharedParamsHelpers
      helpers API::Helpers::V1::SharedServiceActionsHelpers

      before do
        authenticate_user
      end

      namespace :challenges do
        paginated_endpoint do
          desc 'List closed challenges'
          get do
            if current_user.admin?
              collection = Challenge.closed_or_draft
            else
              collection = Challenge.closed
            end
            challenges = paginate(collection.order('created_at DESC'))
            paginated_serialized_array(challenges, root: :challenges, serializer: :challenge)
          end
        end

        params do
          requires :challenge, type: Hash do
            requires :title, type: String
            requires :description, type: String
            requires :delivery_title, type: String
            requires :delivery_date
            requires :delivery_kind
            requires :format_type
            optional :group_size, type: Integer
          end
        end
        desc 'Create challenge'
        post do
          service = execute_service('Challenges::CreateService', current_user, params)
          response_for_create_service(service, :challenge)
        end

        namespace :latest do
          desc 'get latest published challenge'
          get do
            challenge = Challenge.latest.first
            unless challenge
              return not_found_error_response :challenge
            end

            serialized_object(challenge, serializer: :challenge, root: :challenge)
          end

          desc 'unpublish a challenge'
          delete do
            challenge = Challenge.open.first
            unless challenge
              return not_found_error_response :challenge
            end
            challenge_opts = { delivery_date: DateTime.now - 1.hour }
            service = execute_service('Challenges::UpdateService', challenge, current_user, { challenge: challenge_opts })
            response_for_delete_service(service, :challenge)
          end

          desc 'Invite some friends to join to current open challenge.'
          post :invites do
            challenge = Challenge.open.first
            unless challenge
              return not_found_error_response :challenge
            end
            service = execute_service('Challenges::InviteService', challenge, current_user, params)
            response_for_service(service, invited_user_ids: service.merged_user_ids)
          end
        end

        route_param :id do
          desc 'get challenge by id'
          get do
            challenge = Challenge.find_by(id: params[:id])
            unless challenge.present?
              not_found_error_response :challenges
            end
            serialized_object(challenge, serializer: :challenge, root: :challenge)
          end

          desc 'Update challenge'
          params do
            requires :challenge, type: Hash do
              optional :title
              optional :description
              optional :delivery_title
              optional :delivery_date
              optional :delivery_kind
              optional :format_type
              optional :group_size, type: Integer
            end
          end
          put do
            challenge = Challenge.find_by id: params[:id]
            service = execute_service('Challenges::UpdateService', challenge, current_user, params)
            response_for_update_service(service, :challenge)
          end

          desc 'Delete challenge'
          delete do
            challenge = Challenge.find_by id: params[:id]
            service = execute_service('Challenges::DeleteService', challenge, current_user, params)
            response_for_delete_service(service, :challenge)
          end

          desc 'Publish a challenge'
          post :publish do
            challenge = Challenge.draft.find_by id: params[:id]
            service = execute_service('Challenges::PublishService', challenge, current_user)
            simple_response_for_service(service)
          end
        end
      end
    end
  end
end
