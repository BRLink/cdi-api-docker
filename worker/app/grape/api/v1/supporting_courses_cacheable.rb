# encoding: UTF-8

module API
  module V1
    class SupportingCoursesCacheable < API::V1::Base

      helpers API::Helpers::V1::SupportingCoursesHelpers
      helpers API::Helpers::V1::PresentationsHelpers

      before do
        authenticate_user
      end

      with_cacheable_endpoints :supporting_courses do
        paginated_endpoint do
          desc 'Get paginated supporting_courses'
          get do
            if current_user.backoffice_profile?
              response_for_paginated_endpoint 'supporting_courses.all', 'all' do
                supporting_courses = SupportingCourse.published_or_owned(current_user)

                options = {
                  serializer: :supporting_course
                }

                paginated_serialized_supporting_courses(supporting_courses, options).as_json
              end
            else
              forbidden_error_response(:supporting_courses)
            end
          end
        end

        route_param :id do
          desc 'Get SupportingCourse data'
          get do
            supporting_course = SupportingCourse.multiplier_visible(current_user)
                                                .find_by(id: params[:id])

            if supporting_course
              if supporting_course.user_can_access?(current_user)
                serialized_supporting_course(supporting_course)
              else
                forbidden_error_response(:supporting_courses)
              end
            else
              not_found_error_response(:supporting_courses)
            end
          end

          desc 'Get presentation data + slides beloging to SupportingCourse'
          get :presentation do
            supporting_course = SupportingCourse.multiplier_visible(current_user)
                                                .includes(:slides,:user)
                                                .find_by(id: params[:id])

            if supporting_course && supporting_course.presentation
              # if supporting_course.user_can_access?(current_user)

              options = {
                serializer: :supporting_course_presentation
              }

              serialized_presentation(supporting_course.presentation, options)
              # else
              #   forbidden_error_response(:supporting_courses)
              # end
            else
              not_found_error_response(:supporting_courses)
            end
          end
        end

      end


      with_cacheable_endpoints :search do
        paginated_endpoint do
          desc 'search supportingcourses'
          namespace :supporting_courses do
            params do
              requires :q, type: String, regexp: /[^\s]{1,}/, allow_blank: false
            end
            get do
              if current_user.backoffice_profile?
                cache_key = params[:q].try(:parameterize)
                if cache_key
                  response_for_paginated_endpoint 'searchs.supportingcourse', cache_key do
                    courses = SupportingCourse.multiplier_visible(current_user)
                                              .multi_field_search(cache_key)

                    options = {
                      serializer: :supporting_course
                    }

                    paginated_serialized_supporting_courses(courses, options).as_json
                  end
                else
                  bad_request_error_response(:searchs, :invalid_search_term)
                end
              else
                forbidden_error_response(:supporting_courses)
              end
            end
          end
        end
      end
    end
  end
end
