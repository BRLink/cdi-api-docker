module API
  module V1
    class ChallengesDiscussions < API::V1::Base

      helpers API::Helpers::V1::DiscussionsHelpers

      before do
        authenticate_user
      end

      namespace :challenges do

        route_param :id do

          namespace :discussions do
            paginated_endpoint do
              desc 'list all discussions of a challenge'
              get do
                challenge = Challenge.find_by(id: params[:id])
                unless challenge
                  return not_found_error_response :challenge
                end
                discussions = paginate(challenge.discussions.order("created_at DESC"))
                paginated_serialized_array(discussions, root: :discussions, serializer: :discussion)
              end
            end

            desc 'Create new Discussion for challenge'
            params do
              use :new_discussion
            end
            post do
              challenge = Challenge.find_by(id: params[:id])
              unless challenge
                return not_found_error_response :challenge
              end
              service = execute_service('Discussions::CreateService', challenge, current_user, params)
              response_for_create_service(service, :discussion, root: :discussion)
            end
          end
        end
      end
    end
  end
end
