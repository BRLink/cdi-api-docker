# encoding: UTF-8

module API
  module V1
    class MultimediaCacheable < API::V1::Base

      helpers API::Helpers::V1::MultimediaHelpers
      before do
        authenticate_user
      end

      with_cacheable_endpoints :multimedia do

        paginated_endpoint do

          desc 'Get paginated multimedia'
          get do
            if current_user.backoffice_profile?
              response_for_paginated_endpoint 'multimedia.all', 'all' do
                multimedia = params[:own].nil? ?
                  Multimedia.multiplier_visible(current_user).all :
                    current_user.multimedia
                options = {
                  serializer: :multimedia
                }

                paginated_serialized_multimedia(multimedia, options).as_json
              end
            else
              forbidden_error_response(:multimedias)
            end
          end

          route_param :id do
            desc 'Get multimedia data'
            get do
              multimedia = Multimedia.multiplier_visible(current_user).find_by(id: params[:id])

              if multimedia
                if multimedia.user_can_access?(current_user)
                  serialized_multimedia(multimedia).as_json
                else
                  forbidden_error_response(:multimedias)
                end
              else
                not_found_error_response(:multimedias)
              end
            end
          end
        end
      end

      with_cacheable_endpoints :search do
        paginated_endpoint do
          desc 'search multimedia'
          namespace :multimedia do
            params do
              requires :q, type: String, regexp: /[^\s]{1,}/, allow_blank: false
            end
            get do
              if current_user.backoffice_profile?
                cache_key = params[:q].try(:parameterize)
                if cache_key
                  response_for_paginated_endpoint 'searchs.multimedia', cache_key do
                    media = Multimedia.multiplier_visible(current_user).multi_field_search(cache_key)
                    options = {
                      serializer: :multimedia
                    }
                    paginated_serialized_multimedia(media,options).as_json
                  end
                else
                  bad_request_error_response(:searchs, :invalid_search_term)
                end
              else
                forbidden_error_response(:multimedias)
              end
            end
          end
        end
      end
    end
  end
end
