# encoding: UTF-8

module API
  module V1
    class UsersMeCacheable < API::V1::Base

      helpers API::Helpers::V1::UsersHelpers
      helpers API::Helpers::V1::EducationalInstitutionsHelpers

      with_cacheable_endpoints :users do
        namespace :me do

          before do
            authenticate_user
          end

          desc 'Get current logged in user data'
          get do
            cache_data = { user_id: current_user.id }

            respond_with_cacheable('current_user.show', cache_data) do
              serialized_current_user(current_user).as_json
            end
          end

          [:owned_educational_institutions, :educational_institutions].each do |scope_name|
            namespace scope_name do

              paginated_endpoint do

                desc "Get paginated #{scope_name} for current user"
                get do
                  response_for_paginated_endpoint "users.current_user_#{scope_name}" do
                    paginated_scoped_serialized_educational_institutions(current_user, scope_name).as_json
                  end
                end

              end
            end
          end

          paginated_endpoint do
            desc 'Get paginated notifications for current user'
            get :notifications do
              response_for_paginated_endpoint 'users.current_user_notifications' do
                notifications = paginated_notifications_for_user(current_user)

                paginated_serialized_notifications(notifications).as_json
              end
            end
          end

          namespace :gallery do
            paginated_endpoint do
              desc 'Get all current user assets gallery'
              get do
                gallery_album = current_user.default_gallery_album

                if gallery_album
                  response_for_paginated_endpoint 'users.current_user_assets_gallery' do
                    paginated_serialized_gallery(gallery_album).as_json
                  end
                else
                  not_found_error_response(:gallery_albums)
                end

              end
            end
          end
        end

        get ':identifier' do
          user = find_user_by_identifier(params[:identifier])

          if user
            cache_key = params[:identifier].to_s.parameterize

            respond_with_cacheable 'users.show', cache_key do
              serialized_user(user, serializer: :public_user).as_json
            end

          else
            not_found_error_response(:users)
          end
        end
      end
    end
  end
end
