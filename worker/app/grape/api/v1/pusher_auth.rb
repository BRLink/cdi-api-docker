module API
  module V1
    class PusherAuth < API::V1::Base

      before do
        authenticate_user
      end

      namespace :pusher do

        namespace :auth do
          desc 'Authenticate current user to pusher and return channel name'
          post do
            if current_user.id == params[:channel_name].split('-')[2].to_i
              return generic_success_response if headers['X-Provider'] == 'postman'
              auth = Pusher.authenticate(params[:channel_name], params[:socket_id])
              if auth
                return generic_success_response 200, auth
              end
            end

            return forbidden_error_response(:notifications)
          end
        end

      end

    end
  end
end
