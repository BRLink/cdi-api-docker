# encoding: UTF-8

module API
  module V1
    class ChatMessages < API::V1::Base

      helpers API::Helpers::V1::ChatMessagesHelpers

      before do
        authenticate_user
      end

      # one-one chat. get room via user id.
      namespace :messages do

        namespace :user do
          route_param :user_id do

            paginated_endpoint do
              desc 'list chat history with a user'
              get do
                target = User.find_by(id: params[:user_id])
                if target.present?
                  room = get_chat_room(current_user, target, false)
                  if room.present?
                    room_messages_response(room)
                  else
                    not_found_error_response(:chat_rooms)
                  end
                else
                  not_found_error_response(:users)
                end
              end
            end

            desc 'post new message to user'
            params do
              requires :chat_message, type: Hash do
                requires :content, type: String
              end
            end
            post do
              target = User.find_by(id: params[:user_id])
              if target.present?
                room = get_chat_room(current_user, target)
                if room.present?
                  service = execute_service('ChatMessages::CreateService', room, current_user, params)
                  response_for_create_service(service, :chat_message)
                else
                  not_found_error_response(:chat_rooms)
                end
              else
                not_found_error_response(:users)
              end
            end

            desc 'mark all messages as read'
            put :mark_read do
              target = User.find_by(id: params[:user_id])
              if target.present?
                room = get_chat_room(current_user, target,false)
                if room.present?
                  service = execute_service('ChatMessages::BatchMarkService', room, current_user, params)
                  marked_messages = service.chat_message_reads
                  response_for_service(service, marked_messages: marked_messages)
                else
                  not_found_error_response(:chat_rooms)
                end
              else
                not_found_error_response(:users)
              end
            end
          end
        end

        namespace :batch_send do
          desc 'send message to multiplier people'
          params do
            requires :chat_message, type: Hash do
              requires :user_ids, type: Array[Integer]
              requires :content, type: String
            end
          end
          post do
            service = execute_service('ChatMessages::BatchCreateService', current_user, params)
            messages = service.chat_messages
            response_for_service(service, chat_messages: messages)
          end
        end

        namespace :room do

          desc 'create a room with given users'
          # params do
          #   requires :user_id, as: Integer
          # end
          post do
            target = User.find_by(id: params[:user_id])
            if target.present?
              room = get_chat_room(current_user, target, false)
              if room.present?
                serialized_object(room, serializer: :chat_room)
              else
                not_found_error_response(:chat_rooms)
              end
            else
              not_found_error_response(:users)
            end
          end

          route_param :room_id do

            desc 'list all members of a given room'
            get :members do
              room = current_user.chat_rooms_joined.find_by(id: params[:room_id])
              unless room
                return not_found_error_response(ChatRoom.to_s.underscore)
              end
              serialized_array(room.all_users, serializer: :simple_user, root: :users)
            end

            paginated_endpoint do
              desc 'list chat history with a user'
              get do
                room = current_user.chat_rooms_joined.find_by(id: params[:room_id])
                if room.present?
                  room_messages_response(room)
                else
                  not_found_error_response(:chat_rooms)
                end
              end
            end

            desc 'post new message to user'
            params do
              requires :chat_message, type: Hash do
                requires :content, type: String
              end
            end
            post do
              room = ChatRoom.find_by(id: params[:room_id])
              if room.present?
                service = execute_service('ChatMessages::CreateService', room, current_user, params)
                response_for_create_service(service, :chat_message)
              else
                not_found_error_response(:chat_rooms)
              end
            end

            desc 'mark all messages as read'
            put :mark_read do
              room = ChatRoom.find_by(id: params[:room_id])
              if room.present?
                service = execute_service('ChatMessages::BatchMarkService', room, current_user, params)
                marked_messages = service.chat_message_reads
                response_for_service(service, marked_messages: marked_messages)
              else
                not_found_error_response(:chat_rooms)
              end
            end

          end
        end

        namespace :pending_rooms do
          paginated_endpoint do
            desc 'list rooms with messages to read'
            get do
              rooms = paginate(current_user.chat_rooms_to_read)
              rooms_list_response(rooms)
            end
          end
        end

        namespace :rooms do
          paginated_endpoint do
            desc 'list rooms user joined'
            get do
              rooms = current_user.chat_rooms_joined
              rooms_list_response(rooms)
            end
          end

          route_param :room_id do
            desc 'show room information'
            get do
              room = current_user.chat_rooms_joined.find_by(id: params[:room_id])
              unless room
                not_found_error_response(:chat_room)
              end
              serialized_object(room, root: :room, serializer: :chat_room)
            end

          end
        end

        namespace :moderated_rooms do
          paginated_endpoint do
            desc 'list rooms user moderated'
            get do
              rooms = current_user.chat_rooms
              rooms_list_response(rooms)
            end
          end
        end

        route_param :message_id do
          desc 'mark message as read'
          put :mark_read do
            message = ChatMessage.find_by(id: params[:message_id])
            if message.present?
              service = execute_service('ChatMessages::MarkService', message, current_user, params)
              response_for_service(service, marked_message: service.chat_message_read)
            else
              not_found_error_response(:chat_messages)
            end
          end

          desc 'update an existing message'
          params do
            requires :chat_message, type: Hash do
              requires :content, type: String
            end
          end
          put :edit do
            message = ChatMessage.find_by(id: params[:message_id])
            if message.present?
              service = execute_service('ChatMessages::UpdateService', message, current_user, params)
              response_for_update_service(service, :chat_message)
            else
              not_found_error_response(:chat_messages)
            end
          end

          desc 'delete a message'
          delete do
            message = ChatMessage.find_by(id: params[:message_id])
            if message.present?
              service = execute_service('ChatMessages::DeleteService', message, current_user, params)
              response_for_delete_service(service, :chat_message)
            else
              not_found_error_response(:chat_messages)
            end
          end
        end
      end
    end
  end
end
