# encoding: UTF-8

module API
  module V1
    class LearningTracks < API::V1::Base

      helpers API::Helpers::V1::LearningTracksHelpers
      helpers API::Helpers::V1::SharedParamsHelpers
      helpers API::Helpers::V1::SharedServiceActionsHelpers

      before do
        authenticate_user
      end

      namespace :learning_tracks do
        desc 'Create a new learning track for user'
        params do
          use :new_learning_track
          group :learning_track, type: Hash do
            requires :learning_cycle_id, type: Integer
          end
        end

        post do
          new_learning_track_service_response(current_user, params)
        end

        route_param :id do

          ## Nested Resources

          desc 'Create a new presentation for LearningTrack'
          params do
            use :new_presentation
          end
          post :presentations do
            params[:presentation] = {
              learning_track_id:  params[:id]
            }
            new_learning_track_presentation_service_response(current_user, params)
          end

          desc 'Create a new presentation and add slide to LearningTrack'
          params do
            use :new_presentation_slide
          end
          post :slides do
            params[:slide].merge!(learning_track_id: params[:id])

            new_presentation_slide_service_response(current_user, params)
          end

          # TODO: Group params from .create to use here
          desc 'Updates owned track'
          put do
            learning_track = find_learning_track_for_current_user

            service = execute_service('LearningTracks::UpdateService', learning_track, current_user, params)
            response_for_update_service(service, :learning_track, serializer: :simple_learning_track)
          end

        end
      end

    end
  end
end
