# encoding: UTF-8

module API
  module V1
    class Libraries < API::V1::Base

      helpers API::Helpers::V1::LibraryHelpers
      before do
        authenticate_user
      end

      with_cacheable_endpoints :library do

        paginated_endpoint do

          desc 'Get paginated library'
          params do
            optional :column, type: Integer
          end
          get do
            if current_user.backoffice_profile?
              response_for_paginated_endpoint 'libraries.all', 'all' do
                col = params[:column] || 0
                opts = { 'resource' => col }
                opts['owner'] = current_user

                library = Library.query(opts)

                paginated_serialized_library(library).as_json
              end
            else
              forbidden_error_response(:library)
            end
          end
        end
      end

      with_cacheable_endpoints :search do
        paginated_endpoint do
          desc 'search library'
          namespace :library do
            params do
              requires :q,   type: String, regexp: /[^\s]{1,}/, allow_blank: false
              optional :column, type: Integer
            end
            get do
              if current_user.backoffice_profile?
                cache_key = params[:q].try(:parameterize)
                col = params[:column] || 0
                opts = {'resource' => col}
                opts['owner'] = current_user
                if cache_key
                  response_for_paginated_endpoint 'searchs.library', cache_key do
                    opts['q'] = cache_key
                    library = Library.query(opts)
                    paginated_serialized_library(library).as_json
                  end
                else
                  bad_request_error_response(:searchs, :invalid_search_term)
                end
              else
                forbidden_error_response(:library)
              end
            end
          end
        end
      end

      # # my files

      # with_cacheable_endpoints :mylibraries do

      #   paginated_endpoint do

      #     desc 'Get personal paginated library'
      #     get do
      #       response_for_paginated_endpoint 'mylibraries.all', 'all' do
      #         library = Library.my_files current_user
      #         paginated_serialized_library(library).as_json
      #       end
      #     end
      #   end
      # end

      # with_cacheable_endpoints :search do
      #   paginated_endpoint do
      #     desc 'search personal library'
      #     namespace :mylibraries do
      #       params do
      #         requires :q, type: String, regexp: /[^\s]{1,}/, allow_blank: false
      #       end
      #       get do
      #         cache_key = params[:q].try(:parameterize)
      #         if cache_key
      #           response_for_paginated_endpoint 'searchs.mylibrary', cache_key do
      #             library = Library.my_files current_user, cache_key
      #             paginated_serialized_library(library).as_json
      #           end
      #         else
      #           bad_request_error_response(:searchs, :invalid_search_term)
      #         end
      #       end
      #     end
      #   end
      # end

    end
  end
end
