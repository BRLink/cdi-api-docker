# encoding: UTF-8

module API
  module V1
    class Photos < API::V1::Base

      include API::Helpers::V1::PhotosHelpers

      before do
        authenticate_user
      end

      namespace :users do
        namespace :photos do

          desc 'Uploads a new image for user album'
          params do
            requires :photo, type: Hash do
              requires :image, :type => Rack::Multipart::UploadedFile
              optional :caption
              optional :album_id
              optional :tags
            end
          end

          post do
            service = execute_service('Photos::CreateService', current_user, params)
            response_for_create_service(service, :photo)
          end

          route_param :id do
            desc 'Edit the content of photo'
            params do
              requires :photo, type: Hash do
                optional :caption
                optional :tags
              end
            end
            put do
              photo = current_user.photos.find_by(id: params[:id])
              service = execute_service('Photos::UpdateService', photo, current_user, params)
              response_for_update_service(service, :photo)
            end

            desc 'Delete a photo'
            delete do
              photo = current_user.photos.find_by(id: params[:id])
              service = execute_service('Photos::DeleteService', photo, current_user, params)
              response_for_delete_service(service, :photo)
            end
          end
        end
      end

      namespace :albums do
        route_param :id do
          namespace :photos do
            paginated_endpoint do
              desc 'List all albums from a given user.'
              get do
                album = Album.find_by(id: params[:id])
                return not_found_error_response(Album.to_s.underscore) unless album
                photos = paginate(album.photos)
                paginated_serialized_array(photos, root: :photos, serializer: :photo)
              end
            end
          end
        end
      end

    end
  end
end
