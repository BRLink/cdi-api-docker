# encoding: UTF-8

module API
  module V1
    class PresentationsCacheable < API::V1::Base

      helpers API::Helpers::V1::PresentationsHelpers
      helpers API::Helpers::V1::PresentationSlidesHelpers

      before do
        authenticate_user
      end

      # TODO: Verify cache (respond_with_cacheable)
      with_cacheable_endpoints :presentations do
        route_param :id do
          desc 'Get presentation data'
          get do
            presentation = Presentation.multiplier_visible(current_user).find_by(id: params[:id])

            if presentation
              serialized_presentation(presentation)
            else
              not_found_error_response(:presentations)
            end
          end

          namespace :slides do
            paginated_endpoint do
              desc "Get all slides belogings to presentation"
              get do
                presentation = Presentation.multiplier_visible(current_user).find_by(id: params[:id])
                if presentation
                  response_for_paginated_endpoint 'presentations.slides', params[:id] do
                    presentation_slides = presentation.slides
                    paginated_serialized_presentation_slides(presentation_slides).as_json
                  end
                else
                  not_found_error_response(:presentations)
                end
              end
            end
          end
        end
      end

      with_cacheable_endpoints :slides do
        route_param :id do
          desc 'Get slide data'
          get do
            slide = PresentationSlide.find_by(id: params[:id])
            # TODO: Review this cache
            if slide.present? && slide.presentation.present? && ( slide.presentation.status_published? || slide.presentation.user = current_user )
              respond_with_cacheable('presentation_slides.show', params[:id]) do
                serialized_object(slide, serializer: :presentation_slide).as_json
              end
            else
              not_found_error_response(:presentation_slides)
            end
          end
        end
      end
    end
  end
end
