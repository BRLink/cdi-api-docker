# encoding: UTF-8

module API
  module V1
    class SupportingCoursesPresentationsActions < API::V1::Base

      helpers API::Helpers::V1::SupportingCoursesHelpers
      helpers API::Helpers::V1::SharedParamsHelpers
      helpers API::Helpers::V1::SharedServiceActionsHelpers

      before do
        authenticate_user
        user_can_write
      end

      namespace :supporting_courses do
        route_param :id do

          desc 'Create a new presentation and add slide to SupportingCourse'
          params do
            use :new_presentation_slide
          end
          post :slides do
            params[:slide].merge!(supporting_course_id: params[:id])

            new_presentation_slide_service_response(current_user, params)
          end

          namespace :slides do
            route_param :slide_id do
              desc "Update presentation slide for supporting course"
              params do
                use :slide_update
              end
              put do
                slide = get_supporting_course_slide
                if slide.present?
                  service = execute_service('PresentationSlides::UpdateService', slide, current_user, params)
                  response_for_update_service(service, :presentation_slide)
                end
              end

              desc "Delete presentation slide for supporting course"
              delete do
                slide = get_supporting_course_slide
                if slide.present?
                  service = execute_service('PresentationSlides::DeleteService', slide, current_user, params)
                  response_for_delete_service(service, :presentation_slide)
                end
              end

              desc "Reorder presentation slide for supporting course"
              params do
                use :slide_reorder
              end
              put :reorder do
                slide = get_supporting_course_slide
                if slide.present?
                  slide_reorder_service_response(current_user, slide, params)
                end
              end
            end
          end
        end
      end
    end
  end
end
