# encoding: UTF-8

module API
  module V1
    class UsersVisibilitiesTypes < API::V1::Base

      before do
        authenticate_user
      end

      namespace :available_visibility_types do
        get do
          presenter = presenter(:available_visibility_types).new(current_user)
          { available_types: presenter.available_types }
        end
      end

    end
  end
end
