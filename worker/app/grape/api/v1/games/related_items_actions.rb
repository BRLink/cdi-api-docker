# encoding: UTF-8

module API
  module V1
    module Games
      class RelatedItemsActions < API::V1::Base

        helpers API::Helpers::V1::GamesHelpers

        before do
          authenticate_user
        end

        namespace :games do
          namespace :related_items do
            route_param :id do
              namespace :items do
                params do
                  requires :item, type: Hash do
                    requires :word
                    requires :related_word
                  end
                end
                desc "Add a new GameItem for GameRelatedItem"
                post do
                  game = current_user.game_related_items.find_by(id: params[:id])

                  service = execute_service('GameItems::CreateService', game, current_user, params)
                  response_for_create_service(service, :game_item)
                end

                route_param :item_id do
                  desc 'Update a GameItem beloging to a GameRelatedItem'
                  put do
                    game = current_user.game_related_items.find_by(id: params[:id])

                    if game
                      item = game.items.find_by(id: params[:item_id])

                      service = execute_service('GameItems::UpdateService', item, current_user, params)
                      response_for_update_service(service, :game_item)
                    else
                      not_found_error_response(:game_related_items)
                    end
                  end

                  desc 'Delete a GameItem beloging to a GameRelatedItem'
                  delete do
                    game = current_user.game_related_items.find_by(id: params[:id])

                    if game
                      item = game.items.find_by(id: params[:item_id])

                      service = execute_service('GameItems::DeleteService', item, current_user, params)
                      response_for_delete_service(service, :game_item)
                    else
                      not_found_error_response(:game_related_items)
                    end
                  end

                  desc 'Answer an game as Student'
                  params do
                    requires :answer, type: Hash do
                      optional :reply_word
                      optional :related_word

                      mutually_exclusive :reply_word, :related_word
                    end
                  end
                  post :answers do
                    game = find_game(current_user, params[:id], :related_item)
                    if game
                      game_item = game.items.find_by(id: params[:item_id])

                      params[:answer][:reply_word] = params[:answer].delete(:related_word) if params[:answer].key?(:related_word)

                      service = execute_service('GameItemAnswers::CreateService', game_item, current_user, params)
                      response_for_create_service(service, :answer, serializer: :game_item_answer)
                    else
                      not_found_error_response(:game_related_items)
                    end
                  end
                end
              end

              desc 'Answer items in batch as Student'
              params do
                requires :items, type: Hash
              end
              post :answers do
                game = find_game(current_user, params[:id], :related_item)

                service = execute_service('GameItemAnswers::BatchCreateService', game, current_user, params)

                success_answers = service.valid_answers

                data = {}

                if game
                  data = {
                    game: serialized_game_related_item_response_for_user(current_user, game),
                    answers: success_answers
                  }
                end

                response_for_service(service, data)
              end

            end
          end
        end
      end
    end
  end
end
