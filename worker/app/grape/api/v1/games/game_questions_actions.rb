# encoding: UTF-8

module API
  module V1
    module Games
      class GameQuestionsActions < API::V1::Base

        helpers API::Helpers::V1::GamesHelpers

        before do
          authenticate_user
        end

        VALID_GAME_ENDPOINTS = [
          :quiz,
          :poll,
          :open_questions,
          :true_false
        ]

        namespace :games do
          namespace :options do
            route_param :option_id do
              desc 'Delete a option'
              delete do
                option = GameQuestionOption.find_by(id: params[:option_id])

                service = execute_service('GameQuestionOptions::DeleteService', option, current_user, params)
                response_for_delete_service(service, :game_question)
              end
            end
          end

          VALID_GAME_ENDPOINTS.each do |game_type|
            namespace game_type do
              route_param :id do
                namespace_name = [:poll].exclude?(game_type) ? :questions : :question
                namespace namespace_name do

                  endpoint_block = -> {
                    desc 'Reorder a question beloging to a game'
                    put :reorder do

                      question =  find_question_by_game_type(game_type, params[:id], params[:question_id])

                      service = execute_service('GameQuestions::ReorderService', question, current_user, params)
                      response_for_update_service(service, :game_question)
                    end

                    if [:quiz, :poll].member?(game_type)
                      namespace :options do

                        desc 'Create/Add new option for question'
                        params do
                          requires :option, type: Hash do
                            requires :text
                          end
                        end
                        post do
                          question = find_question_by_game_type(game_type, params[:id], params[:question_id])

                          if question
                            service = execute_service('GameQuestionOptions::CreateService', question, current_user, params)
                            response_for_create_service(service, :game_question)
                          else
                            not_found_error_response(:game_questions)
                          end
                        end

                        route_param :option_id do
                          desc 'Delete a option beloging to a question'
                          delete do

                            question = find_question_by_game_type(game_type, params[:id], params[:question_id])
                            option   = question.presence && question.options.find_by(id: params[:option_id])

                            service = execute_service('GameQuestionOptions::DeleteService', option, current_user, params)
                            response_for_delete_service(service, :game_question)
                          end

                          desc 'Reorder a question beloging to a question'
                          put :reorder do
                            question = find_question_by_game_type(game_type, params[:id], params[:question_id])
                            option    = question.presence && question.options.find_by(id: params[:option_id])

                            service = execute_service('GameQuestionOptions::ReorderService', option, current_user, params)
                            response_for_update_service(service, :game_question)
                          end

                        end
                      end
                    end
                  }

                  param_name = [:poll].exclude?(game_type) ? :question_id : nil

                  if param_name
                    route_param param_name do
                      endpoint_block.call
                    end
                  else
                    endpoint_block.call
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
