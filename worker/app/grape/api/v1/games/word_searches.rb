# encoding: UTF-8

module API
  module V1
    module Games
      class WordSearches < API::V1::Base

        helpers API::Helpers::V1::GamesHelpers

        before do
          authenticate_user
        end

        namespace :games do
          namespace :word_searches do
            desc "Create a new GameWordSearch for user"
            params do
              requires :game, type: Hash do
                requires :words
              end
            end

            post do
              service = execute_service('GameWordSearches::CreateService', current_user, params)
              response_for_create_service(service, :game_word_search)
            end

            route_param :id do
              desc "Updates owned game GameWordSearch"
              put do
                game = find_game(current_user, params[:id], :word_search)

                service = execute_service('GameWordSearches::UpdateService', game, current_user, params)
                response_for_update_service(service, :game_word_search)
              end

              desc "Delete owned user GameWordSearch"
              delete  do
                game = find_game(current_user, params[:id], :word_search)

                service = execute_service('GameWordSearches::DeleteService', game, current_user, params)
                response_for_delete_service(service, :game_word_search)
              end

              desc 'Answer an game as Student'
              params do
                requires :words
              end
              post :answers do
                game = find_game(current_user, params[:id], :word_search)

                service = execute_service('GameWordAnswers::CreateService', game, current_user, params)
                response_for_create_service(service, :answer, serializer: :game_word_answer)
              end
            end
          end
        end
      end
    end
  end
end
