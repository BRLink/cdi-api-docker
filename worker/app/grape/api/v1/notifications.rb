module API
  module V1
    class Notifications < API::V1::Base

      before do
        authenticate_user
      end

      namespace :notifications do
        desc 'List all notifications'
        get do
          total_unread = current_user.notifications.where(read: false).count
          notifications = paginate current_user.notifications
          paginated_serialized_array notifications, root: :notification, serializer: :notification, meta: {total_unread: total_unread}
        end

        route_param :id do
          namespace :mark_read do
            desc 'Mark notification as read'
            put do
              notification = current_user.notifications.find_by id: params[:id]
              not_found_error_response(notification) if notification.nil?

              notification.update read: true

              generic_success_response
            end
          end
        end

        namespace :mark_read do
          desc 'Mark all notifications as read'
          put do
            current_user.notifications.each do |notification|
              notification.update read: true
            end
            generic_success_response
          end
        end
      end
    end
  end
end
