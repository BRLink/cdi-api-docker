# encoding: UTF-8

module API
  module V1
    class ContactForms < API::V1::Base

      helpers API::Helpers::V1::ContactFormsHelpers

      before do
        authenticate_user
      end

      namespace :contacts do
        desc 'Send a contact message'
        params do
          requires :contact, type: Hash do
            requires :name
            requires :email
            requires :phone_area_code
            requires :phone_number
            requires :subject
            requires :message_body
          end
        end

        post do
          service = execute_service('ContactForms::CreateService', current_user, params)
          simple_response_for_service(service)
        end

        desc 'Send a feedback message'
        params do
          requires :contact, type: Hash do
            requires :description
            requires :message_body
            optional :image, :type => Rack::Multipart::UploadedFile
          end
        end
        post :feedback do
          service = execute_service('ContactForms::Feedback::CreateService', current_user, params)
          simple_response_for_service(service)
        end
      end
    end
  end
end
