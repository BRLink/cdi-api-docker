# encoding: UTF-8

module API
  module V1
    class UsersSubscriptionsActions < API::V1::Base

      before do
        authenticate_user
      end

      namespace :publishers do
        UserSubscription.publisher_types.keys.each do |key|

          namespace key do

            route_param :id do

              desc 'Subscribe user for a given resource (eg. classes, institutions, users, etc)'
              post do
                klass = UserSubscription.publisher_types[key].constantize
                resource = klass.find_by(id: params[:id])
                service = execute_service("UserSubscriptions::CreateService", resource, current_user)
                response_for_create_service(service, :subscription, serializer: :user_subscription)
              end

              desc 'Unsubscribe user for a given resource (eg. classes, institutions, users, etc)'
              delete do
                klass = UserSubscription.publisher_types[key].constantize
                resource = klass.find_by(id: params[:id])
                service = execute_service("UserSubscriptions::DeleteService", resource, current_user)
                response_for_delete_service(service, :subscription, serializer: :user_subscription)
              end
            end
          end
        end
      end
    end
  end
end
