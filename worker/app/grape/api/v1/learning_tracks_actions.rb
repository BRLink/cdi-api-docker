# encoding: UTF-8

module API
  module V1
    class LearningTracksActions < API::V1::Base

      helpers API::Helpers::V1::LearningTracksHelpers
      helpers API::Helpers::V1::SharedParamsHelpers
      helpers API::Helpers::V1::SharedServiceActionsHelpers

      before do
        authenticate_user
      end

      VALID_GAMES_TYPES = [
        :complete_sentence,
        :open_question,
        :poll,
        :quiz,
        :true_false,
        :word_search,
        :related_item,
        :group_item
      ]


      namespace :learning_tracks do
         route_param :id do

          desc 'Add new Dynamic as last slide of LearningTrack presentation'
          params do
            requires :learning_dynamic_id
          end
          post ':action', requirements: { action: /(add|link)_dynamic/ } do
            params[:learning_track_id] = params[:id]

            dynamic = ::LearningDynamic.find_by(id: params[:learning_dynamic_id])
            service = execute_service('LearningTracks::LearningDynamicLinkService', dynamic, current_user, params)
            response_for_create_service(service, :presentation_slide)
          end

          params do
            requires :supporting_course_id
          end
          post :link_supporting_course do
            params[:learning_track_id] = params[:id]

            supporting_course = ::SupportingCourse.find_by(id: params[:supporting_course_id])

            service = execute_service('LearningTracks::SupportingCourseLinkService', supporting_course, current_user, params)
            opts ={
              serializer: :learning_track_presentation_slide
            }
            response_for_create_service(service, :presentation_slide, opts)
          end

          desc 'Add new Game as last slide of LearningTrack presentation'
          params do
            requires :game_id
            requires :game_type, values: VALID_GAMES_TYPES.map(&:to_s)
          end
          post :link_game do
            params[:learning_track_id] = params[:id]

            game_scope = "game_#{params[:game_type].to_s.downcase.pluralize}"
            game = current_user.send(game_scope).find_by(id: params[:game_id])

            service = execute_service('LearningTracks::GameLinkService', game, current_user, params)
            response_for_create_service(service, :presentation_slide)
          end

          namespace :learning_dynamics do
            desc 'Add a slide associated with a LearningDynamic for LearningTrack'
            params do
              use :new_learning_dynamics
            end
            post do
              params[:dynamic].merge!(
                learning_track_id: params[:id]
              )

              service = execute_service('LearningTracks::LearningDynamicCreateService', current_user, params)
              response_for_create_service(service, :learning_dynamic)
            end
          end

          namespace :presentation do
            namespace :slides do
              route_param :slide_id do

                desc 'Reorder a slide inside a presentation'
                params do
                  use :slide_reorder
                end
                put :reorder do
                  learning_track = find_learning_track_for_current_user

                  if learning_track
                    slide = learning_track.slides.find_by(id: params[:slide_id])
                    slide_reorder_service_response(current_user, slide, params)
                  else
                    not_found_error_response(:learning_tracks)
                  end

                end

              end
            end
          end

          desc 'Clone a learning track for a given learning cycle'
          params do
            requires :learning_cycle_id
          end
          post :clone do
            learning_track = LearningTrack.find_by(id: params[:id])

            if learning_track
              service = execute_service('LearningTracks::CloneService', learning_track, current_user, params)
              options = {
                serializer: :learning_track,
                root: :learning_track
              }

              response_for_create_service(service, :cloned_learning_track, options)
            else
              not_found_error_response(:learning_tracks)
            end

          end

          desc 'Publish a learning track'
          params do
            optional :classes_ids
          end
          post :publish do
            learning_track = find_learning_track_for_current_user

            service = execute_service('LearningTracks::PublishService', learning_track, current_user, params)

            data = {
              classes_published: service_classes_published(service, :classes_published),
              classes_errors: service_classes_published(service, :classes_errors)
            }

            response_for_service(service, data, true)
          end

          desc 'Add user as author/moderator of learning track'
          params do
            requires :users_ids
          end
          put :authors do
            learning_track = find_learning_track_for_current_user

            service = execute_service('LearningTracks::AuthorsReplaceService', learning_track, current_user, params)

            response_for_service(service, authors: service_moderation_users(service, :added_moderators))
          end

          desc 'Update moderators of LearningTrack'
          params do
            requires :users_ids
          end
          post :authors do
            learning_track = find_learning_track_for_current_user

            service = execute_service('LearningTracks::AddAuthorService', learning_track, current_user, params)

            response_for_service(service, added_moderators: service_moderation_users(service, :added_moderators))
          end

          desc 'Removes a author/moderator from learning track moderation list'
          params do
            requires :users_ids
          end
          delete :authors do
            learning_track = find_learning_track_for_current_user

            service = execute_service('LearningTracks::RemoveAuthorService', learning_track, current_user, params)

            response_for_service(service, removed_moderators: service_moderation_users(service, :removed_moderators))
          end

          desc 'Delete owned user learning track'
          delete do
            learning_track = current_user.learning_tracks.find_by(id: params[:id])

            service = execute_service('LearningTracks::DeleteService', learning_track, current_user, params)

            response_for_delete_service(service, :learning_track)
          end

          desc 'Removes a author/moderator from learning track moderation list'
          params do
            requires :classes_ids
          end
          delete :classes do
            learning_track = find_learning_track_for_current_user

            service = execute_service('LearningTracks::RemoveFromStudentClassService', learning_track, current_user, params)

            response_for_service(service, student_classes: service.removed_student_classes)
          end
        end
      end
    end
  end
end
