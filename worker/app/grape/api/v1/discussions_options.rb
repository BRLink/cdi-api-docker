module API
  module V1
    class DiscussionsOptions < API::V1::Base

      before do
        authenticate_user
      end

      namespace :discussions do

        route_param :id do
          namespace :options do
            params do
              requires :option, type: Hash do
                requires :text, type: String
              end
            end
            post do
              discussion = current_user.discussions.find_by(id: params[:id])
              unless discussion
                return not_found_error_response(Discussion.to_s.underscore)
              end
              service = execute_service('DiscussionOptions::CreateService', discussion, current_user, params)
              response_for_create_service(service, :discussion_option)
            end

            route_param :option_id do
              desc 'Get discussion option by id'
              get do
                discussion = current_user.discussions.find_by(id: params[:id])
                unless discussion
                  return not_found_error_response(Discussion.to_s.underscore)
                end
                option = discussion.options.find_by(id: params[:option_id])
                unless option
                  return not_found_error_response(DiscussionOption.to_s.underscore)
                end
                serialized_object(option, serializer: :discussion_option)

              end

              desc 'Update option'
              params do
                requires :option, type: Hash do
                  requires :text, type: String
                end
              end
              put do
                discussion = current_user.discussions.find_by(id: params[:id])
                unless discussion
                  return not_found_error_response(Discussion.to_s.underscore)
                end
                option = discussion.options.find_by(id: params[:option_id])
                unless option
                  return not_found_error_response(DiscussionOption.to_s.underscore)
                end
                service = execute_service('DiscussionOptions::UpdateService', option, current_user, params)
                response_for_update_service(service, :discussion_option)
              end

              desc 'Delete option'
              delete do
                discussion = current_user.discussions.find_by(id: params[:id])
                unless discussion
                  return not_found_error_response(Discussion.to_s.underscore)
                end
                option = discussion.options.find_by(id: params[:option_id])
                unless option
                  return not_found_error_response(DiscussionOption.to_s.underscore)
                end
                service = execute_service('DiscussionOptions::DeleteService', option, current_user, params)
                response_for_delete_service(service, :discussion_option)
              end

              paginated_endpoint do
                get :answers_users do
                  discussion = current_user.discussions.find_by(id: params[:id])
                  unless discussion
                    return not_found_error_response(Discussion.to_s.underscore)
                  end
                  option = discussion.options.find_by(id: params[:option_id])
                  unless option
                    return not_found_error_response(DiscussionOption.to_s.underscore)
                  end
                  users = paginate(option.answers_users)
                  paginated_serialized_array(users, root: :users, serializer: :simple_user)
                end
              end
            end
          end
        end

      end
    end
  end
end
