module API
  module V1
    class StudentDeliveriesGeneratePresentations < API::V1::Base

      before do
        authenticate_user
      end

      namespace :student_deliveries do
        route_param :id do
          desc 'Get Student delivery Presentation'
          get :presentation do
            delivery = StudentDelivery.find_by(id: params[:id])

            if delivery.blank?
              return not_found_error_response(:student_delivery)
            end

            unless delivery.students.include?(current_user) or current_user.backoffice_profile?
              return forbidden_error_response(:student_delivery)
            end

            presentation = delivery.presentation

            if presentation.blank?
              return not_found_error_response(:student_delivery_presentation)
            end

            serialized_object(presentation, serializer: :presentation)
          end
        end
      end
    end
  end
end
