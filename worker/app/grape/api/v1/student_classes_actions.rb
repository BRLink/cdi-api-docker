# encoding: UTF-8

module API
  module V1
  class StudentClassesActions < API::V1::Base

      helpers API::Helpers::V1::StudentClassesHelpers

      before do
        authenticate_user
      end

      namespace :classes do

        desc 'Join StudentClass as user(student)'
        params do
          requires :code, type: String
        end
        post :join do
          student_class = StudentClass.valid.find_by(code: params[:code])

          if student_class
            params[:users_ids] = current_user.id
            service = execute_service('StudentClasses::AddStudentService', student_class, current_user, params)

            response = serialized_object(service.student_class, serializer: :simple_student_class, root: :student_class)
            response_for_service(service, response.as_json)
          else
            not_found_error_response(:student_classes)
          end
        end


        route_param :id do
          {
            active: :active,
            deactive: :closed
          }.each do |endpoint, status|
            desc "Migrate StudentClass to #{status}"

            put endpoint do
              params[:new_status] = status

              student_class = current_user.administered_student_classes.find_by(id: params[:id])

              service = execute_service('StudentClasses::StatusChangeService', student_class, current_user, params)
              response_for_update_service(service, :student_class, serializer: :simple_student_class)
            end
          end

          desc 'Update code expiration if code is expired'
          put :code_expiration do
            student_class = current_user.administered_student_classes.find_by(id: params[:id])

            service = execute_service('StudentClasses::ActiveCodeService', student_class, current_user)
            response_for_update_service(service, :student_class, serializer: :simple_student_class)
          end

          desc 'Add users/students to StudentClass'
          params do
            requires :users_ids
          end
          post :students do
            student_class = current_user.administered_student_classes.find_by(id: params[:id])

            service = execute_service('StudentClasses::AddStudentService', student_class, current_user, params)
            response_for_service(service, added_students: service_students(service, :added_students))
          end

          desc 'Remove users/students from StudentClass'
          params do
            requires :users_ids
          end
          delete :students do
            student_class = current_user.administered_student_classes.find_by(id: params[:id])

            service = execute_service('StudentClasses::RemoveStudentService', student_class, current_user, params)
            response_for_service(service, removed_students: service_students(service, :removed_students))
          end
        end

      end
    end
  end
end
