# encoding: UTF-8

module API
  module V1
    class Posts < API::V1::Base

      helpers API::Helpers::V1::PostsHelpers

      before do
        authenticate_user
      end

      namespace :posts do
        namespace :feed do
          paginated_endpoint do
            desc 'List the content feed (paginated) of current user.'
            get do
              feeds = current_user.feeds.order("created_at DESC")
              paginated_posts(feeds)
            end
          end

          namespace :featured do
            paginated_endpoint do
              desc 'List the content feed (paginated) of current user, only featured.'
              get do
                feeds = current_user.feeds.order("created_at DESC")
                paginated_posts(feeds, featured: true)
              end
            end
          end

          namespace :me do
            paginated_endpoint do
              desc 'List user posts (paginated).'
              get do
                posts = current_user.published_posts.order("created_at DESC")
                paginated_posts(posts)
              end
            end
          end
        end

        Post.publisher_types.keys.each do |key|
          namespace key do
            route_param :id do
              desc 'Create a new post and attach in a given resource (eg. users, institutions).'
              params do
                use :new_post
              end
              post do
                resource = current_user.find_publisher(type: key, id: params[:id])
                service = execute_service("Posts::CreateService", resource, current_user, params)
                response_for_create_service(service, :post)
              end

              paginated_endpoint do
                desc 'List all posts (paginated) from a given resource (eg. users, institutions).'
                get do
                  klass = Post.publisher_types[key].constantize
                  resource = klass.find_by(id: params[:id])
                  unless resource
                    return not_found_error_response(klass.to_s.underscore)
                  end
                  posts = resource.published_posts.order("created_at DESC")
                  paginated_posts(posts)
                end
              end

              namespace :featured do
                paginated_endpoint do
                  desc 'List featured posts (paginated) from a given resource.'
                  get do
                    klass = Post.publisher_types[key].constantize
                    resource = klass.find_by(id: params[:id])
                    unless resource
                      return not_found_error_response(klass.to_s.underscore)
                    end
                    posts = resource.published_posts.order("created_at DESC")
                    paginated_posts(posts, featured: true)
                  end
                end
              end

            end
          end
        end

        namespace :institutions do
          route_param :id do
            namespace :me do
              paginated_endpoint do
                desc 'List posts of current_user (paginated) from a given resource.'
                get do
                  resource = EducationalInstitution.find_by(id: params[:id])
                  unless resource
                    return not_found_error_response(klass.to_s.underscore)
                  end
                  posts = resource.published_posts.order("created_at DESC")
                  paginated_posts(posts, me: true)
                end
              end
            end
          end
        end

        desc 'Create a new post and attach in current user.'
        params do
          use :new_post
        end
        post do
          service = execute_service("Posts::CreateService", current_user, current_user, params)
          response_for_create_service(service, :post)
        end

        route_param :id do

          desc 'get post by id.'
          get do
            post = Post.find_by(id: params[:id])
            return not_found_error_response(Post.to_s.underscore) unless post
            serialized_object(post, serializer: :post, root: :post)
          end

          desc 'Update a given post.'
          params do
            requires :post, type: Hash do
              requires :content, type: String
            end
          end
          put do
            post = current_user.posts.find_by(id: params[:id])
            service = execute_service("Posts::UpdateService", post, current_user, params)
            response_for_update_service(service, :post)
          end

          desc 'Delete a given post.'
          delete do
            post = current_user.posts.find_by(id: params[:id])
            service = execute_service("Posts::DeleteService", post, current_user, params)
            response_for_delete_service(service, :post)
          end

          namespace :share do
            desc 'Share the post.'
            params do
              requires :post, type: Hash do
                optional :content, type: String
                requires :visibility_type, type: String
                optional :visibility_id, type: Integer
              end
            end
            post do
              post = Post.find_by(id: params[:id])
              service = execute_service("Posts::ShareService", post, current_user, params)
              response_for_create_service(service, :post)
            end
          end

          namespace :abuse do
            desc 'Report post as abuse'
            post do
              post = Post.find_by(id: params[:id])
              service = execute_service("Posts::AbuseService", post, current_user, params)
              simple_response_for_service(service)
            end
          end
        end
      end
    end
  end
end
