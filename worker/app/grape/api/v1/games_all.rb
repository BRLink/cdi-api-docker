# encoding: UTF-8

module API
  module V1
    class GamesAll < API::V1::Base

      mount API::V1::Games::GamesCacheable

      mount API::V1::Games::CompleteSentences

      mount API::V1::Games::GameQuestions
      mount API::V1::Games::GameQuestionsActions
      mount API::V1::Games::GameQuestionsAnswers

      mount API::V1::Games::GroupItems

      mount API::V1::Games::OpenQuestions
      mount API::V1::Games::PollsQuizzes
      mount API::V1::Games::TrueFalse
      mount API::V1::Games::RelatedItems
      mount API::V1::Games::RelatedItemsActions

      mount API::V1::Games::WordSearches
    end
  end
end
