module API
  module V1
    class MultimediaActions < API::V1::Base

      helpers API::Helpers::V1::MultimediaHelpers

      before do
        authenticate_user
      end

      namespace :multimedia do

        desc 'Uploads multimedia content'
        params do
          use :new_multimedia
        end

        post do
          service = execute_service('Multimedia::CreateService', current_user, params)
          response_for_create_service(service, :multimedia)
        end

        route_param :id do
          params do
            use :update_multimedia
          end
          desc "update multimedia"
          put do
            if params['multimedia']['link'].present? and params['multimedia']['file'].present?
              only_one_asset_allowed_response
            else
              multimedia = current_user.multimedia.find(params[:id])
              service = execute_service("Multimedia::UpdateService", multimedia, current_user, params)
              response_for_update_service(service, :multimedia)
            end
          end

          desc "delete multimedia"
          delete do
            multimedia = current_user.multimedia.find(params[:id])
            service = execute_service("Multimedia::DeleteService", multimedia, current_user, params)
            response_for_delete_service(service, :multimedia)
          end
        end
      end


    end
  end
end
