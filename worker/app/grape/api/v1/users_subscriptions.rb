# encoding: UTF-8

module API
  module V1
    class UsersSubscriptions < API::V1::Base

      before do
        authenticate_user
      end

      namespace :publishers do
        UserSubscription.publisher_types.keys.each do |key|
          namespace key do
            route_param :id do
              namespace :subscribers do
                desc 'List all subscribers (paginated) for a given resource (eg. class, institution, user, etc)'
                get do
                  klass = UserSubscription.publisher_types[key].constantize
                  resource = klass.find_by(id: params[:id])

                  unless resource
                    return not_found_error_response(klass.to_s.underscore)
                  end

                  if key == :users
                    collection = paginate(resource.subscribers_users.where.not(id: resource.id))
                  else
                    collection = paginate(resource.subscribers_users)
                  end
                  paginated_serialized_array(collection, {
                    serializer: :followed_user,
                    root: :subscribers
                  })
                end
              end
            end
          end
        end
      end

      namespace :users do
        route_param :id do
          namespace :subscriptions do
            paginated_endpoint do
              desc 'List all subscriptions of an user'
              get do
                user = User.find_by(id: params[:id])
                unless user
                  return not_found_error_response(:user)
                end
                exclude_subscription_id = user.subscriptions.find_by(publisher_id: user.id, publisher_type: 'User')
                collection = paginate(user.subscriptions.where.not(id: exclude_subscription_id))
                paginated_serialized_array(collection.includes(:publisher), {
                  serializer: :user_subscription,
                  root: :subscriptions
                })
              end

              UserSubscription.publisher_types.each do |publisher_type, publisher_class|
                desc "List all user subscriptions, kind of #{publisher_class}"
                get publisher_type do
                  user = User.find_by(id: params[:id])
                  unless user
                    return not_found_error_response(User.to_s.underscore)
                  end

                  if publisher_type == :users
                    exclude_subscription_id = user.self_user_subscription_id
                    collection = paginate(user.send("#{publisher_type}_subscribed").where.not(id: exclude_subscription_id))
                    serializer = 'followed_user'
                  else
                    collection = paginate(user.send("#{publisher_type}_subscribed"))
                    serializer = "simple_#{publisher_class.underscore}"
                  end
                  paginated_serialized_array(collection, {
                    serializer: serializer.to_sym,
                    root: :subscriptions
                  })
                end
              end
            end
          end
        end
      end
    end
  end
end
