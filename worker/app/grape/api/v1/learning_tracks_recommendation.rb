module API
  module V1
    class LearningTracksRecommendation < API::V1::Base

      helpers API::Helpers::V1::LearningTracksHelpers

      before do
        authenticate_user
      end

      namespace :learning_tracks do
        desc 'recommendation for students and staff'
        get :recommendations do
          cache_data = { user_id: current_user.id }

          respond_with_cacheable('current_user.learning_tracks_recommendations', cache_data) do
            tracks_recommendations = []

            if current_user.student?
              tracks_recommendations = current_user.student_tracks_recommendations
            else
              tracks_recommendations = current_user.multiplier_tracks_recommendations
            end

            response = Hash[learning_tracks: serialized_array(tracks_recommendations, serializer: :learning_track_recommendation)]
            if current_user.student?
              track_ids = tracks_recommendations.map(&:id)
              merge_enroll_in_response(current_user, track_ids, response)
            end

            response
          end
        end
      end
    end
  end
end
