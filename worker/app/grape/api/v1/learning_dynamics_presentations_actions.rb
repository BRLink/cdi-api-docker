# encoding: UTF-8

module API
  module V1
    class LearningDynamicsPresentationsActions < API::V1::Base

      helpers API::Helpers::V1::LearningDynamicsHelpers
      helpers API::Helpers::V1::SharedParamsHelpers
      helpers API::Helpers::V1::SharedServiceActionsHelpers

      before do
        authenticate_user
      end

      namespace :learning_dynamics do
        route_param :id do
          [:multiplier_presentation, :student_presentation].each do |presentation_type|

            namespace presentation_type do
              desc "Creates a new #{presentation_type} for dynamic"
              params do
                use :new_presentation
              end
              post do
                dynamic = current_user.learning_dynamics.find_by(id: params[:id])

                params.merge!(
                  presentation_for: dynamic,
                  presentation_for_class: LearningDynamic,
                  presentation_association_method: presentation_type,
                  presentation_type: Presentation::PRESENTATION_TYPES[:dynamic_content],
                )

                options = {
                  serializer: :simple_presentation
                }

                new_presentation_service_response(current_user, params, options)
              end

              namespace :slides do
                desc "Create a new presentation and add slide to #{presentation_type}"
                params do
                  use :new_presentation_slide
                end
                post do
                  dynamic = current_user.learning_dynamics.find_by(id: params[:id])
                  if dynamic
                    params[:slide].merge!(
                      presentation_id: dynamic.send("#{presentation_type}_id")
                    )

                    new_presentation_slide_service_response(current_user, params)
                  else
                    not_found_error_response(:learning_dynamics)
                  end
                end

                route_param :slide_id do
                  desc "Update presentation slide for #{presentation_type}"
                  params do
                    use :slide_update
                  end
                  put do
                    find_learning_dynamic_slide *find_slide_params(presentation_type) do |slide|
                      service = execute_service('PresentationSlides::UpdateService', slide, current_user, params)
                      response_for_update_service(service, :presentation_slide)
                    end
                  end

                  desc 'Delete slide'
                  delete do
                    find_learning_dynamic_slide *find_slide_params(presentation_type) do |slide|
                      service = execute_service('PresentationSlides::DeleteService', slide, current_user, params)
                      response_for_delete_service(service, :presentation_slide)
                    end
                  end

                  desc "Reorder a slide inside #{presentation_type}"
                  params do
                    use :slide_reorder
                  end
                  put :reorder do
                    find_learning_dynamic_slide *find_slide_params(presentation_type) do |slide|
                      slide_reorder_service_response(current_user, slide, params)
                    end
                  end

                  desc "Upload slide thumb for #{presentation_type}"
                  params do
                    use :new_file_upload
                  end
                  post :thumb_upload do
                    find_learning_dynamic_slide *find_slide_params(presentation_type) do |slide|
                      new_presentation_slide_thumb_upload_service_response(slide, user, params)
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
