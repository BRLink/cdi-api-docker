module API
  module V1
    module Surveys
      class Questions < API::V1::Base

        before do
          authenticate_user
        end

        namespace :surveys do
          namespace :open_questions do
            route_param :id do
              namespace :questions do
                desc 'Attach new question to survey'
                params do
                  requires :question, type: Hash do
                    requires :title, type: String
                  end
                end
                post do
                  survey = current_user.survey_open_questions.find_by(id: params[:id])
                  unless survey
                    return not_found_error_response(SurveyOpenQuestion.to_s.underscore)
                  end
                  service = execute_service('SurveyQuestions::CreateService', survey, current_user, params)
                  response_for_create_service(service, :survey_question)
                end

                route_param :question_id do
                  desc 'Retrieve a question by id'
                  get do
                    survey = SurveyOpenQuestion.find_by(id: params[:id])
                    unless survey
                      return not_found_error_response(SurveyOpenQuestion.to_s.underscore)
                    end

                    question = survey.questions.find_by(id: params[:question_id])
                    unless question
                      return not_found_error_response(SurveyQuestion.to_s.underscore)
                    end
                    serialized_object(question, serializer: :survey_question)
                  end

                  desc 'Edit survey question'
                  params do
                    requires :question, type: Hash do
                      optional :title, type: String
                    end
                  end
                  put do
                    survey = current_user.survey_open_questions.find_by(id: params[:id])
                    unless survey
                      return not_found_error_response(SurveyOpenQuestion.to_s.underscore)
                    end

                    question = survey.questions.find_by(id: params[:question_id])

                    service = execute_service('SurveyQuestions::UpdateService', question, current_user, params)
                    response_for_create_service(service, :survey_question)
                  end

                  desc 'Delete survey question'
                  delete do
                    survey = current_user.survey_open_questions.find_by(id: params[:id])
                    unless survey
                      return not_found_error_response(SurveyOpenQuestion.to_s.underscore)
                    end

                    question = survey.questions.find_by(id: params[:question_id])

                    service = execute_service('SurveyQuestions::DeleteService', question, current_user, params)
                    response_for_create_service(service, :survey_question)
                  end

                  namespace :answers do
                    desc 'List answers from a question (paginated)'
                    paginated_endpoint do
                      get do
                        question = SurveyQuestion.find_by(id: params[:question_id])
                        answers = paginate(question.answers)
                        paginated_serialized_array(answers, root: :answers, serializer: :survey_question_answer)
                      end
                    end

                    desc 'Answer a question'
                    params do
                      requires :answer, type: Hash do
                        optional :answer_body, type: String
                      end
                    end
                    post do
                      survey = SurveyOpenQuestion.find_by(id: params[:id])
                      unless survey
                        return not_found_error_response(SurveyOpenQuestion.to_s.underscore)
                      end

                      question = survey.questions.find_by(id: params[:question_id])
                      unless question
                        return not_found_error_response(SurveyQuestion.to_s)
                      end

                      service = execute_service('SurveyQuestionAnswers::CreateService', question, current_user, params)
                      response_for_create_service(service, :survey_question_answer, root: :answer)
                    end
                  end
                end
              end
            end
          end

        end
      end
    end
  end
end
