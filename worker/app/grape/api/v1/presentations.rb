# encoding: UTF-8

module API
  module V1
    class Presentations < API::V1::Base

      helpers API::Helpers::V1::PresentationsHelpers
      helpers API::Helpers::V1::SharedParamsHelpers
      helpers API::Helpers::V1::SharedServiceActionsHelpers

      before do
        authenticate_user
      end

      namespace :presentations do
        desc 'Create a new presentation for LearningTrack'
        params do
          use :new_presentation
          group :presentation, type: Hash do
            requires :learning_track_id
          end
        end

        post do
          new_learning_track_presentation_service_response(current_user, params)
        end

        route_param :id do
          namespace :slides do
            desc 'Create a new PresentationSlide for Presetation'
            params do
              use :new_presentation_slide
            end

            post do
              params[:slide].merge!(presentation_id: params[:id])
              new_presentation_slide_service_response(current_user, params)
            end

            route_param :slide_id do
              desc 'Reorder a slide inside a presentation'
              params do
                use :slide_reorder
              end
              put :reorder do
                slide = current_user.slides.find_by(id: params[:slide_id])

                slide_reorder_service_response(current_user, slide, params)
              end
            end
          end
        end
      end
    end
  end
end
