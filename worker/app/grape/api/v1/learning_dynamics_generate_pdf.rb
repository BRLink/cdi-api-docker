module API
  module V1
    class LearningDynamicsGeneratePdf < API::V1::Base
      before do
        authenticate_user
      end

      namespace :learning_dynamics do
        route_param :id do
          namespace :downloadable do

            get do
              dynamic = LearningDynamic.find_by(id: params[:id])
              if dynamic.present?
                task = dynamic.learning_dynamic_to_document_task
                if task.present?
                  task.request
                else
                  task = LearningDynamicToDocumentTask.create(learning_dynamic_id: dynamic.id)
                  unless task.valid?
                    return {
                      status: 400,
                      messages: task.errors.messages
                    }
                  end
                  task.run_new_task
                end
                task.done? ? {status: task.status, indicator: task['status'], pdf_url: task.out, ppt_url: task.out.gsub(/pdf$/,'ppt')} : {status: task.status, indicator: task['status']}
              else
                not_found_error_response(:learning_track)
              end
            end

          end
        end
      end
    end
  end
end
