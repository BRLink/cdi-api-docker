# encoding: UTF-8

module API
  module V1
    class UsersDiscussions < API::V1::Base

      helpers API::Helpers::V1::DiscussionsHelpers

      before do
        authenticate_user
      end

      namespace :discussions do
        paginated_endpoint do
          desc 'List all discussions of current_user.'
          get do
            feed = current_user.discussion_feeds

            if params[:q].present?
              feed = feed.search(params[:q])
            end

            discussions = paginate(feed)
            paginated_serialized_array(discussions, root: :discussions, serializer: :discussion)
          end
        end

        namespace :featured do
          paginated_endpoint do
            desc 'List featured discussions of current_user.'
            get do
              discussions = paginate(current_user.discussion_feeds.only_featured)
              paginated_serialized_array(discussions, root: :discussions, serializer: :discussion)
            end
          end
        end

        namespace :me do
          paginated_endpoint do
            desc 'List discussions of mine of current_user.'
            get do
              discussions = paginate(current_user.discussion_feeds.only_user(current_user))
              paginated_serialized_array(discussions, root: :discussions, serializer: :discussion)
            end
          end
        end

        get 'by_category/:category_id' do
          collection = Discussion.joins(:category_resources).where(category_resources: { category_id: params[:category_id] })
          discussions = paginate(collection)
          paginated_serialized_array(discussions, root: :discussions, serializer: :discussion)
        end
      end
    end
  end
end
