module API
  module V1
    class Channels < API::V1::Base

      helpers API::Helpers::V1::ChannelsHelpers

      before do
        authenticate_user
      end

      namespace :channels do
        namespace :message do
          post do
            channel = Channel.by_name(params[:channel_name])
            unless channel
              return not_found_error_response(:channels)
            end

            unless current_user_is_admin?(channel)
              return forbidden_error_response(:channels)
            end
            Pusher.trigger channel.name, params[:event], params[:data]
            return generic_success_response
          end
        end

        # Used by ajax to search if the current user have some channel to watch.
        desc 'Find channels for the current user'
        get do
          channels = {}
          current_user.enrolled_learning_tracks.each do |e|
            channel = Channel.find_by student_class_id: e.student_class.id, learning_track_id: e.track.id

            next unless channel.present?

            if channel.expired?
              channel.delete
            else
              channels[channel.name] = {
                student_class:  channel.student_class,
                learning_track: channel.learning_track
              }
            end
          end
          # return not_found_error_response(:channels) if channels.blank?
          return { channels: channels }
        end

        desc 'Create a channel'
        post do
          unless current_user.multiplier? or current_user.admin?
            return forbidden_error_response(:channels)
          end

          class_id, track_id = params[:student_class], params[:learning_track]

          if current_user_have_administered_class_for_this_learning_track?(class_id, track_id)
            service = execute_service("Channels::CreateService", class_id, track_id, current_user, params)
            response_for_create_service(service, :channel, include_root: false)
          else
            return not_found_error_response(:channels)
          end

        end

        desc 'Delete channel'
        delete do
          unless current_user.multiplier? or current_user.admin?
            return forbidden_error_response(:channels)
          end

          channel ||= Channel.by_name(params[:channel_name])

          if channel.present? and current_user_is_admin?(channel)
            channel.destroy
            Pusher.trigger params[:channel_name], :unsubscribe, :channel_deleted
            return generic_success_response(200, message: :deleted)
          end

          return not_found_error_response(:channels)
        end

        # Endpoint used for pusher authentication
        namespace :auth do
          desc 'Client authentitacion to the channel'
          post do
            channel = Channel.by_name params[:channel_name]

            if channel.present? and (current_user_is_enrolled?(channel) or current_user_is_admin?(channel))
              return generic_success_response if headers['X-Provider'] == 'postman'

              auth = Pusher[params[:channel_name]].authenticate(params[:socket_id])

              return generic_success_response 200, auth
            elsif channel.blank?
              return not_found_error_response(:channels)
            end

            return forbidden_error_response(:channels)
          end
        end
      end
    end
  end
end
