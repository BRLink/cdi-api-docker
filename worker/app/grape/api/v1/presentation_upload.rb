require "net/http"
require "uri"
require "json"

module API
  module V1
    class PresentationUpload < API::V1::Base

      helpers API::Helpers::V1::PresentationUploadHelpers

      before do
        authenticate_user
      end

      namespace :challenges do
        route_param :challenges_id do
          namespace :presentation do
            desc 'Upload presentation for challenge'
            namespace :upload do
              params do
                use :new_presentation
              end
              post do
                unless current_user.admin?
                  error!('User cant create')
                end
                model = Challenge.find(params[:id])
                presentation = presentation_existence_check(model)
                presentation_update(presentation: presentation)

              end
            end
          end
        end
      end

      namespace :learning_tracks do
        route_param :learning_track_id do
          namespace :presentation do
            desc 'Upload presentation in ppt/pdf for learning tracks'
            namespace :upload do
              params do
                use :new_presentation
              end
              post do
                model = LearningTrack.find(params[:learning_track_id])
                presentation = presentation_existence_check(model)
                presentation_update(presentation: presentation)
              end
            end
          end
        end
      end

      namespace :supporting_courses do
        route_param :supporting_course_id do
          namespace :presentation do
            desc 'upload presentation in ppt/pdf for supporting courses'
            namespace :upload do
              params do
                use :new_presentation
              end              
              post do
                unless current_user.staff? || current_user.superuser?
                  error!('User cant create')
                end
                model = SupportingCourse.find(params[:supporting_course_id])
                presentation = presentation_existence_check(model)
                presentation_update(presentation: presentation, slide_type: "course")
              end
            end
          end
        end
      end
    end
  end
end


