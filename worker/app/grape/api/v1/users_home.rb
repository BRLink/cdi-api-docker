# encoding: UTF-8

module API
  module V1
    class UsersHome < API::V1::Base

      before do
        authenticate_user
      end

      get :home do
        if current_user.backoffice_profile?
          presenter = presenter('home/home_for_multiplier').new(current_user)
          serialized_object(presenter, serializer: :home_for_multiplier)
        else
          presenter = presenter('home/home_for_student').new(current_user)
          serialized_object(presenter, serializer: :home_for_student)
        end
      end
    end
  end
end
