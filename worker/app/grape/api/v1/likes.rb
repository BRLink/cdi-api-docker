# encoding: UTF-8

module API
  module V1
    class Likes < API::V1::Base
      namespace :likes do
        Like.resource_types.keys.each do |key|
          namespace key do
            route_param :id do
              desc 'Add a like in a given resource (aka. post, learning_track)'
              post do
                klass = Like.resource_types[key].constantize
                resource = klass.find_by(id: params[:id])
                service = execute_service("Likes::CreateService", resource, current_user)
                simple_response_for_service service
              end

              desc 'Remove a like from a given resource (aka. post, learning_track)'
              delete do
                klass = Like.resource_types[key].constantize
                resource = klass.find_by(id: params[:id])
                service = execute_service("Likes::DeleteService", resource, current_user)
                simple_response_for_service service
              end
            end
          end
        end
      end
    end
  end
end
