module API
  module V1
    class SurveysAll < API::V1::Base

      mount API::V1::Surveys::OpenQuestions
      mount API::V1::Surveys::Polls
      mount API::V1::Surveys::Questions
      mount API::V1::Surveys::QuestionsOptions
    end
  end
end
