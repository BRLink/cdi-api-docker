# encoding: UTF-8

module API
  module V1
    class UsersMeStudent < API::V1::Base

      helpers API::Helpers::V1::UsersHelpers

      before do
        authenticate_user
      end

      namespace :learning_tracks do
        route_param :id do
          params do
            requires :class_id, type: Integer
          end

          post :enroll do
            params.merge!(learning_track_id: params[:id])

            # only tracks on user's classes scope
            student_class  = current_user.student_classes.find_by(id: params[:class_id])

            service = execute_service('Users::EnrollTrackService', current_user, student_class, params)
            response_for_create_service(service, :learning_track, serializer: :simple_learning_track)
          end
        end
      end

      #### LearningTrackReviews
      namespace :tracks do
        route_param :id do

          desc 'Create a student review for LearningTrack'
          params do
            requires :review, type: Hash do
              requires :review_type
              requires :class_id, type: Integer
              requires :score,    type: Integer
              optional :comment
            end
          end
          post :review do
            (params[:review] ||= {}).merge!(
              learning_track_id: params[:id],
              student_class_id: params[:review][:class_id]
            )

            service = execute_service('LearningTrackReviews::CreateService', current_user, params)
            response_for_create_service(service, :review, serializer: :learning_track_review)
          end

          desc 'Return user review'
          params do
            optional :only_check, default: false, type: Boolean
          end
          get :review do
            review = current_user.sent_tracks_reviews.find_by(learning_track_id: params[:id])

            if params[:only_check].present?
              reviewed = review.present?
              response_status = reviewed == true ? 200 : 404
              status response_status

              response =  {
                reviewed: reviewed,
                status: response_status
              }
            else
              if review
                serialized_object(review, serializer: :learning_track_review)
              else
                not_found_error_response(:learning_track_reviews)
              end
            end
          end

        end
      end
    end
  end
end
