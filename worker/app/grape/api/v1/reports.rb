# encoding: UTF-8

module API
  module V1
    class Reports < API::V1::Base

      helpers API::Helpers::V1::ReportsHelpers

      # before do
      #   authenticate_user
      # end

      with_cacheable_endpoints :reports do

        get :clear_all do
          if Rails.env.production?
            status 403
            return { message: 'forbidden' }
          end

          { deleted: ::LearningTrackReport.delete_all }
        end

        namespace :learning_tracks do
          route_param :id do
            desc 'Return report data for given track'
            params do
              requires :class_id, type: Integer, allow_blank: false
              optional :force_cache_clear, type: Boolean
            end
            get do
              authenticate_user

              track_id = params[:id]
              class_id = params[:class_id]
              track = current_user.find_editable_learning_track(track_id)

              if track
                meta = { force_refresh: params[:force_cache_clear], student_class_id: class_id }
                respond_with_cacheable('reports.tracks', [track_id, class_id]) do
                  serialized_object(track, serializer: :track_report, meta: meta).as_json
                end
              else
                not_found_error_response(:learning_tracks)
              end

            end
          end
        end

        namespace :student_classes do
          route_param :id do
            desc 'Return report data for given StudentClass'
            get do
              authenticate_user

              class_id = params[:id]
              student_class = current_user.administered_student_classes.find_by(id: class_id)

              if student_class
                respond_with_cacheable('reports.student_classes', [class_id]) do
                  serialized_object(student_class, serializer: :student_class_report).as_json
                end
              else
                not_found_error_response(:student_classes)
              end
            end
          end
        end
      end
    end
  end
end
