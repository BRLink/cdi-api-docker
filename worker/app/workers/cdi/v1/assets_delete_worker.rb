module CDI
  module V1
    class AssetsDeleteWorker

      include Sidekiq::Worker

      sidekiq_options :retry => 5, queue: :assets

      sidekiq_retry_in { |count| count * 60 }

      def perform(assets_urls = [], asset_conditions = {})
        # base_assets_urls = assets_urls.map { |asset_url| File.basename(asset_url) }

        assets_urls = Array.wrap(assets_urls).compact

        assets_urls.each do |asset_url|
          basename = File.basename(asset_url)

          return nil unless basename

          conditions = asset_conditions.merge(image: basename)
          asset = Photo.where(conditions).last

          if asset
            asset.destroy
          end
        end

      end
    end
  end
end
