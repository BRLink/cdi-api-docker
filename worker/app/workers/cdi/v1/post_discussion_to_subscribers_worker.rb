module CDI
  module V1
    class PostDiscussionToSubscribersWorker

      include Sidekiq::Worker

      sidekiq_options :retry => 5, queue: :publishers

      sidekiq_retry_in { |count| count * 60 }

      def perform discussion_id
        discussion = Discussion.find_by id: discussion_id
        service = ::CDI::V1::Discussion::PostDiscussionToSubscribersService.new(discussion)
        service.execute
      end
    end
  end
end
