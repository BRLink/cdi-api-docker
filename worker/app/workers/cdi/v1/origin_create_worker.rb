module CDI
  module V1
    class OriginCreateWorker

      include Sidekiq::Worker

      sidekiq_options :retry => 5, queue: :origins

      sidekiq_retry_in { |count| count * 60 }

      def perform(options)

        options = options.to_options!

        fetch_originable = options.delete(:fetch_originable)

        if fetch_originable.eql?(true)

          originable_type = options.delete(:originable_type)
          originable_id   = options.delete(:originable_id)

          originable = originable_type.to_s.constantize.send(:find_by, id: originable_id)

          originable.create_origin(options) if originable && originable.respond_to?(:create_origin)
        else
          Origin.create(options)
        end

      end
    end
  end
end
