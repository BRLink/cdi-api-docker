module CDI
  module V1
    class PostSurveyToSubscribersWorker

      include Sidekiq::Worker

      sidekiq_options :retry => 5, queue: :publishers

      sidekiq_retry_in { |count| count * 60 }

      def perform survey_type, survey_id
        survey = survey_type.constantize.find survey_id
        service = ::CDI::V1::Surveys::PostSurveyToSubscribersService.new(survey)
        service.execute
      end
    end
  end
end
