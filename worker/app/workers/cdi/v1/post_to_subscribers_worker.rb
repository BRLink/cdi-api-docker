module CDI
  module V1
    class PostToSubscribersWorker

      include Sidekiq::Worker

      sidekiq_options :retry => 5, queue: :publishers

      sidekiq_retry_in { |count| count * 60 }

      def perform(post_id)
        post = Post.find post_id
        service = ::CDI::V1::Posts::PostToSubscribersService.new(post)
        service.execute
      end
    end
  end
end
