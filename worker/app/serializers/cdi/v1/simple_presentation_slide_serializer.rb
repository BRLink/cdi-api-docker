module CDI
  module V1
    class SimplePresentationSlideSerializer < ActiveModel::Serializer
      root false

      attributes :id, :presentation_id, :title, :layout_type, :slide_type,
                 :content, :metadata, :hidden,
                 :multiplier_thumbnail_url, :student_thumbnail_url,
                 :thumbnail_url, :views_count, :position,
                 :associated_resource_type, :associated_resource_id,
                 :created_at, :updated_at

      def content
        parse_json(object.content)
      end

      def metadata
        metadata = parse_json(object.metadata || {})

        if %w(staging development).member?(Rails.env.to_s)
          metadata['thumb_url'] ||= "https://placeholdit.imgix.net/~text?txtsize=22&txt=#{object.position}-#{object.slide_type}&w=150&h=150"
        end

        metadata
      end

      private
      def parse_json(string)
        JSON.parse(string) rescue string
      end

    end
  end
end
