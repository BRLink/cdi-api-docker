module CDI
  module V1
    class SimpleGameQuizSerializer < ActiveModel::Serializer

      root false

      attributes :id, :status, :status_changed_at,
                 :user_id, :image_url,
                 :created_at, :updated_at
    end
  end
end
