module CDI
  module V1
    class GamePollSerializer < SimpleGamePollSerializer

      has_one :question, serializer: GamePollQuestionSerializer

    end
  end
end
