module CDI
  module V1
    class MultiplierSingleStudentDeliverySerializer < SimpleStudentDeliverySerializer

      has_one :learning_delivery, serializer: SimpleLearningDeliverySerializer

      has_one :review, serializer: SimpleStudentDeliveryReviewSerializer

      has_many :students, each_serializer: SimpleUserSerializer

    end
  end
end
