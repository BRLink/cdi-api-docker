module CDI
  module V1
    class NotificationSerializer < SimpleNotificationSerializer
      attributes :metadata, :route

      has_one :sender_user
      has_one :notificable, polimorphic: :true, prefix: 'Simple'
    end
  end
end
