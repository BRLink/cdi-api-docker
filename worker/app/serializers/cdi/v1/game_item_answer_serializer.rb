module CDI
  module V1
    class GameItemAnswerSerializer < SimpleGameItemAnswerSerializer

      has_one :game_item, serializer: SimpleGameItemSerializer
      has_one :user, serializer: SimpleUserSerializer

    end
  end
end
