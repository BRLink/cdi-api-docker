module CDI
  module V1
    class SimpleTrackingEventSerializer < ActiveModel::Serializer

      root false

      attributes :id, :enrolled_learning_track_id, :presentation_slide_id,
                 :event_type, :created_at, :updated_at
    end
  end
end
