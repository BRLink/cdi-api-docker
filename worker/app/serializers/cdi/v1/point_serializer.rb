module CDI
  module V1
    class PointSerializer < SimplePointSerializer

      has_one :educational_institution, serializer: SimpleEducationalInstitutionSerializer

    end
  end
end

