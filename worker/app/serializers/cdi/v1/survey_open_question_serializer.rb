module CDI
  module V1
    class SurveyOpenQuestionSerializer < SimpleSurveyOpenQuestionSerializer

      has_one :user, serializer: SimpleUserSerializer

      has_many :questions, serializer: SurveyQuestionAnsweredSerializer

      has_one :visibility, serializer: SimpleVisibilitySerializer

      def visibility
        object.visibilities.first
      end
    end
  end
end
