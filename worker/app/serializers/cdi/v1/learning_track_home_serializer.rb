module CDI
  module V1
    class LearningTrackHomeSerializer < SimpleLearningTrackSerializer

      has_one :user, serializer: SimpleUserSerializer

      has_many :categories, serializer: SimpleCategorySerializer
    end
  end
end
