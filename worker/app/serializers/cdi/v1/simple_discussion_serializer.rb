module CDI
  module V1
    class SimpleDiscussionSerializer < ActiveModel::Serializer

      root false

      attributes :id,
                 :title,
                 :content,
                 :user_id,
                 :video_link,
                 :image_url,
                 :cover_url,
                 :created_at,
                 :updated_at,
                 :tags,
                 :discussion_type
    end
  end
end
