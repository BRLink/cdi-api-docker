module CDI
  module V1
    class LearningCycleSerializer < SimpleLearningCycleSerializer

      has_one :user, serializer: SimpleUserSerializer

      has_many :categories, serializer: SimpleCategorySerializer

    end
  end
end
