module CDI
  module V1
    module SerializerConcerns
      module LikeableSerializer

        extend ActiveSupport::Concern

        included do
          attributes :total_likes,
                     :liked
        end

        def liked
          scope && scope.liked?(object)
        end
      end
    end
  end
end
