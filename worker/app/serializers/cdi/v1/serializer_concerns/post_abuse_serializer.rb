module CDI
  module V1
    module SerializerConcerns
      module PostAbuseSerializer

        extend ActiveSupport::Concern

        included do
          attributes :total_abuses,
                     :abused
        end

        def abused
          scope && scope.post_abuses.where(post: object).exists?
        end
      end
    end
  end
end
