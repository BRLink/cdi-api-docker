module CDI
  module V1
    class StudentDeliverySerializer < SimpleStudentDeliverySerializer

      has_one :learning_delivery, serializer: SimpleLearningDeliverySerializer

      has_one :review, serializer: SimpleStudentDeliveryReviewSerializer

      has_one :learning_track,
              serializer: SimpleLearningTrackSerializer,
              meta: {
                include_cover_image_url: false
              }

      has_one :student_class, serializer: SimpleStudentClassSerializer
      has_one :user, serializer: SimpleUserSerializer

      has_many :students, each_serializer: SimpleUserSerializer

    end
  end
end
