module CDI
  module V1
    class DiscussionOptionAnsweredSerializer < SimpleDiscussionOptionSerializer

      attributes :answers_count

      has_many :users, serializer: SimpleUserSerializer

      def answers_count
        object.answers.where(option_id: object.id).count
      end

      def users
        object.answers_users.limit(3)
      end
    end
  end
end
