module CDI
  module V1
    class SimpleGameQuestionAnswerSerializer < ActiveModel::Serializer

      root false

      attributes :id, :game_question_id, :user_id, :answer_body, :option_id, :correct?,
                 :created_at, :updated_at, :corrected_at


      def filter(keys)
        blacklist_keys = object.option_id.present? ? [:answer_body] : [:option_id, :correct_options_ids]

        keys - blacklist_keys
      end
    end
  end
end
