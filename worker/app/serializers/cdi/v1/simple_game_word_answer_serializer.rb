module CDI
  module V1
    class SimpleGameWordAnswerSerializer < ActiveModel::Serializer

      root false

      attributes :id, :user_id, :words, :game_id, :game_type, :correct?,
                 :created_at, :updated_at

    end
  end
end
