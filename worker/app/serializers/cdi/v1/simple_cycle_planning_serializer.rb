module CDI
  module V1
    class SimpleCyclePlanningSerializer < ActiveModel::Serializer

      root false
      attributes :id, :learning_cycle_id, :created_at, :updated_at

    end
  end
end
