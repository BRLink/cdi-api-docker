module CDI
  module V1
    class ChatRoomSerializer < SimpleChatRoomSerializer
      has_many :all_users, serializer: SimpleUserSerializer

      attributes :pending_messages_count, :last_message

      def pending_messages_count
        if viewer_id.present?
          return object.my_unread_messages(viewer_id).size
        else
          return '-'
        end
      end

      def filter(keys)
        if viewer_id.present?
          keys
        else
          keys - [:pending_messages_count]
        end
      end

      private
      def viewer_id
        @viewer_id ||= meta_value(:current_user_id)
      end
      def meta_value(key)
        return nil if @meta.blank? || !@meta.is_a?(Hash)
        @meta[key]
      end
    end
  end
end
