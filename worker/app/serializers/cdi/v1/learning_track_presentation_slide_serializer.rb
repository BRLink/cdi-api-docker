module CDI
  module V1
    class LearningTrackPresentationSlideSerializer < SimplePresentationSlideSerializer

      has_one :associated_resource, serializer: SimpleAssociatedResourceSerializer

    end
  end
end
