module CDI
  module V1
    class SimpleUserGalleryAssetSerializer < ActiveModel::Serializer

      root false

      attributes :id, :album_id, :user_id,
                 :content_type, :file_size,
                 :visible, :position, :caption,
                 :tags, :versions,
                 :created_at, :updated_at

      METADATA_ATTRIBUTES = [
        :width, :height,
        :size, :mime_type,
        :type, :resolution
      ]

      def versions
        metadata_attributes = (self.meta && self.meta[:attributes_whitelist] || METADATA_ATTRIBUTES)
        object.images_url_with_metadata(metadata_attributes)
      end

    end
  end
end
