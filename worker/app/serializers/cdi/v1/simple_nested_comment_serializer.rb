module CDI
  module V1

    # Utilizado para a listagem de Comentários (Pai)
    # onde é necessário o embed dos users no linked_data.
    # Por esta razão, apenas o user_id é serializado.
    # Também serve como classe pai para os demais nested_comments_serializer
    class SimpleNestedCommentSerializer < ActiveModel::Serializer

      root false

      attributes :id,
                 :content,
                 :user_id,
                 :created_at

      include CDI::V1::SerializerConcerns::LikeableSerializer
    end
  end
end
