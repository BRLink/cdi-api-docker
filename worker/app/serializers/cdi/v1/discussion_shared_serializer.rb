module CDI
  module V1
    class DiscussionSharedSerializer < SimpleDiscussionSerializer

      attributes :answered, :answered_option_id, :answers_count

      has_many :options, serializer: DiscussionOptionAnsweredSerializer

      has_one :user, serializer: SimpleUserSerializer

      def answered
        @answered ||= scope && object.question.answers.exists?(user_id: scope.id)
      end

      def answered_option_id
        return nil unless answered
        object.question.answers.find_by(user_id: scope.id).option_id
      end

      def answers_count
        object.question.answers.count
      end
    end
  end
end
