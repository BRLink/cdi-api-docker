module CDI
  module V1
    class GameQuizQuestionSerializer < SimpleGameQuestionSerializer

      has_many :options, serializer: SimpleGameQuestionOptionSerializer

    end
  end
end
