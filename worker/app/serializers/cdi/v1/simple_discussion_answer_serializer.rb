module CDI
  module V1
    class SimpleDiscussionAnswerSerializer < ActiveModel::Serializer

      root false

      attributes :id, :user_id, :option_id, :created_at

    end
  end
end
