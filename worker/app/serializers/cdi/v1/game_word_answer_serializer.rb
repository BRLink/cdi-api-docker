module CDI
  module V1
    class GameWordAnswerSerializer < SimpleGameWordAnswerSerializer

      has_one :game,
              serializer: SimpleGameSerializer,
              meta: {
                force_return_words: true
              }

      has_one :user, serializer: SimpleUserSerializer

      attributes :correct_words, :wrong_words
    end
  end
end
