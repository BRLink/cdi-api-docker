module CDI
  module V1
    class CyclePlanningSerializer < SimpleCyclePlanningSerializer

      has_one :learning_cycle, serializer: SimpleLearningCycleSerializer

      has_many :questions, serializer: CyclePlanningQuestionSerializer

      def questions
        object.questions.includes(:answers, :question_template)
      end

    end
  end
end
