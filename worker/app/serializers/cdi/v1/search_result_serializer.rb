module CDI
  module V1
    class SearchResultSerializer < ActiveModel::Serializer

      root false

      attributes :id, :type, :type_key, :name, :image, :username, :profile_type

      has_one :learning_track, serializer: SimpleLearningTrackSerializer
      has_one :user, serializer: SimpleUserSerializer
      has_one :blog_article, serializer: SimpleBlogArticleSerializer
      has_one :educational_institution, serializer: SimpleEducationalInstitutionSerializer
      has_one :challenge, serializer: SimpleChallengeSerializer
      has_one :multimedia, serializer: SimpleMultimediaSerializer
      has_one :supporting_course, serializer: SimpleSupportingCourseSerializer
      has_one :learning_dynamic, serializer: SimpleLearningDynamicSerializer
      has_one :discussion, serializer: SimpleDiscussionSerializer
      has_one :post, serializer: SimplePostSerializer

      def id
        object.searchable_id
      end

      def type
        object.searchable_type
      end

      def type_key
        @type_key ||= object.searchable.class.to_s.underscore.to_sym
      end

      ## Supporting :name
      #  - User
      #  - Multimedia
      #  - LearningTrack
      #  - EducationalInstitution
      ##
      ## Supporting :title
      #  - BlogArticle
      #  - EducationalInstitution
      #  - SupportingCourse
      #  - LearningDynamic
      ##
      ## Supporting :content
      #  - Post
      def name
        searchable = object.searchable
        [:name, :title, :content].each do |method|
          value = searchable.try(method)
          return value if value
        end

        nil
      end

      def image
        searchable = object.searchable
        [:profile_image_url, :thumb_url, :cover_image_url].each do |method|
          value = searchable.try(method)
          return value if value
        end
        nil
      end

      def multimedia_type
        object.searchable.try(:multimedia_type)
      end

      def username
        object.searchable.try(:username)
      end

      def profile_type
        object.searchable.try(:profile_type)
      end

      def filter keys
        blacklisted_keys = [
          :learning_track,
          :user,
          :blog_article,
          :educational_institution,
          :challenge,
          :multimedia,
          :supporting_course,
          :learning_dynamic,
          :discussion,
          :post
        ].select { |item| item != type_key }

        keys - blacklisted_keys
      end
    end
  end
end
