module CDI
  module V1
    class SimpleLikeSerializer < ActiveModel::Serializer

      root false

      attributes :id,
                 :created_at,
                 :resource_type,
                 :resource_id

    end
  end
end

