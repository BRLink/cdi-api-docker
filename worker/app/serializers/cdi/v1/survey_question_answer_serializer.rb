module CDI
  module V1
    class SurveyQuestionAnswerSerializer < SimpleSurveyQuestionAnswerSerializer

      has_one :user,
              serializer: SimpleUserSerializer
    end
  end
end
