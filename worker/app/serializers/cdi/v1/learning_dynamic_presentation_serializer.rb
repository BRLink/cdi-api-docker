module CDI
  module V1
    class LearningDynamicPresentationSerializer < SimplePresentationSerializer

      has_many :slides, serializer: SimplePresentationSlideSerializer

    end
  end
end
