module CDI
  module V1
    class LibrarySerializer < ActiveModel::Serializer
        root false
        attributes :type, :content

        def type
          object.class.name
        end
        def content
          "CDI::V1::Lib#{object.class.to_s}Serializer".constantize.new(object)
        end
    end
  end
end
