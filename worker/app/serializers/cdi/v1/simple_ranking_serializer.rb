module CDI
  module V1
    class SimpleRankingSerializer < ActiveModel::Serializer

      root false
      attributes :user_id,
                 :position,
                 :default_score

    end
  end
end
