module CDI
  module V1
    class SimpleGameTrueFalseSerializer < ActiveModel::Serializer

      root false

      attributes :id, :status, :status_changed_at, :user_id,
                 :questions_count, :created_at, :updated_at
    end
  end
end
