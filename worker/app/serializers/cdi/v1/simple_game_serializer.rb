module CDI
  module V1
    class SimpleGameSerializer < ActiveModel::Serializer

      VALID_GAME_TYPES = [
        GamePoll,
        GameQuiz,
        GameCompleteSentence,
        GameWordSearch,
        GameOpenQuestion,
        GameTrueFalse,
        GameGroupItem,
        GameGroup
      ]

      root false

      def attributes
        attrs = {}

        if VALID_GAME_TYPES.member?(object.class)
          attrs = "::CDI::V1::Simple#{object.class}Serializer".constantize.new(object, scope: @scope, meta: @meta).as_json
        else
          attrs = { message: "Game(#{object.class}) not included in the list (SimpleGameSerializer)" }
        end

        attrs
      end
    end
  end
end
