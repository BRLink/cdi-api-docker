module CDI
  module V1
    class SimpleGameItemAnswerSerializer < ActiveModel::Serializer

      root false

      attributes :id, :game_item_id, :user_id, :correct, :reply_word,
                 :created_at, :updated_at

    end
  end
end
