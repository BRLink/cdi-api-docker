module CDI
  module V1
    class SimpleGameQuestionWithPercentageSerializer < SimpleGameQuestionSerializer

      root false

      attributes :id, :questionable_id, :questionable_type,
                 :title, :input_type, :position, :allow_multiples_answers,
                 :options_answers_percent, :created_at, :updated_at

      def options_answers_percent
        return [] unless self.object.questionable_type == "GamePoll"
        self.object.options_answers_percent
      end

    end
  end
end
