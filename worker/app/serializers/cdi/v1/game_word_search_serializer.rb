module CDI
  module V1
    class GameWordSearchSerializer < SimpleGameWordSearchSerializer

      has_one :user, serializer: SimpleUserSerializer

    end
  end
end
