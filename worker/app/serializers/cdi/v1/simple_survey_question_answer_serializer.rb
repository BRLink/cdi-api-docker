module CDI
  module V1
    class SimpleSurveyQuestionAnswerSerializer < ActiveModel::Serializer

      root false

      attributes :id, :user_id, :answer_body, :option_id, :created_at

      def filter(keys)
        ## FIXME filter keys
        blacklisted_keys = object.open_question_answer? ? [:option_id, :option, :options] : []
        keys - blacklisted_keys
      end
    end
  end
end
