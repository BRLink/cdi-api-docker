module CDI
  module V1
    class SimpleQuestionSerializer < ActiveModel::Serializer

      root false

      attributes :id,
                 :questionable_id, :questionable_type,
                 :template_questionable_id, :template_questionable_type,
                 :question_template_id


      # keep compatibility
      [:questionable_id, :questionable_type].each do |method|
        define_method method do
          self.send "template_#{method}"
        end
      end

    end
  end
end
