module CDI
  module V1
    class TrackingEventSerializer < SimpleTrackingEventSerializer

      has_one :user, serializer: SimpleUserSerializer

      has_one :learning_track, serializer: SimpleLearningTrackSerializer

      has_one :student_class, serializer: SimpleStudentClassSerializer

      has_one :slide, serializer: SimplePresentationSlideSerializer

      def slide
        object.try(:presentation_slide)
      end

    end
  end
end
