module CDI
  module V1
    class SimpleVisibilitySerializer < ActiveModel::Serializer

      root false

      attributes :type, :id, :name

      def type
        object.visibility_type
      end

      def id
        object.visibility_id
      end

      def filter(keys)
        blacklisted_keys = object.preset_visibility? ? [:id, :name] : []
        keys - blacklisted_keys
      end
    end
  end
end
