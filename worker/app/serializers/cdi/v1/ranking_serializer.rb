module CDI
  module V1
    class RankingSerializer < SimpleRankingSerializer

      has_one :user, serializer: SimpleUserSerializer

    end
  end
end

