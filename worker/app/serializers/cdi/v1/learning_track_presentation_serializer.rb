module CDI
  module V1
    class LearningTrackPresentationSerializer < SimplePresentationSerializer

      has_one :learning_track, serializer: SimpleLearningTrackSerializer

      has_one :user, serializer: SimpleUserSerializer

      has_many :slides, serializer: LearningTrackPresentationSlideSerializer

    end
  end
end
