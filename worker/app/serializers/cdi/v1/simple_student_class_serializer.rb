module CDI
  module V1
    class SimpleStudentClassSerializer < ActiveModel::Serializer

      root false

      attributes :id, :name, :admin_user_id,
                 :code, :code_expiration_date, :educational_institution_id,
                 :code_expired?,:status, :status_changed_at,
                 :created_at, :updated_at

    end
  end
end
