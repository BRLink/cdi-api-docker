module CDI
  module V1
    class GameTrueFalseSerializer < SimpleGameTrueFalseSerializer

      has_one :user, serializer: SimpleUserSerializer

      has_many :questions, serializer: GameQuestionSerializer

    end
  end
end
