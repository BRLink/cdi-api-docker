module CDI
  module V1
    ## FIXME: remover esta classe. Deixar apenas a UserSubscriptionSerializer.
    class SimpleUserSubscriptionSerializer < ActiveModel::Serializer
      root false
      attributes :created_at, :publisher_id, :publisher_type, :publisher_name
    end
  end
end
