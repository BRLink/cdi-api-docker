module CDI
  module V1
    module PageStructure
      class ModelSerializer::CategorySerializer < ActiveModel::Serializer
        attributes :id, :name, :position
      end
    end
  end
end