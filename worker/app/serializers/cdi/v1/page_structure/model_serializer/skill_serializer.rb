module CDI
  module V1
    module PageStructure
      class ModelSerializer::SkillSerializer < ActiveModel::Serializer
        attributes :id, :name, :description, :position
      end
    end
  end
end
