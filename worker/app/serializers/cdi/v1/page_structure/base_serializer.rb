module CDI
  module V1
    module PageStructure
      class BaseSerializer < ActiveModel::Serializer

        root false

        attributes :page_structure_name

        def page_structure_name
          self.class.name.to_s.underscore.split('/').last.sub('_page_serializer', '')
        end
      end
    end
  end
end