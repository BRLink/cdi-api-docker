module CDI
  module V1
    class UserFeedLearningCycleSerializer < SimpleLearningCycleSerializer

      has_many :learning_tracks,
              serializer: UserFeedLearningTrackSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      has_many :student_classes,
              serializer: SimpleStudentClassSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      has_one :user,
              serializer: SimpleUserSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      has_many :categories,
              serializer: SimpleCategorySerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      has_many :tracks_categories,
              serializer: SimpleCategorySerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      def learning_tracks
        tracks = current_learning_tracks
        tracks.select {|track| visible_for_scope_user?(track) }
      end

      def visible_for_scope_user?(track)
        return true if scope.backoffice_profile?

        (scope_user_student_class_ids & track.student_class_ids).any?
      end

      def scope_user_student_class_ids
        @_user_student_class_ids ||= ( meta_value?(:student_classes_ids) ?
                                       @meta[:student_classes_ids] :
                                       scope.student_class_ids
                                     ).map(&:to_i)
      end

      def scope_user_learning_tracks_ids(method = :learning_tracks_ids)
        @_user_track_ids ||= {}
        @_user_track_ids[method] ||= ( meta_value?(:learning_tracks_ids) ?
                                       @meta[:learning_tracks_ids] :
                                       scope.send(method)
                                     ).map(&:to_i)
      end

      def current_learning_tracks
        return moderate_tracks_for_user if meta_value?(:only_moderate_tracks)

        # filter
        return categories_tracks if meta_value?(:categories_ids)

        # student
        return published_tracks if only_published?

        # for multipliers
        return all_tracks
      end

      private
      def meta_value?(key)
        return false if @meta.blank? || !@meta.is_a?(Hash)
        @meta[key].present?
      end

      def only_published?
        meta_value?(:only_published_learning_tracks)
      end

      def student_class_published_tracks
        # make sure to add: cycles.includes(:student_class_published_tracks)
        @_student_class_published_tracks ||= object.student_class_published_tracks
      end

      def moderate_tracks_for_user
        @moderate_track_ids ||= scope_user_learning_tracks_ids(:moderate_learning_track_ids)
        # in this case is better to use ruby select to select tracks from cycle
        # to avoid N+1 query
        @user_moderate_tracks ||= object.learning_tracks.to_a.select do |track|
          @moderate_track_ids.member?(track.id)
        end
      end

      def categories_tracks
        @_category_tracks ||= (only_published? ? published_tracks : all_tracks).to_a.select do |track|
          track_categories_ids.any? { |_id| track.category_ids.include?(_id) }
        end
      end

      def track_categories_ids
        @_track_categories_ids ||= (meta_value?(:categories_ids) ? @meta[:categories_ids] : []).map(&:to_i)
      end

      def published_tracks
        # make sure to add: cycles.includes(:published_learning_tracks)
        @_published_tracks ||= object.published_learning_tracks
      end

      def all_tracks
        @_all_tracks ||= object.learning_tracks.uniq
      end

      def student_classes
        return object.student_classes.uniq if scope.backoffice_profile?

        object.student_classes.uniq.select do |_class|
          scope_user_student_class_ids.member?(_class.id)
        end
      end
    end
  end
end
