module CDI
  module V1
    class LearningTrackSerializer < SimpleLearningTrackSerializer

      has_one :learning_cycle, serializer: SimpleLearningCycleSerializer

      has_one :user, serializer: SimpleUserSerializer

      has_many :categories, serializer: SimpleCategorySerializer

      has_many :skills, serializer: SimpleSkillSerializer

      attributes :is_author

      def is_author
        return self.user.id == scope.id
      end

      include CDI::V1::SerializerConcerns::CommentableSerializer
      include CDI::V1::SerializerConcerns::LikeableSerializer

    end
  end
end
