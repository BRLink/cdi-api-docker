module CDI
  module V1
    class SimpleUserSerializer < ActiveModel::Serializer

      root false

      attributes :id,
                 :first_name,
                 :last_name,
                 :profile_type,
                 :username,
                 :fullname,
                 :gender,
                 :profile_image_url,
                 :profile_images,
                 :updated_at
    end
  end
end
