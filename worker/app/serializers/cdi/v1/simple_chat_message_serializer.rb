module CDI
  module V1
    class SimpleChatMessageSerializer < ActiveModel::Serializer

      root false
      attributes :id, :user_id, :chat_room_id, :content, :created_at, :updated_at
    end
  end
end
