module CDI
  module V1
    class SimplePostSerializer < ActiveModel::Serializer

      root false

      attributes :id,
                 :created_at,
                 :content

      has_many :categories,
                serializer: SimpleCategorySerializer,
                embed: :ids,
                embed_in_root_key: :linked_data,
                embed_in_root: true

      has_one :user,
               serializer: SimpleUserSerializer,
               embed: :ids,
               embed_in_root_key: :linked_data,
               embed_in_root: true

      attributes :publisher_type
      has_one :publisher,
              polimorphic: :true,
              prefix: 'Simple'

      has_one :visibility, serializer: SimpleVisibilitySerializer

      has_many :discussions, serializer: SimpleDiscussionSerializer
      has_many :photos, serializer: PhotoSerializer
      has_many :multimedias, serializer: SimpleMultimediaSerializer
      has_many :albums, serializer: PhotosAlbumSerializer
      has_one :parent, serializer: ParentPostSerializer

      has_one :challenge, serializer: SimpleChallengeSerializer

      include CDI::V1::SerializerConcerns::CommentableSerializer
      include CDI::V1::SerializerConcerns::LikeableSerializer
      include CDI::V1::SerializerConcerns::ResourceFeaturedSerializer
      include CDI::V1::SerializerConcerns::PostAbuseSerializer

      def visibility
        object.visibilities.first
      end

      def challenge
        object.challenges.first
      end
    end
  end
end
