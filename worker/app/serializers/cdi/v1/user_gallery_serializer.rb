module CDI
  module V1
    class UserGallerySerializer < SimpleUserGallerySerializer

      has_many :assets, serializer: SimpleUserGalleryAssetSerializer

      def assets
        object.photos
      end

    end
  end
end
