module CDI
  module V1
    class UserLearningTrackSerializer < SimpleLearningTrackSerializer

      has_one :learning_cycle,
              serializer: SimpleLearningCycleSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      has_one :user,
              serializer: SimpleUserSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      has_many :categories,
              serializer: SimpleCategorySerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      has_many :skills,
              serializer: SimpleSkillSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

    end
  end
end
