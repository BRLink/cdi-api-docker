module CDI
  module V1
    class LibraryLearningDynamicSerializer < SimpleLearningDynamicSerializer
      has_one :user,
              serializer: SimpleUserSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      has_many :categories,
              serializer: SimpleCategorySerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      has_many :skills,
               serializer: SimpleSkillSerializer,
               embed: :ids,
               embed_in_root_key: :linked_data,
               embed_in_root: true

      has_one :learning_delivery,
               serializer: SimpleLearningDeliverySerializer

    end
  end
end
