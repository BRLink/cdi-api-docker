class UsersMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.users_mailer.welcome.subject
  #
  def welcome(user)
    @user = user
    localize = I18n.t 'welcome', scope: 'users_mailer'

    @greeting = localize[:greeting] % {
      username: @user.name
    }

    localize.except(:greeting).each do |key, value|
      instance_variable_set("@#{key.to_s}", localize[key])
    end

    mail to: user.email, subject: localize[:subject]
  end

  def password_recovery(user)
    @user = user
    localize = I18n.t 'password_recovery', scope: 'users_mailer'

    @intro_title = localize[:intro_title] % {
      username: @user.name
    }

    localize.except(:intro_title).each do |key, value|
      instance_variable_set("@#{key.to_s}", localize[key])
    end

    mail to: user.email, subject: localize[:subject]
  end

  def password_updated(user)
    @user = user
    localize = I18n.t 'password_updated', scope: 'users_mailer'

    @intro_title = localize[:intro_title] % {
      username: @user.name
    }

    localize.except(:intro_title).each do |key, value|
      instance_variable_set("@#{key.to_s}", localize[key])
    end

    mail to: user.email, subject: localize[:subject]
  end

  def notification(record)
    @user    = record.receiver_user
    localize = I18n.t record.notification_type, scope: 'cdi.notifications'

    # challenge notifications have a title field
    title = record.notificable.is_a?(Challenge) ? localize[:notification_title] % {challenge_title: record.notificable.title} : localize[:notification_title]

    @message = localize[:message] % {
      sender_user_name: record.sender_user.name,
      notification_title: title
    }

    @route = localize[:route] % record.notificable.as_json.symbolize_keys

    mail to: @user.email, subject: localize[:subject]
  end

  def achievement_notification(user_achievement)
    @user    = user_achievement.user
    localize = I18n.t :achievement_granted, scope: 'cdi.notifications'
    subject = localize[:subject]
    localize = localize[:achievement][user_achievement.achievement.action.to_sym]

    @message = localize[:message] % {
      notification_title: localize[:notification_title]
    }
    @route = localize[:route] % {
      #notifiable_path: localize[:notifiable_path],
      id: user_achievement.resource.id
    }

    mail to: @user.email, subject: subject, template_name: "notification"
  end

  # def challenge_notification(user, sender, challenge)
  #   @user      = user
  #   @sender    = sender
  #   @challenge = challenge
  #   localize   = I18n.t 'challenge', scope: 'cdi.notifications'

  #   @message = localize[:message] % {
  #     receiver_user_name: @user.name,
  #     sender_user_name:   @sender.name,
  #     notificable_title:  @challenge.title
  #   }

  #   mail to: user.email, template_name: 'notification', subject: localize[:subject]
  # end
end
