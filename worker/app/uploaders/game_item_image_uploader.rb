class GameItemImageUploader < BaseImageUploader

  include Piet::CarrierWaveExtension

  VERSIONS = {
    thumb: {
      size: [320, 320]
    },
    mini_thumb: {
      size: [60, 60]
    }
  }

  DEFAULT_JPG_QUALITY = 85
  DEFAULT_PNG_QUALITY = 5

  if CDI::Config.enabled?(:compress_images_on_upload)
    process :optimize => [ { quality: DEFAULT_JPG_QUALITY, level: DEFAULT_PNG_QUALITY } ]
  end

  process resize_to_fill: [640, 640]

  VERSIONS.each do |image_version, options|
    version image_version do
      process resize_to_fit: options[:size]

      if options[:process].present? && CDI::Config.enabled?(:compress_images_on_upload)
        process optimize: [ { quality: DEFAULT_JPG_QUALITY, level: DEFAULT_PNG_QUALITY } ]
      end

    end
  end

  def filename
    unique_filename
  end

  def unique_filename
    "#{secure_token}.#{file.extension}" if original_filename.present?
  end

  private
    def secure_token
      var = :"@#{mounted_as}_secure_token"
      model.instance_variable_get(var) or model.instance_variable_set(var, SecureRandom.uuid)
    end
end
