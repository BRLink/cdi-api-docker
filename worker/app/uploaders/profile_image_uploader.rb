class ProfileImageUploader < BaseImageUploader

  include Piet::CarrierWaveExtension

  include ::CarrierWave::Backgrounder::Delay unless Rails.env.development?

  VERSIONS = {
    medium: {
      size: [400, 400],
      process: true
    },
    thumb: {
      size: [200, 200]
    },
    mini_thumb: {
      size: [60, 60]
    }
  }

  DEFAULT_JPG_QUALITY = 85
  DEFAULT_PNG_QUALITY = 5

  process crop: [:profile_image]

  if CDI::Config.enabled?(:compress_images_on_upload)
    process :optimize => [ { quality: DEFAULT_JPG_QUALITY, level: DEFAULT_PNG_QUALITY } ]
  end


  VERSIONS.each do |image_version, options|
    version image_version do
      process resize_to_fit: options[:size]

      if options[:process].present? && CDI::Config.enabled?(:compress_images_on_upload)
        process optimize: [ { quality: DEFAULT_JPG_QUALITY, level: DEFAULT_PNG_QUALITY } ]
      end

    end
  end

  def store_dir
    "uploads/#{Rails.env.to_s}/images/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def filename
    if original_filename
      "#{token}.#{file.extension}"
    end
  end

  def token
    var = :"@#{mounted_as}_token"
    model.instance_variable_get(var) || model.instance_variable_set(var,
                                          Digest::MD5.hexdigest(File.dirname(current_path)))
  end
end
