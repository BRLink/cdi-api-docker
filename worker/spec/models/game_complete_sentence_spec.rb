require 'rails_helper'

RSpec.describe GameCompleteSentence, type: :model do
  before(:each) do
    @game = create(:game_complete_sentence)
  end

  def sample_data
    words = ['Napoleão Bonaparte', 'francês', '1804', '1814']
    sentence = '%s foi imperador %s, entre os periodos de %s à %s'

    { words: words, sentence: sentence }
  end

  it 'must be valid' do
    game = create(:game_complete_sentence)
    expect(game).to be_valid
  end

  it 'must have column indexes' do
    expect(subject).to have_db_index(:status)
    expect(subject).to have_db_index(:words)
    expect(subject).to have_db_index(:user_id)
  end

  it 'must belongs to user' do
    expect(@game).to belong_to(:user)
  end

  it 'must complete the sentence' do
    full_sentence = 'Napoleão Bonaparte foi imperador francês, entre os periodos de 1804 à 1814'

    game = create(:game_complete_sentence, sample_data)

    expect(game.full_sentence).to eq(full_sentence)
  end

  it 'must ignore accents in words match' do
    game = create(:game_complete_sentence, sample_data)

    input_words = ['napoleao bonaparte', 'frances', '1804', '1814']

    expect(game.words_match?(input_words)).to be true
  end

  it 'must consider wrong words match if position dont match' do
    game = create(:game_complete_sentence, sample_data)

    input_words = ['frances', 'napoleao bonaparte', '1804', '1814']

    expect(game.words_match?(input_words)).to be false
  end

  it 'must validate the words count' do
    game = create(:game_complete_sentence, sample_data)

    input_words = ['frances', 'napoleao bonaparte', '1804']

    expect(game.words_match?(input_words)).to be false

    expect(game.correct_words(input_words).size).to be 3
    expect(game.wrong_words(input_words).size).to be 0

  end

  it 'must validate and returns the correct words by position' do
    data = sample_data
    words = data[:words]
    game = create(:game_complete_sentence, data)

    expect(game.correct_words(words)).to be == words
    expect(game.correct_words_by_position(words)).to be == words

    expect(game.wrong_words(words)).to be == []

    # changing words position
    words[0] = 'Teste'

    # the last word is not valid not
    expect(game.correct_words_by_position(words)).to be == %w(francês 1804 1814)
    expect(game.correct_words(words).size).to be == 3
    expect(game.wrong_words(words)) == ['Teste']
  end

  it 'must validate marks in sentence for words' do
    data = sample_data

    data[:words].shift # remove one word

    game = build(:game_complete_sentence, data)

    expect(game).not_to be_valid
    expect(game.errors.size).to be == 1
    expect(game.errors[:sentence]).not_to be_nil
  end

end
