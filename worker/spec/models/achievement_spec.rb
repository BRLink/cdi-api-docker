require 'rails_helper'

RSpec.describe "Achievement:", type: :model do
  LIKE_ACHIEVEMENTS = [:like_post, :like_institution_post, :like_question, :like_blog_article]
  COMMENT_ACHIEVEMENTS = [:comment_blog_article, :comment_post, :comment_institution_post,
                          :comment_learning_track]
  before :each do
    @categories = create_list(:category, 1)
    @student = create(:student_user)
    @mentor  = create(:mentor_user)

    @post = create(:post, categories: @categories)
    @institution_post = create(:post, :institutional, user: @mentor, categories: @categories)
    @blog_article = create(:blog_article, categories: @categories)
    @question = create(:survey_question)
    #@learning_track = create(:learning_track, categories: @categories)
  end

  context "for all achievements:\t\t" do
    [:LIKE, :COMMENT].each do |kind|
      const_get(:"#{kind}_ACHIEVEMENTS").each do |action|
        it "#{action} should be valid" do
          achievement = create(:achievement, action)
          expect(achievement).to be_valid
        end
      end
    end
  end

  context "Student likes a resource:\t\t" do
    def user_likes_a resource
      achievement = create :achievement, :"like_#{resource}"
      Like.create(user: @student, resource: instance_variable_get("@#{resource}"))
      yield achievement
    end

    ["post", "institution_post", "question", "blog_article"].each do |resource|
      it "should grant achievements and points when user likes a #{resource.sub("_", " ")}" do
        user_likes_a resource do |achievement|
          expect(@student.achievements.sum(:points)).to be > 0
          expect(@student.achievements(true)).to include(achievement)
        end
      end
    end
  end

  context "Student comments a resource:\t" do
    def user_comments_a resource
      achievement = create :achievement, :"comment_#{resource}"
      Comment.create(user: @student,
                     resource: instance_variable_get("@#{resource}"),
                     content: "fake content")
      yield achievement
    end

    ["post", "institution_post", "learning_track", "blog_article"].each do |resource|
      it "should grant achievement and points when user comments a #{resource.sub("_", " ")}" do
        user_comments_a resource do |achievement|
          expect(@student.achievements.sum(:points)).to be > 0
          expect(@student.achievements(true)).to include(achievement)
        end
      end
    end
  end
end
