require 'rails_helper'

RSpec.describe User, type: :model do

  before(:each) do
    @student_user = create(:student_user)
    @mentor_user  = create(:mentor_user)
    @teacher_user = create(:teacher_user)
    @staff_user   = create(:admin_user)
  end

  def each_user
    [@student_user, @mentor_user, @teacher_user, @staff_user].each do |user|
      yield user
    end
  end

  it 'must have a valid origin association' do
    each_user do |user|
      expect(user).to have_one(:origin)
    end
  end

  it 'must have database indexes' do
    expect(subject).to have_db_index(:email)
  end

  it 'has a valid email' do
    user = build(:student_user, email: 'invalid_email')
    expect(user.valid?).to be false
    expect(user.errors[:email]).to_not eq([])
  end

  it 'has a valid username' do
    user = build(:student_user, username: 'username_student')
    expect(user).to be_valid

    expect(user.errors[:username]).to eq([])
    expect(user.username).to eq('username_student')
  end

  it 'dont allow invalid profile type' do
    user = build(:student_user, profile_type: 'my_custom_profile_type')

    expect(user).not_to be_valid
    expect(user.errors[:profile_type]).to_not eq([])

    user.profile_type = User::VALID_PROFILES_TYPES[:student]
    expect(user).to be_valid
    expect(user.errors[:profile_type]).to eq([])
  end

  it 'has a valid normalized username' do
    user = create(:student_user, username: 'User Name Student')
    expect(user.username).to eq('user_name_student')

    user = create(:student_user, username: 'User Name Student')
    expect(user.username).to eq('user_name_student2')

    user = create(:student_user, username: 'User Name Student')
    expect(user.username).to eq('user_name_student3')

    user = create(:student_user, username: 'User Name Student')
    expect(user.username).to eq('user_name_student4')

    user = create(:student_user, first_name: 'My Awesome', last_name: 'User')
    expect(user.username).to eq('my_awesome_user')

    user = create(:student_user, first_name: 'My Awesome', last_name: 'User')
    expect(user.username).to eq('my_awesome_user2')
  end

  it 'has a encrypted password' do
    expect(@student_user.password_digest).to_not be_nil
  end

  it 'has a valid gender' do
    expect(@student_user.errors[:gender]).to eq([])
    expect(@teacher_user.errors[:gender]).to eq([])

    user = build(:student_user, gender: 'Masculino')
    expect(user).not_to be_valid
  end

  it 'must validates password updates' do
    expect(@student_user.password_digest).to_not be_nil

    @student_user.password = 'i'

    expect(@student_user).to_not be_valid

    expect(@student_user.errors[:password]).to_not eq([])
    expect(@student_user.errors[:password_confirmation]).to_not eq([])

    expect(@student_user.password_digest_changed?).to be true

    @student_user.password = 'valid_password'

    last_password_digest = @student_user.password_digest

    expect(@student_user).to_not be_valid
    expect(@student_user.errors[:password]).to eq([])
    expect(@student_user.password_digest_changed?).to be true

    @student_user.password_confirmation = 'valid_password'

    expect(@student_user.password_digest).to eq(last_password_digest)

    expect(@student_user).to be_valid
    expect(@student_user.errors[:password_confirmation]).to eq([])

  end

  it 'successfully update user password' do
    expect(@student_user.password_digest).to_not be_nil

    @student_user.update_password('new_password')

    expect(@student_user.password).to eq(@student_user.password_confirmation)
    expect(@student_user.password_digest).to_not be_nil
    expect(@student_user.reset_password_token).to be_nil
    expect(@student_user.authorizations.size).to be 0
    expect(@student_user.password_reseted_at).to be <= Time.zone.now

    expect(@student_user.authenticate('new_password')).to be_a(User)
    expect(@student_user.authenticate('new_password')).not_to be_nil
  end

  it 'sucessfully create reset password token for user' do
    @student_user.reset_password!

    user_id = @student_user.id

    expect(@student_user.reset_password_token).to_not be_nil
    expect(@student_user.reset_password_token).to match %r(\A#{user_id})
    expect(@student_user.reset_password_sent_at).to_not be_nil
    expect(@student_user.reset_password_sent_at).to be < Time.zone.now
  end

  it 'has a fullname' do
    user = build(:student_user, first_name: 'Full', last_name: 'Name')
    expect(user.fullname).to eq('Full Name')
    expect(user.name).to eq('Full Name')
  end

  it 'has a convinience method for check profile type' do
    expect(@staff_user.staff?).to be true
    expect(@staff_user.mentor?).to be false
    expect(@staff_user.multiplier?).to be false

    expect(@student_user.student?).to be true
    expect(@student_user.teacher?).to be false
    expect(@student_user.multiplier?).to be false

    expect(@teacher_user.teacher?).to be true
    expect(@teacher_user.multiplier?).to be true
    expect(@teacher_user.student?).to be false

    expect(@mentor_user.mentor?).to be true
    expect(@mentor_user.admin?).to be false
    expect(@mentor_user.multiplier?).to be true

    librarian_user = create(:librarian_user)
    social_educator_user = create(:social_educator_user)

    expect(librarian_user.librarian?).to be true
    expect(librarian_user.multiplier?).to be true
    expect(librarian_user.mentor?).to be false

    expect(social_educator_user.social_educator?).to be true
    expect(social_educator_user.multiplier?).to be true
    expect(social_educator_user.librarian?).to be false
  end

  it 'must authenticate using class method' do
    user = create(:teacher_user, password: 'password', email: 'teacher@cdi.org.br')

    expect(User.authenticate('teacher@cdi.org.br', 'password')).to be_a(User)
    expect(User.authenticate('teacher@cdi.org.br', 'wrong_password')).to be false
  end

  it 'must have class scope for profile types' do
    User::VALID_PROFILES_TYPES.each do |scope_name, _|
      expect(User.respond_to?(scope_name)).to be true
    end
  end
end
