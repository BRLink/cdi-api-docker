require 'rails_helper'

RSpec.describe Skill, type: :model do
  it 'must be valid' do
    skill = create(:skill)

    expect(skill).to be_valid
  end

  it 'must be invalid' do
    skill = build(:skill, name: nil)

    expect(skill).not_to be_valid
    expect(-> { create(:skill, name: nil) }).to raise_error
  end

  it 'must resolve alias association name' do
    skill = create(:skill_with_tracks, tracks_count: 3)

    expect(skill.respond_to?(:tracks)).to be true
    expect(skill.tracks.size).to be 3
  end

  it 'must has_and_belongs_to_many learning track' do
    skill = create(:skill_with_tracks)

    expect(skill).to have_and_belong_to_many(:learning_tracks)
  end

  it 'must have table indexes' do
    expect(subject).to have_db_index(:name)
  end

  it 'must increment position successfully' do
    skill     = create(:skill)
    new_skill = create(:skill)

    expect(skill.position).to be == (new_skill.position - 1)
    expect(new_skill.position).to be == skill.position.next
    expect(skill.position.next).to be == new_skill.position
  end

  it 'allow same position' do
    skill     = create(:skill, position: 2)
    new_skill = create(:skill, position: 2)

    expect(skill.position).to be == new_skill.position
  end

end
