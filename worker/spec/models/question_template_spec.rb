require 'rails_helper'

RSpec.describe QuestionTemplate, type: :model do

  before(:each) do
    @open_question_template             = create("question_template_type_open")
    @single_choice_question_template    = create("question_template_type_single_choice")
    @multiple_choices_question_template = create("question_template_type_multiple_choices")
  end

  it 'must be valid' do
    question = build(:question_template)

    expect(question).to be_valid

    question.question_type = nil

    expect(question).not_to be_valid
  end

  it 'must have class scope for questions types' do
    QuestionTemplate::QUESTIONS_TYPES.each do |scope_name, _|
      expect(QuestionTemplate.respond_to?(scope_name)).to be true
    end
  end

  it 'must have convenience method to check question type' do
    expect(@open_question_template.question_open?).to be true
    expect(@open_question_template.question_multiple_choices?).to be false
    expect(@open_question_template.question_single_choice?).to be false

    expect(@single_choice_question_template.question_single_choice?).to be true
    expect(@single_choice_question_template.question_open?).to be false
    expect(@single_choice_question_template.question_multiple_choices?).to be false

    expect(@multiple_choices_question_template.question_multiple_choices?).to be true
    expect(@multiple_choices_question_template.question_single_choice?).to be false
    expect(@multiple_choices_question_template.question_open?).to be false

  end

  it 'must have_many questions association' do
    question = create(:question_template_with_questions)

    expect(question).to have_many(:questions)
  end

  it 'must have method to check if question needs choice' do
    expect(@open_question_template.with_choice?).to be false
    expect(@single_choice_question_template.with_choice?).to be true
    expect(@multiple_choices_question_template.with_choice?).to be true
  end

  it 'must clear choices before save if question is open' do
    question = build(:question_template_type_open, choices: ['1', '2', '3'])

    expect(question.choices.size).to be == 3
    expect(question.valid?).to be true

    question.save

    expect(question.choices).to be_nil
  end

  it 'must have database indexes' do
    expect(subject).to have_db_index(:question_type)
    expect(subject).to have_db_index(:question_for)
  end

  it 'must have a valid question type' do
    question_template = build(:question_template, question_type: 'LOL')

    expect(question_template).not_to be_valid
    expect(question_template.errors[:question_type]).not_to be eq([])
  end

end
