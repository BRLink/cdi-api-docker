require 'rails_helper'

RSpec.describe Multimedia, type: :model do
  def create_item(type,user, published=true,title=nil,category=nil,tags=nil)
    ret = create(type)
    ret.user = user
    ret.category = Category.create(name: category) if category
    ret.tags = tags unless tags.nil?
    ret.set_status(:published, false) if published
    ret
  end
  def create_dynamic(user, published=true,title=nil,category=nil,tags=nil)
    ret = create_item(:learning_dynamic,user,published,title,category,tags)
    ret.title = title unless title.nil?
    ret.save
    ret
  end
  def create_supporting_course(user, published=true,title=nil,category=nil,tags=nil)
    ret = create_item(:supporting_course,user,published,title,category,tags)
    ret.title = title unless title.nil?
    ret.save
    ret
  end
  def create_multimedia(user, published=true,title=nil,category=nil,tags=nil)
    ret = create_item(:multimedia_with_link,user,published,title,category,tags)
    ret.name = title unless title.nil?
    ret.save
    ret
  end
  describe "Library Index" do
    it 'index: displays published or owned only' do
      teacher = create(:teacher)
      admin = create(:admin)
      dynamic_1 = create_dynamic(teacher, true)
      dynamic_2 = create_dynamic(teacher, false)
      dynamic_3 = create_dynamic(admin, false)

      course_1 = create_supporting_course(teacher, false)
      course_2 = create_supporting_course(teacher, true)
      course_3 = create_supporting_course(admin, false)

      media_1 = create_multimedia(teacher, false)
      media_2 = create_multimedia(teacher, true)
      media_3 = create_multimedia(admin, false)
      expect(Library.main(nil,admin,true, ['Multimedia','LearningDynamic','SupportingCourse'])).to eq [media_3, media_2, course_3, course_2, dynamic_3, dynamic_1]
    end
  end
  describe "Library Search" do
    it 'can search simple term' do
      teacher = create(:teacher)
      media_1 = create_multimedia(teacher, true, 'hello world')
      expect(Library.main('hello',teacher,true, ['Multimedia','LearningDynamic','SupportingCourse'])).to eq [media_1]
      expect(Library.main('hello w',teacher,true, ['Multimedia','LearningDynamic','SupportingCourse'])).to eq [media_1]
      expect(Library.main('world',teacher,true, ['Multimedia','LearningDynamic','SupportingCourse'])).to eq [media_1]
      expect(Library.main('worl',teacher,true, ['Multimedia','LearningDynamic','SupportingCourse'])).to eq [media_1]
      # expect(Library.main('ello',teacher,true, ['Multimedia','LearningDynamic','SupportingCourse'])).to eq [media_1]
    end
    it 'can search term with space' do
      teacher = create(:teacher)
      media_1 = create_multimedia(teacher, true, 'he llo world')
      expect(Library.main('he llo',teacher,true, ['Multimedia','LearningDynamic','SupportingCourse'])).to eq [media_1]
    end
    it 'can search term with accent' do
      teacher = create(:teacher)
      media_1 = create_multimedia(teacher, true, 'são')
      expect(Library.main('são',teacher,true, ['Multimedia','LearningDynamic','SupportingCourse'])).to eq [media_1]
    end
    it 'is case insensitive search' do
      teacher = create(:teacher)
      media_1 = create_multimedia(teacher, true, 'Hello World')
      expect(Library.main('hELLO',teacher,true, ['Multimedia','LearningDynamic','SupportingCourse'])).to eq [media_1]
    end
    it 'can search by category name' do
      teacher = create(:teacher)
      media_1 = create_multimedia(teacher, true, 'Hello World', 'c1')
      expect(Library.main('c1',teacher,true, ['Multimedia','LearningDynamic','SupportingCourse'])).to eq [media_1]
    end
    it 'can search by tags' do
      teacher = create(:teacher)
      media_1 = create_multimedia(teacher, true, 'Hello World','c1',['t1'])
      expect(Library.main('t1',teacher,true, ['Multimedia','LearningDynamic','SupportingCourse'])).to eq [media_1]
    end
  end

end
