FactoryGirl.define do
  factory :address do
    city nil
    street "MyString"
    addressable_id nil
    addressable_type "MyString"
    number 1
    district "MyString"
    complement "MyString"
    zipcode "MyString"
  end

end
