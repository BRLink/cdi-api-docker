FactoryGirl.define do
  factory :survey_question_user_answer do
    survey_question
    association :user, factory: :student_user
    answer_body { Faker::Lorem.sentence }
  end
end

