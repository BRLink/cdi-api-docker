FactoryGirl.define do
  factory :skill do
    name { Faker::Commerce.department(1)  }
    sequence(:position)

    factory :skill_with_tracks do
      transient do
        tracks_count 5
      end

      after(:create) do |skill, evaluator|
        create_list(:learning_track, evaluator.tracks_count, skills: [skill])
      end
    end

  end
end
