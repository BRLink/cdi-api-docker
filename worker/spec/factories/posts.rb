FactoryGirl.define do

  factory :post do
    association :user, factory: :staff_user
    content { Faker::Lorem.sentence(5) }
    association :publisher, factory: :teacher_user
    categories { [create(:category)] }

    trait :institutional do
      publisher { create(:educational_institution) }
    end
  end

end
