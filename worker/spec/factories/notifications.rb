FactoryGirl.define do
  factory :notification do
    notificable_id 1
    notificable_type "MyString"
    receiver_user_id 1
    sender_user_id 1
    notification_type "MyString"
    read false
  end

end
