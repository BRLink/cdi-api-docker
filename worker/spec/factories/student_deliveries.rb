FactoryGirl.define do
  factory :student_delivery do
    learning_delivery nil
    user nil
    student_class nil
    file "MyString"
    format_type "MyString"
  end
end
