FactoryGirl.define do
  factory :game_poll do
    user nil
    title "MyString"
    description "MyString"
    status "MyString"
    status_changed_at "2015-09-14 15:43:14"
    image "MyString"
  end
end
