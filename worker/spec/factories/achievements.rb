require 'csv'

FactoryGirl.define do
  factory :achievement do

    default_points = [1, 5, 10, 25, 50, 100]
    filename = "gamification_achievements.csv"
    achievements = CSV.parse(File.open(Rails.root.join('docs', 'seeds', filename), 'r'))
    area = ""

    achievements.each_with_index do |data, index|
      area = data[0] || area

      points = data[3..-1].find_index { |data| !data.nil? }
      points = (default_points[points] rescue nil)
      action = data[1] && data[1].squish
      user_type = data[2] && data[2].split(",")

      trait :"#{action}" do
        area area
        action action
        default_points points
        user_type user_type
      end
    end

  end
end
