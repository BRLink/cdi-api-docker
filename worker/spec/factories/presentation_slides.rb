FactoryGirl.define do
  factory :presentation_slide do
    presentation nil
    title "Meu titulo"
    layout_type "cover"
    slide_type "content"
    content [Faker::Lorem.paragraph(2, true) ]
    views_count 1
    position 1
    factory :dynamic_slide do
      slide_type 'dynamic'
    end
  end

  factory :slide_without_content, class: PresentationSlide do
    presentation nil
    title "Meu titulo"
    layout_type "cover"
    slide_type "content"
    content nil
    views_count 0
    position 1
  end

  factory :slide_linked_to_res, class: PresentationSlide do
    presentation nil
    title "Meu titulo"
    layout_type "cover"
    slide_type "game"
    content nil
    views_count 0
    position 1
  end


end
