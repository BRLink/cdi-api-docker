FactoryGirl.define do
  factory :student_class do
    name { Faker::Company.name }
    association :admin_user, factory: :teacher_user
    association :educational_institution, factory: :educational_institution
    code { nil }
    status { }
    status_changed_at {}
  end

end
