FactoryGirl.define do
  factory :learning_track_report do
    learning_track nil
    student_class nil
    enrollment_reports ""
    reviews_reports ""
    student_reports ""
    status "MyString"
  end

end
