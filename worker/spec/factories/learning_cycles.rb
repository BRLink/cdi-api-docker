FactoryGirl.define do
  factory :learning_cycle do
    name { Faker::Commerce.product_name }
    objective_text { Faker::Lorem.paragraph }
    tags { Faker::Commerce.department(3, true).split(/\,|\&/).map(&:squish) }
    association :user, factory: :student_user
    categories { [ create(:category) ] }

    factory :learning_cycle_with_tags do
      transient do
        tags_count 5
      end

      before(:create) do |cycle, evaluator|
        tags = Faker::Commerce.department(evaluator.tags_count, true).split(/\,|\&/).map(&:squish)
        cycle.tags = tags
      end

    end
  end

end
