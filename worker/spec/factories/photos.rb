FactoryGirl.define do
  factory :photo do
    album nil
    user nil
    image "MyString"
    original_filename "MyString"
    content_type "MyString"
    file_size 1
    active false
    position 1
    caption "MyString"
    tags "MyString"
  end

end
