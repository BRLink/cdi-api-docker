FactoryGirl.define do
  factory :game_quiz do
    user_id nil
    title "MyString"
    description "MyString"
    status "MyString"
    status_changed_at "2015-09-08 13:08:39"
    image "MyString"
  end

end
