# Preview all emails at http://localhost:3000/rails/mailers/learning_tracks_mailer
class LearningTracksMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/learning_tracks_mailer/new_track
  def new_track
    LearningTracksMailer.new_track
  end

end
