require "rails_helper"

RSpec.describe LearningCycleMailer, type: :mailer do
  describe "new_cycle" do
    let(:mail) { LearningCycleMailer.new_cycle }

    it "renders the headers" do
      expect(mail.subject).to eq("New cycle")
      expect(mail.to).to eq(["to@example.org"])
      expect(mail.from).to eq(["from@example.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

end
