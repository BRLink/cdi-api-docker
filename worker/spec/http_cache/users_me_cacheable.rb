require 'rails_helper'
require 'airborne'

Airborne.configure do |config|
  config.rack_app = API::V1::Base
end

describe API::V1::UsersMeCacheable do
  before(:all) do
    authenticate_or_create_user
  end

  def app
    API::V1::Base
  end

  def authenticate_or_create_user
    authenticate_user
    create_user
  end

  [:get, :put, :post, :update, :delete, :options].each do |request_method|
    define_method "#{request_method}_authenticate" do |path, body = {}, headers = {}|
      unless @user_auth_data
        authenticate_or_create_user
      end

      auth_data = {
        'X-Token' => @user_auth_data[:auth_token],
        'X-Provider' => @user_auth_data[:provider]
      }

      if [:get, :options].member?(request_method)
        self.send(request_method, path, body.merge(auth_data))
      else
        self.send(request_method, path, body, headers.merge(auth_data))
      end
    end
  end

  def create_user
    admin_user = create(:admin)
    institution = create(:educational_institution, admin_user: admin_user)
    student_class = create(:student_class, educational_institution: institution)

    user_data = {
      first_name: 'Tester',
      last_name: 'User',
      birthday: Date.today,
      gender: 'male',
      password: 'tester123',
      password_confirmation: 'tester123',
      email: 'tester_user@cdi.org.br',
      educational_institution_id: institution.id,
      classes_codes: student_class.code
    }

    post '/api/v1/users', { user: user_data, provider: 'postman' }

    if response.status == 201
      @user_auth_data = json_body[:auth_data]
    end
  end

  def authenticate_user
    post '/api/v1/users/auth', { email: 'tester_user@cdi.org.br', password: 'tester123', provider: 'postman' }

    @user_auth_data = json_body[:auth_data] if response.status == 201
  end

  def request_with_authentication
  end

  context '[GET] /api/v1/users/me' do
    it 'returns 401 status since no authentication token is provided' do
      get_authenticate '/api/v1/users/me', {'X-Cached' => true }

      expect_json_types(student_classes: :array, photos: :array, educational_institutions: :array)
      expect_json_types(learning_cycles_count: :int, learning_deliveries_count: :int, completed_enrolled_learning_tracks_count: :int, learning_tracks_count: :int)
      expect_json_types(notifications_status: :object)
      expect_json_types(password_reseted_at: :date_or_null)
      expect_json_types(oauth_provider: :string_or_null, facebook_oauth_provider_linked: :bool_or_null)
    end


    it 'clear cache when one of user data is updated' do
      get_authenticate '/api/v1/users/me', {'X-Cached' => true }

      last_user_data = OpenStruct.new(json_body.dup)

      user_data = {
        about: last_user_data.about.to_s + ' updated',
        first_name: last_user_data.first_name.to_s + ' updated',
        last_name: last_user_data.last_name.to_s + ' updated',
      }

      put_authenticate '/api/v1/users/me', user: user_data, _clear_cache: ENV['CLEAR_CACHE']

      update_json_body = OpenStruct.new(json_body.dup)

      expect_json_types(changed_attributes: :array_of_strings)

      # database fetched user data
      updated_user_data = OpenStruct.new(update_json_body.user)

      # try to get updated user data
      get_authenticate '/api/v1/users/me', {'X-Cached' => true }

      new_user_data = OpenStruct.new(json_body.dup)

      user_data.each_key do |attribute|
        expect(updated_user_data[attribute]).not_to eq last_user_data[attribute]
        expect(updated_user_data[attribute]).to eq new_user_data[attribute]
      end

    end

  end
end


