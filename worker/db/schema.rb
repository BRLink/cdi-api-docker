# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160318184514) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"
  enable_extension "unaccent"

  create_table "achievements", force: :cascade do |t|
    t.string   "action",        null: false
    t.string   "area",          null: false
    t.integer  "default_score", null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.json     "user_type",     null: false
  end

  add_index "achievements", ["action"], name: "index_achievements_on_action", unique: true, using: :btree

  create_table "addresses", force: :cascade do |t|
    t.integer  "city_id"
    t.integer  "addressable_id"
    t.string   "addressable_type"
    t.string   "street"
    t.integer  "number"
    t.string   "district"
    t.string   "complement"
    t.string   "zipcode"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "addresses", ["addressable_id", "addressable_type"], name: "index_addresses_on_addressable_id_and_addressable_type", using: :btree
  add_index "addresses", ["addressable_id"], name: "index_addresses_on_addressable_id", using: :btree
  add_index "addresses", ["addressable_type"], name: "index_addresses_on_addressable_type", using: :btree
  add_index "addresses", ["city_id"], name: "index_addresses_on_city_id", using: :btree
  add_index "addresses", ["street"], name: "index_addresses_on_street", using: :btree

  create_table "albums", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "title"
    t.string   "description"
    t.boolean  "active"
    t.integer  "position"
    t.string   "tags",                     array: true
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "album_type"
  end

  add_index "albums", ["active"], name: "index_albums_on_active", using: :btree
  add_index "albums", ["tags"], name: "index_albums_on_tags", using: :gin
  add_index "albums", ["user_id"], name: "index_albums_on_user_id", using: :btree

  create_table "attachments", force: :cascade do |t|
    t.integer  "post_id"
    t.string   "attachment_type", null: false
    t.integer  "attachment_id",   null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "attachable_id"
    t.string   "attachable_type"
  end

  add_index "attachments", ["attachment_id"], name: "index_attachments_on_attachment_id", using: :btree
  add_index "attachments", ["post_id"], name: "index_attachments_on_post_id", using: :btree

  create_table "authorizations", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "token"
    t.string   "provider"
    t.datetime "expires_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "authorizations", ["token"], name: "index_authorizations_on_token", using: :btree
  add_index "authorizations", ["user_id"], name: "index_authorizations_on_user_id", using: :btree

  create_table "badges", force: :cascade do |t|
    t.string   "name",        null: false
    t.text     "description", null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.json     "user_type"
    t.text     "action"
    t.integer  "level"
  end

  add_index "badges", ["action"], name: "index_badges_on_action", unique: true, using: :btree

  create_table "blog_articles", force: :cascade do |t|
    t.string   "title",       null: false
    t.text     "link",        null: false
    t.text     "description", null: false
    t.datetime "pub_date",    null: false
    t.string   "guid",        null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.text     "content"
  end

  add_index "blog_articles", ["guid"], name: "index_blog_articles_on_guid", unique: true, using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.integer  "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "categories", ["name"], name: "index_categories_on_name", unique: true, using: :btree

  create_table "category_resources", id: false, force: :cascade do |t|
    t.integer  "resource_id"
    t.string   "resource_type"
    t.integer  "category_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "category_resources", ["resource_id", "resource_type", "category_id"], name: "index_category_resources_on_resource_id_type_and_category", unique: true, using: :btree

  create_table "challenge_deliveries", force: :cascade do |t|
    t.integer  "challenge_id",      null: false
    t.string   "title"
    t.text     "description"
    t.string   "cover_image"
    t.integer  "presentation_id"
    t.string   "video_link"
    t.string   "file"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "cover_description"
  end

  add_index "challenge_deliveries", ["challenge_id"], name: "index_challenge_deliveries_on_challenge_id", using: :btree

  create_table "challenge_deliveries_users", id: false, force: :cascade do |t|
    t.integer "challenge_delivery_id"
    t.integer "user_id"
  end

  add_index "challenge_deliveries_users", ["challenge_delivery_id", "user_id"], name: "idx_challenge_deliveries_users_uniq", unique: true, using: :btree

  create_table "challenge_subscriptions", force: :cascade do |t|
    t.integer  "challenge_id"
    t.integer  "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "challenge_subscriptions", ["challenge_id"], name: "index_challenge_subscriptions_on_challenge_id", using: :btree
  add_index "challenge_subscriptions", ["user_id", "challenge_id"], name: "index_challenge_subscriptions_on_user_id_and_challenge_id", unique: true, using: :btree
  add_index "challenge_subscriptions", ["user_id"], name: "index_challenge_subscriptions_on_user_id", using: :btree

  create_table "challenges", force: :cascade do |t|
    t.string   "title",                               null: false
    t.text     "description",                         null: false
    t.string   "format_type",                         null: false
    t.string   "delivery_kind",                       null: false
    t.datetime "delivery_date",                       null: false
    t.string   "cover_image"
    t.string   "tags",                                             array: true
    t.integer  "presentation_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "group_size"
    t.string   "delivery_title",    default: "",      null: false
    t.string   "status",            default: "draft", null: false
    t.datetime "status_changed_at"
  end

  add_index "challenges", ["tags"], name: "index_challenges_on_tags", using: :gin

  create_table "channels", force: :cascade do |t|
    t.integer  "student_class_id"
    t.integer  "learning_track_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "channels", ["student_class_id", "learning_track_id"], name: "index_channels_on_student_class_id_and_learning_track_id", unique: true, using: :btree

  create_table "chat_message_reads", force: :cascade do |t|
    t.integer  "user_id",         null: false
    t.integer  "chat_message_id", null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "chat_message_reads", ["user_id", "chat_message_id"], name: "index_chat_message_reads_on_user_id_and_chat_message_id", unique: true, using: :btree

  create_table "chat_messages", force: :cascade do |t|
    t.integer  "user_id",      null: false
    t.integer  "chat_room_id", null: false
    t.string   "content"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "chat_room_users", force: :cascade do |t|
    t.integer  "user_id",                  null: false
    t.integer  "chat_room_id",             null: false
    t.integer  "role",         default: 0, null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "chat_room_users", ["user_id", "chat_room_id"], name: "index_chat_room_users_on_user_id_and_chat_room_id", unique: true, using: :btree

  create_table "chat_rooms", force: :cascade do |t|
    t.integer  "user_id",                null: false
    t.string   "name"
    t.integer  "room_type",  default: 0, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "cities", force: :cascade do |t|
    t.integer  "state_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "cities", ["state_id"], name: "index_cities_on_state_id", using: :btree

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "comments", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "resource_type", null: false
    t.integer  "resource_id",   null: false
    t.string   "content",       null: false
    t.integer  "parent_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "comments", ["parent_id"], name: "index_comments_on_parent_id", using: :btree
  add_index "comments", ["resource_id"], name: "index_comments_on_resource_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "conversion_statuses", force: :cascade do |t|
    t.integer  "presentation_id"
    t.string   "status"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "conversion_statuses", ["presentation_id"], name: "index_conversion_statuses_on_presentation_id", using: :btree

  create_table "conversions", force: :cascade do |t|
    t.integer  "status",          default: 0
    t.integer  "slide_count"
    t.integer  "image_count"
    t.json     "slides"
    t.integer  "user_id"
    t.integer  "presentation_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "start_position"
  end

  create_table "cycle_plannings", force: :cascade do |t|
    t.integer  "learning_cycle_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "cycle_plannings", ["learning_cycle_id"], name: "index_cycle_plannings_on_learning_cycle_id", using: :btree

  create_table "discussion_answers", force: :cascade do |t|
    t.integer  "user_id",       null: false
    t.integer  "discussion_id", null: false
    t.integer  "option_id",     null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "discussion_answers", ["option_id", "user_id"], name: "index_discussion_answers_on_option_id_and_user_id", unique: true, using: :btree
  add_index "discussion_answers", ["option_id"], name: "index_discussion_answers_on_option_id", using: :btree

  create_table "discussion_options", force: :cascade do |t|
    t.integer  "discussion_id",             null: false
    t.string   "text",                      null: false
    t.integer  "position"
    t.integer  "answers_count", default: 0, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "discussion_options", ["discussion_id", "text"], name: "index_discussion_options_on_discussion_id_and_text", unique: true, using: :btree

  create_table "discussion_views", force: :cascade do |t|
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "user_id",       null: false
    t.integer  "discussion_id", null: false
  end

  add_index "discussion_views", ["discussion_id", "user_id"], name: "index_discussion_views_on_discussion_id_and_user_id", unique: true, using: :btree

  create_table "discussions", force: :cascade do |t|
    t.integer  "user_id",                        null: false
    t.integer  "publisher_id",                   null: false
    t.string   "publisher_type",                 null: false
    t.integer  "discussion_poll_id"
    t.integer  "cover_id"
    t.integer  "parent_id"
    t.string   "discussion_type",                null: false
    t.string   "title",                          null: false
    t.string   "content"
    t.string   "video_link"
    t.string   "tags",                                        array: true
    t.integer  "answers_count",      default: 0, null: false
    t.integer  "comments_count",     default: 0, null: false
    t.integer  "likes_count",        default: 0, null: false
    t.integer  "featured_count",     default: 0, null: false
    t.integer  "children_count",     default: 0, null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "views_count",        default: 0, null: false
    t.integer  "best_comment_id"
  end

  add_index "discussions", ["publisher_type", "publisher_id"], name: "index_discussions_on_publisher_type_and_publisher_id", using: :btree
  add_index "discussions", ["tags"], name: "index_discussions_on_tags", using: :gin

  create_table "educational_institutions", force: :cascade do |t|
    t.string   "name"
    t.string   "username"
    t.text     "about"
    t.string   "profile_image"
    t.string   "profile_cover_image"
    t.integer  "admin_user_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.hstore   "contacts"
  end

  add_index "educational_institutions", ["admin_user_id"], name: "index_educational_institutions_on_admin_user_id", using: :btree
  add_index "educational_institutions", ["name"], name: "index_educational_institutions_on_name", using: :btree
  add_index "educational_institutions", ["username"], name: "index_educational_institutions_on_username", using: :btree

  create_table "educational_institutions_users", force: :cascade do |t|
    t.integer "educational_institution_id"
    t.integer "user_id"
    t.integer "status",                     default: 100
  end

  add_index "educational_institutions_users", ["educational_institution_id", "user_id"], name: "index_educational_institutions_user_id_institution_id", unique: true, using: :btree
  add_index "educational_institutions_users", ["educational_institution_id"], name: "index_instititution_join_table_institution_id", using: :btree
  add_index "educational_institutions_users", ["user_id"], name: "index_instititution_join_table_user_id", using: :btree

  create_table "enrolled_learning_tracks", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "learning_track_id"
    t.string   "status"
    t.datetime "status_changed_at"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "student_class_id"
    t.float    "progress",          default: 0.0
  end

  add_index "enrolled_learning_tracks", ["learning_track_id"], name: "index_enrolled_learning_tracks_on_learning_track_id", using: :btree
  add_index "enrolled_learning_tracks", ["progress"], name: "index_enrolled_learning_tracks_on_progress", using: :btree
  add_index "enrolled_learning_tracks", ["status"], name: "index_enrolled_learning_tracks_on_status", using: :btree
  add_index "enrolled_learning_tracks", ["student_class_id"], name: "index_enrolled_learning_tracks_on_student_class_id", using: :btree
  add_index "enrolled_learning_tracks", ["user_id", "learning_track_id", "student_class_id"], name: "index_enrolled_tracks_on_user_id_track_id_class_id", unique: true, using: :btree
  add_index "enrolled_learning_tracks", ["user_id"], name: "index_enrolled_learning_tracks_on_user_id", using: :btree

  create_table "game_complete_sentences", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "sentence"
    t.string   "status"
    t.string   "words",                          array: true
    t.datetime "status_changed_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "game_complete_sentences", ["status"], name: "index_game_complete_sentences_on_status", using: :btree
  add_index "game_complete_sentences", ["user_id"], name: "index_game_complete_sentences_on_user_id", using: :btree
  add_index "game_complete_sentences", ["words"], name: "index_game_complete_sentences_on_words", using: :gin

  create_table "game_group_items", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "game_group_items", ["user_id"], name: "index_game_group_items_on_user_id", using: :btree

  create_table "game_groups", force: :cascade do |t|
    t.integer  "game_group_item_id"
    t.string   "title"
    t.string   "items",                           array: true
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "game_groups", ["game_group_item_id", "title"], name: "unique_index_on_game_groups_game_group_title", unique: true, using: :btree
  add_index "game_groups", ["game_group_item_id"], name: "index_game_groups_on_game_group_item_id", using: :btree
  add_index "game_groups", ["items"], name: "index_game_groups_on_items", using: :gin
  add_index "game_groups", ["title"], name: "index_game_groups_on_title", using: :btree

  create_table "game_item_answers", force: :cascade do |t|
    t.integer  "game_item_id"
    t.integer  "user_id"
    t.boolean  "correct"
    t.string   "reply_word"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "game_item_answers", ["correct"], name: "index_game_item_answers_on_correct", using: :btree
  add_index "game_item_answers", ["game_item_id", "user_id"], name: "unique_in_on_game_item_answers_game_id_user_id", unique: true, using: :btree
  add_index "game_item_answers", ["game_item_id"], name: "index_game_item_answers_on_game_item_id", using: :btree
  add_index "game_item_answers", ["user_id"], name: "index_game_item_answers_on_user_id", using: :btree

  create_table "game_items", force: :cascade do |t|
    t.integer  "game_related_item_id"
    t.string   "word"
    t.string   "related_word"
    t.string   "image"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "related_image"
  end

  add_index "game_items", ["game_related_item_id", "word", "related_word"], name: "unique_index_on_game_items_game_word_related_word", unique: true, using: :btree
  add_index "game_items", ["game_related_item_id"], name: "index_game_items_on_game_related_item_id", using: :btree
  add_index "game_items", ["related_word"], name: "index_game_items_on_related_word", using: :btree
  add_index "game_items", ["word"], name: "index_game_items_on_word", using: :btree

  create_table "game_open_questions", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "title"
    t.integer  "questions_count"
    t.string   "image"
    t.string   "status"
    t.datetime "status_changed_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "video_link"
  end

  add_index "game_open_questions", ["status"], name: "index_game_open_questions_on_status", using: :btree
  add_index "game_open_questions", ["title"], name: "index_game_open_questions_on_title", using: :btree
  add_index "game_open_questions", ["user_id"], name: "index_game_open_questions_on_user_id", using: :btree

  create_table "game_polls", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "status"
    t.datetime "status_changed_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "game_polls", ["status"], name: "index_game_polls_on_status", using: :btree
  add_index "game_polls", ["user_id"], name: "index_game_polls_on_user_id", using: :btree

  create_table "game_question_answers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "game_question_id"
    t.integer  "option_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.text     "answer_body"
    t.boolean  "correct"
    t.datetime "corrected_at"
  end

  add_index "game_question_answers", ["correct"], name: "index_game_question_answers_on_correct", using: :btree
  add_index "game_question_answers", ["game_question_id"], name: "index_game_question_answers_on_game_question_id", using: :btree
  add_index "game_question_answers", ["option_id"], name: "index_game_question_answers_on_option_id", using: :btree
  add_index "game_question_answers", ["user_id"], name: "index_game_question_answers_on_user_id", using: :btree

  create_table "game_question_options", force: :cascade do |t|
    t.integer  "game_question_id"
    t.integer  "position"
    t.string   "text"
    t.string   "description"
    t.boolean  "correct",          default: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "game_question_options", ["game_question_id"], name: "index_game_question_options_on_game_question_id", using: :btree
  add_index "game_question_options", ["position"], name: "index_game_question_options_on_position", using: :btree

  create_table "game_questions", force: :cascade do |t|
    t.integer  "questionable_id"
    t.string   "questionable_type"
    t.string   "input_type"
    t.integer  "position"
    t.string   "title"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.boolean  "allow_multiples_answers", default: false
  end

  add_index "game_questions", ["position"], name: "index_game_questions_on_position", using: :btree
  add_index "game_questions", ["questionable_id", "questionable_type"], name: "index_game_questions_on_questionable_id_and_questionable_type", using: :btree
  add_index "game_questions", ["questionable_id"], name: "index_game_questions_on_questionable_id", using: :btree
  add_index "game_questions", ["questionable_type", "questionable_id"], name: "index_game_questions_on_questionable_type_and_questionable_id", using: :btree
  add_index "game_questions", ["questionable_type"], name: "index_game_questions_on_questionable_type", using: :btree
  add_index "game_questions", ["title"], name: "index_game_questions_on_title", using: :btree

  create_table "game_quizzes", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "status"
    t.datetime "status_changed_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "game_quizzes", ["status"], name: "index_game_quizzes_on_status", using: :btree
  add_index "game_quizzes", ["user_id"], name: "index_game_quizzes_on_user_id", using: :btree

  create_table "game_related_items", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "items_count"
    t.string   "status"
    t.datetime "status_changed_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "game_related_items", ["status"], name: "index_game_related_items_on_status", using: :btree
  add_index "game_related_items", ["user_id"], name: "index_game_related_items_on_user_id", using: :btree

  create_table "game_true_false", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "questions_count"
    t.string   "status"
    t.datetime "status_changed_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "game_true_false", ["status"], name: "index_game_true_false_on_status", using: :btree
  add_index "game_true_false", ["user_id"], name: "index_game_true_false_on_user_id", using: :btree

  create_table "game_user_scores", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "game_id"
    t.string   "game_type"
    t.integer  "student_class_id"
    t.integer  "score_points"
    t.integer  "score_percent"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "game_user_scores", ["game_id", "game_type", "user_id", "student_class_id"], name: "unique_index_on_game_scores_game_id_user_id_class_id", unique: true, using: :btree
  add_index "game_user_scores", ["game_id"], name: "index_game_user_scores_on_game_id", using: :btree
  add_index "game_user_scores", ["game_type"], name: "index_game_user_scores_on_game_type", using: :btree
  add_index "game_user_scores", ["student_class_id"], name: "index_game_user_scores_on_student_class_id", using: :btree
  add_index "game_user_scores", ["user_id"], name: "index_game_user_scores_on_user_id", using: :btree

  create_table "game_word_answers", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "words",                           array: true
    t.boolean  "correct"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "game_wordable_id"
    t.string   "game_wordable_type"
  end

  add_index "game_word_answers", ["correct"], name: "index_game_word_answers_on_correct", using: :btree
  add_index "game_word_answers", ["game_wordable_id"], name: "index_game_word_answers_on_game_wordable_id", using: :btree
  add_index "game_word_answers", ["game_wordable_type"], name: "index_game_word_answers_on_game_wordable_type", using: :btree
  add_index "game_word_answers", ["user_id", "game_wordable_id", "game_wordable_type"], name: "uniq_index_game_sentence_answer_on_user_id_game_id", unique: true, using: :btree
  add_index "game_word_answers", ["user_id"], name: "index_game_word_answers_on_user_id", using: :btree
  add_index "game_word_answers", ["words"], name: "index_game_word_answers_on_words", using: :gin

  create_table "game_word_searches", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "words",                          array: true
    t.string   "status"
    t.datetime "status_changed_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "game_word_searches", ["status"], name: "index_game_word_searches_on_status", using: :btree
  add_index "game_word_searches", ["user_id"], name: "index_game_word_searches_on_user_id", using: :btree
  add_index "game_word_searches", ["words"], name: "index_game_word_searches_on_words", using: :gin

  create_table "images_to_pdf_tasks", force: :cascade do |t|
    t.string   "ins",                                 array: true
    t.string   "out",                    null: false
    t.integer  "status",     default: 0
    t.integer  "progress",   default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "user_id"
  end

  add_index "images_to_pdf_tasks", ["user_id"], name: "index_images_to_pdf_tasks_on_user_id", using: :btree

  create_table "institution_managers", force: :cascade do |t|
    t.integer  "managed_institution_id"
    t.integer  "manager_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "institution_managers", ["managed_institution_id", "manager_id"], name: "index_institution_managers_institution_id_user_id", unique: true, using: :btree
  add_index "institution_managers", ["managed_institution_id"], name: "index_institution_managers_institution_id", using: :btree
  add_index "institution_managers", ["manager_id"], name: "index_institution_managers_user_id", using: :btree

  create_table "invitations", force: :cascade do |t|
    t.string   "profile_type"
    t.string   "email",                    null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "token",                    null: false
    t.integer  "status",       default: 0
    t.datetime "expires_at"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "invitations", ["token"], name: "index_invitations_on_token", unique: true, using: :btree

  create_table "learning_cycles", force: :cascade do |t|
    t.string   "name"
    t.text     "objective_text"
    t.integer  "category_id"
    t.integer  "user_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "tags",                           array: true
    t.string   "status"
    t.datetime "status_changed_at"
  end

  add_index "learning_cycles", ["category_id"], name: "index_learning_cycles_on_category_id", using: :btree
  add_index "learning_cycles", ["name"], name: "index_learning_cycles_on_name", using: :btree
  add_index "learning_cycles", ["status"], name: "index_learning_cycles_on_status", using: :btree
  add_index "learning_cycles", ["tags"], name: "index_learning_cycles_on_tags", using: :gin
  add_index "learning_cycles", ["user_id"], name: "index_learning_cycles_on_user_id", using: :btree

  create_table "learning_deliveries", force: :cascade do |t|
    t.string   "format_type"
    t.text     "description"
    t.datetime "delivery_date"
    t.integer  "delivery_kind"
    t.integer  "learning_dynamic_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "group_size"
  end

  add_index "learning_deliveries", ["learning_dynamic_id"], name: "index_learning_deliveries_on_learning_dynamic_id", using: :btree

  create_table "learning_dynamic_to_document_tasks", force: :cascade do |t|
    t.integer "learning_dynamic_id"
    t.integer "status",              default: 0
    t.integer "retries",             default: 0
    t.string  "ins",                             array: true
  end

  add_index "learning_dynamic_to_document_tasks", ["learning_dynamic_id"], name: "index_dynamic_to_document_task_dynamic", unique: true, using: :btree

  create_table "learning_dynamics", force: :cascade do |t|
    t.string   "title"
    t.integer  "category_id"
    t.integer  "user_id"
    t.integer  "student_presentation_id"
    t.integer  "multiplier_presentation_id"
    t.string   "perspectives",                            array: true
    t.string   "status"
    t.datetime "status_changed_at"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "parent_id"
    t.string   "thumb_url"
    t.string   "tags",                                    array: true
  end

  add_index "learning_dynamics", ["category_id"], name: "index_learning_dynamics_on_category_id", using: :btree
  add_index "learning_dynamics", ["multiplier_presentation_id"], name: "index_learning_dynamics_on_multiplier_presentation_id", using: :btree
  add_index "learning_dynamics", ["parent_id"], name: "index_learning_dynamics_on_parent_id", using: :btree
  add_index "learning_dynamics", ["perspectives"], name: "index_learning_dynamics_on_perspectives", using: :gin
  add_index "learning_dynamics", ["status"], name: "index_learning_dynamics_on_status", using: :btree
  add_index "learning_dynamics", ["student_presentation_id"], name: "index_learning_dynamics_on_student_presentation_id", using: :btree
  add_index "learning_dynamics", ["tags"], name: "index_learning_dynamics_on_tags", using: :gin
  add_index "learning_dynamics", ["user_id"], name: "index_learning_dynamics_on_user_id", using: :btree

  create_table "learning_dynamics_skills", id: false, force: :cascade do |t|
    t.integer "learning_dynamic_id"
    t.integer "skill_id"
  end

  add_index "learning_dynamics_skills", ["learning_dynamic_id", "skill_id"], name: "unique_index_learning_dynamics_skills", unique: true, using: :btree
  add_index "learning_dynamics_skills", ["learning_dynamic_id"], name: "index_learning_dynamics_skills_on_learning_dynamic_id", using: :btree
  add_index "learning_dynamics_skills", ["skill_id"], name: "index_learning_dynamics_skills_on_skill_id", using: :btree

  create_table "learning_track_moderators", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "learning_track_id"
  end

  add_index "learning_track_moderators", ["learning_track_id"], name: "index_learning_track_moderators_on_learning_track_id", using: :btree
  add_index "learning_track_moderators", ["user_id", "learning_track_id"], name: "index_moderators_user_id_track_id_unique", unique: true, using: :btree
  add_index "learning_track_moderators", ["user_id"], name: "index_learning_track_moderators_on_user_id", using: :btree

  create_table "learning_track_reports", force: :cascade do |t|
    t.integer  "learning_track_id"
    t.integer  "student_class_id"
    t.hstore   "enrollment_reports"
    t.hstore   "reviews_reports"
    t.json     "students_reports"
    t.string   "status"
    t.datetime "status_changed_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "learning_track_reports", ["learning_track_id", "student_class_id"], name: "index_tracks_reports_on_track_id_class_id", unique: true, using: :btree
  add_index "learning_track_reports", ["learning_track_id"], name: "index_learning_track_reports_on_learning_track_id", using: :btree
  add_index "learning_track_reports", ["status"], name: "index_learning_track_reports_on_status", using: :btree
  add_index "learning_track_reports", ["student_class_id"], name: "index_learning_track_reports_on_student_class_id", using: :btree

  create_table "learning_track_reviews", force: :cascade do |t|
    t.integer  "learning_track_id"
    t.integer  "user_id"
    t.text     "comment"
    t.integer  "review_type"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "student_class_id"
    t.integer  "score",             default: 1, null: false
  end

  add_index "learning_track_reviews", ["learning_track_id"], name: "index_learning_track_reviews_on_learning_track_id", using: :btree
  add_index "learning_track_reviews", ["review_type"], name: "index_learning_track_reviews_on_review_type", using: :btree
  add_index "learning_track_reviews", ["student_class_id"], name: "index_learning_track_reviews_on_student_class_id", using: :btree
  add_index "learning_track_reviews", ["user_id", "learning_track_id", "student_class_id"], name: "index_tracks_reviews_on_user_id_track_id_class_id", unique: true, using: :btree
  add_index "learning_track_reviews", ["user_id"], name: "index_learning_track_reviews_on_user_id", using: :btree

  create_table "learning_tracks", force: :cascade do |t|
    t.integer  "learning_cycle_id"
    t.string   "name"
    t.string   "tags",                                         array: true
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "category_id"
    t.string   "status"
    t.datetime "status_changed_at"
    t.integer  "parent_id"
    t.integer  "presentation_id"
    t.float    "average_score",     default: 0.0, null: false
  end

  add_index "learning_tracks", ["category_id"], name: "index_learning_tracks_on_category_id", using: :btree
  add_index "learning_tracks", ["learning_cycle_id"], name: "index_learning_tracks_on_learning_cycle_id", using: :btree
  add_index "learning_tracks", ["name"], name: "index_learning_tracks_on_name", using: :btree
  add_index "learning_tracks", ["parent_id"], name: "index_learning_tracks_on_parent_id", using: :btree
  add_index "learning_tracks", ["presentation_id"], name: "index_learning_tracks_on_presentation_id", using: :btree
  add_index "learning_tracks", ["status"], name: "index_learning_tracks_on_status", using: :btree
  add_index "learning_tracks", ["tags"], name: "index_learning_tracks_on_tags", using: :gin

  create_table "learning_tracks_skills", id: false, force: :cascade do |t|
    t.integer "learning_track_id"
    t.integer "skill_id"
  end

  add_index "learning_tracks_skills", ["learning_track_id", "skill_id"], name: "unique_index_learning_tracks_skills", unique: true, using: :btree
  add_index "learning_tracks_skills", ["learning_track_id"], name: "index_learning_tracks_skills_on_learning_track_id", using: :btree
  add_index "learning_tracks_skills", ["skill_id"], name: "index_learning_tracks_skills_on_skill_id", using: :btree

  create_table "likes", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "resource_type", null: false
    t.integer  "resource_id",   null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "likes", ["resource_id"], name: "index_likes_on_resource_id", using: :btree
  add_index "likes", ["resource_type", "resource_id", "user_id"], name: "likes_resource_type_resource_id_user_id_unique_index", unique: true, using: :btree
  add_index "likes", ["user_id"], name: "index_likes_on_user_id", using: :btree

  create_table "managers", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "role",                   default: 10
    t.integer  "user_id"
  end

  add_index "managers", ["confirmation_token"], name: "index_managers_on_confirmation_token", unique: true, using: :btree
  add_index "managers", ["email"], name: "index_managers_on_email", unique: true, using: :btree
  add_index "managers", ["reset_password_token"], name: "index_managers_on_reset_password_token", unique: true, using: :btree
  add_index "managers", ["unlock_token"], name: "index_managers_on_unlock_token", unique: true, using: :btree

  create_table "multimedia", force: :cascade do |t|
    t.string   "file"
    t.integer  "category_id"
    t.string   "tags",                           array: true
    t.integer  "user_id"
    t.string   "status"
    t.datetime "status_changed_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "link"
    t.string   "name",              default: ""
    t.json     "metadata"
    t.string   "multimedia_type"
  end

  add_index "multimedia", ["category_id"], name: "index_multimedia_on_category_id", using: :btree
  add_index "multimedia", ["multimedia_type"], name: "index_multimedia_on_multimedia_type", using: :btree
  add_index "multimedia", ["status"], name: "index_multimedia_on_status", using: :btree
  add_index "multimedia", ["tags"], name: "index_multimedia_on_tags", using: :gin
  add_index "multimedia", ["user_id"], name: "index_multimedia_on_user_id", using: :btree

  create_table "notification_configs", force: :cascade do |t|
    t.string   "notification_type"
    t.integer  "user_id"
    t.boolean  "can_email",         default: true
    t.boolean  "can_notification",  default: true
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "notification_configs", ["notification_type"], name: "index_notification_configs_on_notification_type", using: :btree
  add_index "notification_configs", ["user_id", "notification_type"], name: "index_notification_configs_on_user_id_and_notification_type", unique: true, using: :btree
  add_index "notification_configs", ["user_id"], name: "index_notification_configs_on_user_id", using: :btree

  create_table "notifications", force: :cascade do |t|
    t.integer  "notificable_id"
    t.string   "notificable_type"
    t.integer  "receiver_user_id"
    t.integer  "sender_user_id"
    t.string   "notification_type"
    t.boolean  "read"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "notifications", ["notificable_id", "notificable_type"], name: "index_notifications_on_notificable_id_and_notificable_type", using: :btree
  add_index "notifications", ["notificable_id"], name: "index_notifications_on_notificable_id", using: :btree
  add_index "notifications", ["notificable_type"], name: "index_notifications_on_notificable_type", using: :btree
  add_index "notifications", ["read"], name: "index_notifications_on_read", using: :btree
  add_index "notifications", ["receiver_user_id"], name: "index_notifications_on_receiver_user_id", using: :btree
  add_index "notifications", ["sender_user_id"], name: "index_notifications_on_sender_user_id", using: :btree

  create_table "origins", force: :cascade do |t|
    t.integer  "originable_id"
    t.string   "originable_type"
    t.string   "ip"
    t.string   "provider"
    t.string   "user_agent"
    t.string   "locale"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "origins", ["originable_id", "originable_type"], name: "index_origins_on_originable_id_and_originable_type", using: :btree
  add_index "origins", ["originable_id"], name: "index_origins_on_originable_id", using: :btree
  add_index "origins", ["originable_type"], name: "index_origins_on_originable_type", using: :btree

  create_table "pg_search_documents", force: :cascade do |t|
    t.tsvector "content"
    t.integer  "searchable_id",   null: false
    t.string   "searchable_type", null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "pg_search_documents", ["content"], name: "index_pg_search_documents_on_content", using: :gist
  add_index "pg_search_documents", ["searchable_type", "searchable_id"], name: "index_pg_search_documents_on_searchable_type_and_searchable_id", using: :btree

  create_table "photos", force: :cascade do |t|
    t.integer  "album_id"
    t.integer  "user_id"
    t.string   "image"
    t.string   "original_filename"
    t.string   "content_type"
    t.integer  "file_size"
    t.boolean  "visible"
    t.integer  "position"
    t.string   "caption"
    t.string   "tags",                           array: true
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.json     "assets_metadata"
    t.datetime "deleted_at"
  end

  add_index "photos", ["album_id"], name: "index_photos_on_album_id", using: :btree
  add_index "photos", ["deleted_at"], name: "index_photos_on_deleted_at", using: :btree
  add_index "photos", ["tags"], name: "index_photos_on_tags", using: :gin
  add_index "photos", ["user_id"], name: "index_photos_on_user_id", using: :btree
  add_index "photos", ["visible"], name: "index_photos_on_visible", using: :btree

  create_table "points", force: :cascade do |t|
    t.integer  "educational_institution_id", null: false
    t.integer  "achievement_id",             null: false
    t.integer  "score",                      null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "points", ["achievement_id"], name: "index_points_on_achievement_id", using: :btree
  add_index "points", ["educational_institution_id"], name: "index_points_on_educational_institution_id", using: :btree

  create_table "post_abuses", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
    t.integer  "post_id"
  end

  create_table "posts", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "content",        null: false
    t.integer  "parent_id"
    t.string   "status",         null: false
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "publisher_id",   null: false
    t.string   "publisher_type", null: false
  end

  add_index "posts", ["parent_id"], name: "index_posts_on_parent_id", using: :btree
  add_index "posts", ["user_id"], name: "index_posts_on_user_id", using: :btree

  create_table "presentation_slides", force: :cascade do |t|
    t.integer  "presentation_id"
    t.string   "title"
    t.string   "layout_type"
    t.string   "slide_type"
    t.json     "content"
    t.integer  "views_count",              default: 0
    t.integer  "position"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.json     "metadata"
    t.boolean  "hidden",                   default: false
    t.integer  "associated_resource_id"
    t.string   "associated_resource_type"
  end

  add_index "presentation_slides", ["associated_resource_id", "associated_resource_type"], name: "index_presentation_slides_associated_resource", using: :btree
  add_index "presentation_slides", ["presentation_id"], name: "index_presentation_slides_on_presentation_id", using: :btree

  create_table "presentation_to_document_tasks", force: :cascade do |t|
    t.integer "presentation_id"
    t.integer "status",          default: 0
    t.integer "retries",         default: 0
    t.string  "ins",                         array: true
  end

  add_index "presentation_to_document_tasks", ["presentation_id"], name: "index_presentation_to_document_tasks_on_presentation_id", using: :btree

  create_table "presentations", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "slides_count",      default: 0
    t.string   "status"
    t.datetime "status_changed_at"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "file"
    t.string   "presentation_type"
  end

  add_index "presentations", ["presentation_type"], name: "index_presentations_on_presentation_type", using: :btree
  add_index "presentations", ["status"], name: "index_presentations_on_status", using: :btree
  add_index "presentations", ["user_id"], name: "index_presentations_on_user_id", using: :btree

  create_table "question_templates", force: :cascade do |t|
    t.string   "title"
    t.string   "question_type"
    t.string   "question_for"
    t.string   "instructions"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.json     "choices"
    t.string   "subtitle"
  end

  add_index "question_templates", ["question_for"], name: "index_question_templates_on_question_for", using: :btree
  add_index "question_templates", ["question_type"], name: "index_question_templates_on_question_type", using: :btree

  create_table "question_user_answers", force: :cascade do |t|
    t.integer  "question_id"
    t.integer  "user_id"
    t.integer  "answer_index"
    t.string   "answer_text"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "question_user_answers", ["answer_index"], name: "index_question_user_answers_on_answer_index", using: :btree
  add_index "question_user_answers", ["question_id"], name: "index_question_user_answers_on_question_id", using: :btree
  add_index "question_user_answers", ["user_id"], name: "index_question_user_answers_on_user_id", using: :btree

  create_table "questions", force: :cascade do |t|
    t.string   "template_questionable_type"
    t.integer  "template_questionable_id"
    t.integer  "question_template_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "questions", ["question_template_id"], name: "index_questions_on_question_template_id", using: :btree
  add_index "questions", ["template_questionable_id", "template_questionable_type"], name: "index_questions_questionable_id_questionable_type", using: :btree
  add_index "questions", ["template_questionable_id"], name: "index_questions_on_template_questionable_id", using: :btree
  add_index "questions", ["template_questionable_type"], name: "index_questions_on_template_questionable_type", using: :btree

  create_table "resource_featured", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "user_id",       null: false
  end

  add_index "resource_featured", ["resource_type", "resource_id", "user_id"], name: "index_resource_featured_on_resource_and_user", unique: true, using: :btree
  add_index "resource_featured", ["user_id"], name: "index_resource_featured_on_user_id", using: :btree

  create_table "resource_histories", force: :cascade do |t|
    t.string   "historable_type"
    t.integer  "historable_id"
    t.string   "action_type"
    t.string   "new_status"
    t.string   "last_status"
    t.integer  "user_id"
    t.hstore   "user_data"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.hstore   "custom_data"
  end

  add_index "resource_histories", ["historable_id", "historable_type"], name: "index_resource_histories_on_historable_id_and_historable_type", using: :btree
  add_index "resource_histories", ["historable_id"], name: "index_resource_histories_on_historable_id", using: :btree
  add_index "resource_histories", ["historable_type"], name: "index_resource_histories_on_historable_type", using: :btree
  add_index "resource_histories", ["user_id"], name: "index_resource_histories_on_user_id", using: :btree

  create_table "skills", force: :cascade do |t|
    t.string   "name"
    t.integer  "position"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.text     "description"
  end

  add_index "skills", ["name"], name: "index_skills_on_name", using: :btree

  create_table "states", force: :cascade do |t|
    t.string   "name"
    t.string   "acronym"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "states", ["acronym"], name: "index_states_on_acronym", using: :btree

  create_table "student_classes", force: :cascade do |t|
    t.string   "name"
    t.integer  "admin_user_id"
    t.string   "code"
    t.datetime "code_expiration_date"
    t.string   "status"
    t.datetime "status_changed_at"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "educational_institution_id"
  end

  add_index "student_classes", ["admin_user_id"], name: "index_student_classes_on_admin_user_id", using: :btree
  add_index "student_classes", ["code"], name: "index_student_classes_on_code", using: :btree
  add_index "student_classes", ["code_expiration_date"], name: "index_student_classes_on_code_expiration_date", using: :btree
  add_index "student_classes", ["educational_institution_id"], name: "index_student_classes_on_educational_institution_id", using: :btree
  add_index "student_classes", ["name"], name: "index_student_classes_on_name", using: :btree

  create_table "student_classes_tracks", id: false, force: :cascade do |t|
    t.integer "student_class_id"
    t.integer "learning_track_id"
  end

  add_index "student_classes_tracks", ["learning_track_id"], name: "index_student_classes_tracks_on_learning_track_id", using: :btree
  add_index "student_classes_tracks", ["student_class_id", "learning_track_id"], name: "index_track_class_on_student_class_id_and_learning_track_id", unique: true, using: :btree
  add_index "student_classes_tracks", ["student_class_id"], name: "index_student_classes_tracks_on_student_class_id", using: :btree

  create_table "student_classes_users", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "student_class_id"
  end

  add_index "student_classes_users", ["student_class_id"], name: "index_student_classes_users_on_student_class_id", using: :btree
  add_index "student_classes_users", ["user_id", "student_class_id"], name: "index_student_classes_users_on_user_id_and_student_class_id", unique: true, using: :btree
  add_index "student_classes_users", ["user_id"], name: "index_student_classes_users_on_user_id", using: :btree

  create_table "student_deliveries", force: :cascade do |t|
    t.integer  "learning_delivery_id"
    t.integer  "user_id"
    t.integer  "student_class_id"
    t.string   "file"
    t.string   "format_type"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "link"
    t.integer  "learning_track_id"
    t.integer  "presentation_id"
    t.boolean  "correct",              default: false
    t.datetime "corrected_at"
  end

  add_index "student_deliveries", ["correct"], name: "index_student_deliveries_on_correct", using: :btree
  add_index "student_deliveries", ["learning_delivery_id", "user_id"], name: "index_unique_user_id_delivery_id_student_delivery", unique: true, using: :btree
  add_index "student_deliveries", ["learning_delivery_id"], name: "index_student_deliveries_on_learning_delivery_id", using: :btree
  add_index "student_deliveries", ["learning_track_id"], name: "index_student_deliveries_on_learning_track_id", using: :btree
  add_index "student_deliveries", ["link"], name: "index_student_deliveries_on_link", using: :btree
  add_index "student_deliveries", ["student_class_id"], name: "index_student_deliveries_on_student_class_id", using: :btree
  add_index "student_deliveries", ["user_id"], name: "index_student_deliveries_on_user_id", using: :btree

  create_table "student_delivery_groups", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "student_delivery_id"
  end

  add_index "student_delivery_groups", ["student_delivery_id"], name: "index_student_delivery_groups_on_student_delivery_id", using: :btree
  add_index "student_delivery_groups", ["user_id", "student_delivery_id"], name: "student_delivery_group_user_delivery_unique_index", unique: true, using: :btree
  add_index "student_delivery_groups", ["user_id"], name: "index_student_delivery_groups_on_user_id", using: :btree

  create_table "student_delivery_reviews", force: :cascade do |t|
    t.integer  "student_delivery_id"
    t.boolean  "correct"
    t.text     "comment"
    t.integer  "multiplier_user_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "student_delivery_reviews", ["correct"], name: "index_student_delivery_reviews_on_correct", using: :btree
  add_index "student_delivery_reviews", ["multiplier_user_id"], name: "index_student_delivery_reviews_on_multiplier_user_id", using: :btree
  add_index "student_delivery_reviews", ["student_delivery_id"], name: "index_student_delivery_reviews_on_student_delivery_id", using: :btree

  create_table "subscriber_discussions", force: :cascade do |t|
    t.integer  "user_subscription_id", null: false
    t.integer  "discussion_id",        null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "subscriber_discussions", ["user_subscription_id", "discussion_id"], name: "idx_subscriber_discussions_user_subscription_discussion_uniq", unique: true, using: :btree

  create_table "subscriber_posts", force: :cascade do |t|
    t.integer  "post_id"
    t.integer  "user_subscription_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "subscriber_posts", ["post_id", "user_subscription_id"], name: "post_id_user_subscription_id_unique_index", unique: true, using: :btree
  add_index "subscriber_posts", ["post_id"], name: "index_subscriber_posts_on_post_id", using: :btree
  add_index "subscriber_posts", ["user_subscription_id"], name: "index_subscriber_posts_on_user_subscription_id", using: :btree

  create_table "subscriber_surveys", force: :cascade do |t|
    t.integer  "user_subscription_id", null: false
    t.integer  "survey_id"
    t.string   "survey_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "subscriber_surveys", ["user_subscription_id", "survey_type", "survey_id"], name: "index_subscriber_surveys_on_user_subscription_and_survey", unique: true, using: :btree

  create_table "subscribers_group_members", force: :cascade do |t|
    t.integer  "subscribers_group_id"
    t.integer  "user_subscription_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "subscribers_group_members", ["subscribers_group_id", "user_subscription_id"], name: "subscribers_group_members_group_id_user_id_uniq_id", unique: true, using: :btree
  add_index "subscribers_group_members", ["subscribers_group_id"], name: "index_subscribers_group_members_on_subscribers_group_id", using: :btree
  add_index "subscribers_group_members", ["user_subscription_id"], name: "index_subscribers_group_members_on_user_subscription_id", using: :btree

  create_table "subscribers_groups", force: :cascade do |t|
    t.integer "user_id"
    t.string  "name",    null: false
  end

  add_index "subscribers_groups", ["user_id"], name: "index_subscribers_groups_on_user_id", using: :btree

  create_table "supporting_courses", force: :cascade do |t|
    t.string   "title"
    t.integer  "category_id"
    t.integer  "user_id"
    t.string   "tags",                           array: true
    t.integer  "presentation_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "status"
    t.datetime "status_changed_at"
    t.integer  "parent_id"
  end

  add_index "supporting_courses", ["category_id"], name: "index_supporting_courses_on_category_id", using: :btree
  add_index "supporting_courses", ["presentation_id"], name: "index_supporting_courses_on_presentation_id", using: :btree
  add_index "supporting_courses", ["tags"], name: "index_supporting_courses_on_tags", using: :gin
  add_index "supporting_courses", ["user_id"], name: "index_supporting_courses_on_user_id", using: :btree

  create_table "supporting_courses_skills", id: false, force: :cascade do |t|
    t.integer "supporting_course_id"
    t.integer "skill_id"
  end

  add_index "supporting_courses_skills", ["skill_id", "supporting_course_id"], name: "unique_index_supporting_courses_skills", unique: true, using: :btree
  add_index "supporting_courses_skills", ["skill_id"], name: "index_supporting_courses_skills_on_skill_id", using: :btree
  add_index "supporting_courses_skills", ["supporting_course_id"], name: "index_supporting_courses_skills_on_supporting_course_id", using: :btree

  create_table "survey_open_questions", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id",                     null: false
    t.string   "title",                       null: false
    t.integer  "questions_count", default: 0, null: false
    t.integer  "answers_count",   default: 0, null: false
    t.string   "video_link"
  end

  create_table "survey_polls", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id",    null: false
    t.string   "video_link"
  end

  create_table "survey_question_answers", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.integer  "survey_question_id"
    t.integer  "option_id"
    t.string   "answer_body"
  end

  create_table "survey_question_options", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "survey_question_id",             null: false
    t.string   "text",                           null: false
    t.integer  "position"
    t.integer  "answers_count",      default: 0, null: false
  end

  add_index "survey_question_options", ["survey_question_id", "text"], name: "index_survey_question_options_on_survey_question_id_and_text", unique: true, using: :btree

  create_table "survey_questions", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "questionable_id"
    t.string   "questionable_type"
    t.string   "title"
    t.integer  "position"
    t.integer  "options_count",     default: 0, null: false
    t.integer  "answers_count",     default: 0, null: false
  end

  create_table "tracking_events", force: :cascade do |t|
    t.integer  "presentation_slide_id"
    t.integer  "event_type",                 default: 1
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "enrolled_learning_track_id"
  end

  add_index "tracking_events", ["enrolled_learning_track_id"], name: "index_tracking_events_on_enrolled_learning_track_id", using: :btree
  add_index "tracking_events", ["event_type"], name: "index_tracking_events_on_event_type", using: :btree
  add_index "tracking_events", ["presentation_slide_id"], name: "index_tracking_events_on_presentation_slide_id", using: :btree

  create_table "user_achievements", force: :cascade do |t|
    t.integer  "user_id",        null: false
    t.integer  "achievement_id", null: false
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "resource_id",    null: false
    t.string   "resource_type",  null: false
  end

  add_index "user_achievements", ["achievement_id"], name: "index_user_achievements_on_achievement_id", using: :btree
  add_index "user_achievements", ["user_id", "achievement_id", "resource_id"], name: "index_user_achievements_on_user_achievement_resource", unique: true, using: :btree
  add_index "user_achievements", ["user_id"], name: "index_user_achievements_on_user_id", using: :btree

  create_table "user_badges", force: :cascade do |t|
    t.integer  "user_id",    null: false
    t.integer  "badge_id",   null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_badges", ["badge_id"], name: "index_user_badges_on_badge_id", using: :btree
  add_index "user_badges", ["user_id", "badge_id"], name: "index_user_badges_on_user_id_and_badge_id", unique: true, using: :btree
  add_index "user_badges", ["user_id"], name: "index_user_badges_on_user_id", using: :btree

  create_table "user_preferences", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "preference_type", null: false
    t.string   "title",           null: false
    t.string   "author"
    t.string   "link"
  end

  add_index "user_preferences", ["user_id"], name: "index_user_preferences_on_user_id", using: :btree

  create_table "user_subscriptions", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "publisher_id"
    t.string   "publisher_type"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "user_subscriptions", ["publisher_id"], name: "index_user_subscriptions_on_publisher_id", using: :btree
  add_index "user_subscriptions", ["user_id"], name: "index_user_subscriptions_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "username"
    t.string   "profile_type"
    t.text     "about"
    t.string   "gender"
    t.date     "birthday"
    t.string   "password_digest"
    t.datetime "reset_password_sent_at"
    t.datetime "password_reseted_at"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "reset_password_token"
    t.string   "profile_image"
    t.integer  "status",                 default: 0
    t.string   "oauth_provider_uid"
    t.string   "oauth_provider"
    t.string   "phone_number"
    t.string   "phone_area_code"
    t.datetime "password_updated_at"
    t.datetime "tof_accepted_at"
    t.datetime "deleted_at"
  end

  add_index "users", ["deleted_at"], name: "index_users_on_deleted_at", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", using: :btree
  add_index "users", ["profile_type"], name: "index_users_on_profile_type", using: :btree
  add_index "users", ["username"], name: "index_users_on_username", using: :btree

  create_table "visibilities", force: :cascade do |t|
    t.integer "resource_id"
    t.string  "resource_type"
    t.string  "visibility_type", null: false
    t.string  "visibility_id"
  end

  add_foreign_key "addresses", "cities"
  add_foreign_key "attachments", "posts", on_delete: :cascade
  add_foreign_key "authorizations", "users", on_delete: :cascade
  add_foreign_key "category_resources", "categories"
  add_foreign_key "challenge_deliveries", "challenges"
  add_foreign_key "challenge_deliveries", "presentations"
  add_foreign_key "challenge_subscriptions", "challenges", on_delete: :cascade
  add_foreign_key "challenge_subscriptions", "users", on_delete: :cascade
  add_foreign_key "challenges", "presentations", on_delete: :cascade
  add_foreign_key "channels", "learning_tracks"
  add_foreign_key "channels", "student_classes"
  add_foreign_key "cities", "states"
  add_foreign_key "comments", "users"
  add_foreign_key "conversion_statuses", "presentations"
  add_foreign_key "conversions", "presentations"
  add_foreign_key "conversions", "users"
  add_foreign_key "cycle_plannings", "learning_cycles", on_delete: :cascade
  add_foreign_key "discussion_answers", "discussions", on_delete: :cascade
  add_foreign_key "discussion_answers", "users", on_delete: :cascade
  add_foreign_key "discussion_options", "discussions", on_delete: :cascade
  add_foreign_key "discussion_views", "discussions", on_delete: :cascade
  add_foreign_key "discussion_views", "users", on_delete: :cascade
  add_foreign_key "discussions", "users"
  add_foreign_key "game_complete_sentences", "users"
  add_foreign_key "game_group_items", "users"
  add_foreign_key "game_groups", "game_group_items", on_delete: :cascade
  add_foreign_key "game_item_answers", "game_items"
  add_foreign_key "game_item_answers", "users"
  add_foreign_key "game_items", "game_related_items", on_delete: :cascade
  add_foreign_key "game_open_questions", "users"
  add_foreign_key "game_polls", "users"
  add_foreign_key "game_question_answers", "game_question_options", column: "option_id"
  add_foreign_key "game_question_answers", "game_questions"
  add_foreign_key "game_question_options", "game_questions", on_delete: :cascade
  add_foreign_key "game_quizzes", "users"
  add_foreign_key "game_related_items", "users"
  add_foreign_key "game_true_false", "users"
  add_foreign_key "game_user_scores", "student_classes"
  add_foreign_key "game_user_scores", "users"
  add_foreign_key "game_word_answers", "users"
  add_foreign_key "game_word_searches", "users"
  add_foreign_key "images_to_pdf_tasks", "users"
  add_foreign_key "learning_cycles", "categories"
  add_foreign_key "learning_cycles", "users"
  add_foreign_key "learning_deliveries", "learning_dynamics", on_delete: :cascade
  add_foreign_key "learning_dynamics", "categories"
  add_foreign_key "learning_dynamics", "users"
  add_foreign_key "learning_track_reviews", "learning_tracks", on_delete: :cascade
  add_foreign_key "learning_track_reviews", "student_classes"
  add_foreign_key "learning_track_reviews", "users"
  add_foreign_key "learning_tracks", "categories"
  add_foreign_key "learning_tracks", "learning_cycles", on_delete: :cascade
  add_foreign_key "likes", "users"
  add_foreign_key "multimedia", "categories"
  add_foreign_key "multimedia", "users"
  add_foreign_key "notification_configs", "users", on_delete: :cascade
  add_foreign_key "photos", "albums"
  add_foreign_key "photos", "users"
  add_foreign_key "points", "achievements"
  add_foreign_key "points", "educational_institutions"
  add_foreign_key "posts", "users"
  add_foreign_key "presentation_slides", "presentations"
  add_foreign_key "presentations", "users"
  add_foreign_key "question_user_answers", "questions"
  add_foreign_key "resource_featured", "users", on_delete: :cascade
  add_foreign_key "resource_histories", "users"
  add_foreign_key "student_classes", "educational_institutions"
  add_foreign_key "student_classes", "users", column: "admin_user_id"
  add_foreign_key "student_classes_tracks", "learning_tracks", on_delete: :cascade
  add_foreign_key "student_classes_tracks", "student_classes"
  add_foreign_key "student_classes_users", "student_classes"
  add_foreign_key "student_classes_users", "users"
  add_foreign_key "student_deliveries", "learning_deliveries"
  add_foreign_key "student_deliveries", "learning_tracks"
  add_foreign_key "student_deliveries", "student_classes"
  add_foreign_key "student_deliveries", "users"
  add_foreign_key "student_delivery_groups", "student_deliveries", on_delete: :cascade
  add_foreign_key "student_delivery_groups", "users", on_delete: :cascade
  add_foreign_key "student_delivery_reviews", "student_deliveries"
  add_foreign_key "subscriber_discussions", "discussions", on_delete: :cascade
  add_foreign_key "subscriber_discussions", "user_subscriptions", on_delete: :cascade
  add_foreign_key "subscriber_posts", "posts", on_delete: :cascade
  add_foreign_key "subscriber_posts", "user_subscriptions", on_delete: :cascade
  add_foreign_key "subscriber_surveys", "user_subscriptions", on_delete: :cascade
  add_foreign_key "subscribers_group_members", "subscribers_groups", on_delete: :cascade
  add_foreign_key "subscribers_group_members", "user_subscriptions", on_delete: :cascade
  add_foreign_key "subscribers_groups", "users", on_delete: :cascade
  add_foreign_key "supporting_courses", "categories"
  add_foreign_key "supporting_courses", "users"
  add_foreign_key "survey_open_questions", "users", on_delete: :cascade
  add_foreign_key "survey_polls", "users", on_delete: :cascade
  add_foreign_key "survey_question_answers", "survey_questions", on_delete: :cascade
  add_foreign_key "survey_question_answers", "users"
  add_foreign_key "survey_question_options", "survey_questions", on_delete: :cascade
  add_foreign_key "user_achievements", "achievements", on_delete: :cascade
  add_foreign_key "user_achievements", "users", on_delete: :cascade
  add_foreign_key "user_badges", "badges", on_delete: :cascade
  add_foreign_key "user_badges", "users", on_delete: :cascade
  add_foreign_key "user_preferences", "users", on_delete: :cascade
  add_foreign_key "user_subscriptions", "users"
end
