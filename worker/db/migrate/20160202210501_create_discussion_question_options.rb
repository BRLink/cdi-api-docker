class CreateDiscussionQuestionOptions < ActiveRecord::Migration
  def change
    create_table :discussion_question_options do |t|
      t.timestamps
      t.belongs_to :discussion_question, null: false
      t.string :text, null: false
      t.integer :position
      t.integer :answers_count, null: false, default: 0
    end
    add_foreign_key :discussion_question_options, :discussion_questions, on_delete: :cascade
    add_index :discussion_question_options, [:discussion_question_id, :text], unique: true, name: 'idx_discussion_question_opts_on_discussion_id_and_text'
  end
end
