class UpdateChallengeColumns < ActiveRecord::Migration
  def change
    rename_column :challenges, :about, :description
    rename_column :challenges, :delivery_format, :format_type
    rename_column :challenges, :delivery_type, :delivery_kind
    rename_column :challenges, :end_date, :delivery_date
    rename_column :challenges, :featured_image, :cover_image

    reversible do |dir|
      change_table :challenges do |t|
        dir.up   { t.change :format_type, :string }
        dir.down { t.change :format_type, :integer }
      end
    end
  end
end
