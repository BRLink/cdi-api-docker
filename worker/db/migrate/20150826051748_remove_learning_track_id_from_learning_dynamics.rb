class RemoveLearningTrackIdFromLearningDynamics < ActiveRecord::Migration
  def change
    remove_column :learning_dynamics, :learning_track_id, :integer, index: true
  end
end
