class CreateLearningTracksSkills < ActiveRecord::Migration
  def change
    create_table :learning_tracks_skills, id: false do |t|
      t.integer :learning_track_id
      t.integer :skill_id
    end
    # TODO: add indexes?
  end
end
