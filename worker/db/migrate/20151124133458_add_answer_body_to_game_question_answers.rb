class AddAnswerBodyToGameQuestionAnswers < ActiveRecord::Migration
  def change
    add_column :game_question_answers, :answer_body, :text
  end
end
