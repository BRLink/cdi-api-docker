class AddEducationalInstitutionToStudentClasses < ActiveRecord::Migration
  def change
    add_reference :student_classes, :educational_institution, index: true
    add_foreign_key :student_classes, :educational_institutions
  end
end
