class CreateUserSubscribersGroup < ActiveRecord::Migration
  def change
    create_table :user_subscribers_groups do |t|
      t.references :user, index: true
      t.string :name, null: false
    end
  end
end
