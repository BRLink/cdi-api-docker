class AddStudentClassIdToCoursedLearningTracks < ActiveRecord::Migration
  def change
    add_column :coursed_learning_tracks, :student_class_id, :integer
    add_index :coursed_learning_tracks, :student_class_id
  end
end
