class AddChallengeColumns < ActiveRecord::Migration
  def change
    add_column :challenges, :delivery_title, :string, null: false, default: ''
  end
end
