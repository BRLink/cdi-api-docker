class AddStatusToEducationalInstitutionsUsers < ActiveRecord::Migration
  def change
    add_column :educational_institutions_users, :status, :integer, default: 100
    add_column :educational_institutions_users, :id, :primary_key
  end
end
