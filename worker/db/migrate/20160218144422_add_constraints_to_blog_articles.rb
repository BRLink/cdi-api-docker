class AddConstraintsToBlogArticles < ActiveRecord::Migration
  def change
    change_column_null :blog_articles, :title, false
    change_column_null :blog_articles, :link, false
    change_column_null :blog_articles, :description, false
    change_column_null :blog_articles, :pubDate, false
    change_column_null :blog_articles, :guid, false

    add_index :blog_articles, :guid, unique: true
  end
end
