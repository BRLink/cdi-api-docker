class AddFileToPresentations < ActiveRecord::Migration
  def change
    add_column :presentations, :file, :string
  end
end
