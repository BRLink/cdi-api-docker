class CreateChatRooms < ActiveRecord::Migration
  def change
    create_table :chat_rooms do |t|
      t.integer :user_id, null: false
      t.string :name
      t.integer :room_type, default: 0, null: false
      t.timestamps null: false
    end
  end
end
