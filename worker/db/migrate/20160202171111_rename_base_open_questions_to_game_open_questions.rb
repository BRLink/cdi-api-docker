class RenameBaseOpenQuestionsToGameOpenQuestions < ActiveRecord::Migration
  def change
    rename_table :base_open_questions, :game_open_questions
    remove_column :game_open_questions, :type
  end
end
