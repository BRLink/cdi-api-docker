class AddGroupSizeToChallenges < ActiveRecord::Migration
  def change
    add_column :challenges, :group_size, :integer
  end
end
