class CreateResourceFeatured < ActiveRecord::Migration
  def change
    create_table :resource_featured do |t|
      t.timestamps
      t.belongs_to :resource, polymorphic: true, null: false
      t.belongs_to :user, index: true, null: false
    end
    add_foreign_key :resource_featured, :users, on_delete: :cascade
    add_index :resource_featured, [:resource_type, :resource_id, :user_id], unique: true, name: 'index_resource_featured_on_resource_and_user'
  end
end
