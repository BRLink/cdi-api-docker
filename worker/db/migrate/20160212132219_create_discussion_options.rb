class CreateDiscussionOptions < ActiveRecord::Migration
  def change
    create_table :discussion_options do |t|
      t.belongs_to :discussion, null: false
      t.string :text, null: false
      t.integer :position
      t.integer :answers_count, null: false, default: 0
      t.timestamps null: false
    end
    add_foreign_key :discussion_options, :discussions, on_delete: :cascade
    add_index :discussion_options, [:discussion_id, :text], unique: true
  end
end
