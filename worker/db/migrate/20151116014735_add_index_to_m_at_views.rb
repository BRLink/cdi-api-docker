class AddIndexToMAtViews < ActiveRecord::Migration
  def change
    add_index :track_enroll_reports_matview, :student_class_id
    add_index :track_enroll_reports_matview, :learning_track_id
  end
end
