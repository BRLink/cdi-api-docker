class ChangeDefaultValueOfCorrectInStudentDeliveries < ActiveRecord::Migration
  def change
    change_column :student_deliveries, :correct, :boolean, :default => false
    add_column :student_deliveries, :corrected_at, :datetime
  end
end
