class CreateSurveyOpenQuestions < ActiveRecord::Migration
  def change
    create_table :survey_open_questions do |t|
      t.timestamps
      t.belongs_to :user, null: false
      t.string :title, null: false
      t.integer :questions_count, null: false, default: 0
      t.integer :answers_count, null: false, default: 0
      t.string :video_link
    end
    add_foreign_key :survey_open_questions, :users, on_delete: :cascade
  end
end


