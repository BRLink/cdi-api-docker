class CreatePgSearchDocuments < ActiveRecord::Migration
  def self.up
    say_with_time("Creating table for pg_search multisearch") do
      create_table :pg_search_documents do |t|
        t.tsvector :content
        t.belongs_to :searchable, polymorphic: true, index: true, null: false
        t.timestamps null: false
      end
      add_index :pg_search_documents, :content, using: 'gist'
    end
  end

  def self.down
    say_with_time("Dropping table for pg_search multisearch") do
      drop_table :pg_search_documents
    end
  end
end
