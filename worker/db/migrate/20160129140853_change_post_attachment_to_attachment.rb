class ChangePostAttachmentToAttachment < ActiveRecord::Migration
  def change
    rename_table :post_attachments, :attachments
    add_column :attachments, :attachable_id, :integer
    add_column :attachments, :attachable_type, :string
  end
end
