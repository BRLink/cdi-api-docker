class AddNullableToUserPreferencesAuthor < ActiveRecord::Migration
  def change
    change_column_null(:user_preferences, :author, true)
  end
end
