class CreateGeneralRankingMaterializedView < ActiveRecord::Migration
  def up
    execute 'DROP MATERIALIZED VIEW IF EXISTS general_user_ranking_matview'
    execute <<-MATVIEW
    CREATE MATERIALIZED VIEW general_user_ranking_matview AS
      SELECT
             row_number() over(ORDER BY sum(a.default_score) DESC) as position,
             u.id as user_id,
             sum(a.default_score) AS default_score
        FROM users u
  INNER JOIN user_achievements u_a
          ON u_a.user_id = u.id
  INNER JOIN achievements a
          ON u_a.achievement_id = a.id
    GROUP BY u.id
    ORDER BY default_score DESC;

    CREATE UNIQUE INDEX idx_general_user_ranking_matview_position
        ON general_user_ranking_matview (position);
    CREATE UNIQUE INDEX idx_general_user_ranking_matview_user
        ON general_user_ranking_matview (user_id);

    MATVIEW
  end

  def down
    execute 'DROP MATERIALIZED VIEW IF EXISTS general_user_ranking_matview'
  end
end
