class CreateCategoryResources < ActiveRecord::Migration
  def change
    create_table :category_resources, id: false do |t|
      t.integer :resource_id
      t.string :resource_type
      t.references :category

      t.timestamps null: false
    end
    add_foreign_key :category_resources, :categories
    add_index :category_resources, [:resource_id, :resource_type, :category_id], unique: true, name: 'index_category_resources_on_resource_id_type_and_category'
  end
end
