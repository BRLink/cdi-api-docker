class AddUniqueKeyToAchievementsAction < ActiveRecord::Migration
  def change
    add_index :achievements, :action, unique: true
  end
end
