class CreateImagesToPdfTasks < ActiveRecord::Migration
  def change
    create_table :images_to_pdf_tasks do |t|
      t.string :ins, array: true
      t.string :out, null: false
      t.integer :status, default: 0
      t.integer :progress, default: 0

      t.timestamps null: false
    end
  end
end
