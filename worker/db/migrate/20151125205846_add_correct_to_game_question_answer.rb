class AddCorrectToGameQuestionAnswer < ActiveRecord::Migration
  def change
    add_column :game_question_answers, :correct, :boolean
    add_index :game_question_answers, :correct
  end
end
