class RenameGameQuestionsToBaseQuestions < ActiveRecord::Migration
  def change
    rename_table :game_questions, :base_questions
    add_column :base_questions, :type, :string, index: true
  end
end
