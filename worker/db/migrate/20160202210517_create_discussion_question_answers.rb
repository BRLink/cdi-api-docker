class CreateDiscussionQuestionAnswers < ActiveRecord::Migration
  def change
    create_table :discussion_question_answers do |t|
      t.timestamps
      t.belongs_to :user
      t.belongs_to :discussion_question
      t.integer :option_id
      t.string :answer_body
    end
    add_foreign_key :discussion_question_answers, :users, on_delete: :cascade
    add_foreign_key :discussion_question_answers, :discussion_questions, on_delete: :cascade
  end
end
