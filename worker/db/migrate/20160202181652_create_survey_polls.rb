class CreateSurveyPolls < ActiveRecord::Migration
  def change
    create_table :survey_polls do |t|
      t.timestamps
      t.belongs_to :user, null: false
      t.string :video_link
    end
    add_foreign_key :survey_polls, :users, on_delete: :cascade
  end
end
