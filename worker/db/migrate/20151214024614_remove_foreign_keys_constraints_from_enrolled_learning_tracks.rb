class RemoveForeignKeysConstraintsFromEnrolledLearningTracks < ActiveRecord::Migration
  def up
    remove_foreign_key :enrolled_learning_tracks, :users
    remove_foreign_key :enrolled_learning_tracks, :learning_tracks
    remove_reference :tracking_events, :enrolled_learning_track
  end

  def down
    add_reference :tracking_events, :enrolled_learning_tracks, index: true
    add_foreign_key :enrolled_learning_tracks, :users, on_delete: :cascade
    add_foreign_key :enrolled_learning_tracks, :learning_tracks, on_delete: :cascade
  end
end
