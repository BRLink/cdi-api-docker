class CreateSurveyQuestions < ActiveRecord::Migration
  def change
    create_table :survey_questions do |t|
      t.timestamps
      t.belongs_to :questionable, polymorphic: true
      t.string :title, null: true
      t.integer :position
      t.integer :options_count, null: false, default: 0
      t.integer :answers_count, null: false, default: 0
    end
  end
end
