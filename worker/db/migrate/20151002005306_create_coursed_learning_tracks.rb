class CreateCoursedLearningTracks < ActiveRecord::Migration
  def change
    create_table :coursed_learning_tracks do |t|
      t.references :user, index: true
      t.references :learning_track, index: true
      t.string :status
      t.datetime :status_changed_at

      t.timestamps null: false
    end
    add_index :coursed_learning_tracks, :status
    add_index :coursed_learning_tracks, [:user_id, :learning_track_id], unique: true

    add_foreign_key :coursed_learning_tracks, :users, on_delete: :cascade
    add_foreign_key :coursed_learning_tracks, :learning_tracks, on_delete: :cascade
  end
end
