class ChangeIndexesInTrackingEvents < ActiveRecord::Migration
  def change
    remove_index :tracking_events, name: 'slide_event_type_slide_id_unique', column: [:event_type, :presentation_slide_id]
    add_index :tracking_events, [:event_type, :presentation_slide_id, :enrolled_learning_track_id], name: 'index_unique_event_type_enrolled_track_slide_id', unique: true
  end
end
