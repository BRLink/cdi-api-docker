class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :username
      t.string :cdi_code
      t.string :role
      t.text :about
      t.string :gender
      t.date :birthday
      t.string :password_digest
      t.datetime :reset_password_sent_at
      t.datetime :password_reseted_at

      t.timestamps null: false
    end
    add_index :users, :email
    add_index :users, :cdi_code
  end
end
