class CreateStudentDeliveryGroups < ActiveRecord::Migration
  def change
    create_table :student_delivery_groups, id: false do |t|
      t.references :user, index: true
      t.references :student_delivery, index: true
    end

    add_index :student_delivery_groups, [:user_id, :student_delivery_id], unique: true, name: 'student_delivery_group_user_delivery_unique_index'

    add_foreign_key :student_delivery_groups, :users, on_delete: :cascade
    add_foreign_key :student_delivery_groups, :student_deliveries, on_delete: :cascade
  end
end
