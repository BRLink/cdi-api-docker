class CreateChallengeDeliveryGroups < ActiveRecord::Migration
  def change
    create_table :challenge_delivery_groups do |t|
      t.references :challenge_delivery, index: true, null: false
      t.references :user, index: true, null: false

      t.timestamps null: false
    end
    add_foreign_key :challenge_delivery_groups, :challenge_deliveries, on_delete: :cascade
    add_foreign_key :challenge_delivery_groups, :users
  end
end
