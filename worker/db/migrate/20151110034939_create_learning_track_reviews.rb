class CreateLearningTrackReviews < ActiveRecord::Migration
  def change
    create_table :learning_track_reviews do |t|
      t.references :learning_track, index: true
      t.references :user, index: true
      t.text :comment
      t.integer :review_type

      t.timestamps null: false
    end
    add_index :learning_track_reviews, :review_type
    add_index :learning_track_reviews, [:learning_track_id, :user_id], unique: true
    add_foreign_key :learning_track_reviews, :learning_tracks
    add_foreign_key :learning_track_reviews, :users
  end
end
