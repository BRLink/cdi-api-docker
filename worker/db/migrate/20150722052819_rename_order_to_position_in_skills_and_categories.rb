class RenameOrderToPositionInSkillsAndCategories < ActiveRecord::Migration
  def change
    rename_column :skills, :order, :position
    rename_column :categories, :order, :position
  end
end
