class RenameSubscriptionChallenges < ActiveRecord::Migration
  def change
    rename_table :subscription_challenges, :challenge_subscriptions
  end
end
