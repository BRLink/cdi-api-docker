class AddUniqueConstraintOnLikes < ActiveRecord::Migration
  def change
    add_index :likes, [:resource_type, :resource_id, :user_id], unique: true, name: 'likes_resource_type_resource_id_user_id_unique_index'
  end
end
