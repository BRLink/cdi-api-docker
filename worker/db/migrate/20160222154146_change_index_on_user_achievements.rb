class ChangeIndexOnUserAchievements < ActiveRecord::Migration
  def change
    remove_index :user_achievements, name: :index_user_achievements_on_user_id_and_achievement_id
    add_index :user_achievements, [:user_id, :achievement_id, :resource_id],
              unique: true,
              name: :index_user_achievements_on_user_achievement_resource
  end
end
