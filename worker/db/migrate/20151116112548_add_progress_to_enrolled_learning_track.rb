class AddProgressToEnrolledLearningTrack < ActiveRecord::Migration
  def up
    add_column :enrolled_learning_tracks, :progress, :float, default: 0
    add_index :enrolled_learning_tracks, :progress

    # EnrolledLearningTrack.find_each do |enroll|
    #   enroll.update_progress
    # end
  end

  def down
    remove_index :enrolled_learning_tracks, :progress
    remove_column :enrolled_learning_tracks, :progress
  end
end
