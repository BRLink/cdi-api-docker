class CreateBlogMedia < ActiveRecord::Migration
  def change
    create_table :blog_media do |t|
      t.string :file
      t.string :media_type
      t.references :user

      t.timestamps null: false
    end
  end
end
