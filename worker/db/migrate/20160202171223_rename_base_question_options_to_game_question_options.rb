class RenameBaseQuestionOptionsToGameQuestionOptions < ActiveRecord::Migration
  def change
    rename_table :base_question_options, :game_question_options
    rename_column :game_question_options, :base_question_id, :game_question_id
    remove_column :game_question_options, :type
  end
end
