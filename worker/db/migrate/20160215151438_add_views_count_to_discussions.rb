class AddViewsCountToDiscussions < ActiveRecord::Migration
  def change
    add_column :discussions, :views_count, :integer, null: false, default: 0
  end
end
