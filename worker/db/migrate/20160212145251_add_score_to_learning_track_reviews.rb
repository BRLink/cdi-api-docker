class AddScoreToLearningTrackReviews < ActiveRecord::Migration
  def change
    add_column :learning_track_reviews, :score, :integer, :null => false, :default => 1
  end
end
