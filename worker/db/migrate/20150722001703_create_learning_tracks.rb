class CreateLearningTracks < ActiveRecord::Migration
  def change
    create_table :learning_tracks do |t|
      t.references :learning_cycle, index: true
      t.string :name
      t.string :tags, array: true

      t.timestamps null: false
    end

    add_index :learning_tracks, :name
    add_index :learning_tracks, :tags, using: 'gin'
    add_foreign_key :learning_tracks, :learning_cycles

  end
end
