class AddUserTypeToAchievements < ActiveRecord::Migration
  def change
    add_column :achievements, :user_type, :json, null: false
  end
end
