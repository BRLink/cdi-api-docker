class RenameLearningTrackStudentClassesTableToClassesTracks < ActiveRecord::Migration
  def change
    rename_table :learning_tracks_student_classes, :student_classes_tracks
  end
end
