class DropBlogArticles < ActiveRecord::Migration
  def up
    drop_table :blog_articles
  end

  def down
    fail ActiveRecord::IrreversibleMigration
  end
end
