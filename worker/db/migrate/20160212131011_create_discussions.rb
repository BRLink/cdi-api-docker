class CreateDiscussions < ActiveRecord::Migration
  def change
    create_table :discussions do |t|
      t.belongs_to :user, null: false
      t.belongs_to :publisher, polymorphic: true, null: false, index: true
      t.belongs_to :discussion_poll
      t.belongs_to :cover
      t.belongs_to :parent
      t.string :discussion_type, null: false # simple, poll
      t.string :title, null: false
      t.string :content
      t.string :video_link
      t.string :tags, array: true
      t.integer :answers_count, null: false, default: 0
      t.integer :comments_count, null: false, default: 0
      t.integer :likes_count, null: false, default: 0
      t.integer :featured_count, null: false, default: 0
      t.integer :children_count, null: false, default: 0
      t.timestamps null: false
    end
    add_foreign_key :discussions, :users
    add_index :discussions, :tags, using: 'gin'
  end
end
