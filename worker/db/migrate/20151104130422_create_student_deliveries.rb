class CreateStudentDeliveries < ActiveRecord::Migration
  def change
    create_table :student_deliveries do |t|
      t.references :learning_delivery, index: true
      t.references :user, index: true
      t.references :student_class, index: true
      t.string :file
      t.string :format_type

      t.timestamps null: false
    end

    add_index :student_deliveries, [:learning_delivery_id, :user_id], unique: true, name: 'index_unique_user_id_delivery_id_student_delivery'
    add_foreign_key :student_deliveries, :learning_deliveries
    add_foreign_key :student_deliveries, :users
    add_foreign_key :student_deliveries, :student_classes
  end
end
