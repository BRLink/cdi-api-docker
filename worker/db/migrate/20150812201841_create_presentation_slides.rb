class CreatePresentationSlides < ActiveRecord::Migration
  def change
    create_table :presentation_slides do |t|
      t.references :presentation, index: true
      t.string :title
      t.string :layout_type
      t.string :slide_type
      t.json :content
      t.integer :views_count
      t.integer :position

      t.timestamps null: false
    end
    add_foreign_key :presentation_slides, :presentations
  end
end
