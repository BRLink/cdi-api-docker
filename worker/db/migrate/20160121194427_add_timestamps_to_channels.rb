class AddTimestampsToChannels < ActiveRecord::Migration
  def change
    add_timestamps :channels, null: false
  end
end
