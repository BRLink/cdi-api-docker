class AddTagsToLearningCycle < ActiveRecord::Migration
  def change
    change_table :learning_cycles do |t|
      t.string :tags, array: true
    end
    add_index :learning_cycles, :tags, using: 'gin'
  end
end
