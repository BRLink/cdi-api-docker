class ChangeUniqueIndexesInTrackingEvents < ActiveRecord::Migration
  def change
    remove_index :coursed_track_events, name: 'slide_coursed_learning_track_id_unique', column: [:coursed_learning_track_id, :presentation_slide_id]
    add_index :coursed_track_events, [:event_type, :presentation_slide_id], name: 'slide_event_type_slide_id_unique', unique: true
  end
end
