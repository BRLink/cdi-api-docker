class CreateGameRelatedItems < ActiveRecord::Migration
  def change
    create_table :game_related_items do |t|
      t.references :user, index: true
      t.integer :items_count
      t.string :status
      t.datetime :status_changed_at

      t.timestamps null: false
    end
    add_index :game_related_items, :status
    add_foreign_key :game_related_items, :users
  end
end
