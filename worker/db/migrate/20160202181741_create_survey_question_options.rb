class CreateSurveyQuestionOptions < ActiveRecord::Migration
  def change
    create_table :survey_question_options do |t|
      t.timestamps
      t.belongs_to :survey_question, null: false
      t.string :text, null: false
      t.integer :position
      t.integer :answers_count, null: false, default: 0
    end
    add_foreign_key :survey_question_options, :survey_questions, on_delete: :cascade
    add_index :survey_question_options, [:survey_question_id, :text], unique: true
  end
end
