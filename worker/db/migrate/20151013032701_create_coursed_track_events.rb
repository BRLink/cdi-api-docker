class CreateCoursedTrackEvents < ActiveRecord::Migration
  def change
    create_table :coursed_track_events do |t|
      t.references :coursed_learning_track, index: true
      t.integer :presentation_slide_id
      t.integer :event_type, default: 1

      t.timestamps null: false
    end

    add_index :coursed_track_events, :presentation_slide_id
    add_index :coursed_track_events, [:coursed_learning_track_id, :presentation_slide_id], name: 'slide_coursed_learning_track_id_unique', unique: true

    add_index :coursed_track_events, :event_type
    add_foreign_key :coursed_track_events, :coursed_learning_tracks
  end

end
