class AddCategoryToLearningTracks < ActiveRecord::Migration
  def change
    add_reference :learning_tracks, :category, index: true
    add_foreign_key :learning_tracks, :categories
  end
end
