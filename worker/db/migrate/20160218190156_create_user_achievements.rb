class CreateUserAchievements < ActiveRecord::Migration
  def change
    create_table :user_achievements do |t|
      t.references :user,        null: false, index: true
      t.references :achievement, null: false, index: true

      t.timestamps null: false
    end
    add_foreign_key :user_achievements, :users,        on_delete: :cascade
    add_foreign_key :user_achievements, :achievements, on_delete: :cascade
    add_index       :user_achievements, [:user_id, :achievement_id], unique: true
  end
end
