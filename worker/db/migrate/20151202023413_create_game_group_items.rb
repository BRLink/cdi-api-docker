class CreateGameGroupItems < ActiveRecord::Migration
  def change
    create_table :game_group_items do |t|
      t.references :user, index: true

      t.timestamps null: false
    end

    add_foreign_key :game_group_items, :users
  end
end
