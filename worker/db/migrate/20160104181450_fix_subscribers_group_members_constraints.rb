class FixSubscribersGroupMembersConstraints < ActiveRecord::Migration
  def change
    rename_column :subscribers_group_members, :user_subscribers_group_id, :subscribers_group_id
    add_foreign_key :subscribers_group_members, :subscribers_groups, on_delete: :cascade
    add_foreign_key :subscribers_group_members, :user_subscriptions, on_delete: :cascade
    add_index :subscribers_group_members, [:subscribers_group_id, :user_subscription_id], unique: true, name: 'subscribers_group_members_group_id_user_id_uniq_id'
  end
end
