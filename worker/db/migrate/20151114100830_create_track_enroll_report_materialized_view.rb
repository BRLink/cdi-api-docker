class CreateTrackEnrollReportMaterializedView < ActiveRecord::Migration
  def up
    execute 'DROP MATERIALIZED VIEW IF EXISTS track_enroll_reports_matview'
    execute <<-SQL
      CREATE MATERIALIZED VIEW track_enroll_reports_matview AS
        SELECT
          COUNT(*) as total_by_status,
          elt.student_class_id,
          elt.status,
          elt.learning_track_id,
          -- Percentage for class by status
          round(
          -- Amount
          ((COUNT(*) * 100)::numeric)/
          -- Total by class/track
          (SELECT COUNT(*) FROM
            enrolled_learning_tracks
            WHERE student_class_id = elt.student_class_id AND
                  learning_track_id = elt.learning_track_id
          ),
          -- round
          2) as total_percent
        FROM enrolled_learning_tracks elt
        GROUP BY elt.status, elt.student_class_id, elt.learning_track_id;
      SQL
  end

  def down
    execute 'DROP MATERIALIZED VIEW IF EXISTS track_enroll_reports_matview'
  end

end
