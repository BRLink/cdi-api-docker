class CreateChallengeDeliveriesUsers < ActiveRecord::Migration
  def change
    create_table :challenge_deliveries_users, id: false do |t|
      t.belongs_to :challenge_delivery
      t.belongs_to :user
    end
    add_index :challenge_deliveries_users, [:challenge_delivery_id, :user_id], unique: true, name: 'idx_challenge_deliveries_users_uniq'
  end
end
