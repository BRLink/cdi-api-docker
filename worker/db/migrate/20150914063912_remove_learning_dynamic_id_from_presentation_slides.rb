class RemoveLearningDynamicIdFromPresentationSlides < ActiveRecord::Migration
  def change
    remove_column :presentation_slides, :learning_dynamic_id, :integer, index: true
  end
end
