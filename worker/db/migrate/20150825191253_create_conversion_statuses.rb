class CreateConversionStatuses < ActiveRecord::Migration
  def change
    create_table :conversion_statuses do |t|
      t.references :presentation, index: true
      t.string :status

      t.timestamps null: false
    end
    add_foreign_key :conversion_statuses, :presentations
  end
end
