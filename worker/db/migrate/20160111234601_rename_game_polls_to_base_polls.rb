class RenameGamePollsToBasePolls < ActiveRecord::Migration
  def change
    rename_table :game_polls, :base_polls
    add_column :base_polls, :type, :string, index: true
  end
end
