class RenameBasePollsToGamePolls < ActiveRecord::Migration
  def change
    rename_table :base_polls, :game_polls
    remove_column :game_polls, :type
    remove_column :game_polls, :video_link
  end
end
