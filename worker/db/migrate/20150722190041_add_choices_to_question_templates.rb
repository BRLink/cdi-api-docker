class AddChoicesToQuestionTemplates < ActiveRecord::Migration
  def change
    add_column :question_templates, :choices, :json
  end
end
