class AddUniqueIndexOnSubscriberPosts < ActiveRecord::Migration
  def change
    remove_foreign_key :subscriber_posts, :posts
    remove_foreign_key :subscriber_posts, :user_subscriptions
    add_foreign_key :subscriber_posts, :posts, on_delete: :cascade
    add_foreign_key :subscriber_posts, :user_subscriptions, on_delete: :cascade
    add_index :subscriber_posts, [:post_id, :user_subscription_id], unique: true, name: 'post_id_user_subscription_id_unique_index'
  end
end
