class CreateAuthorizations < ActiveRecord::Migration
  def change
    create_table :authorizations do |t|
      t.references :user, index: true
      t.string :token
      t.string :provider
      t.datetime :expires_at

      t.timestamps null: false
    end
    add_index :authorizations, :token
    add_foreign_key :authorizations, :users
  end
end
