class RenamePerspectiveToPerspectivesInLearningDynamics < ActiveRecord::Migration
  def change
    rename_column :learning_dynamics, :perspective, :perspectives
  end
end
