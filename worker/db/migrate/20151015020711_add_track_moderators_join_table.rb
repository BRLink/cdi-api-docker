class AddTrackModeratorsJoinTable < ActiveRecord::Migration
  def change
    create_join_table :users, :learning_tracks, table_name: :learning_track_moderators do |t|
      t.references :user, index: true
      t.references :learning_track, index: true

      t.index [:user_id, :learning_track_id], unique: true, name: 'index_moderators_user_id_track_id_unique'
    end
  end
end
