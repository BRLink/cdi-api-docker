class AddCascadeOnDeleteToNotificationConfigs < ActiveRecord::Migration
  def change
    remove_foreign_key :notification_configs, :users
    add_foreign_key :notification_configs, :users, on_delete: :cascade
  end
end
