class DropBlogCategories < ActiveRecord::Migration
  def up
    drop_table :blog_categories
  end

  def down
    fail ActiveRecord::IrreversibleMigration
  end
end
