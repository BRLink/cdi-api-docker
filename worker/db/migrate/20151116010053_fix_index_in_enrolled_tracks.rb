class FixIndexInEnrolledTracks < ActiveRecord::Migration
  def change
    remove_index :enrolled_learning_tracks, name: "index_enrolled_learning_tracks_on_user_id_and_learning_track_id", column: [:user_id, :learning_track_id]
    add_index :enrolled_learning_tracks, [:user_id, :learning_track_id, :student_class_id], name: "index_enrolled_tracks_on_user_id_track_id_class_id", unique: true
  end
end
