class RenameRoleToProfileTypeInUsers < ActiveRecord::Migration
  def change
    rename_column :users, :role, :profile_type
  end
end
