class AddSubtitleToQuestionTemplates < ActiveRecord::Migration
  def change
    add_column :question_templates, :subtitle, :string
  end
end
