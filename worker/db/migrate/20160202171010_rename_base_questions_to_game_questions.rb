class RenameBaseQuestionsToGameQuestions < ActiveRecord::Migration
  def change
    rename_table :base_questions, :game_questions
    remove_column :game_questions, :type
  end
end
