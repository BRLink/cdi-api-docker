class AddProviderColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :oauth_provider_uid, :string, index: true
    add_column :users, :oauth_provider, :string, index: true
  end
end
