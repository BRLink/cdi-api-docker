class CreateGameItems < ActiveRecord::Migration
  def change
    create_table :game_items do |t|
      t.references :game_related_item, index: true
      t.string :word
      t.string :related_word
      t.string :image

      t.timestamps null: false
    end
    add_index :game_items, :word
    add_index :game_items, [:game_related_item_id, :word, :related_word], unique: true, name: 'unique_index_on_game_items_game_word_related_word'

    add_index :game_items, :related_word
    add_foreign_key :game_items, :game_related_items, on_delete: :cascade
  end
end
