class RecreateBlogArticles < ActiveRecord::Migration
  def change
    create_table :blog_articles do |t|
      t.string :title
      t.text :link
      t.text :description
      t.datetime :pubDate
      t.string :guid

      t.timestamps null: false
    end
  end
end
