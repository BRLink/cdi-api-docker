class CreateUserStudentClass < ActiveRecord::Migration
  def change
    create_table :student_classes_users, id: false do |t|
      t.references :user, index: true
      t.references :student_class, index: true
    end

    add_foreign_key :student_classes_users, :users
    add_foreign_key :student_classes_users, :student_classes

    add_index :student_classes_users, [:user_id, :student_class_id], unique: true
  end
end
