class CreateSubscriptionChallenges < ActiveRecord::Migration
  def change
    create_table :subscription_challenges, id: false do |t|
      t.references :challenge, index: true
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :subscription_challenges, :challenges, on_delete: :cascade
    add_foreign_key :subscription_challenges, :users, on_delete: :cascade
    add_index :subscription_challenges, [:user_id, :challenge_id], unique: true
  end
end
