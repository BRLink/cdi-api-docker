class CreateUserPreferences < ActiveRecord::Migration
  def change
    create_table :user_preferences do |t|
      t.belongs_to :user, index: true
      t.timestamps null: true
      t.string :preference_type, null: false
      t.string :title, null: false
      t.string :author, null: false
      t.string :link, null: true
    end
    add_foreign_key :user_preferences, :users, on_delete: :cascade
  end
end
