class CreateUserBadges < ActiveRecord::Migration
  def change
    create_table :user_badges do |t|
      t.references :user,  null:false, index: true
      t.references :badge, null:false, index: true

      t.timestamps         null: false
    end
    add_foreign_key :user_badges, :users,                on_delete: :cascade
    add_foreign_key :user_badges, :badges,               on_delete: :cascade
    add_index       :user_badges, [:user_id, :badge_id], unique: :true
  end
end
