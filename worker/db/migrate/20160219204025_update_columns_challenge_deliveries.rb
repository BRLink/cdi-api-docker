class UpdateColumnsChallengeDeliveries < ActiveRecord::Migration
  def change
    rename_column :challenge_deliveries, :cover, :cover_image
    add_column :challenge_deliveries, :cover_description, :string
  end
end
