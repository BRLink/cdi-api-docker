class AddContentToBlogArticle < ActiveRecord::Migration
  def change
    add_column :blog_articles, :content, :text
  end
end
