#!/bin/bash

source /app/project/docker/env/${RACK_ENV:-development}.bash

cp -aR /app/project/* /app/current
cd /app/current
bundle install --quiet --deployment --without=development test
bundle exec sidekiq -C config/sidekiq.yml
