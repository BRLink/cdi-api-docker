total_multipliers = 2
educational_institutions = EducationalInstitution.all

total_multipliers.times do |index|
  password, email = "admin_user_#{index}", "#{password}@test.com"

  user_params = {
    user: {
      password: password,
      password_confirmation: password,
      email: email,
      first_name: "Admin",
      last_name: index.to_s,
      gender: User::VALID_GENDERS.values.sample,
      profile_type: 'staff',
      educational_institution_id: educational_institutions.sample.id
    }
  }

  user_service = CDI::V1::Users::CreateService.new(nil, user_params).execute
  user = user_service.user

  binding.pry

  s = StudentClass.create(name: Faker::Company.name, admin_user: user)
end
