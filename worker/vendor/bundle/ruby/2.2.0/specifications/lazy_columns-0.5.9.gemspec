# -*- encoding: utf-8 -*-
# stub: lazy_columns 0.5.9 ruby lib

Gem::Specification.new do |s|
  s.name = "lazy_columns"
  s.version = "0.5.9"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Jorge Manrubia"]
  s.date = "2015-02-11"
  s.description = "This plugin lets you define specific columns in ActiveRecord models to be loaded lazily"
  s.email = ["jorge.manrubia@gmail.com"]
  s.homepage = "https://github.com/jorgemanrubia/lazy_columns"
  s.rubygems_version = "2.4.5"
  s.summary = "Rails plugin that let you configure ActiveRecord columns to be loaded lazily"

  s.installed_by_version = "2.4.5" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<activerecord>, [">= 3.1"])
      s.add_development_dependency(%q<minitest>, [">= 0"])
      s.add_development_dependency(%q<sqlite3>, [">= 0"])
      s.add_development_dependency(%q<appraisal>, ["~> 1.0.2"])
      s.add_development_dependency(%q<rspec>, ["~> 2.12.0"])
      s.add_development_dependency(%q<rspec-rails>, ["~> 2.12.0"])
    else
      s.add_dependency(%q<activerecord>, [">= 3.1"])
      s.add_dependency(%q<minitest>, [">= 0"])
      s.add_dependency(%q<sqlite3>, [">= 0"])
      s.add_dependency(%q<appraisal>, ["~> 1.0.2"])
      s.add_dependency(%q<rspec>, ["~> 2.12.0"])
      s.add_dependency(%q<rspec-rails>, ["~> 2.12.0"])
    end
  else
    s.add_dependency(%q<activerecord>, [">= 3.1"])
    s.add_dependency(%q<minitest>, [">= 0"])
    s.add_dependency(%q<sqlite3>, [">= 0"])
    s.add_dependency(%q<appraisal>, ["~> 1.0.2"])
    s.add_dependency(%q<rspec>, ["~> 2.12.0"])
    s.add_dependency(%q<rspec-rails>, ["~> 2.12.0"])
  end
end
