# -*- encoding: utf-8 -*-
# stub: carrierwave_backgrounder 0.4.2 ruby lib

Gem::Specification.new do |s|
  s.name = "carrierwave_backgrounder"
  s.version = "0.4.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Larry Sprock"]
  s.date = "2015-08-26"
  s.email = ["larry@lucidbleu.com"]
  s.homepage = "https://github.com/lardawge/carrierwave_backgrounder"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.4.5"
  s.summary = "Offload CarrierWave's image processing and storage to a background process using Delayed Job, Resque, Sidekiq, Qu, Queue Classic or Girl Friday"

  s.installed_by_version = "2.4.5" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<carrierwave>, ["~> 0.5"])
      s.add_development_dependency(%q<rspec>, ["~> 3.1.0"])
      s.add_development_dependency(%q<rake>, [">= 0"])
    else
      s.add_dependency(%q<carrierwave>, ["~> 0.5"])
      s.add_dependency(%q<rspec>, ["~> 3.1.0"])
      s.add_dependency(%q<rake>, [">= 0"])
    end
  else
    s.add_dependency(%q<carrierwave>, ["~> 0.5"])
    s.add_dependency(%q<rspec>, ["~> 3.1.0"])
    s.add_dependency(%q<rake>, [">= 0"])
  end
end
