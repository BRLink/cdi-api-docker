# -*- encoding: utf-8 -*-
# stub: carrierwave-base64 2.1.1 ruby lib

Gem::Specification.new do |s|
  s.name = "carrierwave-base64"
  s.version = "2.1.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Yury Lebedev"]
  s.date = "2015-09-22"
  s.description = "This gem can be useful, if you need to upload files to your API from mobile devises."
  s.email = ["lebedev.yurii@gmail.com"]
  s.homepage = "https://github.com/lebedev-yury/carrierwave-base64"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.4.5"
  s.summary = "Upload images encoded as base64 to carrierwave."

  s.installed_by_version = "2.4.5" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<carrierwave>, ["< 0.11.0", ">= 0.8.0"])
      s.add_development_dependency(%q<rails>, [">= 3.2.0"])
      s.add_development_dependency(%q<sqlite3>, [">= 0"])
      s.add_development_dependency(%q<bundler>, ["~> 1.7"])
      s.add_development_dependency(%q<rake>, ["~> 10.0"])
      s.add_development_dependency(%q<rspec>, ["~> 2.14"])
      s.add_development_dependency(%q<sham_rack>, [">= 0"])
      s.add_development_dependency(%q<pry>, [">= 0"])
    else
      s.add_dependency(%q<carrierwave>, ["< 0.11.0", ">= 0.8.0"])
      s.add_dependency(%q<rails>, [">= 3.2.0"])
      s.add_dependency(%q<sqlite3>, [">= 0"])
      s.add_dependency(%q<bundler>, ["~> 1.7"])
      s.add_dependency(%q<rake>, ["~> 10.0"])
      s.add_dependency(%q<rspec>, ["~> 2.14"])
      s.add_dependency(%q<sham_rack>, [">= 0"])
      s.add_dependency(%q<pry>, [">= 0"])
    end
  else
    s.add_dependency(%q<carrierwave>, ["< 0.11.0", ">= 0.8.0"])
    s.add_dependency(%q<rails>, [">= 3.2.0"])
    s.add_dependency(%q<sqlite3>, [">= 0"])
    s.add_dependency(%q<bundler>, ["~> 1.7"])
    s.add_dependency(%q<rake>, ["~> 10.0"])
    s.add_dependency(%q<rspec>, ["~> 2.14"])
    s.add_dependency(%q<sham_rack>, [">= 0"])
    s.add_dependency(%q<pry>, [">= 0"])
  end
end
