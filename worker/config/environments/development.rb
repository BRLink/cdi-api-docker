Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  cache_servers = (ENV["MEMCACHIER_SERVERS"]  || CDI::Config.cache_servers || "").split(",")
  cache_username = ENV["MEMCACHIER_USERNAME"] || CDI::Config.cache_username
  cache_password = ENV["MEMCACHIER_PASSWORD"] || CDI::Config.cache_password

  cache_namespace = CDI::Config.cache_namespace || "tec-escola-#{Rails.env}"
  cache_max_size  = (CDI::Config.cache_value_max_bytes || (1024 * 1024) * 10).to_f # 10 MB

  cache_options = {
                    :namespace            => cache_namespace,
                    :compress             => true,
                    :username             => cache_username,
                    :password             => cache_password,
                    :failover             => true,
                    :socket_timeout       => (CDI::Config.cache_socket_timeout || 1.5).to_f,
                    :socket_failure_delay => (CDI::Config.cache_socket_failure_delay || 0.2).to_f,
                    # 10MB
                    :value_max_bytes => cache_max_size
                  }

  if CDI::Config.enabled?(:aws_deploy) && !!defined?(Dalli::ElastiCache)
    cache_endpoint = cache_servers.first
    # fetch all "disoverable" memcached nodes
    elasticache = Dalli::ElastiCache.new(cache_endpoint)
    cache_servers = elasticache.servers
  end

  config.cache_store = :dalli_store, cache_servers, cache_options

  config.action_mailer.delivery_method = :letter_opener

  if CDI::Config.enabled?(:use_amazon_ses_smtp_in_development)
    smtp_username = ENV['SENDGRID_USERNAME'] || CDI::Config.smtp_username
    smtp_password = ENV['SENDGRID_PASSWORD'] || CDI::Config.smtp_password
    smtp_host     = ENV['SENDGRID_HOST']     || CDI::Config.smtp_host
    smtp_port     = ENV['SENDGRID_PORT']     || CDI::Config.smtp_port
    smtp_domain   = ENV['SENDGRID_DOMAIN']   || CDI::Config.smtp_domain

    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
      :port           => smtp_port,
      :address        => smtp_host,
      :user_name      => smtp_username,
      :password       => smtp_password,
      :domain         => smtp_domain,
      :authentication => :login,
      :enable_starttls_auto => true
    }
  end

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true
end
