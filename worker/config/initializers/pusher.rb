config = {
  development: %w(164453 8060e17a3eed0eb56bab 024a48b3fe5e0db41993),
  staging: %w(164454 eed20d9b3065671ff52f 89fd3a233bbb5d2137e7),
  production: %w(164455 742c0f814a42b73726ae a398c787512a9f23e7cf)
}
config.default = Array.new(3)
env_config = config[Rails.env.to_sym]

Pusher.app_id = env_config[0]
Pusher.key    = env_config[1]
Pusher.secret = env_config[2]
