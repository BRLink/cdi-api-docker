if CDI::Config.enabled?(:bullet_log) && defined?(Bullet)
  Bullet.enable = true
  Bullet.alert = true
  Bullet.bullet_logger = true
  Bullet.console = true
  # Bullet.growl = true
  Bullet.rails_logger = true
  Bullet.rollbar = true
  Bullet.add_footer = true
end
