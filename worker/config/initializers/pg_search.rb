PgSearch.multisearch_options = {
  using: {tsearch: { dictionary: "portuguese", prefix: true, any_word: true }},
  ignoring: :accents
}

module PgSearch
  class Document
    belongs_to :post, -> { where(pg_search_documents: {searchable_type: 'Post'}) }, foreign_key: 'searchable_id'
    belongs_to :discussion, -> { where(pg_search_documents: {searchable_type: 'Discussion'}) }, foreign_key: 'searchable_id'
    belongs_to :user, -> { where(pg_search_documents: {searchable_type: 'User'}) }, foreign_key: 'searchable_id'
  end
end
