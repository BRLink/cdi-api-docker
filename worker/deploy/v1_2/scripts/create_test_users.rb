
def create_user params
  params = {
    first_name: nil,
    last_name: nil,
    email: nil,
    profile_type: nil,
    birthday: Faker::Date.between(40.years.ago, Date.today),
    password: '123456',
    password_confirmation: '123456',
    gender: 'male'
  }.merge(params)
  User.create params
end

def update_password user
  user.password = '123456'
  user.password_confirmation = '123456'
  user.save
end

# update_password User.find_by(email: 'will.soares@ever.ag') # superadmin

create_user first_name: 'Jefferson', last_name: 'Lopes', email: 'jefferson.jslopes@gmail.com', profile_type: 'student'
create_user first_name: 'Jefferson', last_name: 'Lopes', email: 'jefferson.lopes@ever.ag', profile_type: 'teacher'

create_user  first_name: 'Augusto', last_name: 'Abreu', email: 'augusto.abreu@ever.ag', profile_type: 'teacher'
# update_password User.find_by(email: 'guto@live.ca') # student

# update_password User.find_by(email: 'alexandre.canuto@ever.ag') # multiplier
create_user first_name: 'Alexandre', last_name: 'Canuto', email: 'alexandre.canuto@me.com', profile_type: 'student'

create_user first_name: 'Isac', last_name: 'Araujo', email: 'isac.araujo@ever.ag', profile_type: 'teacher'
create_user first_name: 'Isac', last_name: 'Araujo', email: 'isac.gaiao@gmail.com', profile_type: 'student'


User.all.each do |u|
  update_password u
end
