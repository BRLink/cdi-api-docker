require_relative '../user'

module CDI
  module FakeData
    module Generators
      module Games
        class GameQuiz < Game

          def initialize(options = {})
            super(options)
            @game_type = :game_quiz
          end

          def sample_game_data(index, game_type = nil)
            {
              questions: create_questions
            }
          end

          def create_option_data
            {
              text: fake_paragraph(1),
              correct: rand(10) < 2
            }
          end

        end
      end
    end
  end
end
