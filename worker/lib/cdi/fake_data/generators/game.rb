require_relative '../base'

module CDI
  module FakeData
    module Generators
      class Game < Base

        attr_reader :game_type, :total_questions

        def initialize(options = {})
          super(options)
          @game_type = options.fetch(:game_type, :game_quiz)
          @total_questions = options.fetch(:total_questions, 10).to_i
        end

        def create
          run do
            create_games(total_amount, @game_type)
          end
        end

        def create_games(amount, game_type = :quiz)

          info "\nCriando games do tipo: #{game_type} com #{total_questions} questões\n"

          amount.to_i.times do |index|
            game_data = game_data(@current_index, game_type)

            service = service_for_game(game_data)
            service.execute

            info service.errors unless service.success?

            print_status index.next, amount

            advance_to_next_iteration
          end
        end

        def service_for_game(game_data)
          game_class = @game_type.to_s.camelize.pluralize

          klass = "CDI::V1::#{game_class}::CreateService".constantize

          service = klass.new(owner_user,
            async: false,
            game: game_data,
            origin: fake_origin_data,
            delivery_email: false
          )
        end

        def owner_user
          email = ENV['OWNER_EMAIL']

          return ::User.find_by(email: email) if email.present?

          # default is always admin
          admin_user
        end

        def game_data(index, game_type)
          data = basic_game_data(index)
          data.merge!(sample_game_data(data, game_type))
        end

        def basic_game_data(index)
          {
            title: faker_name.title,
            description: fake_paragraph
          }
        end

        # Custom data for specific game type
        def sample_game_data(user_data = {}, game_type = nil)
          data = {}
        end

        def create_questions
          questions = []

          @total_questions.to_i.times do
            questions << create_question
          end

          questions
        end

        def create_question
          question = {}
          question[:title] = fake_paragraph(1)
          question[:options] = []

          total_options.to_i.times do
            option = create_option_data
            question[:options] << option
          end

          if has_correct_option?
            any_correct = question[:options].any? {|q| q[:correct].present? }
            question[:options].first[:correct] = true unless any_correct
          end

          question
        end

        def total_options
          @options.fetch(:total_options, (5 + rand(5))).to_i
        end

        def has_correct_option?
          true
        end

      end
    end
  end
end
