module CDI
  module CacheExtensions
    module Simple
      extend ActiveSupport::Concern

      class_methods do
        def fetch_from_rails_cache(cache_key, options, &block)
          Rails.cache.fetch(cache_key, options, &block)
        end

        def delete_from_rails_cache(key, options = {})
          Rails.cache.delete(key)
        end

        def clear_all_from_rails_cache
          Rails.cache.clear
        end

        def read_from_rails_cache(key)
          Rails.cache.read(key)
        end
      end
    end
  end
end
