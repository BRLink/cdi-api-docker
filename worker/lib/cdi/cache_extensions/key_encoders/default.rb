module CDI
  module CacheExtensions
    module KeyEncoders
      module Default

        def encoded_key_value(value)
          value.to_s
        end

        def decoded_key_value(value)
          value.to_s
        end

      end
    end
  end
end
