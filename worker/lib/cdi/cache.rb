module CDI
  module Cache

    include CacheExtensions::Simple
    include CacheExtensions::Matched if CDI::Config.enabled?(:collect_namespaced_user_memcached_keys)

    module_function

    CONFIG_FILE = -> { File.read(Rails.root.join('config', 'caches.yml')) }

    def config
      @@config ||= YAML.load(ERB.new(CONFIG_FILE.call).result).fetch(Rails.env, {})
    end

    def fetch(key, options = {}, &block)
      cache_key = key

      if replace_data = options.delete(:replace_data)
        cache_key = key_for(key, replace_data)
        options[:expires_in] = expires_for(key)
      end

      fetch_from_rails_cache(cache_key, options, &block)
    end

    def read(key)
      read_from_rails_cache(key)
    end

    def delete(keys, options = {})
      delete_from_rails_cache(keys, options =  {})
    end

    def delete_key(key, replace_data = [])
      key = key_for(key, replace_data)

      delete(key)
    end

    def clear_all
      clear_all_from_rails_cache
    end


    def key_for(key, replace_data = [])
      cache_key = key
      key_data  = data_for_key(key)

      if key_data[:key]
        cache_key = format_cache_key(key_data[:key], replace_data)
      end

      cache_key
    end

    def expires_for(key)
      key_data = data_for_key(key)
      key_data[:expires]
    end

    def data_for_key(key)
      key_data  = CDI.utils.get_value_by_dot_string(key, self.config)

      if key_data.present? && key_data.any?
        return key_data
      end

      {}
    end

    def format_cache_key(key, replace_data=[])
      # named placeholders
      if replace_data.any? && replace_data.is_a?(Hash)
        valid_placeholders = replace_data.keys.select {|_key| key.match("%{#{_key}}") }

        if valid_placeholders.any?
          formated_key = key % replace_data.to_h.symbolize_keys
        else
          formated_key = format_replace_cache_key(key, replace_data.values)
        end

        return formated_key
      end

      format_replace_cache_key(key, replace_data)
    end

    def format_replace_cache_key(key, replace_data)
      if key.match(/(\%(s|i|d))/i) &&
        key = key % replace_data
      end

      key
    end
  end

  module Utils
    def self.get_value_by_dot_string(string, hash)
      hash  = hash.deep_symbolize_keys
      value = nil
      keys  = string.split('.')
      while new_key = keys.shift do
        value = (value.nil? ? hash[new_key.to_sym] : value[new_key.to_sym])
      end
      value
    end
  end

  def self.cache
    Cache
  end
  def self.utils
    Utils
  end
end
