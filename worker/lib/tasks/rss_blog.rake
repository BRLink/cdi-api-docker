# this task is to be used in a cron job to sync the rss blog posts to the database
require 'rss'

namespace :rss do
  desc "Sync db with rss feed. (to be used in a cron job)"
  task :sync => :environment do
    puts "Synchronizing database with rss feed..."
    feed = RSS::Parser.parse("http://www.cdi.org.br/feed/", false)
    feed.items.each do |item|
      categories = []
      item.categories.each do |each|
        name = each.content.split.map(&:capitalize).join(" ")
        category = Category.find_or_create_by(name: name)
        categories << category if !categories.include? category
      end
      BlogArticle.new(
        title: item.title,
        link: item.link,
        description: item.description,
        pub_date: item.pubDate,
        guid: item.guid.content,
        categories: categories
      ).save
    end
    puts "Done!"
  end
end
