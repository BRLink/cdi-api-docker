namespace :postman do

	desc "Sync all postman routes with s3."
	task :sync => :environment do
		s3_config = {
	    :provider               => 'AWS',
	    :aws_access_key_id      => CDI::Config.aws_access_key_id,
	    :aws_secret_access_key  => CDI::Config.aws_secret_access_key,
	    :region                 => CDI::Config.aws_region
	  }
	  connection = Fog::Storage.new(s3_config)
	  bucket = connection.directories.get(CDI::Config.aws_bucket_name)
	  Dir.glob('docs/postman/*.postman_collection').each do |filename|
	  	basename = filename.split('/').pop
	  	file = bucket.files.create(
			  :key    => "postman/#{basename}",
			  :body   => File.open(filename),
			  :public => true
			)
			puts "Created #{file.public_url}"
	  end
	end
end
