# CDI API Docker - Deploy

Este repositório é utilizado para deploy do projeto cdi-api.

### Development

Clone o projeto pelo comando:

```
$ git clone git@bitbucket.org:ever-ag/cdi-api-docker.git --recursive
```

**NOTA:** Sempre que realizar um git pull, execute o comando para atualizar os submodulos:

```
$ git submodule foreach git pull origin master
```
* Utilize o comando sempre que o repositório `cdi-api` for atualizado.  
     
     
Compile todas as imagens do diretorio `{cdi-api}/docker/dockerfiles`:

``` 
$ docker build -t ever-ag/pg:9.5 -f docker/dockerfiles/Dockerfile.postgres .
$ docker build -t ever-ag/redis:2.8.4 -f docker/dockerfiles/Dockerfile.redis .
$ docker build -t ever-ag/rails:2.2.1 -f docker/dockerfiles/Dockerfile.rails .
$ docker build -t ever-ag/cdi-api:latest -f docker/dockerfiles/Dockerfile .
```

Ative os serviços através do utiliário abaixo:

```
$ ./docker/dev/start-services.sh
```

Para inicializar o docker (first install), execute o comando abaixo:

```
$ ./docker/dev/start-cdi-api-install.sh
```

Para ativar o serviço do docker (web e worker), utilize o utilitário abaixo:

```
$ ./docker/dev/start-cdi-api.sh
```

Para parar os serviços do docker (web e worker), utilize o utilitário abaixo:

```
$ docker stop web worker
$ docker rm web worker
```

### Staging

Execute os comandos:

```
$ docker run \
	-d \
	-p 80:80 \
	-v $(pwd)/project:/app/project \
	-v $(pwd)/docker:/app/docker \
	-v $(pwd)/web:/app/current \
	--name cdi-api-web \
	-e RACK_ENV="staging" \
	880440865424.dkr.ecr.us-east-1.amazonaws.com/ever-ag/cdi-api \
	/app/docker/bin/boot-web.sh

$ docker run \
	-d \
	-v $(pwd)/project:/app/project \
	-v $(pwd)/docker:/app/docker \
	-v $(pwd)/worker:/app/current \
	--name cdi-api-worker \
	-e RACK_ENV="staging" \
	880440865424.dkr.ecr.us-east-1.amazonaws.com/ever-ag/cdi-api \
	/app/docker/bin/boot-worker.sh
```

ou pelo `eb client`:

```
$ eb local run --envvars RACK_ENV="staging"
```

### Production

Execute os comandos:

```
$ docker run \
	-d \
	-p 80:80 \
	-v $(pwd)/project:/app/project \
	-v $(pwd)/docker:/app/docker \
	-v $(pwd)/web:/app/current \
	--name cdi-api-web \
	-e RACK_ENV="production" \
	880440865424.dkr.ecr.us-east-1.amazonaws.com/ever-ag/cdi-api \
	/app/docker/bin/boot-web.sh

$ docker run \
	-d \
	-v $(pwd)/project:/app/project \
	-v $(pwd)/docker:/app/docker \
	-v $(pwd)/worker:/app/current \
	--name cdi-api-worker \
	-e RACK_ENV="production" \
	880440865424.dkr.ecr.us-east-1.amazonaws.com/ever-ag/cdi-api \
	/app/docker/bin/boot-worker.sh
```

ou pelo `eb client`:

```
$ eb local run --envvars RACK_ENV="production"
```
# cdi-api-docker
