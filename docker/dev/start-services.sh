#!/bin/bash

echo "> Starting Memcached ..."
docker run -d -P --name memcache memcached

echo "> Starting Redis ..."
docker run -d -P --name redis ever-ag/redis:2.8.4

echo "> Starting Postgresql ..."
docker run -d -P --name postgres ever-ag/pg:9.5
