#!/bin/bash

RACK_ENV="development"

echo "> Installing cdi-api for env '${RACK_ENV}' ..."

docker run \
	   --rm -it \
	   -v $(pwd)/project:/app/project \
	   -v $(pwd)/web:/app/current \
	   -v $(pwd)/docker:/app/docker \
	   --link postgres:pg \
	   --link redis:rd \
	   --link memcache:mc \
	   --name cdi-api-install \
	   -e RACK_ENV="${RACK_ENV}" \
	   ever-ag/cdi-api /app/docker/bin/first-install.sh
