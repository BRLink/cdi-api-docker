#!/bin/bash

RACK_ENV="development"

echo "> Starting cdi-api for env '${RACK_ENV}' ..."

docker run \
	   -d \
	   -p 9090:80 \
	   -v $(pwd)/project:/app/project \
	   -v $(pwd)/docker:/app/docker \
	   -v $(pwd)/web:/app/current \
	   --link postgres:pg \
	   --link redis:rd \
	   --link memcache:mc \
	   --name cdi-api-web \
	   -e RACK_ENV="${RACK_ENV}" \
	   ever-ag/cdi-api /app/docker/bin/boot-web.sh

docker run \
	   -d \
	   -v $(pwd)/project:/app/project \
	   -v $(pwd)/docker:/app/docker \
	   -v $(pwd)/worker:/app/current \
	   --link postgres:pg \
	   --link redis:rd \
	   --link memcache:mc \
	   --name cdi-api-worker \
	   -e RACK_ENV="${RACK_ENV}" \
	   ever-ag/cdi-api /app/docker/bin/boot-worker.sh
