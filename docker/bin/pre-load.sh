#!/bin/bash

ENV_FILE="/app/docker/env/${RACK_ENV:-development}.bash"

echo "> Load env file: $ENV_FILE"
source $ENV_FILE

echo "> Copy project files into /app/current"
cp -aR /app/project/* /app/current

echo "> Change current work directory to /app/current"
cd /app/current

echo "> Install bundle"
bundle install --quiet --deployment --without=development test
