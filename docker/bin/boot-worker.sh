#!/bin/bash

source "/app/docker/bin/pre-load.sh"

echo "> Start sidekiq"
bundle exec sidekiq -C config/sidekiq.yml
