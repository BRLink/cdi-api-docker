#!/bin/bash

source "/app/docker/bin/pre-load.sh"

echo "> Run rake db:reset"
bundle exec rake db:reset
