#!/bin/bash

source "/app/docker/bin/pre-load.sh"

echo "> Run migrations"
bundle exec rake db:migrate

echo "> Start puma"
bundle exec puma -C config/puma.rb
