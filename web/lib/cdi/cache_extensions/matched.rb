module CDI
  module CacheExtensions
    module Matched

      extend ActiveSupport::Concern

      NAMESPACED_MEMCACHED_KEYS = 'namespace:user_memcached_keys:user_id_%s'

      ENCODE_KEYS_VALUE = CDI::Config.enabled?(:encode_user_memcached_keys)

      class_methods do

        include CDI::CacheExtensions::KeyEncoders::Default # default

        def self.include_custom_key_encoder
          key_encoder = CDI::Config.encode_user_memcached_keys_method

          unless key_encoder.blank?
            module_name = "CDI::CacheExtensions::KeyEncoders::#{key_encoder.to_s.classify}"

            begin
              include module_name.constantize
            rescue => e
              include CacheExtensions::KeyEncoders::Default
            end
          end
        end

        include_custom_key_encoder

        def fetch_from_rails_cache(cache_key, options, &block)
          save_keys = options.delete(:save_keys).present?
          user_key_id = options.delete(:user_id).try(:to_i)

          Rails.cache.fetch(cache_key, options) do
            if save_keys && user_key_id.present?
              keys = get_user_memcached_keys(user_key_id)
              unless keys.member?(cache_key)
                keys.add(cache_key)
                save_user_memcached_keys(user_key_id, keys)
              end
            end

            yield block
          end
        end

        def delete_from_rails_cache(key, options =  {})
          deleted = Rails.cache.delete(key) rescue nil

          if deleted && options[:update_saved_keys].present? && user_id = options[:user_id].try(:to_i)
            remove_and_update_key_from_memcached_keys_for(user_id, keys)
          end

          deleted
        end

        def clear_all_from_rails_cache(options = {})
          Rails.cache.clear
        end

        def read_from_rails_cache(key)
          Rails.cache.read(key)
        end

        def expire!(key, replace_data, options = {})
          cache_key = key_for(key, replace_data)
          delete(cache_key, options)
        end

        def remove_and_update_key_from_memcached_keys_for(user_id, key, really_save = true)
          user_cache_keys = get_user_memcached_keys(user_id)

          if user_cache_keys.member?(key)
            user_cache_keys = remove_keys_from_memcached_keys(user_id, key, user_cache_keys)
            save_user_memcached_keys(user_id, user_cache_keys) if really_save
          end

          user_cache_keys
        end

        def remove_keys_from_memcached_keys(user_id, keys, user_cache_keys = nil)
          user_cache_keys ||= get_user_memcached_keys(user_id)

          Array.wrap(keys).each {|key| user_cache_keys.delete(key) }

          user_cache_keys
        end

        def get_user_memcached_keys(user_id)
          data = begin
            key_name = (NAMESPACED_MEMCACHED_KEYS % user_id)
            key_data = read(key_name)

            data_to_parse = user_memcached_keys_value_to_read(key_data)
            JSON.parse data_to_parse
          rescue Exception => e
            {}
          end

          Set.new(data)
        end

        def save_user_memcached_keys(user_id, keys)
          cache_key = NAMESPACED_MEMCACHED_KEYS % user_id

          serialized_value = user_memcached_keys_value_to_write(keys)

          Rails.cache.write(cache_key, serialized_value)
        end

        def user_memcached_keys_value_to_write(value)
          return encoded_key_value(value.to_json) if ENCODE_KEYS_VALUE

          value.to_json
        end

        def user_memcached_keys_value_to_read(value)
          return decoded_key_value(value.to_s) if ENCODE_KEYS_VALUE

          value.as_json
        end

        def expire_matched!(matcher, options)
          user_id = options[:user_id]

          return nil unless user_id

          keys = get_matched_memcached_keys_for(user_id, matcher)

          keys.each do |cache_key|
            delete(cache_key, options)
          end
        end

        # use this method to expire multiples keys (single hit in memcached)
        def expire_matched_keys!(keys, options = {})
          user_id = options[:user_id]

          return nil unless user_id

          user_cache_keys ||= get_user_memcached_keys(user_id)

          cache_keys = keys.map do |key|
            get_matched_memcached_keys_for(user_id, key, user_cache_keys)
          end.flatten

          deleted_keys = cache_keys.map {|cache_key| Rails.cache.delete(cache_key) }

          deleted = deleted_keys.any?

          if deleted
            user_cache_keys = remove_keys_from_memcached_keys(user_id, cache_keys, user_cache_keys)
            save_user_memcached_keys(user_id, user_cache_keys)
          end

          deleted
        end

        def get_matched_memcached_keys_for(user_id, matcher, user_cache_keys = nil)
          user_cache_keys ||= get_user_memcached_keys(user_id)

          matcher = Regexp.new(matcher) unless matcher.is_a?(Regexp)

          user_cache_keys.select {|key| matcher.match(key) }
        end

      end
    end
  end
end
