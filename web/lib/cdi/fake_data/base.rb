module CDI
  module FakeData
    class Base
      attr_reader :options, :errors, :total_amount

      def initialize(options = {})
        @options       = set_default_options(options)
        @total_amount  = total_amount_from_options
        @current_index = 0
      end

      def set_default_options(options={})
        {
          total_amount: 1
        }.merge(options)
      end

      def total_amount_from_options
        @options.fetch(:total_amount, 1)
      end

      def run(&block)
        if hide_active_record_logger?
          ActiveRecord::Base.logger.silence do
            instance_eval(&block)
          end
        else
          instance_eval(&block)
        end

        puts "\n\n" if verbose?
      end

      def info(message)
        return if silent?
        puts message
      end

      def print_status(current_index, last_index)
        return if silent?

        print "\r>> %s/%s" % [current_index, last_index]
      end

      def admin_user
        @admin_user ||= User.where(email: CDI::Config.admin_email).first
      end

      def remote_avatar_url(slug=nil, dimensions="500x500")
        faker::Avatar.image(slug, dimensions)
      end

      def advance_to_next_iteration
        @current_index ||= 0
        @current_index += 1
      end

      def random_user
        ::User.order("RANDOM()").first
      end

      def owner_user
        email = ENV['OWNER_EMAIL']

        @_owner_user ||= ::User.find_by(email: email) if email.present?

        # default is always admin
        @_owner_user ||= admin_user

        @_owner_user
      end

      def fake_paragraph(depth = 2)
        faker::Lorem.paragraph(depth)
      end

      def faker
        ::Faker
      end

      def faker_name
        faker::Name
      end

      def faker_company
        faker::Company
      end

      def faker_internet
        faker::Internet
      end

      def faker_number
        faker::Number
      end

      def faker_commerce
        faker::Commerce
      end

      def faker_email(first_name=nil)
        faker_internet.free_email(first_name)
      end

      def faker_date(start_date = 20.years.ago, end_date = Date.today)
        Faker::Date.between(start_date, end_date)
      end

      def fake_origin_data
        { ip: faker_internet.ip_v4_address, provider: "faker", locale: I18n.locale }
      end

      def silent?
        @silent == true || @verbose == false
      end

      def verbose?
        !silent?
      end

      def hide_active_record_logger?
        return true
      end
    end
  end
end
