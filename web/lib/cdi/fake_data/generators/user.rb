require_relative '../base'

module CDI
  module FakeData
    module Generators
      class User < Base

        attr_reader :profile_type

        def initialize(options = {})
          super(options)
          @profile_type  = options.fetch(:profile_type, :student)
        end

        def create
          run do
            create_users(total_amount, @profile_type)
          end
        end

        def create_users(amount, profile_type = :student)

          info "\nCriando usuários do tipo: #{profile_type}\n"

          amount.to_i.times do |index|
            service = CDI::V1::Users::CreateService.new(admin_user, data_for_service(profile_type))

            service.execute

            info service.errors unless service.success?

            print_status index.next, amount

            advance_to_next_iteration
          end
        end

        def data_for_service(profile_type)
          user_data = user_data(@current_index, profile_type)

          {
            async: false,
            user: user_data,
            origin: fake_origin_data,
            delivery_email: false
          }
        end

        def user_data(index, profile_type)
          data = basic_user_data(index)
          data.merge!(sample_user_data(data, profile_type))
        end

        def basic_user_data(index)
          first_name, last_name = faker_name.first_name, faker_name.last_name
          fullname = [first_name, last_name].join(' ')

          password = [fullname, '123'].join.parameterize

          user_data = {
            first_name: first_name,
            last_name: last_name,
            gender: ::User::VALID_GENDERS.values.sample,
            email: faker_email(fullname),
            password: password,
            password_confirmation: password,
            profile_type: @profile_type,
            birthday: faker_date,
            profile_image: user_profile_image(index)
          }
        end

        def user_profile_image(index)
          return nil if @options[:create_profile_image].blank?
          remote_avatar_url("user-#{index}")
        end

        # Custom data for specific profile type
        def sample_user_data(user_data = {}, profile_type = nil)
          data = {}
        end
      end
    end
  end
end
