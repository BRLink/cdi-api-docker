require_relative '../user'

module CDI
  module FakeData
    module Generators
      module Users
        class Student < User

          def initialize(options={})
            super(options)
            @profile_type = :student
          end

          def sample_user_data(index, profile_type=nil)
            {
              educational_institution_id: ::EducationalInstitution.order("random()").limit(1).pluck(:id),
              classes_codes: StudentClass.pluck(:code)
            }
          end
        end
      end
    end
  end
end
