require_relative '../user'

module CDI
  module FakeData
    module Generators
      module Users
        class Multiplier < User

          def initialize(options = {})
            super(options)
          end

          def sample_user_data(index, profile_type = nil)
            if profile_type.blank? || profile_type == 'multiplier'
              @profile_type = ::User::VALID_MULTIPLIER_PROFILES_TYPES.values.sample
            else
              @profile_type = profile_type
            end

            {
              profile_type: @profile_type,
              educational_institution_id: ::EducationalInstitution.all.sample.id
            }
          end

        end
      end
    end
  end
end
