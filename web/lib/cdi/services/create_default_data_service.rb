require 'csv'
require 'json'

module CDI
  module Services
    class CreateDefaultDataService < BaseService

      def initialize(options={})
        super(options)
        @verbose = options.fetch(:verbose, true)
        @actions = [options.delete(:actions) || :all_actions].flatten
      end

      def execute
        ActiveRecord::Base.transaction do
          @actions.each do |action|
            self.send(action)
          end
        end
      end
      def exe_multimedia
        find_or_create_multimedia
      end
      private
      def all_actions
        create_achievements
        create_badges
        find_or_create_states
        find_or_create_cities
        find_or_create_educational_institutions
        find_or_create_users
        find_or_create_categories
        find_or_create_skills
        find_or_create_questions_templates
        find_or_create_multimedia
        find_or_create_student_classes
        create_default_subscriptions
        fix_categories
        fix_supporting_courses_parent
        fix_supporting_courses_presentation_publishment
      end

      def string_beautify str
        str.strip.capitalize
      end

      def create_achievements
        default_points = [1, 5, 10, 25, 50, 100]
        filename = 'gamification_achievements.csv'

        puts "\nAdicionando conquistas\n"

        achievements = CSV.parse(File.open(Rails.root.join('docs', 'seeds', filename), 'r'))
        area = ""

        achievements.each_with_index do |data, index|
          area = data[0] || area
          points = data[3..-1].find_index { |data| !data.nil? }

          achievement = Achievement.new(
            area: area.squish,
            action: data[1] && data[1].squish,
            user_type: data[2] && data[2].split(","),
            default_score: (default_points[points] rescue nil)
          )

          achievement.save

          if achievement.persisted?
            print "\r>> %s/%s" % [index.next, achievements.size]
          else
            puts "\nImpossível criar Achievement: %s\n" % achievement.errors.full_messages.join("; ")
          end
        end
      end

      def create_badges
        puts "\nAdicionando medalhas\n"
        badges = CSV.parse(File.open(Rails.root.join('docs', 'seeds', "gamification_badges.csv"), 'r'))
        badges.each_with_index do |data, index|
          badge = Badge.new(
            level: data[0] && data[0].squish.to_i,
            name: data[1] && data[1].squish,
            action: data[4] && data[4].squish,
            user_type: data[3] && data[3].split(","),
            description: data[2] && data[2].squish
          )

          badge.save

          if badge.persisted?
            print "\r>> %s/%s" % [index.next, badges.size]
          else
            puts "\nImpossível criar Badge: %s\n" % badge.errors.full_messages.join("; ")
          end
        end
      end

      def find_or_create_multimedia
        user = User.teacher.first
        multimedia = CSV.parse(File.open(Rails.root.join('docs', 'seeds', 'multimedia.csv'), 'r'))
        puts "\nAdicionando Multimedias"
        multimedia.each_with_index do | data, index|
          # category_name = string_beautify(data.fourth)
          category_name = data.fourth
          category = Category.find_or_create_by(name: category_name)
          tags_string = data[5].squish
          tags = tags_string.split(/[;,\.]/)
          tags.map!{|s| string_beautify(s)}

          item = Multimedia.new
          item.name = data.first.squish
          item.link = data.second.squish
          item.categories << category
          item.tags = tags
          item.user=user
          item.set_status(:published, false)
          item.multimedia_type = (data[6].squish == 'vídeo') ? 'video' : 'link'
          item.metadata = {
            url: item.link
          }

          begin
            item.save
          rescue ActiveRecord::StatementInvalid => e
            puts "\nCriação de Multimedia falhou"
            puts "Item %s" % item.as_json
            puts "Exceção: %s" % e.message
            break
          end
          if item.persisted?
            print "\r>> %s/%s " % [index.next, multimedia.size]
          else
            puts "\n Impossível criar multimedia: %s" % item.errors.full_messages.join("; ")
          end
        end
        puts "\nTotal de %s Multimedias Registradas" % multimedia.size
      end

      def find_or_create_states
        puts "\nAdicionando estados\n"
        states = CSV.parse(File.open(Rails.root.join('docs', 'seeds', 'states.csv'), 'r'))

        states.each_with_index do |state_data, index|
          State.where(name: state_data.first.squish, acronym: state_data.last.squish).first_or_create
          print "\r>> %s/%s" % [index.next, states.size]
        end
      end

      def find_or_create_cities
        cities          = CSV.parse(File.open(Rails.root.join('docs', 'seeds', 'cities.csv'), 'r'))
        cities_by_state = {}

        cities.each do |city_data|
          state = city_data[2].squish.upcase
          cities_by_state[state] ||= []
          cities_by_state[state] << city_data.first.squish
        end

        cities_by_state.each do |state_acronym, cities|
          state = State.where(acronym: state_acronym).first

          if state
            puts "\nAdicionando cidades para o estado: #{state.name}\n"
            cities.each_with_index do |city, index|
              state.cities.where(name: city).first_or_create
              print "\r>> %s/%s" % [index.next, cities.size]
            end
          else
            puts "Estado não encontrado: %s" % state_acronym
          end
        end
      end

      def find_or_create_questions_templates
        questions_templates = load_seed_file('questions_templates.json')
        puts "\nAdicionando Questions Templates"
        index = 0
        questions_templates.each_value do | questions|
          QuestionTemplate.create(questions)
          print "\r>> %s/%s" % [index.next, questions_templates.values.size]
          index += 1
        end
        puts "\nTotal de %s Question Templates adicionados" % questions_templates.values.size
      end

      private

      def find_or_create_student_classes
        user = User.find_by(email: "admin_user@cdi.org.br")
        return puts "USUARIO INVALIDO PARA CRIACAO DE TURMA DEFAULT" if user.blank?

        user.administered_student_classes.find_or_create_by(name: "Default Student Class", educational_institution: EducationalInstitution.first)
      end

      def find_or_create_users
        find_or_create_student_users
        find_or_create_teacher_users
        find_or_create_mentor_users
        find_or_create_admin_users
      end

      def find_or_create_student_users
        create_user("student_user@cdi.org.br", :student)
      end

      def find_or_create_teacher_users
        create_user("teacher_user@cdi.org.br", :teacher)
      end

      def find_or_create_mentor_users
        create_user("mentor_user@cdi.org.br", :mentor)
      end

      def find_or_create_admin_users
        create_user("admin_user@cdi.org.br", :staff)
        create_user(CDI::Config.admin_email, :staff)
      end

      def find_or_create_educational_institutions
        find_or_create_admin_users

        institutions = load_seed_file('educational_institutions.json')

        institutions.each do |institution_params|

          # institution_params.transform_values! {|v| v.to_s.titleize }

          admin_user_email = institution_params.delete('admin_user_email') || 'admin_user@cdi.org.br'

          admin_user = User.find_by(email: admin_user_email)

          find_params = institution_params.slice('name')

          unless admin_user.owned_educational_institutions.exists?(find_params)

            address = institution_params.slice('city_name', 'street', 'number', 'district', 'complement', 'zipcode')

            city_name = address.delete('city_name')

            city = City.where('lower(name) = ?', city_name.downcase).first
            city = City.create(name: city_name) if city.blank?

            city_id = city.id

            params = {
              educational_institution: {
                name: institution_params['name'],
                address: address.merge(city_id: city_id)
              }
            }

            service = CDI::V1::EducationalInstitutions::CreateService.new(admin_user, params)
            service.execute

            if service.success?
              puts "Instituição '%s' criada com sucesso" % [service.educational_institution.name]
            else
              puts "Erro na criação de instituição de ensino."
              puts service.errors
            end
          end
        end
      end

      def find_or_create_categories
        find_or_create_collection('categories.json', 'Category', 'name')
      end

      def find_or_create_skills
        find_or_create_collection('skills.json', 'Skill', 'name', 'description')
      end

      def find_or_create_collection(seed_filename, class_name, *attributes)
        collection = load_seed_file(seed_filename)

        collection.each_with_index do |data, index|

          record = class_name.constantize.find_or_create_by(data.slice(*attributes))

          if record.persisted?
            print "\r%s/%s" % [index.next, collection.size]
          end
        end

        puts "\nTotal de #{collection.size} #{class_name} adicionadas"
      end

      def create_default_subscriptions
        puts "Inscrevendo usuários a eles mesmos.\n"
        collection = User.all
        collection.each_with_index do |user, index|
          subscribe_user_on_publisher(user, user)
          print "\r%s/%s" % [index.next, collection.size]
        end

        puts "\nInscrevendo usuários às instituições."
        collection = EducationalInstitution.all
        collection.each_with_index do |institution, index|
          institution.users.each {|user| subscribe_user_on_publisher(user, institution) }
          institution.managers.each {|user| subscribe_user_on_publisher(user, institution) }
          subscribe_user_on_publisher(institution.admin_user, institution)
          print "\r%s/%s" % [index.next, collection.size]
        end

        puts "\nInscrevendo alunos aos professores."
        collection = User.student
        collection.each_with_index do |student, index|
          student.classes_teachers.each {|teacher| subscribe_user_on_publisher(student, teacher) }
          print "\r%s/%s" % [index.next, collection.size]
        end
        puts ""
      end

      def subscribe_user_on_publisher(user, publisher)
        service = CDI::V1::UserSubscriptions::CreateService.new(publisher, user)
        service.execute
      end

      def add_flags_to_albums
        default_gallery_title = 'User Uploads'
        default_album_title = 'User Album'

        puts "\nAdicionando flags aos albuns dos usuários."
        Album.all.each do |album|
          album.album_type = if album.title == default_gallery_title
            Album::TYPES[:gallery]
          elsif album.title == default_album_title
            Album::TYPES[:default]
          else
            Album::TYPES[:standard]
          end
          album.save
        end

      end

      # Cycle, Track, SupportingCourse, LearningDynamic, Multimedia, Post
      def fix_categories
        ActiveRecord::Base.transaction do
          models = %w(LearningCycle LearningTrack SupportingCourse LearningDynamic Multimedia)

          puts "Fix Categories"
          models.each do |model|
            model = model.constantize
            total = model.count
            model.all.each_with_index do |m, index|
              category_id = m.category_id
              category_id = m.cycle.category_ids.first if model.name == 'LearningTrack'
              category = Category.find_by(id: category_id)
              m.categories << category if category.present?
              print "\r#{model.name} #{index.next}/#{total}"
            end
            puts
          end
        end
      end

      def fix_supporting_courses_parent
        puts 'Fix supporting courses'
        SupportingCourse.group(:title)
        .select("count(id) AS total, title")
        .select { |s| s.total > 1 }
        .each do |sc|
          q = SupportingCourse.where(title: sc.title).order('created_at')
          original = q.first
          q.where.not(id: original.id).update_all(parent_id: original.id)
        end
      end

      def fix_supporting_courses_presentation_publishment
        puts 'Fix supporting courses presentation publishment'
        SupportingCourse.published.map(&:presentation).each { |p| p.set_status :published }
      end

      def migrate_posts_visibilities
        puts "\nMigrando a visibilidade dos posts para a nova estrutura."
        collection = Post.all
        collection.each_with_index do |post, index|
          post.visibilities.create(visibility_type: post.visibility_type, visibility_id: post.visibility_id)
          print "\r%s/%s" % [index.next, collection.size]
        end
        puts ""
      end

      # Precisa de mais teste!
      #
      # Verificar se todos as notificações estão cadastrando informação correta
      def fix_sender_user_id_for_notifications
        Notification.all.each do |notification|
          if notification.notificable.present?
            if notification.notificable_type == 'User'
              notification.update sender_user_id: notification.notificable.id
            else
              notification.update sender_user_id: notification.notificable.user_id
            end
          end
        end
      end

      def create_user(email, profile_type)
        user_password = "#{profile_type.downcase}123"

        params = {
          async: false,
          user: {
            first_name: Faker::Name.first_name,
            last_name: Faker::Name.last_name,
            birthday: Faker::Date.between(40.years.ago, Date.today),
            email: email,
            profile_type: profile_type,
            password: user_password,
            password_confirmation: user_password,
            gender: User::VALID_GENDERS.values.sample
          }
        }

        # TODO: Migrate to CDI::Users::CreateService
        user = User.create(params[:user])

        if user.persisted? && user.valid?
          user.educational_institutions << EducationalInstitution.first rescue false
          puts "\nRegistrando usuário com perfil do tipo = '%s' e email '%s'\n" % [profile_type, email]
        else
          puts "\nErro ao cadastrar usuário: #{user.errors.messages}"
        end
      end

      def load_seed_file(filename)
        JSON.load(Rails.root.join('docs', 'seeds', filename))
      end
    end
  end
end
