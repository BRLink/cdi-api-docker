namespace :ranking do

  desc "Refresh general ranking materialized view"
  task :refresh => :environment do
    puts "Refreshing 'general_user_ranking_matview...'"
    MaterializedViews::GeneralUserRanking.refresh
    puts "Done! :)"
  end
end

