namespace :search_document do

  desc "Recria todos os índices de pesquisa do pg_search_documents"
  task :rebuild, [:model] => :environment do |_task, args|
    model_class = args.model.classify.constantize if args.model
    SearchDocument.rebuild model_class
  end
end
