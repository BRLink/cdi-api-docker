class CreateGameWordSearches < ActiveRecord::Migration
  def change
    create_table :game_word_searches do |t|
      t.references :user, index: true
      t.string :words, array: true
      t.string :status
      t.datetime :status_changed_at

      t.timestamps null: false
    end

    add_index :game_word_searches, :words, using: 'gin'
    add_index :game_word_searches, :status

    add_foreign_key :game_word_searches, :users
  end
end
