class ChangeArticleCoverImageToString < ActiveRecord::Migration
  def change
  	remove_column :blog_articles, :blog_media_id
  	add_column :blog_articles, :cover_image_url, :string
  end
end
