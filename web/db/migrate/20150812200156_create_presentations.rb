class CreatePresentations < ActiveRecord::Migration
  def change
    create_table :presentations do |t|
      t.references :user, index: true
      t.references :learning_track, index: true
      t.integer :slides_count
      t.string :status, index: true
      t.datetime :status_changed_at

      t.timestamps null: false
    end
    add_foreign_key :presentations, :users
    add_foreign_key :presentations, :learning_tracks
  end
end
