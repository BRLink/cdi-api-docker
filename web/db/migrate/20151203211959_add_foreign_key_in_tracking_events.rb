class AddForeignKeyInTrackingEvents < ActiveRecord::Migration
  def change
    add_foreign_key :tracking_events, :enrolled_learning_tracks, on_delete: :cascade
  end
end
