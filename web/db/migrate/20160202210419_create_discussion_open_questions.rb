class CreateDiscussionOpenQuestions < ActiveRecord::Migration
  def change
    create_table :discussion_open_questions do |t|
      t.timestamps
      t.belongs_to :user, null: false
      t.string :title, null: false
      t.integer :questions_count, null: false, default: 0
      t.integer :answers_count, null: false, default: 0
      t.integer :cover_id
      t.string :video_link
      t.integer :parent_id
    end
    add_foreign_key :discussion_open_questions, :users, on_delete: :cascade
  end
end
