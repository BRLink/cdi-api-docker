class CreateSurveyQuestionAnswers < ActiveRecord::Migration
  def change
    create_table :survey_question_answers do |t|
      t.timestamps
      t.belongs_to :user
      t.belongs_to :survey_question
      t.integer :option_id
      t.string :answer_body
    end
    add_foreign_key :survey_question_answers, :users, on_cascade: :delete
    add_foreign_key :survey_question_answers, :survey_questions, on_cascade: :delete
  end
end
