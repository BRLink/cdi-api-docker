class AddVideoLinkToBasePoll < ActiveRecord::Migration
  def change
    add_column :base_polls, :video_link, :string
  end
end
