class CreateSubscriberPost < ActiveRecord::Migration
  def change
    create_table :subscriber_posts do |t|
      t.references :post, index: true
      t.references :user_subscription, index: true
      t.timestamps null: false
    end
    add_foreign_key :subscriber_posts, :posts
    add_foreign_key :subscriber_posts, :user_subscriptions
  end
end
