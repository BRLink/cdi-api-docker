class CreateResourceHistories < ActiveRecord::Migration
  def change
    create_table :resource_histories do |t|
      t.string :historable_type
      t.integer :historable_id
      t.string :action_type
      t.string :new_status
      t.string :last_status
      t.references :user, index: true
      t.hstore :user_data

      t.timestamps null: false
    end
    add_index :resource_histories, :historable_type
    add_index :resource_histories, :historable_id
    add_foreign_key :resource_histories, :users
  end
end
