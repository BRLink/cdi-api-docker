class AddDeleteCascadeToSurveyQuestionAnswers < ActiveRecord::Migration
  def change
    remove_foreign_key :survey_question_answers, :survey_questions
    add_foreign_key :survey_question_answers, :survey_questions, on_delete: :cascade
  end
end
