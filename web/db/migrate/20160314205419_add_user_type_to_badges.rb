class AddUserTypeToBadges < ActiveRecord::Migration
  def change
    add_column :badges, :user_type, :json
  end
end
