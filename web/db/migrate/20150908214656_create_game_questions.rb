class CreateGameQuestions < ActiveRecord::Migration
  def change
    create_table :game_questions do |t|
      t.references :questionable, index: true, polymorphic: true
      t.string :input_type
      t.integer :position
      t.string :title
      t.boolean :correct, default: false

      t.timestamps null: false
    end
    add_index :game_questions, :title
    add_index :game_questions, :position
    add_index :game_questions, :questionable_id
    add_index :game_questions, :questionable_type
  end
end
