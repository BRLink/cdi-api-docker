class ChangeNotificationConfigs < ActiveRecord::Migration
  def change
    add_index :notification_configs, [:user_id, :notification_type], unique: true
    change_column_default :notification_configs, :receive_email, true
    change_column_default :notification_configs, :receive_notification, true
  end
end
