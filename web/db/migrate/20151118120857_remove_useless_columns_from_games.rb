class RemoveUselessColumnsFromGames < ActiveRecord::Migration

  def up
    remove_column :game_polls, :title, :string
    remove_column :game_polls, :description, :string
    remove_column :game_polls, :image, :string
    remove_column :game_quizzes, :title, :string
    remove_column :game_quizzes, :description, :string
    remove_column :game_quizzes, :image, :string
  end

  def down
    add_column :game_polls, :title, :string
    add_index :game_polls, :title
    add_column :game_polls, :description, :string
    add_column :game_polls, :image, :string
    add_column :game_quizzes, :title, :string
    add_index :game_quizzes, :title
    add_column :game_quizzes, :description, :string
    add_column :game_quizzes, :image, :string
  end
end
