class AddVisibilityOnPosts < ActiveRecord::Migration
  def change
    add_column :posts, :visibility_type, :string, index: true, null: false
    add_column :posts, :visibility_id, :integer, index: true, null: true
  end
end
