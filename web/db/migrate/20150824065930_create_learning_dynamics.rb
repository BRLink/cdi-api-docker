class CreateLearningDynamics < ActiveRecord::Migration
  def change
    create_table :learning_dynamics do |t|
      t.string :title
      t.references :category, index: true
      t.references :learning_track, index: true
      t.references :user, index: true
      t.integer :student_presentation_id
      t.integer :multiplier_presentation_id
      t.string :perspective, array: true
      t.string :status, index: true
      t.datetime :status_changed_at

      t.timestamps null: false
    end

    add_index :learning_dynamics, :perspective, using: 'gin'
    add_foreign_key :learning_dynamics, :categories
    add_foreign_key :learning_dynamics, :learning_tracks
    add_foreign_key :learning_dynamics, :users
  end
end
