class AddIdToChallengeSubscription < ActiveRecord::Migration
  def change
    add_column :challenge_subscriptions, :id, :primary_key
  end
end
