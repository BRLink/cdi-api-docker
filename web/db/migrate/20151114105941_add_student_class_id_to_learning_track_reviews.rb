class AddStudentClassIdToLearningTrackReviews < ActiveRecord::Migration
  def change
    add_reference :learning_track_reviews, :student_class, index: true
    add_foreign_key :learning_track_reviews, :student_classes
  end
end
