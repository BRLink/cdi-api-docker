class AddGroupSizeToLearningDelivery < ActiveRecord::Migration
  def change
    add_column :learning_deliveries, :group_size, :integer
  end
end
