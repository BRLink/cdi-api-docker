class AddUserToImagesToPdfTask < ActiveRecord::Migration
  def change
    add_reference :images_to_pdf_tasks, :user, index: true
    add_foreign_key :images_to_pdf_tasks, :users
  end
end
