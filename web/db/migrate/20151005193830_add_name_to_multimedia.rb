class AddNameToMultimedia < ActiveRecord::Migration
  def change
    change_table :multimedia do |t|
      t.string :name, default: ""
    end
  end
end
