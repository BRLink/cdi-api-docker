class AddLearningTrackToStudentDeliveries < ActiveRecord::Migration
  def change
    add_reference :student_deliveries, :learning_track, index: true
    add_foreign_key :student_deliveries, :learning_tracks
  end
end
