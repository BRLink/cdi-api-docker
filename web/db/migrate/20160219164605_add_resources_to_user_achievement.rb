class AddResourcesToUserAchievement < ActiveRecord::Migration
  def change
    add_column :user_achievements, :resource_id,  :integer, null: false, index: true
    add_column :user_achievements, :resource_type, :string, null: false, index: true
  end
end
