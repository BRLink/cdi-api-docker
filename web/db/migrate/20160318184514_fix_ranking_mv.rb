class FixRankingMv < ActiveRecord::Migration
  def up
    execute 'DROP MATERIALIZED VIEW IF EXISTS general_user_ranking_matview'
    execute <<-MATVIEW
    CREATE MATERIALIZED VIEW general_user_ranking_matview AS
      SELECT row_number() over (ORDER BY score DESC) as position,
             user_id, score as default_score FROM
     (SELECT
             u.id as user_id,
             coalesce(sum(a.default_score), 0) AS score
        FROM users u
   LEFT JOIN user_achievements u_a
          ON u_a.user_id = u.id
   LEFT JOIN achievements a
          ON u_a.achievement_id = a.id
    GROUP BY u.id
    ) as ranking;

    CREATE UNIQUE INDEX idx_general_user_ranking_matview_position
        ON general_user_ranking_matview (position);
    CREATE UNIQUE INDEX idx_general_user_ranking_matview_user
        ON general_user_ranking_matview (user_id);

    MATVIEW
  end
  def down
    execute 'DROP MATERIALIZED VIEW IF EXISTS general_user_ranking_matview'
  end
end
