class CreateQuestionTemplates < ActiveRecord::Migration
  def change
    create_table :question_templates do |t|
      t.string :title
      t.string :question_type
      t.string :question_for
      t.string :instructions

      t.timestamps null: false
    end
    add_index :question_templates, :question_type
    add_index :question_templates, :question_for
  end
end
