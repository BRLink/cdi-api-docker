class RenameBaseQuestionAnswersToGameQuestionAnswers < ActiveRecord::Migration
  def change
    rename_table :base_question_answers, :game_question_answers
    rename_column :game_question_answers, :base_question_id, :game_question_id
    remove_column :game_question_answers, :type
  end
end
