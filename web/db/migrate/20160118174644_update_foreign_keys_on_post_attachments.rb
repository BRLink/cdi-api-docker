class UpdateForeignKeysOnPostAttachments < ActiveRecord::Migration
  def change
    remove_foreign_key :post_attachments, :posts
    add_foreign_key :post_attachments, :posts, on_delete: :cascade
  end
end
