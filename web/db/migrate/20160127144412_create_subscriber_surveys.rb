class CreateSubscriberSurveys < ActiveRecord::Migration
  def change
    create_table :subscriber_surveys do |t|
      t.belongs_to :user_subscription, null: false
      t.belongs_to :survey, polymorphic: true
      t.timestamps
    end
    add_foreign_key :subscriber_surveys, :user_subscriptions, on_delete: :cascade
    add_index :subscriber_surveys, [:user_subscription_id, :survey_type, :survey_id], unique: true, name: 'index_subscriber_surveys_on_user_subscription_and_survey'
  end
end
