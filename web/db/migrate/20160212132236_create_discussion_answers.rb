class CreateDiscussionAnswers < ActiveRecord::Migration
  def change
    create_table :discussion_answers do |t|
      t.belongs_to :user, null: false
      t.belongs_to :discussion, null: false
      t.belongs_to :option, null: false, index: true
      t.timestamps null: false
    end
    add_foreign_key :discussion_answers, :users, on_delete: :cascade
    add_foreign_key :discussion_answers, :discussions, on_delete: :cascade
    add_index :discussion_answers, [:option_id, :user_id], unique: true
  end
end
