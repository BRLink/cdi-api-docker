class AddIndexToUsers < ActiveRecord::Migration
  def change
    add_index :users, :username
    add_index :users, :profile_type
  end
end
