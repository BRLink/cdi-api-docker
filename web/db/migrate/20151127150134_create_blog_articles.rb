class CreateBlogArticles < ActiveRecord::Migration
  def change
    create_table :blog_articles do |t|

      t.references :user
      t.references :blog_category

      t.string  :title, null: false
      t.string  :excerpt, null: false
      t.text    :body
      # cover image
      t.references :blog_media
      t.string :tags, array: true

      t.integer :article_type
      t.integer :status

      t.timestamps null: false
    end
  end
end
