class CreateGameQuestionAnswers < ActiveRecord::Migration
  def change
    create_table :game_question_answers do |t|
      t.references :user, index: true
      t.references :game_question, index: true
      t.integer :option_id

      t.timestamps null: false
    end
    add_index :game_question_answers, :option_id
    add_foreign_key :game_question_answers, :game_questions
    add_foreign_key :game_question_answers, :game_question_options, column: :option_id
  end
end
