class CreateGameQuizzes < ActiveRecord::Migration
  def change
    create_table :game_quizzes do |t|
      t.references :user, index: true
      t.string :title
      t.string :description
      t.string :status
      t.datetime :status_changed_at
      t.string :image

      t.timestamps null: false
    end
    add_index :game_quizzes, :status
    add_foreign_key :game_quizzes, :users
  end
end
