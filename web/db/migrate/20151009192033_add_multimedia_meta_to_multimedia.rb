class AddMultimediaMetaToMultimedia < ActiveRecord::Migration
  def change
    add_column :multimedia, :metadata, :json
    add_column :multimedia, :multimedia_type, :string
    add_index :multimedia, :multimedia_type
  end
end
