class CreateCategoriesPosts < ActiveRecord::Migration
  def change
    create_table :categories_posts, id: false do |t|
      t.belongs_to :category, index: true
      t.belongs_to :post, index: true
    end

    add_index :categories_posts, [:category_id, :post_id], unique: true, name: 'categories_posts_category_id_post_id_unique_index'

    add_foreign_key :categories_posts, :categories, on_delete: :cascade
    add_foreign_key :categories_posts, :posts, on_delete: :cascade
  end
end
