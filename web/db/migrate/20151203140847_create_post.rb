class CreatePost < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.references :user, index: true
      t.string :content, null: false
      t.integer :parent_id, null: true, index: true
      t.string :status, null: false
      t.timestamps null: false
    end
    add_foreign_key :posts, :users
  end
end
