class CreateGameUserScores < ActiveRecord::Migration
  def change
    create_table :game_user_scores do |t|
      t.references :user, index: true
      t.integer :game_id
      t.string :game_type
      t.references :student_class, index: true
      t.integer :score_points
      t.integer :score_percent

      t.timestamps null: false
    end
    add_index :game_user_scores, :game_id
    add_index :game_user_scores, :game_type
    add_index :game_user_scores, [:game_id, :game_type, :user_id, :student_class_id], unique: true, name: 'unique_index_on_game_scores_game_id_user_id_class_id'
    add_foreign_key :game_user_scores, :users
    add_foreign_key :game_user_scores, :student_classes
  end
end
