class CreateNotificationConfigs < ActiveRecord::Migration
  def change
    create_table :notification_configs do |t|
      t.string :notification_type
      t.references :user, index: true
      t.boolean :receive_email, default: false
      t.boolean :receive_notification, default: false

      t.timestamps null: false
    end
    add_foreign_key :notification_configs, :users
    add_index :notification_configs, :notification_type
  end
end
