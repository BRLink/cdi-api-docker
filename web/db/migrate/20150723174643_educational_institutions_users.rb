class EducationalInstitutionsUsers < ActiveRecord::Migration
  def change
    create_table :educational_institutions_users, id: false do |t|
      t.integer :educational_institution_id
      t.integer :user_id
    end
    # TODO: add indexes?
  end
end
