class CreateStates < ActiveRecord::Migration
  def change
    create_table :states do |t|
      t.string :name
      t.string :acronym

      t.timestamps null: false
    end
    add_index :states, :acronym
  end
end
