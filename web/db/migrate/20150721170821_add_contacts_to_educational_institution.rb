class AddContactsToEducationalInstitution < ActiveRecord::Migration
  def change
    ActiveRecord::Schema.define do
      enable_extension 'hstore' unless extension_enabled?('hstore')

      change_table :educational_institutions do |t|
        t.hstore :contacts
      end
    end

  end
end
