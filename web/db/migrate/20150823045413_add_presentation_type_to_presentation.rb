class AddPresentationTypeToPresentation < ActiveRecord::Migration
  def change
    add_column :presentations, :presentation_type, :string
    add_index :presentations, :presentation_type
  end
end
