class AddTimeLineToMultimedia < ActiveRecord::Migration
  def change
    change_table :multimedia do |t|
      t.timestamps
    end
  end
end
