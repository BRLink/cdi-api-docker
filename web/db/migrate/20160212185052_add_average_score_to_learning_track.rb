class AddAverageScoreToLearningTrack < ActiveRecord::Migration
  def change
    add_column :learning_tracks, :average_score, :float, :null => false, :default => 0
  end
end
