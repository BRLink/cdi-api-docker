class CreateAchievements < ActiveRecord::Migration
  def change
    create_table :achievements do |t|
      t.string :action,  null: false
      t.string :area,    null: false
      t.integer :points, null: false

      t.timestamps null: false
    end
  end
end
