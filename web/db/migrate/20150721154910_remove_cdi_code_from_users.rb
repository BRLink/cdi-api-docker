class RemoveCdiCodeFromUsers < ActiveRecord::Migration
  def change
    # this code must be in instituitions
    remove_column :users, :cdi_code
  end
end
