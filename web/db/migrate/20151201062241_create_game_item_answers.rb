class CreateGameItemAnswers < ActiveRecord::Migration
  def change
    create_table :game_item_answers do |t|
      t.references :game_item, index: true
      t.references :user, index: true
      t.boolean :correct
      t.string :reply_word

      t.timestamps null: false
    end
    add_index :game_item_answers, :correct
    add_index :game_item_answers, [:game_item_id, :user_id], unique: true, name: 'unique_in_on_game_item_answers_game_id_user_id'

    add_foreign_key :game_item_answers, :game_items
    add_foreign_key :game_item_answers, :users
  end
end
