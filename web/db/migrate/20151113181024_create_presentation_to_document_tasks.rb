class CreatePresentationToDocumentTasks < ActiveRecord::Migration
  def change
    create_table :presentation_to_document_tasks do |t|
      t.references :presentation, index: true
      t.integer :status, default: 0
      t.integer :retries, default: 0
      t.string :ins, array: true
    end
  end
end
