class RenamePresentationToDocumentTasks < ActiveRecord::Migration
  def change
    rename_table :learning_dynamics_to_document_tasks, :learning_dynamic_to_document_tasks
  end
end
