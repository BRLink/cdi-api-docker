class CreateSupportingCoursesSkills < ActiveRecord::Migration
  def change
    create_table :supporting_courses_skills, id: false do |t|
      t.integer :supporting_course_id
      t.integer :skill_id
    end
  end
end
