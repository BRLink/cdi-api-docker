class CreateLearningTrackReports < ActiveRecord::Migration
  def change
    create_table :learning_track_reports do |t|
      t.references :learning_track, index: true
      t.references :student_class, index: true
      t.hstore :enrollment_reports
      t.hstore :reviews_reports
      t.json :students_reports
      t.string :status
      t.datetime :status_changed_at

      t.timestamps null: false
    end

    add_index :learning_track_reports, [:learning_track_id, :student_class_id], name: "index_tracks_reports_on_track_id_class_id", unique: true
    add_index :learning_track_reports, :status
  end
end
