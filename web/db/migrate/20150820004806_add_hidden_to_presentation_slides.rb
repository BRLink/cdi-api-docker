class AddHiddenToPresentationSlides < ActiveRecord::Migration
  def change
    add_column :presentation_slides, :hidden, :boolean, default: false
  end
end
