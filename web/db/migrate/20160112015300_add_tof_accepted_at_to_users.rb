class AddTofAcceptedAtToUsers < ActiveRecord::Migration
  def change
    add_column :users, :tof_accepted_at, :datetime
  end
end
