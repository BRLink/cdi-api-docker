class CreateGameQuestionOptions < ActiveRecord::Migration
  def change
    create_table :game_question_options do |t|
      t.references :game_question, index: true
      t.integer :position
      t.string :text
      t.string :description
      t.boolean :correct, default: false

      t.timestamps null: false
    end
    add_index :game_question_options, :position
    add_foreign_key :game_question_options, :game_questions, on_delete: :cascade
  end
end
