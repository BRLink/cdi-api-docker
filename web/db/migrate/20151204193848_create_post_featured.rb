class CreatePostFeatured < ActiveRecord::Migration
  def change
    create_table :post_featureds do |t|
      t.references :user, index: true
      t.references :post, index: true
      t.timestamps null: false
    end
    add_foreign_key :post_featureds, :users
    add_foreign_key :post_featureds, :posts
  end
end
