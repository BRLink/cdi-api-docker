class AddCustomDataToResourceHistories < ActiveRecord::Migration
  def change
    add_column :resource_histories, :custom_data, :hstore
  end
end
