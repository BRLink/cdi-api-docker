class CreateChallengeDeliveries < ActiveRecord::Migration
  def change
    create_table :challenge_deliveries do |t|
      t.references :challenge, index: true, null: false
      t.string :title
      t.text :description
      t.string :cover
      t.references :presentation
      t.string :video_link
      t.string :file

      t.timestamps null: false
    end
    add_foreign_key :challenge_deliveries, :challenges
    add_foreign_key :challenge_deliveries, :presentations
  end
end
