class CreateChatRoomsUsers < ActiveRecord::Migration
  def change
    create_table :chat_rooms_users do |t|
      t.integer :user_id, null: false
      t.integer :chat_room_id, null: false
      t.integer :role, default: 0, null: false
      t.timestamps null: false
    end
  end
end
