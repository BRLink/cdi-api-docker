class RenameActiveToVisibleInPhotos < ActiveRecord::Migration
  def change
    rename_column :photos, :active, :visible
  end
end
