class RemovePresentationSlideColumns < ActiveRecord::Migration
  def change
    if column_exists?(:presentation_slides, :raw_html_content)
      remove_column :presentation_slides, :raw_html_content, :text
    end
    if column_exists?(:presentation_slides, :parsed_json_content)
      remove_column :presentation_slides, :parsed_json_content, :hstore
    end
  end
end
