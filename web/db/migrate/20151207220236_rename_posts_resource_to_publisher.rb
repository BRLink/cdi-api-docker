class RenamePostsResourceToPublisher < ActiveRecord::Migration
  def change
    rename_column :posts, :resource_id, :publisher_id
    rename_column :posts, :resource_type, :publisher_type
  end
end
