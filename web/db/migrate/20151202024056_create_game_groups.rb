class CreateGameGroups < ActiveRecord::Migration
  def change
    create_table :game_groups do |t|
      t.references :game_group_item, index: true
      t.string :title
      t.string :items, array: true

      t.timestamps null: false
    end

#    add_index :game_groups, [:game_group_item_id, :items], unique: true, name: 'unique_index_on_game_groups_game_group_items'
    add_index :game_groups, [:game_group_item_id, :title], unique: true, name: 'unique_index_on_game_groups_game_group_title'

    add_foreign_key :game_groups, :game_group_items, on_delete: :cascade

    add_index :game_groups, :items, using: 'gin'
    add_index :game_groups, :title
  end
end
