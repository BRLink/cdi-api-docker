class RenameTableChatRoomsUsers < ActiveRecord::Migration
  def change
    rename_table :chat_rooms_users, :chat_room_users
  end
end
