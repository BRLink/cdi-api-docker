class CreateSubscriberDiscussions < ActiveRecord::Migration
  def change
    create_table :subscriber_discussions do |t|
      t.belongs_to :user_subscription, null: false
      t.belongs_to :discussion, polymorphic: true
      t.timestamps
    end
    add_foreign_key :subscriber_discussions, :user_subscriptions, on_delete: :cascade
    add_index :subscriber_discussions, [:user_subscription_id, :discussion_type, :discussion_id], unique: true, name: 'idx_subscriber_discussions_on_user_subscription_and_discussion'
  end
end
