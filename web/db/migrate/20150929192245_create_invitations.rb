class CreateInvitations < ActiveRecord::Migration
  def change
    create_table :invitations do |t|
      # user info
      t.string   :profile_type
      t.string   :email,        null: false, unique: true
      t.string   :first_name
      t.string   :last_name

      # token
      t.string   :token,        null: false
      t.integer  :status,       default: 0
      t.datetime :expires_at

      #default
      t.timestamps null: false

    end

    add_index :invitations, [:token], name: "index_invitations_on_token", unique: true, using: :btree

  end
end
