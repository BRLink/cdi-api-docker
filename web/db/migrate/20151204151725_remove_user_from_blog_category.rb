class RemoveUserFromBlogCategory < ActiveRecord::Migration
  def change
  	remove_column :blog_categories, :user_id, :integer
  end
end
