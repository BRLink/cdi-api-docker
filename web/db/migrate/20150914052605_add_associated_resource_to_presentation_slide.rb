class AddAssociatedResourceToPresentationSlide < ActiveRecord::Migration
  def change
    add_reference :presentation_slides, :associated_resource, polymorphic: true
  end
end
