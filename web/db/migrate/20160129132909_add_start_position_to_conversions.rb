class AddStartPositionToConversions < ActiveRecord::Migration
  def change
    add_column :conversions, :start_position, :integer
  end
end
