class AddThumbToLearningDynamics < ActiveRecord::Migration
  def change
    change_table :learning_dynamics do |t|
      t.string :thumb_url
    end
  end
end
