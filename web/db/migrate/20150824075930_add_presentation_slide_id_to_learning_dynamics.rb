class AddPresentationSlideIdToLearningDynamics < ActiveRecord::Migration
  def change
    add_column :presentation_slides, :learning_dynamic_id, :integer, index: true
  end
end
