class AddStatusAndStatusChangedAtToLearningCycle < ActiveRecord::Migration
  def change
    add_column :learning_cycles, :status, :string
    add_index :learning_cycles, :status
    add_column :learning_cycles, :status_changed_at, :datetime
  end
end
