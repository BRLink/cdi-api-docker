class CreateSupportingCourses < ActiveRecord::Migration
  def change
    create_table :supporting_courses do |t|
      t.string :title
      t.references :category, index: true
      t.references :user, index: true
      t.string :tags, array: true
      t.references :presentation, index: true

      t.timestamps null: false

      t.string :status
      t.datetime :status_changed_at
    end
    add_foreign_key :supporting_courses, :categories
    add_foreign_key :supporting_courses, :users
    add_index :supporting_courses, :tags, using: 'gin'
  end
end
