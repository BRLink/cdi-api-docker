class AddEnrolledLearningTrackIdToTrackingEvents < ActiveRecord::Migration
  def change
    add_column :tracking_events, :enrolled_learning_track_id, :integer
    add_index :tracking_events, :enrolled_learning_track_id
  end
end
