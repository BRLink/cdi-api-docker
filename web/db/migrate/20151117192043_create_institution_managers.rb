class CreateInstitutionManagers < ActiveRecord::Migration
  def change
    create_table :institution_managers do |t|
      t.integer :managed_institution_id
      t.integer :manager_id
      t.timestamps null: false
    end
    add_index :institution_managers, :managed_institution_id, name: 'index_institution_managers_institution_id'
    add_index :institution_managers, [:managed_institution_id, :manager_id], name: 'index_institution_managers_institution_id_user_id', unique: true
    add_index :institution_managers, :manager_id, name: 'index_institution_managers_user_id'
  end
end
