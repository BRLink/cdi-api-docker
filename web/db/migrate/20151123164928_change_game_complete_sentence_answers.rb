class ChangeGameCompleteSentenceAnswers < ActiveRecord::Migration
  def up
    answers = ActiveRecord::Base.connection.execute("SELECT * FROM game_complete_sentence_answers").to_a

    remove_index :game_complete_sentence_answers, name: 'uniq_index_game_sentence_answer_on_user_id_game_id', column: [:user_id, :game_complete_sentence_id]
    remove_foreign_key :game_complete_sentence_answers, :game_complete_sentences
    remove_column :game_complete_sentence_answers, :game_complete_sentence_id

    add_column :game_complete_sentence_answers, :game_wordable_id, :integer
    add_column :game_complete_sentence_answers, :game_wordable_type, :string

    add_index :game_complete_sentence_answers, :game_wordable_id
    add_index :game_complete_sentence_answers, :game_wordable_type

    add_index :game_complete_sentence_answers, [:user_id, :game_wordable_id, :game_wordable_type], unique: true, name: 'uniq_index_game_sentence_answer_on_user_id_game_id'

    rename_table :game_complete_sentence_answers, :game_word_answers

    Thread.new {
      answers.each do |answer|

        answer.merge!('game_wordable_id': answer.delete('game_complete_sentence_id'), 'game_wordable_type': "GameCompleteSentence")

        game_id = answer['game_wordable_id'] || answer[:game_wordable_id]

        game = GameCompleteSentence.find_by(id: game_id)

        answer['words'] = (answer['words'] || '').gsub(/(\}|\{|\")/im, '').split(',')

        if game
          game.answers.create(answer) rescue nil
        end
      end
    }

  end

  def down
    answers = ActiveRecord::Base.connection.execute("SELECT * FROM game_word_answers").to_a

    rename_table :game_word_answers, :game_complete_sentence_answers
    remove_index :game_complete_sentence_answers, name: 'uniq_index_game_sentence_answer_on_user_id_game_id'

    remove_column :game_complete_sentence_answers, :game_wordable_id
    remove_column :game_complete_sentence_answers, :game_wordable_type

    add_column :game_complete_sentence_answers, :game_complete_sentence_id, :integer

    add_index :game_complete_sentence_answers, :game_complete_sentence_id, name: 'index_game_complete_sentence_answers_on_game_id'
    add_index :game_complete_sentence_answers, [:user_id, :game_complete_sentence_id], unique: true, name: 'uniq_index_game_sentence_answer_on_user_id_game_id'

    add_foreign_key :game_complete_sentence_answers, :game_complete_sentences

    Thread.new {
      answers.each do |answer|
        game = answer.delete('game_wordable_type').constantize.find_by(id: answer.delete('game_wordable_id')) rescue nil

        if game
          game.answers.create(answer) rescue nil
        end
      end
    }

  end
end
