class DropOldDiscussions < ActiveRecord::Migration
  def change
    drop_table :discussion_question_answers
    drop_table :discussion_question_options
    drop_table :discussion_questions
    drop_table :discussion_open_questions
    drop_table :discussion_polls
  end
end
