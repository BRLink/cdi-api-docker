class AddStatusAndStatusChangedAtToLearningTrack < ActiveRecord::Migration
  def change
    add_column :learning_tracks, :status, :string
    add_index :learning_tracks, :status
    add_column :learning_tracks, :status_changed_at, :datetime
  end
end
