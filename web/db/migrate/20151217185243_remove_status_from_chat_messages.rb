class RemoveStatusFromChatMessages < ActiveRecord::Migration
  def change
    remove_column :chat_messages, :status, :integer
    remove_column :chat_messages, :status_changed_at, :datetime
  end
end
