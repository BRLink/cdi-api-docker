class RenameGameQuestionAnswersToBaseQuestionAnswers < ActiveRecord::Migration
  def change
    rename_table :game_question_answers, :base_question_answers
    add_column :base_question_answers, :type, :string, index: true
    rename_column :base_question_answers, :game_question_id, :base_question_id
  end
end
