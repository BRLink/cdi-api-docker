actions = ENV['ACTIONS'].present? ? ENV['ACTIONS'].split(',').map(&:squish) : nil
CDI::Services::CreateDefaultDataService.new(actions: actions).execute