#!/bin/bash

RACK_ENV="development"

echo "> Starting cdi-api for env '${RACK_ENV}' ..."

docker run \
	   -d \
	   -p 9090:80 \
	   -v $(pwd):/app \
	   --link postgres:pg \
	   --link redis:rd \
	   --link memcache:mc \
	   --name cdi-api-web \
	   -e RACK_ENV="${RACK_ENV}" \
	   ever-ag/cdi-api /app/project/docker/bin/boot-web.sh

docker run \
	   -d \
	   -v $(pwd):/app \
	   --link postgres:pg \
	   --link redis:rd \
	   --link memcache:mc \
	   --name cdi-api-worker \
	   -e RACK_ENV="${RACK_ENV}" \
	   ever-ag/cdi-api /app/project/docker/bin/boot-worker.sh
