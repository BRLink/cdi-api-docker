#!/bin/bash

echo "> Starting Memcached ..."
docker run -d -P --name memcache memcached

echo "> Starting Redis ..."
docker run -d -P --name redis ever-ag/redis

echo "> Starting Postgresql ..."
docker run -d -P --name postgres ever-ag/pg
