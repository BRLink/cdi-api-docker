#!/bin/bash

source /app/project/docker/env/${RACK_ENV:-development}.bash

cp -aR /app/project/* /app/current
cd /app/current
bundle install --quiet --deployment --without=development test
bundle exec rake db:migrate
bundle exec puma -C config/puma.rb
