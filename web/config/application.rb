require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
# require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

require File.join(File.dirname(__FILE__), '..', 'lib', 'cdi', 'config')

module CdiApi
  class Application < Rails::Application

    config.generators do |g|
      # g.template_engine nil
      g.stylesheets     false
      g.javascripts     false
      g.assets          false
      g.helper          false
    end

    config.time_zone                = 'Brasilia'
    config.i18n.default_locale      = :'pt-BR'
    config.i18n.available_locales   = [:en, :'pt-BR']

    config.active_job.queue_adapter = :sidekiq

    # load Grape API files
    config.paths.add File.join('app', 'grape'), glob: File.join('**', '*.rb')
    config.paths.add File.join('lib', 'cdi'), glob: File.join('**', '*.rb')

    config.autoload_paths << File.join(Rails.root, 'lib')
    config.autoload_paths += Dir.glob(File.join(Rails.root, 'app', 'grape', '{**,*}'))
    config.autoload_paths += Dir.glob(File.join(Rails.root, 'lib', 'cdi', '{**,*}'))

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true
  end
end
