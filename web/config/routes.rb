# encoding: UTF-8
require 'sidekiq/web'

Rails.application.routes.draw do

  mount API::Base => '/'

  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    username == CDI::Config.sidekiq_username && password == CDI::Config.sidekiq_password
  end

  mount Sidekiq::Web, at: '/sidekiq'
end

