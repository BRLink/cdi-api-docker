Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # The test environment is used exclusively to run your application's
  # test suite. You never need to work with it otherwise. Remember that
  # your test database is "scratch space" for the test suite and is wiped
  # and recreated between test runs. Don't rely on the data there!
  config.cache_classes = true

  # Do not eager load code on boot. This avoids loading your whole application
  # just for the purpose of running a single test. If you are using a tool that
  # preloads Rails for running tests, you may have to set it to true.
  config.eager_load = false

  # Configure static file server for tests with Cache-Control for performance.
  config.serve_static_files   = true
  config.static_cache_control = 'public, max-age=3600'

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Raise exceptions instead of rendering exception templates.
  config.action_dispatch.show_exceptions = false

  # Disable request forgery protection in test environment.
  config.action_controller.allow_forgery_protection = false

  cache_servers = (ENV["MEMCACHIER_SERVERS"]  || CDI::Config.cache_servers || "").split(",")
  cache_username = ENV["MEMCACHIER_USERNAME"] || CDI::Config.cache_username
  cache_password = ENV["MEMCACHIER_PASSWORD"] || CDI::Config.cache_password

  cache_namespace = CDI::Config.cache_namespace || "tec-escola-#{Rails.env}"
  cache_max_size  = (CDI::Config.cache_value_max_bytes || (1024 * 1024) * 10).to_f # 10 MB

  cache_options = {
                    :namespace            => cache_namespace,
                    :compress             => true,
                    :username             => cache_username,
                    :password             => cache_password,
                    :failover             => true,
                    :socket_timeout       => (CDI::Config.cache_socket_timeout || 1.5).to_f,
                    :socket_failure_delay => (CDI::Config.cache_socket_failure_delay || 0.2).to_f,
                    # 10MB
                    :value_max_bytes => cache_max_size
                  }

  if CDI::Config.enabled?(:aws_deploy) && !!defined?(Dalli::ElastiCache)
    cache_endpoint = cache_servers.first
    # fetch all "disoverable" memcached nodes
    elasticache = Dalli::ElastiCache.new(cache_endpoint)
    cache_servers = elasticache.servers
  end

  config.cache_store = :dalli_store, cache_servers, cache_options

  # Tell Action Mailer not to deliver emails to the real world.
  # The :test delivery method accumulates sent emails in the
  # ActionMailer::Base.deliveries array.
  config.action_mailer.delivery_method = :test

  # Randomize the order test cases are executed.
  config.active_support.test_order = :random

  # Print deprecation notices to the stderr.
  config.active_support.deprecation = :stderr

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true
end
