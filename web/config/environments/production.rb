# encoding: UTF-8
Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # Code is not reloaded between requests.
  config.cache_classes = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Enable Rack::Cache to put a simple HTTP cache in front of your application
  # Add `rack-cache` to your Gemfile before enabling this.
  # For large-scale production use, consider using a caching reverse proxy like
  # NGINX, varnish or squid.
  # config.action_dispatch.rack_cache = true

  # Disable serving static files from the `/public` folder by default since
  # Apache or NGINX already handles this.
  config.serve_static_files = ENV['RAILS_SERVE_STATIC_FILES'].present?


  # Specifies the header that your server uses for sending files.
  # config.action_dispatch.x_sendfile_header = 'X-Sendfile' # for Apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for NGINX

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true

  # Use the lowest log level to ensure availability of diagnostic information
  # when problems arise.
  config.log_level = :debug

  # Prepend all log lines with the following tags.
  # config.log_tags = [ :subdomain, :uuid ]

  # Use a different logger for distributed setups.
  # config.logger = ActiveSupport::TaggedLogging.new(SyslogLogger.new)

  # Use a different cache store in production.
  # config.cache_store = :mem_cache_store

  cache_servers = (ENV["MEMCACHIER_SERVERS"]  || CDI::Config.cache_servers || "").split(",")
  cache_username = ENV["MEMCACHIER_USERNAME"] || CDI::Config.cache_username
  cache_password = ENV["MEMCACHIER_PASSWORD"] || CDI::Config.cache_password

  cache_namespace = CDI::Config.cache_namespace || "tec-escola-#{Rails.env}"
  cache_max_size  = (CDI::Config.cache_value_max_bytes || (1024 * 1024) * 10).to_f # 10 MB

  cache_options = {
                    :namespace            => cache_namespace,
                    :compress             => true,
                    :username             => cache_username,
                    :password             => cache_password,
                    :failover             => true,
                    :socket_timeout       => (CDI::Config.cache_socket_timeout || 1.5).to_f,
                    :socket_failure_delay => (CDI::Config.cache_socket_failure_delay || 0.2).to_f,
                    # 10MB
                    :value_max_bytes => cache_max_size
                  }

  if CDI::Config.enabled?(:aws_deploy) && !!defined?(Dalli::ElastiCache)
    cache_endpoint = cache_servers.first
    # fetch all "disoverable" memcached nodes
    elasticache = Dalli::ElastiCache.new(cache_endpoint)
    cache_servers = elasticache.servers
  end

  config.cache_store = :dalli_store, cache_servers, cache_options

  # Enable serving of images, stylesheets, and JavaScripts from an asset server.
  # config.action_controller.asset_host = 'http://assets.example.com'

  # Ignore bad email addresses and do not raise email delivery errors.
  # Set this to true and configure the email server for immediate delivery to raise delivery errors.
  # config.action_mailer.raise_delivery_errors = false

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  config.i18n.fallbacks = true

  smtp_username = ENV['SENDGRID_USERNAME'] || CDI::Config.smtp_username
  smtp_password = ENV['SENDGRID_PASSWORD'] || CDI::Config.smtp_password
  smtp_host     = ENV['SENDGRID_HOST']     || CDI::Config.smtp_host
  smtp_port     = ENV['SENDGRID_PORT']     || CDI::Config.smtp_port
  smtp_domain   = ENV['SENDGRID_DOMAIN']   || CDI::Config.smtp_domain

  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings   = {
    :port           => smtp_port,
    :address        => smtp_host,
    :user_name      => smtp_username,
    :password       => smtp_password,
    :domain         => smtp_domain,
    :authentication => :plain,
    :enable_starttls_auto => true
  }

  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # Use default logging formatter so that PID and timestamp are not suppressed.
  config.log_formatter = ::Logger::Formatter.new

  # Do not dump schema after migrations.
  config.active_record.dump_schema_after_migration = false
end
