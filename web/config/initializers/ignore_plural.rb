ActiveSupport::Inflector.inflections do |inflect|
  inflect.irregular 'multimedia', 'multimedia'
  inflect.irregular 'blog_media', 'blog_media'
  inflect.irregular 'true_false', 'true_false'
  inflect.irregular 'game_true_false', 'game_true_false'
end
