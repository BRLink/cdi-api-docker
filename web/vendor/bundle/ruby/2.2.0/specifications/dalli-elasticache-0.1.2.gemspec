# -*- encoding: utf-8 -*-
# stub: dalli-elasticache 0.1.2 ruby lib

Gem::Specification.new do |s|
  s.name = "dalli-elasticache"
  s.version = "0.1.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 1.3.5") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Aaron Suggs", "Zach Millman"]
  s.date = "2014-07-08"
  s.description = "    This gem provides an interface for fetching cluster information from an AWS\n    ElastiCache AutoDiscovery server and configuring a Dalli client to connect\n    to all nodes in the cache cluster.\n"
  s.email = ["aaron@ktheory.com", "zach@magoosh.com"]
  s.homepage = "http://github.com/ktheory/dalli-elasticache"
  s.licenses = ["MIT"]
  s.rdoc_options = ["--charset=UTF-8"]
  s.required_ruby_version = Gem::Requirement.new(">= 1.9.2")
  s.rubygems_version = "2.4.5"
  s.summary = "Configure Dalli clients with ElastiCache's AutoDiscovery"

  s.installed_by_version = "2.4.5" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rake>, [">= 0"])
      s.add_development_dependency(%q<rspec>, [">= 0"])
      s.add_runtime_dependency(%q<dalli>, [">= 1.0.0"])
    else
      s.add_dependency(%q<rake>, [">= 0"])
      s.add_dependency(%q<rspec>, [">= 0"])
      s.add_dependency(%q<dalli>, [">= 1.0.0"])
    end
  else
    s.add_dependency(%q<rake>, [">= 0"])
    s.add_dependency(%q<rspec>, [">= 0"])
    s.add_dependency(%q<dalli>, [">= 1.0.0"])
  end
end
