# -*- encoding: utf-8 -*-
# stub: png_quantizator 0.2.1 ruby lib

Gem::Specification.new do |s|
  s.name = "png_quantizator"
  s.version = "0.2.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Roger Campos"]
  s.date = "2015-08-04"
  s.description = "Small wrapper around pngquant"
  s.email = ["roger@itnig.net"]
  s.homepage = ""
  s.rubygems_version = "2.4.5"
  s.summary = "Small wrapper around pngquant"

  s.installed_by_version = "2.4.5" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rspec>, [">= 0"])
    else
      s.add_dependency(%q<rspec>, [">= 0"])
    end
  else
    s.add_dependency(%q<rspec>, [">= 0"])
  end
end
