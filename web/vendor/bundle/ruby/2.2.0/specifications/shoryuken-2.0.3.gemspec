# -*- encoding: utf-8 -*-
# stub: shoryuken 2.0.3 ruby lib

Gem::Specification.new do |s|
  s.name = "shoryuken"
  s.version = "2.0.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Pablo Cantero"]
  s.date = "2015-12-30"
  s.description = "Shoryuken is a super efficient AWS SQS thread based message processor"
  s.email = ["pablo@pablocantero.com"]
  s.executables = ["shoryuken"]
  s.files = ["bin/shoryuken"]
  s.homepage = "https://github.com/phstc/shoryuken"
  s.licenses = ["LGPL-3.0"]
  s.rubygems_version = "2.4.5"
  s.summary = "Shoryuken is a super efficient AWS SQS thread based message processor"

  s.installed_by_version = "2.4.5" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<bundler>, ["~> 1.6"])
      s.add_development_dependency(%q<rake>, [">= 0"])
      s.add_development_dependency(%q<rspec>, [">= 0"])
      s.add_development_dependency(%q<pry-byebug>, [">= 0"])
      s.add_development_dependency(%q<nokogiri>, [">= 0"])
      s.add_development_dependency(%q<dotenv>, [">= 0"])
      s.add_runtime_dependency(%q<aws-sdk-core>, ["~> 2"])
      s.add_runtime_dependency(%q<celluloid>, ["~> 0.16"])
    else
      s.add_dependency(%q<bundler>, ["~> 1.6"])
      s.add_dependency(%q<rake>, [">= 0"])
      s.add_dependency(%q<rspec>, [">= 0"])
      s.add_dependency(%q<pry-byebug>, [">= 0"])
      s.add_dependency(%q<nokogiri>, [">= 0"])
      s.add_dependency(%q<dotenv>, [">= 0"])
      s.add_dependency(%q<aws-sdk-core>, ["~> 2"])
      s.add_dependency(%q<celluloid>, ["~> 0.16"])
    end
  else
    s.add_dependency(%q<bundler>, ["~> 1.6"])
    s.add_dependency(%q<rake>, [">= 0"])
    s.add_dependency(%q<rspec>, [">= 0"])
    s.add_dependency(%q<pry-byebug>, [">= 0"])
    s.add_dependency(%q<nokogiri>, [">= 0"])
    s.add_dependency(%q<dotenv>, [">= 0"])
    s.add_dependency(%q<aws-sdk-core>, ["~> 2"])
    s.add_dependency(%q<celluloid>, ["~> 0.16"])
  end
end
