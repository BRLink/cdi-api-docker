module CDI
  module V1
    class LearningTrackQuestionsAnswersCreateWorker

      include Sidekiq::Worker

      sidekiq_options :retry => 5, queue: :learning_tracks

      sidekiq_retry_in { |count| count * 60 }

      def perform(learning_track_id, user_id, answers)

        learning_track = learning_track_id.is_a?(::LearningTrack) ?
                         learning_track_id :
                         ::LearningTrack.find_by(id: learning_track_id)

        user = user_id.is_a?(::User) ? user_id : ::User.find_by(id: user_id)

        if learning_track && user
          options = { answers: answers }

          service = CDI::V1::QuestionsAnswersCreateService.new(user, learning_track, options)
          service.execute
        end

      end
    end
  end
end
