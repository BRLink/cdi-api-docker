module CDI
  module V1
    class SendToPusherWorker

      include Sidekiq::Worker

      sidekiq_options :retry => 5, queue: :system_notifications

      sidekiq_retry_in { |count| count * 60 }

      def perform(channel_name, event, options=nil)
        data = {}
        user = User.select(:id, :username, :first_name, :last_name).find_by(id: options[:sender_user_id])
        data[:type] = options[:type]
        data[:notificable_type] = options[:notificable_type]
        data[:notificable_id] = options[:notificable_id]
        data[:user] = user

        localize = I18n.t(options[:notificable_type], scope: 'cdi.notifications')
        data[:message] = localize[:message]

        Pusher.trigger(channel_name, event, data)
      end
    end
  end
end
