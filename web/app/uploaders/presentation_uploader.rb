class PresentationUploader < CarrierWave::Uploader::Base

	# storage Rails.env.development? ? :file : :fog
	storage :fog

	def store_dir
		"#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
	end

	def extension_white_list
		%w(ppt pptx pdf)
	end

end
