class PhotoUploader < BaseImageUploader

  configure do |config|
    config.remove_previously_stored_files_after_update = false
  end

  include Piet::CarrierWaveExtension

  before :cache, :save_original_filename

  VERSIONS = {
    large: {
      size: [800, 800],
      process: true,
      process_method: :resize_to_fit
    },
    medium: {
      size: [500, 500],
      process: true,
      process_method: :resize_to_fit
    },
    small: {
      size: [300, 300],
      process: false,
      process_method: :resize_to_fit
    },
    thumb: {
      size: [180, 180],
      process_method: :resize_to_fill
    }
  }

  DEFAULT_JPG_QUALITY = 85
  DEFAULT_PNG_QUALITY = 5

  if CDI::Config.enabled?(:compress_images_on_upload)
    process :optimize => [ { quality: DEFAULT_JPG_QUALITY, level: DEFAULT_PNG_QUALITY } ]
  end

  process :store_dimensions

  VERSIONS.each do |image_version, options|
    version image_version, if: :create_upload_version? do
      process_options = { (options[:process_method] || :resize_to_fill) => options[:size] }

      process process_options

      if options[:process].present? && CDI::Config.enabled?(:compress_images_on_upload)
        process optimize: [ { quality: DEFAULT_JPG_QUALITY, level: DEFAULT_PNG_QUALITY } ]
      end

      process :store_dimensions
    end
  end

  process resize_to_fill: [800,600], if: :is_slide_thumb?

  process resize_to_fill: [640,640], if: :is_game_item_image?

  version :cover, if: :is_slide_thumb? do
    process optimize: [ { quality: DEFAULT_JPG_QUALITY, level: DEFAULT_PNG_QUALITY } ]
    process resize_to_fill: [480, 360]
  end

  version :other, if: :is_slide_thumb? do
    process resize_to_fill: [240, 180]
  end

  def filename
    unique_filename
  end

  def is_slide_thumb?(version = nil)
    model.try(:slide_thumb_upload).present?
  end

  def is_game_item_image?(version = nil)
    model.try(:game_item_image_upload).present?
  end

  def unique_filename
    "#{secure_token}.#{file.extension}" if original_filename.present?
  end

  def store_dimensions
    if file && model && file.content_type.match(/\Aimage\//) && model.respond_to?(:assets_metadata)
      image = ::MiniMagick::Image.open(file.to_file)

      current_version_name = version_name || :original

      model.assets_metadata ||= {}
      model.assets_metadata[current_version_name] = {
        width: image.width,
        height: image.height,
        size: image.size,
        mime_type: image.mime_type,
        type: image.type
      }
    end
  end

  def create_upload_version?(version)
    return false if model.create_image_versions == false || is_slide_thumb? || is_game_item_image?
    true
  end

  protected
    def save_original_filename(file)
      model.original_filename ||= file.original_filename if file.respond_to?(:original_filename)
    end

    def secure_token
      var = :"@#{mounted_as}_secure_token"
      model.instance_variable_get(var) or model.instance_variable_set(var, SecureRandom.uuid)
    end
end
