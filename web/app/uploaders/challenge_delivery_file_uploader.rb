class ChallengeDeliveryFileUploader < BaseImageUploader

  storage Rails.env.development? ? CDI::Config.enabled?(:upload_to_s3_in_development) ? :fog : :file : :fog

  def store_dir
    "uploads/#{Rails.env.to_s}/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def extension_white_list
    if CDI::Config.enabled?(:strict_challenge_delivery_format_validation)
      return Array.wrap(self.model.format_type)
    end

    ::Challenge::VALID_FORMAT_TYPES.values.flatten.map(&:to_s)
  end

  def filename
    unique_filename
  end

  protected
    def unique_filename
      "#{secure_token}.#{file.extension}" if original_filename.present?
    end

    def secure_token
      var = :"@#{mounted_as}_secure_token"
      model.instance_variable_get(var) or model.instance_variable_set(var, SecureRandom.uuid)
    end
end
