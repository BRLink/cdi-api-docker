class GenericImageUploader < BaseImageUploader

  include Piet::CarrierWaveExtension

  DEFAULT_JPG_QUALITY = 85
  DEFAULT_PNG_QUALITY = 5

  process :optimize => [ { quality: DEFAULT_JPG_QUALITY, level: DEFAULT_PNG_QUALITY } ]
end
