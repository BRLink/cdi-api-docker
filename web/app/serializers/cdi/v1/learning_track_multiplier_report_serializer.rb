module CDI
  module V1
    class LearningTrackMultiplierReportSerializer < ActiveModel::Serializer

      root false

      attributes :starts_at, :ends_at, :tracks
    end
  end
end
