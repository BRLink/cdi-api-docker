module CDI
  module V1
    class SurveyQuestionSerializer < SimpleSurveyQuestionSerializer

      has_many :answers, serializer: SimpleSurveyQuestionAnswerSerializer
    end
  end
end
