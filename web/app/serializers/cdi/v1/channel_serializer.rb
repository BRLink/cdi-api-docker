module CDI
  module V1
    class ChannelSerializer < ActiveModel::Serializer

      has_one :student_class,  serializer: SimpleStudentClassSerializer
      has_one :learning_track, serializer: SimpleLearningTrackSerializer

      attributes :name

    end
  end
end
