module CDI
  module V1
    class SimpleSkillSerializer < ActiveModel::Serializer

      root false

      attributes :id, :name, :description, :position, :created_at, :updated_at

    end
  end
end
