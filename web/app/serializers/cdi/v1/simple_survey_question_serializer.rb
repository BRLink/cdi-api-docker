module CDI
  module V1
    class SimpleSurveyQuestionSerializer < ActiveModel::Serializer

      root false

      attributes :id, :title, :answers_count, :answered

      def answered
        scope && object.answers.exists?(user_id: scope.id)
      end
    end
  end
end
