module CDI
  module V1
    class BlogArticleSerializer < SimpleBlogArticleSerializer

      include CDI::V1::SerializerConcerns::CommentableSerializer
      include CDI::V1::SerializerConcerns::LikeableSerializer
      has_many :categories, serializer: SimpleCategorySerializer

    end
  end
end
