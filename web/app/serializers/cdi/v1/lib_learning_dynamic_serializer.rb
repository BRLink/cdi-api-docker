module CDI
  module V1
    class LibLearningDynamicSerializer < SimpleLearningDynamicSerializer
      has_many :skills, serializer: SimpleSkillSerializer
      has_one :user, serializer: SimpleUserSerializer
      has_many :categories, serializer: SimpleCategorySerializer
    end
  end
end
