module CDI
  module V1
    class LibMultimediaSerializer < SimpleMultimediaSerializer

      has_one :user, serializer: SimpleUserSerializer
      has_many :categories, serializer: SimpleCategorySerializer
    end
  end
end
