module CDI
  module V1
    class SimplePresentationSlideLearningDynamicSerializer < SimpleLearningDynamicSerializer

      attributes :category, :student_presentation, :multiplier_presentation

      ### Dont touch below this (magic happens (aka gambeta))

      def category
        self.object.try(:category).try(:as_json, only: [:id, :name])
      end

      [:student_presentation, :multiplier_presentation].each do |presentation_type|
        define_method presentation_type do
          presentation = self.object.try(presentation_type)

          return nil unless presentation

          LearningDynamicPresentationSerializer.new(presentation)
        end
      end

    end
  end
end
