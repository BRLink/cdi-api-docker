module CDI
  module V1
    class SimpleAchievementSerializer < ActiveModel::Serializer

      attributes :id,
                 :area,
                 :action,
                 :default_score

    end
  end
end

