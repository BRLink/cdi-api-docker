module CDI
  module V1
    class SimpleInstitutionUserSerializer < ActiveModel::Serializer

      root false
      attributes :user_id, :educational_institution_id, :status
    end
  end
end
