module CDI
  module V1
    class GameQuizSerializer < SimpleGameQuizSerializer

      has_many :questions, serializer: GameQuizQuestionSerializer

    end
  end
end
