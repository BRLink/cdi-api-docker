module CDI
  module V1
    class ChallengeDeliverySerializer < ActiveModel::Serializer

      root false

      attributes :id,
                 :cover_image_url,
                 :cover_description,
                 :title,
                 :description,
                 :video_link,
                 :file_url

      has_many :users, serializer: SimpleUserSerializer

    end
  end
end
