module CDI
  module V1
    class StudentClassSerializer < SimpleStudentClassSerializer

      has_one :educational_institution, serializer: SimpleEducationalInstitutionSerializer

      has_one :admin_user, serializer: SimpleUserSerializer

      has_many :students, serializer: SimpleUserSerializer

      attributes :total_students

      STUDENTS_LIMIT = 12

      def students
        if @meta && @meta[:limit_students].present?
          return object.students[0, STUDENTS_LIMIT]
        end

        object.students
      end

      def total_students
        object.students.size
      end

    end
  end
end
