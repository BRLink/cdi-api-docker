module CDI
  module V1
    class MultiplierLearningTrackReviewSerializer < SimpleLearningTrackReviewSerializer

      has_one :learning_track,
              serializer: SimpleLearningTrackSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      has_one :user,
              serializer: SimpleUserSerializer

    end
  end
end
