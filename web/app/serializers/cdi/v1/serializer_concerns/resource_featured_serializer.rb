module CDI
  module V1
    module SerializerConcerns
      module ResourceFeaturedSerializer

        extend ActiveSupport::Concern

        included do
          attributes :total_featured,
                     :featured
        end

        def featured
          scope && scope.resources_featured.where(resource: object).exists?
        end
      end
    end
  end
end
