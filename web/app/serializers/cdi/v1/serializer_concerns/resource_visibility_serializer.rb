module CDI
  module V1
    module SerializerConcerns
      module ResourceVisibilitySerializer

        extend ActiveSupport::Concern

        included do
          has_one :visibility, serializer: SimpleVisibilitySerializer
        end

        def visibility
          object.visibilities.first
        end
      end
    end
  end
end
