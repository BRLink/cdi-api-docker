module CDI
  module V1
    module SerializerConcerns
      module CommentableSerializer

        extend ActiveSupport::Concern

        included do
          attributes :total_comments, :total_nested_comments
          has_many :comments, serializer: EmbedCommentSerializer
        end

        def comments
          object.nested_comments.limit(3)
        end
      end
    end
  end
end
