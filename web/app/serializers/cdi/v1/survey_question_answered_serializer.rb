module CDI
  module V1
    class SurveyQuestionAnsweredSerializer < SimpleSurveyQuestionSerializer

      has_many :answers, serializer: SurveyQuestionAnswerSerializer

      def answers
        object.answers.order('created_at ASC').limit(3)
      end
    end
  end
end
