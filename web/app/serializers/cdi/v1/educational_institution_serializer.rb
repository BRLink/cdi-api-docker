module CDI
  module V1
    class EducationalInstitutionSerializer < SimpleEducationalInstitutionSerializer

      attributes :city_id, :state_id, :relation_with_me, :can_edit, :address

      def address
        object.address.present? ? InstitutionAddressSerializer.new(object.address) : nil
      end
    end
  end
end
