module CDI
  module V1
    class SimpleGameGroupItemSerializer < ActiveModel::Serializer

      root false

      attributes :id, :user_id, :created_at, :updated_at

    end
  end
end
