module CDI
  module V1
    class HomeForStudentSerializer < SimpleHomeSerializer

      has_many :learning_tracks, serializer: LearningTrackHomeSerializer

    end
  end
end
