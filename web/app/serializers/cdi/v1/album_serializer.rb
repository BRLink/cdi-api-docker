module CDI
  module V1
    class AlbumSerializer < SimpleAlbumSerializer

      attributes :total_photos

      # FIXME implementar counter_cache para esta coluna
      def total_photos
        object.photos.count
      end
    end
  end
end
