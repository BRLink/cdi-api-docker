module CDI
  module V1
    class SimpleGameCompleteSentenceSerializer < ActiveModel::Serializer

      root false

      attributes :id, :status, :sentence, :words, :words_length, :status_changed_at,
                 :user_id, :created_at, :updated_at

      def filter(keys)
        if scope.backoffice_profile?
          keys
        else
          if @meta && @meta[:force_return_words].blank?
            keys - [:words]
          else
            keys
          end
        end
      end

    end
  end
end
