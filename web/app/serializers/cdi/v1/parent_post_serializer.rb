module CDI
  module V1

    ## FIXME esta classe esta muito ruim. Precisa colocar nos padrões de SimplePost
    class ParentPostSerializer < ActiveModel::Serializer

      root false

      attributes :id,
                 :created_at,
                 :content

      has_many :categories,
               serializer: SimpleCategorySerializer

      has_one :user,
              serializer: SimpleUserSerializer

      attributes :publisher_type
      has_one :publisher,
              polimorphic: :true,
              prefix: 'Simple'

      has_many :photos, serializer: PhotoSerializer
      has_many :multimedias, serializer: SimpleMultimediaSerializer
      has_many :albums, serializer: PhotosAlbumSerializer
      has_one :challenge, serializer: SimpleChallengeSerializer

      def challenge
        object.challenges.first
      end

    end
  end
end
