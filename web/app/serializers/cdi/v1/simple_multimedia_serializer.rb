module CDI
  module V1
    class SimpleMultimediaSerializer < ActiveModel::Serializer
      root false

      attributes :id, :tags, :user_id, :url, :name,
        :status, :created_at, :updated_at, :file_size, :multimedia_type

      def file_size
        return nil if object.metadata.nil?
        fs = object.metadata['file_size']
        fs.present? ? ActionController::Base.helpers.number_to_human_size(fs) : nil
      end

      def url
        return nil if object.metadata.nil?
        object.metadata['url']
      end
    end
  end
end
