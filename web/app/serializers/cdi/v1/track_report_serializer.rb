module CDI
  module V1
    class TrackReportSerializer < ActiveModel::Serializer

      root :reports

      attributes :report_updated_at

      attributes :enrolled_labels, :enrolled_data
      attributes :review_labels, :review_data

      attributes :students_reports


      ENROLL_LABELS = I18n.t('app.track_enroll_reports_texts')
      REVIEW_LABELS = I18n.t('app.track_review')

      REVIEW_LABELS_INVERTED = LearningTrackReview::REVIEWS_TYPES.invert

      def enrolled_labels
        ENROLL_LABELS
      end

      def review_labels
        REVIEW_LABELS
      end

      def track_report
        student_class_id = @meta.try(:[], :student_class_id)
        @track_report ||= report = LearningTrackReport.where(learning_track_id: object.id, student_class_id: student_class_id).first_or_create
      end

      def enrolled_data
        formatted_json_data(track_report.report_key_for(:enrollment_reports, force_refresh?))
      end

      def review_data
        formatted_json_data(track_report.report_key_for(:reviews_reports, force_refresh?))
      end

      def students_reports
        formatted_json_data(track_report.report_key_for(:students_reports, force_refresh?))
      end

      def report_updated_at
        track_report.updated_at
      end

      def force_refresh?
        @meta[:force_refresh].present?
      end


      protected

      def formatted_json_data(data)
        return data if data.is_a?(Hash)
        JSON.parse data
      end

      #### ============================================================
      #### PAY ATTENTION
      #### REPORTS ARE GENERATED USING POSTGRESQL MATERIALIZED VIEWS
      #### SO DATA BELOW IS ONLY FOR FUTURE REFERENCE
      #### ============================================================


      def enrolled_tracks
        object.enrolled_learning_tracks
      end

      def reviews
        object.reviews
      end

      def total_enrolled_tracks
        @total_enrolled_tracks ||= object.enrolled_learning_tracks.count
      end

      def total_reviews
        @total_reviews ||= reviews.count
      end

      def get_percentage(num, total)
        return 0 if num.to_f.zero? || total.to_f.zero?

        ((num.to_f * 100)/total.to_f).round(2)
      end

      def get_enrolled_percentage(scope)
        raise 'Invalid EnrolledLearningTrack scope' if ::EnrolledLearningTrack::STATUS.keys.exclude?(scope.to_sym)

        get_percentage(enrolled_tracks.send(scope).count, total_enrolled_tracks)
      end

      def get_review_percentage(scope)
        raise 'Invalid LearningTrackReview scope' if ::LearningTrackReview::REVIEWS_TYPES.keys.exclude?(scope.to_sym)

        get_percentage(reviews.send(scope).count, total_reviews)
      end

      def enrolled_data_without_materialized_view
        [
          get_enrolled_percentage(:completed),
          get_enrolled_percentage(:coursing),
          # TODO: Count using all students from class who don't have enrolled_learning_track for current
          get_enrolled_percentage(:enrolled),
        ]
      end

      def review_data_without_materialized_view
        [
          get_review_percentage(:all_new),
          get_review_percentage(:something_new),
          get_review_percentage(:not_new),
        ]
      end

      protected

      #### ============================================================
      #### PAY ATTENTION
      #### MATERIALIZED VIEW WAS NOT THE BETTER SOLUTION FOR
      #### THIS SCENARIO
      #### SO DATA BELOW IS ONLY FOR FUTURE REFERENCE
      #### ============================================================


      def enrolled_data_materialized
        Hash[materialized_enroll_reports.pluck(:status, :total_percent)]
      end

      def materialized_enroll_reports
        @matview_enroll_reports = object.matview_enroll_reports
      end

      def materialized_review_reports
        @matview_review_reports = object.matview_review_reports
      end

      def review_data_materialized
        Hash[materialized_review_reports.pluck(:review_type, :total_percent).map do |data|
          [REVIEW_LABELS_INVERTED[data[0]], data[1]]
        end]
      end

    end
  end
end
