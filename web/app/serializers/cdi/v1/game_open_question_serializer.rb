module CDI
  module V1
    class GameOpenQuestionSerializer < SimpleGameOpenQuestionSerializer

      has_one :user, serializer: SimpleUserSerializer

      has_many :questions, serializer: SimpleGameQuestionSerializer

    end
  end
end
