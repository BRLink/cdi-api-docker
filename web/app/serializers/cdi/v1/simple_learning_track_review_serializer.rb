module CDI
  module V1
    class SimpleLearningTrackReviewSerializer < ActiveModel::Serializer

      root false

      attributes :id, :learning_track_id, :student_class_id, :comment,
                 :user_id, :review_type, :review_type_text, :score,
                 :review_types_texts, :created_at, :updated_at

    end
  end
end
