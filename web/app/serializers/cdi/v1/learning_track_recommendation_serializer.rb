module CDI
  module V1
    class LearningTrackRecommendationSerializer < SimpleLearningTrackSerializer

      attributes :presentation_slides_count

      has_many :categories, serializer: SimpleCategorySerializer
    end
  end
end
