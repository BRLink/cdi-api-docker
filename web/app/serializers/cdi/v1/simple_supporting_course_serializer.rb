module CDI
  module V1
    class SimpleSupportingCourseSerializer < ActiveModel::Serializer
      root false

      attributes :id, :tags, :user_id, :title, :presentation_id,
        :status, :created_at, :updated_at

      has_many :slides


      def slides
        return [] if @meta.blank? || @meta[:include_slides].blank?
        object.slides
      end

    end
  end
end
