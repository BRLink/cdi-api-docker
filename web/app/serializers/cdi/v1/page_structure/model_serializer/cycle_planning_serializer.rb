module CDI
  module V1
    module PageStructure
      class ModelSerializer::CyclePlanningSerializer < ActiveModel::Serializer
        attributes :learning_cycle_id
      end
    end
  end
end