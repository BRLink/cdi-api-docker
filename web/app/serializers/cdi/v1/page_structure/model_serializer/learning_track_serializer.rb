module CDI
  module V1
    module PageStructure
      class ModelSerializer::LearningTrackSerializer < ActiveModel::Serializer
        attributes :name, :tags, :category_id
      end
    end
  end
end