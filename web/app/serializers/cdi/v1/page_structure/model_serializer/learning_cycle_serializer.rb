module CDI
  module V1
    module PageStructure
      class ModelSerializer::LearningCycleSerializer < ActiveModel::Serializer
        attributes :name, :objective_text, :tags, :category_id
      end
    end
  end
end
