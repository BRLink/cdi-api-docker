module CDI
  module V1
    module PageStructure
      class ModelSerializer::QuestionTemplateSerializer < ActiveModel::Serializer
        attributes :id, :title, :subtitle, :question_for,
                   :question_type, :instructions, :choices
      end
    end
  end
end
