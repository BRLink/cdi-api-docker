module CDI
  module V1
    module PageStructure
      class LearningTrackPageSerializer < BaseSerializer

        has_one :learning_track, serializer: ModelSerializer::LearningTrackSerializer

        has_many :questions_templates, serializer: ModelSerializer::QuestionTemplateSerializer

        has_many :categories, serializer: ModelSerializer::CategorySerializer

        has_many :skills, serializer: ModelSerializer::SkillSerializer

      end
    end
  end
end