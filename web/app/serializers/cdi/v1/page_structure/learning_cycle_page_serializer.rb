module CDI
  module V1
    module PageStructure
      class LearningCyclePageSerializer < BaseSerializer

        has_one :learning_cycle, serializer: ModelSerializer::LearningCycleSerializer

        has_many :categories, serializer: ModelSerializer::CategorySerializer

      end
    end
  end
end
