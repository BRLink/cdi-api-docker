module CDI
  module V1
    class SimpleGameItemSerializer < ActiveModel::Serializer

      root false

      attributes :id, :game_related_item_id, :word, :related_word,
                 :has_uploaded_image?, :image_url,
                 :has_uploaded_related_image?, :related_image_url,
                 :created_at, :updated_at

      def filter(keys)
        blacklist_keys = scope.try(:student?) ? [:related_word] : []
        keys - blacklist_keys
      end

    end
  end
end
