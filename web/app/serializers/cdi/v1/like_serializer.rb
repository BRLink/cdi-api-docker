module CDI
  module V1
    class LikeSerializer < SimpleLikeSerializer

      has_one :user,
              serializer: SimpleUserSerializer

    end
  end
end

