module CDI
  module V1
    class StudentClassReportSerializer < ActiveModel::Serializer

      root :reports

      attributes :report_updated_at

      attributes :students_age_range, :students_tracks_enrollments,
                 :finished_tracks_count


      def finished_tracks_count
        object.enrolled_learning_tracks.completed.size
      end

      def students_age_range
        object.formatted_students_age_range
      end

      def students_tracks_enrollments
        object.students_tracks_enrollments_in_interval
      end

      def report_updated_at
        Time.zone.now
      end

    end
  end
end
