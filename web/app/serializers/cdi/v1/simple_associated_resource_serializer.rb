module CDI
  module V1
    class SimpleAssociatedResourceSerializer < ActiveModel::Serializer

      SERIALIZER_OPTIONS = {
        'SupportingCourse' => { meta: { include_slides: true } }
      }

      SERIALIZER_OPTIONS.default = {}

      def attributes
        attrs = {}
        if PresentationSlide::ASSOCIATED_RESOURCE_TYPES.include? object.class.to_s
          klass   = object.class.to_s
          options = { meta: @meta, scope: @scope }.deep_merge(SERIALIZER_OPTIONS[klass])
          attrs = "CDI::V1::Simple#{klass}Serializer".constantize.new(object, options).as_json
        end
        attrs
      end

    end
  end
end
