module CDI
  module V1
    class SimpleLearningCycleSerializer < ActiveModel::Serializer
      root false

      attributes :id, :name, :objective_text, :tags,
                 :status, :status_changed_at,
                 :created_at, :updated_at

      attributes :has_planning


      def has_planning
        object.cycle_planning.present?
      end
    end
  end
end
