module CDI
  module V1
    class PublicUserSerializer < SimpleUserSerializer

      attributes :first_name, :last_name

      attributes :about, :profile_images, :multiplier?

      has_many :photos, serializer: SimplePhotoSerializer

      include CDI::V1::SerializerConcerns::PublisherSerializer
      include CDI::V1::SerializerConcerns::FollowingFollowersSerializer

      attributes :followed_by

      def followed_by
        scope && object.follow?(scope)
      end

      def photos
        object.photos.limit(CDI::Config.user_profile_photos_limit || 20)
      end
    end
  end
end
