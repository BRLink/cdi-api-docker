module CDI
  module V1
    class SupportingCoursePresentationSerializer < SimplePresentationSerializer

      has_one :user, serializer: SimpleUserSerializer

      has_many :slides, serializer: SimplePresentationSlideSerializer

    end
  end
end
