module CDI
  module V1
    class SimpleAddressSerializer < ActiveModel::Serializer

      attributes :city_id, :addressable_id, :addressable_type, :street, :number,
                 :district, :complement, :zipcode, :created_at, :updated_at
    end
  end
end
