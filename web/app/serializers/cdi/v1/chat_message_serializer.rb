module CDI
  module V1
    class ChatMessageSerializer < SimpleChatMessageSerializer

      attributes :user, :has_read

      def user
        CDI::V1::SimpleUserSerializer.new(object.user)
      end

      def has_read
        if viewer_id.present? and viewer_id != object.user.id
          return object.user_has_read?(viewer_id) ? true : false
        else
          return '-'
        end
      end

      def filter(keys)
        if viewer_id.present?
          keys
        else
          keys - [:has_read]
        end
      end

      private
      def viewer_id
        @viewer_id ||= meta_value(:current_user_id)
      end
      def meta_value(key)
        return nil if @meta.blank? || !@meta.is_a?(Hash)
        @meta[key]
      end
    end
  end
end
