module CDI
  module V1
    class StudentClassForStudentSerializer < SimpleStudentClassSerializer

      has_one :admin_user, serializer: SimpleUserSerializer
    end
  end
end
