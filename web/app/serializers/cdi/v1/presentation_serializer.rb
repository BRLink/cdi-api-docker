module CDI
  module V1
    class PresentationSerializer < SimplePresentationSerializer

      has_one :learning_track, serializer: SimpleLearningTrackSerializer

      has_one :user, serializer: SimpleUserSerializer

      has_many :slides, serializer: SimplePresentationSlideSerializer

    end
  end
end
