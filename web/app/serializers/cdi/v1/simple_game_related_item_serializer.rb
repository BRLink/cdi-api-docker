module CDI
  module V1
    class SimpleGameRelatedItemSerializer < ActiveModel::Serializer

      root false

      attributes :id, :status, :status_changed_at, :user_id,
                 :items_count, :created_at, :updated_at

    end
  end
end
