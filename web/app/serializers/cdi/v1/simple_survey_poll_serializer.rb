module CDI
  module V1
    class SimpleSurveyPollSerializer < ActiveModel::Serializer

      root false

      attributes :id,
                 :title,
                 :user_id,
                 :video_link,
                 :image_url,
                 :created_at,
                 :updated_at,
                 :type

      def type
        object.class.to_s['Survey'.length, object.class.to_s.length]
      end
    end
  end
end
