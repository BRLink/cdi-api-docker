module CDI
  module V1
    class InstitutionManagerSerializer < ActiveModel::Serializer

      root false
      attributes :manager_id, :managed_institution_id
    end
  end
end
