module CDI
  module V1
    class GameItemSerializer < SimpleGameItemSerializer

      has_one :game_related_item, serializer: SimpleGameRelatedItemSerializer

    end
  end
end
