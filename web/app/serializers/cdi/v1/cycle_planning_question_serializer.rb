module CDI
  module V1
    class CyclePlanningQuestionSerializer < ActiveModel::Serializer

      root false

      attributes :id, :question_template_id ,:title, :subtitle,
                 :question_type, :instructions, :choices

      has_many :answers,
              serializer: SimpleQuestionUserAnswerSerializer

    end
  end
end

