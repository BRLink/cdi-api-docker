module CDI
  module V1

    # Utilizado para a listagem de comentários filho (com paginação)
    class EmbedNestedCommentSerializer < SimpleNestedCommentSerializer

      has_one :user,
              serializer: SimpleUserSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

    end
  end
end
