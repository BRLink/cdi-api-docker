module CDI
  module V1
    class SimpleChatRoomSerializer < ActiveModel::Serializer

      root false
      attributes :id, :user_id, :name, :room_type
    end
  end
end
