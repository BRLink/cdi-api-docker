module CDI
  module V1
    class ChallengeSerializer < SimpleChallengeSerializer

      attributes :delivered_users_count, :challenge_deliveries_count

      has_many :delivered_users, serializer: SimpleUserSerializer

      include CDI::V1::SerializerConcerns::LikeableSerializer
    end
  end
end
