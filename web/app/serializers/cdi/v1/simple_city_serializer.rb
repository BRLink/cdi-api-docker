module CDI
  module V1
    class SimpleCitySerializer < ActiveModel::Serializer

      root false

      has_one :state, serializer: SimpleStateSerializer

      attributes :id, :name
    end
  end
end
