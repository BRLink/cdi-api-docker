module CDI
  module V1
    class DiscussionAnswerSerializer < SimpleDiscussionAnswerSerializer

      has_one :user,
              serializer: SimpleUserSerializer
    end
  end
end
