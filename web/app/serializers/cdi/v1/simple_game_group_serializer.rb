module CDI
  module V1
    class SimpleGameGroupSerializer < ActiveModel::Serializer

      attributes :id, :game_group_item_id, :title, :items,
                 :created_at, :updated_at


      def items
        scope.try(:backoffice_profile?) ? object.items : []
      end

    end
  end
end
