module CDI
  module V1
    class SimpleHomeSerializer < ActiveModel::Serializer

      root false

      attributes :latest_blog_post,
                 :categories_discussions,
                 :gamefication,
                 :ranking

      has_one :latest_blog_post, serializer: SimpleBlogArticleSerializer

      has_one :challenge, serializer: SimpleChallengeSerializer
    end
  end
end
