module CDI
  module V1
    # Serializer usado na listagem de comentários por resource
    class CommentSerializer < SimpleCommentSerializer

      has_one :user,
              serializer: SimpleUserSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      # Utilizamos simple_nested com propósito de adicionar
      # os usuários como linked_data
      has_many :nested_comments,
               serializer: SimpleNestedCommentSerializer

      has_many :nested_comments_users,
               serializer: SimpleUserSerializer,
               embed: :ids,
               embed_in_root_key: :linked_data,
               embed_in_root: true,
               root: 'users'

      def nested_comments_users
        nested_comments.map(&:user).uniq
      end
    end
  end
end
