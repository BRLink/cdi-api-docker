module CDI
  module V1
    class SimpleCategorySerializer < ActiveModel::Serializer

      root false

      attributes :id, :name

    end
  end
end
