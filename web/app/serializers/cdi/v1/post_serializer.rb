module CDI
  module V1
    class PostSerializer < SimplePostSerializer

      has_many :categories,
               serializer: SimpleCategorySerializer

      has_one :user,
              serializer: SimpleUserSerializer

      attributes :publisher_type
      has_one :publisher,
              polimorphic: :true,
              prefix: 'Simple'
    end
  end
end
