module CDI
  module V1
    class SimpleCommentSerializer < ActiveModel::Serializer

      root false

      attributes :id,
                 :content,
                 :total_children_comments,
                 :created_at,
                 :resource_type,
                 :resource_id

      has_one :user,
              serializer: SimpleUserSerializer

      has_many :nested_comments,
               serializer: NestedCommentSerializer


      include CDI::V1::SerializerConcerns::LikeableSerializer

      def nested_comments
        object.nested_comments.includes(:user).limit(3)
      end

    end
  end
end
