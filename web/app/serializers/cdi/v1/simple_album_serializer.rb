module CDI
  module V1
    class SimpleAlbumSerializer < ActiveModel::Serializer

      root false

      attributes :id,
                 :user_id,
                 :active,
                 :title,
                 :description,
                 :tags,
                 :cover_image_url,
                 :cover_images_url,
                 :created_at,
                 :updated_at,
                 :album_type
    end
  end
end
