module CDI
  module V1
    class GameQuestionAnswerSerializer < SimpleGameQuestionAnswerSerializer

      attributes :correct_options_ids

      has_one :user, serializer: SimpleUserSerializer

      has_one :game, serializer: SimpleGameSerializer

      has_one :game_question

      has_one :option, serializer: SimpleGameQuestionOptionSerializer

      def filter(keys)
        blacklist_keys = object.open_question_answer? ? [:correct_options_ids, :option] : []

        keys - blacklist_keys
      end

      def option
        return nil if object.open_question_answer?

        return object.option
      end

      def game_question
        if [GamePoll].member?(object.game.class)
          serializer = SimpleGameQuestionWithPercentageSerializer
        else
          serializer = SimpleGameQuestionSerializer
        end

        serializer.new(object.game_question, meta: @meta, scope: @scope).serializable_hash
      end

      def correct_options_ids
        question_options.select(&:correct).map(&:id)
      end

      def question_options
        @options ||= object.game_question.options
      end

    end
  end
end
