module CDI
  module V1
    class SimpleSurveyQuestionOptionSerializer < ActiveModel::Serializer

      root false

      attributes :id,
                 :text
    end
  end
end
