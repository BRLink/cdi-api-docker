module CDI
  module V1
    class LearningDynamicSerializer < SimpleLearningDynamicSerializer

      has_many :skills, serializer: SimpleSkillSerializer

      has_one :user, serializer: SimpleUserSerializer

      has_many :categories, serializer: SimpleCategorySerializer

      has_one :student_presentation, serializer: LearningDynamicPresentationSerializer

      has_one :multiplier_presentation, serializer: LearningDynamicPresentationSerializer

      has_one :learning_delivery, serializer: SimpleLearningDeliverySerializer

    end
  end
end
