module CDI
  module ServiceConcerns
    module Cacheable
      def replace_data_for_cache_key(key)
        {}
      end

      def cache_keys_to_expire_after_success
        []
      end

      def matched_cache_keys_to_expire_after_success
        []
      end

      def needs_cache_expiration?
        false
      end

      def clear_cache_for_request?
        true
      end

      def expires_cache?
        needs_cache_expiration? && clear_cache_for_request?
      end

      def expire_cache_for_resource
        if expires_cache?
          expire_simple_cache_keys
          expire_matched_cache_keys
        end
      end

      def user_id_for_cache
        return user.try(:id) if self.respond_to?(:user)

        @user.try(:id)
      end

      def expire_simple_cache_keys
        return nil unless CDI::Cache.respond_to?(:expire!)

        keys = cache_keys_to_expire_after_success || []

        keys.each do |key|
          replace_data = replace_data_for_cache_key(key)

          options = options_for_cache_delete

          CDI::Cache.expire!(key, replace_data, options)
        end
      end

      def expire_matched_cache_keys
        return nil unless CDI::Cache.respond_to?(:expire_matched_keys!)

        keys = matched_cache_keys_to_expire_after_success || []

        CDI::Cache.expire_matched_keys!(keys, options_for_cache_delete)
      end

      def options_for_cache_delete
        options = {}

        if CDI::Config.enabled?(:collect_namespaced_user_memcached_keys)
          options.merge!(user_id: user_id_for_cache, update_saved_keys: true)
        end

        options
      end
    end
  end
end
