# encoding: UTF-8

module CDI
  module V1
    module PresentationSlides

      class CreateService < BaseCreateService


        include V1::ServiceConcerns::PresentationSlideParams

        record_type ::PresentationSlide

        # For some reason `dynamic_presentations` must be
        # limited to one slide
        MAX_SLIDES_FOR_LEARNING_DYNAMIC_PRESENTATION = 2

        private
        def can_create_record?
          unless valid_content_json?
            return bad_request_error!('slides.invalid_content_json')
          end

          unless valid_metadata_json?
            return bad_request_error!('slides.invalid_metadata_json')
          end

          if create_for_challenge? && !user_can_create_presentation_for_challenge?
            return not_found_error!('challenges.user_cant_create')
          end

          if create_for_challenge? && !valid_challenge?
            return not_found_error!('challenges.not_found')
          end

          if create_for_learning_track? && !valid_learning_track?
            return not_found_error!('learning_tracks.not_found')
          end

          if create_for_supporting_course? && !valid_supporting_course?
            return not_found_error!('supporting_courses.not_found')
          end

          unless valid_presentation?
            return not_found_error!('presentations.not_found')
          end

          if create_for_challenge? && !valid_challenge_slide?
            return forbidden_error!('presentations.wrong_slide_type_presentation_for_challenge')
          end

          unless valid_learning_dynamic_slide?
            return forbidden_error!('presentations.presentation_for_dynamic_slide_limit_reached')
          end

          return @user.backoffice_profile?
        end

        def user_can_create_presentation_for_challenge?
          @user.staff?
        end

        def valid_challenge_slide?
          return true if %w(content game).include? @options[:slide][:slide_type]
        end

        def valid_learning_dynamic_slide?
          if presentation.presentation_type == 'dynamic_content'
            return presentation.slides_count < MAX_SLIDES_FOR_LEARNING_DYNAMIC_PRESENTATION
          end

          return true
        end

        def build_record
          presentation.slides.build(presentation_slide_params)
        end

        def presentation
          @presentation ||= if create_for_learning_track?
            learning_track.presence && (learning_track.presentation || learning_track.create_presentation)
          elsif create_for_supporting_course?
            supporting_course.presence && (supporting_course.presentation || supporting_course.make_presentation)
          elsif create_for_challenge?
            challenge.presence && (challenge.presentation || challenge.create_presentation)
          else
            presentation_id = presentation_slide_params[:presentation_id]
            @user.presentations.find_by(id: presentation_id)|| @user.moderate_presentations.find_by(id: presentation_id)
          end
        end

        def challenge
          return nil unless create_for_challenge?
          @challenge ||= Challenge.find_by(id: challenge_id)
        end

        def learning_track
          return nil unless create_for_learning_track?
          @learning_track ||= @user.learning_tracks.find_by(id: learning_track_id)
        end

        def supporting_course
          return nil unless create_for_supporting_course?
          @supporting_course ||= @user.supporting_courses.find_by(id: supporting_course_id)
        end

        def create_for_challenge?
          challenge_id.present?
        end

        def create_for_learning_track?
          learning_track_id.present?
        end

        def create_for_supporting_course?
          supporting_course_id.present?
        end

        def challenge_id
          @options[:slide] && @options[:slide][:challenge_id]
        end

        def learning_track_id
          @options[:slide] && @options[:slide][:learning_track_id]
        end

        def supporting_course_id
          @options[:slide] && @options[:slide][:supporting_course_id]
        end

        def valid_presentation?
          valid_object?(presentation, ::Presentation)
        end

        def valid_challenge?
          valid_object?(challenge, ::Challenge)
        end

        def valid_learning_track?
          valid_object?(learning_track, ::LearningTrack)
        end

        def valid_supporting_course?
          valid_object?(supporting_course, ::SupportingCourse)
        end

        def after_success
          create_thumb_for_slide
        end

        def create_thumb_for_slide
          if @options[:slide] && thumb_data = @options[:slide].delete(:thumb)
            upload_options = {
              upload: {
                file: thumb_data
              }
            }

            begin
              upload_service = ThumbUploadService.new(@record, @user, upload_options)
              upload_service.execute
            rescue Exception => e
              Rollbar.error(e)
            end
          end
        end
      end
    end
  end
end
