# encoding: UTF-8

module CDI
  module V1
    module PresentationSlides
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::PresentationSlideParams

        record_type ::PresentationSlide

        private
        def record_params
          presentation_slide_params
        end

        def user_can_update?
          if presentation_slide_params[:content].present? && !valid_content_json?
            return bad_request_error!('slides.invalid_content_json')
          end

          if presentation_slide_params[:metadata].present? && !valid_metadata_json?
            return bad_request_error!('slides.invalid_metadata_json')
          end

          super
        end

      end

    end
  end
end
