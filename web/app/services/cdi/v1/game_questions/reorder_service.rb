# encoding: UTF-8

module CDI
  module V1
    module GameQuestions

      class ReorderService < BaseReorderService

        record_type ::GameQuestion

        def record_error_key
          :game_questions
        end

      end
    end
  end
end
