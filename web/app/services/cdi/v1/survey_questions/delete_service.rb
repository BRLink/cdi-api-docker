# encoding: UTF-8

module CDI
  module V1
    module SurveyQuestions
      class DeleteService < BaseDeleteService

        record_type ::SurveyQuestion

      end
    end
  end
end
