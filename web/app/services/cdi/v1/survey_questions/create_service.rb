module CDI
  module V1
    module SurveyQuestions
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::SurveyQuestionParams

        record_type ::SurveyQuestion

        def initialize(survey, user, options)
          super user, options
          @survey = survey
        end

        def can_create_record?
          unless valid_survey?
            return forbidden_error('survey_question.survey_not_found')
          end
          if survey_need_options? && !has_options?
            return forbidden_error('survey_question.supply_at_least_one_option')
          end
          user_can_create_question?
        end

        def user_can_create_question?
          @survey.user_id == @user.id
        end

        def survey_need_options?
          survey_poll?
        end

        def has_options?
          attributes_hash[:options] && attributes_hash[:options].any?
        end

        def build_record
          if survey_poll?
            build_survey_poll_question
          else
            @survey.questions.build(survey_question_params)
          end
        end

        def after_success
          create_options if survey_need_options?
          fix_survey_poll_question
        end

        def create_options
          attributes_hash[:options].each {|option| create_option_with_service(option) }
        end

        def fix_survey_poll_question
          if survey_poll?
            @survey.question = @record
          end
        end

        def create_option_with_service(option_hash)
          service = SurveyQuestionOptions::CreateService.new(@record, @user, { option: option_hash })
          service.execute
          service.success?
        end

        def build_survey_poll_question
          question = SurveyQuestion.new(survey_question_params)
          question.questionable = @survey
          question
        end
      end
    end
  end
end
