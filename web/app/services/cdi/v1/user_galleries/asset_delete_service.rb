# encoding: UTF-8

module CDI
  module V1
    module UserGalleries
      class AssetDeleteService < BaseDeleteService

        # TODO: Rename to Asset
        record_type ::Photo

        def record_error_key
          :assets
        end

      end
    end
  end
end
