# encoding: UTF-8

module CDI
  module V1
    module LearningCycles
      class DeleteService < BaseDeleteService

        record_type ::LearningCycle

      end
    end
  end
end
