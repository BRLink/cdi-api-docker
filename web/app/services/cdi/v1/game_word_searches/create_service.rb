# encoding: UTF-8

module CDI
  module V1
    module GameWordSearches
      class CreateService < Games::BaseCreateService

        include V1::ServiceConcerns::GameWordSearchesParams

        record_type ::GameWordSearch

        private
        def base_options_key
          :game
        end

        def game_params
          game_word_search_params
        end

        def game_user_scope
          @user.game_word_searches
        end
      end
    end
  end
end
