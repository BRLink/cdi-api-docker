# encoding: UTF-8

module CDI
  module V1
    module ChatMessages
      class MarkService < BaseActionService

        action_name :chat_message_mark_read

        attr_reader :chat_message_read

        def initialize(message, user, options={})
          @message = message
          @user = user
          super(options)
        end

        def execute_action
          @chat_message_read = @message.chat_message_reads.create(user: @user)
        end

        private

        def success_runned_action?
          @chat_message_read && @chat_message_read.valid?
        end

        def action_errors
          @chat_message_read.errors.full_messages
        end

        def user_can_execute_action?
          @user.chat_messages_to_read.include? @message
        end

        def valid_record?
          valid_object?(@message, ::ChatMessage)
        end

        def after_success
        end
        def record_error_key
          :chat_message_read
        end
      end
    end
  end
end
