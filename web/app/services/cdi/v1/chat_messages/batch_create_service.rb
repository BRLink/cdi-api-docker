# encoding: UTF-8

module CDI
  module V1
    module ChatMessages
      class BatchCreateService < BaseActionService

        action_name :batch_chat_message_create

        attr_reader :chat_messages

        def initialize(user, options={})
          @user = user
          super(options)
        end

        def execute_action
          @chat_messages = targets.map do |target|
            room = get_chat_room(@user, target)
            if room.present?
              @user.chat_messages.create(chat_room: room, content: message_content)
            else
              nil
            end
          end
          return @chat_messages
        end

        private
        def get_chat_room(me, target)
          # as long as me == multiplier, send message to anyone.
          if me.id == target.id
            return nil
          end
          room = me.chat_rooms_joined.joins(:chat_room_users).
                  where(chat_room_users: {user: target})

          room = me.chat_rooms_joined.where(id: ChatRoom.rooms_with_user(target)).first

          if room.blank?
            room = ChatRoom.create(user: me)
            room.members << target
          end
          return room
        end

        def targets
          @targets = User.where(id: target_ids)
        end

        def message_content
          attributes_hash[:content]
        end

        def attributes_hash
          @options[:chat_message]
        end

        def target_ids
          attributes_hash[:user_ids]
        end




        def success_runned_action?
          @chat_messages && @chat_messages.all?(&:valid?)
        end

        def action_errors
          @chat_messages.map(&:errors).map(&:full_messages)
        end

        def user_can_execute_action?
          @user.backoffice_profile?
        end

        def valid_record?
          true
        end

        def after_success
        end
      end
    end
  end
end
