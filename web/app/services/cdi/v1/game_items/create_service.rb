# encoding: UTF-8

module CDI
  module V1
    module GameItems
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::GameItemsParams

        record_type ::GameItem

        attr_reader :game_related_item

        def initialize(game_related_item, user, options = {})
          @game_related_item = game_related_item
          super(user, options)
        end

        private
        def can_create_record?
          return false unless valid_user?

          unless valid_game?
            return not_found_error!('game_related_items.not_found')
          end

          return @user.backoffice_profile?
        end

        def valid_game?
          valid_object?(@game_related_item, ::GameRelatedItem)
        end

        def build_record
          @game_related_item.items.build(game_item_params)
        end

        def after_success
        end
      end
    end
  end
end
