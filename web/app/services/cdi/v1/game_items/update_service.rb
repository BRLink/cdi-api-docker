# encoding: UTF-8

module CDI
  module V1
    module GameItems
    class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::GameItemsParams

        record_type ::GameItem

        def record_params
          record_update_params(game_item_params)
        end

        def user_can_update?
          @record.game_related_item.user_can_update?(@user)
        end

        def whitelist_attributes_to_clear
          [:image, :word]
        end

        def after_success
          if removing_attribute?(:image)
            @record.remove_image!
            @record.save
          end
        end

        def removing_attribute?(attribute)
          attributes_to_clear.member?(attribute.to_sym) && record_params[attribute.to_sym].nil?
        end

      end
    end
  end
end
