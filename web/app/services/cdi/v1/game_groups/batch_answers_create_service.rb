# encoding: UTF-8

module CDI
  module V1
    module GameGroups
      class BatchAnswersCreateService < BaseActionService

        action_name :game_groups_batch_answers_create

        attr_reader :answers, :services

        def initialize(game, user, options = {})
          @game, @user = game, user
          @answers = []
          @services = []

          super(options)
        end

        def execute_action
          answers_hash.each do |group_id, answer_data|
            game_group = find_game_group(group_id)

            if game_group
              # force hash if is string (comma separated)
              answer_data[:words] ||= array_values_from_params(answer_data, :items)

              service = create_answer_with_service(game_group, answer_data.symbolize_keys)

              @answers << service.answer if service.success?
              @services << service
            end
          end
        end

        def valid_answers
          @answers.compact.select(&:persisted?) || []
        end

        private
        # better than one query per group
        def find_game_group(group_id)
          @game_groups ||= @game.groups
          # converting to string cuz group_id can be a Symbol
          @game_groups.find {|group| group.id == group_id.to_s.to_i }
        end

        def create_answer_with_service(game_group, answer_data, options = {})
          options = @options.slice(:origin)
                    .merge(
                      answer: answer_data
                    )
                    .deep_merge(options)


          service = GameWordAnswers::CreateService.new(game_group, @user, options)
          service.execute

          service
        end

        def answers_hash
          attributes_hash
        end

        def success_runned_action?
          valid_answers.any?
        end

        def action_errors
          [@answers.map(&:errors).map(&:full_messages), @services.map(&:errors)].flatten(2).compact.uniq
        end

        def user_can_execute_action?
          return student_user_can_access_game? if @user.student?

          unless valid_answers?
            return bad_request_error!('game_group_items.invalid_answers')
          end

          return @user.backoffice_profile?
        end

        def valid_answers?
          answers_hash.all? do |group_id, answer_data|
            answer_data[:words] ||= array_values_from_params(answer_data, :items)
            answer_data.delete(:items)

            valid_answer?(answer_data)
          end
        end

        def valid_answer?(answer_data)
          return false if answer_data.blank? || !answer_data.is_a?(Hash)

          answer = ::GameWordAnswer.new(answer_data)
          answer.skip_basic_validation = true

          forbidden_errors(answer.errors) unless answer.valid?

          answer.valid? # words/items validation
        end

        def student_user_can_access_game?
          return false unless valid_record?

          @user.enrolled_tracks.joins(:presentation_slides).exists?(presentation_slides: {
            associated_resource_id: @game.id,
            associated_resource_type: @game.class.to_s
          })
        end

        def attributes_hash
          @options[:groups]
        end

        def valid_record?
          valid_object?(@game, ::GameGroupItem)
        end

        def record_error_key
          :game_group_items
        end

        def after_success
          @response_status = 201
        end
      end
    end
  end
end
