# encoding: UTF-8

module CDI
  module V1
    module GameGroups
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::GameGroupParams

        record_type ::GameGroup

        def record_params
          game_group_params
        end
      end

    end
  end
end
