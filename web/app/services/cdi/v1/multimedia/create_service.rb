# encoding: UTF-8

module CDI
  module V1
    module Multimedia
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::MultimediaParams

        record_type ::Multimedia

        private
        def can_create_record?
          return false unless valid_user?

          return @user.backoffice_profile? || social_published?
        end

        def build_record
          @multimedia = @user.multimedia.build(multimedia_params)
        end

      end
    end
  end
end
