# encoding: UTF-8

module CDI
  module V1
    module Users
      class CreateService < BaseCreateService

        record_type ::User

        include V1::ServiceConcerns::UserParams
        include V1::ServiceConcerns::OauthParams

        attr_reader :authorization

        alias :auth_token :authorization

        delegate :id, :username, :profile_image_url, :profile_type, to: :user, prefix: :user
        delegate :token, :provider, :expires_at, to: :authorization, prefix: :user_auth, allow_nil: true

        private
        def can_create_record?
          if multiplier_signup?
            unless valid_educational_institution?
              return not_found_error!('educational_institutions.not_found')
            end
          end

          if student_signup?
            unless valid_student_classes?
              return not_found_error!('student_classes.not_found')
            end

            if using_facebook_data? && invalid_facebook_access_token?
              return bad_request_error!('users.invalid_facebook_access_token')
            end
          end

          return true
        end

        def valid_student_classes?
          student_classes.any? {|_class|
            valid_object?(_class, ::StudentClass)
          }
        end

        def student_classes
          @student_classes ||= ::StudentClass.valid.where(code: classes_codes)
        end

        def build_record
          deleted_user = nil

          # TODO: Only restore user account is not secure
          # needed second step to authenticate user ownership of email account
          if CDI::Config.enabled?(:restore_deleted_account_on_signup)
            deleted_user = User.with_deleted.find_by(email: params[:user][:email])
          end

          if deleted_user
            user = restore_deleted_user(deleted_user)
          else
            user = User.new(new_user_data)
          end

          set_user_attributes(user)

          user
        end

        def new_user_data
          if using_facebook_data?
            user_password = SecureRandom.hex
            password_data = {
              password: user_password,
              password_confirmation: user_password
            }

            user_params.merge!(facebook_data.merge(password_data))
          end

          user_params
        end

        def restore_deleted_user(user)
          user.restore(recursive: true)
          user.update_password(params[:user][:password], true)
          set_user_attributes(user)

          user
        end

        def set_user_attributes(user)
          user.student_classes = student_classes
          set_user_profile_type(user)
        end

        def set_user_profile_type(user)
          user.profile_type ||= :student
        end

        ### Final step
        def after_success
          add_user_to_educational_institutions
          create_authorization
          fetch_avatar_from_provider
          execute_async_actions
          notify_users
        end

        def notify_users
          student_classes.each do |student_class|
            create_system_notification_async(
              sender_user: @record,
              receiver_user: student_class.admin_user,
              notification_type: :new_student_on_class,
              notificable: student_class
            )
          end
        end

        def async?
          return true
          return CDI::Config.enabled?(:update_user_after_signup_async) || @options[:async].eql?(true)
        end

        def add_user_to_educational_institutions
          if multiplier_signup?
            @record.educational_institutions << educational_institution
          end
        end

        def create_authorization
          @authorization ||= @record.authorizations.create(provider: @options[:provider] || DEFAULT_PROVIDER)
        end

        def execute_async_actions
          update_service_options = @options.slice(:origin)

          if async?
            CDI::V1::UserSignupUpdateWorker.perform_async(@record.id, update_service_options)
          else
            service = CDI::V1::Users::PostSignupUpdateService.new(@record, update_service_options)
            service.execute
          end
        end

        def valid_user?
          true
        end
      end
    end
  end
end
