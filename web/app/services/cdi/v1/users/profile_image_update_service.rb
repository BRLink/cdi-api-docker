# encoding: UTF-8

module CDI
  module V1
    module Users
      class ProfileImageUpdateService < Users::UpdateService

        private
        def record_params
          @record_params ||= set_image_crop_params(filter_hash(@options[:user], [:profile_image]))
        end

      end
    end
  end
end
