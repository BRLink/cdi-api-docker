# encoding: UTF-8

module CDI
  module V1
    module Users
      class PostSignupUpdateService < BaseActionService

        action_name :post_signup_update_user

        attr_reader :user

        def initialize(user, options={})
          @user = user
          super(options)
        end

        def user_can_execute_action?
          @user.origin.blank?
        end

        def success_runned_action?
          @user.origin.present?
        end

        def execute_action
          notify_user
          # create_default_albums
          create_user_subscriptions
          create_origin(@user, @options)
        end

        def notify_user
          send_signup_email
        end

        # def create_default_albums
        #   @user.default_gallery_album
        #   @user.send(:create_default_album)
        # end

        def create_user_subscriptions
          if user_is_student?
            take_user_part_of_institutions
            subscribe_user_on_teachers
          end
          subscribe_user_in_institutions
          subscribe_user_itself
        end

        def take_user_part_of_institutions
          @user.educational_institutions << @user.student_classes_institutions
        end

        def subscribe_user_itself
          @user.user_subscriptions.create(publisher: @user)
        end

        def subscribe_user_in_institutions
          @user.institutions_subscribed << @user.educational_institutions
        end

        def subscribe_user_on_teachers
          @user.users_subscribed << @user.classes_teachers
        end

        def send_signup_email
          delivery_async_email(UsersMailer, :welcome, @user)
        end

        def valid_record?
          valid_user?
        end

        def user_is_student?
          # User::VALID_MULTIPLIER_PROFILES_TYPES.values.member?(@user.profile_type)
          User::VALID_PROFILES_TYPES[:student] == @user.profile_type
        end

        def record_error_key
          :users
        end
      end
    end
  end
end
