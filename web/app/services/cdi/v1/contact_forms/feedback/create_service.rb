# encoding: UTF-8

module CDI
  module V1
    module ContactForms
      module Feedback
        class CreateService < ContactForms::BaseCreateService

          record_type ::FeedbackContactForm, alias_name: :feedback_contact

          def contact_type
            :feedback_contact_form
          end

          def record_type_params
            feedback_contact_form_params
          end

        end
      end
    end
  end
end
