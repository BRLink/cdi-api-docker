# encoding: UTF-8

module CDI
  module V1
    module Games
      class BaseUpdateService < CDI::BaseUpdateService

        include V1::ServiceConcerns::GameParams

        def record_params
          game_params
        end

        def user_can_update?
          return false unless @user.backoffice_profile?
          super
        end

      end

    end
  end
end
