# encoding: UTF-8

module CDI
  module V1
    module GameCompleteSentences
      class UpdateService < Games::BaseUpdateService

         include V1::ServiceConcerns::GameCompleteSentencesParams

        record_type ::GameCompleteSentence

        def game_params
          game_complete_sentence_params
        end

        def base_options_key
          :game
        end

      end
    end
  end
end
