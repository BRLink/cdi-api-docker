# encoding: UTF-8

module CDI
  module V1
    module GameCompleteSentences
      class DeleteService < Games::BaseDeleteService

        record_type ::GameCompleteSentence

      end
    end
  end
end
