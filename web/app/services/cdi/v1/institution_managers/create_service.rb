# encoding: UTF-8

module CDI
  module V1
    module InstitutionManagers
      class CreateService < BaseCreateService

        record_type ::InstitutionManager

        include V1::ServiceConcerns::EducationalInstitutionsParams

        private
        def can_create_record?
          return false unless valid_user?

          # institution owner and manager can update
          user_can_edit_educational_institution?
        end

        def build_record
          params_hash  = filter_hash(@options, [:manager_id])

          institution_manager = institution.institution_managers.build(params_hash)

          institution_manager
        end

        def institution
          @institution ||= EducationalInstitution.find_by(id: options[:id])
        end
      end
    end
  end
end
