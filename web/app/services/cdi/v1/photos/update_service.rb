# encoding: UTF-8

module CDI
  module V1
    module Photos
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::PhotoParams

        record_type ::Photo

        def record_params
          update_photo_params
        end
      end
    end
  end
end
