module CDI
  module V1
    module DiscussionOptions
      class CreateService < BaseCreateService

        record_type ::DiscussionOption

        def initialize(discussion, user, options)
          super user, options
          @discussion = discussion
        end

        def can_create_record?
          unless valid_discussion?
            return forbidden_error('discussion.not_found_or_invalid')
          end
          @discussion.user_id == @user.id
        end

        def valid_discussion?
          @discussion.present? && @discussion.poll?
        end

        def build_record
          @discussion.options.build(option_params)
        end

        def attributes_hash
          @options[:option]
        end

        def option_params
          filter_hash(attributes_hash, [:text])
        end
      end
    end
  end
end
