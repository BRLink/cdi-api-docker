module CDI
  module V1
    module ServiceConcerns
      module CyclePlanningParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :learning_cycle_id,
          :questions_answers
        ]

        included do
          def cycle_planning_params
            @cycle_planning_params ||= filter_hash(@options[:cycle_planning], WHITELIST_ATTRIBUTES)
          end
        end

      end
    end
  end
end
