module CDI
  module V1
    module ServiceConcerns
      module StudentClassParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :name
        ]

        included do
          def student_class_params
            @student_class_params ||= filter_hash(attributes_hash, WHITELIST_ATTRIBUTES)
          end

          def attributes_hash
            @options[:class] || @options[:student_class]
          end
        end

      end
    end
  end
end
