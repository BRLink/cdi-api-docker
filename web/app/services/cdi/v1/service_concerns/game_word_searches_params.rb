module CDI
  module V1
    module ServiceConcerns
      module GameWordSearchesParams
        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :words,
        ]

        included do
          def game_word_search_params
            (@game_params ||= {})[:words] ||= array_values_from_params(attributes_hash, :words)

            @game_params
          end
        end
      end
    end
  end
end
