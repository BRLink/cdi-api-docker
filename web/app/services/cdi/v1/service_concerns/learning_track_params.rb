module CDI
  module V1
    module ServiceConcerns
      module LearningTrackParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :name,
          :category_ids,
          :learning_cycle_id,
          :tags,
          :average_score
        ]

        included do
          def learning_track_params
            @learning_track_params ||= filter_hash(@options[:learning_track], WHITELIST_ATTRIBUTES)

            @learning_track_params[:tags]      = tags_from_params
            @learning_track_params[:skill_ids] = skills_from_params
            @learning_track_params[:category_ids] = categories_from_params

            @learning_track_params
          end

          def tags_from_params
            array_values_from_params(@options, :tags, :learning_track)
          end

          def skills_from_params
            array_values_from_params(@options, :skills, :learning_track)
          end

          def categories_from_params
            array_values_from_params(@options, :category_ids, :learning_track)
          end

          # TODO: Create answers async
          def create_or_update_answers_for_questions
            if @options[:learning_track].key?(:questions_answers)
              answers = @options[:learning_track][:questions_answers]
              worker = CDI::V1::LearningTrackQuestionsAnswersCreateWorker

              if CDI::Config.enabled?(:create_template_questionable_records_questions_async)
                worker.perform_async(@record.id, @user.id, answers)
              else
                worker.new.perform(@record, @user, answers)
              end
            end
          end
        end
      end
    end
  end
end
