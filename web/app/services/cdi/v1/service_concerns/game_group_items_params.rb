module CDI
  module V1
    module ServiceConcerns
      module GameGroupItemsParams
        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
        ]

        included do
          def game_group_items_params
            @game_group_items_params ||= filter_hash(attributes_hash, WHITELIST_ATTRIBUTES)
          end

          def groups
            cgroups = attributes_hash && attributes_hash.fetch(:groups, [])

            values = cgroups.is_a?(Hash) ? cgroups.values : cgroups
            values.each {|group| group[:items] = array_values_from_params(group, :items) }

            cgroups
          end

          def has_groups?
            groups.many? # more than 1 valid
          end

          def updating_groups?
            groups.any? {|_, group| valid_group?(group) }
          end

          def valid_group?(group)
            return false if group.blank? || !group.is_a?(Hash)

            game_group = ::GameGroup.new(group)
            game_group.skip_father_game_validation = true

            game_group.valid?
          end
        end
      end
    end
  end
end
