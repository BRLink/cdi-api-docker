module CDI
  module V1
    module ServiceConcerns
      module TrackingEventParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :event_type,
          :presentation_slide_id
        ]

        included do
          def tracking_event_params
            @tracking_event_params ||= filter_hash(@options[:event], WHITELIST_ATTRIBUTES)
          end

          def next_slide_id
            next_slide.try(:id)
          end

          def next_slide
            @next_slide ||= last_slide_for_track.try(:next)
          end

          def enrolled_learning_track
            @enrolled_learning_track ||= @user.enrolled_learning_tracks.find_by(learning_track_id: learning_track_id)
          end

          def valid_enrolled_learning_track?
            valid_object?(enrolled_learning_track, ::EnrolledLearningTrack)
          end

          def learning_track
            @learning_track ||= enrolled_learning_track.try(:learning_track)
          end

          def learning_track_id
            @options[:learning_track_id] || (@options[:event] && @options[:event].fetch(:learning_track_id, nil))
          end

          def presentation_slide_id
            @options[:presentation_slide_id] || (@options[:event] && @options[:event].fetch(:presentation_slide_id, nil))
          end

          def valid_learning_track?
            valid_object?(learning_track, ::LearningTrack)
          end

          def slide
            return nil unless valid_learning_track?

            @slide ||= learning_track.slides.find_by(id: presentation_slide_id)
          end

          def valid_slide_for_track?
            valid_object?(slide, ::PresentationSlide)
          end

          def last_slide_for_track
            return nil unless valid_enrolled_learning_track?
            return nil unless valid_slide_for_track?

            @coursed_event ||= enrolled_learning_track.events.where(enrolled_learning_track_id: enrolled_learning_track.id).last
            @last_slide ||= @coursed_event.try(:presentation_slide)
          end

          def events_by_type
            if enrolled_learning_track
              @events_by_type ||= enrolled_learning_track.events.select(:event_type).group(:event_type).count
            else
              @events_by_type ||= {}
            end

            @events_by_type.default = 0

            @events_by_type
          end

          def migrate_to_complete
            enrolled_learning_track.migrate_to_completed
          end

          def migrate_to_coursing
            enrolled_learning_track.migrate_to_coursing
          end

        end

      end
    end
  end
end
