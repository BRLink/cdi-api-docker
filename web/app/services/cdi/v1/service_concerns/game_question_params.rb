module CDI
  module V1
    module ServiceConcerns
      module GameQuestionParams
        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :title,
          :input_type,
          :allow_multiples_answers
        ]

        VALID_GAMES = [
          ::GameQuiz,
          ::GamePoll,
          ::GameOpenQuestion,
          ::GameTrueFalse
        ]

        included do
          def game_question_params
            @game_question_params ||= filter_hash(attributes_hash, WHITELIST_ATTRIBUTES)
          end

          def attributes_hash
            @options[:question]
          end

          def valid_game?
            VALID_GAMES.any? { |game_class| valid_object?(game, game_class) }
          end

          def game
            return false if game_id.blank?
            @game ||= find_game
          end

          def game_type
            @options[:game_type] || attributes_hash.fetch(:game_type, nil)
          end

          def game_id
            @options[:game_id] || attributes_hash.fetch(:game_id, nil)
          end

          def find_game
            return nil unless [game_type, game_id].all?(&:present?)
            @user.send(game_type.underscore.pluralize).find_by(id: game_id)
          end

          def set_options_data_for_question
            return nil if game_type.blank?

            method_name = "set_options_data_for_question_#{game_type.to_s.underscore}"
            self.try(method_name)
          end

          def set_options_data_for_question_game_true_false
            options_data = ::GameTrueFalse::QUESTION_OPTIONS

            correct = (attributes_hash[:correct] == 'true')

            attributes_hash[:options] ||= []

            [false, true].each do |status|
              attributes_hash[:options].push({
                text: options_data[status][:text],
                correct: (correct = !correct)
              })
            end

            attributes_hash
          end
        end
      end
    end
  end
end
