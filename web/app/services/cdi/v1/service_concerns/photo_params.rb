module CDI
  module V1
    module ServiceConcerns
      module PhotoParams
        extend ActiveSupport::Concern

        UPDATE_WHITELIST_ATTRIBUTES = [
          :caption,
          :tags
        ]

        CREATE_WHITELIST_ATTRIBUTES = [
          :image,
          :album_id
        ] + UPDATE_WHITELIST_ATTRIBUTES

        included do
          def create_photo_params
            @photo_params ||= filter_hash(@options[:photo], CREATE_WHITELIST_ATTRIBUTES)
            @photo_params[:tags] = tags_from_params

            @photo_params
          end

          def update_photo_params
            @photo_params ||= filter_hash(@options[:photo], UPDATE_WHITELIST_ATTRIBUTES)
            @photo_params[:tags] = tags_from_params

            @photo_params
          end

          def tags_from_params
            array_values_from_params(@options, :tags, :photo)
          end
        end
      end
    end
  end
end
