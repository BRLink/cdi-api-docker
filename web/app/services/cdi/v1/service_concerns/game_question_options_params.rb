module CDI
  module V1
    module ServiceConcerns
      module GameQuestionOptionsParams
        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :text,
          :description,
          :correct
        ]

        included do
          def game_question_option_params
            @game_question_option_params = filter_hash(attributes_hash, WHITELIST_ATTRIBUTES)
          end

          def attributes_hash
            @options[:option]
          end
        end
      end
    end
  end
end
