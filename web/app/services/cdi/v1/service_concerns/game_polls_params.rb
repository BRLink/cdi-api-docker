module CDI
  module V1
    module ServiceConcerns
      module GamePollsParams
        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :image
        ]

        included do
          def game_poll_params
            @game_poll_params ||= filter_hash(@options[:game] || @options[:poll], WHITELIST_ATTRIBUTES)
          end
        end
      end
    end
  end
end
