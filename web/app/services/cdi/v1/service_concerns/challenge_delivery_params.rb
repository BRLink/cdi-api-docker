module CDI
  module V1
    module ServiceConcerns
      module ChallengeDeliveryParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :title,
          :description,
          :cover_image,
          :cover_description,
          :user_ids,
          :video_link,
          :file
        ]

        included do

          def challenge_delivery_params
            @challenge_delivery_params ||= filter_hash(attributes_hash, WHITELIST_ATTRIBUTES).tap do |params|
              params[:user_ids] = user_ids
            end
          end

          def attributes_hash
            @options[:delivery]
          end

          def user_ids
            @user_ids ||= array_values_from_params(attributes_hash, :user_ids).push(@user.id).map(&:to_i).uniq
          end

          def file_extension
            @file_extension ||= MIME::Types.type_for(attributes_hash[:file][:filename]).last.extensions.first
          end
        end
      end
    end
  end
end
