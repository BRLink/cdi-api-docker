module CDI
  module V1
    module ServiceConcerns
      module GameParams
        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :title,
          :description,
          :input_type,
          :image
        ]

        included do
          def game_params
            @game_params ||= filter_hash(attributes_hash, WHITELIST_ATTRIBUTES)
          end

          def attributes_hash
            @options[:game] || @options[base_options_key]
          end

        end
      end
    end
  end
end
