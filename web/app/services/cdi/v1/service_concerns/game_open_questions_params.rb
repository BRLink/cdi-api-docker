module CDI
  module V1
    module ServiceConcerns
      module GameOpenQuestionsParams
        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :title,
          :image,
          :video_link
        ]

        included do
          def game_open_question_params
            @game_open_question_params ||= filter_hash(attributes_hash, WHITELIST_ATTRIBUTES)
          end
        end
      end
    end
  end
end
