module CDI
  module V1
    module ServiceConcerns
      module FileUploadParams
        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :file,
          :album_id,
          :caption,
          :tags,
          :visible
        ]

        included do
          def upload_params
            @upload_params ||= filter_hash(@options[:upload], WHITELIST_ATTRIBUTES)
            @upload_params[:tags] = tags_from_params

            @upload_params
          end

          def tags_from_params
            array_values_from_params(@options, :tags, :upload)
          end
        end

      end
    end
  end
end
