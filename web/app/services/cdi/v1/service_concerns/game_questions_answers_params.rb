module CDI
  module V1
    module ServiceConcerns
      module GameQuestionsAnswersParams
        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :option_id,
          :answer_body
        ]

        VALID_GAMES = [
          ::GameQuiz,
          ::GamePoll,
          ::GameOpenQuestion,
          ::GameTrueFalse,
        ]

        included do
          def game_question_answers_params
            @game_question_params ||= normalize_answer_data(filter_hash(attributes_hash, WHITELIST_ATTRIBUTES))
          end

          def normalize_answer_data(data)
            data.delete(:answer_body) if data[:option_id]
            data.delete(:option_id) if data[:answer_body]

            data
          end

          def attributes_hash
            @options[:answer]
          end

          def valid_game?
            VALID_GAMES.any? { |game_class| valid_object?(game, game_class) }
          end

          def valid_game_question?
            valid_object?(game_question, ::GameQuestion)
          end

          def valid_game_question_option?
            return true if game_question_answers_params[:answer_body].present?

            if multiple_answers?
              game_question_options.all? { |option| valid_object?(option, ::GameQuestionOption) }
            else
              valid_object?(game_question_option, ::GameQuestionOption)
            end
          end

          def game
            return @game unless @game.nil?

            @game = (find_game || false)
          end

          def multiple_answers?
            return game_question_options_ids.present?
          end

          def game_question
            @game_question ||= find_question
          end

          def game_question_option
            return nil if game_question.blank?
            @game_question_option ||= game_question.options.find_by(id: game_question_option_id)
          end

          def game_question_options
            return nil if game_question.blank?
            @game_question_options ||= game_question.options.where(id: game_question_options_ids)
          end

          def game_type
            @options[:game_type] || attributes_hash.fetch(:game_type, nil)
          end

          def game_id
            @options[:game_id] || attributes_hash.fetch(:game_id, nil)
          end

          def find_game
            return nil unless [game_type, game_id].all?(&:present?)
            "Game#{game_type.to_s.classify}".constantize.find_by(id: game_id)
          end

          def find_question
            return nil if game.blank? || (game_question_id.blank? && game_type.to_sym == :quiz)
            game_type.to_sym == :poll ? game.question : game.questions.find_by(id: game_question_id)
          end

          def game_question_id
            @options[:question_id] || attributes_hash.fetch(:question_id, nil)
          end

          def game_question_option_id
            @options[:option_id] || attributes_hash.fetch(:option_id, nil)
          end

          def game_question_options_ids
            ids = (@options[:option_ids] || attributes_hash.fetch(:option_ids, []))

            options_ids = { options_ids: ids }
            array_values_from_params(options_ids, :options_ids)
          end
        end
      end
    end
  end
end
