module CDI
  module V1
    module ServiceConcerns
      module AlbumParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :title,
          :description,
          :tags
        ]

        included do

          def album_params
            @album_params ||= filter_hash(@options[:album], WHITELIST_ATTRIBUTES)

            if @options[:album][:tags]
              @album_params[:tags] = tags_from_params
            end

            @album_params[:album_type] = Album::TYPES[:standard]

            @album_params
          end

          def tags_from_params
            array_values_from_params(@options, :tags, :album)
          end

          def default_album_setted?
            @options[:album][:default] == 1
          end
        end

      end
    end
  end
end
