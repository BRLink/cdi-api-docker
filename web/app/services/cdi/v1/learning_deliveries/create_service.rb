# encoding: UTF-8

module CDI
  module V1
    module LearningDeliveries
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::LearningDeliveriesParams

        record_type ::LearningDelivery

        private
        def can_create_record?
          return false unless valid_user?

          unless valid_learning_dynamic?
            return not_found_error!('learning_dynamics.not_found')
          end

          if existent_learning_delivery?
            return forbidden_error!("learning_dynamics.learning_delivery_yet_created")
          end

          return @user.backoffice_profile?
        end

        def build_record
          learning_dynamic.build_learning_delivery(learning_delivery_params)
        end

        def learning_dynamic
          @learning_dynamic ||= @user.learning_dynamics.find_by(id: learning_dynamic_id)
        end

        def learning_dynamic_id
          @options[:delivery] && @options[:delivery][:learning_dynamic_id]
        end

        def valid_learning_dynamic?
          valid_object?(learning_dynamic, ::LearningDynamic)
        end

        def existent_learning_delivery?
          return false unless valid_learning_dynamic?

          learning_dynamic.learning_delivery.present?
        end

      end
    end
  end
end
