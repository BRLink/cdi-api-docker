# encoding: UTF-8

module CDI
  module V1
    module Posts
      class UpdateService < BaseUpdateService

        record_type ::Post

        def record_params
          filter_hash(@options[:post], [:content])
        end
      end
    end
  end
end
