# encoding: UTF-8

module CDI
  module V1
    module Posts
      class ShareService < BaseCreateService

        include V1::ServiceConcerns::PostParams
        include V1::ServiceConcerns::ResourceVisibilityParams

        record_type ::Post

        def initialize post, user, options={}
          super user, options
          @post = post
        end

        def can_create_record?
          if original_post.nil?
            return not_found_error!('posts.resource_not_found')
          end

          unless has_visibilities?
            return forbidden_error!("#{record_error_key}.supply_at_least_one_visibility")
          end

          original_post.user_can_read? @user
        end

        def build_record
          @user.posts.build(post_params)
        end

        def after_success
          create_visibilities
          publish_post_to_subscribers_async
          notify_user
        end

        def original_post
          @post.try(:parent) ? @post.parent : @post
        end

        def post_params
          @post_params ||= filter_hash(attributes_hash, [:content]).tap do |params|
            params[:parent] = original_post
            params[:publisher] = @user
            params[:category_ids] = original_post.category_ids
            params[:visibilities_attributes] = nested_visibilities_attrs
          end
        end

        def notify_user
          unless @user == original_post.user
            create_system_notification_async(
              sender_user: @user,
              receiver_user: original_post.user,
              notification_type: :share_post,
              notificable: original_post
            )
          end
        end
      end
    end
  end
end
