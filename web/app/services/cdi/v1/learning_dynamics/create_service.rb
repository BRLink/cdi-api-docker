# encoding: UTF-8

module CDI
  module V1
    module LearningDynamics
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::LearningDynamicParams

        record_type ::LearningDynamic

        private
        def can_create_record?
          return false unless valid_user?

          return @user.backoffice_profile?
        end

        def build_record
          @learning_dynamic = @user.learning_dynamics.build(learning_dynamic_params)
        end

      end
    end
  end
end
