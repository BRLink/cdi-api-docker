# encoding: UTF-8

module CDI
  module V1
    module LearningDynamics
      class DeleteService < BaseDeleteService
        record_type ::LearningDynamic

        def after_success
          # Is better delete all associations here, dependent: :destroy will perform
          # several UPDATE and DELETE queries in database, better avoid this
          [:student_presentation, :multiplier_presentation].each do |presentation_type|
            presentation = @record.send(presentation_type)
            if presentation
              slides = presentation.slides

              asset_urls = slides.map(&:thumbnail_url)

              CDI::V1::AssetsDeleteWorker.perform_async(asset_urls, visible: false)

              slides.delete_all # don't call dependent destroy

              presentation.destroy
            end
          end
        end
      end
    end
  end
end
