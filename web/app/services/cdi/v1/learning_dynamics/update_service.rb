# encoding: UTF-8

module CDI
  module V1
    module LearningDynamics
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::LearningDynamicParams

        record_type ::LearningDynamic

        def record_params
          learning_dynamic_params.keep_if { |_, v| v.present? }
        end

      end
    end
  end
end
