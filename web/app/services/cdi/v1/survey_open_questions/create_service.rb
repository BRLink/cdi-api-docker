module CDI
  module V1
    module SurveyOpenQuestions
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::SurveyParams
        include V1::ServiceConcerns::SurveyAttachmentParams
        include V1::ServiceConcerns::ResourceVisibilityParams

        record_type ::SurveyOpenQuestion

        def can_create_record?
          unless has_questions?
            return forbidden_error!("#{record_error_key}.supply_at_least_one_question")
          end
          unless has_visibilities?
            return forbidden_error!("#{record_error_key}.supply_at_least_one_visibility")
          end
          return @user.backoffice_profile?
        end

        def duplicate_record?
          false
        end

        def has_questions?
          attributes_hash.fetch(:questions, []).any?
        end

        def build_record
          @user.survey_open_questions.build(survey_open_question_params)
        end

        def after_success
          create_questions
          create_image
          publish_survey_to_subscribers_async
        end

        def create_questions
          attr_hash = attributes_hash[:questions]
          attr_hash.each { |q| create_question_with_service(q) }
        end

        def create_question_with_service(question_hash)
          question_opts = { question: question_hash }
          service = CDI::V1::SurveyQuestions::CreateService.new(@record, @user, question_opts)
          service.execute
          service.success?
        end

        def publish_async?
          CDI::Config.enabled?(:publish_survey_to_subscribers_async)
        end

        def publish_survey_to_subscribers_async
          if publish_async?
            ::CDI::V1::PostSurveyToSubscribersWorker.perform_async(@record.class.to_s, @record.id)
          else
            publish_survey_to_subscribers
          end
        end

        def publish_survey_to_subscribers
          service = ::CDI::V1::Surveys::PostSurveyToSubscribersService.new(@record)
          service.execute
        end
      end
    end
  end
end
