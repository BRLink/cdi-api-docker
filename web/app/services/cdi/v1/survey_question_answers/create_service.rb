module CDI
  module V1
    module SurveyQuestionAnswers
      class CreateService < BaseCreateService

        record_type ::SurveyQuestionAnswer
        alias :answer :survey_question_answer

        def initialize(question, user, options)
          super user, options
          @question = question
        end

        def can_create_record?
          valid_question?
        end

        def valid_question?
          @question.present?
        end

        def build_record
          @question.answers.build(answer_params)
        end

        def attributes_hash
          @options[:answer]
        end

        def answer_params
          return @answer_params if @answer_params.present?
          if @question.open_question?
            @answer_params = filter_hash(attributes_hash, [:answer_body])
          else
            @answer_params = filter_hash(attributes_hash, [:option_id])
          end
          @answer_params[:user] = @user
          @answer_params
        end

        def survey
          @record.survey
        end
      end
    end
  end
end
