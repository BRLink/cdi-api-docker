module CDI
  module V1
    module SurveyQuestionAnswers
      class BatchCreateService < BaseActionService

        action_name :batch_survey_question_answers_options_create

        def initialize(survey, user, options = {})
          @survey, @user = survey, user
          @answers = []
          @services = []

          super(options)
        end

        def execute_action
          answers_hash.each do |question_id, answer_body|
            answer_data = { answer_body: answer_body }
            service = create_answer_with_service(question_id.to_s, answer_data)
            answer = service.answer

            @answers << answer
            @services << service
          end
        end

        def valid_answers
          @answers.compact.select(&:persisted?) || []
        end

        private

        def create_answer_with_service(question_id, answer_data)
          question = @survey.questions.find_by(id: question_id)
          service = CreateService.new(question, @user, {answer: answer_data })
          service.execute
          service
        end

        def answers_hash
          attributes_hash
        end

        def success_runned_action?
          valid_answers.any?
        end

        def action_errors
          model_errors = @answers.compact.map(&:errors).map(&:full_messages).flatten
          service_errors = @services.compact.map(&:errors).flatten

          [model_errors, service_errors].flatten.compact.uniq
        end

        def user_can_execute_action?
          true
        end

        def attributes_hash
          @options[:questions]
        end

        def record_error_key
          :survey_questions_answers
        end

        def after_success
          @response_status = 201
        end

        def valid_record?
          valid_object?(@survey, ::SurveyOpenQuestion)
        end
      end
    end
  end
end
