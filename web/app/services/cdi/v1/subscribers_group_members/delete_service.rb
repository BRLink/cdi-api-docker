module CDI
  module V1
    module SubscribersGroupMembers
      class DeleteService < BaseDeleteService

        record_type ::SubscribersGroupMember

        def initialize group, user, options
          member = group.members.joins(:user_subscription).where("user_subscriptions.user_id" => options[:user_id]).first
          super member, user, options
        end

      end
    end
  end
end
