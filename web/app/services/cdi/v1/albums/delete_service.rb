# encoding: UTF-8

module CDI
  module V1
    module Albums
      class DeleteService < BaseDeleteService

        record_type ::Album
      end
    end
  end
end
