# encoding: UTF-8

module CDI
  module V1
    module Albums
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::AlbumParams

        record_type ::Album

        private
        def build_record
          @user.albums.build(album_params)
        end

        def after_success
          if default_album_setted?
            @user.default_album = @record
          end
        end

        def can_create_record?
          unless can_create_album?
            return forbidden_error!('albums.duplicate_title')
          end

          return true
        end

        def can_create_album?
          !duplicate_record_in_interval?(@user.albums, 30.seconds, title: album_params[:title])
        end
      end
    end
  end
end
