# encoding: UTF-8

module CDI
  module V1
    module Libraries
      class CloneService < BaseCreateService

        def initialize(item, user, options={})
          @item = item
          @user = user
          super(user, options)
        end

        private
          def can_create_record?
            return false unless valid_user?

            return @user.backoffice_profile?
          end

          def build_record
            @item.duplicate(@user)
          end
      end
    end
  end
end
