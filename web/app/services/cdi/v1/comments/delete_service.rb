# encoding: UTF-8

module CDI
  module V1
    module Comments
      class DeleteService < BaseDeleteService

        record_type ::Comment

      end
    end
  end
end
