# encoding: UTF-8

module CDI
  module V1
    module GameRelatedItems
      class DeleteService < Games::BaseDeleteService

        record_type ::GameRelatedItem

      end
    end
  end
end
