# encoding: UTF-8

module CDI
  module V1
    module GameGroupItems
      class DeleteService < Games::BaseDeleteService

        record_type ::GameGroupItem

      end
    end
  end
end
