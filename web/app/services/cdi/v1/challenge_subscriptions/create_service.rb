module CDI
  module V1
    module ChallengeSubscriptions
      class CreateService < BaseCreateService

        record_type ::ChallengeSubscription

        def initialize challenge, user, options={}
          super user, options
          @challenge = challenge
        end

        def build_record
          @user.challenge_subscriptions.build(challenge: @challenge)
        end

        def can_create_record?
          true
        end
      end
    end
  end
end

