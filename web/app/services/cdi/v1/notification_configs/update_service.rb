module CDI
  module V1
    module NotificationConfigs
      class UpdateService < BaseUpdateService

        record_type ::NotificationConfig

        def record_params
          filter_hash(@options, [:notification, :email])
        end

      end
    end
  end
end
