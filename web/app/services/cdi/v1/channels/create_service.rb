# encoding: UTF-8

module CDI
  module V1
    module Channels
      class CreateService < BaseCreateService

        record_type ::Channel

        def initialize student_class_id, learning_track_id, user, options={}
          super user, options
          @student_class  = StudentClass.find_by(id: student_class_id)
          @learning_track = LearningTrack.find_by(id: learning_track_id)
        end

        def can_create_record?
          if @student_class.nil? || @learning_track.nil?
            return not_found_error!("#{record_error_key}.resource_not_found")
          end
          return true
        end

        def build_record
          channel = Channel.find_or_initialize_by(
            student_class_id:  @student_class.id,
            learning_track_id: @learning_track.id
          )

          channel
        end

        def after_success
          notify_users
        end

        def notify_users
          @learning_track.enrolled_users.each do |student|
            create_system_notification_async(
              sender_user: @user,
              receiver_user: student,
              notification_type: :new_channel,
              notificable: @learning_track
            )
          end
        end
      end
    end
  end
end
