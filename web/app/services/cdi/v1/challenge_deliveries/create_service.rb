module CDI
  module V1
    module ChallengeDeliveries
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::ChallengeDeliveryParams

        record_type ::ChallengeDelivery

        def initialize challenge, user, options
          super user, options
          @challenge = challenge
        end

        def can_create_record?
          unless valid_delivery?
            return forbidden_error!("#{record_error_key}.invalid_format_type")
          end

          if delivery_has_been_made?
            return forbidden_error!("#{record_error_key}.delivery_has_been_made")
          end

          unless valid_group_size?
            return forbidden_error!("#{record_error_key}.invalid_group_size")
          end

          return true
        end

        def build_record
          @challenge.challenge_deliveries.build(challenge_delivery_params)
        end

        def after_success
          if need_convert_to_presentation?
            # @presentation = @record.create_presentation user_id: @user.id, file: attributes_hash[:presentation][:file]
            # Shoryuken::Client.queues("#{Rails.env}_converter").send_message({ presentation_id: @presentation.id }.to_json)
          end
        end

        def valid_delivery?
          if @challenge.format_type == 'video'
            return attributes_hash[:video_link].present?
          end

          attributes_hash[:file].present? &&
            @challenge.valid_format_types.include?(file_extension)
        end

        def delivery_has_been_made?
          @challenge.delivered_users.exists?(id: user_ids)
        end

        def need_convert_to_presentation?
          Challenge::VALID_FORMAT_TYPES[:presentation].map(&:to_s).include? file_extension
        end

        def valid_group_size?
          if @challenge.individual?
            return user_ids.size == 1
          end

          user_ids.size > 1 && user_ids.size <= @challenge.group_size
        end
      end
    end
  end
end
