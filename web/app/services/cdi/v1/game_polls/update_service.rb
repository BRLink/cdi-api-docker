# encoding: UTF-8

module CDI
  module V1
    module GamePolls
      class UpdateService < Games::BaseUpdateService

        record_type ::GamePoll

        include V1::ServiceConcerns::GamePollsParams

        def record_params
          game_poll_params
        end

        def base_options_key
          :poll
        end

      end
    end
  end
end
