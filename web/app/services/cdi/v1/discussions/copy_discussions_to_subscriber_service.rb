module CDI
  module V1
    module Discussions
      class CopyDiscussionsToSubscriberService < BaseActionService

        action_name :copy_discussions_to_subscriber

        def initialize(subscription, options={})
          @subscription = subscription
          @user         = subscription.user
          super options
        end

        def execute_action
          @subscription.publisher.published_discussions
            .select(&method(:user_can_read?))
            .each(&method(:create_subscriber_discussion))
        end

        def user_can_read? discussion
          discussion.user_can_read? @user
        end

        def create_subscriber_discussion discussion
          @subscription.subscriber_discussions.create(discussion: discussion)
        end

        def success_runned_action?
          true
        end

        def user_can_execute_action?
          true
        end

        def valid_record?
          valid_object?(@subscription, ::UserSubscription)
        end

        def record_error_key
          :user_subscription
        end
      end
    end
  end
end
