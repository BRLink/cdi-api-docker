# encoding: UTF-8

module CDI
  module V1
    module StudentClasses

      class RemoveStudentService < BaseUpdateService

        record_type ::StudentClass

        attr_reader :removed_students

        def changed_attributes
          return [] if fail?
          [:users]
        end

        private
        def record_params
          @_params ||= {
            user_ids: @options[:user_ids] || @options[:users_ids] || @options[:student_ids]
          }
        end

        def users_ids
          array_values_from_params(record_params, :user_ids)
        end

        def update_record
          @users = User.where(id: users_ids)
          @removed_students = []

          @users.each do |user|
            if user_eligible_for_class?(user)
              if (@record.users -= Array.wrap(user))
                @removed_students << user
              end
            end
          end
        end

        def update_errors
          @users.map do |user|
            { user.id => process_error_message_for_key('users.not_student_of_class', {}) }
          end
        end

        def user_eligible_for_class?(user)
          return false if user.blank? || user.id == @user.id

          user.in_class?(@record)
        end

        def success_updated?
          @removed_students.any?
        end

        def user_can_update?
          return false if @record.status_closed?
          super
        end

      end
    end
  end
end
