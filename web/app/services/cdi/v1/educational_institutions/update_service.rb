# encoding: UTF-8

module CDI
  module V1
    module EducationalInstitutions
      class UpdateService < BaseUpdateService

        record_type ::EducationalInstitution

        include V1::ServiceConcerns::EducationalInstitutionsParams

        private

        def record_params
          educational_institution_params
        end

        def update_record
          params  = record_params
          @address_params = params.delete(:address)

          @record.update(params)
          if @address_params
            if @record.address.present?
              @record.address.update(@address_params)
            else
              @record.create_address(@address_params)
            end
          end
          @record
        end

        # handle avatar, cover images
        def after_success
          update_profile_image if institution_hash.has_key?(:profile_image)
          update_profile_cover_image if institution_hash.has_key?(:profile_cover_image)
          @record.reload
        end
        def update_profile_image
          service = EducationalInstitutions::ProfileImageUpdateService.new(@record, @user, @options)
          service.execute
        end
        def update_profile_cover_image
          service = EducationalInstitutions::ProfileCoverImageUpdateService.new(@record, @user, @options)
          service.execute
        end
        def institution_hash
          @options[:institution] ||  @options[:educational_institution]
        end
      end
    end
  end
end
