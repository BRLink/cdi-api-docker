# encoding: UTF-8

module CDI
  module V1
    module EducationalInstitutions
      class DeleteService < BaseDeleteService

        record_type ::EducationalInstitution

        private


        def after_success
          @record.address.destroy if @record.address.present?
        end

      end
    end
  end
end
