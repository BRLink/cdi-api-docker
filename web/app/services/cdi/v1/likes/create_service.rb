# encoding: UTF-8

module CDI
  module V1
    module Likes
      class CreateService < BaseCreateService

        record_type ::Like

        def initialize resource, user, options={}
          super user, options
          @resource = resource
        end

        def can_create_record?
          if @resource.nil?
            return not_found_error!('likes.resource_not_found')
          end
          if @user.liked? @resource
            forbidden_error!('likes.already_liked')
          end
          true
        end

        def duplicate_record?
          false
        end

        def build_record
          @like = @user.likes.build(resource: @resource)
        end

        def after_success
          notify_users if @resource.respond_to? :user
        end

        def notify_users
          unless @user == @resource.user
            create_system_notification_async(
              sender_user: @user,
              receiver_user: @resource.user,
              notification_type: :"new_like_on_#{@resource.class.to_s.underscore}",
              notificable: @resource
            )
          end
        end

      end
    end
  end
end
