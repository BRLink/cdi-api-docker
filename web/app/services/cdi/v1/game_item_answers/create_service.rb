# encoding: UTF-8

module CDI
  module V1
    module GameItemAnswers
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::GameItemAnswersParams
        include V1::Games::GameProgressTrackMethods

        record_type ::GameItemAnswer, alias_name: :answer

        attr_reader :game_item

        def initialize(game_item, user, options = {})
          @game_item = game_item
          super(user, options)
        end

        private
        def after_success
          register_user_progress_for_track
        end

        def can_create_record?
          return false unless valid_user?

          unless valid_game_item?
            return not_found_error!('game_items.not_found')
          end

          return can_create_answer?
        end

        def can_create_answer?
          return student_user_can_access_game? if @user.student?

          return @user.backoffice_profile?
        end

        def student_user_can_access_game?
          return false unless valid_game_item?

          @user.enrolled_tracks.joins(:presentation_slides).exists?(presentation_slides: {
            associated_resource_id: game_item.game_related_item_id,
            associated_resource_type: 'GameRelatedItem'
          })
        end

        def build_record
          game_item.answers.build(answer_params.merge(user_id: @user.id))
        end

        def save_record
          begin
            @record.save
          rescue PG::UniqueViolation, ActiveRecord::RecordNotUnique => e
            Rollbar.error(e)
            forbidden_error!('game_related_items.yet_answered')
            false
          end
        end

      end
    end
  end
end
