# encoding: UTF-8

module CDI
  module V1
    module GameItemAnswers
      class BatchCreateService < BaseActionService

        action_name :batch_game_item_answers_options_create

        attr_reader :answers, :services

        def initialize(game, user, options = {})
          @game, @user = game, user
          @answers = []
          @services = []

          super(options)
        end

        def execute_action
          answers_hash.each do |item_id, answer_data|
            game_item = find_game_item(item_id)

            if game_item
              service = create_answer_with_service(game_item, answer_data.symbolize_keys)
              answer = service.answer

              @answers << answer
              @services << service
            end
          end
        end

        def valid_answers
          @answers.compact.select(&:persisted?) || []
        end

        private
        # better than one query per item
        def find_game_item(item_id)
          @game_items ||= @game.items
          # converting to string cuz item_id can be a Symbol
          @game_items.find {|item| item.id == item_id.to_s.to_i }
        end

        def create_answer_with_service(game_item, answer_data, options = {})
          answer_data[:reply_word] = answer_data.delete(:related_word) if answer_data.key?(:related_word)

          options = @options.slice(:origin)
                    .merge(
                      answer: answer_data
                    )
                    .deep_merge(options)

          service = GameItemAnswers::CreateService.new(game_item, @user, options)
          service.execute

          service
        end

        def answers_hash
          attributes_hash
        end

        def success_runned_action?
          valid_answers.any?
        end

        def action_errors
          [@answers.map(&:errors).map(&:full_messages), @services.map(&:errors)].flatten(2).compact.uniq
        end

        def user_can_execute_action?
          return student_user_can_access_game? if @user.student?

          return @user.backoffice_profile?
        end

        def student_user_can_access_game?
          return false unless valid_record?

          @user.enrolled_tracks.joins(:presentation_slides).exists?(presentation_slides: {
            associated_resource_id: @game.id,
            associated_resource_type: @game.class.to_s
          })
        end

        def attributes_hash
          @options[:items]
        end

        def valid_record?
          valid_object?(@game, ::GameRelatedItem)
        end

        def record_error_key
          :game_related_items
        end

        def after_success
          @response_status = 201
        end
      end
    end
  end
end
