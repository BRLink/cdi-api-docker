# encoding: UTF-8

module CDI
  module V1
    module LearningTracks
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::LearningTrackParams

        record_type ::LearningTrack

        def record_params
          learning_track_params #.slice!(:learning_cycle_id)
        end

        def after_success
          create_or_update_answers_for_questions
        end

        # def user_can_update?
        #   return false if @record.status_published?
        #   super
        # end

      end
    end
  end
end
