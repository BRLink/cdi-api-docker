# encoding: UTF-8

# module CDI
#   module V1
#     module LearningTracks
#       class MultimediaLinkService < BaseLinkResourceService

#         action_name :link_multimedia

#         def user_can_associated_resource?
#           unless valid_multimedia?
#             return not_found_error!('multimedia.not_found')
#           end

#           return @user.backoffice_profile?
#         end

#         def valid_multimedia?
#           valid_object?(@resource, ::Multimedia)
#         end

#         def record_error_key
#           :multimedias
#         end

#         def slide_type_for_resource
#           PresentationSlide::SLIDE_TYPES[:multimedia]
#         end
#       end
#     end
#   end
# end
