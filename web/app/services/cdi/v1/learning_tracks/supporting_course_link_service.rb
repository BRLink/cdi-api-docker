# encoding: UTF-8

module CDI
  module V1
    module LearningTracks
      class SupportingCourseLinkService < BaseLinkResourceService

        action_name :link_supporting_course

        def user_can_associated_resource?
          unless valid_supporting_course?
            return not_found_error!('supporting_courses.not_found')
          end

          return @user.backoffice_profile?
        end

        def valid_supporting_course?
          valid_object?(@resource, ::SupportingCourse)
        end

        def record_error_key
          :supporting_courses
        end

        def slide_type_for_resource
          PresentationSlide::SLIDE_TYPES[:course]
        end

        def slide_metadata_for_resource(resource = nil)
          {
            thumbnail_url: resource.try(:thumbnail_url)
          }
        end
      end
    end
  end
end
