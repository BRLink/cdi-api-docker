# encoding: UTF-8

module CDI
  module V1
    module LearningTracks
      class PublishService < BaseUpdateService

        record_type ::LearningTrack

        attr_reader :classes_published, :classes_errors

        def changed_attributes
          return [] if fail?
          [:status]
        end

        private

          def record_params
            {}
          end

          def can_execute_action?
            can_execute = super
            if can_execute
              unless @record.slides.count > 0
                return forbidden_error!("#{record_error_key}.cant_publish_empty")
              end
            end

            can_execute
          end

          def classes_ids
            classes_ids = array_values_from_params(@options, :classes_ids)

            classes_ids.map(&:to_i).delete_if {|n| n.zero? }
          end

          def student_classes
            # only self classes
            @student_classes ||= @user.administered_student_classes.where(id: classes_ids)
          end

          def update_record
            publish_in_classes
            # will migrate to publish in any way
            migrate_to_published

            @record
          end

          def migrate_to_published
            @record.migrate_to_published
          end

          def publish_in_classes
            @classes_published ||= []
            @classes_errors    ||= []

            student_classes.each do |student_class|
              if publish_in_class(student_class)
                @classes_published << student_class
              else
                if valid?
                  forbidden_error('learning_tracks.yet_published_in_class')
                end

                @classes_errors << student_class
              end
            end
          end

          def publish_in_classes?
            classes_ids.any?
          end

          def publish_in_class(student_class)
            return false if @record.published_in_class?(student_class)

            begin
              @record.student_classes << student_class
              student_class
            rescue PG::UniqueViolation, ActiveRecord::RecordNotUnique => e
              false
            end
          end

          def update_errors
            @errors
          end

          def success_updated?
            return true if @record.status_published? && !publish_in_classes?

            return (publish_in_classes? && @classes_published.any?)
          end

          def after_success
            #sync cycle status
            @record.learning_cycle.refresh_status
            notify_users
          end

          def notify_users
            @record.students.each do |student|
              create_system_notification_async(
                sender_user: @record.user,
                receiver_user: student,
                notification_type: :new_track,
                notificable: @record
              )
            end
          end

      end
    end
  end
end
