# encoding: UTF-8

module CDI
  module V1
    module LearningTracks

      class RemoveAuthorService < BaseUpdateService

        record_type ::LearningTrack

        attr_reader :removed_moderators

        def changed_attributes
          return [] if fail?
          [:moderators]
        end

        private
        def record_params
          @_params ||= { moderator_ids: @options.delete(:users_ids) || @options.delete(:moderator_ids) }
        end

        def update_record
          @users = User.where(id: record_params[:moderator_ids])
          @removed_moderators = []

          @users.each do |user|
            if user_eligible_for_moderation_revoke?(user)
              if user.remove_track_moderation @record
                @removed_moderators << user
              end
            end
          end
        end

        def update_errors
          @users.map do |user|
            { user.id => process_error_message_for_key('users.has_not_moderator_role', {}) }
          end
        end

        def user_eligible_for_moderation_revoke?(user)
          return false if user.blank? || user.id == @user.id
          user.moderator_of_track? @record
        end

        def success_updated?
          @removed_moderators.any?
        end
      end
    end
  end
end
