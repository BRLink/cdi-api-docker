# encoding: UTF-8

module CDI
  module V1
    module LearningTracks
      class AddAuthorService < BaseUpdateService

        record_type ::LearningTrack

        attr_reader :added_moderators

        def changed_attributes
          return [] if fail?
          [:moderators]
        end

        private
        def record_params
          @_params ||= { moderator_ids: @options.delete(:users_ids) || @options.delete(:moderator_ids) }
        end

        def update_record
          @users = User.where(id: record_params[:moderator_ids])
          @added_moderators = []

          @users.each do |user|
            if user_eligible_for_moderation?(user)
              if user.add_as_track_moderator @record
                @added_moderators << user
              end
            end
          end
        end

        def update_errors
          @users.map do |user|
            { user.id => process_error_message_for_key('users.not_eligible_for_moderation', {}) }
          end
        end

        def user_eligible_for_moderation?(user)
          return false if user.blank? || user.id == @user.id

          user.backoffice_profile? && !(user.moderator_of_track? @record)
        end

        def success_updated?
          @added_moderators.any?
        end

        def after_success
          notify_users
        end

        def notify_users
          @added_moderators.each do |moderator|
            create_system_notification_async(
              sender_user: @user,
              receiver_user: moderator,
              notification_type: :added_as_track_author,
              notificable: @record
            )
          end
        end
      end
    end
  end
end
