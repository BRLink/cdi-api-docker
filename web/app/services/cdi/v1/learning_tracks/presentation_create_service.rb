# encoding: UTF-8

module CDI
  module V1
    module LearningTracks
      class PresentationCreateService < BaseCreateService

        include V1::ServiceConcerns::PresentationParams

        record_type ::Presentation

        private
        def can_create_record?
          unless valid_learning_track?
            return not_found_error!('learning_tracks.not_found')
          end

          if existent_presentation?
            return forbidden_error!('learning_tracks.presentation_yet_created')
          end

          return @user.backoffice_profile?
        end

        def build_record
          learning_track.build_presentation(presentation_params)
        end

        def learning_track
          return nil if learning_track_id.blank?
          @learning_track ||= @user.learning_tracks.find_by(id: learning_track_id)
        end

        def learning_track_id
          @options[:learning_track_id] || @options[:presentation][:learning_track_id]
        end

        def valid_learning_track?
          valid_object?(learning_track, ::LearningTrack)
        end

        def existent_presentation?
          return false unless valid_learning_track?

          learning_track.presentation.present?
        end
      end

    end
  end
end
