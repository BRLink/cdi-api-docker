# encoding: UTF-8

module CDI
  module V1
    module LearningTracks

      class RemoveFromStudentClassService < BaseUpdateService

        record_type ::LearningTrack

        attr_reader :student_classes
        attr_reader :removed_student_classes

        def changed_attributes
          return [] if fail?
          [:student_classes]
        end

        private
        def record_params
          @_params ||= { student_class_ids: classes_ids }
        end

        def classes_ids
          array_values_from_params(@options, :classes_ids) ||
            array_values_from_params(@options, :student_classes_ids)
        end

        def update_record
          @student_classes = @user.administered_student_classes.where(id: classes_ids)
          @removed_student_classes = []

          @student_classes.each do |student_class|
            if class_eligible_for_track_remotion?(student_class)
              if @record.student_classes.delete(student_class)
                @removed_student_classes << student_class
              end
            end
          end

          @record
        end

        def update_errors
          @student_classes.map do |s_class|
            { s_class.id => process_error_message_for_key('student_classes.not_found', {}) }
          end
        end

        def class_eligible_for_track_remotion?(student_class)
          return false if student_class.blank?

          student_class.learning_tracks.exists?(id: @record.id)
        end

        def success_updated?
          @removed_student_classes.any?
        end

        def after_success
          if @record.student_classes.count.zero?
            @record.migrate_to_draft
          end
        end

      end
    end
  end
end
