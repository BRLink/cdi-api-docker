# encoding: UTF-8

module CDI
  module V1
    module GameQuestionOptions
      class BatchCreateService < BaseActionService

        action_name :batch_game_question_options_create

        OPTION_VALID_KEYS = [:text, :correct].sort.freeze

        WHITELIST_ATTRIBUTES = [
          :text,
          :description,
          :correct
        ].freeze

        def initialize(game_question, user, options={})
          @game_question = game_question
          @user = user
          super(options)
        end

        def execute_action
          @game_options = @game_question.options.create(game_question_option_params)
        end

        private

        def success_runned_action?
          @game_options && @game_options.all?(&:valid?)
        end

        def action_errors
          @game_options.map(&:errors).map(&:full_messages)
        end

        def user_can_execute_action?
          return false unless game_question_option_params.any?

          @game_question.user_can_manage?(@user)
        end

        def set_defaults
          attributes_hash.each do |option|
            option[:correct] ||= false
          end

          attributes_hash
        end

        def game_question_option_params
          return @game_question_option_params if @game_question_option_params.present?

          set_defaults

          @game_question_option_params = @rejected_options = []

          sanitezed_attributes = attributes_hash.map {|option| filter_hash(option, WHITELIST_ATTRIBUTES) }

          sanitezed_attributes.each do |option|
            if valid_option_object?(option)
              @game_question_option_params << option
            else
              @rejected_options << option
            end
          end

          @game_question_option_params
        end

        def valid_option_object?(option)
          return false if option.blank? || !option.is_a?(Hash)

          valid_keys = option.keys.map(&:to_sym).sort == OPTION_VALID_KEYS

          return valid_keys && option.values.map(&:to_s).all?
        end

        def attributes_hash
          @options[:options]
        end

        def valid_record?
          valid_object?(@game_question, ::GameQuestion)
        end

        def record_error_key
          :game_questions
        end

        def after_success
        end
      end
    end
  end
end
