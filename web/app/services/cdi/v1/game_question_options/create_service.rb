# encoding: UTF-8

module CDI
  module V1
    module GameQuestionOptions
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::GameQuestionOptionsParams

        record_type ::GameQuestionOption

        attr_reader :game_question

        def initialize(game_question, user, options={})
          @game_question = game_question
          super(user, options)
        end

        private
        def can_create_record?
          return false unless valid_user?

          unless valid_question?
            return not_found_error!('game_questions.not_found')
          end

          return @user.backoffice_profile?
        end

        def valid_question?
          valid_object?(@game_question, ::GameQuestion)
        end

        # def validate_ip_on_create?
        #   true
        # end

        def build_record
          @game_question.options.build(game_question_option_params)
        end

        def after_success
        end
      end
    end
  end
end
