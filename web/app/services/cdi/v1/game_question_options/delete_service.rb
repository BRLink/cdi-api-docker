# encoding: UTF-8

module CDI
  module V1
    module GameQuestionOptions
      class DeleteService < BaseDeleteService

        record_type ::GameQuestionOption

        def after_success
        end

        def user_can_delete?
          @record.question.questionable.user_can_delete?(@user)
        end

        def game_question
          @record.try(:question)
        end

      end
    end
  end
end
