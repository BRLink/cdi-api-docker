# encoding: UTF-8

module API
  module Helpers
    module V1
      module PostsHelpers

        extend Grape::API::Helpers

        params :new_post do
          requires :post, type: Hash do
            requires :content, type: String
            requires :category_ids
            requires :visibility_type, type: String
            optional :visibility_id, type: Integer
            optional :photos, type: Array
            optional :multimedias, type: Array
            optional :albums, type: Array
          end
        end

        def paginated_posts posts, options={}
          if params[:category]
            posts = posts.joins(:categories).where({categories: { id: params[:category].to_i }})
          end

          if options.fetch(:featured, false)
            posts = posts.only_featured
          end

          if options.fetch(:me, false)
            posts = posts.only_user(current_user)
          end

          ret = paginate(posts)
          paginated_serialized_array(ret, { root: :posts, serializer: :simple_post })
        end
      end
    end
  end
end
