# encoding: UTF-8

module API
  module Helpers
    module V1
      module EducationalInstitutionsHelpers

        extend Grape::API::Helpers

        def serialized_educational_institution(educational_institution, options = {})
          options = { serializer: :educational_institution }.merge(options)
          serialized_object(educational_institution, options)
        end

        def serialized_educational_institutions(educational_institutions, options={})
          options = { serializer: :educational_institution, root: :educational_institutions }.merge(options)
          serialized_array(educational_institutions, options)
        end

        def paginated_serialized_educational_institutions(educational_institutions, options = {})
          options = {
            serializer: :simple_educational_institution,
            root: :educational_institutions
          }.merge(options)

          collection = paginate(educational_institutions).includes(address: [:city])

          paginated_serialized_array(collection, options)
        end

        def paginated_serialized_users(users, options = {})
          options = {
            serializer: :simple_user,
            root: :institution_members
          }.merge(options)

          collection = paginate(users)

          paginated_serialized_array(collection, options)
        end

        # TODO: Refactor method name
        def paginated_scoped_serialized_educational_institutions(user, scope = :owned_educational_institutions, options = {})
          valid_scopes = [:owned_educational_institutions, :educational_institutions]

          raise "Invalid scope" unless valid_scopes.member?(scope.to_sym)

          paginated_serialized_educational_institutions(user.send(scope), options)
        end

        def paginated_serialized_educational_institutions_list(educational_institutions, options={})
            options = {
              serializer: :simple_educational_institution,
              root: :educational_institutions,
              paginate: true
            }.merge(options)
            paginated_serialized_array(educational_institutions,options)
        end

      end
    end
  end
end
