# encoding: UTF-8

module API
  module Helpers
    module V1
      module StatesHelpers

        extend Grape::API::Helpers
        include EducationalInstitutionsHelpers

        def states_as_json(states)
          serialized_array(states, serializer: :state)
        end

        def state_as_json(state, meta = {})
          meta_data = { meta: { cities: true }.merge(meta) }
          options   = { serializer: :state }.merge(meta_data)
          serialized_object(state, options)
        end

        def state_info_as_json(state_id, options = {})
          state = State.joins(:cities).find_by(id: state_id)

          if state
            return state_as_json(state, options.delete(:meta) || {})
          else
            not_found_error_response(:states)
          end
        end

        def state_educational_institutions_as_json(state_id, options = {})
          state = State.joins(:cities).find_by(id: state_id)
          options = { serializer: :simple_educational_institution }.merge(options)

          if state
            educational_institutions = state.educational_institutions.includes(:city)
            serialized_educational_institutions(educational_institutions, options)
          else
            not_found_error_response(:states)
          end
        end
      end
    end
  end
end
