# encoding: UTF-8

module API
  module Helpers
    module V1
      module LearningCyclesHelpers

        extend Grape::API::Helpers

        def serialized_learning_cycle(learning_cycle, options = {})
          options = { serializer: :learning_cycle }.merge(options)
          serialized_object(learning_cycle, options)
        end

        def serialized_cycle_planning(cycle_planning, options = {})
          options = { serializer: :cycle_planning }.merge(options)
          serialized_object(cycle_planning, options)
        end

        def paginated_serialized_learning_cycles(learning_cycles, options = {})
          options = {
            serializer: :simple_learning_cycle,
            root: :learning_cycles,
            meta: {
              include_cover_image_url: false
            }
          }.deep_merge(options)

          includes = include_options_for_learning_cycles(options)

          collection = paginate(learning_cycles).includes(includes)

          paginated_serialized_array(collection, options)
        end

        private
        def include_options_for_learning_cycles(options = {})
          includes = { categories: [], learning_tracks: [], cycle_planning: [] }
          meta, each_meta = options[:meta],options[:each_serializer_meta]

          options[:includes] ||= {}

          if meta && meta[:include_cover_image_url].present?
            # this avoids a huge N+1
            tracks_options = options[:includes][:learning_tracks] ||= {}

            tracks_options.is_a?(Hash) ? tracks_options.merge!(presentation_slides: []) : tracks_options.push(:presentation_slides)
          end

          if each_meta && each_meta[:only_published_learning_tracks]
            options[:includes][:published_learning_tracks] = (options[:includes].delete(:learning_tracks) || [])
            options[:includes][:published_learning_tracks].push(:learning_cycle)

            includes.delete(:learning_tracks)
          end

          options[:includes].reverse_merge(includes)
        end

      end
    end
  end
end
