# encoding: UTF-8

module API
  module Helpers
    module V1
      module ApplicationHelpers

        extend Grape::API::Helpers

        # This alias must be used in all paginated endpoints
        params :pagination do
          optional :page, type: Integer
          optional :per_page, type: Integer, values: ALLOWED_PAGINATION_PER_PAGE
        end

        def simple_response_for_service(service)
          status service.response_status

          if service.success?
            response = success_response_for_service(service)
          else
            response = error_response_for_service(service)
          end

          response
        end

        def success_response_for_service(service)
          {
            success: true,
            status_code: service.response_status
          }
        end

        def error_response_for_service(service)
          {
            error: true,
            status_code: service.response_status,
            errors: service.errors
          }
        end

        def response_for_service(service, response_append = {}, force_merge = false)
          response = simple_response_for_service(service)
          response.merge!(response_append) if force_merge || service.success?

          response
        end

        def response_for_update_service(service, record_type, options = {})
          cache_key = options.fetch(:cache_key, :nil)
          cache_replace_data = options.fetch(:cache_replace_data, nil)

          if cache_key.present? && cache_replace_data.present?
            cache_options = {
              replace_data: cache_replace_data
            }

            serialized_object = CDI::Cache.fetch(cache_key, cache_options) do
              serialized_object_from_service(service, record_type, options.merge(include_root: false)).as_json
            end

            # if record data won't changed, try to use the representation who is
            # still in cache, to avoid a new serialization process with same results
            response_root = options.fetch(:root, record_type).to_sym

            unless (serialized_object.key?(response_root) && serialized_object[response_root].is_a?(Hash))
              serialized_object = Hash[response_root, serialized_object]
            end
          else
            serialized_object = serialized_object_from_service(service, record_type, options)
          end

          update_response = {
            updated: service.changed?,
            changed_attributes: service.changed_attributes
          }

          update_response.merge!(serialized_object)

          response_for_service(service, update_response)
        end

        def response_for_delete_service(service, record_type, options = {})
          service_response = serialized_object_from_service(service, record_type, options)
          response_for_service(service, service_response)
        end

        def response_for_create_service(service, record_type, options = {})
          service_response = {}

          if service.success?
            service_response = serialized_object_from_service(service, record_type, options)
          end

          response_for_service(service, service_response)
        end

        def serialized_object_from_service(service, record_type, options = {})
          record = service.send(options.fetch(:service_record_method, record_type))

          serializer    = options.fetch(:serializer, record_type)
          response_root = options.fetch(:root, record_type.to_sym)
          meta_options  = options.fetch(:meta, nil)

          serialized_object_hash = serialized_object(record, serializer: serializer, meta: meta_options).as_json

          return serialized_object_hash if options[:include_root] == false

          Hash[response_root => serialized_object_hash]
        end

        def not_found_error_response(message_key = nil)
          error_response(404, 'not_found', message_key)
        end

        def forbidden_error_response(message_key = nil)
          error_response(403, 'cant_access', message_key)
        end

        def bad_request_error_response(message_key = nil, type = 'invalid')
          error_response(400, type, message_key)
        end

        def error_response(status_code, type, message_key)
          raise 'Invalid message_key for message error' if message_key.blank?

          response = {
            error: true,
            status_code: status_code,
            errors: [
              I18n.t("cdi.errors.#{message_key}.#{type}")
            ]
          }

          error!(response, status_code)
        end

        def generic_error_response(status = 400, errors = {})
          status status

          {
            status_code: status,
            error: true,
            errors: errors
          }
        end

        def generic_success_response(status = 200, response = {})
          status status

          response.merge({
            success: true,
            status_code: status
          })
        end

        def only_backoffice_allowed!
          unless current_user.backoffice_profile?
            return forbidden_error_response(:student_deliveries)
          end
        end

        def categories_ids_from_params
          array_values_from_params(params, :categories_ids)
        end

      end
    end
  end
end
