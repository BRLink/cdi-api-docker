# encoding: UTF-8

module API
  module Helpers
    module V1
      module MultimediaHelpers

        extend Grape::API::Helpers

        params :new_multimedia do
          requires :multimedia, type: Hash do
            optional :file, :type => Rack::Multipart::UploadedFile
            optional :link, :type => String
            mutually_exclusive :file, :link
            optional :name, :type => String
            requires :category_ids
            optional :tags
            optional :social, type: Integer, values: [1]
          end
        end

        params :update_multimedia do
          requires :multimedia, type: Hash do
            optional :file, :type => Rack::Multipart::UploadedFile
            optional :link, :type => String
            mutually_exclusive :file, :link
            optional :name, :type => String
            optional :category_ids
            optional :tags
          end
        end

        def paginated_serialized_multimedia(multimedia, options = {})
          options = {
            serializer: :simple_multimedia,
            root: :multimedia
          }.merge(options)
          collection = paginate(multimedia)
          paginated_serialized_array(collection, options)
        end

        def serialized_multimedia(multimedia, options = {})
          options = { serializer: :multimedia }.merge(options)
          serialized_object(multimedia, options)
        end

        def only_one_asset_allowed_response
          status 400
          {
            success: false,
            status_code: 404,
            errors: 'only one asset is allowed'
          }
        end
      end
    end
  end
end
