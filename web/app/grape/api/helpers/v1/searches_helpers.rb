# encoding: UTF-8

module API
  module Helpers
    module V1
      module SearchesHelpers

        extend Grape::API::Helpers

        include EducationalInstitutionsHelpers
        include LearningDynamicsHelpers
        include LearningTracksHelpers

        def profile_types_for_search(params)
          profile_types = []
          valid_profiles_types = User::VALID_PROFILES_TYPES.values

          if params[:profile_types].present?
            profile_types = params[:profile_types]
                            .split(',')
                            .map(&:squish)
                            .map(&:parameterize)

          elsif profile_type = params[:profile_type]
            if profile_type.to_s.downcase == 'multiplier'
              profile_types = User::VALID_MULTIPLIER_PROFILES_TYPES.values
            else
              profile_types = Array.wrap(params[:profile_type].try(:parameterize))
            end
          end

          profile_types.keep_if {|p_t| valid_profiles_types.member?(p_t) }
        end

        def cache_data_for_user_search(profile_types, params)
          replace_data = [
            params[:name].parameterize,
            (profile_types.compact.presence || ['all']).join('_'),
            params[:page],
            params[:per_page]
          ].compact
        end
      end
    end
  end
end
