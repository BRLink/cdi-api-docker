# encoding: UTF-8

module API
  module Helpers
    module V1
      module PresentationsHelpers

        extend Grape::API::Helpers

        def serialized_presentation(presentation, options = {})
          options = { serializer: :presentation }.merge(options)
          serialized_object(presentation, options)
        end

        def paginated_serialized_presentations(presentations, options = {})
          options = {
            serializer: :simple_presentation,
            root: :presentations
          }.merge(options)

          collection = paginate(presentations).includes(:learning_track)

          paginated_serialized_array(collection, options)
        end

      end
    end
  end
end
