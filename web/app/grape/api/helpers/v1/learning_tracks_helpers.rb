# encoding: UTF-8

module API
  module Helpers
    module V1
      module LearningTracksHelpers

        include LearningCyclesHelpers

        extend Grape::API::Helpers

        def handle_learning_track_response(learning_track, user)
          if learning_track
            learning_track_access_response(learning_track, user)
          else
            not_found_error_response(:learning_tracks)
          end
        end

        def learning_track_access_response(learning_track, user)
          if learning_track.user_can_read?(user)
            learning_track
          else
            forbidden_error_response(:learning_tracks)
          end
        end

        def find_learning_track_for_current_user(query = nil)
          query ||= { id: params[:id] }
          current_user.learning_tracks.find_by(query) || current_user.moderate_learning_tracks.find_by(query)
        end

        def find_student_class_for_current_user(query = {})
          current_user.student_classes.find_by(query) || current_user.administered_student_classes.find_by(query)
        end

        def serialized_learning_track(learning_track, options = {})
          options = { serializer: :learning_track }.merge(options)
          serialized_object(learning_track, options)
        end

        def serialized_learning_track_authors(learning_track, options = {})
          authors = learning_track.authors

          options = {
            serializer: :simple_user,
            root: :authors
          }.merge(options)

          collection = paginate(authors)

          paginated_serialized_array(collection, options)
        end

        def response_for_track_moderators(learning_track, options = {})
          response = serialized_learning_track_authors(learning_track).as_json
          meta = response.delete(:meta)
          response.merge!(owner: serialized_object(learning_track.user, serializer: :simple_user), meta: meta)
        end

        def paginated_serialized_learning_tracks(learning_tracks, options = {})
          options = {
            serializer: :simple_learning_track,
            root: :learning_tracks
          }.merge(options)


          collection = paginate_tracks_and_includes(learning_tracks)

          paginated_serialized_array(collection, options)
        end

        def paginate_tracks_and_includes(learning_tracks)
          paginate(learning_tracks).includes(categories: [],
                                             skills: [],
                                             learning_cycle: [:cycle_planning],
                                             user: [],
                                             presentation_slides: []
                                            )
        end

        def paginated_serialized_enrolled_tracks(tracks, options = {})
          options = {
            serializer: :student_learning_track,
            root: :learning_tracks,
            each_serializer_meta: {
              include_cover_image_url: false
            }
          }.merge(options)

          # collection = paginated_serialized_learning_tracks(tracks, options)

          collection = paginate_tracks_and_includes(tracks)

          # max 30 records
          collection_ids = collection.map(&:id)

          paginated_tracks = paginated_serialized_array(collection, options).as_json

          merge_enroll_in_response(current_user, collection_ids, paginated_tracks).as_json
        end

        def merge_enroll_in_response(user, collection_ids, response)
          enroll_status_response = enroll_status_for(user, collection_ids)

          if response[:linked_data]
            response[:linked_data].merge!(enroll_status_response)
          else
            response.merge!(enroll_status_response)
          end

          response
        end

        def enroll_status_for(user, collection_ids)
          user_tracks_status = user.enrolled_learning_tracks
                                   .real_enrolled
                                   .where(learning_track_id: collection_ids)

          enrolled_track_status  = user_tracks_status.as_json(only: [:learning_track_id, :student_class_id, :status, :status_changed_at])

          response = { enrollment_status: enrolled_track_status }
        end

        def simple_enrollment_status_for(user, id)
          status = enroll_status_for(user, id)[:enrollment_status]
          response = {
            enrollment_status: status.empty? ? "not_enrolled" : status[0]["status"]
          }
          response
        end

        def learning_tracks_by_cycles_feed_response(cycles, merge_enroll_status = true, options = {})
          options = {
            serializer: :user_feed_learning_cycle,
            meta: {
              include_cover_image_url: true
            },
            includes: {
              user: [],
              categories: [],
              cycle_planning: [],
              student_classes: [],
              learning_tracks: [:student_classes, :categories]
            }
          }.deep_merge(options)

          response = paginated_serialized_learning_cycles(cycles, options).as_json

          return response unless merge_enroll_status

          linked_tracks  = ((response[:linked_data] && response[:linked_data]['learning_tracks']) || [])
          collection_ids = linked_tracks.map {|l| l[:id] }

          merge_enroll_in_response(current_user, collection_ids, response)
        end

        # TODO: Refactor method name
        def paginated_scoped_serialized_learning_tracks(user, scope = :learning_tracks, options = {})
          valid_scopes = [:learning_tracks, :moderate_learning_tracks, :enrolled_tracks, :enrolled_learning_tracks]

          raise "Invalid scope" unless valid_scopes.member?(scope.to_sym)

          options = { serializer: :user_learning_track }.merge(options)

          if options[:conditions].present?
            tracks = user.send(scope).where(options[:conditions])
          else
            tracks = user.send(scope)
          end

          paginated_serialized_learning_tracks(tracks, options)
        end

        def service_moderation_users(service, method, root = nil)
          service.send(method).as_json(only: [:id, :user, :profile_type], methods: [:fullname], root: root)
        end

        def service_classes_published(service, method, root = nil)
          service.send(method).as_json(only: [:id, :name, :admin_user_id, :status], root: root)
        end

      end
    end
  end
end
