# encoding: UTF-8

module API
  module Helpers
    module V1
      module CitiesHelpers

        extend Grape::API::Helpers

        include EducationalInstitutionsHelpers

        def serialized_city(city, options = {})
          options = { serializer: :city }.merge(options)
          serialized_object(city, options)
        end

        def city_educational_institutions_as_json(city_id)
          city = City.joins(:state).find_by(id: city_id)

          if city
            serialized_educational_institutions(city.educational_institutions)
          else
            not_found_error_response(:cities)
          end
        end

      end
    end
  end
end
