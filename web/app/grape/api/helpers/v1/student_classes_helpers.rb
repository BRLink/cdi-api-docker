# encoding: UTF-8

module API
  module Helpers
    module V1
      module StudentClassesHelpers

        extend Grape::API::Helpers

        def serialized_student_class(student_class, options = {})
          options = { serializer: :student_class }.merge(options)
          serialized_object(student_class, options)
        end

        def paginated_serialized_student_classes(student_classes, options = {})
          options = {
            serializer: :simple_student_class,
            root: :classes,
            paginate: true
          }.merge(options)

          paginated_serialized_array(student_classes, options)
        end

        def service_students(service, method, root = nil)
          service.send(method).as_json(
            only: [:id, :user, :profile_type],
            methods: [:fullname, :profile_images],
            root: root
          )
        end

      end
    end
  end
end
