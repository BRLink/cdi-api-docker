# encoding: UTF-8

module API
  module Helpers
    module V1
      module LearningDeliveriesHelpers

        extend Grape::API::Helpers

        def serialized_learning_delivery(learning_delivery, options = {})
          options = { serializer: :learning_delivery }.merge(options)
          serialized_object(learning_delivery, options)
        end

        def learning_delivery_for_track(user, query = nil)
          learning_dynamic = user.learning_dynamics.find_by(query)

          delivery = learning_dynamic && learning_dynamic.learning_delivery
        end

      end
    end
  end
end
