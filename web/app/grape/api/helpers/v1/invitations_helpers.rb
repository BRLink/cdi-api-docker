# encoding: UTF-8

module API
  module Helpers
    module V1
      module InvitationsHelpers

        extend Grape::API::Helpers

        def valid_invitation_response invitation
          {
            success: true,
            user_data: {
              email: invitation.email,
              profile_type:  invitation.profile_type,
              first_name: invitation.first_name,
              last_name: invitation.last_name
            }
          }
        end

      end
    end
  end
end
