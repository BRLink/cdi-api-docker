# encoding: UTF-8

module API
  module Helpers
    module V1
      module OthersHelpers

        extend Grape::API::Helpers

        POSTMON_ENDPOINT = 'http://api.postmon.com.br/v1/cep/%s'

        def address_attributes_from_zipcode(zipcode)
          address_attributes = zipcode_data(zipcode)

          if address_attributes.any?
            city_name = address_attributes[:cidade]
            city_uf   = address_attributes[:estado]

            query = {
              city_name: city_name.downcase,
              city_uf: city_uf
            }

            city = ::City.joins(:state)
                         .where("lower(cities.name) = :city_name AND states.acronym = :city_uf", query).last

            if city
              address_attributes[:city] = ::CDI::V1::SimpleCitySerializer.new(city).serializable_hash
            end
          end

          address_attributes
        end

        # merge this hash with address params
        def address_hash_from_zipcode(zipcode)
          address_attributes = zipcode_data(zipcode)
          address_hash={}
          if address_attributes.any?
            city = city_from_address_attributes address_attributes
            address_hash = {
              street: address_attributes[:logradouro],
              district: address_attributes[:bairro],
              city_id: city.id,
              zipcode: address_attributes[:cep]
            }
            address_hash.keep_if { |_, v| v.present? }
          end
          address_hash
        end

        def zipcode_data(zipcode)
          zipcode = zipcode.to_s.parameterize.gsub(/\s?|\-/, '')
          data = nil
          cep_endpoint = POSTMON_ENDPOINT % zipcode

          begin
            zipcode_data = open(cep_endpoint)
            data = JSON.parse(zipcode_data.read).symbolize_keys rescue nil
          rescue => e
            data = {}
          end

          data
        end

        def city_from_address_attributes address_attributes
          city_name = address_attributes[:cidade]
            city_uf   = address_attributes[:estado]

            query = {
              city_name: city_name.downcase,
              city_uf: city_uf
            }

          ::City.joins(:state)
              .where("lower(cities.name) = :city_name AND states.acronym = :city_uf", query).last
        end

      end
    end
  end
end
