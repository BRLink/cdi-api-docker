# encoding: UTF-8

module API
  module Helpers
    module V1
      module PhotosHelpers

        extend Grape::API::Helpers

        def serialized_photo(user, options={})
          options = { serializer: :photo }.merge(options)
          serialized_object(user, options)
        end
      end
    end
  end
end
