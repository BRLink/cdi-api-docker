# encoding: UTF-8

module API
  module V1
    class Others < API::V1::Base

      helpers API::Helpers::V1::OthersHelpers

      before do
        authenticate_user
      end

      with_cacheable_endpoints :cep do

        desc "Return cep data"

        route_param :cep do
          get do
            respond_with_cacheable('cep.show', params[:cep]) do
              data = address_attributes_from_zipcode(params[:cep])

              return not_found_error_response(:cep) if data.blank?

              data
            end
          end
        end
      end
    end
  end
end
