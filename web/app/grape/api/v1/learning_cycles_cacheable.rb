# encoding: UTF-8

module API
  module V1
    class LearningCyclesCacheable < API::V1::Base

      helpers API::Helpers::V1::LearningCyclesHelpers

      before do
        authenticate_user
      end

      with_cacheable_endpoints :learning_cycles do
        route_param :id do
          # FIXME: respond_with_cacheable
          get do
            learning_cycle = LearningCycle.find_by(id: params[:id])

            if learning_cycle
              if learning_cycle.user_can_read?(current_user)
                serialized_learning_cycle(learning_cycle)
              else
                forbidden_error_response(:learning_cycles)
              end
            else
              not_found_error_response(:learning_cycles)
            end
          end

          [:cycle_planning, :planning].each do |endpoint|
            get endpoint do
              learning_cycle = LearningCycle.find_by(id: params[:id])

              if learning_cycle
                if learning_cycle.user_can_read?(current_user)
                  planning = learning_cycle.cycle_planning

                  if planning
                    planning = ::CyclePlanning
                                 .includes(questions: [:template_questionable, :question_template, :answers], answers: nil)
                                 .find(planning.try(:id))

                    serialized_cycle_planning(planning)
                  else
                    not_found_error_response(:cycle_plannings)
                  end
                else
                  forbidden_error_response(:learning_cycles)
                end
              else
                not_found_error_response(:learning_cycles)
              end
            end
          end
        end
      end
    end
  end
end
