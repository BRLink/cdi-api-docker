# encoding: UTF-8

module API
  module V1
    class EducationalInstitutionsTakePartActions < API::V1::Base

      before do
        authenticate_user
      end

      namespace :educational_institutions do

        namespace :take_part do
          [:all, :accepted, :pending, :rejected].each do |status|
            desc "list current user's #{status} institution membership requests"
            get status do
              institution_users = (status == :accepted) ?
                      current_user.institution_users :
                      current_user.send("#{status}_institution_users")
              serialized_array(institution_users, serializer: :simple_institution_user)
            end
          end
        end

        route_param :id do
          namespace :take_part do

            desc "request to be a member of an institution"
            post do
              service = execute_service('InstitutionUsers::CreateService', current_user, params)
              response_for_create_service(service, :institution_user, serializer: :simple_institution_user)
            end

            desc "leave the institution"
            delete do
              institution = EducationalInstitution.find_by(id: params[:id])
              return not_found_error_response(:educational_institution) if institution.blank?
              institution_user = institution.all_institution_users.find_by(user_id: current_user.id)
              service = execute_service('InstitutionUsers::DeleteService', institution_user, current_user, params)
              response_for_delete_service(service, :institution_user, serializer: :simple_institution_user)
            end
            desc "get relation with an institution"
            get do
              institution = EducationalInstitution.find_by(id: params[:id])
              return not_found_error_response(:educational_institution) if institution.blank?
              institution_user = institution.all_institution_users.find_by(user_id: current_user.id)
              return not_found_error_response(:institution_user) if institution_user.blank?
              serialized_object(institution_user, serializer: :simple_institution_user)
            end
          end

          namespace :take_part_manage do
            [:all, :accepted, :pending, :rejected].each do |status|
              desc "list #{status} member requests of an institution"
              get status do
                institution = EducationalInstitution.find_by(id: params[:id])
                return not_found_error_response(:educational_institution) if institution.blank?
                if institution.all_managers.include? current_user
                  institution_users = (status == :accepted) ?
                          institution.institution_users.includes(:user) :
                          institution.send("#{status}_institution_users").includes(:user)
                  serialized_array(institution_users,serializer: :institution_user)
                else
                  return forbidden_error_response(:institution_user)
                end
              end
            end
            route_param :user_id do

              desc "institution admin handles request"
              params do
                requires :status , values: ['accepted', 'rejected', :accepted, :rejected]
              end
              put do
                institution = EducationalInstitution.find_by(id: params[:id])
                return not_found_error_response(:educational_institution) if institution.blank?
                institution_user = institution.all_institution_users.find_by(user_id: params[:user_id])
                service = execute_service('InstitutionUsers::UpdateService', institution_user, current_user, params)
                response_for_update_service(service, :institution_user, serializer: :simple_institution_user)
              end
            end
          end
        end
      end
    end
  end
end
