# encoding: UTF-8

module API
  module V1
    class EducationalInstitutionsByUsers < API::V1::Base

      helpers API::Helpers::V1::EducationalInstitutionsHelpers

      before do
        authenticate_user
      end
      namespace :me do
        namespace :educational_institutions do
          paginated_endpoint do
            desc "institutions current user can edit"
            get :managed do
              response_for_paginated_endpoint 'me.educational_institutions.managed', 'managed' do
                educational_institutions = current_user.owned_educational_institutions + current_user.managed_institutions
                options = {
                  serializer: :educational_institution
                }
                paginated_serialized_educational_institutions_list(educational_institutions, options)
              end
            end
          end
          paginated_endpoint do
            desc "institutions current_user is a member of"
            get :membered do
              response_for_paginated_endpoint 'me.educational_institutions.membered', 'membered' do
                educational_institutions = current_user.educational_institutions
                options = {
                  serializer: :educational_institution
                }
                paginated_serialized_educational_institutions(educational_institutions, options)
              end
            end
          end

          paginated_endpoint do
            desc "institutions current user is related to"
            get :related do
              response_for_paginated_endpoint 'me.educational_institutions.related', 'reladed' do
                educational_institutions = (current_user.owned_educational_institutions +
                          current_user.managed_institutions +
                          current_user.educational_institutions).uniq
                options = {
                  serializer: :educational_institution
                }
                paginated_serialized_educational_institutions_list(educational_institutions, options)
              end
            end
          end

          paginated_endpoint do
            desc "institutions current student have classes in"
            get :study_in do
              response_for_paginated_endpoint 'me.educational_institutions.student_related', 'student_related' do
                educational_institutions = current_user.student_classes_institutions.distinct
                options = {
                  serializer: :educational_institution
                }
                paginated_serialized_educational_institutions_list(educational_institutions, options)
              end
            end
          end

        end
      end
    end
  end
end
