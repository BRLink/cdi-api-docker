# encoding: UTF-8

module API
  module V1
    module PageStructure
      class Base < API::V1::Base

        helpers Helpers::V1::PagesStructuresHelpers

        PAGE_STRUCTURES_ENDPOINTS = [
          :learning_track,
          :learning_cycle,
          :cycle_planning
        ]

        with_cacheable_endpoints :page_structures do

          PAGE_STRUCTURES_ENDPOINTS.each do |page_name|
            get page_name do
              cached_page_structure_response(page_name)
            end
          end

        end
      end
    end
  end
end
