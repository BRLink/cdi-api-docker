# encoding: UTF-8

module API
  module V1
    class Cities < API::V1::Base

      helpers API::Helpers::V1::CitiesHelpers

      with_cacheable_endpoints :city do

        desc "Return all educational_institutions of given city"

        route_param :id do
          get do
            respond_with_cacheable('cities.show', params[:id]) do
              city = City.includes(:state).find_by(id: params[:id])
              if city
                options = {
                  serializer: :simple_city
                }
                serialized_city(city, options).as_json
              else
                not_found_error_response(:cities)
              end
            end
          end

          get :educational_institutions do
            respond_with_cacheable('cities.educational_institutions', params[:id]) do
              city_educational_institutions_as_json(params[:id]).as_json
            end
          end
        end

      end
    end
  end
end
