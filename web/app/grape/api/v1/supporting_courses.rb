# encoding: UTF-8

module API
  module V1
    class SupportingCourses < API::V1::Base

      helpers API::Helpers::V1::SupportingCoursesHelpers
      helpers API::Helpers::V1::SharedParamsHelpers
      helpers API::Helpers::V1::SharedServiceActionsHelpers
      before do
        authenticate_user
      end


      namespace :supporting_courses do

        desc 'Create supporting course'
        params do
          use :new_supporting_course
        end

        post do
          service = execute_service('SupportingCourses::CreateService', current_user, params)
          response_for_create_service(service, :supporting_course)
        end

        route_param :id do
          params do
            use :update_supporting_course
          end
          desc "update supporting course"
          put do
            supporting_course = current_user.supporting_courses.find(params[:id])

            service = execute_service("SupportingCourses::UpdateService", supporting_course, current_user, params)
            response_for_update_service(service,:supporting_course)
          end

          desc "delete supporting course"
          delete do
            supporting_course = current_user.supporting_courses.find(params[:id])

            service = execute_service("SupportingCourses::DeleteService", supporting_course, current_user, params)
            response_for_delete_service(service,:supporting_course)
          end
        end
      end
    end
  end
end
