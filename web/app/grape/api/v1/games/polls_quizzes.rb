# encoding: UTF-8

module API
  module V1
    module Games
      class PollsQuizzes < API::V1::Base

        helpers API::Helpers::V1::GamesHelpers

        before do
          authenticate_user
        end

        namespace :games do
          [:quiz, :poll].each do |game_type|
            namespace game_type do
              desc "Create a new game #{game_type} for user"
              params do
                optional game_type, type: Hash do
                  optional :title
                  optional :description
                  optional :image, type: Rack::Multipart::UploadedFile
                end
              end

              post do
                service = execute_service(service_name_for_game(game_type, 'Create'), current_user, params)
                response_for_create_service(service, :"game_#{game_type}")
              end

              route_param :id do
                desc "Updates owned game #{game_type}"
                put do
                  game = find_game(current_user, params[:id], game_type)

                  service = execute_service(service_name_for_game(game_type, 'Update'), game, current_user, params)
                  response_for_update_service(service, :"game_#{game_type}")
                end

                desc "Delete owned user game #{game_type}"
                delete  do
                  game = find_game(current_user, params[:id], game_type)

                  service = execute_service(service_name_for_game(game_type, 'Delete'), game, current_user, params)
                  response_for_delete_service(service, :"game_#{game_type}")
                end
              end
            end
          end
        end
      end
    end
  end
end
