# encoding: UTF-8

module API
  module V1
    module Games
      class CompleteSentences < API::V1::Base

        helpers API::Helpers::V1::GamesHelpers

        before do
          authenticate_user
        end

        namespace :games do
          namespace :complete_sentence do
            desc "Create a new GameCompleteSentence for user"
            params do
              requires :game, type: Hash do
                requires :sentence
                requires :words #, type: Array
              end
            end

            desc 'Create a new CompleteSentence Game'
            post do
              service = execute_service('GameCompleteSentences::CreateService', current_user, params)
              response_for_create_service(service, :game_complete_sentence)
            end

            route_param :id do
              desc "Updates owned game GameCompleteSentence"
              put do
                game = find_game(current_user, params[:id], :complete_sentence)

                service = execute_service('GameCompleteSentences::UpdateService', game, current_user, params)
                response_for_update_service(service, :game_complete_sentence)
              end

              desc "Delete owned user GameCompleteSentence"
              delete  do
                game = find_game(current_user, params[:id], :complete_sentence)

                service = execute_service('GameCompleteSentences::DeleteService', game, current_user, params)
                response_for_delete_service(service, :game_complete_sentence)
              end

              desc 'Answer an game as Student'
              params do
                requires :words
              end
              post :answers do
                game = find_game(current_user, params[:id], :complete_sentence)

                service = execute_service('GameWordAnswers::CreateService', game, current_user, params)
                response_for_create_service(service, :answer, serializer: :game_word_answer)
              end
            end
          end
        end
      end
    end
  end
end
