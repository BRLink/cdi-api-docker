# encoding: UTF-8

module API
  module V1
    module Games
      class GameQuestionsAnswers < API::V1::Base

        helpers API::Helpers::V1::GamesHelpers

        before do
          authenticate_user
        end

        VALID_GAME_ENDPOINTS = [
          :quiz,
          :poll,
          :open_questions,
          :true_false
        ]

        namespace :games do

          VALID_GAME_ENDPOINTS.each do |game_type|
            namespace game_type do
              namespace_name = [:poll].exclude?(game_type) ? :questions : :question
              route_param :game_id do
                namespace namespace_name do

                  endpoint_block = -> {
                    params do
                      requires :answer, type: Hash do
                        mutually_exclusive :option_id, :option_ids

                        mutually_exclusive :option_id, :answer_body
                        mutually_exclusive :option_ids, :answer_body

                        optional :answer_body
                        optional :option_ids
                        optional :option_id, type: Integer
                      end
                    end

                    namespace :answers do
                      desc "Answer #{game_type.to_s.classify} game"
                      post do
                        params[:answer].merge!(game_id: params[:game_id], game_type: game_type)

                        service = execute_service('GameQuestionsAnswers::CreateService', current_user, params)

                        options = { serializer: :game_question_answer }

                        if service.answer.is_a?(GameQuestionAnswer)
                          response_for_create_service(service, :answer, options)
                        else
                          response_for_batch_answer_service(service, options)
                        end
                      end
                    end
                  }

                  if [:poll].exclude?(game_type)
                    route_param :question_id do
                      endpoint_block.call
                    end
                  else
                    endpoint_block.call
                  end

                end
              end
            end
          end
        end
      end
    end
  end
end
