module API
  module V1
    module Games
      class GamesCacheable < API::V1::Base

        helpers API::Helpers::V1::GamesHelpers

        before do
          authenticate_user
        end

        VALID_GAMES_ENDPOINTS = [
          :quiz,
          :poll,
          :complete_sentence,
          :word_search,
          :open_question,
          :true_false,
          :related_item,
          :group_item
        ]

        with_cacheable_endpoints :games do
          VALID_GAMES_ENDPOINTS.each do |game_type|
            endpoint_block = -> {
              route_param :id do
                desc "Get Game#{game_type.to_s.classify} data"
                get do
                  game_scope = "game_#{game_type.to_s.pluralize}"
                  game = find_game(current_user, params[:id], game_type, params)

                  if game
                    game_response_for_user(current_user, game, params)
                  else
                    not_found_error_response(game_scope)
                  end
                end
              end
            }

            # singular
            namespace game_type do
              endpoint_block.call
            end

            # plural
            namespace game_type.to_s.pluralize do
              endpoint_block.call
            end

          end
        end
      end
    end
  end
end
