# encoding: UTF-8

module API
  module V1
    module Games
      class RelatedItems < API::V1::Base

        helpers API::Helpers::V1::GamesHelpers

        before do
          authenticate_user
        end

        namespace :games do
          namespace :related_items do
            desc "Create a new game GameRelatedItem for user"
            params do
              requires :game, type: Hash do
                requires :columns, type: Array
              end
            end

            post do
              service = execute_service('GameRelatedItems::CreateService', current_user, params)
              response_for_create_service(service, :game_related_item)
            end

            route_param :id do
              desc "Updates owned game GameRelatedItem"
              put do
                game = find_game(current_user, params[:id], :related_item)

                service = execute_service('GameRelatedItems::UpdateService', game, current_user, params)
                response_for_update_service(service, :game_related_item)
              end

              desc "Delete owned user game GameRelatedItem"
              delete  do
                game = find_game(current_user, params[:id], :related_item)

                service = execute_service('GameRelatedItems::DeleteService', game, current_user, params)
                response_for_delete_service(service, :game_related_item)
              end
            end
          end
        end
      end
    end
  end
end
