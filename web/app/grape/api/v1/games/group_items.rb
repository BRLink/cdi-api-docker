# encoding: UTF-8

module API
  module V1
    module Games
      class GroupItems < API::V1::Base

        helpers API::Helpers::V1::GamesHelpers

        before do
          authenticate_user
        end

        namespace :games do
          namespace :group_items do
            desc "Create a new game GameGroupItem for user"
            params do
              requires :game, type: Hash do
                requires :groups, type: Array
              end
            end
            post do
              service = execute_service('GameGroupItems::CreateService', current_user, params)
              response_for_create_service(service, :game_group_item)
            end

            route_param :id do
              desc "Updates owned game GameGroupItem"
              put do
                game = find_game(current_user, params[:id], :group_item, false)

                service = execute_service('GameGroupItems::UpdateService', game, current_user, params)
                response_for_update_service(service, :game_group_item)
              end

              desc "Delete owned user game GameGroupItem"
              delete  do
                game = find_game(current_user, params[:id], :group_item, false)

                service = execute_service('GameGroupItems::DeleteService', game, current_user, params)
                response_for_delete_service(service, :game_group_item)
              end

              desc 'Answer items in batch as Student'
              params do
                requires :groups, type: Hash
              end
              post :answers do
                game = find_game(current_user, params[:id], :group_item)

                service = execute_service('GameGroups::BatchAnswersCreateService', game, current_user, params)

                success_answers = serialized_array(service.valid_answers, serializer: :game_group_answer).as_json

                response_for_service(service, answers: success_answers)
              end

              namespace :groups do
                desc 'Add a new GameGroup(with items) to GameGroupItem'
                params do
                  requires :group, type: Hash do
                    requires :items, type: Array
                  end
                end
                post do
                  game = find_game(current_user, params[:id], :group_item, false)

                  service = execute_service('GameGroups::CreateService', game, current_user, params)
                  response_for_create_service(service, :game_group, serializer: :simple_game_group)
                end

                route_param :group_id do
                  desc 'Remove GameGroup from GameGroupItem'
                  delete do
                    game = find_game(current_user, params[:id], :group_item)

                    if game
                      group = game.groups.find_by(id: params[:group_id])

                      service = execute_service('GameGroups::DeleteService', group, current_user, params)
                      response_for_delete_service(service, :game_group, serializer: :simple_game_group)
                    else
                      not_found_error_response(:game_group_items)
                    end
                  end

                  namespace :items do
                    {
                      delete: :remove,
                      post: :add
                    }.each do |req_method, action_name|

                      desc "#{action_name.to_s.titleize} an item to a group"
                      params do
                        requires :item_title
                      end

                      send req_method do
                        game = find_game(current_user, params[:id], :group_item)

                        if game
                          group = game.groups.find_by(id: params[:group_id])

                          service = execute_service("GameGroups::#{action_name.to_s.titleize}ItemService", group, current_user, params)
                          response_for_update_service(service, :game_group, serializer: :simple_game_group)
                        else
                          not_found_error_response(:game_group_items)
                        end
                      end
                    end

                  end

                end
              end
            end
          end
        end
      end
    end
  end
end
