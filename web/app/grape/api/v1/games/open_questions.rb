# encoding: UTF-8

module API
  module V1
    module Games
      class OpenQuestions < API::V1::Base

        helpers API::Helpers::V1::GamesHelpers

        before do
          authenticate_user
        end

        namespace :games do
          namespace :open_questions do
            desc "Create a new game GameOpenQuestion for user"
            params do
              requires :game, type: Hash do
                requires :title
                optional :image #, type: Rack::Multipart::UploadedFile
                optional :video_link #, type: Rack::Multipart::UploadedFile

                mutually_exclusive :image, :video_link
              end
            end

            post do
              service = execute_service('GameOpenQuestions::CreateService', current_user, params)
              response_for_create_service(service, :game_open_question)
            end

            route_param :id do
              desc "Updates owned game GameOpenQuestion"
              put do
                game = find_game(current_user, params[:id], :open_questions)

                service = execute_service('GameOpenQuestions::UpdateService', game, current_user, params)
                response_for_update_service(service, :game_open_question)
              end

              desc "Delete owned user game GameOpenQuestion"
              delete  do
                game = find_game(current_user, params[:id], :open_questions)

                service = execute_service('GameOpenQuestions::DeleteService', game, current_user, params)
                response_for_delete_service(service, :game_open_question)
              end

              [:corrected_answers, :pending_correction_answers].each do |namespace|
                paginated_endpoint do
                  desc 'Get paginated answers to correct for game open question'
                  get namespace do
                    if current_user.backoffice_profile?
                      response_for_paginated_endpoint "game_open_questions.#{namespace}", params[:id] do
                        game = find_game(current_user, params[:id], :open_questions)

                        return not_found_error_response(:game_open_questions) if game.blank?

                        answers = paginate(game.send(namespace)).includes(:user, :game_question)

                        options = {
                          serializer: :game_question_answer_list,
                          root: :answers
                        }

                        paginated_serialized_array(answers, options).as_json
                      end
                    else
                      forbidden_error_response(:game_open_questions)
                    end
                  end
                end
              end

              paginated_endpoint do
                desc 'Get paginated answers for game open question'
                params do
                  requires :student_id, type: Integer
                end

                get :answers do
                  if current_user.backoffice_profile?
                    response_for_paginated_endpoint "game_open_questions.answers", params[:id] do
                      game = find_game(current_user, params[:id], :open_questions)

                      return not_found_error_response(:game_open_questions) if game.blank?

                      student_answers = game.answers.where(user_id: params[:student_id])
                      answers = paginate(student_answers).includes(:user, :game_question)

                      options = {
                        serializer: :game_question_answer_list,
                        root: :answers
                      }

                      paginated_serialized_array(answers, options).as_json
                    end
                  else
                    forbidden_error_response(:game_open_questions)
                  end
                end
              end

              namespace :questions do
                route_param :question_id do

                  namespace :answers do
                    paginated_endpoint do
                      desc 'Get paginated answers for game question'
                      get do
                        if current_user.backoffice_profile?
                          response_for_paginated_endpoint 'game_open_questions.answers', params[:id] do
                            game = find_game(current_user, params[:id], :open_questions)

                            return not_found_error_response(:game_open_questions) if game.blank?

                            question = game.questions.find_by(id: params[:question_id])

                            return not_found_error_response(:game_questions) if question.blank?

                            answers = paginate(question.answers).includes(:user, :game_question)

                            options = {
                              serializer: :game_question_answer_list,
                              root: :answers
                            }

                            paginated_serialized_array(answers, options).as_json
                          end
                        else
                          forbidden_error_response(:game_open_questions)
                        end
                      end
                    end

                    route_param :answer_id do
                      desc 'Mark answer as correct/incorrect'
                      params do
                        requires :correct, type: Boolean
                      end
                      put do
                        game = find_game(current_user, params[:id], :open_questions)

                        if game
                          answer = game.answers.find_by(id: params[:answer_id], game_question_id: params[:question_id])

                          service = execute_service('GameQuestionsAnswers::UpdateService', answer, current_user, params)

                          response_append = {}

                          if service.success? && student_class_id = params[:student_class_id]
                            service_user = service.try(:answer).try(:user)

                            if service_user
                              score = service_user.games_scores
                                                  .find_by(game: game, student_class_id: student_class_id)

                              response_append.merge!(
                                student_report: {
                                  student_id: score.user_id,
                                  associated_resource_id: score.game_id,
                                  associated_resource_type: score.game_type,
                                  student_score: score.score_percent
                                }
                              ) if score
                            end

                          end

                          response_for_update_service(service,
                            :answer,
                            serializer: :game_question_answer
                          ).as_json.merge(response_append)

                        else
                          not_found_error_response(:game_open_questions)
                        end
                      end
                    end
                  end

                end
              end
            end
          end
        end
      end
    end
  end
end
