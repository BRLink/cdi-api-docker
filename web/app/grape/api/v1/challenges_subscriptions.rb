module API
  module V1
    class ChallengesSubscriptions < API::V1::Base

      before do
        authenticate_user
      end

      namespace :challenges do
        route_param :id do
          namespace :subscriptions do
            paginated_endpoint do
              desc 'list all subscriptions of a given challenge'
              get do
                challenge = Challenge.find_by(id: params[:id])
                unless challenge
                  return not_found_error_response :challenge
                end
                collection = paginate(challenge.users)
                paginated_serialized_array(collection, root: :subscriptions, serializer: :simple_user)
              end
            end

            desc 'subscribe on challenge'
            post do
              challenge = Challenge.find_by id: params[:id]
              unless challenge
                return not_found_error_response :challenge
              end
              service = execute_service('ChallengeSubscriptions::CreateService', challenge, current_user)
              simple_response_for_service(service)
            end

            desc 'unsubscribe on challenge'
            delete do
              challenge = Challenge.find_by id: params[:id]
              unless challenge
                return not_found_error_response :challenge
              end
              subscription = challenge.subscriptions.find_by(user_id: current_user.id)
              service = execute_service('ChallengeSubscriptions::DeleteService', subscription, current_user)
              simple_response_for_service(service)
            end
          end
        end
      end
    end
  end
end
