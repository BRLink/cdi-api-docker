# encoding: UTF-8

module API
  module V1
    class EducationalInstitutionsActions < API::V1::Base

      helpers API::Helpers::V1::EducationalInstitutionsHelpers
      helpers API::Helpers::V1::OthersHelpers

      before do
        authenticate_user
      end

      namespace :educational_institutions do

        desc "create institution"
        params do
          requires :institution, type: Hash do
            requires :name
            optional :email
            optional :about
            optional :site
            optional :telephone
            optional :address, type: Hash
          end
        end
        post do
          if params[:institution][:address].present? and params[:institution][:address][:zipcode].present?
            address_hash=address_hash_from_zipcode(params[:institution][:address][:zipcode])
            params[:institution][:address].merge!(address_hash)
          end
          service = execute_service('EducationalInstitutions::CreateService', current_user, params)
          response_for_create_service(service, :educational_institution)
        end

        route_param :id do

          desc "update institution"
          params do
            requires :institution, type: Hash do
              optional :name
              optional :email
              optional :about
              optional :site
              optional :telephone
              optional :address
            end
          end
          put do
            #institution_admin
            institution = EducationalInstitution.find_by(id: params[:id])
            if params[:institution][:address].present? and params[:institution][:address][:zipcode].present?
              address_hash=address_hash_from_zipcode(params[:institution][:address][:zipcode])
              params[:institution][:address].merge(address_hash)
            end
            service = execute_service('EducationalInstitutions::UpdateService', institution, current_user, params)
            response_for_update_service(service, :educational_institution)
          end

          desc "delete institution"
          delete do
            #must be owner
            institution = EducationalInstitution.find_by(id: params[:id])
            service = execute_service('EducationalInstitutions::DeleteService', institution, current_user, params)
            response_for_delete_service(service, :educational_institution)
          end

          desc 'update institution profile image'
          params do
            requires :institution, type: Hash do
              requires :profile_image
            end
          end
          put :profile_image do
            institution = EducationalInstitution.find_by(id: params[:id])
            service = execute_service('EducationalInstitutions::ProfileImageUpdateService', institution, current_user, params)
            response_for_update_service(service, :educational_institution)
          end

          desc 'update institution cover image'
          params do
            requires :institution, type: Hash do
              requires :profile_cover_image
            end
          end
          put :cover_image do
            institution = EducationalInstitution.find_by(id: params[:id])
            service = execute_service('EducationalInstitutions::ProfileCoverImageUpdateService', institution, current_user, params)
            response_for_update_service(service, :educational_institution)
          end


          namespace :admins do
            route_param :manager_id do
              desc "add admin"
              post do
                service = execute_service('InstitutionManagers::CreateService', current_user, params)
                response_for_create_service(service, :institution_manager)
              end

              desc "remove admin"
              delete do
                institution = EducationalInstitution.find_by(id: params[:id])
                institution_manager = institution.institution_managers.find_by(manager_id: params[:manager_id])
                service = execute_service('InstitutionManagers::DeleteService',institution_manager, current_user, params)
                response_for_delete_service(service, :institution_manager)
              end
            end

            desc "list admin+managers"
            get do
              institution = EducationalInstitution.find_by(id: params[:id])
              managers = institution.all_managers
              options = {
                serializer: :simple_user
              }
              serialized_array(managers, options)
            end
          end
        end


      end
    end
  end
end
