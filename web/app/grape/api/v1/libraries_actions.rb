# encoding: UTF-8

module API
  module V1
    class LibrariesActions < API::V1::Base

      before do
        authenticate_user
      end

      namespace :library do
        namespace :duplicate do
          [Multimedia, SupportingCourse, LearningDynamic].each do |klass|
            namespace klass.to_s.downcase do

              desc 'Clone a copy to my library'
              params do
                requires :id, type: Integer
              end
              post do
                item = klass.find_by(id: params[:id])
                return not_found_error_response(klass.to_s.underscore.pluralize) if item.nil?

                service = execute_service("#{klass.to_s.pluralize}::CloneService", item, current_user)
                response_for_create_service(service, klass.to_s.gsub(/(.)([A-Z])/,'\1_\2').downcase.to_sym)

              end
            end
          end
        end
      end
    end
  end
end
