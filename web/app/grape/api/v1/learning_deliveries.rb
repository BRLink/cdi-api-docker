# encoding: UTF-8

module API
  module V1
    class LearningDeliveries < API::V1::Base

      helpers API::Helpers::V1::LearningDeliveriesHelpers

      before do
        authenticate_user
      end

      namespace :learning_deliveries do
      end

      namespace :learning_dynamics do
        route_param :id do
          namespace :learning_delivery do

            desc 'Associates a LearningDelivery for LearningDynamic'
            params do
              requires :delivery, type: Hash do
                requires :format_type, type: String
                requires :delivery_date, type: DateTime
                requires :delivery_kind, type: Integer, values: LearningDelivery::DELIVERY_KINDS.keys
                optional :group_size, type: Integer, default: 1
                optional :description, type: String
              end
            end

            post do
              params.deep_merge!(
                delivery: {
                  learning_dynamic_id: params[:id]
                }
              )

              service = execute_service('LearningDeliveries::CreateService', current_user, params)
              response_for_create_service(service, :learning_delivery)
            end

            desc 'Update LearningDelivery'
            put do
              delivery = learning_delivery_for_track(current_user, id: params[:id])

              service = execute_service('LearningDeliveries::UpdateService', delivery, current_user, params)
              response_for_update_service(service, :learning_delivery)
            end

            desc 'Remove LearningDelivery from LearningDynamic'
            delete do
              delivery = learning_delivery_for_track(current_user, id: params[:id])

              service = execute_service('LearningDeliveries::DeleteService', delivery, current_user, params)
              response_for_delete_service(service, :learning_delivery)
            end

          end
        end
      end
    end
  end
end
