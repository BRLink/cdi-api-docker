# encoding: UTF-8

module API
  module V1
    class UsersMeStudentsCacheable < API::V1::Base

      helpers API::Helpers::V1::UsersHelpers
      helpers API::Helpers::V1::EducationalInstitutionsHelpers
      helpers API::Helpers::V1::LearningCyclesHelpers
      helpers API::Helpers::V1::LearningTracksHelpers

      with_cacheable_endpoints :users do
        namespace :me do

          before do
            authenticate_user
          end

          # no need to be paginated for now
          namespace :classes do
            desc 'Get all classes for current user'
            params do
              optional :status, default: nil, values: ::StudentClass::STATUS.values
            end
            get do
              cache_replace = [current_user.id, params[:status] || 'all']

              respond_with_cacheable 'users.current_user_classes', cache_replace do
                student_classes = current_user.student_classes

                if params[:status].present?
                  student_classes = student_classes.where(status: params[:status])
                end

                serialized_array(student_classes, root: :classes, serializer: :student_class_for_student).as_json
              end
            end
          end

          namespace :tracks do

            route_param :id do
              get :progress do
                enrollment = current_user.enrolled_learning_tracks.find_by(learning_track_id: params[:id])
                if enrollment
                  enrollment.as_json(methods: [
                    :progress,
                    :watched_slides_ids_for_track,
                    :not_watched_slides_ids_for_track,
                    :played_slides_ids_for_track,
                    :not_played_slides_ids_for_track
                  ])
                else
                  not_found_error_response(:learning_tracks)
                end
              end
            end

            paginated_endpoint do
              desc 'Get tracks feed for user (based on student_classes)'

              get :feed do
                # TODO: Come back and take a look in query caches
                response_for_paginated_endpoint 'users.current_user_tracks_feed', current_user.id do

                  categories_ids = categories_ids_from_params

                  if categories_ids.any?
                    cycles = current_user.classes_cycles
                                         .joins(:learning_tracks)
                                         .where(learning_tracks: { category_id: categories_ids })
                                         .group('learning_cycles.id')
                                         .uniq
                  else
                    # LearningCycles who user has access (by tracks)
                    cycles = current_user.classes_cycles.group('learning_cycles.id').uniq
                  end

                  options = {
                    each_serializer_meta: {
                      only_published_learning_tracks: true,
                      student_classes_ids: current_user.student_class_ids,
                      categories_ids: categories_ids
                    }
                  }

                  learning_tracks_by_cycles_feed_response(cycles, true, options)
                end
              end
            end

            paginated_endpoint do
              desc 'Get paginated enrolled LearningTracks for current user'
              params do
                optional :status, default: nil, values: EnrolledLearningTrack::STATUS.values
              end

              get do
                params[:status] ||= 'all'
                cache_replace = [current_user.id, params[:status]]
                categories_ids = categories_ids_from_params

                response_for_paginated_endpoint 'users.current_user_enrolled_tracks', cache_replace do
                  tracks = (params[:status] == 'all' ?
                            current_user.enrolled_tracks :
                            current_user.enrolled_tracks
                                        .where(enrolled_learning_tracks: { status: params[:status] })
                           ).order(updated_at: :desc)

                  if categories_ids.any?
                    tracks = tracks.where(learning_tracks: { category_id: categories_ids })
                  end

                  paginated_serialized_enrolled_tracks(tracks).as_json
                end
              end
            end
          end
        end
      end
    end
  end
end
