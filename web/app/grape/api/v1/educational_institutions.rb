# encoding: UTF-8

module API
  module V1
    class EducationalInstitutions < API::V1::Base

      helpers API::Helpers::V1::EducationalInstitutionsHelpers

      with_cacheable_endpoints :educational_institutions do

        paginated_endpoint do

          desc 'Get paginated educational institutions'
          get do
            response_for_paginated_endpoint 'educational_institutions.all', 'all' do
              educational_institutions = EducationalInstitution.all
              options = {
                serializer: :educational_institution
              }

              paginated_serialized_educational_institutions(educational_institutions, options).as_json
            end
          end
        end

        desc 'Get all educational institutions'
        get :all_simple do
          respond_with_cacheable 'educational_institutions.all_simple' do
            EducationalInstitution.all.as_json(only: [:id, :name, :created_at, :updated_at])
          end
        end

        route_param :id do
          desc 'Get one institution when logged in'
          get do
            authenticate_user
            institution = EducationalInstitution.find_by(id: params[:id])
            if institution.present?
              # is current_user member of the institution
              institution_user = institution.all_institution_users.find_by(user_id: current_user.id)
              institution.relation_with_me = institution_user.present? ? institution_user.status : nil
              # user can edit the institution?
              institution.can_edit = institution.user_can_update?(current_user)
              serialized_educational_institution(institution)
            else
              not_found_error_response(:educational_institution)
            end
          end

          namespace :members do
            desc 'Get members of an institution'
            paginated_endpoint do
              get do
                institution = EducationalInstitution.find_by(id: params[:id])
                if institution.present?
                  members = institution.users
                  paginated_serialized_users(members)
                else
                  not_found_error_response(:educational_institution)
                end
              end
            end
          end

        end
      end
    end
  end
end
