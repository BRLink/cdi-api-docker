module API
  module V1
    module Surveys
      class QuestionsOptions < API::V1::Base

        before do
          authenticate_user
        end

        namespace :surveys do
          namespace :polls do
            route_param :id do
              namespace :options do
                params do
                  requires :option, type: Hash do
                    requires :text, type: String
                  end
                end
                post do
                  survey = current_user.survey_polls.find_by(id: params[:id])
                  unless survey
                    return not_found_error_response(SurveyPoll.to_s.underscore)
                  end
                  service = execute_service('SurveyQuestionOptions::CreateService', survey.question, current_user, params)
                  response_for_create_service(service, :survey_question_option)
                end

                route_param :option_id do
                  desc 'Get survey question option by id'
                  get do
                    survey = SurveyPoll.find_by(id: params[:id])
                    unless survey
                      return not_found_error_response(SurveyPoll.to_s.underscore)
                    end
                    option = survey.options.find_by(id: params[:option_id])
                    unless option
                      return not_found_error_response(SurveyQuestionOption.to_s.underscore)
                    end
                    serialized_object(option, serializer: :survey_question_option)

                  end

                  desc 'Update question option'
                  params do
                    requires :option, type: Hash do
                      requires :text, type: String
                    end
                  end
                  put do
                    survey = current_user.survey_polls.find_by(id: params[:id])
                    unless survey
                      return not_found_error_response(SurveyPoll.to_s.underscore)
                    end
                    option = survey.options.find_by(id: params[:option_id])
                    unless option
                      return not_found_error_response(SurveyQuestionOption.to_s.underscore)
                    end
                    service = execute_service('SurveyQuestionOptions::UpdateService', option, current_user, params)
                    response_for_update_service(service, :survey_question_option)
                  end

                  desc 'Delete question option'
                  delete do
                    survey = current_user.survey_polls.find_by(id: params[:id])
                    unless survey
                      return not_found_error_response(SurveyPoll.to_s.underscore)
                    end
                    option = survey.options.find_by(id: params[:option_id])
                    unless option
                      return not_found_error_response(SurveyQuestionOption.to_s.underscore)
                    end
                    service = execute_service('SurveyQuestionOptions::DeleteService', option, current_user, params)
                    response_for_delete_service(service, :survey_question_option)
                  end

                  paginated_endpoint do
                    get :answers_users do
                      survey = SurveyPoll.find_by(id: params[:id])
                      unless survey
                        return not_found_error_response(SurveyPoll.to_s.underscore)
                      end
                      option = survey.options.find_by(id: params[:option_id])
                      unless option
                        return not_found_error_response(SurveyQuestionOption.to_s.underscore)
                      end
                      users = paginate(option.answers_users)
                      paginated_serialized_array(users, root: :users, serializer: :simple_user)
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
