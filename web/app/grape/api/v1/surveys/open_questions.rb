module API
  module V1
    module Surveys
      class OpenQuestions < API::V1::Base

        before do
          authenticate_user
        end

        namespace :surveys do

          namespace :open_questions do
            desc 'Create new Survey with Open Question'
            params do
              requires :survey, type: Hash do
                requires :title, type: String
                requires :visibility_type, type: String
                optional :visibility_id, type: Integer
                optional :image, type: Integer
                optional :video_link, type: String
              end
            end
            post do
              service = execute_service('SurveyOpenQuestions::CreateService', current_user, params)
              response_for_create_service(service, :survey_open_question, root: :survey)
            end

            route_param :id do

              desc 'Get Open Question by Id'
              get do
                survey = SurveyOpenQuestion.find_by(id: params[:id])
                unless survey
                  return not_found_error_response(SurveyOpenQuestion.to_s.underscore)
                end
                serialized_object(survey, serializer: :survey_open_question, root: :survey)
              end

              desc 'Update a survey - Open Question'
              params do
                requires :survey, type: Hash do
                  optional :title, type: String
                  optional :image, type: String
                  optional :video_link, type: String
                end
              end
              put do
                survey = current_user.survey_open_questions.find_by(id: params[:id])
                service = execute_service('SurveyOpenQuestions::UpdateService', survey, current_user, params)
                response_for_update_service(service, :survey_open_question, root: :survey)
              end

              desc 'Delete a survey - Open Question'
              delete do
                survey = current_user.survey_open_questions.find_by(id: params[:id])
                service = execute_service('SurveyOpenQuestions::DeleteService', survey, current_user, params)
                response_for_delete_service(service, :survey_open_question, root: :survey)
              end

              namespace :answers do
                desc 'Answer questions in batch'
                post do
                  survey = SurveyOpenQuestion.find_by(id: params[:id])
                  unless survey
                    return not_found_error_response(type_name)
                  end

                  service = execute_service('SurveyQuestionAnswers::BatchCreateService', survey, current_user, params)
                  success_answers = service.valid_answers
                  response_for_service(service, answers: success_answers)
                end
              end
            end
          end
        end
      end
    end
  end
end
