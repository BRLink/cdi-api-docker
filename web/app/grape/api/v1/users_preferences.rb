# encoding: UTF-8

module API
  module V1
    class UsersPreferences < API::V1::Base

      helpers API::Helpers::V1::UsersPreferencesHelpers

      before do
        authenticate_user
      end

      namespace :preferences do

        desc 'List current user preferences.'
        get do
          serialized_user_preferences(current_user)
        end

        desc 'Create new preference.'
        params do
          requires :preference, type: Hash do
            requires :type, type: String
            requires :title, type: String
            optional :author, type: String
            optional :link, type: String
          end
        end
        post do
          service = execute_service('UsersPreferences::CreateService', current_user, params)
          response_for_create_service(service, :user_preference)
        end

        route_param :id do
          desc 'Edit a preference.'
          params do
            requires :preference, type: Hash do
              optional :title, type: String
              optional :author, type: String
              optional :link, type: String
            end
          end
          put do
            preference = current_user.preferences.find_by(id: params[:id])
            service = execute_service('UsersPreferences::UpdateService', preference, current_user, params)
            response_for_update_service(service, :user_preference)
          end

          desc 'Delete a preference.'
          delete do
            preference = current_user.preferences.find_by(id: params[:id])
            service = execute_service('UsersPreferences::DeleteService', preference, current_user, params)
            response_for_delete_service(service, :user_preference)
          end
        end
      end


      namespace :users do


        route_param :id do
          namespace :preferences do

            desc 'List user preferences.'
            get do
              user = User.find_by(id: params[:id])
              unless user
                return not_found_error_response(User.to_s.underscore)
              end
              serialized_user_preferences(user)
            end
          end
        end
      end
    end
  end
end
