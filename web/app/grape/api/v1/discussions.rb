module API
  module V1
    class Discussions < API::V1::Base

      helpers API::Helpers::V1::DiscussionsHelpers

      before do
        authenticate_user
      end

      namespace :discussions do

        desc 'Create new Discussion'
        params do
          use :new_discussion
        end
        post do
          service = execute_service('Discussions::CreateService', current_user, current_user, params)
          response_for_create_service(service, :discussion, root: :discussion)
        end

        route_param :id do
          desc 'Get discussion by id'
          get do
            discussion = Discussion.find_by(id: params[:id])
            unless discussion
              return not_found_error_response(Discussion.to_s.underscore)
            end
            execute_service('DiscussionViews::CreateService', discussion, current_user)
            serialized_object(discussion, serializer: :discussion, root: :discussion)
          end

          desc 'Edit a given discussion'
          params do
            optional :discussion, type: Hash do
              optional :title, type: String
              optional :content, type: String
              optional :image, type: Integer
              optional :video_link, type: String
              optional :cover_id, type: Integer
              optional :tags
              optional :best_comment_id, type: Integer
            end
          end
          put do
            discussion = current_user.discussions.find_by(id: params[:id])
            service = execute_service('Discussions::UpdateService', discussion, current_user, params)
            response_for_update_service(service, :discussion, root: :discussion)
          end

          desc 'Delete a given discussion'
          delete do
            discussion = current_user.discussions.find_by(id: params[:id])
            service = execute_service('Discussions::DeleteService', discussion, current_user, params)
            response_for_delete_service(service, :discussion)
          end

          namespace :answers do
            desc 'Answer a discussion'
            params do
              requires :answer, type: Hash do
                requires :option_id, type: Integer
              end
            end
            post do
              discussion = Discussion.find_by(id: params[:id])

              unless discussion
                return not_found_error_response(Discussion.to_s.underscore)
              end

              service = execute_service('DiscussionAnswers::CreateService', discussion, current_user, params)
              response_for_create_service(service, :discussion_answer, {
                root: :discussion,
                service_record_method: :discussion,
                serializer: :discussion
              })
            end
          end

        end
      end
    end
  end
end
