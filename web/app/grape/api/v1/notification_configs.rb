module API
  module V1
    class NotificationConfigs < API::V1::Base

      helpers API::Helpers::V1::NotificationConfigsHelpers

      before do
        authenticate_user
      end

      namespace :notifications do
        namespace :configs do
          desc 'Get user notification configs'
          get do
            CDI::V1::NotificationConfigPresenter.new(current_user).to_json
          end

          desc 'Update user notification config'
          put do
            params[:notification_configs].each do |options|
              notification_config = current_user.notification_config(options[0])
              service = execute_service("NotificationConfigs::UpdateService", notification_config, current_user, options[1])

              service.execute
            end

            return generic_success_response
          end

        end
      end

    end
  end
end
