# encoding: UTF-8

module API
  module V1
    class UsersAuth < API::V1::Base

      helpers API::Helpers::V1::UsersHelpers

      namespace :users do

        namespace :auth do
          desc 'Try to authenticate user using social provider (Current only supports Facebook)'
          params do
            optional :create_user
            requires :access_token, type: String
          end
          post ':oauth_provider' do
            # if you need to create the user in login (not only check if exists)
            # use 'Users::FromProviderCreateService'

            service_name = service_name_for_provider_auth(params)

            service = execute_service("Users::#{service_name}",
              params.delete(:access_token),
              params.delete(:oauth_provider),
              params
            )

            user_provider_auth_response(service)
          end

          desc 'Authenticate an user using email and password'
          params do
            requires :provider, type: String
            requires :email   , type: String
            requires :password, type: String
          end

          post do
            service = authentication_service
            service.execute

            if service.success?
              response = user_success_response_for_service(service)
            else
              status service.response_status
              response = error_response_for_service(service)
            end

            response

          end

          desc 'Authenticate using token (must used for app tests)'
          post :token do
            authenticate_user
            user_success_response_for_service(token_authentication_service)
          end
        end

        namespace :password_reset do
          desc 'Send a password reset email to user'
          params do
            requires :user, type: Hash do
              requires :email
            end
          end

          post do

            email = params[:user][:email]
            user  = User.where('email = :identifier OR username = :identifier', identifier: email).first

            service = execute_service('Users::PasswordRecoveryService', user, params)

            response_service = {}

            if in_sandbox_environment? && service.success?
              response_service.merge!({
                reset_password_token: service.user.reset_password_token
              })
            end

            response_for_service(service, response_service)
          end

          desc 'Update user password'
          params do
            requires :password
            requires :password_confirmation
          end

          put ':token' do
            user = User.find_by(reset_password_token: params[:token])

            service  = execute_service('Users::PasswordUpdateService', user, params)

            simple_response_for_service(service)
          end
        end

        desc 'Clear user authorizations for token provider'
        delete '/:logout_action', requirements: { logout_action: /(logout|auth)/ }  do
          authenticate_user

          success = token_authentication_service.auth_token.try(:destroy)

          status success ? 200 : 400
        end
      end
    end
  end
end
