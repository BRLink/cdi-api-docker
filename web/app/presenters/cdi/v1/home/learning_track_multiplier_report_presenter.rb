module CDI
  module V1
    module Home
      class LearningTrackMultiplierReportPresenter < BasePresenter

        ## SEE LearningTrackMultiplierReportSerializer
        def initialize(user, options={})
          today = DateTime.now
          @options = {
            limit:     30
          }.merge(options)
          @user = user
        end

        def report
          @user.learning_tracks.order("updated_at DESC").limit(@options[:limit]).map{ |track| learning_track_stats(track) }
        end

        private

          ## FIXME possivelmente precisara retornar o acumulado por periodo >= DateTime.now - 15.days,
          ## tanto de enrolled quanto delivered.
          def learning_track_stats(track)
            {
              id: track.id,
              name: track.name,
              enrolled: track.enrolled_learning_tracks.count,
              delivered: track.student_deliveries.count
            }
          end
      end
    end
  end
end
