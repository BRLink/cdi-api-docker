module CDI
  module V1
    module PageStructure
      class CyclePlanningPagePresenter < BasePresenter

        attr_reader :cycle_planning, :questions_templates

        def initialize
          @cycle_planning      = ::CyclePlanning.new
          @questions_templates = ::QuestionTemplate.questions_for(@cycle_planning.class)
        end

      end
    end
  end
end
