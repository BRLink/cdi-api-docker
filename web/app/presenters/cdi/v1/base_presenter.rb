module CDI
  module V1
    class BasePresenter

      # workaround to respond_to ActiveModel::Serializer `attributes` class method
      def read_attribute_for_serialization(attribute)
        self.send(attribute)
      end

    end
  end
end
