class ChallengeDelivery < ActiveRecord::Base

  include Likeable
  include Commentable
  include AchievementConcerns::ChallengeDeliveryAchievementable
  include BadgeConcerns::ChallengeDeliveryBadgeable

  belongs_to :challenge
  belongs_to :presentation

  has_and_belongs_to_many :users

  validates :challenge_id, presence: true
  validates :video_link, presence: true, format: { with: URI.regexp }, if: -> { video_link.present? }

  mount_uploader :cover_image, ChallengeDeliveryCoverUploader
  mount_uploader :file, ChallengeDeliveryFileUploader
end
