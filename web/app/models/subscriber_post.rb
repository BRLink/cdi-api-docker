# This class is a relation between a subscriber (user) and post :)
class SubscriberPost < ActiveRecord::Base

  belongs_to :post
  belongs_to :user_subscription

  delegate :user, to: :user_subscription

  validates :post_id, uniqueness: { scope: [:user_subscription_id] }

end
