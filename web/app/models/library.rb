# encoding: UTF-8

class Library

  def self.query opts
    res=['Multimedia','SupportingCourse','LearningDynamic']
    if opts['resource']>=1 and opts['resource'] <=3
      res=[res[opts['resource']-1]]
    end
    main( opts['q'].nil? ? nil : clean_query_term(opts['q']),
          opts['owner'],
          opts['sort'].nil? || true,
          res,
          opts['resource'] == 4)
  end

  def self.clean_query_term term
    term.gsub('-', ' ')
  end

  def self.main(query=nil, owner=nil, sort=true, res=['Multimedia','LearningDynamic','SupportingCourse'],only_owned=false)

    ret=nil
    return ret if res.nil? or res.size==0 or owner.nil?
    unless only_owned
      ret=res.map{|item| item.constantize.includes(:user, :categories).multiplier_visible(owner)}
    else
      ret=[owner.learning_dynamics.includes(:user, :categories).roots,
          owner.multimedia.includes(:user, :categories),
          owner.supporting_courses.includes(:user, :categories)]
    end

    unless query.nil? # get all'
      ret=ret.map{|item| item.multi_field_search(query)}
    end

    if (not only_owned) and query.nil?
      ret=ret.map do |item|
        response = item.all
        response = response.roots if response.respond_to?(:roots)

        response
      end
    end

    ret=ret.flatten(1) if ret.size>0
    unless sort == false
      ret=ret.sort_by {|obj| (obj.updated_at || 10.years.ago)}
      ret=ret.reverse
    end
    ret
  end

end
