class GameQuestionAnswer < ActiveRecord::Base

  include Accessable

  has_one :origin, as: :originable

  belongs_to :game_question
  belongs_to :user
  belongs_to :option, class_name: 'GameQuestionOption'

  scope :corrected, -> { where.not(corrected_at: nil) }
  scope :pending_correction, -> { where(corrected_at: nil) }

  scope :correct, -> { where(correct: true) }
  scope :wrong, -> { where.not(correct: true) }

  has_many :question_options, through: :game_question, source: :options

  validates :game_question, :user, presence: true

  validates :option_id, presence: true, uniqueness: {
    # FIXME: message: -> I18n.t will not change the language, only after application restart
    scope: [:game_question_id, :user_id], message: I18n.t('cdi.errors.game_questions.cant_answer_question_twice')
  }, unless: :open_question_answer?

  validates :game_question_id, presence: true, uniqueness: {
    scope: [:user_id],
    # FIXME: message: -> I18n.t will not change the language, only after application restart
    message: I18n.t('cdi.errors.game_questions.cant_answer_question_twice')
  }, if: :open_question_answer?

  validates :answer_body, presence: true, if: :open_question_answer?

  validate :multiples_answers_for_question
  validate :option_belongs_to_question

  before_save :set_correct_flag

  # has_one through not working properly
  def game
    @game ||= self.game_question.try(:questionable)
  end

  def correct?
    return correct unless self.correct.nil?

    option && option.correct?
  end

  def open_question_answer?
    answer_body.present?
  end

  # for now, only GameOpenQuestion can be updated
  def user_can_update?(user)
    return false unless game_question.try(:questionable_type) == 'GameOpenQuestion'
    # game owner (teacher)
    user.id == game_question.try(:user_id) || super(user)
  end

  def corrected?
    corrected_at && corrected_at.present?
  end

  private
  def multiples_answers_for_question
    return self if open_question_answer?

    return self if game_question.allow_multiples_answers &&
                   game_question.questionable_type == 'GameQuiz'

    user_replied = game_question.answers.exists?(user_id: self.user_id)

    if user_replied
      self.errors.add(:base, I18n.t('cdi.errors.game_questions.cant_answer_question_twice'))
    end
  end

  def option_belongs_to_question
    return self if open_question_answer?

    unless question_options.exists?(id: self.option_id)
      self.errors.add(:option_id, I18n.t('cdi.errors.game_questions.option_not_found_in_question'))
    end
  end

  def set_correct_flag
    unless open_question_answer?
      self.correct = option && option.correct?
    end

    true
  end
end
