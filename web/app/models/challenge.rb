class Challenge < ActiveRecord::Base

  include Likeable
  include Commentable
  include Statusable
  include Accessable
  include Searchable

  VALID_FORMAT_TYPES = {
    text: [:doc, :txt],
    image: [:jpeg, :jpg, :png],
    video: [:avi, :mp3],
    presentation: [:ppt, :pptx, :pdf]
  }

  DELIVERY_KINDS = [
    :individual,
    :group
  ]

  STATUS = {
    draft:  'draft',
    published:   'published'
  }

  MAX_GROUP_SIZE = 6

  searchable_for :title, :tags

  belongs_to :presentation

  has_many :challenge_subscriptions
  has_many :users, through: :challenge_subscriptions
  has_many :challenge_deliveries
  has_many :discussions, as: :publisher
  has_many :delivered_users, through: :challenge_deliveries, source: :users

  alias :deliveries :challenge_deliveries
  alias :subscriptions :challenge_subscriptions

  scope :draft,  ->{ where("status = ?", STATUS[:draft]) }
  scope :open,   ->{ where("status = ? AND delivery_date >= ?", STATUS[:published], DateTime.now) }
  scope :closed, ->{ where("status = ? AND delivery_date < ?", STATUS[:published], DateTime.now) }
  scope :published, ->{ where("status = ?", STATUS[:published]) }
  scope :latest, ->{ published.order('status_changed_at DESC') }
  scope :closed_or_draft, ->{ where("(status = ? AND delivery_date < ?) OR status = ?", STATUS[:published], DateTime.now, STATUS[:draft]) }

  validates :title, :description, :format_type, :delivery_kind, :delivery_date, :delivery_title, presence: true

  validates :format_type, inclusion: VALID_FORMAT_TYPES.keys.map(&:to_s)
  validates :delivery_kind, inclusion: DELIVERY_KINDS.map(&:to_s)

  validates :group_size, numericality: {
    only_integer: true,
    greater_than: 0,
    less_than_or_equal_to: MAX_GROUP_SIZE,
    if: -> { group_size.present? }
  }

  mount_uploader :cover_image, ChallengeUploader

  status_configure default_status: :draft,
                      status_hash: STATUS

  def open?
    status == STATUS[:published] && delivery_date >= DateTime.now
  end

  def draft?
    status == STATUS[:draft]
  end

  def valid_format_types
    VALID_FORMAT_TYPES[format_type.to_sym].map(&:to_s)
  end

  def user_can_access? user
    user.admin?
  end

  def individual?
    delivery_kind == DELIVERY_KINDS.first.to_s
  end

  def group?
    delivery_kind == DELIVERY_KINDS.last.to_s
  end

  def delivered_users_count
    delivered_users.count
  end

  def challenge_deliveries_count
    challenge_deliveries.count
  end
end
