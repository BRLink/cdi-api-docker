class GameGroupItem < ActiveRecord::Base

  include Accessable
  include Historiable
  include GameScorable

  amoeba do
    nullify [:user_id]

    exclude_association [:origin, :edition_histories, :answers, :user_scores]

    include_association [:groups]
  end

  has_one :origin, as: :originable

  belongs_to :user

  has_many :groups, class_name: 'GameGroup'

  has_many :answers, through: :groups

  validates :user_id, presence: true

  def clone!(user_id)
    duped_object = self.amoeba_dup
    duped_object.user_id = user_id

    duped_object.save

    duped_object
  end

  def max_answers_count_per_user
    groups.size
  end

  def shuffled_group_items
    group_items.shuffle
  end

  def group_items
    groups.pluck(:items).flatten
  end

  def correct_percent_for_user(user)
    ((groups.map {|group| group.correct_percent_for_user(user) }.sum) / groups.size).round(2)
  end

  def items_by_title
    Hash[self.groups.pluck(:title, :items)]
  end
end
