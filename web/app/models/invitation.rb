class Invitation < ActiveRecord::Base
  # constants
  VALID_PERIOD = 5.days

  STATUS = {
    expired: -20,
    failed: -10,
    normal: 0,
    verified: 10
  }

  enum status: STATUS

  # validation
  validates :token, presence: true
  validates :email, presence: true
  validates :email, uniqueness: true
  validates :email, format: { with: User::EMAIL_REGEXP }, if: -> {
    self.email.present?
  }

  validates :profile_type, inclusion: User::VALID_MULTIPLIER_PROFILES_TYPES.keys.map(&:to_s)+['superuser','staff'], if: -> {
    self.profile_type.present?
  }

  validate :validate_email_unique_in_users

  # callbacks
  before_validation :set_need_a_token

  # fun
  def self.valid_invitation_with_token(token)
    item = self.where(token: token).first
    return nil if item.nil?
    return item if item.token_valid?
    nil
  end

  def valid_token?
    normal? and (not token_expired?)
  end

  alias :token_valid? :valid_token?

  def migrate_to_verified
    update_column(:status, self.class.statuses[:verified])
  end

  alias :use :migrate_to_verified

  def new_token(save = true)
    generate_token
    set_expires_at
    self.status = :normal
    self.save
  end

  private
  def token_expired?
    Time.zone.now >= expires_at ? (expired! || true) : false
  end

  def validate_email_unique_in_users
    if User.unscoped.exists?(email: email)
      errors.add(:email, "Um usuário com esse e-mail já existe.")
    end
  end

  def set_need_a_token
    unless self.token.present?
      new_token false
    end
  end

  def set_expires_at
    self.expires_at = Time.zone.now + VALID_PERIOD
  end

  def generate_token
    begin
      self.token = SecureRandom.hex
    end while self.class.exists?(token: token)
  end
end
