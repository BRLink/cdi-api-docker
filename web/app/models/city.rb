# encoding: UTF-8

class City < ActiveRecord::Base
  belongs_to :state

  has_many :addresses
  
  has_many :educational_institutions,
           through: :addresses,
           source: :addressable,
           source_type: "EducationalInstitution"
end
