class SurveyQuestion < ActiveRecord::Base

  include Accessable
  include Positionable
  include Likeable
  include AchievementConcerns::SurveyQuestionAchievementable

  belongs_to :questionable, polymorphic: true

  has_many :answers, class_name: 'SurveyQuestionAnswer'

  has_many :users_answers, through: :answers, source: :user

  has_many :options, class_name: 'SurveyQuestionOption'

  validates :questionable_id, :questionable_type, :title, presence: true

  validates :title, uniqueness: { scope: [:questionable_type, :questionable_id] }

  delegate :user_id, to: :questionable, allow_nil: true

  default_scope { order('created_at ASC') }

  after_initialize :set_defaults
  before_create :increment_questionable_counter
  before_destroy :decrement_questionable_counter

  def set_defaults
  end

  def user_answered? user
    answers.where(user_id: user.id).exists?
  end

  def set_position_before_save?
    false
  end

  def open_question?
    questionable.is_a? SurveyOpenQuestion
  end

  def question_poll?
    questionable.is_a? SurveyPoll
  end

  def increment_questionable_counter
    if questionable_type == 'SurveyOpenQuestion'
      SurveyOpenQuestion.increment_counter(:questions_count, questionable_id)
    end
  end

  def decrement_questionable_counter
    if questionable_type == 'SurveyOpenQuestion'
      SurveyOpenQuestion.decrement_counter(:questions_count, questionable_id)
    end
  end
end
