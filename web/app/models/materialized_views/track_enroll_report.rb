module MaterializedViews
  class TrackEnrollReport < BaseMaterializedView

    belongs_to :learning_track
    belongs_to :student_class

    alias :track :learning_track

    self.table_name = 'track_enroll_reports_matview'

    ::EnrolledLearningTrack::STATUS.each do |status, value|
      scope status, -> { where(status: value) }
    end

  end
end
