module MaterializedViews
  class TrackReviewReport < BaseMaterializedView

    belongs_to :learning_track
    belongs_to :student_class

    alias :track :learning_track

    self.table_name = 'track_review_reports_matview'

  end
end
