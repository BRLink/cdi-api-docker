class StudentDeliveryReview < ActiveRecord::Base
  include Accessable

  belongs_to :student_delivery

  has_one :student_user, through: :student_delivery, source: :user
  has_one :multiplier_user, foreign_key: :multiplier_user_id, class_name: 'User'

  validates :student_delivery_id, uniqueness: true, presence: true
  validates :multiplier_user_id, presence: true

  after_initialize :set_defaults

  private
  def set_defaults
    self.correct ||= true if correct.nil?
  end
end
