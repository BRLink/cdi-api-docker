module UserConcerns
  module Search
    extend ActiveSupport::Concern

    included do
      include PgSearch
      include Searchable

      searchable_for :first_name, :last_name, :username, if: ->(user) { user.normal? }

      # FIXME mudar para SearchDocument
      pg_search_scope :search_by_full_name,
        against: [:first_name, :last_name, :username],
        using: {
          tsearch: { prefix: true, any_word: true }
        }
    end
  end
end
