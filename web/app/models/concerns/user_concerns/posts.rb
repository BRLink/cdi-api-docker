module UserConcerns
  module Posts
    extend ActiveSupport::Concern

    included do
      has_many :posts
      has_many :post_abuses, class_name: 'PostAbuse'
    end
  end
end
