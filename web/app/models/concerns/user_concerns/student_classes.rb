module UserConcerns
  module StudentClasses
    extend ActiveSupport::Concern

    included do
      # teacher student classes
      has_many :administered_student_classes,
               class_name: 'StudentClass',
               foreign_key: :admin_user_id

      has_and_belongs_to_many :student_classes

      has_many :classes_friends, through: :student_classes, source: :users
      has_many :classes_teachers, through: :student_classes, source: :admin_user
      has_many :students_from_administred_classes, through: :administered_student_classes, source: :users

      # Educational Institution para alunos (through student_classes)
      has_many :student_classes_institutions, through: :student_classes, source: :educational_institution

      # Educational Institution
      has_many :owned_educational_institutions,
               class_name: 'EducationalInstitution',
               foreign_key: :admin_user_id

      # default scope
      has_many :institution_users, -> { accepted }
      has_many :educational_institutions, through: :institution_users

      has_many :pending_institution_users, -> { pending }, class_name: 'InstitutionUser'
      has_many :pending_educational_institutions, through: :pending_institution_users, source: :educational_institution

      has_many :rejected_institution_users, -> { rejected }, class_name: 'InstitutionUser'
      has_many :rejected_educational_institutions, through: :rejected_institution_users, source: :educational_institution

      has_many :all_institution_users, -> { all }, class_name: 'InstitutionUser'
      has_many :all_educational_institutions, through: :all_institution_users, source: :educational_institution


      # institution manager
      has_many :institution_managers, foreign_key: :manager_id
      has_many :managed_institutions, through: :institution_managers, class_name: 'EducationalInstitution'
      has_many :institution_tracks, through: :educational_institutions, source: :learning_tracks

      alias :classes :student_classes
    end

    def in_class?(student_class)
      self.student_classes.where(id: student_class.id).exists?
    end
  end
end
