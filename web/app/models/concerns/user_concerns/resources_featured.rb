module UserConcerns
  module ResourcesFeatured
    extend ActiveSupport::Concern

    included do
      has_many :resources_featured, class_name: 'ResourceFeatured'
    end
  end
end
