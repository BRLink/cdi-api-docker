module UserConcerns
  module Surveys
    extend ActiveSupport::Concern

    included do
      has_many :survey_open_questions
      has_many :survey_polls
    end
  end
end
