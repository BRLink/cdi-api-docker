module SearchRebuilder

  extend ActiveSupport::Concern

  module ClassMethods
    def rebuild model=nil
      return rebuild_all if model.nil?
      where(searchable_type: model.to_s).destroy_all
      model.find_each { |record| record.sync_search_document }
    end

    def rebuild_all
      self::VALID_SEARCHABLE_TYPES.values.each do |klass|
        rebuild(klass.constantize)
      end
    end
  end
end
