# Concern adicionado as objetos (resources) que
# servem como grupos de visibilidade.
# Pode ser um User, StudentClass, SubscribersGroup ou
# EducationalInstitution.
module ResourceVisibilitable
  extend ActiveSupport::Concern

  included do
    has_many :visible_posts, as: :visibility
  end

  def user_can_view_post? user
    user.id == id
  end
end
