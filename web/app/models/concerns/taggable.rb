module Taggable

  extend ActiveSupport::Concern

  included do
    before_save :set_tags

    unless self.const_defined?('MAX_TAGS_COUNT')
      self.const_set('MAX_TAGS_COUNT', 10)
    end

    scope :search_by_tags, -> (tags) {
      tags = Array.wrap(tags)
      where("tags @> ARRAY[?]::varchar[]", tags).
      order("#{table_name}.updated_at DESC")
    }

    scope :search_by_tag, -> (tag) {
      where("? = ANY (tags)", tag)
    }
  end

  private
  def set_tags
    self.tags = self.tags[0, self.class.const_get('MAX_TAGS_COUNT')] if self.tags
  end
end
