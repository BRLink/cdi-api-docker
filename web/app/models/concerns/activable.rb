module Activable
  extend ActiveSupport::Concern

  included do
    scope :active, -> { where(active: true) }

    after_initialize :set_active

    before_save :set_active
  end

  private
  def set_active
    self.active ||= true
  end

end
