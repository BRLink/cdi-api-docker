module Searchable

  extend ActiveSupport::Concern

  included do

    include FullTextSearchable

    has_one :search_document, as: :searchable, dependent: :destroy
    after_save :sync_search_document
  end

  def sync_search_document
    return unless can_create_search_document?
    build_search_document unless search_document
    search_document.sync_content
    search_document.save
  end

  def searchable_content
    self.class.searchable_keys.map { |key| send(key) }.compact.join(' ').squish
  end

  private
    def can_create_search_document?
      callable_if     = self.class.searchable_options.fetch(:if, nil)
      return callable_if.call(self) if callable_if.respond_to?(:call)

      callable_unless = self.class.searchable_options.fetch(:unless, nil)
      return !callable_unless.call(self) if callable_unless.respond_to?(:call)

      true
    end

  module ClassMethods
    def searchable_for *args
      keys    = args.select { |a| a.is_a?(Symbol) }
      options = args.last.is_a?(Hash) ? args.last : {}

      define_singleton_method(:searchable_keys) do
        keys
      end

      define_singleton_method(:searchable_options) do
        options
      end
    end
  end
end
