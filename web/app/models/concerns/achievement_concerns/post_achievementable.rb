module AchievementConcerns
  module PostAchievementable
    extend ActiveSupport::Concern
    include AchievementConcerns::SimpleAchievementable

    private
    def create_post
      if shared_through_attachments?
        grant_on_create :share_question
      elsif shared?
        # was shared from another post
        grant_on_create self.parent.publisher_type == "EducationalInstitution" ?
          :share_institution_post : :share_post
        grant_to(self.parent.user, :receive_share_on_post)
      else
        grant_on_create self.publisher_type == "EducationalInstitution" ?
          :create_institution_post : :create_post
      end
    end

  end
end

