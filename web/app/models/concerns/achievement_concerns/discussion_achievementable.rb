module AchievementConcerns
  module DiscussionAchievementable
    extend ActiveSupport::Concern
    include AchievementConcerns::SimpleAchievementable

    included do
      after_update :grant_on_best_answer_selected
    end

    private
    def ask_question
      grant_on_create :ask_question
    end

    def grant_on_best_answer_selected
      if self.changed.include?("best_comment_id") && self.best_comment
        grant_to self.best_comment.user, :have_answer_selected_as_best_answer
      end
    end
  end
end



