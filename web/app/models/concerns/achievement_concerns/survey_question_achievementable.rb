module AchievementConcerns
  module SurveyQuestionAchievementable
    extend ActiveSupport::Concern
    include AchievementConcerns::SimpleAchievementable

    private
    def create_survey
      grant_to self.questionable.user, :create_survey_question
    end

  end
end



