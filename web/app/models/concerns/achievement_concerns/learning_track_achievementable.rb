module AchievementConcerns
  module LearningTrackAchievementable
    extend ActiveSupport::Concern
    include AchievementConcerns::SimpleAchievementable

    included do
      after_update :publish_learning_track
    end

    private
    def create_learning_track
      unless self.parent.nil?
        grant_on_create :clone_learning_track
        grant_to self.parent.user, :learning_track_cloned_by_multiplier
      end
    end

    def publish_learning_track
      if self.changed.include?("status") && self.status_published?
        grant_on_create :publish_learning_track
      end
    end
  end
end


