module AchievementConcerns
  module SimpleAchievementable
    extend ActiveSupport::Concern

    # This concern is for the general cases of granting Achievements upon
    # resources creation (Like, Comment, Post, etc)

    # ACTION_MAP [resource that triggered achievement][optional resource]

    ACTION_MAP = {
      "Like" => {
        "Post" => :like_post,
        "BlogArticle" => :like_blog_article,
        "Discussion" => :like_question,
        "LearningTrack" => :like_learning_track,
        "ChallengeDelivery" => :like_challenge_delivery
      },
      "Comment" => {
        "Post" => :comment_post,
        "LearningTrack" => :comment_learning_track,
        "BlogArticle" => :comment_blog_article,
        "Discussion" => :answer_discussion,
        "ChallengeDelivery" => :comment_challenge_delivery
      },
      "Post" => :create_post,
      "User" => :complete_sign_up,
      "Discussion" => :ask_question,
      "LearningTrack" => :create_learning_track,
      "LearningCycle" => :create_learning_cycle,
      "EnrolledLearningTrack" => :create_enrollment,
      "LearningTrackReview" => :review_learning_track,
      "InstitutionUser" => nil, # only grants achievements on update
      "ChallengeDelivery" => :create_challenge_delivery,
      "ChallengeSubscription" => :create_challenge_subscription,
      "StudentClass" => :create_student_class,
      "SurveyQuestion" => :create_survey,
      "SurveyQuestionAnswer" => :create_survey_answer
    }

    included do
      after_create do
        action = ACTION_MAP[self.class.to_s]
        action = action[self.resource.class.to_s] if self.respond_to? :resource
        send action if action
      end

      after_destroy do
        revoke_from_resource
      end
    end

    private
    def grant_on_create achievement_action
      grant_to self.user, achievement_action
    end

    def grant_on_receive achievement_action
      grant_to self.resource.user, achievement_action
    end

    def grant_to user, achievement_action, resource = self
      user.user_achievements.create(
        resource: resource,
        achievement: Achievement.find_by(action: achievement_action)
      )
    end

    def revoke_from_resource
      UserAchievement.destroy_all(resource: self)
    end
  end
end

