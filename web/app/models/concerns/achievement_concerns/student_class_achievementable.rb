module AchievementConcerns
  module StudentClassAchievementable
    extend ActiveSupport::Concern
    include AchievementConcerns::SimpleAchievementable

    private
    def create_student_class
      grant_to self.admin_user, :create_student_class
    end

  end
end


