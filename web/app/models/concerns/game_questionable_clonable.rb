module GameQuestionableClonable

  extend ActiveSupport::Concern

  included do
    def clone!(user_id)
      duped_object = self.amoeba_dup
      duped_object.user_id = user_id
      duped_object.save

      if self.respond_to?(:question)
        duped_object.question = self.question.amoeba_dup
        duped_object.question.options = self.question.options.map(&:amoeba_dup)
      else
        self.questions.each do |q|
          new_question = q.amoeba_dup
          duped_object.questions << new_question
          new_question.save
          new_question.options = q.options.map(&:amoeba_dup)
        end
      end

      duped_object
    end
  end
end
