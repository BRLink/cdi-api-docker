# Concern adicionado as objetos (resources) que
# suportam visibilidade. Ex: Post, DiscussionPoll,
# DiscussionOpenQuestion, SurveyPoll, SurveyOpenQuestion.
module ResourceVisibility

  extend ActiveSupport::Concern

  included do
    has_many :visibilities, as: :resource, dependent: :destroy

    accepts_nested_attributes_for :visibilities
  end

  def visible_to_all?
    visibilities.first.all?
  end

  def visible_to_multipliers?
    visibilities.first.multipliers?
  end

  def visible_to_students?
    visibilities.first.students?
  end

  def user_can_read? user
    return true if user_can_access?(user) || visible_to_all?
    if visible_to_students?
      user.students?
    elsif visible_to_multipliers?
      user.multiplier?
    else
      visibilities.any? { |visibility| visibility.visibility_object.user_can_view_post? user }
    end
  end
end
