module Codeable

  extend ActiveSupport::Concern

  ALPHABET_LETTERS = ('A'..'Z').to_a

  included do

    def self.code_options(options = {})
      options = {
        can_generate_duplicate_code: false,
        generate_code: true
      }.merge(options)

      define_method :can_generate_duplicate_code? do
        options[:can_generate_duplicate_code].present?
      end

      define_method :generate_code? do
        options[:generate_code].present?
      end
    end

    code_options

    validates :code, uniqueness: true, if: -> {
      self.generate_code? && !self.can_generate_duplicate_code?
    }

    validates :code, presence: true, if: -> {
      self.generate_code?
    }

    before_validation :generate_code

    private

    def generate_code
      return true unless generate_code?
      return true if self.code?

      begin
        self.code = generate_alphanumeric_code
      end while self.class.exists?(code: self.code)
    end

    def generate_alphanumeric_code
      letters       = ALPHABET_LETTERS.dup.shuffle.sample(4).join('')
      random_number = ("%04d" % (SecureRandom.random_number * 10000)).to_s

      letters << random_number
    end
  end
end
