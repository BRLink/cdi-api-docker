module Imageable

  extend ActiveSupport::Concern

  attr_accessor :original_filename unless self.respond_to?(:original_filename)
  attr_accessor :create_image_versions

  included do
    mount_uploader :image, PhotoUploader
    mount_base64_uploader :image, PhotoUploader, base_filename: :image

    process_in_background :image if CDI::Config.enabled?(:process_photo_upload_in_background) ||
                                    CDI::Config.enabled?(:process_upload_in_background)

    before_save :update_asset_attributes

    after_initialize :set_imageable_defaults

    skip_callback :commit, :after, :remove_image!
  end

  def images_url
    image_versions = self.image.versions
    versions = image_versions.map(&:first)
    images   = image_versions.map(&:last).map(&:url)

    Hash[versions.zip(images)].presence || [self.image_url]
  end

  def images_url_with_metadata(metadata_whitelist=[])
    image_urls = { original: self.image_url }.merge(self.images_url)
    images     = Hash.new {|k, v| k[v] = {} }

    image_urls.each do |version, image_data|
      images[version][:url] = image_data

      metadata = (((self.assets_metadata || {}).symbolize_keys[version.to_sym]) || {}).symbolize_keys
      metadata.slice!(*metadata_whitelist.map(&:to_sym)) if metadata_whitelist.any?

      images[version][:metadata] = metadata
    end

    images
  end

  def has_uploaded_image?
    self.image_url.present? &&
    !self.image_url.match(/(fallback|default)\.(png|jpg|jpeg)$/)
  end

  private
  def set_imageable_defaults
    @create_image_versions ||= true
  end

  def update_asset_attributes
    if image.present? && image_changed?
      self.content_type = image.file.content_type if self.respond_to?(:content_type)
      self.file_size    = image.file.size if self.respond_to?(:file_size)
    end
  end
end
