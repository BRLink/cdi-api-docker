module BadgeConcerns
  module DiscussionBadgeable
    extend ActiveSupport::Concern
    include BadgeConcerns::SimpleBadgeable

    included do
      after_update :award_on_best_answer_selected
    end

    private
    def award_on_discussion_created
      quantity = self.user.discussions.count
      award_on_create "created_%d_discussions", quantity
    end

    def award_on_best_answer_selected
      if self.changed.include?("best_comment_id") && self.best_comment
        user = self.best_comment.user
        quantity = Discussion.joins(:best_comment).where("comments.user_id = ?", user.id).count
        award_to user, "had_%d_answers_selected_as_best_answer", quantity
      end
    end
  end
end



