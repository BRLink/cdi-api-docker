module BadgeConcerns
  module PostBadgeable
    extend ActiveSupport::Concern
    include BadgeConcerns::SimpleBadgeable

    private
    def award_on_post_created
      unless shared_through_attachments? || shared?
        quantity = self.user.posts.where(parent: nil).count
        award_on_create "posted_%d_times_in_feed", quantity
      end
    end

  end
end

