class SupportingCourse < ActiveRecord::Base

  include Taggable
  include Accessable
  include Statusable
  include Categorizable
  include Searchable
  include LibrarySearchable

  MAX_SKILLS_COUNT = 5

  # Suporte a busca global
  searchable_for :title, :tags, :categories_names, if: ->(course) { course.status_published? }

  # Suporte a busca interna
  search_extra_fields :title

  has_one :origin, as: :originable

  belongs_to :user

  belongs_to :presentation

  has_many :slides, through: :presentation, source: :presentation_slides

  has_and_belongs_to_many :skills, join_table: :supporting_courses_skills

  validates :title, :user, presence: true
  # validates :presentation, presence: true, on: [:update] # allow presentation to be blank on creation?

  before_save :set_skills

  after_create :make_presentation

  amoeba do
    nullify [:user_id, :status, :status_changed_at]
    include_association :skills
  end

  def clone!(owner_or_id, options = {})
    duped_object = self.amoeba_dup
    duped_object.user_id = owner_or_id.respond_to?(:id) ? owner_or_id.id : owner_or_id
    duped_object.category_ids = self.category_ids
    duped_object.parent_id = self.parent_id || self.id

    duped_presentation = self.presentation.clone!(owner_or_id) if self.presentation.present?

    if duped_presentation.errors.blank?
      duped_object.presentation = duped_presentation
      duped_object.save
      duped_object.presentation.destroy unless duped_object.errors.blank?
    else
      duped_presentation.errors.each do |name, detail|
        duped_object.errors.add("presentation_#{name}".to_sym, detail)
      end
    end

    duped_object
  end

  alias :duplicate :clone!

  def thumbnail_url
    return nil if self.slides.size == 0
    self.slides.first.try(:thumbnail_url)
  end

  private
  def set_skills
    self.skill_ids = self.skill_ids[0, MAX_SKILLS_COUNT||5] if self.skill_ids.any?
  end

  def make_presentation
    return if presentation.present?
    presentation_type = Presentation::PRESENTATION_TYPES[:content]
    create_presentation(user: user, presentation_type: presentation_type)

    self.save
  end

  def max_categories
    4
  end
end
