class SubscribersGroupMember < ActiveRecord::Base

  belongs_to :subscribers_group
  belongs_to :user_subscription

  delegate :user, to: :user_subscription

  alias :subscription :user_subscription

  validates :subscribers_group_id, uniqueness: { scope: [:user_subscription] }

  include Accessable

  def user_can_access? user
    user.id == subscribers_group.user_id
  end
end
