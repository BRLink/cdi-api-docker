class LearningTrackReview < ActiveRecord::Base

  include Accessable
  include AchievementConcerns::LearningTrackReviewAchievementable

  belongs_to :learning_track
  belongs_to :student_class
  belongs_to :user

  REVIEWS_TYPES_TEXTS = I18n.t('app.track_review')

  REVIEWS_TYPES = {
    :all_new => 1,
    :something_new  => 2,
    :nothing_new => 3
  }

  enum review_type: REVIEWS_TYPES

  validates :learning_track_id, uniqueness: { scope: [:user_id, :student_class_id] }
  validates :review_type, presence: true
  validates :score, presence: true, inclusion: { in: 1..5 }

  after_initialize :set_defaults
  after_save       :update_average_score

  def review_types_texts
    REVIEWS_TYPES_TEXTS
  end

  def review_type_text
    REVIEWS_TYPES_TEXTS[self.review_type.to_sym]
  end

  private
  def set_defaults
    self.review_type ||= :all_new
  end

  def update_average_score
    learning_track = LearningTrack.find(self.learning_track_id)
    learning_track.average_score = learning_track.reviews.average(:score)
    learning_track.save
  end
end
