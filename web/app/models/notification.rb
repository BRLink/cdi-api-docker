# encoding: UTF-8
class Notification < ActiveRecord::Base

  default_scope { order(created_at: :desc) }

  NOTIFICATION_CONFIG = JSON.load(Rails.root.join('config', 'notifications_types.json'))
  TYPES = NOTIFICATION_CONFIG.keys

  READ_STATUS = {
    read: true,
    unread: false
  }

  belongs_to :notificable, polymorphic: true

  belongs_to :receiver_user, foreign_key: :receiver_user_id, class_name: 'User'
  belongs_to :sender_user,   foreign_key: :sender_user_id  , class_name: 'User'

  has_one :origin, as: :originable

  after_initialize :set_defaults

  validates :receiver_user_id, :notification_type, presence: true
  validates :notification_type, inclusion: TYPES.map(&:to_s)

  validate :notified_user_is_not_sender_user, on: :create
  validate :profile_type_is_permitted

  READ_STATUS.each do |status, value|
    scope status, -> { where(read: value)  }

    define_method "mark_as_#{status}" do
      self.update_attribute(:read, value)
    end

    define_method "#{status}?" do
      self.read == value
    end
  end

  def body
    unless @body
      localize = I18n.t(self.notification_type, scope: 'cdi.notifications')
      if notificable_type == "UserAchievement"
        @body = localize[:achievement][notificable.achievement.action.to_sym][:message]
      else
        @body = localize[:message]
      end
    end

    @body
  end

  def notification_title
    unless @notification_title
      localize = I18n.t(self.notification_type, scope: 'cdi.notifications')
      if achievement_notification?
        @notification_title = localize[:achievement][notificable.achievement.action.to_sym][:notification_title]
      else
        @notification_title = localize[:notification_title]
      end
    end

    @notification_title
  end

  def route
    unless @route
      localize = I18n.t(self.notification_type, scope: 'cdi.notifications')
      if self.notificable
        if achievement_notification?
          localize = localize[:achievement][notificable.achievement.action.to_sym]
          @route = localize[:route] % {
            id: notificable.resource.id,
            notifiable_path: localize[:notifiable_path]
          }
        else
          @route = localize[:route] % self.notificable.as_json.symbolize_keys
        end
      end
    end
    @route
  end

  def metadata
    metadata = { sender_user_id: self.sender_user_id, body: self.body }

    return metadata if self.notificable_id.blank?

    metadata.merge!(self.slice("notificable_id", "notificable_type", "notification_title"))

    if self.notificable.respond_to?(:profile_images)
      metadata.merge!(images: self.notificable.profile_images)
    end

    metadata
  end

  private
  def notification_meta
    @_receiver_user ||= self.receiver_user
    @_sender_user   ||= self.sender_user
    @_notificable   ||= self.notificable

    if @_notificable.blank? && self.notificable_type.present?
      @_notificable = self.notificable_type.constantize.try(:with_deleted).try(:find, self.notificable_id)
    end

    CDI::NotificationMetaParse.new(@_receiver_user, @_sender_user, @_notificable).parse
  end

  def notified_user_is_not_sender_user
    if self.sender_user_id == self.receiver_user_id
      self.errors.add(:email, I18n.t('cdi.errors.notifications.user_cant_notify_yourself'))
    end
  end

  def profile_type_is_permitted
    valid_types = NOTIFICATION_CONFIG[notification_type.to_s] || []

    # using any? instead of include?, config could have 'multiplier' and receive profile type is 'teacher'
    unless valid_types.include?("all") || valid_types.any? { |type| receiver_user.send(:"#{type}?") }
      self.errors.add(:receiver_profile_type,
                      I18n.t('cdi.errors.notifications.forbidden_user_type') % {
        profile_type: receiver_user.profile_type,
        notification: notification_type
      })
    end
  end

  def set_defaults
    self.read ||= false
  end

  def achievement_notification?
    self.notificable.present? && self.notificable.is_a?(UserAchievement)
  end
end
