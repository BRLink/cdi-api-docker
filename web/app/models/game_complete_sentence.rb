class GameCompleteSentence < ActiveRecord::Base

  include Accessable
  include Statusable
  include Historiable
  include GameScorable

  amoeba do
    nullify [:user_id, :status, :status_changed_at]

    exclude_association [:origin, :edition_histories, :answers, :user_scores]
  end

  # TODO: Move to config and update tests spec when value is false
  PARAMETERIZE_WORDS = true
  WORDS_MARK_REGEXP = /(\<word_\d+\>)|(\%s)/im

  has_one :origin, as: :originable

  belongs_to :user

  has_many :answers, as: :game_wordable, class_name: 'GameWordAnswer'

  validates :user_id, :words, :sentence, presence: true

  after_initialize :set_defaults

  validate :validate_valid_sentence_marks_for_words

  before_save :replace_marks_in_sentence

  def clone!(user_id)
    duped_object = self.amoeba_dup
    duped_object.user_id = user_id

    duped_object.save
    duped_object
  end

  def words_length
    (self.words || []).map(&:size)
  end

  def full_sentence
    sentence % words
  end

  def max_answers_count_per_user
    1
  end

  def words_match?(words = [])
    swords = self.words.map {|w| parameterize_word(w) }
    words  = words.map {|w| parameterize_word(w) }

    swords == words
  end

  def parametized_words
    self.words.map(&:parameterize)
  end

  def correct_words(words = [])
    words.select {|w| self.parametized_words.member?(parameterize_word(w)) }
  end

  def wrong_words(words = [])
    words.select {|w| self.parametized_words.exclude?(parameterize_word(w)) }
  end

  # TODO: Refactor this to use one common method
  def correct_words_by_position(words)
    swords = self.parametized_words
    correct_words = []

    words.each_with_index do |word, index|
      correct_words << word if swords.at(index) == parameterize_word(word)
    end

    correct_words
  end

  # TODO: Refactor this to use one common method
  def wrong_words_by_position(words)
    swords = self.parametized_words
    wrong_words = []

    words.each_with_index do |word, index|
      wrong_words << word if swords.at(index) != parameterize_word(word)
    end

    wrong_words
  end

  def correct?(words)
    correct_words_by_position(words).map(&:parameterize) == parametized_words
  end

  def correct_percent_for_user(user)
    answer = self.answers.find_by(user_id: user.id)

    return 0 if answer.blank?

    ((correct_words_by_position(answer.words).size * 100) / self.words.size).round(2)
  end

  private
  def replace_marks_in_sentence
    self.sentence.gsub!(WORDS_MARK_REGEXP, '%s') if self.sentence
  end

  def valid_sentence_marks_for_words?
    sentence = self.sentence || ''
    words = self.words || []

    marks = sentence.scan(WORDS_MARK_REGEXP)

    # remove nil from not matched cases
    marks.flatten.compact.size == words.size
  end


  def validate_valid_sentence_marks_for_words
    unless valid_sentence_marks_for_words?
      self.errors.add(:sentence, 'invalid_words_num_for_sentence_marks')
    end
  end

  def parameterize_word(word)
    PARAMETERIZE_WORDS ? word.parameterize : word
  end

  def set_defaults
  end

  def custom_data_for_histories
    self.changes.symbolize_keys.slice(:user_id, :status, :status_changed_at)
  end
end
