class Multimedia < ActiveRecord::Base

  include Taggable
  include Accessable
  include Statusable
  include PostAttachable
  include Categorizable
  include Searchable
  include LibrarySearchable

  STATUS = Statusable::DEFAULT_STATUS.merge({
    social_published: 'social_published'
  })

  # clone behavior
  # use metadata to preserve url, file_size file_type etc.
  amoeba do
    enable
    exclude_association [:categories, :category_resources]
    nullify [:file, :link, :status, :status_changed_at]
  end

  mount_uploader :file, MultimediaUploader

  skip_callback :commit, :after, :remove_file!

  status_configure default_status: :published,
                      status_hash: STATUS,
        validate_status_inclusion: false

  attr_accessor :flag

  MULTIMEDIA_TYPES = {
    'link'  => ['external url'],
    'ppt'   => ['powerpoint','presentation'],
    'pdf'   => ['pdf'],
    'audio' => ['audio'],
    'video' => ['video'],
    'image' => ['image'],
    'word'  => ['word']
  }

  # Suporte a busca global
  searchable_for :name, :tags, :categories_names, if: ->(multimedia) { multimedia.status_published? }

  # Suporte a busca interna
  search_extra_fields :name

  has_one :origin, as: :originable

  after_save :set_file_link

  belongs_to :user

  validates :link, format: URI::regexp(%w(http https)),
            allow_blank: true,
            unless: "Rails.env.development? || Rails.env.test?"

  validates :user, presence: true

  validates :multimedia_type, presence: true, inclusion: MULTIMEDIA_TYPES.keys

  validates :status, inclusion: STATUS.values

  def clone!(owner_or_id, options = {})
    duped_object = self.amoeba_dup
    duped_object.user_id = owner_or_id.respond_to?(:id) ? owner_or_id.id : owner_or_id
    duped_object.category_ids = category_ids

    puts "-> Multimedia.clone! #{category_ids}"

    duped_object.save

    duped_object
  end

  alias :duplicate :clone!

  private
    def max_categories
      4
    end

  protected

    def set_file_link
      if flag == 'file'
        self.metadata={} if self.metadata.nil?
        self.metadata['url']=self.file.url
        self.update_column(:metadata, self.metadata)
        flag = nil
      end
    end
end
