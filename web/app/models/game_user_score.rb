class GameUserScore < ActiveRecord::Base

  belongs_to :user
  belongs_to :student_class

  belongs_to :game, polymorphic: true

  class << self
    def score_for(user, student_class, game)
      score = game.user_scores.find_or_create_by(user_id: user.id, student_class_id: student_class.id)
      game  = score.game

      sgame = game.is_a?(GameGroup) ? game.game_group_item : game

      game_slide ||= ::PresentationSlide.where(associated_resource: sgame).last

      if game_slide
        score.update_attribute(:score_percent, total_correct_percent_for_game(user, sgame))
      end

      score
    end

    def total_user_replied_answers_for_game(user, game)
      user_replied_answers_for_game(user, game).try(:size) || 0
    end

    def total_correct_percent_for_game(user, game)
      return 0 if game.blank? || user.blank?

      if game.respond_to?(:correct_percent_for_user)
        return game.correct_percent_for_user(user)
      end

      if game.is_a?(GamePoll)
        total_questions = ::GamePoll::MAX_QUESTIONS_BY_GAME # TODO:
      elsif game.is_a?(GameRelatedItem)
        total_questions = game.items_count || s.games.items.count
      elsif game.respond_to?(:questions)
        total_questions = game.questions.count
      elsif game.respond_to?(:max_answers_count_per_user)
        total_questions = game.max_answers_count_per_user
      end

      total_correct_questions = total_correct_user_replied_answers_for_game(user, game)

      ((total_correct_questions * 100) / total_questions).round(2) rescue 0
    end

    def total_correct_user_replied_answers_for_game(user, game)
      if game.is_a?(GamePoll) # there's not wrong/correct answers for polls
        answers = user_replied_answers_for_game(user, game)
      elsif game.is_a?(GameOpenQuestion)
        answers = user_replied_answers_for_game(user, game).corrected.correct
      else
        answers = user_replied_answers_for_game(user, game).correct
      end

      answers.try(:size) || 0
    end

    def user_replied_answers_for_game(user, game)
      if game.respond_to?(:questions)
        user.game_question_answers
            .joins(:game_question)
            .where(game_questions: {
              questionable_id: game,
              questionable_type: game.class.to_s
            })
      elsif game.is_a?(GameGroup)
        game.game_group_item.answers.where(user_id: user.id)
      elsif game.respond_to?(:answers)
        game.answers.where(user_id: user.id)
      end
    end

  end
end
