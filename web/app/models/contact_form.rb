class ContactForm

  include ActiveModelable

  attributes :name, :email, :phone_area_code, :phone_number,
             :subject, :message_body, :origin_ip, :user_agent

  validates :name, :email, :phone_area_code,
            :phone_number, :subject, :message_body, presence: true

  validates :email, format: /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i

  def save
    valid?
  end

end
