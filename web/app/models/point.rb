class Point < ActiveRecord::Base

  belongs_to :educational_institution
  belongs_to :achievement

  validates :educational_institution_id, :achievement_id, :score, presence: true
  validates :achievement_id, uniqueness: { scope: :educational_institution_id }

end
