class Badge < ActiveRecord::Base

  GRANTABLE_QUANTITIES = [1, 5, 10, 15, 20, 50, 100]

  has_many :user_badges
  has_many :users, through: :user_badges

  validates :name, :description, presence: true
end
