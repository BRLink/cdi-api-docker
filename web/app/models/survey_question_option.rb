class SurveyQuestionOption < ActiveRecord::Base

  include Accessable
  include Positionable

  belongs_to :survey_question, counter_cache: :options_count
  alias :question :survey_question

  has_many :answers, class_name: 'SurveyQuestionAnswer', foreign_key: :option_id, dependent: :destroy
  has_many :answers_users, through: :answers, source: :user

  validates :text, :survey_question, presence: true

  after_initialize :set_defaults

  validates :text, uniqueness: { scope: [:survey_question_id] }

  validate :survey_question_reached_options_limit, on: [:create]

  def survey
    survey_question.questionable
  end

  def user_can_access?(user)
    survey_question.user_id == user.id
  end

  def survey_question_reached_options_limit
    if survey.is_a?(SurveyPoll)
      max_options = SurveyPoll::MAX_OPTIONS_BY_QUESTION
    end

    if max_options && survey_question.options.count >= max_options
      self.errors.add(:base, I18n.t('cdi.errors.surveys.max_options_for_question_reached'))
    end
  end

  def validate_duplicate_option_for_question
    return self if self.text.blank?

    if question.options.where(text: self.text).exists?
      self.errors.add(:base,  'cdi.errors.survey_question_options.duplicated_option_for_question')
    end
  end

  def set_defaults
  end

  def set_position_before_save?
    false
  end
end
