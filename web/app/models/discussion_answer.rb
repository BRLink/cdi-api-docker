class DiscussionAnswer < ActiveRecord::Base

  include Accessable

  belongs_to :user
  belongs_to :discussion, counter_cache: :answers_count
  belongs_to :option, class_name: 'DiscussionOption', counter_cache: :answers_count, dependent: :destroy

  has_many :options, through: :discussion

  validates :discussion, :user, presence: true
  validate :multiples_answers_for_discussion
  validate :option_belongs_to_discussion

  def user_can_update?(user)
    false
  end

  def multiples_answers_for_discussion
    if discussion.answers.exists?(user_id: self.user_id)
      self.errors.add(:base, I18n.t('cdi.errors.discussions.cant_answer_discussion_twice'))
    end
  end

  def option_belongs_to_discussion
    unless options.exists?(id: self.option_id)
      self.errors.add(:option_id, I18n.t('cdi.errors.discussions.option_not_found_in_discussion'))
    end
  end
end
