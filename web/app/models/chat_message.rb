# encoding: UTF-8

class ChatMessage < ActiveRecord::Base

  # todo for group chat
  # 1. mention users (create an unnread record for the user+message)
  # 2. quote messages

  include Historiable
  include Accessable

  EDITABLE_PERIOD_LENGTH = 5.minutes

  belongs_to :user
  belongs_to :chat_room
  has_many :chat_message_reads

  validates :user, :chat_room, :content,  presence: true
  validate :validates_editable_period, on: :update

  # accessable config
  def user_can_access?(user)
    user.admin? or self.chat_room.users.include?(user)
  end

  def user_can_update?(user)
    # only owner can edit message
    self.user_id == user.id and editable?
  end

  def user_can_delete?(user)
    # owner and admin of the room can delete message
    # owner can only delete when the message is editable
    if self.user_id == user.id and editable?
      return true
    end
    if self.chat_room.admins.include?(user)
      return true
    end
    return false
  end

  def editable?
    # remove the following line when EDITABLE_PERIOD_LENGTH is necessary
    return true
    Time.zone.now - created_at <= EDITABLE_PERIOD_LENGTH
  end

  # read status
  def user_has_read?(user)
    chat_message_reads.where(user_id: user).present?
  end

  private
  def validates_editable_period
    unless editable?
      self.errors.add(:editable_period, "message is not editable after #{EDITABLE_PERIOD_LENGTH}")
    end
  end

end
