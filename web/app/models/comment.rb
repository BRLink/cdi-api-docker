class Comment < ActiveRecord::Base

  include Accessable
  include Likeable
  include AchievementConcerns::CommentAchievementable
  include BadgeConcerns::CommentBadgeable

  VALID_RESOURCE_TYPES = {
    :tracks => 'LearningTrack',
    :posts => 'Post',
    :questions => 'GameQuestion',
    :photos => 'Photo',
    :discussions => 'Discussion',
    :blog_articles => 'BlogArticle',
    :challenges => 'Challenge',
    :challenge_deliveries => 'ChallengeDelivery'
  }

  belongs_to :user
  belongs_to :resource, polymorphic: true
  belongs_to :parent, class_name: 'Comment'
  has_many :children_comments, class_name: 'Comment', foreign_key: 'parent_id', dependent: :destroy
  has_many :nested_comments_users, through: :children_comments, source: :user

  validates :resource_type, presence: true, inclusion: VALID_RESOURCE_TYPES.values
  validates :content, presence: true

  def self.resource_types
    VALID_RESOURCE_TYPES
  end

  def nested_comments
    children_comments
  end

  def total_children_comments
    children_comments.count
  end

  def user_can_manage?(user)
    # TODO: Admins can edit/delete others users comments?
    self.user_id == user.id
  end
end
