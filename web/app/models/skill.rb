# encoding: UTF-8
class Skill < ActiveRecord::Base

  include Historiable
  include Positionable

  validates :name, presence: true

  has_and_belongs_to_many :learning_tracks

  alias :tracks :learning_tracks

  has_and_belongs_to_many :learning_dynamics

  has_and_belongs_to_many :supporting_courses, join_table: :supporting_courses_skills

  private
  def custom_data_for_histories
    self.changes.symbolize_keys.slice(:name, :position)
  end
end
