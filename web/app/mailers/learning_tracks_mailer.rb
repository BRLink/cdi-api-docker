class LearningTracksMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.learning_tracks_mailer.new_track.subject
  #
  def new_track(learning_track)
    @learning_track = learning_track
    @user           = @learning_track.user

    mail to: @user.email
  end
end
