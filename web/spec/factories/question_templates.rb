FactoryGirl.define do

  factory :question_template do
    question_type { QuestionTemplate::QUESTIONS_TYPES.values.sample }
    title { Faker::Lorem.sentence }
    question_for 'Learning Track'
    choices { Faker::Lorem.words(4) }

    QuestionTemplate::QUESTIONS_TYPES.each do |question_type, value|

      factory "question_template_type_#{question_type}" do
        question_type value
        title { Faker::Lorem.sentence }
        question_for 'Learning Track'

        if [:single_choice, :multiple_choices].member?(question_type)
          choices { Faker::Lorem.words(4) }
        end
      end
    end

    factory :question_template_with_questions do
      transient do
        questions_count 5
      end

      after(:create) do |question_template, evaluator|
        create_list(:question_without_template, evaluator.questions_count, question_template: question_template)
      end

      after(:build) do |question_template, evaluator|
        build_list(:question_without_template, evaluator.questions_count, question_template: question_template)
      end
    end

  end

end
