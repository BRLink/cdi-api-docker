FactoryGirl.define do
  factory :student_delivery_review do
    student_delivery nil
    correct false
    reply 1
    comment "MyText"
    multiplier_user_id 1
  end

end
