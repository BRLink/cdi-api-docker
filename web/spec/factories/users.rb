FactoryGirl.define do

  factory :user do

    trait :basic_data do
      first_name 'CDI'
      last_name { profile_type.to_s.titleize }
      sequence(:email) {|n| "cdi_#{profile_type.downcase}_#{n}@cdi.org.br" }
      password { "cdi_#{profile_type.downcase}" }
      password_confirmation { password  }
      about 'Small text about user'
      birthday { Faker::Date.between(20.years.ago, Date.today) }
      gender { User::VALID_GENDERS.values.sample }
    end

    trait :user_with_origin do
      basic_data

      after(:build) do |user|
        origin = build(:origin)
        # better than user.origin = origin (avoid error Mistmatch)
        origin.originable_id   = user.id
        origin.originable_type = user.class.name
      end

      after(:create) do |user|
        create(:origin, originable_id: user.id, originable_type: user.class )
      end
    end

    User::VALID_PROFILES_TYPES.each do |key, value|
      factory key, class: User, aliases: [ "#{key}_user" ] do |f|
        profile_type value
        user_with_origin
      end

      factory "#{key}_without_origin", class: User, aliases: [ "#{key}_user_without_origin" ] do |f|
        basic_data
        profile_type value
      end
    end

    factory :admin, class: User, aliases: [ "admin_user" ] do |f|
      profile_type :staff
      user_with_origin
    end

  end

end
