FactoryGirl.define do

  factory :blog_article do
    title { Faker::Lorem.sentence(4) }
    link "http://example.com/"
    description { Faker::Lorem.sentence(10) }
    pub_date { rand(1.week).seconds.ago }
    categories { [ create(:category) ] }
    sequence(:guid) { |n| "Guid #{n}" }
  end

end
