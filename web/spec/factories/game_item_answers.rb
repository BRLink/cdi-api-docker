FactoryGirl.define do
  factory :game_item_answer do
    game_related_item nil
    user nil
    correct false
    reply_word "MyString"
  end

end
