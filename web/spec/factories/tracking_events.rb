FactoryGirl.define do
  factory :tracking_events do
    enrolled_learning_track nil
    presentation_slide_id 1
    event_type 1
  end
end
