FactoryGirl.define do

  factory :survey_question do
    sequence(:position)
    title { Faker::Lorem.sentence(5) }
    association :questionable, factory: :survey_open_question

    trait :with_answers do
      transient do
        answers_count 5
      end
    end

    trait :with_options do
      transient do
        options_count 3
      end
    end

    after(:create) do |question, evaluator|
      create_list(:survey_question_user_answer, evaluator.answers_count,
                  survey_question: survey_question)
    end
  end

end

