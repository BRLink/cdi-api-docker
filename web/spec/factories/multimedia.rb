FactoryGirl.define do
  factory :multimedia do
    association :user, factory: :admin
    association :category
    status nil
    status_changed_at nil

    factory :multimedia_with_image do
      file {Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, '/spec/fixtures/library/test.png')))}
      multimedia_type 'image'
      flag 'file'
    end

    factory :multimedia_with_link do
      link 'http://www.google.com'
      multimedia_type 'link'
    end

  end

end
