FactoryGirl.define do
  factory :presentation do
    association :user, factory: :admin
    learning_track nil
    status nil
    status_changed_at nil

    factory :presentation_with_slides do
      transient do
        slides_count 5
      end
      after(:create) do |presentation, evaluator|
        presentation.slides = create_list(:presentation_slide, evaluator.slides_count)
      end
    end

    factory :student_presentation do
      after(:create) do |p|
        p.slides = [create(:presentation_slide)]
      end
    end
    factory :multiplier_presentation do
      after(:create) do |p|
        p.slides = [create(:dynamic_slide)]
      end
    end

  end

end
