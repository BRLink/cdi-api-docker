FactoryGirl.define do
  factory :resource_history do
    resource_type "MyString"
    resource_id 1
    action_type "MyString"
    new_status "MyString"
    last_status "MyString"
    user nil
    user_data ""
  end

end
