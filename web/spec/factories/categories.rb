FactoryGirl.define do
  factory :category do
    sequence(:name) { |n| "#{Faker::Commerce.department(1)} #{n}"  }
    sequence(:position)

    factory :category_with_cycles do
      transient do
        cycles_count 5
      end

      after(:create) do |category, evaluator|
        create_list(:learning_cycle, evaluator.cycles_count, category: category)
      end
    end
  end
end
