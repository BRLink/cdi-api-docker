FactoryGirl.define do
  factory :game_complete_sentence do
    association :user, factory: :teacher_user_without_origin
    status 'draft'
    status_changed_at nil
    sentence 'Complete %s the sentence %s with %s'
    words ['word_1', 'word_2', 'word_3']
  end
end
