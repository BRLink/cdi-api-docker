FactoryGirl.define do
  factory :learning_dynamic do
    title "MyString"
    association :user, factory: :admin
    association :category
    association :student_presentation, factory: :student_presentation
    association :multiplier_presentation, factory: :multiplier_presentation
    status nil
    status_changed_at nil
    perspectives ["Nos"]
  end

end
