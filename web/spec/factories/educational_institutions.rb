FactoryGirl.define do

  factory :educational_institution do
    name { Faker::Company.name }
    about { Faker::Lorem.paragraph(2, true) }
    association :admin_user, factory: :teacher_user
  end

end
