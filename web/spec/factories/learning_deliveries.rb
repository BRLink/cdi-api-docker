FactoryGirl.define do
  factory :learning_delivery do
    format_type "MyString"
    description "MyText"
    delivery_date "2015-08-28 03:10:55"
    delivery_kind 1
    learning_dynamic nil
  end

end
