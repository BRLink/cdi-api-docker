# Preview all emails at http://localhost:3000/rails/mailers/learning_cycle_mailer
class LearningCycleMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/learning_cycle_mailer/new_cycle
  def new_cycle
    LearningCycleMailer.new_cycle
  end

end
