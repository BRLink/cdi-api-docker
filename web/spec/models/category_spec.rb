require 'rails_helper'

RSpec.describe Category, type: :model do
  it 'must be valid' do
    category = create(:category)

    expect(category).to be_valid
  end

  it 'must be invalid' do
    category = build(:category, name: nil)

    expect(category).not_to be_valid
    expect(-> { create(:category, name: nil) }).to raise_error(ArgumentError)
  end

  it 'must have many learning cycles' do
    category = create(:category_with_cycles, cycles_count: 5)

    expect(category).to have_many(:learning_cycles)
    expect(category.learning_cycles.size).to be 5
  end

  it 'must resolve alias association name' do
    category = create(:category_with_cycles, cycles_count: 5)

    expect(category.respond_to?(:cycles)).to be true
    expect(category.cycles.size).to be 5
  end

  it 'must increment position successfully' do
    category     = create(:category)
    new_category = create(:category)

    expect(category.position).to be == (new_category.position - 1)
    expect(new_category.position).to be == category.position.next
    expect(category.position.next).to be == new_category.position
  end

  it 'allow same position' do
    category     = create(:category, position: 2)
    new_category = create(:category, position: 2)

    expect(category.position).to be == new_category.position
  end

end
