require 'rails_helper'

RSpec.describe LearningCycle, type: :model do
  before(:each) do
    @learning_cycle = create(:learning_cycle)
  end

  it 'must be valid' do
    learning_cycle = create(:learning_cycle)
    expect(learning_cycle).to be_valid
  end

  it 'must be invalid' do
    learning_cycle = build(:learning_cycle, category: nil)

    expect(learning_cycle).not_to be_valid
    expect(-> { create(:learning_cycle, category: nil) }).to raise_error(ArgumentError)
  end

  it 'must have column indexes' do
    expect(subject).to have_db_index(:name)
    expect(subject).to have_db_index(:category_id)
    expect(subject).to have_db_index(:user_id)
  end

  it 'must belongs to user' do
    expect(@learning_cycle).to belong_to(:user)
  end

  it 'must belongs to category' do
    expect(@learning_cycle).to belong_to(:category)
  end

  it 'must limit the number of tags' do
    max_tags_count = LearningCycle::MAX_TAGS_COUNT

    track = create(:learning_cycle_with_tags, tags_count: max_tags_count * 2)

    expect(track.tags.size).to be max_tags_count
  end

  it 'must be searchable by single tag' do
    tags  = ['education', 'english', 'student']
    cycle = create(:learning_cycle, tags: tags)

    cycles = LearningCycle.search_by_tag('education')
    invalid_cycles = LearningCycle.search_by_tag('_education')

    expect(cycles.count).to be 1
    expect(cycles).to be_a(ActiveRecord::Relation)

    expect(invalid_cycles.count).to be 0
    expect(invalid_cycles).to be_a(ActiveRecord::Relation)

    10.times { |i|
      create(:learning_cycle, tags: tags.dup.push("tag_#{i.next}"))
    }

    expect(LearningCycle.search_by_tag('english').count).to be 11
    expect(LearningCycle.search_by_tag('tag_1').count).to be 1
    expect(LearningCycle.search_by_tag('tag_2').count).to be 1
  end


  it 'must be searchable by multiples tags' do
    tags  = ['education', 'english', 'student']
    cycle = create(:learning_cycle, tags: tags)

    cycles = LearningCycle.search_by_tags(['education', 'english'])

    expect(cycles.count).to be 1
    expect(cycles).to be_a(ActiveRecord::Relation)

    10.times { |i|
      create(:learning_cycle, tags: tags.dup.push("tag_#{i.next}"))
    }

    expect(LearningCycle.search_by_tags(['education', 'student']).count).to be 11
    expect(LearningCycle.search_by_tags(['tag_1','student']).count).to be 1
  end

  it 'must raise error in tags search' do
    expect(-> { LearningCycle.search_by_tag('tag1', 'tag2') }).to raise_error(ArgumentError)
    expect(-> { LearningCycle.search_by_tags('tag1', 'tag2') }).to raise_error(ArgumentError)

    expect(-> { LearningCycle.search_by_tag(['tag1', 'tag2']).count }).to raise_error(ActiveRecord::StatementInvalid)
  end

end
