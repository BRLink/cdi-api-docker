require 'rails_helper'

RSpec.describe Question, type: :model do
  before(:each) do
    @question = create(:question)
    @question_without_template = build(:question_without_template)
    @question_with_answers = create(:question_with_answers)
  end

  it 'must delegate methods do question template' do
    expect(@question).to delegate_method(:title).to(:question_template)
    expect(@question).to delegate_method(:question_type).to(:question_template)
    expect(@question).to delegate_method(:instructions).to(:question_template)
    expect(@question).to delegate_method(:choices).to(:question_template)
  end

  it 'must raise error when trying to delegate to invalid question template' do
    expect( -> {@question_without_template.title } ).to raise_error(Module::DelegationError)
    expect( -> {@question_without_template.question_type } ).to raise_error(Module::DelegationError)
    expect( -> {@question_without_template.instructions } ).to raise_error(Module::DelegationError)
    expect( -> {@question_without_template.choices } ).to raise_error(Module::DelegationError)
  end

  it 'must belongs to question template' do
    expect(@question).to belong_to(:question_template)
  end

  it 'must belongs to template questionable' do
    expect(@question).to belong_to(:template_questionable)
  end

  it 'must have many users who replied the question' do
    expect(@question_with_answers.participating_users.size).to be > 0
    expect(@question_with_answers).to have_many(:participating_users)
    expect(@question_with_answers.participating_users.first).to be_a(User)
  end

  it 'must have many answers' do
    expect(@question_with_answers.answers.size).to be > 0
    expect(@question_with_answers).to have_many(:answers)
    expect(@question_with_answers.answers.first).to be_a(QuestionUserAnswer)
  end

  it 'must delegate convenience methods to question_template' do
    QuestionTemplate::QUESTIONS_TYPES.each do |key, _|
      expect(@question).to delegate_method("question_#{key}?".to_sym).to(:question_template)
    end
  end

  it 'must find index by answer text' do
    template = build(:question_template_type_single_choice, choices: ['1', '2', '3', '4'])
    question = build(:question, question_template: template)

    expect(question.index_by_answer('2')).to be == 1
    expect(question.index_by_answer('1')).to be == 0
    expect(question.index_by_answer('10')).to be_nil
  end

  it 'must find anwser text by answer index' do
    template = build(:question_template_type_single_choice, choices: ['1', '2', '3', '4'])
    question = build(:question, question_template: template)

    expect(question.text_by_index(1)).to be == '2'
    expect(question.text_by_index(3)).to be == '4'

    expect(question.text_by_index(10)).to be_nil
    expect(question.text_by_index('10')).to be_nil
  end

  it 'must format answer for question with choices (using string)' do
    template = create(:question_template_type_single_choice, choices: ['1', '2', '3', '4'])
    question = build(:question, question_template: template)

    answer_data = question.send(:process_answer_data_for_choices, '1')

    expect(answer_data).to be_a(Hash)
    expect(answer_data.key?(:answer_text)).to be true
    expect(answer_data.key?(:answer_index)).to be true
    expect(answer_data.keys).to be == [:answer_text, :answer_index]

    expect(answer_data[:answer_text]).to be == '1'
    expect(answer_data[:answer_index]).to be == 0

    answer_data = question.send(:format_data_for_answer, '3')

    expect(answer_data[:answer_text]).to be == '3'
    expect(answer_data[:answer_index]).to be == 2

    answer_data = question.send(:format_data_for_answer, '5')

    expect(answer_data[:answer_text]).to be == '5'
    expect(answer_data[:answer_index]).to be_nil

    answer_data = question.send(:format_data_for_answer, 3)

    expect(answer_data[:answer_text]).to be == '3'
    expect(answer_data[:answer_index]).to be == 2

  end

  it 'must format answer for question with choices (using hash)' do
    template = create(:question_template_type_single_choice, choices: ['1', '2', '3', '4'])
    question = build(:question, question_template: template)

    answer_data = question.send(:process_answer_data_for_choices, index: '1')

    expect(answer_data).to be_a(Hash)
    expect(answer_data.key?(:answer_text)).to be true
    expect(answer_data.key?(:answer_index)).to be true
    expect(answer_data.keys).to be == [:answer_text, :answer_index]

    expect(answer_data[:answer_text]).to be == '2'
    expect(answer_data[:answer_index]).to be == 1

    answer_data = question.send(:process_answer_data_for_choices, index: '3')

    expect(answer_data[:answer_text]).to be == '4'
    expect(answer_data[:answer_index]).to be == 3

    answer_data = question.send(:process_answer_data_for_choices, index: '10')

    expect(answer_data[:answer_text]).to be_nil
    expect(answer_data[:answer_index]).to be == 10

    answer_data = question.send(:process_answer_data_for_choices, text: '2')

    expect(answer_data[:answer_text]).to be == '2'
    expect(answer_data[:answer_index]).to be == 1

    answer_data = question.send(:process_answer_data_for_choices, text: '4')

    expect(answer_data[:answer_text]).to be == '4'
    expect(answer_data[:answer_index]).to be == 3

    answer_data = question.send(:process_answer_data_for_choices, text: '10')

    expect(answer_data[:answer_text]).to be == '10'
    expect(answer_data[:answer_index]).to be_nil
  end

  it 'must format answer for open question' do
    template = create(:question_template_type_open)
    question = build(:question, question_template: template)

    answer_data = question.send(:process_answer_data_for_open_question, 'Lorem Impsun')

    expect(answer_data[:answer_text]).to be == 'Lorem Impsun'
    expect(answer_data[:answer_index]).to be_nil
  end

  it 'must format answer data for question type' do
    template_choice = create(:question_template_type_single_choice, choices: ['1', '2', '3', '4'])
    question_choice = build(:question, question_template: template_choice)

    template_open = create(:question_template_type_open)
    question_open = build(:question, question_template: template_open)

    choice_answer_data = question_choice.format_data_for_answer('1')
    open_answer_data   = question_open.format_data_for_answer("Can't think, can't sleep, can't breathe")

    expect(choice_answer_data[:answer_text]).to be == '1'
    expect(choice_answer_data[:answer_index]).to be == 0

    expect(open_answer_data[:answer_text]).to be == "Can't think, can't sleep, can't breathe"
    expect(open_answer_data[:answer_index]).to be_nil

    choice_answer_data = question_choice.format_data_for_answer(index: '3')
    open_answer_data   = question_open.format_data_for_answer('Everybody got a target tonight')

    expect(choice_answer_data[:answer_text]).to be == '4'
    expect(choice_answer_data[:answer_index]).to be == 3

    expect(open_answer_data[:answer_text]).to be == 'Everybody got a target tonight'
    expect(open_answer_data[:answer_index]).to be_nil
  end

  it 'must allow manys answers by user' do
    template = create(:question_template_type_open)
    question = build(:question, question_template: template)
  end

  it 'must raise error when creating invalid answer' do
    template = create(:question_template_type_single_choice, choices: ['1', '2', '3'])
    question = create(:question, question_template: template)
    user     = create(:student_user)

    expect( -> { question.create_answer_for!(user, '5') } ).to raise_error(ActiveRecord::RecordInvalid)
  end

  it 'must raise error for invalid anwser create method' do
    question = build_stubbed(:question, question_template: build(:question_template_type_open))
    user     = build(:student_user)

    expect(-> { question.create_answer_for(user, 'LOL', :_create) }).to raise_error(ArgumentError)
  end

  it 'must not allow duplicate question title for resource and question_type' do
    question_template = create(:question_template)
    track    = create(:learning_track)

    question = build(:question_without_template, template_questionable: track, question_template: question_template)

    expect(question).to be_valid

    question.save

    invalid_question = build(:question_without_template, template_questionable: track, question_template: question_template)

    expect(invalid_question).not_to be_valid
    expect(invalid_question.errors[:base]).not_to be([])
    expect(invalid_question.errors[:base].join).to match(/Existent question/)
  end

end
