require 'rails_helper'

RSpec.describe Multimedia, type: :model do
  describe "Multimedia validations" do
    it { should validate_presence_of(:category)}
    it { should validate_presence_of(:user)}
  end
  describe "Multimedia associations" do
    it { belong_to(:category) }
    it { belong_to(:user) }
  end

  describe 'create Multimedia' do
    it 'can create with file' do
      item = build(:multimedia_with_image)
      expect(item.file.file.basename).to eq 'test'
      expect(item).to be_valid
    end
    it 'can create with link' do
      item = build(:multimedia_with_link)
      expect(item.link).to eq 'http://www.google.com'
    end
  end

  describe 'clone multimedia' do
    def is_a_copy a, b
      expect(a.name).to eq b.name
      expect(a.metadata).to eq b.metadata
      expect(a.category).to eq b.category
      expect(a.multimedia_type).to eq b.multimedia_type
      expect(a.status).to eq 'draft'
    end
    it 'can clone valid multimedia with file' do
      @orig = create(:multimedia_with_image)
      @orig.set_status(:published)
      dup = @orig.duplicate(@orig.user)
      is_a_copy dup, @orig
    end
    it 'can clone valid multimedia with link' do
      @orig = create(:multimedia_with_link)
      @orig.set_status(:published)
      dup = @orig.duplicate(@orig.user)
      is_a_copy dup, @orig
    end

  end

end
