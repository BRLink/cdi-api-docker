require 'rails_helper'

RSpec.describe LearningTrack, type: :model do
  before(:each) do
    @learning_track = create(:learning_track)
  end

  it 'must be valid' do
    learning_track = create(:learning_track)
    expect(learning_track).to be_valid
  end

  it 'must be invalid' do
    learning_track = build(:learning_track, learning_cycle_id: nil)

    expect(learning_track).not_to be_valid
    expect(-> { create(:learning_track, name: nil) }).to raise_error
  end

  it 'must have column indexes' do
    expect(subject).to have_db_index(:name)
    expect(subject).to have_db_index(:tags)
    expect(subject).to have_db_index(:learning_cycle_id)
    expect(subject).to have_db_index(:category_id)
  end

  it 'must belongs to user' do
    expect(@learning_track).to have_one(:user)
  end

  it 'must belongs to category' do
    expect(@learning_track).to belong_to(:category)
  end

  it 'must belongs to learning cycle' do
    expect(@learning_track).to belong_to(:learning_cycle)
  end

  it 'must have many tags' do
    max_tags   = LearningTrack::MAX_TAGS_COUNT
    tags_count = rand(1..max_tags)

    track = create(:learning_track_with_tags, tags_count: tags_count)

    expect(track.tags).to be_a(Array)
    expect(track.tags.size).to be tags_count
  end

  it 'must resolve alias association name' do
    expect(@learning_track.respond_to?(:cycle)).to be true
    expect(@learning_track.cycle).to be_a(LearningCycle)
  end

  it 'must limit the number of tags' do
    max_tags_count = LearningTrack::MAX_TAGS_COUNT

    track = create(:learning_track_with_tags, tags_count: max_tags_count * 2)

    expect(track.tags.size).to be max_tags_count
  end

  it 'must be searchable by single tag' do
    tags  = ['education', 'english', 'student']
    track = create(:learning_track, tags: tags)

    tracks = LearningTrack.search_by_tag('education')
    invalid_tracks = LearningTrack.search_by_tag('_education')

    expect(tracks.count).to be 1
    expect(tracks).to be_a(ActiveRecord::Relation)

    expect(invalid_tracks.count).to be 0
    expect(invalid_tracks).to be_a(ActiveRecord::Relation)

    10.times { |i|
      create(:learning_track, tags: tags.dup.push("tag_#{i.next}"))
    }

    expect(LearningTrack.search_by_tag('english').count).to be 11
    expect(LearningTrack.search_by_tag('tag_1').count).to be 1
    expect(LearningTrack.search_by_tag('tag_2').count).to be 1
  end


  it 'must be searchable by multiples tags' do
    tags  = ['education', 'english', 'student']
    track = create(:learning_track, tags: tags)

    tracks = LearningTrack.search_by_tags(['education', 'english'])

    expect(tracks.count).to be 1
    expect(tracks).to be_a(ActiveRecord::Relation)

    10.times { |i|
      create(:learning_track, tags: tags.dup.push("tag_#{i.next}"))
    }

    expect(LearningTrack.search_by_tags(['education', 'student']).count).to be 11
    expect(LearningTrack.search_by_tags(['tag_1','student']).count).to be 1
  end

  it 'must raise error in tags search' do
    expect(-> { LearningTrack.search_by_tag('tag1', 'tag2') }).to raise_error(ArgumentError)
    expect(-> { LearningTrack.search_by_tags('tag1', 'tag2') }).to raise_error(ArgumentError)

    expect(-> { LearningTrack.search_by_tag(['tag1', 'tag2']).count }).to raise_error(ActiveRecord::StatementInvalid)
  end

  it 'must has_and_belongs_to_many skills' do
    expect(subject).to have_and_belong_to_many(:skills)
  end

  it 'must limit the number of skills' do
    max_skills_count = LearningTrack::MAX_SKILLS_COUNT

    track = create(:learning_track_with_skills, skills_count: max_skills_count * 2)

    expect(track.skills.count).to be max_skills_count
  end

end
