require 'rails_helper'

RSpec.describe GameWordAnswer, type: :model do
  before(:each) do
    @answer = create(:game_word_answer)
  end

  def game_sample_data
    words = ['Napoleão Bonaparte', 'francês', '1804', '1814']
    sentence = '%s foi imperador %s, entre os periodos de %s à %s'

    { words: words, sentence: sentence }
  end

  it 'must be valid' do
    answer = build(:game_word_answer)

    expect(answer).to be_valid
  end

  it 'must set the correct flag based on words' do
    data = game_sample_data
    game = create(:game_complete_sentence, data)

    answer = create(:game_word_answer, words: data[:words], game_wordable: game)

    expect(answer.correct).to be == true
  end

  it 'belongs to user' do
    expect(@answer.user).not_to be_nil
    expect(@answer).to belong_to(:user)
  end

  it 'has columns indexes' do
    expect(subject).to have_db_index(:game_wordable_id)
    expect(subject).to have_db_index(:game_wordable_type)
    expect(subject).to have_db_index(:correct)
    expect(subject).to have_db_index(:words)
    expect(subject).to have_db_index([:user_id, :game_wordable_id, :game_wordable_type])
  end

  it 'must be invalid' do
    answer = build(:game_word_answer, game_wordable: nil)

    expect(answer).to_not be_valid
    expect(answer.errors[:game_wordable]).to_not be eq([])
  end

  it 'must set correct to false when no words is provided' do
    answer = create(:game_word_answer, words: [])

    expect(answer.correct).to be == false
  end

  it 'dont allow empty words when game has words' do
    data = game_sample_data

    game = create(:game_complete_sentence, data)
    answer = build(:game_word_answer, words: data[:words] * 2, game_wordable: game)

    expect(answer).not_to be_valid
  end
end
