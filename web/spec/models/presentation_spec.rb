require 'rails_helper'

RSpec.describe Presentation, type: :model do
  it 'creates presentation' do
    p = create(:presentation_with_slides)
    expect(p).to be_valid
    expect(p.presentation_slides.size).to eq(5)
  end

  it 'create multiplier dynamic presentation' do
    p = create(:multiplier_presentation)
    expect(p).to be_valid
    expect(p.slides.size).to eq(1)
    expect(p.slides.first.slide_type).to eq('dynamic')
  end

  describe 'clone presentation' do
    before(:each) do
      @orig = create(:presentation_with_slides, slides_count: 3)
    end
    it 'can clone valid presentation' do
      duplicated = @orig.duplicate(@orig.user)
      expect(duplicated).to be_valid
      expect(duplicated.slides.size).to eq(3)
      duplicated.slides.each do |slide|
        expect(slide).to be_valid
      end
    end
    it 'can capture slide clone errors' do
      @orig.slides.first.update_column(:content, nil)
      dup = @orig.duplicate(@orig.user)
      expect(dup.errors.key?("slide_0_content".to_sym)).to be true
    end
  end
end

