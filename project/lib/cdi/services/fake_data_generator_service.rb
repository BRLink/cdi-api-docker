module CDI
  module Services
    class FakeDataGeneratorService
      def initialize(options={})
        @verbose = options.fetch(:verbose, true)
        @silent  = options.fetch(:silent, true)
        @actions = options.delete(:actions) || []
        @options = options
      end

      def execute
        @actions.each do |action|
          self.send(action)
        end
      end

      private
      def create_enrolled_tracks
        StudentClass.find_each do |student_class|
          student_class.learning_tracks.find_each do |track|
            student_class.students.find_each do |student|

              service = CDI::V1::Users::EnrollTrackService.new(student, student_class, learning_track_id: track.id)
              service.execute

              puts service.success?
              puts service.errors if service.errors
            end
          end
        end
      end

      def create_enrolled_tracks_by_student
        # must use find_each
        User.student.joins(:student_classes).order('random()').each do |student|
          # must use find_each
          student.student_classes.order('random()').each do |student_class|

            total_tracks = student_class.learning_tracks.count
            index = 0

            student_class.learning_tracks.find_each do |track|
              params = {
                learning_track_id: track.id,
                status: ::EnrolledLearningTrack::STATUS.values.sample,
                allow_status_change: true,
                validate: false
              }

              service = CDI::V1::Users::EnrollTrackService.new(student, student_class, params)
              service.execute

              print "\r>> #{index = index.next}/#{total_tracks}"
            end
          end
        end
      end

      def create_tracks_reviews_by_student
        fallback_student_class = StudentClass.first
        default_student_class = nil

        if student_class_id = ENV['CLASS_ID']
          fallback_student_class = StudentClass.find_by(id: student_class_id)
          default_student_class = fallback_student_class
        end

        # must use find_each
        User.student.joins(:student_classes).order('random()').each do |student|
          if track_id = ENV['TRACK_ID']
            tracks = LearningTrack.where(id: track_id)
          else
            tracks = LearningTrack.limit(200).order('random()').all
          end

          total_tracks = tracks.size

          tracks.each_with_index do |track, index|
            track.student_classes << fallback_student_class rescue false

            student_class = default_student_class || track.student_classes.order('random()').first

            params = {
              review: {
                learning_track_id: track.id,
                student_class_id: student_class.id,
                review_type: ::LearningTrackReview::REVIEWS_TYPES.keys.sample,
                comment: ::Faker::Lorem.paragraph(5)
              }
            }

            student.enrolled_learning_tracks.create(learning_track: track, student_class: student_class) rescue false

            service = CDI::V1::LearningTrackReviews::CreateService.new(student, params)
            service.execute

            print "\r>> #{index.next}/#{total_tracks}"
          end
        end
      end

      def create_students
        create_users(:student)
      end

      def create_multipliers
        multiplier_type = ENV['PROFILE_TYPE']
        create_users(:multiplier, profile_type: multiplier_type)
      end

      def create_game_quizzes
        create_games(:game_quiz)
      end

      def create_game_polls
        create_games(:game_poll)
      end

      def create_learning_tracks
        generator = get_class('learning_track')
        dispatch(generator)
      end

      def create_learning_cycles
        generator = get_class('learning_cycle')
        dispatch(generator)
      end

      def create_answers_for_game_poll
        ENV['GAME_TYPE'] = 'game_poll'

        create_answer
      end

      def create_answers_for_game_quiz
        ENV['GAME_TYPE'] = 'game_quiz'

        create_answer
      end

      def create_answer(options = {})
        game_id      = ENV['GAME_ID']
        game_type    = ENV['GAME_TYPE']
        option_index = ENV['OPTION_INDEX']
        users_limit  = ENV['USERS_LIMIT']
        users_offset  = ENV['USERS_OFFSET']

        options.merge!(
          game_type: game_type,
          game_id: game_id,
          option_index: option_index,
          users_limit: users_limit,
          users_offset: users_offset
        )

        generator = get_class('game_question_answer')
        dispatch(generator, options)
      end

      def create_games(game_type, options = {})
        total_questions = ENV['TOTAL_QUESTIONS']
        total_options   = ENV['TOTAL_OPTIONS']

        options.merge!(
          total_questions: total_questions,
          total_options: total_options,
          game_type: game_type
        )

        generator = get_class("games/#{game_type}")
        dispatch(generator, options)
      end

      def create_users(profile_type, options = {})
        generator = get_class("users/#{profile_type}")
        dispatch(generator, options.reverse_merge(profile_type: profile_type))
      end

      def dispatch(klass, options={})
        options = @options.merge(options)

        generator = klass.new(options)
        generator.create

        generator
      end

      def get_class(base_name)
        get_class_name(base_name).constantize
      end

      def get_class_name(base_name)
        "CDI/fake_data/generators/#{base_name}".camelize
      end
    end
  end
end
