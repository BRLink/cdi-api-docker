module CDI
  module CacheExtensions
    module KeyEncoders
      module Marshal

        def encoded_key_value(value)
          ::Marshal.dump(value.to_s)
        end

        def decoded_key_value(value)
          ::Marshal.load(value.to_s) rescue value.to_s
        end

      end
    end
  end
end
