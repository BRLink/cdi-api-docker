module CDI
  module CacheExtensions
    module KeyEncoders
      module Base64

        def encoded_key_value(value)
          ::Base64.encode64(value.to_s)
        end

        def decoded_key_value(value)
          ::Base64.decode64(value.to_s)
        end

      end
    end
  end
end
