require_relative '../base'

module CDI
  module FakeData
    module Generators
      class GameQuestionAnswer < Base

        attr_reader :game_type, :game_id

        def initialize(options = {})
          super(options)
          # game options
          @game_type = options.fetch(:game_type, :game_quiz)
          @game_id   = options.fetch(:game_id, nil)
          @user_id   = options.fetch(:user_id, nil)

          # user options
          @users_limit  = options.fetch(:users_limit, nil)
          @users_offset = options.fetch(:users_offset, nil)
          @option_index = options.fetch(:option_index, false)
        end

        def create
          run do
            create_answers(total_amount)
          end
        end

        def create_answers(amount)

          if game.blank?
            return info "Jogo #{game_type}/ID: #{game_id || 'Unknown'} não encontrado. Abortando criação de respostas!"
          end

          info "\nCriando respostas para o jogo: #{game_type}/ID: #{game_id}"

          amount.to_i.times do |index|

            questions = game_questions
            users     = users_for_answers

            info "\nTotal de usuários: #{users.size}"
            info "\nCriando respostas para #{questions.size} questões"

            questions.each_with_index do |question, index|
              print_status index.next, questions.size

              users.each do |user|
                service = CDI::V1::GameQuestionsAnswers::CreateService.new(user, data_for_service(question))
                service.execute

                info "\n#{service.errors}" unless service.success?
              end
            end

            print_status index.next, amount

            advance_to_next_iteration
          end
        end

        def users_for_answers
          if @user_id.present?
            users = ::User.find_by(id: @user_id)
          else
            users = User.student.all.limit(@users_limit).offset(@users_offset)
          end

          Array.wrap(users)
        end

        def game
          @game ||= find_game
        end

        def find_game
          return nil unless [game_type, game_id].all?(&:present?)
          game_type.to_s.camelize.constantize.find_by(id: game_id)
        end

        def game_questions
          Array.wrap(game_type.to_sym == :game_poll ? game.question : game.questions)
        end

        def option_id_for_question(question)
          if @option_index.present?
            option = question.options.at(@option_index.to_i)
          else
            option = question.options.sample
          end

          option.id
        end

        def data_for_service(question)
          answer_data = answer_data(@current_index, question)

          {
            async: false,
            answer: answer_data,
            origin: fake_origin_data,
            delivery_email: false
          }
        end

        def game_type_for_service
          @game_type.to_s.sub('game_', '')
        end

        def answer_data(index, question)
          {
            question_id: question.id,
            option_id: option_id_for_question(question),
            game_type: game_type_for_service,
            game_id: @game_id
          }
        end

      end
    end
  end
end
