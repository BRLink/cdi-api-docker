require_relative '../base'

module CDI
  module FakeData
    module Generators
      class LearningTrack < Base
        def initialize(options = {})
          super(options)
        end

        def create
          run do
            create_tracks(total_amount)
          end
        end

        def create_tracks(amount)

          info "\nCriando #{amount} tracks\n"

          amount.to_i.times do |index|
            track_data = track_data(@current_index)

            options = {
              async: false,
              learning_track: track_data,
              origin: fake_origin_data,
              delivery_email: false
            }

            service = ::CDI::V1::LearningTracks::CreateService.new(owner_user, options)
            service.execute

            info service.errors unless service.success?

            print_status index.next, amount

            advance_to_next_iteration
          end
        end

        def track_data(index)
          {
            name: faker_company.name,
            description: fake_paragraph,
            category_id: Category.pluck(:id).sample,
            learning_cycle_id: learning_cycle_id(index),
            skills: Skill.unscoped.order("random()").pluck(:id).sample(3),
            tags: Faker::Lorem.words(3)
          }
        end

        def learning_cycle_id(index)
          owner_user.learning_cycles.offset(index).first.id
        end
      end
    end
  end
end
