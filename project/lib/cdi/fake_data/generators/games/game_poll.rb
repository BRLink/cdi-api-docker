require_relative '../user'

module CDI
  module FakeData
    module Generators
      module Games
        class GamePoll < Game

          def initialize(options = {})
            super(options)
            @game_type = :game_poll
            @total_questions = 1
          end

          def sample_game_data(index, game_type = nil)
            {
              question: create_questions
            }
          end

          def create_option_data
            {
              text: fake_paragraph(1),
              correct: false
            }
          end

          def has_correct_option?
            false
          end

        end
      end
    end
  end
end
