require_relative '../base'

module CDI
  module FakeData
    module Generators
      class LearningCycle < Base
        def initialize(options = {})
          super(options)
        end

        def create
          run do
            create_cycles(total_amount)
          end
        end

        def create_cycles(amount)

          info "\nCriando #{amount} LearningCycles\n"

          amount.to_i.times do |index|
            cycle_data = cycle_data(@current_index)

            options = {
              async: false,
              learning_cycle: cycle_data,
              origin: fake_origin_data,
              delivery_email: false
            }

            service = ::CDI::V1::LearningCycles::CreateService.new(owner_user, options)
            service.execute

            info service.errors unless service.success?

            print_status index.next, amount

            advance_to_next_iteration
          end
        end

        def cycle_data(index)
          {
            name: "#{faker_company.name} #{index}",
            category_id: Category.pluck(:id).sample,
            objective_text: fake_paragraph,
            tags: Faker::Lorem.words(3)
          }
        end
      end
    end
  end
end
