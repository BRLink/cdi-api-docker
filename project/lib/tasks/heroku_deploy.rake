require_relative '../rake_heroku_deployer'

namespace :deploy do
	namespace :staging do
		task :migrations do
			deployer = RakeHerokuDeployer.new(:staging)
			deployer.run_migrations
		end

		task :rollback do
			deployer = RakeHerokuDeployer.new(:staging)
			deployer.rollback
		end
	end

	task :staging do
		deployer = RakeHerokuDeployer.new(:staging)
		deployer.deploy
	end

	namespace :production do
		task :migrations do
			deployer = RakeHerokuDeployer.new(:production)
			deployer.run_migrations
		end

		task :rollback do
			deployer = RakeHerokuDeployer.new(:production)
			deployer.rollback
		end
	end

	task :production do
		deployer = RakeHerokuDeployer.new(:production)
		deployer.deploy
	end

	task :all do
		Rake::Task['deploy:staging'].invoke
		Rake::Task['deploy:production'].invoke
	end
end
