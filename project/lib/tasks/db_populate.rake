namespace :db do

	desc "Populate database with fake data"
	task :populate => :environment do

		default_actions = [:create_students, :create_multipliers]
		default_options = { total_amount: (ENV['TOTAL'] || 1).to_i }
		actions         = (ENV['ACTIONS'] || "").split(",").map(&:squish).map(&:to_sym)
		options         = default_options.merge(actions: actions.present? ? actions : default_actions)

		service = CDI::Services::FakeDataGeneratorService.new(options)
		service.execute
	end

	namespace :populate do
		task :all => :environment do
			ENV['ACTIONS'] = "create_students"

			Rake::Task["db:populate"].reenable
			Rake::Task["db:populate"].invoke
		end
	end
end
