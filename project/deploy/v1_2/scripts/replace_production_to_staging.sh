# Replica do ambiente de produção para staging
# PASSWORD m4k3u557r0n63r-57461n6

export DB_HOST="tec-escola-api-staging-database.cxf65fi89iab.us-east-1.rds.amazonaws.com"
export DB_NAME="cdi_api_staging"
export DB_USER="tecescola_stg"
export BACKUP_FILE="db_backup/production-2016-02-24-12-27.dump"

echo "drop schema public cascade; create schema public;" | psql -U $DB_USER -d $DB_NAME -h $DB_HOST -W
pg_restore -U $DB_USER -d $DB_NAME -h $DB_HOST --no-owner --verbose $BACKUP_FILE
RAILS_ENV=staging bundle exec rake db:migrate
RAILS_ENV=staging bundle exec rake db:seed ACTIONS=create_default_subscriptions,fix_categories,fix_supporting_courses_presentation_publishment

