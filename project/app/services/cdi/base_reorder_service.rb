module CDI
  class BaseReorderService < BaseUpdateService

    private
    def record_params
      @options.slice(:position)
    end

    def user_can_update?
      unless valid_position?
        return forbidden_error!("#{record_error_key}.invalid_position_number")
      end
      super
    end

    def update_record
      @record.set_list_position(position_number)
      @record
    end

    def valid_position?
      return false if @options[:position].blank?
      return @options[:position].to_i > 0
    end

    def position_number
      last_position = @record.class.maximum(:position)
      position = @options[:position].to_i

      [1, [last_position, position].min, [position, last_position].min].max
    end

  end
end
