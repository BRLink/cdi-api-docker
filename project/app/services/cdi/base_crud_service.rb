module CDI
  class BaseCrudService < BaseService

    include ::CDI::ServiceConcerns::Cacheable

    attr_reader :record

    def self.record_type(record_type, options={})
      record_alias = options.delete(:alias_name) || record_type.to_s.underscore

      alias_method record_alias.to_sym, :record

      class_eval %{
        def record_type
          #{record_type}
        end
      }
    end

    def self.response_options(_options = {})
      class_eval %{
        def response_options
          #{
            default_options = {
              success_code: 200
            }.merge(_options).symbolize_keys
          }
        end
      }
    end

    def initialize(record, user, options={})
      @record = record
      @user   = user

      super(options)
    end

    def execute
      not_implemented_exception(__method__)
    end

    def record_type
      not_implemented_exception(__method__)
    end

    private

    def record_params
      not_implemented_exception(__method__)
    end

    def record_error_key
      record_type.to_s.pluralize.underscore
    end

    def valid_record?
      valid_object?(@record, record_type)
    end

    def duplicate_record_in_interval?(relation, interval = 10.seconds, attributes = {})
      return false if CDI::Config.enabled?(:allow_duplicated_resources_titles)

      record = relation.where(attributes).last

      return false if record.blank?

      (record.created_at + interval) > Time.zone.now
    end

    def redis_setnx(key, ttl=60)
      begin
        if key_created = CDI.redis_client.setnx(key, "true")
          CDI.redis_client.expire(key, ttl)
        end

        return key_created
      rescue Exception => e
        Rollbar.error(e)
      end

      # if any error ocurr
      return true
    end

    def replace_data_for_cache_key(key)
      {
        user_id: @user.id,
        resource_id: @record.id
      }
    end

    def clear_cache_for_request?
      return false if ["false", false].member?(@options[:_clear_cache])

      return true
    end

  end
end
