module CDI
  class BaseCreateService < BaseCrudService

    response_options success_code: 201

    DEFAULT_REDIS_KEY_TTL = 5

    def initialize(user, options={})
      @user = user
      super(nil, user, options)
    end

    def execute
      if can_execute_action?
        @record = build_record

        if save_record
          after_execute_success_response
          expire_cache_for_resource
          create_origin_for_record
        else
          errors = create_error_response(@record)
          after_error_response(errors)
        end
      end

      success?
    end

    private
    def after_execute_success_response
      success_response(response_options[:success_code] || 201)
    end

    def create_error_response(record)
      record.errors
    end

    def after_error_response(errors)
      bad_request_error(errors) if errors.present?
    end

    def save_record
      @record.save
    end

    def after_success
    end

    def create_origin_for_record
      if @record.respond_to?(:create_origin)
        create_origin_async(@record, @options)
      end
    end

    def build_record
      if record_type.present? && record_type.respond_to?(:model_name)
        return build_from_record_type
      end

      return not_implemented_exception(__method__)
    end

    def record_type_params
      params_method = "#{record_type.model_name.param_key}_params"
      if self.respond_to?(params_method, true)
        return self.send(params_method)
      end
    end

    def build_from_record_type
      if record_type_params.present?
        record_type.send(:new, record_type_params)
      end
    end

    def can_execute_action?
      if validate_ip_on_create? && !can_create_with_ip?
        return forbidden_error!(:ip_temporarily_blocked)
      end

      unless valid_user?
        return not_found_error!('users.not_found')
      end

      unless can_create?
        if @errors.blank?
          return forbidden_error!("#{record_error_key}.user_cant_create")
        else
          return false
        end
      end

      return valid?
    end

    def can_create?
      return false unless valid_user?
      return false unless can_create_record?

      return true
    end

    def can_create_record?
      return false
    end

    def validate_ip_on_create?
      false
    end

    def redis_ip_key
      ip = @options[:origin].try(:[], :ip) || @options[:origin_ip]
      return nil unless ip

      @_redis_ip_key ||= Digest::MD5.hexdigest ip
    end

    def can_create_with_ip?
      redis_setnx("create_#{record_type.to_s.pluralize.underscore}_ip_#{redis_ip_key}", default_redis_ttl)
    end

    def default_redis_ttl
      (create_redis_ttl || CDI::Config.contact_form_redis_key_ttl).to_i
    end

    def create_redis_ttl
      DEFAULT_REDIS_KEY_TTL
    end

    def needs_cache_expiration?
      success?
    end

  end
end
