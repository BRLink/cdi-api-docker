module CDI
  module V1
    module SurveyQuestionOptions
      class DeleteService < BaseDeleteService

        record_type ::SurveyQuestionOption
      end
    end
  end
end
