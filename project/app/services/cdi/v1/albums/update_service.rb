# encoding: UTF-8

module CDI
  module V1
    module Albums
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::AlbumParams

        record_type ::Album

        def record_params
          album_params
        end

        def after_success
          if default_album_setted?
            @user.default_album = @record
          end
        end

      end
    end
  end
end
