module CDI
  module V1
    module Comments
      class UpdateService < BaseUpdateService

        record_type ::Comment

        def record_params
          filter_hash(@options[:comment], [:content])
        end
      end
    end
  end
end
