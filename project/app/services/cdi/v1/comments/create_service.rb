# encoding: UTF-8

module CDI
  module V1
    module Comments
      class CreateService < BaseCreateService

        record_type ::Comment

        attr_reader :resource

        WHITELIST_ATTRIBUTES = [:content]

        def initialize resource, user, options={}
          super user, options
          @resource = resource
        end

        def can_create_record?
          if @resource.nil?
            return not_found_error!('comments.resource_not_found')
          end

          if has_parent? && !valid_parent?
            return not_found_error!('comments.parent_not_found')
          end

          @resource.user_can_create_comment?(@user)
        end

        def build_record
          @comment = @user.comments.build(comment_params)
        end

        def after_success
          notify_users if @resource.respond_to? :user
        end

        def notify_users
          if @resource.user != @comment.user
            create_system_notification_async(
              sender_user: @comment.user,
              receiver_user: @resource.user,
              notification_type: :"new_comment_on_#{@resource.class.to_s.underscore}",
              notificable: @comment
            )
          end
        end

        def comment_params
          return @comment_params if @comment_params
          @comment_params = filter_hash(attributes_hash, WHITELIST_ATTRIBUTES)
          @comment_params[:resource] = @resource
          if has_parent?
            @comment_params[:parent_id] = parent_id
          end
          @comment_params
        end

        def has_parent?
          attributes_hash[:parent_id].present?
        end

        def valid_parent?
          @parent_comment = Comment.find_by(id: attributes_hash[:parent_id])
          @parent_comment.present?
        end

        def parent_id
          @parent_comment.parent_id || @parent_comment.id
        end

        def attributes_hash
          @options[:comment]
        end
      end
    end
  end
end
