# encoding: UTF-8

module CDI
  module V1
    module EducationalInstitutions
      class ProfileImageUpdateService < BaseUpdateService

        record_type ::EducationalInstitution

        include V1::ServiceConcerns::UserParams

        private

        def record_params
          @record_params ||= set_image_crop_params(filter_hash(@options[:institution], [:profile_image]))
        end

        def attributes_hash
          @options[:institution]
        end
        def educational_institution
          @record
        end
      end
    end
  end
end
