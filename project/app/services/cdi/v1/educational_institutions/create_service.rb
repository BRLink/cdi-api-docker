# encoding: UTF-8

module CDI
  module V1
    module EducationalInstitutions
      class CreateService < BaseCreateService

        record_type ::EducationalInstitution

        include V1::ServiceConcerns::EducationalInstitutionsParams

        private
        def can_create_record?
          return false unless valid_user?

          # Only (admin, of course) and multiplier users can create institituions
          @user.backoffice_profile?
        end

        def build_record
          params  = educational_institution_params
          address = params.delete(:address)

          institution = @user.owned_educational_institutions.build(params)
          institution.build_address(address) if address

          institution
        end

        # handle avatar, cover images
        def after_success
          update_profile_image if institution_hash.has_key?(:profile_image)
          update_profile_cover_image if institution_hash.has_key?(:profile_cover_image)
          @record.reload
        end
        def update_profile_image
          service = EducationalInstitutions::ProfileImageUpdateService.new(@record, @user, @options)
          service.execute
        end
        def update_profile_cover_image
          service = EducationalInstitutions::ProfileCoverImageUpdateService.new(@record, @user, @options)
          service.execute
        end
        def institution_hash
          @options[:institution] ||  @options[:educational_institution]
        end
      end
    end
  end
end
