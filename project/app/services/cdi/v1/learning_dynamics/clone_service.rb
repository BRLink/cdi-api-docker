# encoding: UTF-8

module CDI
  module V1
    module LearningDynamics
      class CloneService < Libraries::CloneService
        record_type ::LearningDynamic
      end
    end
  end
end