# encoding: UTF-8

module CDI
  module V1
    module LearningTracks
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::LearningTrackParams

        record_type ::LearningTrack

        MAX_TRACKS_BY_CYCLE = (CDI::Config.max_tracks_by_cicle_limit || 20).to_i

        private
        def can_create_record?
          unless valid_learning_cycle?
            return not_found_error!('learning_cycles.not_found')
          end

          if cycle_tracks_limit_reached?
            return forbidden_error!('learning_cycles.tracks_limit_reached')
          end

          return @user.backoffice_profile?
        end

        def build_record
          learning_cycle.learning_tracks.build(learning_track_params)
        end

        def learning_cycle
          @learning_cycle ||= @user.learning_cycles.find_by(id: learning_track_params[:learning_cycle_id])
        end

        def valid_learning_cycle?
          valid_object?(learning_cycle, ::LearningCycle)
        end

        ### Final step
        def after_success
          create_or_update_answers_for_questions
          notify_users

          #sync cycle status
          learning_cycle.refresh_status
        end

        def notify_users
          # delivery_async_email(LearningTracksMailer, :new_track, @record)
        end

        def cycle_tracks_limit_reached?
          return false unless valid_learning_cycle?

          learning_cycle.learning_tracks.count >= MAX_TRACKS_BY_CYCLE
        end

        def cache_keys_to_expire_after_success
          %w(current_user.show) # /users/me
        end
      end
    end
  end
end
