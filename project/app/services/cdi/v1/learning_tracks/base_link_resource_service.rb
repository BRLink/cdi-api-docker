# encoding: UTF-8

module CDI
  module V1
    module LearningTracks
      class BaseLinkResourceService < PresentationSlides::AssociateResourceService

        private

        def user_can_execute_action?
          unless valid_learning_track?
            return not_found_error!('learning_tracks.not_found')
          end

          user_can_associated_resource?
        end

        def user_can_associated_resource?
          not_implemented_exception(__method__)
        end

        def overwrite?
          @options[:overwrite].present? && @options[:overwrite].to_s == "true"
        end

        def learning_track
          @learning_track ||= @user.learning_tracks.find_by(id: learning_track_id)
        end

        def presentation
          return nil unless valid_learning_track?
          learning_track.presentation || learning_track.create_presentation
        end

        def learning_track_id
          @options[:learning_track_id] || (@options[:dynamic] && @options[:dynamic].fetch(:learning_track_id, nil))
        end

        def valid_learning_track?
          valid_object?(learning_track, ::LearningTrack)
        end

        def valid_record?
          true
        end

      end
    end
  end
end
