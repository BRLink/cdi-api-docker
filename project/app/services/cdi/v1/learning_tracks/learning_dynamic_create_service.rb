# encoding: UTF-8

module CDI
  module V1
    module LearningTracks
      class LearningDynamicCreateService < LearningDynamics::CreateService

        include V1::ServiceConcerns::LearningDynamicParams

        record_type ::LearningDynamic

        private
        def can_create_record?
          return false unless valid_user?

          unless valid_learning_track?
            return not_found_error!('learning_tracks.not_found')
          end

          return @user.backoffice_profile?
        end

        def after_success
          @slide_service ||= create_presentation_slide_with_service
        end

        def create_presentation_slide_with_service
          options = @options.dup.merge!(
            slide: {
              presentation_id: presentation.try(:id),
              slide_type: PresentationSlide::SLIDE_TYPES[:dynamic],
              content: {},
              metadata: {},
            }
          )

          service = PresentationSlides::CreateService.new(@user, options)
          service.execute

          slide = service.presentation_slide

          slide.learning_dynamic = @record
          slide.save
        end

        def learning_track
          @learning_track ||= @user.learning_tracks.find_by(id: learning_track_id)
        end

        def presentation
          return nil unless valid_learning_track?
          learning_track.presentation || learning_track.create_presentation
        end

        def learning_track_id
          @options[:dynamic].fetch(:learning_track_id, nil)
        end

        def valid_learning_track?
          valid_object?(learning_track, ::LearningTrack)
        end
      end
    end
  end
end
