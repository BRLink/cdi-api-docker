# encoding: UTF-8

module CDI
  module V1
    module LearningTracks
    class GameLinkService < BaseLinkResourceService

        action_name :link_game

        VALID_GAMES = [
          GameCompleteSentence,
          GameQuiz,
          GamePoll,
          GameOpenQuestion,
          GameTrueFalse,
          GameWordSearch,
          GameRelatedItem,
          GameGroupItem
        ]

        def user_can_associated_resource?
          unless valid_game?
            return not_found_error!("#{record_error_key}.not_found")
          end

          return @user.backoffice_profile?
        end

        def valid_game?
          VALID_GAMES.any? { |game| valid_object?(@resource, game) }
        end

        def record_error_key
          return :games if @resource.blank?
          @resource.class.to_s.underscore.pluralize
        end

        def slide_type_for_resource
          PresentationSlide::SLIDE_TYPES[:game]
        end

      end
    end
  end
end
