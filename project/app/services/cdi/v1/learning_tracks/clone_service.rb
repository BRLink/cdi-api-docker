module CDI
  module V1
    module LearningTracks
      class CloneService < BaseActionService

        attr_reader :learning_track, :cloned_learning_track

        action_name :clone

        def initialize(learning_track, user, options={})
          @learning_track = learning_track
          @user = user

          super(options)
        end

        def execute_action
          unless learning_cycle
            return not_found_error('learning_cycles.not_found')
          end

          @cloned_learning_track = @learning_track.clone!(learning_cycle)
          unless @cloned_learning_track.persisted?
            return forbidden_error(@cloned_learning_track.errors)
          end

          success_created_response
        end

        def user_can_execute_action?
          @learning_track.user_can_clone?(@user)
        end

        def learning_cycle
          @user.learning_cycles.find_by(id: @options[:learning_cycle_id])
        end

        def valid_record?
          valid_object?(@learning_track, ::LearningTrack)
        end

        def record_error_key
          :learning_tracks
        end

        def after_success
          #sync cycle status
          learning_cycle.refresh_status
        end
      end
    end
  end
end
