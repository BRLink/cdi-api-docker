module CDI
  module V1
    module Users
      class FacebookUnlinkService < BaseActionService

        include V1::ServiceConcerns::OauthParams

        attr_reader :user

        action_name :facebook_unlink_account

        def initialize(user, options = {})
          @user = user
          super(options)
        end

        def execute_action
          begin
            @user.reset_oauth_provider!
          rescue Exception => e
            bad_request_error(e.message)
            Rollbar.error(e)
          end
        end

        def after_success
          @record = @user
        end

        def action_errors
          @errors
        end

        def success_runned_action?
          @user.oauth_provider_uid.blank?
        end

        def user_can_execute_action?
          unless @user.from_oauth_provider?
            return forbidden_error!('users.facebook_not_linked')
          end

          return true
        end

        def valid_record?
          valid_user?
        end

        def record_error_key
          :users
        end

        def cache_keys_to_expire_after_success
          %w(current_user.show) # /users/me
        end

        def replace_data_for_cache_key(key)
          {
            user_id: @user.id
          }
        end
      end
    end
  end
end
