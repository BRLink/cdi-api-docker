# encoding: UTF-8

module CDI
  module V1
    module Users
      class PasswordUpdateService < BaseService

        include ::CDI::ServiceConcerns::Cacheable

        attr_reader :user

        def initialize(user, options={})
          super(options)
          @user = user
        end

        def execute
          if can_execute_action?
            if @user.update_password(new_password_value)
              success_response
              expire_cache_for_resource
            else
              bad_request_error(@user.errors)
            end
          end
        end

        private
        def cache_keys_to_expire_after_success
          %w(current_user.show) # /users/me
        end

        def replace_data_for_cache_key(key)
          {
            user_id: @user.id
          }
        end

        def needs_cache_expiration?
          success?
        end

        def new_password_value
          @options[:password]
        end

        def can_execute_action?
          unless valid_token?
            return not_found_error!('users.invalid_password_update_token')
          end

          unless valid_user?
            return not_found_error!('users.not_found')
          end

          unless confirmation_match?
            return bad_request_error!('users.password_combination_dont_match')
          end

          return true
        end

        def valid_token?
          return false if @options[:token].blank?

          @token ||= User.find_by(reset_password_token: @options[:token]).try(:reset_password_token)

          return @token.present? && @user.try(:reset_password_token) == @token
        end

        def confirmation_match?
          return false if @options[:password].blank? || @options[:password_confirmation].blank?
          return @options[:password] == @options[:password_confirmation]
        end

        def after_success
          send_password_updated_email
        end

        def send_password_updated_email
          delivery_async_email(UsersMailer, :password_updated, @user)
        end

        def clear_cache_for_request?
          return false if ["false", false].member?(@options[:_clear_cache])

          return true
        end
      end
    end
  end
end
