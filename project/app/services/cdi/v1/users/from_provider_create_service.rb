# encoding: UTF-8

module CDI
  module V1
    module Users
      class FromProviderCreateService < Users::CreateService

        VALID_PROVIDERS = [
          :facebook
        ]

        attr_reader :access_token, :provider

        def initialize(access_token, provider, options = {})
          @access_token = access_token
          @provider     = provider
          super(nil, options)
        end

        def new_user?
          return false if @auth_class.nil?
          !@auth_class.existent_user?
        end

        private
        def build_record
          method_to_call = "create_user_from_#{provider.to_s.underscore}"

          # must return a User instance
          send(method_to_call) if self.respond_to?(method_to_call, true)
        end

        def create_user_from_facebook
          @auth_class = AuthProviders::Facebook.new(@access_token)
          @user = @auth_class.fetch_user

          if errors = @auth_class.errors
            error = (errors.flatten.first || { 'message' => 'Unknown error' }).symbolize_keys
            bad_request_error(error[:message], no_i18n: true)
          else
            @user.oauth_provider_uid = @auth_class.oauth_provider_uid
            @user.oauth_provider = @auth_class.provider_name
          end

          @user
        end

        def after_execute_success_response
          new_user? ? success_created_response : success_response
        end

        def can_create_record?
          return false if @access_token.blank?

          unless valid_service_oauth_provider?
            return bad_request_error!('users.invalid_service_oauth_provider')
          end

          unless valid_authentication_provider?
            return bad_request_error!('users.invalid_authentication_provider')
          end

          return true
        end

        def valid_service_oauth_provider?
          return false if @provider.blank?

          VALID_PROVIDERS.member?(@provider.to_sym)
        end

        def valid_authentication_provider?
          return false if @options[:provider].blank?

          Authorization::PROVIDERS.values.member?(@options[:provider].to_s)
        end

        ### Final step
        def after_success
          fetch_avatar_from_provider
          create_authorization
          execute_async_actions
        end

        def create_error_response(record)
          return {} unless @errors.blank?
          super(record)
        end

      end
    end
  end
end
