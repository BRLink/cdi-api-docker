# encoding: UTF-8

module CDI
  module V1
    module Users
      class ProviderAuthenticationCreateService < AuthenticationService

        VALID_PROVIDERS = [
          :facebook
        ]

        attr_reader :access_token, :oauth_provider

        def initialize(access_token, provider, options = {})
          @access_token  = access_token
          @auth_provider = provider
          @user = @auth_token = nil
          super(nil, nil, options)
        end

        def execute_action
          method_to_call = "fetch_user_from_#{@auth_provider.to_s.underscore}"

          # must return a User instance
          send(method_to_call) if self.respond_to?(method_to_call, true)
        end

        def new_user?
          return false if @auth_class.nil?
          !@auth_class.existent_user?
        end

        private
        def fetch_user_from_facebook
          @auth_class = AuthProviders::Facebook.new(@access_token)
          @auth_class.fetch_user
        end

        def can_execute_action?
          return false if @access_token.blank?

          unless valid_service_oauth_provider?
            return bad_request_error!('users.invalid_service_oauth_provider')
          end

          super
        end

        def valid_service_oauth_provider?
          return false if @auth_provider.blank?

          VALID_PROVIDERS.member?(@auth_provider.to_sym)
        end

        def valid_user?
          @user.try(&:persisted?)
        end

        def invalid_credentials_error
          'invalid_user'
        end

      end
    end
  end
end
