# encoding: UTF-8

module CDI
  module V1
    module Users
      module AuthProviders
        class Facebook

          attr_reader :access_token, :options, :auth_provider_data, :errors, :existent_user

          PROVIDER_ATTRIBUTES = [:email, :first_name, :last_name, :gender, :birthday, :about]

          def initialize(access_token, options = {})
            @access_token = access_token
            @options = options.to_options!
          end

          def fetch_user
            user_data = memoized_fetch_user_data || {}

            find_user(user_data)
          end

          def find_user(user_data)
            @existent_user = find_existent_user(user_data)
            if @existent_user.present? and @existent_user.banned?
              @existent_user = nil
              return
            end
            @existent_user || ::User.new(user_data)
          end

          def find_existent_user(provider_data)
             oauth_provider_uid = provider_data[:oauth_provider_uid]

             return nil if oauth_provider_uid.blank?
             ::User.where(
              "(oauth_provider_uid = ? AND oauth_provider = ?) OR email = ?",
              oauth_provider_uid,
              provider_name,
              provider_data[:email]
            ).first
          end

          def memoized_fetch_user_data(scope = 'me')
            @users_data ||= {}
            @users_data[scope.to_sym] ||= fetch_user_data(scope)
          end

          def oauth_provider_uid
            data = memoized_fetch_user_data
            data.is_a?(Hash) ? data[:oauth_provider_uid] : nil
          end

          def existent_user?
            @existent_user.present? && @existent_user.is_a?(User)
          end

          def fetch_user_data(scope = 'me')
            user_data = nil

            begin
              request = facebook_request(access_token, scope)
              request_body = request.body
              parsed_body  = JSON.parse(request_body)

              if request.is_a?(Net::HTTPOK)
                @auth_provider_data = parsed_body
                user_data = normalized_user_data_from_provider(parsed_body)
              else
                (@errors ||= []) << (Array.wrap(parsed_body['error'] || parsed_body))
              end
            rescue => e
              (@errors ||= []) << {
                message: e.try(:fb_error_message) || e.try(:message),
                code: e.try(:fb_error_code) || e.try(:code),
                status_code: e.try(:http_status) || 500
              }
            end

            user_data
          end

          def error?
            @errors.present?
          end

          def success?
            !error?
          end

          def provider_name
            :facebook
          end

          private
          def facebook_request(access_token, uuid='me')
            uri = URI("https://graph.facebook.com/#{uuid}/?access_token=#{access_token}&fields=#{PROVIDER_ATTRIBUTES.join(',')}")

            timeout = (CDI::Config.facebook_api_read_timeout || 10).to_i

            Net::HTTP.start(uri.host, uri.port,
              :read_timeout => timeout,
              :use_ssl => uri.scheme == 'https') do |http|
              request = Net::HTTP::Get.new uri

              response = http.request request # Net::HTTPResponse object
            end
          end

          def normalized_user_data_from_provider(provider_data)
            user_data = provider_data.slice(*PROVIDER_ATTRIBUTES.map(&:to_s))

            user_password = SecureRandom.hex

            user_data = user_data.merge(
              birthday: birthday_date_from_facebook(provider_data['birthday']),
              profile_type: User::VALID_PROFILES_TYPES[:student],
              password: user_password,
              password_confirmation: user_password,
              oauth_provider_uid: provider_data['id'],
              oauth_provider: provider_name
            ).deep_symbolize_keys
          end

          def birthday_date_from_facebook(birthday)
            return Date.today if birthday.blank?
            date = begin
              Date.parse(birthday)
            rescue => e
              date = birthday.to_s.gsub(/(\d{2})\/(\d{2})\/(\d{4})/, '\\2/\\1/\\3') rescue Date.today
              Date.parse(date)
            end
          end
        end
      end
    end
  end
end
