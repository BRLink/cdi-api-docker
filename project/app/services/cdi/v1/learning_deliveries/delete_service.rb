# encoding: UTF-8

module CDI
  module V1
    module LearningDeliveries
      class DeleteService < BaseDeleteService

        record_type ::LearningDelivery

      end
    end
  end
end
