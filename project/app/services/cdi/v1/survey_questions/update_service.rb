module CDI
  module V1
    module SurveyQuestions

      class UpdateService < BaseUpdateService

        record_type ::SurveyQuestion

        def record_params
          survey_question_params
        end

        def survey_question_params
          filter_hash(@options[:question], [:title])
        end
      end
    end
  end
end
