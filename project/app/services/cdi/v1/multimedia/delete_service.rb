# encoding: UTF-8

module CDI
  module V1
    module Multimedia
      class DeleteService < BaseDeleteService
        record_type ::Multimedia

        def after_success
          # nothing to do
        end
      end
    end
  end
end
