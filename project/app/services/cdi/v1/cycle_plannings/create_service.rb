# encoding: UTF-8

module CDI
  module V1
    module CyclePlannings

      class CreateService < BaseCreateService

        include V1::ServiceConcerns::CyclePlanningParams

        record_type ::CyclePlanning

        private
        def can_create_record?
          unless valid_learning_cycle?
            return not_found_error!('learning_cycles.not_found')
          end

          return @user.backoffice_profile?
        end

        def build_record
          learning_cycle.cycle_planning || learning_cycle.build_cycle_planning
        end

        def after_success
          if service_response = create_answers_for_questions
            @response_status = service_response.response_status
          end
        end

        def create_answers_for_questions
          if @options[:cycle_planning].key?(:questions_answers)
            answers = @options[:cycle_planning][:questions_answers]
            service = CDI::V1::QuestionsAnswersCreateService.new(@user, @record, answers: answers)
            service.execute

            service
          end
        end

        def learning_cycle
          @learning_cycle ||= @user.learning_cycles.find_by(id: cycle_planning_params[:learning_cycle_id])
        end

        def valid_learning_cycle?
          valid_object?(learning_cycle, ::LearningCycle)
        end

        ## This code is currently ONLY for reference
        ## must be erased in a near future
        def create_answers_for_questions_worker
          if @options[:cycle_planning].key?(:questions_answers)
            answers = @options[:cycle_planning][:questions_answers]

            worker = CDI::V1::CyclePlanningQuestionsAnswersCreateWorker

            if CDI::Config.enabled?(:create_planning_questions_async)
              worker.perform_async(@record.id, @user.id, answers)
            else
              worker.new.perform(@record, @user, answers)
            end
          end
        end
      end
    end
  end
end
