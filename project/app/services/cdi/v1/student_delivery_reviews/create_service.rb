# encoding: UTF-8

module CDI
  module V1
    module StudentDeliveryReviews

      class CreateService < BaseCreateService

        include V1::ServiceConcerns::StudentDeliveryReviewsParams

        record_type ::StudentDeliveryReview, alias_name: :review

        private
        def can_create_record?
          unless valid_student_delivery?
            return not_found_error!('student_deliveries.not_found')
          end

          true
        end

        def build_record
          @user.sent_deliveries_reviews.build(delivery_review_params)
        end

        def valid_student_delivery?
          valid_object?(student_delivery, ::StudentDelivery)
        end

        def student_delivery
          @student_delivery ||= ::StudentDelivery.find_by(id: attributes_hash.fetch(:student_delivery_id, nil))
        end

        def after_success
          if student_delivery
            student_delivery.update_attributes(correct: @record.correct, corrected_at: Time.zone.now)
            update_report_row(student_delivery)
          end
        end

        def update_report_row(student_delivery)
          begin
            learning_track = student_delivery.learning_track
            student_class_id = student_delivery.student_class_id

            if learning_track && student_class_id
              report = learning_track.reports.find_by(student_class_id: student_class_id)
              report.try(:update_student_row, student_delivery.user, student_delivery, false)
            end

          rescue Exception => e
            Rollbar.error(e)
          end
        end
      end
    end
  end
end
