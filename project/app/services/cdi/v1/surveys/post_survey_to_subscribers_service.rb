module CDI
  module V1
    module Surveys
      class PostSurveyToSubscribersService < BaseService

        def initialize survey, options={}
          super options
          @survey = survey
        end

        def execute
          if can_execute?
            users.each { |user| create_subscriber_survey(user) }
            create_subscriber_survey @survey.user
          end
        end

        def can_execute?
          @survey.present?
        end

        def users
          all_users = []
          @survey.visibilities.each { |v| all_users += users_by_visibilities(v) }
          all_users
        end

        def users_by_visibilities visibility
          if visibility.students?
            @survey.user.subscribers_users.student
          elsif visibility.multipliers?
            @survey.user.subscribers_users.multiplier
          elsif visibility.all?
            @survey.user.subscribers_users
          else
            obj = visibility.visibility_object
            @survey.user.subscribers_users.reduce_by_visibility(obj)
          end
        end

        def create_subscriber_survey user
          subscription = user.user_subscriptions.find_by(publisher: @survey.user)
          @survey.subscriber_surveys.build(user_subscription: subscription).save
        end
      end
    end
  end
end
