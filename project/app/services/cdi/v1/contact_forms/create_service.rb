# encoding: UTF-8

module CDI
  module V1
    module ContactForms
      class CreateService < ContactForms::BaseCreateService

        record_type ::ContactForm, alias_name: :contact

        def contact_type
          :contact_form
        end
      end
    end
  end
end
