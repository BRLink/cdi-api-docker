# encoding: UTF-8

module CDI
  module V1
    module ContactForms
      class BaseCreateService < ::CDI::BaseCreateService

        include V1::ServiceConcerns::ContactFormParams

        private
        def record_type_params
          contact_form_params
        end

        def can_create_record?
          unless can_send_with_params?
            return not_authorized_error!("#{contact_type.to_s}.cant_send_same_message")
          end

          if validate_ip_on_create? && !can_send_with_ip?
            # clear previous key in redis to allow message validation pass when
            # ip key expires
            CDI.redis_client.expire(redis_params_key, 0)
            return not_authorized_error!("#{contact_type.to_s}.ip_temporarily_blocked")
          end

          return true
        end

        def after_success
          @record.origin_ip  = @options[:origin] && @options[:origin][:ip]
          @record.user_agent = @options[:origin] && @options[:origin][:user_agent]

          # TODO: Change to async?
          delivery_sync_email(ContactsMailer, contact_type, @record)
          delivery_sync_email(ContactsMailer, contact_type, @record, @user.email)
        end

        def contact_type
          return not_implemented_exception(__method__)
        end

        def redis_params_key
          key_as_json = record_type_params.dup.slice!(:image)
          @_redis_params_key ||= Digest::MD5.hexdigest key_as_json.to_json
        end

        def redis_ip_key
          ip = @options[:origin].try(:[], :ip)
          return nil unless ip

          @_redis_ip_key ||= Digest::MD5.hexdigest ip
        end

        def validate_ip_on_create?
          true
        end

        def can_send_with_params?
          redis_setnx("#{contact_type}_#{redis_params_key}", default_redis_ttl)
        end

        def can_send_with_ip?
          redis_setnx("#{contact_type}_#{redis_ip_key}", default_redis_ttl)
        end

        def default_redis_ttl
          (CDI::Config.contact_form_redis_key_ttl || 10).to_i
        end
      end
    end
  end
end
