# encoding: UTF-8

module CDI
  module V1
    module GameCompleteSentences
      class CreateService < Games::BaseCreateService

        include V1::ServiceConcerns::GameCompleteSentencesParams

        record_type ::GameCompleteSentence

        private
        def base_options_key
          :complete_sentence
        end

        def game_params
          game_complete_sentence_params
        end

        def game_user_scope
          @user.game_complete_sentences
        end
      end
    end
  end
end
