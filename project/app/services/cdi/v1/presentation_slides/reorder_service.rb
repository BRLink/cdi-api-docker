# encoding: UTF-8

module CDI
  module V1
    module PresentationSlides

      class ReorderService < BaseReorderService

        record_type ::PresentationSlide

        def record_error_key
          :presentation_slides
        end

      end
    end
  end
end
