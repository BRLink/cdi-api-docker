# encoding: UTF-8

module CDI
  module V1
    module PresentationSlides
      class DeleteService < BaseDeleteService

        record_type ::PresentationSlide

      end
    end
  end
end
