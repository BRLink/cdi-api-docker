module CDI
  module V1
    module ServiceConcerns
      module SurveyParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [:title, :video_link]

        included do

          def attributes_hash
            @options[:survey]
          end

          def survey_open_question_params
            @survey_open_question_params ||= filter_hash(attributes_hash, WHITELIST_ATTRIBUTES).tap do |params|
              params[:video_link] = nil if has_image? || params[:video_link] ==  'null'
            end
          end

          def survey_poll_params
            @survey_poll_params ||= filter_hash(attributes_hash, [:video_link]).tap do |params|
              params[:video_link] = nil if has_image? || params[:video_link] ==  'null'
            end
          end
        end
      end
    end
  end
end
