module CDI
  module V1
    module ServiceConcerns
      module OauthParams

        extend ActiveSupport::Concern

        included do
          def fetch_avatar_from_provider
            return false if @record.has_uploaded_image?

            begin
              # in case is nil
              if @record.oauth_provider.to_s == 'facebook'
                @record.remote_profile_image_url = facebook_remote_avatar_url(@record.oauth_provider_uid)
                @record.save
              end
            rescue Exception => e
              Rollbar.error(e)
            end
          end

          def using_facebook_data?
            return fb_access_token.present?
          end

          def fb_access_token
            @options.fetch(:fb_access_token, nil) || @options[:user] && @options[:user][:fb_access_token]
          end

          def facebook_remote_avatar_url(user_id, type = 'large')
            "https://graph.facebook.com/#{user_id}/picture?type=#{type}"
          end

          def facebook_data
            return {} unless using_facebook_data?

            {
              oauth_provider_uid: facebook_user_data[:oauth_provider_uid],
              oauth_provider: facebook_user_data[:oauth_provider]
            }
          end

          def facebook_auth_provider
            @facebook_provider ||= CDI::V1::Users::AuthProviders::Facebook.new(fb_access_token)
          end

          def facebook_user_data
            @facebook_user_data ||= facebook_auth_provider.memoized_fetch_user_data
          end

          def invalid_facebook_access_token?
            facebook_user_data

            return false if facebook_auth_provider.errors.blank?

            facebook_auth_provider.errors.flatten.any? {|v| v['type'] == 'OAuthException' }
          end

        end

      end
    end
  end
end
