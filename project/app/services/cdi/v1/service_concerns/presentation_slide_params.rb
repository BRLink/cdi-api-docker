module CDI
  module V1
    module ServiceConcerns
      module PresentationSlideParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :presentation_id,
          :title,
          :layout_type,
          :slide_type,
          :content,
          :metadata,
          :position,
          :hidden
        ]

        included do
          def presentation_slide_params
            @_filtered_params          ||= filter_hash(@options[:slide], WHITELIST_ATTRIBUTES)
            @presentation_slide_params ||= format_as_json(@_filtered_params, [:content, :metadata])
          end

          def format_as_json(hash, keys=[])
            keys.each do |key|
              if ( hash[key] && (hash[key].is_a?(Hash) || hash[key].is_a?(Array)) )
                hash[key] = (hash[key] || {}).to_json
              end
            end

            hash
          end

          def valid_content_json?
            valid_json_string? presentation_slide_params[:content]
          end

          def valid_metadata_json?
            valid_json_string? presentation_slide_params[:metadata] || '{}', false
          end

          def valid_json_string?(content, check_blank = true)
            return false if check_blank && content.blank?
            return !!JSON.parse(content) rescue false
          end
        end
      end
    end
  end
end
