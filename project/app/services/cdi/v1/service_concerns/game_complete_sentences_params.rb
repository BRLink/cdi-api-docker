module CDI
  module V1
    module ServiceConcerns
      module GameCompleteSentencesParams
        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :sentence,
          :words,
        ]

        included do
          def game_complete_sentence_params
            @game_params ||= filter_hash(attributes_hash, WHITELIST_ATTRIBUTES)
            @game_params[:words] = array_values_from_params(attributes_hash, :words)

            @game_params
          end
        end
      end
    end
  end
end
