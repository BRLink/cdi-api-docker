module CDI
  module V1
    module ServiceConcerns
      module GameWordAnswersParams
        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :words
        ]

        VALID_RESOURCES_TYPES = [
          ::GameCompleteSentence,
          ::GameWordSearch,
          ::GameGroup
        ]

        included do
          def answer_params
            @answer_params ||= {
              words: array_values_from_params(attributes_hash, :words)
            }
          end

          def attributes_hash
            @options[:answer] || @options
          end

          def valid_game?
            VALID_RESOURCES_TYPES.any? {|game_class| valid_object?(game, game_class) }
          end

          # enroll track progress methods
          def game_type
            game.class.to_s
          end

          def game_id
            @game.id
          end
        end
      end
    end
  end
end
