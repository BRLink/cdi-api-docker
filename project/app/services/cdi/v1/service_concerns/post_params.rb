module CDI
  module V1
    module ServiceConcerns
      module PostParams

        extend ActiveSupport::Concern

        def attributes_hash
          @options[:post]
        end

        def publish_async?
          CDI::Config.enabled?(:publish_post_to_subscribers_async)
        end

        def publish_post_to_subscribers_async
          if publish_async?
            ::CDI::V1::PostToSubscribersWorker.perform_async(@record.id)
          else
            publish_post_to_subscribers
          end
        end

        def publish_post_to_subscribers
          service = ::CDI::V1::Posts::PostToSubscribersService.new(@record)
          service.execute
        end

      end
    end
  end
end
