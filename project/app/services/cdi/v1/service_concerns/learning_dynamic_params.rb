module CDI
  module V1
    module ServiceConcerns
      module LearningDynamicParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :title,
          :category_ids,
          :perspectives
        ]

        included do
          def learning_dynamic_params
            @learning_dynamic_params ||= filter_hash(@options[:dynamic], WHITELIST_ATTRIBUTES)
            @learning_dynamic_params[:skill_ids] = skills_from_params
            @learning_dynamic_params[:perspectives] = perspectives_from_params
            @learning_dynamic_params[:category_ids] = categories_from_params

            @learning_dynamic_params
          end

          def skills_from_params
            array_values_from_params(@options, :skills, :dynamic)
          end

          def perspectives_from_params
            array_values_from_params(@options, :perspectives, :dynamic)
          end

          def categories_from_params
            array_values_from_params(@options, :category_ids, :dynamic)
          end
        end
      end
    end
  end
end
