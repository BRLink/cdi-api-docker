module CDI
  module V1
    module ServiceConcerns
      module ChallengeParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :title,
          :description,
          :format_type,
          :delivery_title,
          :delivery_kind,
          :delivery_date,
          :cover_image,
          :group_size
        ]

        included do
          def challenge_params
            @challenge_params ||= filter_hash(attributes_hash, WHITELIST_ATTRIBUTES).tap do |params|
              params[:tags] = tags_from_params if attributes_hash[:tags]
            end
          end

          def attributes_hash
            @options[:challenge]
          end

          def tags_from_params
            array_values_from_params(@options, :tags, :challenge)
          end

          def valid_delivery_kind?
            attributes_hash[:delivery_kind] == 'individual' ||
              (attributes_hash[:delivery_kind] == 'group' &&
                attributes_hash[:group_size].present?)
          end

        end
      end
    end
  end
end
