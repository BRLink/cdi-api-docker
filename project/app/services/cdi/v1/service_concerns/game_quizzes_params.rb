module CDI
  module V1
    module ServiceConcerns
      module GameQuizzesParams
        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :image
        ]

        included do
          def game_quiz_params
            @game_quiz_params ||= filter_hash(attributes_hash, WHITELIST_ATTRIBUTES)
          end

          def attributes_hash
            @options[:game] || @options[:quiz]
          end
        end
      end
    end
  end
end
