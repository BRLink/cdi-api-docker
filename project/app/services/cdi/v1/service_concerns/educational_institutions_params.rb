module CDI
  module V1
    module ServiceConcerns
      module EducationalInstitutionsParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :name,
          :email,
          :about,
          :site,
          :telephone,
          :address
        ]

        included do
          def educational_institution_params
            params = @options[:institution] ||  @options[:educational_institution]
            filter_hash(params, WHITELIST_ATTRIBUTES)
          end

          def user_can_edit_educational_institution?
            @user.owned_educational_institutions.find_by(id: @options[:id]) or
            @user.managed_institutions.find_by(id: @options[:id])
          end
        end

      end
    end
  end
end
