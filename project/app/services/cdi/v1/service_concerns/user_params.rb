module CDI
  module V1
    module ServiceConcerns
      module UserParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :first_name,
          :last_name,
          :birthday,
          :gender,
          :email,
          :about,
          :profile_type,
          :password,
          :password_confirmation,
          :profile_image,
          :phone_number,
          :phone_area_code,
          :tof_accepted
        ]

        ADDRESS_WHITELIST_ATTRIBUTES = [
          :city_id,
          :street,
          :number,
          :district,
          :complement,
          :zipcode
        ]

        POSTMON_ENDPOINT = 'http://api.postmon.com.br/v1/cep/%s'

        DEFAULT_PROVIDER = :site

        included do
          def user_params
            @user_params ||= set_image_crop_params(filter_hash(attributes_hash, WHITELIST_ATTRIBUTES))
            @user_params[:password_confirmation] ||= @options[:user] && @options[:user].fetch(:password_confirmation, true)

            @user_params
          end

          def attributes_hash
            @options[:user]
          end

          def set_image_crop_params(params, mounted_as = :profile_image)
            return params unless updating_profile_image?

            geometry = (attributes_hash && attributes_hash[:"#{mounted_as}_crop_geometry"]) || []

            if geometry
              x,y,w,h = nil

              if geometry.is_a?(String)
                x,y,w,h = array_values_from_string(geometry)
              elsif geometry.is_a?(Array)
                x,y,w,h = geometry
              elsif geometry.is_a?(Hash)
                x,y,w,h = geometry[:x], geometry[:y], geometry[:w], geometry[:h]
              end

              geometries = [x,y,w,h]

              if geometries.any?
                # remove negative values
                geometries.map! {|g| g.to_i.abs }

                [:crop_x, :crop_y, :crop_w, :crop_h].each_with_index do |a , i|
                  params.merge!(:"#{mounted_as}_#{a}" => geometries[i])
                end
              end
            end

            params
          end

          def classes_codes
            @classes_codes ||= array_values_from_params(@options, :classes_codes, :user)
          end

          def non_change_trackable_attributes
            [:password_confirmation]
          end

          def educational_institution
            institution_id = attributes_hash.fetch(:educational_institution_id, nil)

            @educational_institution ||= EducationalInstitution.find_by(id: institution_id)
          end

          def valid_educational_institution?
            valid_object?(educational_institution, ::EducationalInstitution)
          end

          def student_signup?
            profile_type == User::VALID_PROFILES_TYPES[:student]
          end

          def multiplier_signup?
            User::VALID_MULTIPLIER_PROFILES_TYPES.values.member?(profile_type)
          end

          def profile_type
            user_params[:profile_type].to_s
          end

          def updating_profile_image?
            attributes_hash && attributes_hash[:profile_image].present?
          end
        end

      end
    end
  end
end
