module CDI
  module V1
    module ServiceConcerns
      module MultimediaParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :file,
          :link,
          :name,
          :category_ids,
          :tags
        ]

        included do
          def multimedia_params
            @multimedia_params ||= filter_hash(@options[:multimedia], WHITELIST_ATTRIBUTES)
            # puts "@@@ #{@multimedia_params[:file]}"
            @multimedia_params[:multimedia_type] = multimedia_type_from_params if multimedia_type_from_params.present?
            @multimedia_params[:metadata] = metadata_from_params if metadata_from_params.present?
            @multimedia_params[:tags] = tags_from_params
            @multimedia_params[:status] = "social_published" if social_published?
            @multimedia_params[:category_ids] = categories_from_params

            @multimedia_params
          end

          def categories_from_params
            array_values_from_params(@options, :category_ids, :multimedia)
          end

          def tags_from_params
            array_values_from_params(@options, :tags, :multimedia)
          end

          def multimedia_type_from_params
            return 'link' if @multimedia_params[:link].present?
            if @multimedia_params[:file].present?
              info = @multimedia_params[:file][:type]
              ::Multimedia::MULTIMEDIA_TYPES.each do |key, items|
                items.each do |item|
                  if info.include? item
                    return key
                  end
                end
              end
              return 'unknown'
            end
            nil
          end

          def metadata_from_params
            if @multimedia_params[:file]
              return {
                file_size: @multimedia_params[:file][:tempfile].size
              }
            elsif @multimedia_params[:link]
              return {
                url: @multimedia_params[:link]
              }
            end
            nil
          end

          def social_published?
            @options[:multimedia][:social] == 1
          end

        end
      end
    end
  end
end
