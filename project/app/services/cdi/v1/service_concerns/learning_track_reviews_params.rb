module CDI
  module V1
    module ServiceConcerns
      module LearningTrackReviewsParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :learning_track_id,
          :student_class_id,
          :review_type,
          :comment,
          :score
        ]

        included do
          def track_review_params
            @track_review_params ||= filter_hash(attributes_hash, WHITELIST_ATTRIBUTES)
          end

          def attributes_hash
            @options.fetch(:review, {})
          end

        end
      end
    end
  end
end
