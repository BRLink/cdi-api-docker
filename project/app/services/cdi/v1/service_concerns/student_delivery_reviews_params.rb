module CDI
  module V1
    module ServiceConcerns
      module StudentDeliveryReviewsParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :correct,
          :comment,
          :student_delivery_id
        ]

        included do
          def delivery_review_params
            @delivery_review_params ||= filter_hash(attributes_hash, WHITELIST_ATTRIBUTES)
          end

          def attributes_hash
            @options.fetch(:review, {})
          end

        end
      end
    end
  end
end
