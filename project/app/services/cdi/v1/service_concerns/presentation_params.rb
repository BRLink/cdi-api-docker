module CDI
  module V1
    module ServiceConcerns
      module PresentationParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :file
        ]

        included do
          def presentation_params
            @presentation_params ||= filter_hash(@options[:presentation], WHITELIST_ATTRIBUTES)
          end
        end
      end
    end
  end
end
