module CDI
  module V1
    module ServiceConcerns
      module GameItemAnswersParams
        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :reply_word
        ]

        included do
          def answer_params
            @answer_params ||= filter_hash(attributes_hash , WHITELIST_ATTRIBUTES)
          end

          def attributes_hash
            @options[:answer] || @options
          end

          def valid_game_item?
            valid_object?(game_item, ::GameItem)
          end

          def game
            game_item.game
          end

          def game_id
            game.id
          end

          def game_type
            game.class.to_s
          end

        end
      end
    end
  end
end
