module CDI
  module V1
    module SurveyPolls
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::SurveyParams
        include V1::ServiceConcerns::SurveyAttachmentParams

        record_type ::SurveyPoll

        def record_params
          survey_poll_params
        end

        def whitelist_attributes_to_clear
          ['video_link']
        end

        def after_success
          update_question
          update_image
        end

        def update_question
          if attributes_hash[:title]
            @record.question.update_attribute(:title, attributes_hash[:title])
          end
        end
      end
    end
  end
end
