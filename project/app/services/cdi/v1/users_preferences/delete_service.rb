# encoding: UTF-8

module CDI
  module V1
    module UsersPreferences
      class DeleteService < BaseDeleteService

        record_type ::UserPreference
      end
    end
  end
end
