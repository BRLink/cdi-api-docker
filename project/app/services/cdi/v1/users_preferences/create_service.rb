# encoding: UTF-8

module CDI
  module V1
    module UsersPreferences
      class CreateService < BaseCreateService

        record_type ::UserPreference

        def initialize user, options={}
          super user, options
        end

        def can_create_record?
          true
        end

        def duplicate_record?
          false
        end

        def build_record
          @user.user_preferences.build(preference_params)
        end

        def preference_params
          return @preference_params if @preference_params
          @preference_params = filter_hash(@options[:preference], [:type, :title, :author, :link])
          @preference_params[:preference_type] = @preference_params.delete(:type)
          @preference_params
        end
      end
    end
  end
end
