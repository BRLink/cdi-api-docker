# encoding: UTF-8

module CDI
  module V1
    module GameQuizzes
      class UpdateService < Games::BaseUpdateService

        record_type ::GameQuiz

        def base_options_key
          :quiz
        end

      end
    end
  end
end
