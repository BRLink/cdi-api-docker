# encoding: UTF-8

module CDI
  module V1
    module StudentDeliveries
      class UpdateService < BaseUpdateService

        WHITELIST_ATTRIBUTES = [:correct]

        record_type ::StudentDelivery

        def record_params
          params = filter_hash(@options[:delivery], WHITELIST_ATTRIBUTES)
          params.merge!(corrected_at: DateTime.now)

          params
        end

      end
    end
  end
end
