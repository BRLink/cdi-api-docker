# encoding: UTF-8

module CDI
  module V1
    module GamePolls
      class DeleteService < Games::BaseDeleteService

        record_type ::GamePoll

      end
    end
  end
end
