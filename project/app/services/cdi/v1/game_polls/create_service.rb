# encoding: UTF-8

module CDI
  module V1
    module GamePolls
      class CreateService < Games::BaseCreateService

        include V1::ServiceConcerns::GamePollsParams

        record_type ::GamePoll

        private
        def base_options_key
          :poll
        end

        def game_params
          game_poll_params
        end

        def game_user_scope
          @user.game_polls
        end

        def questions_key
          :question
        end

        def game_with_questions?
          true
        end

      end
    end
  end
end
