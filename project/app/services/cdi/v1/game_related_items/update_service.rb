# encoding: UTF-8

module CDI
  module V1
    module GameRelatedItems
      class UpdateService < Games::BaseUpdateService

        include V1::ServiceConcerns::GameRelatedItemsParams

        record_type ::GameRelatedItem

        private
        def game_params
          game_related_items_params
        end

        def base_options_key
          :game
        end

      end
    end
  end
end
