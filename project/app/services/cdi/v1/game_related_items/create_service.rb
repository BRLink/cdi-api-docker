# encoding: UTF-8

module CDI
  module V1
    module GameRelatedItems
      class CreateService < Games::BaseCreateService

        include V1::ServiceConcerns::GameRelatedItemsParams

        record_type ::GameRelatedItem

        private
        def can_create_record?
          unless valid_items?
            return forbidden_error!('game_related_items.supply_at_least_two_item')
          end

          super
        end

        def items
          attributes_hash && attributes_hash.fetch(:columns, [])
        end

        def has_items?
          items.many? # more than 1 valid
        end

        def valid_items?
          return false unless has_items?

          items.all? do |item|
            valid_item?(item)
          end
        end

        def valid_item?(item)
          return false if item.blank? || !item.is_a?(Hash)

          duped_item = filter_hash(item.dup.symbolize_keys, ServiceConcerns::GameItemsParams::WHITELIST_ATTRIBUTES)

          return false if !duped_item.key?(:word) || !duped_item.key?(:related_word)

          game_item = ::GameItem.new(item)
          game_item.skip_father_game_validation = true

          game_item.valid?
        end

        def base_options_key
          :game
        end

        def game_params
          game_related_items_params
        end

        def game_user_scope
          @user.game_related_items
        end

        def has_questions?
          false
        end

        def after_success
          create_items
        end

        def create_items
          if has_items?
            items.each do |item_hash|
              create_game_item_with_service(item_hash)
            end
          end
        end

        def create_game_item_with_service(item_hash, options = {})
          item_hash = item_hash.last if item_hash.is_a?(Array)

          options = @options.slice(:origin)
                    .merge(
                      item: item_hash,
                      validate_ip_on_create: false
                    )
                    .deep_merge(options)

          service = GameItems::CreateService.new(@record, @user, options)
          service.execute

          service.success?
        end

      end
    end
  end
end
