module CDI
  module V1
    module Notifications
      class CreateService < BaseCreateService

        record_type ::Notification

        private
        def can_create_record?
          return notification_sender_user_id != notification_receiver_user_id
        end

        def build_record
          @user.notify(notification_data, :build)
        end

        def after_success
          notification_config = @user.notification_config(notification_type)

          UsersMailer.notification(@record).deliver_later if notification_config.can_email?
          Pusher.trigger("private-user-#{@user.id}", :notification, NotificationSerializer.new(@record)) if notification_config.can_notification?
        end

        def notificable
          @options[:notificable]
        end

        def notificable_type
          return @options[:notificable_type] if @options[:notificable_type].present?
          notificable.try(:class).try(:name)
        end

        def notificable_id
          return @options[:notificable_id] if @options[:notificable_id].present?
          notificable.try(:id)
        end

        def notification_type
          @options[:type] || @options[:notification_type]
        end

        def notification_sender_user_id
          return @options[:sender_user_id] if @options[:notificable_id].present?
          @options[:sender_user].id
        end

        def notification_receiver_user_id
          @user.id
        end

        def notification_data
          {
            notification_type: notification_type,
            sender_user_id:    notification_sender_user_id,
            notificable_type:  notificable_type,
            notificable_id:    notificable_id,
          }
        end

      end
    end
  end
end
