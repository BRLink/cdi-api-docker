module CDI
  module V1
    module StudentClasses
      class ActiveCodeService < BaseUpdateService

        record_type ::StudentClass

        private

        def record_params
          @_params ||= @record.slice(:code_expiration_date)
        end

        def user_can_update?
          if @record.status_closed?
            return forbidden_error!('student_classes.must_be_active_to_activate_code')
          end

          if @record.code_active?
            return forbidden_error!('student_classes.code_not_expired')
          end

          super
        end

        def update_record
          @record.activate_code

          @record
        end
      end
    end
  end
end
