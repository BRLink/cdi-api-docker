# encoding: UTF-8

module CDI
  module V1
    module StudentClasses
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::StudentClassParams

        record_type ::StudentClass

        private
        def can_create_record?
          return false unless valid_user?
          return false unless @user.backoffice_profile?

          if duplicate_record?
            return forbidden_error!("#{record_error_key}.duplicate_title")
          end

          unless valid_educational_institution?
            return not_found_error!('educational_institutions.not_found')
          end

          return true
        end

        def valid_educational_institution?
          valid_object?(educational_institution, ::EducationalInstitution)
        end

        def duplicate_record?
          duplicate_record_in_interval?(@user.administered_student_classes, 10.seconds, student_class_params.slice(:name))
        end

        def build_record
          student_class_data = {
            educational_institution_id: educational_institution.id
          }.merge(student_class_params)

          @user.administered_student_classes.build(student_class_data)
        end

        def educational_institution
          return @educational_institution if @educational_institution.present?

          educational_institution = nil

          if institution_id = attributes_hash.fetch(:educational_institution_id, nil)
            educational_institution = @user.educational_institutions.find_by(id: institution_id)
          else
            # user just belongs to one institution, so use it
            if @user.educational_institutions.size == 1
              educational_institution = @user.educational_institutions.first
            end
          end

          @educational_institution = educational_institution
        end

        def cache_keys_to_expire_after_success
          %w(current_user.show) # /users/me
        end

      end
    end
  end
end
