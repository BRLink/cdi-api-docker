module CDI
  module V1
    module DiscussionOptions
      class UpdateService < BaseUpdateService

        record_type ::DiscussionOption

        def record_params
          filter_hash(@options[:option], [:text])
        end
      end
    end
  end
end
