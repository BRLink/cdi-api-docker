module CDI
  module V1
    module DiscussionOptions
      class DeleteService < BaseDeleteService

        record_type ::DiscussionOption
      end
    end
  end
end
