# encoding: UTF-8

module CDI
  module V1
    module GameGroups
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::GameGroupParams

        record_type ::GameGroup

        attr_reader :game

        def initialize(game, user, options = {})
          @game = game
          super(user, options)
        end

        private
        def can_create_record?
          return false unless valid_user?

          unless valid_game?
            return not_found_error!("game_group_items.not_found")
          end

          return can_create_group?
        end

        def can_create_group?
          return @user.backoffice_profile?
        end

        def build_record
          game.groups.build(game_group_params)
        end
      end
    end
  end
end
