# encoding: UTF-8

module CDI
  module V1
    module GameGroups
      class AddItemService < GameGroups::UpdateService

        include V1::ServiceConcerns::GameGroupParams

        record_type ::GameGroup

        def changed_attributes
          [:items]
        end

        def record_params
          @record_params ||= { items: new_items_list }
        end

        def new_items_list
          (@record.items || []).push(item_to_add)
        end

        def attributes_hash
          @options[:group] || @options
        end

        def user_can_update?
          return false unless super

          unless item_to_add.present?
            return forbidden_error!('game_groups.invalid_item')
          end

          if existent_item?
            return forbidden_error!('game_groups.item_yet_added')
          end

          return true
        end

        def existent_item?
          @record.items.map(&:downcase).member?(item_to_add.to_s.downcase)
        end

        def item_to_add
          attributes_hash.fetch(:item_title, nil)
        end

      end

    end
  end
end
