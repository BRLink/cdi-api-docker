# encoding: UTF-8

module CDI
  module V1
    module GameGroups
      class DeleteService < BaseDeleteService

        record_type ::GameGroup

      end
    end
  end
end
