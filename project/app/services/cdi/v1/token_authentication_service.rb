# encoding: UTF-8

module CDI
  module V1
    class TokenAuthenticationService < AuthenticationService

      def initialize(token, provider, options={})
        super(nil, nil, options)
        @token    = token
        @provider = provider
      end

      def execute
        if valid_provider?

          @auth_token = Authorization.valid.find_by(token: @token, provider: @provider)

          if @auth_token
            if @auth_token.valid_access?
              success_response
            else
              not_authorized_error(:invalid_auth_token)
              @auth_token.delete
            end
          else
            not_authorized_error(:invalid_auth_token)
          end
        else
          invalid_provider_response
        end

        success?
      end

      private
      def after_success
        @auth_token.update_token_expires_at
        @user = @auth_token.try(:user)
      end
    end
  end
end
