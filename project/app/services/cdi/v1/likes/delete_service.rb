# encoding: UTF-8

module CDI
  module V1
    module Likes
      class DeleteService < BaseDeleteService

        record_type ::Like

        def initialize resource, user, options={}
          record = user.get_like_of resource
          super record, user, options
        end
      end
    end
  end
end
