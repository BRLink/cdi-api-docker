module CDI
  module V1
    module Discussions
      class DeleteService < BaseDeleteService

        record_type ::Discussion
      end
    end
  end
end
