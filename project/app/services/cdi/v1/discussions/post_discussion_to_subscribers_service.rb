module CDI
  module V1
    module Discussions
      class PostDiscussionToSubscribersService < BaseService

        def initialize discussion, options={}
          @discussion = discussion
          super options
        end

        def execute
          if can_execute?
            users.each { |user| create_subscriber_discussion(user) }
            create_subscriber_discussion @discussion.user
          end
        end

        def can_execute?
          @discussion.present?
        end

        def users
          all_users = []
          @discussion.visibilities.each { |v| all_users += users_by_visibilities(v) }
          all_users
        end

        def users_by_visibilities visibility
          if visibility.students?
            @discussion.user.subscribers_users.student
          elsif visibility.multipliers?
            @discussion.user.subscribers_users.multiplier
          elsif visibility.all?
            @discussion.user.subscribers_users
          else
            obj = visibility.visibility_object
            @discussion.user.subscribers_users.reduce_by_visibility(obj)
          end
        end

        def create_subscriber_discussion user
          subscription = user.user_subscriptions.find_by(publisher: @discussion.user)
          @discussion.subscriber_discussions.create(user_subscription: subscription)
        end
      end
    end
  end
end
