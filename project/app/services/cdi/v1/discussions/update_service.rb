module CDI
  module V1
    module Discussions
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::DiscussionParams
        include V1::ServiceConcerns::SurveyAttachmentParams

        record_type ::Discussion

        def record_params
          discussion_params
        end

        def whitelist_attributes_to_clear
          ['cover_id', 'video_link']
        end

        def can_execute_action?
          if has_cover? && !valid_cover?
            return forbidden_error!("#{record_error_key}.cover_invalid_or_not_found")
          end

          if has_best_comment? && !valid_best_comment?
            return forbidden_error!("#{record_error_key}.comment_invalid_or_not_found")
          end

          super
        end

        def after_success
          update_image
        end
      end
    end
  end
end
