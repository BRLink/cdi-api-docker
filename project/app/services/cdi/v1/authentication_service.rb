# encoding: UTF-8

module CDI
  module V1
    class AuthenticationService < BaseService

      attr_reader :email, :password, :user, :auth_token

      delegate :id, :username, :profile_image_url, :profile_type, to: :user, prefix: :user
      delegate :provider, :expires_at, :token, to: :auth_token, prefix: :user_auth

      MAX_TOKEN_BY_PROVIDER = (CDI::Config.max_auth_tokens_by_provider || 3).to_i

      def initialize(email, password, options = {})
        super(options, 401)
        @email    = email
        @password = password
      end

      def execute
        if can_execute_action?

          @user = execute_action

          if valid_user?
            success_created_response
          else
            not_authorized_error(invalid_credentials_error)
          end
        end

        success?
      end

      private

      def execute_action
        User.authenticate(@email, @password)
      end

      def can_execute_action?
        unless valid_provider?
          return invalid_provider_response
        end

        return true
      end

      def after_success
        clear_provider_authorizations
        create_authorization_for_provider
      end

      def valid_providers
        Authorization::PROVIDERS.keys.map(&:to_sym)
      end

      def valid_provider?
        @provider ||= @options[:provider]

        return valid_providers.member?(@provider.to_s.downcase.to_sym)
      end

      def create_authorization_for_provider
        @auth_token = @user.authorizations.create(provider: @options[:provider])
      end

      def clear_provider_authorizations
        return false if CDI::Config.disabled?(:limit_max_login_by_provider)

        scope = @user.authorizations.where(provider: @options[:provider])
        scope.delete_all if scope.count > MAX_TOKEN_BY_PROVIDER
      end

      def invalid_provider_response
        bad_request_error!(:invalid_auth_provider, valid_providers: valid_providers.to_sentence)
      end

      def invalid_credentials_error
        :invalid_user_credentials
      end
    end
  end
end
