# encoding: UTF-8

module CDI
  module V1
    module LearningCycles

      class CreateService < BaseCreateService

        include V1::ServiceConcerns::LearningCycleParams

        record_type ::LearningCycle

        private

        def can_create_record?
          if duplicate_record?
            return forbidden_error!("#{record_error_key}.duplicate_title")
          end

          return @user.backoffice_profile?
        end

        def duplicate_record?
          duplicate_record_in_interval?(@user.learning_cycles, 10.seconds, name: learning_cycle_params[:name])
        end

        def build_record
          @user.learning_cycles.build(learning_cycle_params)
        end

        ### Final step
        def after_success
          notify_users
        end

        def notify_users
          # delivery_async_email(LearningCycleMailer, :new_cycle, @record)
        end

        def cache_keys_to_expire_after_success
          %w(current_user.show) # /users/me
        end

        def matched_cache_keys_to_expire_after_success
          %W(current_user_id_#{@user.id}_learning_cycles)
        end

      end

    end
  end
end
