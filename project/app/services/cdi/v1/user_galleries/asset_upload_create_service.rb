# encoding: UTF-8

module CDI
  module V1
    module UserGalleries
      class AssetUploadCreateService < BaseCreateService

        include V1::ServiceConcerns::FileUploadParams

        record_type ::Photo, alias_name: :asset

        response_options success_code: 200

        private
        def build_record
          file = upload_params.delete(:file)
          is_base_64 = (file.is_a?(String) && file.match(/\Adata\:image/)).present?
          is_file = (!is_base_64 && file[:type] && file[:type].to_s.match(/\Aimage\//))

          if (file && ( is_base_64 || is_file ))
            upload_params[:create_image_versions] = @options[:upload][:create_image_versions]
            upload_params[:slide_thumb_upload] = @options[:upload][:slide_thumb_upload]
            upload_params[:game_item_image_upload] = @options[:upload][:game_item_image_upload]

            photo = album.photos.build(upload_params)
            # this fix base-64 versions problems
            photo.image = file

            photo
          end
        end

        def can_create_record?
          return true
        end

        def album
          current_upload_params = upload_params.dup
          album_id = current_upload_params.delete(:album_id)

          if album_id
            @album = @user.albums.find_by(id: album_id)
          end

          @album ||= @user.default_gallery_album
        end

        def record_error_key
          :assets
        end
      end
    end
  end
end
