# encoding: UTF-8

module CDI
  module V1
    module SubscribersGroups
      class CreateService < BaseCreateService

        record_type ::SubscribersGroup

        def can_create_record?
          true
        end

        def duplicate_record?
          false
        end

        def build_record
          @user.subscribers_groups.build(subscribers_group_params)
        end

        def after_success
          @created_members = []
          subscriptions.each do |subscription|
            member = @record.members.build(user_subscription: subscription)
            @created_members << member if member.save
          end
        end

        def created_members
          @created_members
        end

        def subscribers_group_params
          @subscribers_group_params ||= filter_hash(@options[:subscribers_group], [:name])
        end

        def subscriptions
          @subscriptions ||= @user.subscribers.where(user_id: @options[:members])
        end
      end
    end
  end
end
