# encoding: UTF-8

module CDI
  module V1
    module Games
      class BaseDeleteService < CDI::BaseDeleteService

        def after_success
          # dependent destroy not working properly
          ActiveRecord::Base.transaction do
            delete_questions_options
            delete_questions
          end
        end

        def delete_questions
          if @record.respond_to?(:questions)
            ::GameQuestion.where(id: @record.question_ids).delete_all
          elsif @record.respond_to?(:question)
            @record.question.try(:delete)
          end
        end

        def delete_questions_options
          return false unless @record.respond_to?(:options)

          option_ids = @record.options.pluck(:id)
          ::GameQuestionOption.where(id: option_ids).delete_all
        end
      end
    end
  end
end
