# encoding: UTF-8

module CDI
  module V1
    module Games
      class BaseCreateService < CDI::BaseCreateService

        include V1::ServiceConcerns::GameParams

        private
        # def validate_ip_on_create?
        #   @options[:validate_ip_on_create] != false
        # end

        def can_create_record?
          return false unless @user.backoffice_profile?

          if game_with_questions? && !has_questions?
            return forbidden_error!("#{record_error_key}.supply_at_least_one_question")
          end

          if duplicate_record?
            return forbidden_error!("#{record_error_key}.duplicate_title")
          end

          return @user.backoffice_profile?
        end

        def duplicate_record?
          # TODO: Games won't have title for now, review this later
          return false
          duplicate_record_in_interval?(game_user_scope, 5.seconds, title: game_params[:title])
        end

        def build_record
          game_user_scope.build(game_params)
        end

        def after_success
          create_questions
        end

        def create_questions
          if has_questions?
            questions.each do |question_hash|
              create_question_with_service(question_hash)
            end
          end
        end

        def create_question_with_service(question_hash, options = {})
          question_hash = question_hash.last if question_hash.is_a?(Array)
          question_hash.merge!(game_id: @record.id, game_type: @record.class.to_s)

          question_data = normalize_data_for_question(question_hash)

          options = @options.slice(:origin)
                    .merge(
                      question: question_data,
                      validate_ip_on_create: false
                    )
                    .deep_merge(options)

          service = GameQuestions::CreateService.new(@user, options)
          service.execute

          service.success?
        end

        def game_user_scope
          # ex: @user.game_quizzes
          not_implemented_exception(__method__)
        end

        def base_options_key
          :games
        end

        def questions_key
          :questions
        end

        def has_questions?
          attributes_hash && attributes_hash.fetch(questions_key, []).any?
        end

        def questions
          Array.wrap(attributes_hash[questions_key])
        end

        def game_question_options_for_game
          {}
        end

        def normalize_data_for_question(question_data)
          question_data
        end

        def game_with_questions?
          false
        end

      end
    end
  end
end
