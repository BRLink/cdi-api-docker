module CDI
  module V1
    module Games
      module GameProgressTrackMethods
        extend ActiveSupport::Concern

        included do
          def register_user_progress_for_track
            sgame = game.is_a?(GameGroup) ? game.game_group_item : game

            game_slide ||= ::PresentationSlide.where(associated_resource: sgame).last

            if game_slide
              if sgame.is_a?(GamePoll)
                total_questions = ::GamePoll::MAX_QUESTIONS_BY_GAME # TODO:
              elsif sgame.is_a?(GameRelatedItem)
                total_questions = sgame.items_count || s.games.items.size
              elsif sgame.respond_to?(:questions)
                total_questions = sgame.questions.size
              elsif sgame.respond_to?(:max_answers_count_per_user)
                total_questions = sgame.max_answers_count_per_user
              end

              # if user replied to all questions of game
              if total_questions && (total_user_replied_answers_for_game >= total_questions)
                run_tracking_event_service(game_slide)
              end

            end
          end

          def total_user_replied_answers_for_game
            if game.respond_to?(:questions)
              @total_user_replied_answers ||=
                @user.game_question_answers
                     .joins(:game_question)
                     .where(game_questions: {
                        questionable_type: "Game#{game_type.to_s.classify}",
                        questionable_id: game_id
                      })
                     .size
            elsif game.is_a?(GameGroup)
              game.game_group_item.answers.where(user_id: @user.id).size
            elsif game.respond_to?(:answers)
              game.answers.where(user_id: @user.id).size
            end
          end

          def run_tracking_event_service(game_slide, event_type = :played)
            return nil if game_slide.blank?

            learning_track = game_slide.learning_track

            event_params = {
              event_type: ::TrackingEvent::EVENT_TYPES[event_type.to_sym],
              learning_track_id: learning_track.id,
              presentation_slide_id: game_slide.id
            }

            begin
              service = ::CDI::V1::TrackingEvents::CreateService.new(@user, event: event_params)
              service.execute
            rescue PG::UniqueViolation, ActiveRecord::RecordNotUnique => e
              # validation or uniqueness constrait
              Rollbar.error(e)
            rescue => e
              Rollbar.error(e)
            end
          end
        end
      end
    end
  end
end
