module CDI
  module V1
    module ResourcesFeatured
      class CreateService < BaseCreateService

        record_type ::ResourceFeatured

        def initialize resource, user, options
          super user, options
          @resource = resource
        end

        def can_create_record?
          if @resource.nil?
            return not_found_error!(@options[:resource_type].underscore)
          end
          @user.backoffice_profile?
        end

        def duplicate_record?
          false
        end

        def build_record
          @user.resources_featured.build(resource: @resource)
        end
      end
    end
  end
end
