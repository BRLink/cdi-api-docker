# encoding: UTF-8

module CDI
  module V1
    module GameGroupItems
      class CreateService < Games::BaseCreateService

        include V1::ServiceConcerns::GameGroupItemsParams

        record_type ::GameGroupItem

        private
        def can_create_record?
          unless valid_groups?
            return forbidden_error!('game_group_items.supply_at_least_two_groups')
          end

          super
        end

        def valid_groups?
          return false unless has_groups?

          groups.all? do |group|
            valid_group?(group)
          end
        end

        def base_options_key
          :game
        end

        def game_params
          game_group_items_params
        end

        def game_user_scope
          @user.game_group_items
        end

        def has_questions?
          false
        end

        def after_success
          create_groups
        end

        def create_groups
          if has_groups?
            groups.each do |group_hash|
              create_game_group_with_service(group_hash)
            end
          end
        end

        def create_game_group_with_service(group_hash, options = {})
          group_hash = group_hash.last if group_hash.is_a?(Array)

          options = @options.slice(:origin)
                    .merge(
                      group: group_hash
                    )
                    .deep_merge(options)

          service = GameGroups::CreateService.new(@record, @user, options)
          service.execute

          service.success?
        end

      end
    end
  end
end
