# encoding: UTF-8

module CDI
  module V1
    module GameTrueFalses
      class DeleteService < Games::BaseDeleteService

        record_type ::GameTrueFalse

      end
    end
  end
end
