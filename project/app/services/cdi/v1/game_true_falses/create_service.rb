# encoding: UTF-8

module CDI
  module V1
    module GameTrueFalses
      class CreateService < Games::BaseCreateService

        include V1::ServiceConcerns::GameTrueFalseParams

        record_type ::GameTrueFalse

        private
        def can_create_record?
          unless has_questions?
            return forbidden_error!('game_true_false.supply_at_least_one_question')
          end

          super
        end

        def base_options_key
          :game
        end

        def game_params
          game_true_false_params
        end

        def game_user_scope
          @user.game_true_false
        end

      end
    end
  end
end
