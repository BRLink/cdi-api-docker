module CDI
  module V1
    module Challenges
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::ChallengeParams

        record_type ::Challenge

        def record_params
          challenge_params
        end
      end
    end
  end
end
