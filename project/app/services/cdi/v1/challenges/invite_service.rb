module CDI
  module V1
    module Challenges
      class InviteService < BaseActionService

        action_name :invite_challenge

        def initialize(challenge, user, options={})
          @challenge = challenge
          @user = user
          super options
        end

        def can_execute_action?
          can_execute = super
          return can_execute unless can_execute

          unless merged_user_ids.size > 0
            return forbidden_error!("#{record_error_key}.invalid_or_not_found_users")
          end

          can_execute
        end

        def execute_action
          @user.user_achievements.create(
            resource: @challenge,
            achievement: Achievement.find_by(action: :challenge_colleague)
          )
          User.where(id: merged_user_ids).each do |user|
            notify_user user
          end
        end

        def notify_user user
          create_system_notification_async(
            sender_user: @user,
            receiver_user: user,
            notification_type: :challenge,
            notificable: @challenge
          )
        end

        def success_runned_action?
          merged_user_ids.size > 0
        end

        def user_can_execute_action?
          true
        end

        def valid_record?
          valid_object?(@challenge, ::Challenge)
        end

        def record_error_key
          :challenge
        end

        def user_ids
          @user_ids ||= array_values_from_params(@options, :user_ids).map(&:to_i).uniq
        end

        def delivered_users_ids
          @delivered_users_ids ||= @challenge.delivered_users.where(id: user_ids).pluck(:id)
        end

        def merged_user_ids
          @merged_user_ids ||= (following_users + followed_users).uniq - delivered_users_ids
        end

        def following_users
          @user.subscribers_users.where(id: user_ids).pluck(:id)
        end

        def followed_users
          @user.users_subscribed.where(id: user_ids).pluck(:id)
        end
      end
    end
  end
end
