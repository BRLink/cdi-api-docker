module CDI
  module V1
    module Challenges
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::ChallengeParams

        record_type ::Challenge

        def build_record
          Challenge.new(challenge_params)
        end

        def can_create_record?
          unless valid_delivery_date?
            return forbidden_error!("#{record_error_key}.delivery_date_must_be_in_the_future")
          end
          return @user.admin?
        end

        def valid_delivery_date?
          delivery_date = Date.iso8601(challenge_params[:delivery_date])
          delivery_date > DateTime.now
        end

        def after_success
          create_presentation
          publish_if_first
        end

        def create_presentation
          @record.create_presentation user_id: @user.id
        end

        def publish_if_first
          if Challenge.count == 1
            PublishService.new(@record, @user).execute
          end
        end
      end
    end
  end
end
