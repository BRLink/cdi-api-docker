# encoding: UTF-8

module CDI
  module V1
    module ChatMessages
      class DeleteService < BaseDeleteService

        record_type ::ChatMessage

        # users that can delete the message
        #   1. owner of the message
        #   2. admin of the room



      end
    end
  end
end
