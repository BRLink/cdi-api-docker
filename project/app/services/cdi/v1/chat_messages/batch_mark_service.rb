# encoding: UTF-8

module CDI
  module V1
    module ChatMessages
      class BatchMarkService < BaseActionService

        action_name :batch_chat_message_mark_read

        attr_reader :chat_message_reads

        def initialize(chat_room, user, options={})
          @chat_room = chat_room
          @user = user
          super(options)
        end

        def execute_action
          chat_messages = ChatMessage.where(chat_room_id: @chat_room).
                            where.not(user_id: @user).
                            where.not(id: ChatMessageRead.where(user: @user).
                                            select('distinct chat_message_id'))

          @chat_message_reads = chat_messages.map do |message|
            message.chat_message_reads.create(user: @user)
          end
        end

        private

        def success_runned_action?
          @chat_message_reads && @chat_message_reads.all?(&:valid?)
        end

        def action_errors
          @chat_message_reads.map(&:errors).map(&:full_messages)
        end

        def user_can_execute_action?
          @chat_room.all_users.include? @user
        end

        def valid_record?
          valid_object?(@chat_room, ::ChatRoom)
        end

        def after_success
        end
      end
    end
  end
end
