module CDI
  module V1
    module DiscussionAnswers
      class CreateService < BaseCreateService

        record_type ::DiscussionAnswer
        alias :answer :discussion_answer

        def initialize(discussion, user, options)
          super user, options
          @discussion = discussion
        end

        def can_create_record?
          valid_discussion?
        end

        def valid_discussion?
          @discussion.present?
        end

        def build_record
          @discussion.answers.build(answer_params)
        end

        def attributes_hash
          @options[:answer]
        end

        def answer_params
          return @answer_params if @answer_params.present?
          @answer_params = filter_hash(attributes_hash, [:option_id])
          @answer_params[:user] = @user
          @answer_params
        end

        def discussion
          @record.discussion
        end
      end
    end
  end
end
