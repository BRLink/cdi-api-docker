# encoding: UTF-8

module CDI
  module V1
    module GameQuestionsAnswers
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::GameQuestionsAnswersParams
        include V1::Games::GameProgressTrackMethods

        record_type ::GameQuestionAnswer, alias_name: :answer

        alias :answers :answer

        private
        # def validate_ip_on_create?
        #   true
        # end

        def can_create_record?
          return false unless valid_user?

          unless valid_game?
            return not_found_error!('games.not_found')
          end

          unless valid_game_question?
            return not_found_error!('game_questions.not_found')
          end

          unless valid_game_question_option?
            return not_found_error!('game_question_options.not_found')
          end

          return can_create_answer?
        end

        def can_create_answer?
          if @user.student?
            return student_user_can_access_game?
          end

          return @user.backoffice_profile?
        end

        def student_user_can_access_game?
          return false unless valid_game?

          @user.enrolled_tracks.joins(:presentation_slides).exists?(presentation_slides: {
            associated_resource_id: game_id,
            associated_resource_type: "Game#{game_type.to_s.classify}"
          })
        end

        def after_success
          register_user_progress_for_track
        end

        def cache_keys_to_expire_after_success
        end

        def matched_cache_keys_to_expire_after_success
          game_type = game.class.to_s.underscore.sub(/\Agame_/, '')
          game_id = game.id

          # clear game_data for current_user
          %W(game_user_data_for_user_id_#{@user.id}_game_type_#{game_type}_game_id_#{game_id})
        end

        def answer_params
          game_question_answers_params.merge(user_id: @user.id)
        end

        def build_record
          if multiple_answers?
            game_question_options.map do |option|
              game_question.answers.build(answer_params.merge(option_id: option.id))
            end
          else
            game_question.answers.build(answer_params)
          end
        end

        def save_record
          if multiple_answers?
            @record.map(&:save).all?(&:present?)
          else
            super
          end
        end

        def create_error_response(record)
          if multiple_answers?
            records = record.select {|r| !r.valid? }

            records.map(&:errors).map do |error|
              option_id = error.instance_variable_get("@base").option_id
              messages  = error.full_messages.to_sentence
              "#{option_id} #{messages}"
            end
          else
            super
          end
        end
      end
    end
  end
end
