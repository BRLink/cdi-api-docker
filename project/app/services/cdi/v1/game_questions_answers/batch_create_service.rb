# encoding: UTF-8

module CDI
  module V1
    module GameQuestionsAnswers
      class BatchCreateService < BaseActionService

        action_name :batch_game_question_answers_options_create

        attr_reader :answers, :services

        def initialize(game, user, options = {})
          @game, @user = game, user
          @answers = []
          @services = []

          super(options)
        end

        def execute_action
          answers_hash.each do |question_id, answer_data|
            service = create_answer_with_service(question_id.to_s, answer_data.symbolize_keys)
            answer = service.answer

            @answers << answer
            @services << service
          end
        end

        def valid_answers
          @answers.compact.select(&:persisted?) || []
        end

        private
        # better than one query per item
        def find_game_question(question_id)
          @game_questions ||= @game.questions
          # converting to string cuz question_id can be a Symbol
          @game_questions.find {|item| item.id == question_id.to_s.to_i }
        end

        def create_answer_with_service(question_id, answer_data, options = {})

          options = @options.slice(:origin)
                    .merge(
                      answer: {
                        question_id: question_id,
                        game_type: 'true_false',
                        game_id: @game.id
                      }.merge(answer_data)
                    )
                    .deep_merge(options)


          service = GameQuestionsAnswers::CreateService.new(@user, options)
          service.execute

          service
        end

        def answers_hash
          attributes_hash
        end

        def success_runned_action?
          valid_answers.any?
        end

        def action_errors
          model_errors = @answers.compact.map(&:errors).map(&:full_messages).flatten
          service_errors = @services.compact.map(&:errors).flatten

          [model_errors, service_errors].flatten.compact.uniq
        end

        def user_can_execute_action?
          return student_user_can_access_game? if @user.student?

          return @user.backoffice_profile?
        end

        def student_user_can_access_game?
          return false unless valid_record?

          @user.enrolled_tracks.joins(:presentation_slides).exists?(presentation_slides: {
            associated_resource_id: @game.id,
            associated_resource_type: @game.class.to_s
          })
        end

        def attributes_hash
          @options[:answers]
        end

        def record_error_key
          :game_questions_answers
        end

        def after_success
          @response_status = 201
        end

        def valid_record?
          valid_object?(@game, ::GameQuiz) || valid_object?(@game, ::GameTrueFalse)
        end
      end
    end
  end
end
