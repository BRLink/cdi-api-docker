module CDI
  class BaseUpdateService < BaseCrudService

    def execute
      if can_execute_action?

        set_history_user

        track_attributes_changes

        @updated_record = update_record
        @updated_record ||= @record

        if success_updated?
          track_record_versions
          success_response
          expire_cache_for_resource
        else
          errors = update_errors
          bad_request_error(errors) if errors.present?
        end
      end

      success?
    end

    def changed_attributes
      return [] if fail?
      @changed_attributes ||= (
        changes(@old_record, @record, changed_attributes_array)
      ).concat((@last_attributes_values ||= {}).keys)
    end

    private
    def changes(old, current, attributes = {})
      record_changes = super(old, current, attributes)

      record_changes.map(&:to_s).uniq
    end

    # This method is useful to track record associations updates
    def track_attributes_changes
      return @last_attributes_values if @last_attributes_values

      @last_attributes_values = {}

      attrs_to_check = (record_params || {}).keep_if do |k, v|
        v.present? && non_change_trackable_attributes.exclude?(k.to_sym)
      end.keys

      attrs_to_check.each do |_attr|
        current_value = @last_attributes_values[_attr] || @record.send(_attr)
        new_value     = record_params[_attr]

        # TODO: Check if we keep this
        # normalize data to compare more precise
        current_value = current_value.map(&:to_s) if current_value.is_a?(Array)
        new_value = new_value.map(&:to_s) if new_value.is_a?(Array)

        if current_value.is_a?(DateTime)
          new_value = DateTime.parse(new_value)
        elsif current_value.is_a?(Date)
          new_value = Date.parse(new_value)
        end

        if current_value.to_s != new_value.to_s
          @last_attributes_values[_attr] = [current_value, new_value]
        end
      end

      @last_attributes_values
    end

    def non_change_trackable_attributes
      []
    end

    def set_history_user
      if @record.respond_to?(:history_user) && valid_user?
        @record.history_user = @user
      end
    end

    def changed_attributes_array
      @_changed_attributes_array ||= changes(@old_record, @record, record_params.keys)
    end

    def success_updated?
      @updated_record.valid?
    end

    def update_errors
      @updated_record.errors
    end

    def update_record
      prm = record_update_params(record_params)
      @record.class.send(:update, @record.id, prm)
    end

    def attributes_to_clear
      return @_attrs_to_clear if @_attrs_to_clear

      attrs = array_values_from_string(@options[:_clear_attrs] || '')

      whitelist = whitelist_attributes_to_clear.map(&:to_sym)

      @attributes_to_clear = attrs.select {|_attr| whitelist.member?(_attr.to_sym) }.map(&:to_sym)
    end

    def record_update_params(record_params)
      attributes_to_clear.each {|param| record_params[param.to_sym] = nil }

      record_params
    end

    def whitelist_attributes_to_clear
      []
    end

    def can_execute_action?

      unless valid_record?
        return not_found_error!("#{record_error_key}.not_found")
      end

      unless valid_user?
        return not_found_error!('users.not_found')
      end

      unless can_update?
        if @errors.blank?
          return forbidden_error!("#{record_error_key}.user_cant_update")
        else
          return false
        end
      end

      return true
    end

    def can_update?
      return false unless valid_user?
      return false unless valid_record?

      user_can_update?
    end

    def user_can_update?
      @record.user_can_update?(@user)
    end

    def track_record_versions
      @old_record = @record.dup
      @record     = @updated_record
    end

    def needs_cache_expiration?
      changed?
    end

  end
end
