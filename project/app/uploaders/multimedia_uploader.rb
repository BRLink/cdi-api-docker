class MultimediaUploader < CarrierWave::Uploader::Base

  configure do |config|
    config.remove_previously_stored_files_after_update = false
  end

	storage (Rails.env.development? or Rails.env.test?) ? :file : :fog

  before :cache, :set_flag

	def store_dir
		"uploads/#{Rails.env.to_s}/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
	end

	def extension_white_list
		%w(ppt pptx doc docx xls xlsx pdf mp4 mp3 wav mov jpg jpeg png gif)
	end

  def set_flag opt
    model.flag = 'file'
  end

end
