# encoding: UTF-8

module API
  module Helpers
    module V1
      module SurveysHelpers

        extend Grape::API::Helpers

        def paginated_subscriber_surveys(surveys_list, options={})
          response_data = surveys_list.map do |subscriber_survey|
            if subscriber_survey.survey.is_a? SurveyPoll
              serialized_object(subscriber_survey.survey, options.merge(serializer: :survey_poll))
            elsif subscriber_survey.survey.is_a? SurveyOpenQuestion
              serialized_object(subscriber_survey.survey, options.merge(serializer: :survey_open_question))
            end
          end
          { meta: pagination_meta, surveys: response_data }
        end
      end
    end
  end
end
