# encoding: UTF-8

module API
  module Helpers
    module V1
      module GamesHelpers

        extend Grape::API::Helpers

        include GameQuizzesHelpers
        include GamePollsHelpers

        VALID_GAME_TYPES = [
          :quiz,
          :poll,
          :complete_sentence,
          :word_search,
          :open_question,
          :true_false,
          :related_item,
          :group_item
        ]

        def find_question_by_game_type(game_type, game_id, question_id = nil)
          game_type = "game_#{game_type.to_s.pluralize}"

          game = current_user.send(game_type).find_by(id: game_id)
          question = nil

          if game.respond_to?(:questions)
            question = game.questions.includes(:options).find_by(id: question_id)
          else game.respond_to?(:question)
            question = game.question
          end

          question
        end

        def game_response_for_user(user, game, options = {})
          custom_method = "serialized_#{game.class.to_s.underscore}_response_for_user"

          if self.respond_to?(custom_method, true)
            self.send(custom_method, user, game, options)
          else
            serialized_simple_game_response_for_user(user, game, options)
          end
        end

        def serialized_simple_game_response_for_user(user, game, options = {})
          serialized_game = serialized_game(game).as_json
          user_game_data = user_answers_data_for_game(user, game, options)

          serialized_game.merge!(user_data: user_game_data.as_json)

          serialized_game
        end

        def user_answers_data_for_game(user, game, options = {})
          game_status_method_name = "#{game.class.to_s.singularize.underscore}_response_for_user"

          (self.try(game_status_method_name, user, game, options) || {})
        end

        def serialized_game_related_item_response_for_user(user, game, options = {})
          serialized_game = serialized_game(game).as_json

          user_game_data = user_answers_data_for_game_related_item(user, game, options)
          serialized_game.merge!(user_data: user_game_data.as_json)

          serialized_game
        end

        def user_answers_data_for_game_related_item(user, game, options = {})
          user_game_data = user_answers_data_for_game(user, game, options)

          wrong_answers = user_game_data[:answers].select {|answer| answer[:correct] == false }

          elegible_items = game.items.dup.to_a

          wrong_answers.each do |wrong_answer|
            answer = wrong_answer[:reply_word]
            wrong_item = elegible_items.find {|item| item.related_word.downcase == answer.downcase }

            eligible_wrongs = wrong_answers.select {|answer| answer[:game_item_id] == wrong_item.id }
            eligible = eligible_wrongs.find {|answer| answer[:_marked].blank? }

            if eligible.nil?
              wrong_answer[:related_item_id] = wrong_item.try(:id) || wrong_item[:id]
            else
              eligible[:_marked] = true
              elegible_items.delete(wrong_item)

              wrong_answer[:related_item_id] = eligible[:game_item_id]
            end
          end

          user_game_data
        end

        def game_poll_response_for_user(user, game, options = {})
          options_percent = {}
          user_response   = last_game_question_response_for_user(user, game, options).as_json

          options_percent = game.question.try(:options_answers_percent) || {}

          user_response.merge(options_answers_percent: options_percent)
        end

        def game_quiz_response_for_user(user, game, options = {})
          response = last_game_question_response_for_user(user, game, options)

          answer_options = {
            serializer: options[:serializer] || :simple_game_question_answer
          }

          answers = serialized_array(game.answers.where(user_id: user.id), answer_options).as_json
          answers = answers.group_by {|a| a[:game_question_id] } if options[:group_answers].present?

          response.merge(answers: answers)
        end

        def game_true_false_response_for_user(user, game, options = {})
          game_question_answers_response_for_user(user, game, options)
        end

        def game_open_question_response_for_user(user, game, options = {})
          game_question_answers_response_for_user(user, game, options)
        end

        def game_question_answers_response_for_user(user, game, options = {})
          user_responses = user.all_responses_for_game(game)

          options.reverse_merge!(
            serializer: :simple_game_question_answer,
            root: :answers
          )

          serialized_array(user_responses, options).as_json
        end

        def game_word_search_response_for_user(user, game, options = {})
          game_word_response_for_user(user, game, options)
        end

        def game_complete_sentence_response_for_user(user, game, options = {})
          game_word_response_for_user(user, game, options)
        end

        def game_group_item_response_for_user(user, game, options = {})
          answers = game.groups.map do |group|
            group_answer = game_word_response_for_user(user, group, options.merge(root: nil, serializer: :game_group_answer))
          end

          return {} unless answers.any?(&:present?)

          { group_answers: answers }
        end

        def game_word_response_for_user(user, game, options = {})
          response = {}

          if answer = user.last_response_for_game(game)
            data = serialized_object(answer, serializer: options[:serializer] || :game_word_answer).as_json
            # remove useless info
            data.delete(:game); data.delete(:user)
            if options[:root] == nil
              response.merge!(data)
            else
              response.merge!(answer: data)
            end
          end

          response
        end

        def game_related_item_response_for_user(user, game, options = {})
          game_question_answers_response_for_user(user, game, options.merge(serializer: :simple_game_item_answer))
        end

        def last_game_question_response_for_user(user, game, options = {})
          user_last_response = user.last_response_for_game(game)

          response = {
            last_replied_question_id: user_last_response.try(:game_question_id),
            last_replied_option_id: user_last_response.try(:option_id)
          }.merge(options)
        end

        def service_name_for_game(game_type, service_type)
          validate_game_type!(game_type)

          "Game#{game_type.to_s.pluralize.titleize}::#{service_type.titleize}Service"
        end


        def find_game(user, game_id, game_type, include_associations = true)
          if user.student?
            find_game_for_student(user, game_id, game_type, include_associations)
          else
            find_game_for_backoffice_profile(user, game_id, game_type, include_associations)
          end
        end

        def find_game_for_student(user, game_id, game_type, include_associations)
          validate_game_type!(game_type)

          game_scope = "tracks_game_#{game_type.to_s.pluralize}"

          if include_associations
            user.send(game_scope)
                .includes(associations_include_options(game_type))
                .find_by(id: game_id)
          else
            user.send(game_scope).find_by(id: game_id)
          end
        end

        def find_game_for_backoffice_profile(user, game_id, game_type, include_associations)
          validate_game_type!(game_type)
          game_klass = "Game#{game_type.to_s.classify}".constantize

          if include_associations
            game_klass.includes(associations_include_options(game_type))
                      .find_by(id: game_id)
          else
            game_klass.find_by(id: game_id)
          end
        end

        def find_public_backoffice_game(current_user, game_id, game_type)
          validate_game_type!(game_type)

          return nil unless current_user.backoffice_profile?

          game_class = "Game#{game_type.to_s.camelize}".constantize

          game_class.includes(associations_include_options(game_type)).find_by(id: game_id)
        end

        def associations_include_options(game_type)
          validate_game_type!(game_type)

          includes = {
            quiz: { questions: [:options] },
            poll: { question: [:options] },
            true_false: { questions: [:options] },
            open_question: { questions: [] },
            related_item: { items: [] },
            complete_sentence: {},
            word_search: {},
            group_item: { groups: [] }
          }[game_type.to_sym]
        end

        def serialized_game(game, options = {})
          options = { serializer: game_serializer(game) }.merge(options)
          serialized_object(game, options)
        end

        def serialized_answers(answers, options = {})
          options = { serializer: :game_question_answer }.merge(options)
          serialized_array(answers, options)
        end

        def game_serializer(game)
          game.class.to_s.underscore
        end

        def validate_game_type!(game_type)
          game_type = game_type.to_s.singularize.to_sym
          raise "Invalid game type: #{game_type}" unless VALID_GAME_TYPES.member?(game_type)
        end

        def response_for_batch_answer_service(service, options = {})
          options.merge!(root: :answers)

          answers = service.answers
          any_answered = answers && answers.any? {|a| a.persisted? }

          status any_answered ? 201 : 400

          if any_answered
            answer = serialized_answers(answers, options)
            errors = service.errors

            answer.as_json.merge(errors: errors)
          else
            error_response_for_service(service)
          end
        end
      end
    end
  end
end
