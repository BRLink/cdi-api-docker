# encoding: UTF-8

module API
  module Helpers
    module V1
      module DiscussionsHelpers

        extend Grape::API::Helpers

        params :new_discussion do
          requires :discussion, type: Hash do
            requires :title, type: String
            requires :category_ids
            requires :visibility_type, type: String
            optional :visibility_id, type: Integer
            optional :content, type: String
            optional :image, type: Integer
            optional :video_link, type: String
            optional :cover_id, type: Integer
            optional :tags
          end
        end

        def paginated_subscriber_discussions(discussions_list, options={})
          response_data = discussions_list.map do |subscriber_discussion|
            if subscriber_discussion.discussion.is_a? DiscussionPoll
              serialized_object(subscriber_discussion.discussion, options.merge(serializer: :discussion_poll))
            elsif subscriber_discussion.discussion.is_a? DiscussionOpenQuestion
              serialized_object(subscriber_discussion.discussion, options.merge(serializer: :discussion_open_question))
            end
          end
          { meta: pagination_meta, discussions: response_data }
        end
      end
    end
  end
end
