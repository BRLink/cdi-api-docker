# encoding: UTF-8

module API
  module Helpers
    module V1
      module PresentationSlidesHelpers

        extend Grape::API::Helpers

        def serialized_presentation_slide(presentation_slide, options = {})
          options = { serializer: :presentation_slide }.merge(options)
          serialized_object(presentation_slide, options)
        end

        def paginated_serialized_presentation_slides(presentation_slides, options = {})
          options = {
            serializer: :simple_presentation_slide,
            root: :presentation_slides
          }.merge(options)

          collection = paginate(presentation_slides).includes(:presentation)

          paginated_serialized_array(collection, options)
        end

      end
    end
  end
end
