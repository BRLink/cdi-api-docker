module API
  module Helpers
    module V1
      module NotificationConfigsHelpers

        extend Grape::API::Helpers

        def notification_params
          {
            can_email: params[:can_email],
            can_notification: params[:can_notification]
          }
        end

      end
    end
  end
end
