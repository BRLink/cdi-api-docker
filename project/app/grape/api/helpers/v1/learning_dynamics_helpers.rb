# encoding: UTF-8

module API
  module Helpers
    module V1
      module LearningDynamicsHelpers

        extend Grape::API::Helpers

        def serialized_learning_dynamic(learning_dynamic, options = {})
          options = { serializer: :learning_dynamic }.merge(options)
          serialized_object(learning_dynamic, options)
        end

        def paginated_serialized_learning_dynamics(dynamics, options = {})
          options = {
            serializer: :simple_learning_dynamic,
            root: :dynamics
          }.merge(options)

          default_includes = [:categories, :user, :student_presentation, :multiplier_presentation]
          includes = options.delete(:includes) || default_includes

          collection = paginate(dynamics).includes(*includes)

          paginated_serialized_array(collection, options)
        end

        def find_slide_for_presentation(user, presentation_type, slide_id, dynamic = nil)
          dynamic ||= user.learning_dynamics.find_by(id: params[:id])

          presentation = dynamic.send(presentation_type)

          return nil unless presentation

          presentation.slides.find_by(id: slide_id)
        end

        def find_learning_dynamic(current_user, dynamic_id, &block)
          dynamic = current_user.learning_dynamics.find_by(id: dynamic_id)
          if dynamic
            yield dynamic
          else
            not_found_error_response(:learning_dynamics)
          end
        end

        def find_learning_dynamic_slide(user, presentation_type, dynamic_id, slide_id, &block)
          find_learning_dynamic user, dynamic_id do |dynamic|
            slide = find_slide_for_presentation(user, presentation_type, slide_id, dynamic)
            yield slide
          end
        end

        def find_slide_params(presentation_type)
          [current_user, presentation_type, params[:id], params[:slide_id]]
        end

      end
    end
  end
end
