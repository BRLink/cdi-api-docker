# encoding: UTF-8

module API
  class Base < Grape::API

    prefix 'api' if CDI::Config.enabled?(:prefix_api_path)

    format :json
    formatter :json, Grape::Formatter::ActiveModelSerializers

    include Grape::Kaminari
    include API::Helpers::ApplicationHelpers

    before do
      set_locale
      set_origin
    end

    get :health_check do
      status 200

      {
        health_check: 'ok'
      }
    end

    mount API::V1::Base
  end
end
