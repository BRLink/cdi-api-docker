# encoding: UTF-8

module API
  module V1
    class Comments < API::V1::Base

      before do
        authenticate_user
      end

      namespace :comments do
        Comment.resource_types.each do |key, resource_type|
          namespace key do

            route_param :id do
              paginated_endpoint do
                desc "List all comments (paginated) from a given resource(#{resource_type}) (aka. post, learning_track)"
                get do
                  # TODO: Limit read access
                  klass = resource_type.constantize
                  resource = klass.find_by(id: params[:id])

                  unless resource
                    return not_found_error_response(klass.to_s.underscore)
                  end

                  comments = paginate(resource.nested_comments)
                  paginated_serialized_array(comments, root: :comments, serializer: :comment)
                end
              end

              desc "Create a comment in a given resource(#{resource_type}) (aka. post, learning_track)"
              params do
                requires :comment, type: Hash do
                  requires :content, type: String
                  optional :parent_id, type: Integer
                end
              end
              post do
                klass = resource_type.constantize
                resource = klass.find_by(id: params[:id])

                service = execute_service('Comments::CreateService', resource, current_user, params)
                response_for_create_service(service, :comment, serializer: :simple_comment)
              end
            end
          end
        end

        route_param :id do
          namespace :children do
            paginated_endpoint do
              desc 'List all comments (paginated), children of another comment.'
              get do
                comment = current_user.comments.find_by(id: params[:id])

                unless comment
                  return not_found_error_response('comment')
                end

                comments = paginate(comment.children_comments)
                paginated_serialized_array(comments, root: :comments, serializer: :embed_nested_comment)
              end
            end
          end

          desc 'Get comment by id'
          get do
            comment = Comment.find_by(id: params[:id])
            unless comment
              return not_found_error_response('comment')
            end
            serialized_object(comment, serializer: :comment, root: :comment)
          end

          desc 'Create a comment, child of other comment.'
          params do
            requires :comment, type: Hash do
              requires :content, type: String
            end
          end
          post do
            resource = Comment.find_by(id: params[:id]).try(:resource)
            params[:comment].merge!({ parent_id: params[:id] })

            service = execute_service('Comments::CreateService', resource, current_user, params)
            response_for_create_service(service, :comment, serializer: :simple_comment)
          end

          desc 'Update a comment from a given resource (aka. post, learning_track).'
          params do
            requires :comment, type: Hash do
              requires :content, type: String
            end
          end
          put do
            comment = current_user.comments.find_by(id: params[:id])

            service = execute_service('Comments::UpdateService', comment, current_user, params)
            response_for_update_service(service, :comment, serializer: :simple_comment)
          end

          desc 'Delete a comment from a given resource (aka. post, learning_track)'
          delete do
            comment = current_user.comments.find_by(id: params[:id])

            service = execute_service('Comments::DeleteService', comment, current_user)
            response_for_delete_service(service, :comment, serializer: :simple_comment)
          end
        end
      end
    end
  end
end
