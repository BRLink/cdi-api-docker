module API
  module V1
    class LearningTracksGeneratePdf < API::V1::Base
      before do
        authenticate_user
      end

      namespace :learning_tracks do
        route_param :id do
          namespace :downloadable do
            get do
              Shoryuken::Client.queues("#{Rails.env}_images_to_pdf").send_message({user_id: current_user.id, track_id: params[:id].to_s}.to_json)
            end
          end
        end
      end
    end
  end
end
