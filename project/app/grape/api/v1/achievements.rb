module API
  module V1
    class Achievements < API::V1::Base

      before do
        authenticate_user
      end

      namespace :achievements do
        paginated_endpoint do
          desc 'List all achievements'
          get do
            achievements = paginate Achievement.all
            paginated_serialized_array achievements, root: :achievements,
                                               serializer: :achievement
          end

          desc 'List all achievements granted to the current user'
          get :history do
            achievements = paginate UserAchievement.history_for current_user
            paginated_serialized_array achievements, root: :user_achievements,
                                               serializer: :user_achievement
          end

          namespace :ranking do
            desc 'Overall ranking, without institutions'
            get do
              users = paginate MaterializedViews::GeneralUserRanking.all
              paginated_serialized_array users, root: :ranking,
                                          serializer: :ranking
            end

            namespace :institution do
              desc 'Ranking by institution'
              route_param :id do
                get do
                  institution = EducationalInstitution.find_by(id: params[:id])
                  unless institution
                    return not_found_error_response(EducationalInstitution.to_s.underscore)
                  end
                  users = paginate MaterializedViews::GeneralUserRanking.by_institution(institution).override_position
                  paginated_serialized_array users, root: :ranking,
                                              serializer: :ranking
                end
              end
            end

            namespace :student_class do
              desc 'Ranking by student class'
              route_param :id do
                get do
                  student_class = StudentClass.find_by(id: params[:id])
                  unless student_class
                    return not_found_error_response(StudentClass.to_s.underscore)
                  end
                  users = paginate MaterializedViews::GeneralUserRanking.by_student_class(student_class).override_position
                  paginated_serialized_array users, root: :ranking,
                                              serializer: :ranking
                end
              end
            end
          end
        end # END OF PAGINATED ENDPOINTS

        namespace :ranking do
          desc 'Returns ranking with 2 users above and 2 users below'
          get :personal do
            ranking = MaterializedViews::GeneralUserRanking.around_user(current_user)
            serialized_array ranking, root: :ranking,
                                serializer: :ranking
          end
        end
        # namespace :score do
        #   namespace :institution do
        #     route_param :id do
        #       desc 'User score for a particular educational institution'
        #       get do
        #         {
        #           rank: 'NOT_IMPLEMENTED_YET',
        #           score: current_user.score_by_institution(EducationalInstitution.find_by(id: params[:id])),
        #           badges: 'NOT_IMPLEMENTED_YET'
        #         }
        #       end
        #     end
        #   end
        # end

      end
    end
  end
end

