module API
  module V1
    class ChallengesDeliveries < API::V1::Base

      before do
        authenticate_user
      end

      namespace :challenges do
        route_param :id do
          namespace :deliveries do
            paginated_endpoint do
              desc 'List paginated deliveries'
              get do
                challenge = Challenge.find_by id: params[:id]
                unless challenge
                  return not_found_error_response :challenge
                end
                deliveries = paginate(challenge.deliveries)
                paginated_serialized_array deliveries, root: :deliveries, serializer: :challenge_delivery
              end
            end

            desc 'Get delivery by id'
            get ":delivery_id" do
              challenge = Challenge.find_by id: params[:id]
              unless challenge
                return not_found_error_response :challenge
              end

              delivery = challenge.deliveries.find_by(id: params[:delivery_id])
              unless delivery
                return not_found_error_response :challenge_delivery
              end

              serialized_object(delivery, serializer: :challenge_delivery, root: :delivery)
            end

            desc 'Create delivery challenge'
            params do
              requires :delivery, type: Hash do
                requires :title
                requires :description
                optional :user_ids
                optional :cover_image
                optional :cover_description
                optional :video_link
                optional :file
              end
            end
            post do
              challenge = Challenge.open.find_by id: params[:id]
              unless challenge
                return not_found_error_response :challenge
              end
              service = execute_service('ChallengeDeliveries::CreateService', challenge, current_user, params)
              response_for_create_service service, :challenge_delivery
            end
          end
        end
      end
    end
  end
end
