# encoding: UTF-8

module API
  module V1
    class SubscribersGroups < API::V1::Base

      before do
        authenticate_user
      end

      namespace :subscribers_groups do

        paginated_endpoint do
          desc 'List all subscribers groups of authenticated user.'
          get do
            groups = paginate(current_user.subscribers_groups)
            paginated_serialized_array(groups, { root: :subscribers_groups, serializer: :subscribers_group })
          end
        end

        desc 'Create a group of subscribers for future submission of posts.'
        params do
          requires :subscribers_group, type: Hash do
            requires :name, type: String
          end
          requires :members, type: Array
        end
        post do
          service = execute_service("SubscribersGroups::CreateService", current_user, params)
          response_for_create_service(service, :subscribers_group)
        end

        route_param :id do

          desc 'Update a group of subscribers.'
          params do
            requires :subscribers_group, type: Hash do
              requires :name, type: String
            end
          end
          put do
            group = current_user.subscribers_groups.find_by(id: params[:id])
            service = execute_service("SubscribersGroups::UpdateService", group, current_user, params)
            response_for_update_service(service, :subscribers_group)
          end

          desc 'Destroy the group of subscribers.'
          delete do
            group = current_user.subscribers_groups.find_by(id: params[:id])
            service = execute_service("SubscribersGroups::DeleteService", group, current_user, params)
            response_for_delete_service(service, :subscribers_group)
          end
        end
      end
    end
  end
end
