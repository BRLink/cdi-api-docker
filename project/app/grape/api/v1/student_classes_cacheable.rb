# encoding: UTF-8

module API
  module V1
  class StudentClassesCacheable < API::V1::Base

      helpers API::Helpers::V1::StudentClassesHelpers
      helpers API::Helpers::V1::LearningTracksHelpers

      before do
        authenticate_user
      end

      with_cacheable_endpoints :classes do

        route_param :id do

          paginated_endpoint do
            desc 'Get all tracks of StudentClass'
            get :tracks do
              student_class = find_student_class_for_current_user(id: params[:id])

              if student_class
                response_for_paginated_endpoint 'student_class.tracks', params[:id] do
                  tracks = student_class.learning_tracks.order(updated_at: :desc)

                  options = {
                    serializer: :student_class_learning_track
                  }

                  paginated_serialized_learning_tracks(tracks, options).as_json
                end
              else
                not_found_error_response(:student_classes)
              end

            end
          end

          paginated_endpoint do
            desc 'Get all tracks of StudentClass belogings to current_user'
            get :my_tracks do
              student_class = find_student_class_for_current_user(id: params[:id])

              if student_class
                response_for_paginated_endpoint 'student_class.my_tracks', params[:id] do
                  tracks = current_user.learning_tracks
                                       .joins(:student_classes)
                                       .where('student_classes.id': student_class.id)
                                       .order(updated_at: :desc)

                  options = {
                    serializer: :student_class_learning_track
                  }

                  paginated_serialized_learning_tracks(tracks, options).as_json
                end
              else
                not_found_error_response(:student_classes)
              end

            end
          end

          paginated_endpoint do
            desc 'Get all students/users of StudentClass'
            get :students do
              student_class = find_student_class_for_current_user(id: params[:id])

              if student_class
                response_for_paginated_endpoint 'student_class.students', params[:id] do
                  students = student_class.students

                  options = {
                    serializer: :simple_user,
                    paginate: true,
                    root: :students
                  }

                  paginated_serialized_array(students, options).as_json
                end
              else
                not_found_error_response(:student_classes)
              end
            end
          end
        end
      end
    end
  end
end
