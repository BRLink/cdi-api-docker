# encoding: UTF-8

module API
  module V1
    class Invitations < API::V1::Base

      helpers API::Helpers::V1::UsersHelpers
      helpers API::Helpers::V1::InvitationsHelpers

      namespace :invitation do
        namespace :token do
          desc 'get information from invitation link'
          route_param :token do
            get do
              invitation = Invitation.valid_invitation_with_token(params['token'])
              if invitation.nil?
                not_found_error_response(:invitations)
              else
                valid_invitation_response invitation
              end
            end
          end
        end


      namespace :users do
        desc 'Create a multiplier via invitation'
        params do
          requires :user, type: Hash do
            requires :first_name
            requires :last_name
            requires :birthday
            requires :email
            requires :gender

            optional :password
            optional :password_confirmation
            optional :fb_access_token

            requires :token
            requires :profile_type, values: User::VALID_MULTIPLIER_PROFILES_TYPES.keys.map(&:to_s) + [:staff]
            requires :educational_institution_id, type: Integer
          end

          requires :provider, values: Authorization::PROVIDERS.keys.map(&:to_s)
        end

        post do
          invitation = Invitation.valid_invitation_with_token(params['user']['token'])
          if invitation.nil?
            response = invalid_invitation_response
          else
            params[:user].merge!(email: invitation.email)

            # if invitation role is superuser or staff, then user can not change it
            # regular user are not allowed to promote self to admin
            if ['superuser', 'staff'].include? invitation.profile_type.to_s
              params[:user].merge!(profile_type: invitation.profile_type)
            elsif params[:user][:profile_type].present? and ['superuser', 'staff'].include? params[:user][:profile_type].to_s
                return forbidden_error_response(:invitations)
            end
            service = execute_service('Users::CreateService', nil, params)

            if service.success?
              invitation.use
              response = user_success_response_for_service(service)
            else
              status service.response_status
              response = error_response_for_service(service)
            end
          end

          response
        end
      end
      end
    end
  end
end
