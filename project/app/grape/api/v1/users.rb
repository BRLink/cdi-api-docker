# encoding: UTF-8

module API
  module V1
    class Users < API::V1::Base

      helpers API::Helpers::V1::UsersHelpers

      namespace :users do
        desc 'Create a new user'
        params do
          requires :user, type: Hash do
            requires :classes_codes, allow_blank: false
            requires :first_name
            requires :last_name
            requires :birthday
            requires :email
            requires :gender

            optional :password
            optional :password_confirmation
            optional :fb_access_token
          end

          requires :provider, values: Authorization::PROVIDERS.keys.map(&:to_s)
        end

        post do
          # only students signup in this endpoint
          params[:user].merge!(profile_type: User::VALID_PROFILES_TYPES[:student])

          service = execute_service('Users::CreateService', nil, params)

          if service.success?
            response = user_success_response_for_service(service)
          else
            status service.response_status
            response = error_response_for_service(service)
          end

          response
        end
      end

    end
  end
end
