# encoding: UTF-8

module API
  module V1
    class SubscribersGroupMembers < API::V1::Base

      before do
        authenticate_user
      end

      namespace :subscribers_groups do

        route_param :id do

          namespace :members do
            paginated_endpoint do
              desc 'List all members of a given group.'
              get do
                group = current_user.subscribers_groups.find_by(id: params[:id])
                unless group
                  return not_found_error_response(group.class.to_s.underscore)
                end
                members = paginate(group.users_members)
                paginated_serialized_array(members, {
                  root: :members,
                  serializer: :simple_user
                })
              end
            end

            desc 'Add members in a group.'
            params do
              requires :members, type: Array
            end
            post do
              group = current_user.subscribers_groups.find_by(id: params[:id])
              unless group
                return not_found_error_response(SubscribersGroup.to_s.underscore)
              end
              service = execute_service("SubscribersGroupMembers::CreateService", group, current_user, params)
              service_response = {}
              if service.success?
                created_members = serialized_array(service.created_members.map(&:user), {
                  serializer: :simple_user
                })
                service_response = { members: created_members }
              end
              response_for_service(service, service_response)
            end

            route_param :user_id do
              desc 'Delete a member by the user.'
              delete do
                group = current_user.subscribers_groups.find_by(id: params[:id])
                unless group
                  return not_found_error_response(group.class.to_s.underscore)
                end
                service = execute_service("SubscribersGroupMembers::DeleteService", group, current_user, params)
                simple_response_for_service(service)
              end
            end
          end
        end
      end
    end
  end
end
