# encoding: UTF-8

module API
  module V1
    class StudentDeliveries < API::V1::Base

      helpers API::Helpers::V1::StudentDeliveriesHelpers

      before do
        authenticate_user
      end

      namespace :student_deliveries do
        route_param :id do
          get do
            delivery = StudentDelivery.includes(:students).find_by(id: params[:id])

            if delivery.blank?
              return not_found_error_response(:student_deliveries)
            end

            unless delivery.students.include?(current_user) or current_user.backoffice_profile?
              return forbidden_error_response(:student_deliveries)
            end

            serialized_object(delivery, serializer: :multiplier_single_student_delivery)
          end
        end
      end

      namespace :tracks do
        route_param :id do
          namespace :student_delivery do

            desc 'Send a Delivery for LearningTrack as student'
            params do
              requires :delivery, type: Hash do
                requires :class_id, type: Integer, allow_blank: false
                optional :file, :type => Rack::Multipart::UploadedFile
                optional :link

                mutually_exclusive :file, :link
              end
            end
            post do
              (params[:delivery] ||= {}).merge!(
                student_class_id: params[:delivery][:class_id],
                learning_track_id: params[:id]
              )

              service = execute_service('StudentDeliveries::CreateService', current_user, params)
              response_for_create_service(service, :student_delivery)
            end
          end
        end
      end

      with_cacheable_endpoints :tracks do
        route_param :id do

          desc 'Get \'Delivery\' for LearningDelivery as student'
          get :student_delivery do
            cache_replace = [params[:id], current_user.id]

            respond_with_cacheable('student_deliveries.show', cache_replace) do
              delivery = current_user.group_students_deliveries.find_by(learning_track_id: params[:id])
              if delivery
                options = {
                  serializer: :student_delivery
                }
                serialized_object(delivery, options).as_json
              else
                not_found_error_response(:student_deliveries)
              end
            end
          end

          desc 'Get \'LearningDelivery\' for LearningTrack'
          get :delivery do
            respond_with_cacheable 'learning_deliveries.track_show', params[:id] do
              track = LearningTrack.find_by(id: params[:id])
              if track
                if delivery = track.learning_delivery
                  serialized_object(delivery, serializer: :learning_delivery).as_json
                else
                  not_found_error_response(:learning_deliveries)
                end
              else
                not_found_error_response(:learning_tracks)
              end
            end
          end

          paginated_endpoint do
            desc 'Get all Deliveries made by students'
            get :student_deliveries do
              only_backoffice_allowed!

              track = current_user.find_editable_learning_track(params[:id])

              if track
                response_for_paginated_endpoint 'learning_tracks.all_student_deliveries', 'all' do
                  options = {
                    serializer: :multiplier_student_delivery,
                    root: :student_deliveries
                  }

                  collection = paginate(track.student_deliveries)
                               .includes(:student_class, :learning_delivery, :learning_track,
                                         :user, :students, :review)

                  paginated_serialized_array(collection, options)
                end
              else
                not_found_error_response(:learning_tracks)
              end
            end
          end
        end
      end

      namespace :student_deliveries do
        route_param :id do
          desc 'Review StudentDelivery'

          params do
            requires :review, type: Hash do
              requires :correct, type: Boolean
            end
          end

          post :review do
            only_backoffice_allowed!

            (params[:review] ||= {}).merge!(
              student_delivery_id: params[:id]
            )

            service = execute_service('StudentDeliveryReviews::CreateService', current_user, params)
            response_for_create_service(service, :review, serializer: :student_delivery_review)
          end

          desc 'Mark StudentDelivery as correct/incorrect'
          params do
            requires :correct, type: Boolean
          end
          put :correct do
            student_delivery = current_user.received_student_deliveries.find_by(id: params[:id])
            params[:delivery] = params.slice(:correct)

            service = execute_service('StudentDeliveries::UpdateService', student_delivery, current_user, params)
            response_for_update_service(service, :student_delivery)
          end
        end
      end
    end
  end
end
