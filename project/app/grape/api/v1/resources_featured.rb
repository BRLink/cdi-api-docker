module API
  module V1
    class ResourcesFeatured < API::V1::Base

      before do
        authenticate_user
      end

      ResourceFeatured::VALID_RESOURCES_TYPES.keys.each do |key|
        namespace key do
          route_param :id do

            namespace :featured do
              desc 'Mark a given resource as featured.'
              post do
                klass = ResourceFeatured::VALID_RESOURCES_TYPES[key].constantize
                resource = klass.find_by(id: params[:id])
                params.merge!(resource_type: klass.to_s)
                service = execute_service("ResourcesFeatured::CreateService", resource, current_user, params)
                simple_response_for_service(service)
              end

              desc 'Remove featured mark from a given resource.'
              delete do
                resource_type = ResourceFeatured::VALID_RESOURCES_TYPES[key]
                resource_featured = current_user.resources_featured.find_by(resource_id: params[:id], resource_type: resource_type)
                service = execute_service("ResourcesFeatured::DeleteService", resource_featured, current_user, params)
                simple_response_for_service(service)
              end
            end
          end
        end
      end
    end
  end
end
