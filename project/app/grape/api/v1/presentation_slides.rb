# encoding: UTF-8

module API
  module V1
    class PresentationSlides < API::V1::Base

      helpers API::Helpers::V1::PresentationSlidesHelpers
      helpers API::Helpers::V1::SharedParamsHelpers
      helpers API::Helpers::V1::SharedServiceActionsHelpers

      before do
        authenticate_user
      end

      namespace :slides do
        desc 'Create a new PresentationSlide for Presentation'
        params do
          use :new_presentation_slide
          group :slide, type: Hash do
            optional :presentation_id
            optional :learning_track_id

            mutually_exclusive :presentation_id, :learning_track_id
          end
        end
        post do
          new_presentation_slide_service_response(current_user, params)
        end

        route_param :id do
          desc 'Update presentation slide'
          params do
            requires :slide, type: Hash do
            end
          end
          put do
            slide = current_user.find_editable_slide(params[:id])

            service = execute_service('PresentationSlides::UpdateService', slide, current_user, params)
            response_for_update_service(service, :presentation_slide)
          end

          desc 'Delete slide'
          delete do
            slide = current_user.find_editable_slide(params[:id])

            service = execute_service('PresentationSlides::DeleteService', slide, current_user, params)
            response_for_delete_service(service, :presentation_slide)
          end

          desc 'Reorder a slide inside a presentation'
          params do
            use :slide_reorder
          end
          put :reorder do
            slide = current_user.find_editable_slide(params[:id])
            slide_reorder_service_response(current_user, slide, params)
          end

          desc "Upload slide thumb"
          params do
            use :new_file_upload
          end
          post :thumb_upload do
            slide = current_user.find_editable_slide(params[:id])
            new_presentation_slide_thumb_upload_service_response(current_user, slide, params)
          end
        end
      end

    end
  end
end
