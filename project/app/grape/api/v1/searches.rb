# encoding: UTF-8

module API
  module V1
    class Searches < API::V1::Base

      helpers API::Helpers::V1::SearchesHelpers

      SUPPORT_FILTER_BY_DATE = [:discussions, :posts]
      STUDENT_SEARCHABLE_TYPES = [
        'User',
        'BlogArticle',
        'EducationalInstitution',
        'Challenge',
        'Discussion',
        'Post'
      ]

      with_cacheable_endpoints :search do

        desc 'Perform a global search'
        params do
          requires :q, type: String
        end
        paginated_endpoint do
          get do
            authenticate_user
            search_text = params[:q]

            if current_user.backoffice_profile?
              collection  = SearchDocument.search(search_text)
            else
              collection  = SearchDocument.where(searchable_type: STUDENT_SEARCHABLE_TYPES).search(search_text)
            end
            results = paginate(collection)
            paginated_serialized_array(results, serializer: :search_result, root: :results)
          end
        end

        SearchDocument::VALID_SEARCHABLE_TYPES.keys.each do |key|
          desc "Perform a search in objects kind of #{SearchDocument::VALID_SEARCHABLE_TYPES[key]}"
          params do
            requires :q, type: String
            optional :from_date, type: String
          end
          paginated_endpoint do
            get key do
              authenticate_user
              type = SearchDocument::VALID_SEARCHABLE_TYPES[key]
              if current_user.student? && STUDENT_SEARCHABLE_TYPES.exclude?(type)
                return forbidden_error_response(key)
              end

              search_text = params[:q]
              collection  = SearchDocument.where(searchable_type: type).search(search_text)

              if SUPPORT_FILTER_BY_DATE.include?(key) && params[:from_date].present?
                from_date  = DateTime.iso8601(params[:from_date])
                collection = collection.joins(type.underscore.to_sym).where("#{key}.created_at >= ?", from_date)
              end

              results = paginate(collection)
              paginated_serialized_array(results, serializer: :search_result, root: :results)
            end
          end
        end

        namespace :me do

          desc 'search for users who is student of any current user StudentClass'
          params do

            requires :name, type: String, regexp: /[^\s]{1,}/, allow_blank: false
          end
          paginated_endpoint do
            get :class_friends do
              authenticate_user

              cache_replace = [current_user.id, params[:name].parameterize]

              response_for_paginated_endpoint 'searchs.users_class_friends', cache_replace do
                searched_users = current_user.classes_friends.search_by_full_name(params[:name])

                users = paginate(searched_users)

                paginated_serialized_array(users, serializer: :simple_user, root: :users).as_json
              end
            end
          end

          ## classes_teachers
          desc 'search for users who is admin of any current user StudentClass'
          params do
            # requires :name, type: String, regexp: /[^\s]{1,}/, allow_blank: false
            optional :name, type: String, regexp: /[^\s]{1,}/, allow_blank: false
          end
          paginated_endpoint do
            get :class_teachers do
              authenticate_user
              param_name = (params[:name].present? ? params[:name] : 'empty').parameterize
              cache_replace = [current_user.id, param_name]

              response_for_paginated_endpoint 'searchs.users_class_teachers', cache_replace do
                if params[:name].blank?
                  searched_users = current_user.classes_teachers
                else
                  searched_users = current_user.classes_teachers.search_by_full_name(params[:name])
                end

                users = paginate(searched_users)

                paginated_serialized_array(users, serializer: :simple_user, root: :users).as_json
              end
            end
          end

          # students_from_administred_classes
          desc 'search for users from current user administered classes'
          params do
            # requires :name, type: String, regexp: /[^\s]{1,}/, allow_blank: false
            optional :name, type: String, regexp: /[^\s]{1,}/, allow_blank: false
          end
          paginated_endpoint do
            get :classes_students do
              authenticate_user
              param_name = (params[:name].present? ? params[:name] : 'empty').parameterize
              cache_replace = [current_user.id, param_name]

              response_for_paginated_endpoint 'searchs.classes_students', cache_replace do
                if params[:name].blank?
                  searched_users = current_user.students_from_administred_classes
                else
                  searched_users = current_user.students_from_administred_classes.search_by_full_name(params[:name])
                end

                users = paginate(searched_users)

                paginated_serialized_array(users, serializer: :simple_user, root: :users).as_json
              end
            end
          end

          desc 'search current_user administered StudentClass'
          params do
            requires :q, type: String, regexp: /[^\s]{1,}/, allow_blank: false
          end
          paginated_endpoint do
            get :classes do
              authenticate_user

              cache_replace = [current_user.id, params[:q].parameterize]

              response_for_paginated_endpoint 'searchs.multiplier_classes_by_term', cache_replace do
                founded_classes = current_user.administered_student_classes.search_by_term(params[:q])

                founded_classes = paginate(founded_classes)

                paginated_serialized_array(founded_classes, serializer: :simple_student_class, root: :classes).as_json
              end
            end
          end

          desc 'search an user into a list of following and followed users'
          params do
            requires :q, type: String, regexp: /[^\s]{1,}/, allow_blank: false
          end
          paginated_endpoint do
            get :following_followed_users do
              authenticate_user

              search_text = params[:q]
              publishers  = UserSubscription.where(user_id: current_user.id, publisher_type: 'User').select(:publisher_id)
              subscribers = UserSubscription.where(publisher_id: current_user.id, publisher_type: 'User').select(:user_id)
              collection = SearchDocument.search(search_text)
                                   .where(searchable_type: 'User')
                                   .where("(searchable_id IN (?) OR searchable_id IN (?))",
                                    publishers,
                                    subscribers
                                  )
              results = paginate(collection)
              paginated_serialized_array(results, serializer: :search_result, root: :results)
            end
          end
        end

        # Users
        namespace :users do
          paginated_endpoint do
            desc 'search for users with given name'
            params do
              requires :name, allow_blank: false
            end

            get :by_type do
              profile_types = profile_types_for_search(params)

              replace_data = cache_data_for_user_search(profile_types, params)
              cache_key = profile_types.all? ? 'users_by_name_and_profile_type' : 'users_by_name'

              respond_with_cacheable("searchs.#{cache_key}", replace_data) do
                docs  = SearchDocument.users
                if profile_types.all?
                  docs = docs.joins(:user).where(users: {profile_type: profile_types})
                end
                docs  = paginate(docs.search(params[:name]))
                users = docs.map(&:searchable)
                paginated_serialized_array(users, serializer: :simple_user, root: :users).as_json
              end
            end
          end
        end

      end
    end
  end
end
