# encoding: UTF-8

module API
  module V1
    class UsersMe < API::V1::Base

      helpers API::Helpers::V1::UsersHelpers
      helpers API::Helpers::V1::SharedParamsHelpers
      helpers API::Helpers::V1::SharedServiceActionsHelpers

      before do
        authenticate_user
      end

      namespace :users do
        namespace :me do

          desc 'Flag notification as read'
          put '/notifications/:id' do
            notification = current_user.notifications.unread.find_by(id: params[:id])

            if notification.try(:mark_as_read)
              status 204
            else
              status 403
            end
          end

          params do
            requires :fb_access_token
          end
          desc 'Connect facebook account with current user system account'
          put :connect_fb do
            service = execute_service('Users::FacebookLinkService', current_user, params)
            simple_response_for_service(service)
          end

          desc 'Disconnect facebook account from current user system account'
          put :desconnect_fb do
            service = execute_service('Users::FacebookUnlinkService', current_user, params)
            simple_response_for_service(service)
          end

          desc 'Update user data'
          params do
            requires :user, type: Hash
          end
          put do
            service = execute_service('Users::UpdateService', current_user, current_user, params)

            options = {
              serializer: :current_user,
              # this will use the same representation in cache if nothing changes to render response
              cache_key: 'current_user.show',
              cache_replace_data: { user_id: service.user.try(:id) }
            }

            response_for_update_service(service, :user, options)
          end

          desc 'Updates user profile picture'
          put :picture do
            service = execute_service('Users::ProfileImageUpdateService', current_user, current_user, params)

            response_for_update_service(service, :user)
          end

          namespace :learning_cycles do
            params do
              use :new_learning_cycle
            end
            post do
              new_learning_cycle_service_response(current_user, params)
            end

            route_param :id do
              params do
                use :new_learning_track
              end
              post :learning_tracks do
                params[:learning_track].merge!(learning_cycle_id:  params[:id])

                new_learning_track_service_response(current_user, params)
              end
            end
          end

          namespace :gallery do
            desc 'Upload file to slide (and save in user gallery)'
            params do
              use :new_file_upload
            end
            post :upload do
              metadata_filter = array_values_from_params(params, :metadata_attributes) || []
              options = {
                meta: {
                  attributes_whitelist: metadata_filter
                }
              }

              user_gallery_new_file_upload_service_response(current_user, params, options)
            end

            namespace :assets do
              route_param :id do
                delete do
                  album = current_user.default_gallery_album
                  if album
                    asset = album.photos.find_by(id: params[:id])

                    service = execute_service('UserGalleries::AssetDeleteService', asset, current_user, params)
                    response_for_delete_service(service, :photo, serializer: :simple_user_gallery_asset)
                  else
                    not_found_error_respomse(:gallery_album)
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
