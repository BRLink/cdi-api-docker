module API
  module V1
    class DiscussionsViews < API::V1::Base

      before do
        authenticate_user
      end

      post '/discussions/:id/views' do
        discussion = Discussion.find_by(id: params[:id])
        unless discussion
          return not_found_error_response(:discussion)
        end
        service = execute_service('DiscussionViews::CreateService', discussion, current_user, params)
        simple_response_for_service(service)
      end
    end
  end
end
