# encoding: UTF-8

module API
  module V1
    class LearningCycles < API::V1::Base

      helpers API::Helpers::V1::LearningCyclesHelpers
      helpers API::Helpers::V1::SharedParamsHelpers
      helpers API::Helpers::V1::SharedServiceActionsHelpers

      before do
        authenticate_user
      end

      namespace :learning_cycles do
        desc 'Create a new cycle for user'
        params do
          use :new_learning_cycle
        end
        post do
          new_learning_cycle_service_response(current_user, params)
        end

        route_param :id do
          # TODO: Group params from .create to use here
          desc 'Updates owned cicle'
          put do
            learning_cycle = current_user.learning_cycles.find_by(id: params[:id])

            service = execute_service('LearningCycles::UpdateService', learning_cycle, current_user, params)
            response = response_for_update_service(service, :learning_cycle)
            if !service.success? && service.categories_in_use.any?
              response.merge! category_ids: service.categories_in_use
            end

            response
          end

          desc 'Delete owned user cycle'
          delete  do
            learning_cycle = current_user.learning_cycles.find_by(id: params[:id])

            service = execute_service('LearningCycles::DeleteService', learning_cycle, current_user, params)
            response_for_delete_service(service, :learning_cycle)
          end
        end

        ## Nested Resources

        route_param :id do
          params do
            use :new_learning_track
          end
          desc 'Creates a new learning track beloging to a specific learning cycle'
          post :learning_tracks do
            params[:learning_track].merge!(learning_cycle_id:  params[:id])

            new_learning_track_service_response(current_user, params)
          end

          # put and post do basically the same thing
          [:put, :post].each do |_method|
            params do
              use :new_cycle_planning
            end
            desc 'Create or update planning beloging to specific learning cycle'

            self.send(_method, :planning) do

              params[:cycle_planning].merge!(learning_cycle_id:  params[:id])

              service = execute_service('CyclePlannings::CreateService', current_user, params)
              response_for_create_service(service, :cycle_planning)
            end
          end
        end
      end
    end
  end
end
