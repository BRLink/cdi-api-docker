# encoding: UTF-8

module API
  module V1
    class Albums < API::V1::Base

      include API::Helpers::V1::AlbumsHelpers

      before do
        authenticate_user
      end

      namespace :users do
        route_param :id do
          namespace :albums do
            paginated_endpoint do
              desc 'List all albums from a given user.'
              get do
                user = User.find_by(id: params[:id])
                return not_found_error_response(User.to_s.underscore) unless user
                albums = paginate(user.albums.visible)
                paginated_serialized_array(albums, root: :albums, serializer: :album)
              end
            end
          end
        end

        namespace :albums do
          desc 'Optional method to create an album'
          params do
            requires :album, type: Hash do
              requires :title
              optional :description
              optional :position
              optional :default, type: Integer, values: [1]
            end
          end
          post do
            service = execute_service('Albums::CreateService', current_user, params)
            response_for_create_service(service, :album)
          end
        end
      end

      namespace :albums do
        paginated_endpoint do
          desc 'List all albums of current_user.'
          get do
            albums = paginate(current_user.albums.visible)
            paginated_serialized_array(albums, root: :albums, serializer: :album)
          end
        end

        desc 'Create an album'
        params do
          requires :album, type: Hash do
            requires :title
            optional :description
            optional :position
            optional :default, type: Integer, values: [1]
          end
        end
        post do
          service = execute_service('Albums::CreateService', current_user, params)
          response_for_create_service(service, :album)
        end

        route_param :id do
          desc 'Update an album'
          params do
            requires :album, type: Hash do
              optional :title
              optional :description
              optional :position
              optional :default, type: Integer, values: [1]
            end
          end
          put do
            album = current_user.albums.find_by(id: params[:id])
            service = execute_service('Albums::UpdateService', album, current_user, params)
            response_for_update_service(service, :album)
          end

          desc 'Delete an album'
          delete do
            album = current_user.albums.find_by(id: params[:id])
            service = execute_service('Albums::DeleteService', album, current_user, params)
            response_for_delete_service(service, :album)
          end
        end
      end
    end
  end
end
