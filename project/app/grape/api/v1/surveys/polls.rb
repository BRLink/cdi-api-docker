module API
  module V1
    module Surveys
      class Polls < API::V1::Base

        before do
          authenticate_user
        end

        namespace :surveys do
          namespace :polls do
            desc 'Create new survey - Poll'
            params do
              requires :survey, type: Hash do
                requires :title, type: String
                requires :visibility_type, type: String
                optional :visibility_id, type: Integer
                optional :image, type: Integer
                optional :video_link, type: String
              end
            end
            post do
              service = execute_service('SurveyPolls::CreateService', current_user, params)
              response_for_create_service(service, :survey_poll, root: :survey)
            end

            route_param :id do

              desc 'Get survey poll by id'
              get do
                survey = SurveyPoll.find_by(id: params[:id])
                unless survey
                  return not_found_error_response(SurveyPoll.to_s.underscore)
                end
                serialized_object(survey, serializer: :survey_poll, root: :survey)
              end

              desc 'Edit a given survey poll'
              params do
                requires :survey, type: Hash do
                  optional :title, type: String
                  optional :video_link, type: String
                end
              end
              put do
                survey = current_user.survey_polls.find_by(id: params[:id])
                service = execute_service('SurveyPolls::UpdateService', survey, current_user, params)
                response_for_update_service(service, :survey_poll, root: :survey)
              end

              desc 'Delete a given survey poll'
              delete do
                survey = current_user.survey_polls.find_by(id: params[:id])
                service = execute_service('SurveyPolls::DeleteService', survey, current_user, params)
                response_for_delete_service(service, :survey_poll, root: :survey)
              end

              namespace :answers do
                desc 'Answer a survey poll'
                params do
                  requires :answer, type: Hash do
                    requires :option_id, type: Integer
                  end
                end
                post do
                  survey = SurveyPoll.find_by(id: params[:id])

                  unless survey
                    return not_found_error_response(SurveyPoll.to_s.underscore)
                  end

                  service = execute_service('SurveyQuestionAnswers::CreateService', survey.question, current_user, params)
                  response_for_create_service(service, :survey_question_answer, {
                    root: :survey,
                    service_record_method: :survey,
                    serializer: :survey_poll
                  })
                end
              end

            end
          end
        end
      end
    end
  end
end
