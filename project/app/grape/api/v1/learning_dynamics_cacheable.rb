# encoding: UTF-8

module API
  module V1
    class LearningDynamicsCacheable < API::V1::Base

      helpers API::Helpers::V1::LearningDynamicsHelpers
      helpers API::Helpers::V1::PresentationsHelpers
      helpers API::Helpers::V1::PresentationSlidesHelpers

      before do
        authenticate_user
      end

      with_cacheable_endpoints :learning_dynamics do

        route_param :id do

          # TODO: Verify read permissions
          desc 'Get LearningDynamic data'
          get do
            respond_with_cacheable 'learning_dynamics.show', params[:id] do
              learning_dynamic = LearningDynamic
                                 .includes(student_presentation: [:presentation_slides], multiplier_presentation: [:presentation_slides])
                                 .find_by(id: params[:id])
              if learning_dynamic
                serialized_learning_dynamic(learning_dynamic).as_json
              else
                not_found_error_response(:learning_dynamics)
              end
            end
          end

          [:multiplier_presentation, :student_presentation].each do |presentation_type|
            namespace presentation_type do
              desc "Get #{presentation_type} data for LearningDynamic"
              get do

                if ( (presentation_type == :multiplier_presentation) &&
                   (!current_user.backoffice_profile?) )
                  return forbidden_error_response(:presentations)
                end

                respond_with_cacheable "learning_dynamics.#{presentation_type}", params[:id] do
                  learning_dynamic = LearningDynamic.find_by(id: params[:id])
                  if learning_dynamic
                    presentation = learning_dynamic.send(presentation_type)

                    options = {
                      serializer: :learning_dynamic_presentation
                    }
                    serialized_presentation(presentation, options).as_json
                  else
                    not_found_error_response(:learning_dynamics)
                  end
                end
              end

              namespace :slides do
                paginated_endpoint do
                  desc "Get all slides belogings to #{presentation_type}"
                  get do
                    unless current_user.backoffice_profile?
                      return forbidden_error_response(:learning_dynamics)
                    end

                    dynamic = LearningDynamic.find_by(id: params[:id])

                    unless dynamic
                      not_found_error_response(:learning_dynamics)
                    end

                    response_for_paginated_endpoint "learning_dynamics.#{presentation_type}_slides", params[:id] do
                      presentation = dynamic.send(presentation_type)
                      presentation_slides = presentation.slides

                      paginated_serialized_presentation_slides(presentation_slides).as_json
                    end
                  end
                end
              end
            end
          end
        end

        paginated_endpoint do
          desc 'List all avaliable LearningDynamic (only for admin and multipliers)'
          get do
            if current_user.backoffice_profile?
              response_for_paginated_endpoint 'learning_dynamics.all', 'all' do
                dynamics = LearningDynamic.published_or_owned(current_user)

                options = {
                  serializer: :library_learning_dynamic,
                  includes: [:categories, :user, :learning_delivery, :skills]
                }

                paginated_serialized_learning_dynamics(dynamics, options).as_json
              end

            else
              forbidden_error_response(:learning_dynamics)
            end
          end
        end
      end
    end
  end
end
