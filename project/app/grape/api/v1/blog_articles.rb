# encoding: UTF-8

module API
  module V1
    class BlogArticles < API::V1::Base

      helpers API::Helpers::V1::BlogArticlesHelpers

      before do
        authenticate_user
      end


      namespace :blog do
        paginated_endpoint do
          desc "List all blog posts."
          get do
            articles = paginate(BlogArticle.all)

            options = {
              serializer: :simple_blog_article,
              root: :articles
            }

            paginated_serialized_array(articles, options)
          end
        end

        route_param :id do
          desc "Returns a BlogArticle."
          params { requires :id, type: Integer, desc: "Article id" }
          get do
            article = BlogArticle.find(params[:id])
            serialized_object(article, serializer: :blog_article)
          end
        end
      end
    end
  end
end
