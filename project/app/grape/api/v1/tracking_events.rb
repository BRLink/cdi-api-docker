# encoding: UTF-8

module API
  module V1
    class TrackingEvents < API::V1::Base

      helpers API::Helpers::V1::TrackingEventsHelpers

      before do
        authenticate_user
      end

      namespace :tracks do
        route_param :id do

          desc 'Track "LearningTrack" progress for user'
          params do
            requires :slide_id, type: Integer, allow_blank: false
            optional :type, type: Integer,
                            default: ::TrackingEvent::EVENT_TYPES[:watched],
                            values: ::TrackingEvent::EVENT_TYPES.values
          end

          post :events do
            service = execute_service('TrackingEvents::CreateService', current_user, event_params)
            response_for_create_service(service, :event, serializer: :tracking_event)
          end
        end
      end
    end
  end
end
