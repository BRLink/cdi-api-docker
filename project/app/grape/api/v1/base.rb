# encoding: UTF-8

# load Rails.root.join('lib', 'cdi', 'cache.rb') if Rails.env.development?

module API
  module V1
    class Base < API::Base

      version 'v1'

      helpers API::Helpers::V1::ApplicationHelpers
      include API::Helpers::V1::ErrorsHelpers

      mount API::V1::Albums
      mount API::V1::Cities
      mount API::V1::ContactForms

      ## Institutions
      mount API::V1::EducationalInstitutions
      mount API::V1::EducationalInstitutionsActions
      mount API::V1::EducationalInstitutionsTakePartActions
      mount API::V1::EducationalInstitutionsByUsers

      mount API::V1::GamesAll

      mount API::V1::Invitations

      # LearningCycles + LearningTracks + Tracks Contents
      mount API::V1::LearningCycles
      mount API::V1::LearningCyclesCacheable
      mount API::V1::LearningDeliveries
      mount API::V1::StudentDeliveriesGeneratePresentations

      ### Dynamicas
      mount API::V1::LearningDynamics
      mount API::V1::LearningDynamicsCacheable
      mount API::V1::LearningDynamicsGeneratePdf
      mount API::V1::LearningDynamicsPresentationsActions

      ### Tracks
      mount API::V1::LearningTracks
      mount API::V1::LearningTracksRecommendation
      mount API::V1::LearningTracksCacheable
      mount API::V1::LearningTracksActions
      mount API::V1::LearningTracksGeneratePdf

      # Library
      mount API::V1::Libraries
      mount API::V1::LibrariesActions

      mount API::V1::MultimediaActions
      mount API::V1::MultimediaCacheable
      mount API::V1::Others
      mount API::V1::Photos

      # Presentations
      mount API::V1::Presentations
      mount API::V1::PresentationsCacheable
      mount API::V1::PresentationSlides
      mount API::V1::PresentationUpload

      mount API::V1::Reports

      mount API::V1::Searches
      mount API::V1::States

      # Student Classes
      mount API::V1::StudentClasses
      mount API::V1::StudentClassesCacheable
      mount API::V1::StudentClassesActions
      mount API::V1::StudentDeliveries

      # Supportin Courses
      mount API::V1::SupportingCourses
      mount API::V1::SupportingCoursesCacheable
      mount API::V1::SupportingCoursesPresentationsActions

      mount API::V1::TrackingEvents

      # Users
      mount API::V1::Users
      mount API::V1::UsersAuth
      mount API::V1::UsersMe
      mount API::V1::UsersMeCacheable
      mount API::V1::UsersMeMultiplierCacheable
      mount API::V1::UsersSubscriptions
      mount API::V1::UsersSubscriptionsActions
      mount API::V1::PusherAuth
      mount API::V1::Notifications
      mount API::V1::NotificationConfigs

      # Student Users
      mount API::V1::UsersMeStudent
      mount API::V1::UsersMeStudentsCacheable

      # Social api
      mount API::V1::Comments
      mount API::V1::Likes
      mount API::V1::Posts
      mount API::V1::ResourcesFeatured
      mount API::V1::UsersVisibilitiesTypes
      mount API::V1::SubscribersGroups
      mount API::V1::SubscribersGroupMembers
      mount API::V1::UsersPreferences

      mount API::V1::Channels

      # chat
      mount API::V1::ChatMessages

      # Surveys (Pesquisa)
      mount API::V1::SurveysAll
      mount API::V1::UsersSurveys

      # Discussions (Discussão)
      mount API::V1::UsersDiscussions
      mount API::V1::Discussions
      mount API::V1::DiscussionsOptions
      mount API::V1::DiscussionsViews

      # Blog Articles (RSS feed)
      mount API::V1::BlogArticles

      ### For front end client
      mount API::V1::PageStructure::Base

      ### Users Home
      mount API::V1::UsersHome

      # Challenge
      mount API::V1::Challenges
      mount API::V1::ChallengesSubscriptions
      mount API::V1::ChallengesDiscussions
      mount API::V1::ChallengesDeliveries

      # Achievements
      mount API::V1::Achievements

      add_swagger_documentation(
        base_path: "/api",
        hide_format: true,
        hide_documentation_path: true,
        api_version: 'v1'
      )
    end
  end
end
