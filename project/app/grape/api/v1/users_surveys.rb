# encoding: UTF-8

module API
  module V1
    class UsersSurveys < API::V1::Base

      ALLOWED_QUESTION_NAMESPACES = [
        :open_questions,
        :polls
      ]

      helpers API::Helpers::V1::SurveysHelpers

      before do
        authenticate_user
      end

      namespace :surveys do
        paginated_endpoint do
          desc 'List all surveys of current_user.'
          get do
            subscriber_surveys = paginate(current_user.subscriber_surveys)
            paginated_subscriber_surveys(subscriber_surveys)
          end
        end

        ALLOWED_QUESTION_NAMESPACES.each do |survey_type|
          namespace survey_type do
            desc "List surveys of current_user filtered by #{survey_type}."
            paginated_endpoint do
              get do
                subscriber_surveys = paginate(current_user.subscriber_surveys.send(survey_type))
                paginated_subscriber_surveys(subscriber_surveys)
              end
            end
          end
        end
      end
    end
  end
end
