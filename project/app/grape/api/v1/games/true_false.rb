# encoding: UTF-8

module API
  module V1
    module Games
      class TrueFalse < API::V1::Base

        helpers API::Helpers::V1::GamesHelpers

        before do
          authenticate_user
        end

        namespace :games do
          namespace :true_false do
            desc "Create a new game GameTrueFalse for user"
            params do
              requires :game, type: Hash do
                requires :questions, type: Array
              end
            end

            post do
              service = execute_service('GameTrueFalses::CreateService', current_user, params)
              response_for_create_service(service, :game_true_false)
            end

            route_param :id do
              desc "Updates owned game GameTrueFalse"
              put do
                game = find_game(current_user, params[:id], :true_false)

                service = execute_service('GameTrueFalses::UpdateService', game, current_user, params)
                response_for_update_service(service, :game_true_false)
              end

              desc "Delete owned user game GameTrueFalse"
              delete  do
                game = find_game(current_user, params[:id], :true_false)

                service = execute_service('GameTrueFalses::DeleteService', game, current_user, params)
                response_for_delete_service(service, :game_true_false)
              end

              namespace :answers do
                params do
                  requires :answers, type: Hash
                end
                desc 'Send answer for GameTrueFalse'
                post do
                  game = find_game(current_user, params[:id], :true_false)

                  service = execute_service('GameQuestionsAnswers::BatchCreateService', game, current_user, params)
                  success_answers = service.valid_answers

                  response_for_service(service, answers: success_answers)
                end
              end
            end
          end
        end
      end
    end
  end
end
