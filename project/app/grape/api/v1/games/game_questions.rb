# encoding: UTF-8

module API
  module V1
    module Games
      class GameQuestions < API::V1::Base

        helpers API::Helpers::V1::GamesHelpers

        before do
          authenticate_user
        end

        VALID_GAME_ENDPOINTS = [
          :quiz,
          :poll,
          :open_questions,
          :true_false
        ]

        namespace :games do

          VALID_GAME_ENDPOINTS.each do |game_type|
            namespace game_type do
              route_param :id do
                namespace_name = [:poll].exclude?(game_type) ? :questions : :question
                namespace namespace_name do
                  params do
                    requires :question, type: Hash do
                      requires :title
                      optional :options, type: Array # TODO (must be required for poll/quiz)
                    end
                  end

                  desc "Create a new question for Game#{game_type.to_s.classify}"

                  post do
                    params[:question].merge!(game_id: params[:id], game_type: "Game#{game_type.to_s.classify}")

                    service = execute_service('GameQuestions::CreateService', current_user, params)
                    response_for_create_service(service, :game_question)
                  end

                  desc "Replace questions of game #{game_type.to_s.classify} (except for GamePoll)"
                  params do
                    if [:poll].exclude?(game_type)
                      requires :questions, type: Array do
                      end
                    elsif game_type == :poll
                      requires :question, type: Hash
                    end
                  end
                  put :replace do
                    params.merge!(game_id: params[:id], game_type: "Game#{game_type.to_s.classify}")

                    game_scope = "game_#{game_type.to_s.pluralize}"
                    game = current_user.send(game_scope).find_by(id: params[:id])

                    if game
                      if [:poll].exclude?(game_type)
                        service = execute_service('GameQuestions::ReplaceService', game, current_user, params)
                        response_for_create_service(service, :"game_#{game_type.to_s.singularize}")
                      else
                        question = game.question
                        service = execute_service('GameQuestions::UpdateService', question, current_user, params)
                        response_for_update_service(service, :game_question)
                      end
                    else
                      not_found_error_response(game_scope)
                    end

                  end

                  endpoint_block = -> {
                    params do
                      requires :question, type: Hash do
                        optional :options
                      end
                    end
                    desc "Update a question beloging to a game #{game_type.to_s.classify}"
                    put do
                      params[:question] ||= {}
                      params[:question].merge!(game_id: params[:id], game_type: "Game#{game_type.to_s.classify}")

                      game_question_type = "game_#{game_type.to_s.pluralize}_questions"

                      if game_type == :poll
                        game = current_user.game_polls.find_by(id: params[:id])
                        question = game.try(:question)
                      else
                        question = current_user.send(game_question_type).find_by(id: params[:question_id], questionable_id: params[:id])
                      end

                      service = execute_service('GameQuestions::UpdateService', question, current_user, params)
                      response_for_update_service(service, :game_question)
                    end

                    desc "Delete a question beloging to a game #{game_type.to_s.classify}"
                    delete do
                      game_scope = "game_#{game_type.to_s.pluralize}"

                      game = current_user.send(game_scope).find_by(id: params[:id])

                      if game
                        question = game_type == :poll ? game.question : game.questions.find_by(id: params[:question_id])

                        service = execute_service('GameQuestions::DeleteService', question, current_user, params)
                        response_for_delete_service(service, :game_question, serializer: :simple_game_question)
                      else
                        not_found_error_response(game_scope.to_sym)
                      end
                    end
                  }

                  param_name = [:poll].exclude?(game_type) ? :question_id : nil

                  if param_name
                    route_param param_name do
                      endpoint_block.call
                    end
                  else
                    endpoint_block.call
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
