# encoding: UTF-8

module API
  module V1
    class LearningDynamics < API::V1::Base

      helpers API::Helpers::V1::LearningDynamicsHelpers
      helpers API::Helpers::V1::SharedParamsHelpers
#      helpers API::Helpers::V1::SharedServiceActionsHelpers

      before do
        authenticate_user
      end

      namespace :learning_dynamics do

        # / POST
        desc 'Create a new learning dynamic'
        params do
          use :new_learning_dynamics
        end
        post do
          service = execute_service('LearningDynamics::CreateService', current_user, params)
          response_for_create_service(service, :learning_dynamic, options)
        end

        route_param :id do

          # / PUT
          desc 'Update Learning Dynamic'
          params do
            use :update_learning_dynamics
          end
          put do
            dynamic = current_user.learning_dynamics.find_by(id: params[:id])

            service = execute_service('LearningDynamics::UpdateService', dynamic, current_user, params)
            response_for_update_service(service, :learning_dynamic)
          end

          # / DELETE

          desc 'Delete Learning Dynamic'
          delete do
            dynamic = current_user.learning_dynamics.find_by(id: params[:id])

            service = execute_service('LearningDynamics::DeleteService', dynamic, current_user, params)
            response_for_delete_service(service, :learning_dynamic, serializer: :simple_learning_dynamic)
          end

        end
      end
    end
  end
end
