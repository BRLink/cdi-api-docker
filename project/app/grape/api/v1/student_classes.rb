# encoding: UTF-8

module API
  module V1
  class StudentClasses < API::V1::Base

      helpers API::Helpers::V1::StudentClassesHelpers

      before do
        authenticate_user
      end

      namespace :classes do

        # / POST
        desc 'Create a new StudentClass'
        params do
          requires :class, type: Hash do
            requires :name
          end
        end
        post do
          service = execute_service('StudentClasses::CreateService', current_user, params)
          response_for_create_service(service, :student_class)
        end

        route_param :id do

          # / PUT
          desc 'Update StudentClass'
          put do
            student_class = current_user.administered_student_classes.find_by(id: params[:id])

            service = execute_service('StudentClasses::UpdateService', student_class, current_user, params)
            response_for_update_service(service, :student_class, serializer: :simple_student_class)
          end

          # / DELETE

          desc 'Delete StudentClass'
          delete do
            student_class = current_user.administered_student_classes.find_by(id: params[:id])

            service = execute_service('StudentClasses::DeleteService', student_class, current_user, params)
            response_for_delete_service(service, :student_class, serializer: :simple_student_class)
          end

        end
      end
    end
  end
end
