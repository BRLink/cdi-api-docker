# encoding: UTF-8

module API
  module V1
    class LearningTracksCacheable < API::V1::Base

      helpers API::Helpers::V1::LearningTracksHelpers
      helpers API::Helpers::V1::LearningCyclesHelpers
      helpers API::Helpers::V1::StudentClassesHelpers
      helpers API::Helpers::V1::PresentationsHelpers
      helpers API::Helpers::V1::PresentationSlidesHelpers

      before do
        authenticate_user
      end

      with_cacheable_endpoints :learning_tracks do
        paginated_endpoint do
          desc 'Get all published tracks feed for multiplier user'

          get :feed do
            unless current_user.backoffice_profile?
              return forbidden_error_response(:multiplier_learning_cycles_feed)
            end

            response_for_paginated_endpoint 'learning_tracks.multiplier_published_tracks_feed', 'all' do
              categories_ids = categories_ids_from_params
              options = {
                each_serializer_meta: {
                  only_published_learning_tracks: true
                }
              }

              track_filters = { status: ::LearningTrack.status_map[:published] }
              if categories_ids.any?
                # track_filters.merge!(category_resources: {category_id: categories_ids})
                options.deep_merge!({
                  each_serializer_meta: {
                    categories_ids: categories_ids
                  }
                })
              end

              cycles = LearningCycle.joins(learning_tracks: [:category_resources])
                                    .where(learning_tracks: track_filters)
                                    .group('learning_cycles.id')
              if categories_ids.any?
                cycles = cycles.where(category_resources: {category_id: categories_ids})
              end

              # incluido suporte a busca nos ciclos
              if params[:q].present?
                cycles = cycles.learning_track_search(params[:q])
              end

              learning_tracks_by_cycles_feed_response(cycles, false, options)
            end
          end
        end

        route_param :id do

          # FIXME: Cache(respond_with_cacheable)
          desc 'Get LearningTrack data'
          get do
            learning_track = LearningTrack.find_by(id: params[:id])
            response = handle_learning_track_response(learning_track, current_user)

            if response.is_a?(::LearningTrack)
              response = serialized_learning_track(response)
              response.as_json.merge(simple_enrollment_status_for(current_user, params[:id]))
            end
          end

          # FIXME: Cache(respond_with_cacheable)
          desc 'Get presentation data + slides beloging to LearningTrack'
          get :presentation do
            learning_track = LearningTrack.includes(presentation_slides: [:associated_resource])
                                          .find_by(id: params[:id])

            response = handle_learning_track_response(learning_track, current_user)

            if response.is_a?(::LearningTrack)
              options = {
                serializer: :learning_track_presentation
              }

              response = serialized_presentation(response.presentation, options)
            end

            response
          end

          desc 'Get learning tracks questions + answers'
          get :questions do
            learning_track = LearningTrack.find_by(id: params[:id])
            response = handle_learning_track_response(learning_track, current_user)

            if response.is_a?(::LearningTrack)
              questions = response.questions.includes(:template_questionable, :question_template, :answers)

              options = {
                serializer: :learning_track_question,
                root: :questions
              }

              response = serialized_array(questions, options)
            end

            response
          end

          paginated_endpoint do

            # FIXME: Cache(response_for_paginated_endpoint)
            desc 'Get all slides belogings to LearningTrack'
            get :slides do
              learning_track = LearningTrack.find_by(id: params[:id])
              response = handle_learning_track_response(learning_track, current_user)

              if response.is_a?(::LearningTrack)
                response = paginated_serialized_presentation_slides(response.slides, root: :slides)
              end

              response
            end
          end

          paginated_endpoint do
            desc 'Get all authors(moderators) of learning track'
            get :authors do
              learning_track = LearningTrack.find_by(id: params[:id])

              if learning_track
                if learning_track.user_can_read_authors?(current_user)
                  response_for_paginated_endpoint('learning_tracks.authors', learning_track.id) do
                    response_for_track_moderators(learning_track, params)
                  end
                else
                  forbidden_error_response(:learning_tracks)
                end
              else
                not_found_error_response(:learning_tracks)
              end
            end
          end

          paginated_endpoint do
            desc 'Get all received reviews of students for LearningTrack'
            params do
              requires :class_id, type: Integer
            end

            get :reviews do
              class_id = params[:class_id]
              learning_track = current_user.find_editable_learning_track(params[:id])

              if learning_track
                response_for_paginated_endpoint('learning_tracks.reviews', [learning_track.id, class_id]) do
                  reviews = learning_track
                            .reviews
                            .where(student_class_id: class_id)
                            .includes(:user).order(created_at: :desc)

                  options = {
                    serializer: :multiplier_learning_track_review,
                    root: :reviews,
                    paginate: true
                  }

                  paginated_serialized_array(reviews, options).as_json
                end
              else
                not_found_error_response(:learning_tracks)
              end
            end
          end

          paginated_endpoint do
            desc 'Get all users enrolled in LearningTrack by status'
            params do
              requires :class_id, type: Integer
            end
            get :users_by_enrollment_status do

              class_id = params[:class_id]
              learning_track = current_user.find_editable_learning_track(params[:id])

              if learning_track
                enroll_status = array_values_from_params(params, :status)

                cache_options = [learning_track.id, enroll_status.join('_'), class_id]

                response_for_paginated_endpoint('learning_tracks.users_by_enrollment_status', cache_options) do

                  if enroll_status.size == 1 && enroll_status.first == ::EnrolledLearningTrack::STATUS[:not_enrolled]
                    student_class = StudentClass.find_by(id: class_id)
                    students_not_enrolled = student_class.not_enrolled_students(learning_track)

                    users = paginate(students_not_enrolled)
                  else
                    enrolled_users = learning_track.enrolled_users
                                                   .where(enrolled_learning_tracks: {
                                                      status: enroll_status,
                                                      student_class_id: class_id
                                                    })

                    users = paginate(enrolled_users)
                  end


                  options = {
                    serializer: :simple_user,
                    root: :users
                  }

                  paginated_serialized_array(users, options).as_json
                end
              else
                not_found_error_response(:learning_tracks)
              end
            end
          end

          paginated_endpoint do
            get :classes do
              learning_track = LearningTrack.find_by id: params[:id]

              response = handle_learning_track_response(learning_track, current_user)

              if response.is_a?(::LearningTrack)
                student_classes = paginate(response.student_classes.active)

                response = paginated_serialized_student_classes(student_classes).as_json
              end

              response
            end
          end
        end
      end
    end
  end
end
