# encoding: UTF-8

module API
  module V1
    class States < API::V1::Base

      helpers API::Helpers::V1::StatesHelpers

      with_cacheable_endpoints :states do

        desc 'Return a list of all states'
        get do
          respond_with_cacheable('states.all') do
            states_as_json(State.includes(:cities).all).as_json
          end
        end

        desc 'Return info and cities list of given state'
        route_param :id do
          get do
            respond_with_cacheable('states.show', params[:id]) do
              state_info_as_json(params[:id]).as_json
            end
          end

          desc 'Return only cities of given state'
          get :cities do
            respond_with_cacheable('states.cities', params[:id]) do
              state_info_as_json(params[:id], meta: { only_cities: true }).as_json
            end
          end

          # TODO: Paginar este endpoint (futuro)
          desc 'Return all educational_institutions of given state'
          get :educational_institutions do
            respond_with_cacheable('states.educational_institutions', params[:id]) do
              state_educational_institutions_as_json(params[:id]).as_json
            end
          end
        end
      end
    end
  end
end
