class StudentDelivery < ActiveRecord::Base

  include Accessable
  include Historiable

  scope :correct, -> { where(correct: true) }
  scope :wrong, -> { where.not(correct: true) }

  belongs_to :learning_delivery
  belongs_to :user
  belongs_to :student_class
  belongs_to :learning_track
  belongs_to :presentation

  has_one :review, class_name: 'StudentDeliveryReview'
  has_many :reviewer_students, through: :review, source: :student_user

  alias :track :learning_track

  validates :user_id, :learning_delivery_id, presence: true
  validates :learning_delivery_id, uniqueness: { scope: [:user_id] }

  validates :format_type,
            inclusion: LearningDelivery::VALID_FORMAT_TYPES.values.flatten.map(&:to_s),
            presence: true

  validates :link, presence: true, format: { with: URI.regexp }, if: -> {
    self.file.blank?
  }

  validates :file, presence: true, if: -> { self.link.blank? }

  validate :format_type_is_the_same_of_delivery

  mount_uploader :file, StudentDeliveryFileUploader

  after_initialize :set_format_type
  before_save :set_format_type

  has_and_belongs_to_many :students, class_name: 'User', join_table: 'student_delivery_groups'

  delegate :delivery_kind, :group_delivery?, to: :learning_delivery, allow_nil: true


  def user_can_manage?(user)
    return learning_delivery.user_can_manage?(user)
  end

  private
  def custom_data_for_histories
    self.changes.symbolize_keys.slice(:correct, :corrected_at)
  end

  def set_format_type
    self.format_type ||= self.learning_delivery.try(:format_type)
  end

  def format_type_is_the_same_of_delivery
    if self.learning_delivery && (self.format_type != self.learning_delivery.format_type)
      self.errors.add(:format_type, I18n.t('cdi.errors.student_deliveries.format_type_not_equal_delivery_format_type'))
    end
  end
end
