class ResourceFeatured < ActiveRecord::Base

  VALID_RESOURCES_TYPES = {
    posts: 'Post',
    discussions: 'Discussion'
  }

  self.table_name = 'resource_featured'

  include Accessable

  belongs_to :resource, polymorphic: true
  belongs_to :user

  validates :resource, :user, presence: true
  validates :user_id, uniqueness: { scope: [:resource_type, :resource_id] }

  def user_can_access? user
    user.id == self.user_id
  end
end
