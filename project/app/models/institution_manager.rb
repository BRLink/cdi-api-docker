class InstitutionManager < ActiveRecord::Base

  belongs_to :managed_institution, class_name: 'EducationalInstitution', foreign_key: :managed_institution_id
  belongs_to :manager, class_name: 'User', foreign_key: :manager_id

  validates :managed_institution, :manager, presence: true

  validate :validate_user_has_backoffice_profile
  validate :validate_unique_institution_user_combo

  def user_can_delete? user
    managed_institution.user_can_update? user
  end

  private
  def validate_user_has_backoffice_profile
    unless manager.backoffice_profile?
      self.errors.add(:manager, "user does not have backoffice profile")
    end
  end

  def validate_unique_institution_user_combo
    if managed_institution.managers.find_by(id: manager.id) or managed_institution.admin_user == manager
      self.errors.add(:manager, "user is already manager/owner of the institution")
    end
  end
end
