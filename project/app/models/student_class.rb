class StudentClass < ActiveRecord::Base

  include Accessable
  include Codeable
  include Statusable
  include PubsubConcerns::Publisher
  include ReportSQL::StudentClasses
  include ResourceVisibilitable
  include PgSearch
  include AchievementConcerns::StudentClassAchievementable

  STATUS = {
    active: 'active',
    closed: 'closed'
  }

  pg_search_scope :search_by_term,
    against: [
      [:name, 'A'],
      [:code, 'B']
    ],
    using: { tsearch: { prefix: true } }

  scope :valid, -> {
    active.valid_code
  }

  scope :valid_code, -> {
    where('code_expiration_date >= NOW()')
  }

  scope :recent, -> {
    where('created_at > ?', Time.zone.now - 5.minutes)
  }

  status_configure status_hash: STATUS,
                   default_status: :active,
                   validate_status_inclusion: false


  has_one :origin, as: :originable

  belongs_to :admin_user, class_name: 'User'

  belongs_to :educational_institution

  has_many :enrolled_learning_tracks

  has_many :enrolled_users, -> {
    where.not('enrolled_learning_tracks.status': ::EnrolledLearningTrack::STATUS[:not_enrolled])
  }, through: :enrolled_learning_tracks, source: :user


  has_and_belongs_to_many :learning_tracks, join_table: :student_classes_tracks

  alias :tracks :learning_tracks
  alias :tracks= :learning_tracks=

  has_many :learning_cycles, through: :learning_tracks

  has_and_belongs_to_many :users

  has_many :learning_cycles, through: :learning_tracks

  has_many :report, class_name: 'LearningTrackReport'

  alias :students :users

  validates :name, :admin_user, presence: true

  # TODO: status_configure must clear this validation and define again
  validates :status, inclusion: STATUS.values

  after_create :activate_code

  def user_can_access?(user)
    user.admin? || self.admin_user_id == user.id
  end

  # Update/Delete/Create
  def user_can_manage?(user)
    super(user) && user.backoffice_profile?
  end

  def user_can_view_post? u
    admin_user_id == u.id || users.exists?(id: u.id)
  end

  def code_active?
    return false unless [self.code, self.code_expiration_date].all?(&:present?)

    self.code_expiration_date >= Time.zone.now
  end

  def code_expired?
    !code_active?
  end

  def before_migrate_to_closed
    self.code_expiration_date = Time.zone.now
  end

  def before_migrate_to_active
    activate_code(false)
  end

  def activate_code(save = true)
    expiration_days = (CDI::Config.student_class_code_expiration_days || 30).to_i.days
    self.code_expiration_date = Time.zone.now + expiration_days

    save ? self.save : self
  end

  def not_enrolled_students(learning_track)
  student_ids = learning_track.enrolled_users
                  .where('enrolled_learning_tracks.student_class_id' => self.id)
                  .where('enrolled_learning_tracks.status NOT IN (?)', ['not_enrolled'])
                  .pluck(:id)

    users.where.not(id: student_ids)
  end

  def students_count
    self[:students_count].present? ? self[:students_count].to_i : students.size
  end

  def create_not_enrolled_records_for_reports(learning_track)
    students = not_enrolled_students(learning_track)

    students.each do |student|

      enroll_data = {
        user: student,
        learning_track: learning_track,
        status: ::EnrolledLearningTrack::STATUS[:not_enrolled]
      }

      begin
        ::EnrolledLearningTrack.transaction do
          self.enrolled_learning_tracks.create(enroll_data)
        end
      rescue => e
        Rollbar.error(e) # better know if something weird happens here
      end
    end
  end

  protected
  def format_age_range_row(row)
    gender = row.delete('gender')

    data = row.map do |key, value|
      { label: AGE_RANGES[key.to_sym], value: value.to_i }
    end

    {
      gender: gender,
      gender_text: I18n.t(gender, scope: 'app.genders'),
      data: data
    }
  end
end
