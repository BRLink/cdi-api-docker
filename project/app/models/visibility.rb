class Visibility < ActiveRecord::Base

  VALID_VISIBILITY_TYPES = {
    all: "all",
    multipliers: "multipliers",
    students: "students",
    subscribers_group: "SubscribersGroup",
    classes: "StudentClass",
    institutions: "EducationalInstitution",
    users: "User"
  }

  PRESET_VISIBILITY_TYPES = [
    "all",
    "multipliers",
    "students"
  ]

  # ResourceVisibily Concern
  belongs_to :resource, polymorphic: true

  # FIXME por conta do `accepts_nested_attributes_for` não é possível validar o resource.
  # validates :resource, presence: true

  # ResourceVisibilitable Concern
  validates :visibility_type, presence: true, inclusion: VALID_VISIBILITY_TYPES.values
  validates :visibility_id, presence: true, unless: ->{ PRESET_VISIBILITY_TYPES.include? (visibility_type) }

  def all?
    visibility_type == VALID_VISIBILITY_TYPES[:all]
  end

  def multipliers?
    visibility_type == VALID_VISIBILITY_TYPES[:multipliers]
  end

  def students?
    visibility_type == VALID_VISIBILITY_TYPES[:students]
  end

  def preset_visibility?
    PRESET_VISIBILITY_TYPES.include?(visibility_type)
  end

  # ResourceVisibilitable Concern
  def visibility_object
    @visibility_object ||= visibility_type.constantize.find_by(id: visibility_id) unless preset_visibility?
  end

  def name
    visibility_object.try(:name)
  end
end
