class SubscriberDiscussion < ActiveRecord::Base

  belongs_to :user_subscription
  belongs_to :discussion

  validates :user_subscription, :discussion, presence: true
  validates :user_subscription_id, uniqueness: { scope: [:discussion_id] }

  # FIXME há um bug nos relations que,
  # conforme vc vai usando through, ele vai pushando o
  # default_scope de cada relation envolvida na query.
  # comentado temporariamente.
  # default_scope { order('created_at DESC') }
end
