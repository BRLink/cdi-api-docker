class FeedbackContactForm

  extend CarrierWave::Mount
  include ActiveModelable

  attributes :name, :email, :type, :description, :image, :message_body, :origin_ip, :user_agent

  validates :type, :description, :message_body, presence: true

  mount_uploader :image, GenericImageUploader

  def save
    if valid?
      self.store_image!
    end

    valid?
  end

end
