# encoding: UTF-8
class Address < ActiveRecord::Base
  belongs_to :city

  has_one :state, through: :city

  belongs_to :addressable, polymorphic: true

  delegate :state_id, to: :city, allow_nil: true

  validates :street, :district, presence: true
end
