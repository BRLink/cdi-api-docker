class GameWordAnswer < ActiveRecord::Base

  attr_accessor :skip_basic_validation

  belongs_to :user

  belongs_to :game_wordable, polymorphic: true

  scope :correct, -> { where(correct: true) }
  scope :wrong, -> { where.not(correct: true) }

  alias :game :game_wordable
  alias :game= :game_wordable=

  validates :user, :game_wordable_id, :game_wordable_type, presence: true, unless: -> {
    self.skip_basic_validation.present?
  }

  validate :validates_words_length

  before_save :set_correct

  def correct_words
    self.game.correct_words_by_position(self.words || [])
  end

  def wrong_words
    self.game.wrong_words_by_position(self.words || [])
  end

  # TODO: Check why alias :game_id :game_wordable_id is raising an error
  def game_id
    game_wordable_id
  end

  # TODO: Check why alias :game_type :game_wordable_type is raising an error
  def game_type
    game_wordable_type
  end

  private
  def validates_words_length
    if self.game_wordable_type == 'GameGroup'
      self.errors.add(:words, 'must be the same group items size') if self.game.items.size != self.words.size
    end

    if self.game && self.words.size > self.game.words.size
      self.errors.add(:words, "answer cant have more words than game")
    # elsif self.words.blank? && self.game.words
    #   self.errors.add(:words, 'provide at least one word')
    elsif words.empty?
      self.errors.add(:words, "suply at least one word")
    end
  end

  def set_correct
    self.correct = game.correct?(self.words) if self.words
    self
  end
end
