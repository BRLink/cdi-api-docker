# encoding: UTF-8

class UserPreference < ActiveRecord::Base

  include Accessable

  VALID_PREFERENCE_TYPES = [
    "music", "movie", "book", "tv_show"
  ]

  belongs_to :user

  validates :title, presence: true
  validates :preference_type, presence: true, inclusion: VALID_PREFERENCE_TYPES

  VALID_PREFERENCE_TYPES.each do |type|
    scope type, -> { where(preference_type: type) }
  end

  def user_can_access? user
    user.id == self.user_id
  end
end
