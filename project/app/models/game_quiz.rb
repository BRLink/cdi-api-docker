class GameQuiz < ActiveRecord::Base

  include Accessable
  include Statusable
  include Historiable
  include Imageable
  include GameQuestionableClonable
  include GameScorable

  amoeba do
    nullify [:user_id, :status, :status_changed_at]

    exclude_association [:origin, :edition_histories, :answers, :user_scores, :questions]

    # include_association [:questions]
  end

  has_one :origin, as: :originable

  belongs_to :user

  has_many :questions, as: :questionable, class_name: 'GameQuestion'
  has_many :options, through: :questions
  has_many :answers, through: :questions

  validates :user_id, presence: true

  after_initialize :set_defaults

  MAX_OPTIONS_BY_QUESTION = (CDI::Config.game_quiz_question_options_max_limit || 10).to_i
  MAX_QUESTIONS_BY_GAME = (CDI::Config.game_quiz_questions_max_limit || 5).to_i

  private

  def set_defaults
  end

  def custom_data_for_histories
    self.changes.symbolize_keys.slice(:user_id, :status, :status_changed_at)
  end
end
