class GameItem < ActiveRecord::Base

  include Accessable
  include Historiable

  belongs_to :game_related_item, counter_cache: :items_count

  alias :game :game_related_item
  alias :game= :game_related_item=

  has_one :user, through: :game_related_item

  has_many :answers, class_name: 'GameItemAnswer'

  validates :word, :related_word, presence: true

  validates :game_related_item, presence: true, unless: :skip_father_game_validation?

  validates :word, uniqueness: { scope: [:game_related_item_id, :related_word] }, on: :create

  validate :validate_word_is_different_from_related_word
  validate :validate_game_reached_items_limit, on: :create

  attr_accessor :skip_father_game_validation

  # mount_uploader :image, GameItemImageUploader
  # mount_base64_uploader :image, GameItemImageUploader, base_filename: :image

  # mount_uploader :related_image, GameItemImageUploader
  # mount_base64_uploader :related_image, GameItemImageUploader, base_filename: :related_image

  # skip_callback :commit, :after, :remove_image!

  validates :image, format: { with: URI.regexp }, if: -> {
    self.image.present?
  }

  validates :related_image, format: { with: URI.regexp }, if: -> {
    self.related_image.present?
  }

  def skip_father_game_validation?
    return skip_father_game_validation.present?
  end

  [:image, :related_image].each do |image_method|
    define_method "has_uploaded_#{image_method}?" do
      self.send("#{image_method}_url").present? &&
      !self.send("#{image_method}_url").match(/(fallback|default)\.(png|jpg|jpeg)$/)
    end

    define_method "#{image_method}_url" do
      self.send(image_method)
    end

    define_method "has_uploaded_#{image_method}?" do
      self.send(image_method).present?
    end
  end

  private
  def validate_word_is_different_from_related_word
    # in case self.word or self.related_word is nil
    if self.word.to_s.parameterize == self.related_word.to_s.parameterize
      errors.add(:word, I18n.t('cdi.errors.game_related_items.word_can_be_equal_related_word'))
    end

    # keep validations
    true
  end

  def validate_game_reached_items_limit
    if game_related_item
      current_items_count = game_related_item.items_count || 0

      if current_items_count >= ::GameRelatedItem::MAX_ITENS_BY_GAME
        self.errors.add(:game_related_item, I18n.t('cdi.errors.game_related_items.max_items_for_game_reached'))
      end
    end

    true
  end
end
