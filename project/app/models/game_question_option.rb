class GameQuestionOption < ActiveRecord::Base

  include Accessable
  include Positionable

  has_one :origin, as: :originable

  belongs_to :game_question
  alias :question :game_question

  acts_as_list scope: :game_question_id

  validates :text, :game_question, presence: true

  after_initialize :set_defaults

  validates :text, uniqueness: { scope: [:game_question_id] }, if: -> { game_question_id.present? }

  validate :game_question_reached_options_limit, on: [:create]

  amoeba do
    exclude_association [:origin]
  end

  def user_can_update?(user)
    game_question.try(:user_can_update?, user)
  end

  # has_one through not working properly
  def game
    @game ||= self.game_question.try(:questionable)
  end

  def correct?
    self.correct && self.correct == true
  end

  # TODO: User group by
  def percent
    total_options = game.options.size
    total = GameQuestionAnswer.where(game_question_id: self.game_question_id, option_id: self.id).size

    (total * 100) / total_options
  end

  private

  def game_question_reached_options_limit
    if game
      if game.is_a?(GamePoll)
        max_options = GamePoll::MAX_OPTIONS_BY_QUESTION
      elsif game.is_a?(GameQuiz)
        max_options = GameQuiz::MAX_OPTIONS_BY_QUESTION
      elsif game.is_a?(GameTrueFalse)
        max_options = 2
      end

      if max_options && game_question.options.count >= max_options
        self.errors.add(:base, I18n.t('cdi.errors.games.max_options_for_question_reached'))
      end
    end
  end

  def validate_duplicate_option_for_question
    return self if self.text.blank?

    if question.options.where(text: self.text).exists?
      self.errors.add(:base,  'cdi.errors.game_question_options.duplicated_option_for_question')
    end
  end

  def set_defaults
    self.correct ||= false
  end

  def set_position_before_save?
    false
  end
end
