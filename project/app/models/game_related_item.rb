class GameRelatedItem < ActiveRecord::Base

  include Accessable
  include Historiable
  include Statusable
  include GameScorable

  amoeba do
    nullify [:user_id]

    exclude_association [:origin, :edition_histories, :answers, :user_scores]

    include_association [:items]
  end

  MAX_ITENS_BY_GAME = (CDI::Config.game_related_item_max_items_by_game || 10).to_i

  has_one :origin, as: :originable

  has_many :items, class_name: 'GameItem'

  has_many :answers, through: :items

  alias :game_items :items
  alias :game_items= :items=

  belongs_to :user

  after_initialize :set_defaults

  alias :owner_user :user
  alias :owner_user= :user=

  def clone!(user_id)
    duped_object = self.amoeba_dup
    duped_object.user_id = user_id

    duped_object.save

    duped_object
  end

  def max_answers_count_per_user
    1
  end

  def words
    cached_items.map(&:word)
  end

  def related_words
    cached_items.map(&:related_word)
  end

  def words_with_image
    cached_items.map {|item| word_hash_for_grid(item, :word, :image) }
  end

  def related_words_with_image
    cached_items.map {|item| word_hash_for_grid(item, :related_word, :related_image) }
  end

  def word_hash_for_grid(item, word_key, image_method = :image)
    {
      item_id: item.id,
      word: item[word_key.to_sym],
      image_url: item.send("#{image_method}_url"),
      has_uploaded_image: item.send("has_uploaded_#{image_method}?")
    }
  end

  def cached_items
    @cached_items ||= self.items
  end

  def random_game_grid(clear = false, include_images = true)
    @shuffled_game_words = nil if clear

    if include_images
      w, rw = words_with_image, related_words_with_image
    else
      w, rw = words, related_words
    end

    @shuffled_game_words ||= {
      words: w.shuffle,
      related_words: rw.shuffle
    }
  end

  def random_game_grid_for_user(user, clear = false, include_images = true)
    @user_grids ||= {}

    return @user_grids[user.id] if @user_grids[user.id]

    game_grid = random_game_grid(clear, include_images)
    related_words = game_grid[:related_words]
    user_answers = user.all_responses_for_game(self)

    related_words.each do |word|
      word.delete(:item_id) unless user_answers.find {|answer| answer.game_item_id == word[:item_id] }
    end

    @user_grids[user.id] ||= game_grid
  end

  private
  def custom_data_for_histories
    self.changes.symbolize_keys.slice(:user_id, :status, :status_changed_at)
  end

  def set_defaults
  end

end
