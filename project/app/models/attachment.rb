class Attachment < ActiveRecord::Base

  VALID_ATTACHABLE_TYPES = [
    'Post',
    'SurveyOpenQuestion',
    'SurveyPoll',
    'Discussion'
  ]

  VALID_ATTACHMENT_TYPES = {
    :photo => 'Photo',
    :album => 'Album',
    :multimedia => 'Multimedia',
    :challenge => 'Challenge',
    :discussion => 'Discussion'
  }

  belongs_to :attachable, polymorphic: true
  belongs_to :attachment, polymorphic: true

  validates :attachable, :attachment, presence: true
  validates :attachable_type, inclusion: VALID_ATTACHABLE_TYPES
  validates :attachment_type, inclusion: VALID_ATTACHMENT_TYPES.values

  def self.attachment_types
    VALID_ATTACHMENT_TYPES
  end
end
