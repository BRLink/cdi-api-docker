# encoding: UTF-8

class ChatRoom < ActiveRecord::Base

  ROOM_TYPES = {
    one_to_one: 0,
    middle_size: 10
  }
  enum room_type: ROOM_TYPES

  scope :rooms_with_user, ->(u){
    joins(:chat_room_users).where(chat_room_users: {user: u})
  }

  belongs_to :user
  alias :owner :user
  has_many :chat_messages, dependent: :destroy
  alias :messages :chat_messages
  has_many :chat_room_admins, ->{ admin }, class_name: 'ChatRoomUser'
  has_many :admins, through: :chat_room_admins, source: :user
  has_many :chat_room_members, ->{ member }, class_name: 'ChatRoomUser'
  has_many :members, through: :chat_room_members, source: :user
  has_many :chat_room_users
  has_and_belongs_to_many :all_users, join_table: :chat_room_users, class_name: 'User'

  validates :user,  presence: true

  after_save :update_permanent_member

  def last_message
    message = messages.last
    if message.present?
      {
        id: message.id,
        content: message.content,
        created_at: message.created_at,
        updated_at: message.updated_at,
        user_id: message.user_id
      }
    end
  end

  # messages for given user
  def my_received_messages(me)
    chat_messages.where.not(user: me)
  end
  def my_unread_messages(me)
    my_received_messages(me).where.
        not(id: ChatMessageRead.where(user: me).
                  select('distinct chat_message_id'))
  end

  private
  def update_permanent_member
    unless admins.include? owner
      self.admins << owner
    end
  end


end
