# encoding: UTF-8
class Question < ActiveRecord::Base

  belongs_to :template_questionable, polymorphic: true

  amoeba do
    include_association :question_template
    clone [:answers]

    customize(lambda { |original_question,new_question|
      user_id = new_question.template_questionable.user_id
      new_question.answers.each {|answer| answer.user_id = user_id }
    })
  end

  belongs_to :question_template

  has_many :answers, class_name: 'QuestionUserAnswer', foreign_key: :question_id

  alias :user_answers :answers

  has_many :participating_users, through: :answers, source: :user

  delegate :question_type, :title, :instructions, :choices,
           :with_choice?, :subtitle, to: :question_template #, allow_nil: true

  validates :template_questionable_id, :template_questionable_type, :question_template_id, presence: true

  validate :same_question_for_questionable

  QuestionTemplate::QUESTIONS_TYPES.each do |key, _|
    delegate "question_#{key}?", to: :question_template
  end

  def create_answer_for(user, answer, method=:create)
    raise ArgumentError, 'Invalid method' if [:create, :create!, :build, :build!].exclude?(method.to_sym)

    answer_data = format_data_for_answer(answer)

    if answer_data.is_a?(Hash)
      answer_data.merge!(user_id: user.id)
    elsif answer_data.is_a?(Array)
      answer_data.map {|answer_data| answer_data.merge!(user_id: user.id) }
    end

    self.answers.send(method, answer_data)
  end

  def create_answer_for!(user, answer)
    create_answer_for(user, answer, :create!)
  end

  def index_by_answer(answer)
    return nil if self.choices.blank?
    self.choices.index {|choice| choice['answer'] == answer.to_s || choice == answer.to_s }
  end

  def text_by_index(index)
    choice = self.choices.at(index.to_i)
    choice.try(:[], 'answer') || choice
  end

  def choices_count
    self.choices.try(:size) || 0
  end

  def format_data_for_answer(answer)
    if self.question_single_choice?
      answer_data = process_answer_data_for_choices(answer)
    elsif self.question_multiple_choices?
      answer_data = process_answer_data_for_choices(answer)
    else
      answer_data = process_answer_data_for_open_question(answer)
    end

    answer_data
  end

  private
    def same_question_for_questionable
      question_attributes = self.attributes.slice('template_questionable_type', 'template_questionable_id', 'question_template_id')

      if self.class.exists?(question_attributes)
        self.errors.add(:base, "Existent question for #{template_questionable_type}")
      end
    end

    def process_answer_data_for_open_question(answer)
      answer = answer.is_a?(Hash) ? answer.values.first : answer

      {
        answer_text: answer.to_s,
        answer_index: index_by_answer(answer)
      }
    end

    def process_answer_data_for_choices(answer)
      if answer.is_a?(Hash)
        answer_data = process_answer_data_from_hash(answer)
      else
        if self.question_multiple_choices?
          answer_data = process_answer_data_for_multiples_choices(answer)
        else
          answer_data = { answer_text: answer.to_s, answer_index: index_by_answer(answer) }
        end
      end

      answer_data
    end

    def process_answer_data_from_hash(answer)
      if index = answer.delete(:index)
        answer_data = { answer_text: text_by_index(index), answer_index: index.to_i }
      elsif text = answer.delete(:text)
        answer_data = { answer_text: text.to_s, answer_index: index_by_answer(text) }
      end

      answer_data
    end

    def process_answer_data_for_multiples_choices(answer)
      if answer.to_s.match(/\d/).present?
        answers = answer.to_s.split(',').map(&:to_i)
        answer_data = answers.map do |answer_index|
          { answer_text: text_by_index(answer_index), answer_index: answer_index }
        end
      else
        answer_data = []
      end

      answer_data
    end
end
