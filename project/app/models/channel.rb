class Channel < ActiveRecord::Base

  belongs_to :student_class
  belongs_to :learning_track

  EXPIRE_TIME = 1.day

  def name
    "private-class-#{student_class_id}-track-#{learning_track_id}"
  end

  def self.by_name(channel_name)
    class_id, track_id = [channel_name.split('-')[2].to_i, channel_name.split('-')[4].to_i]
    return find_by student_class_id: class_id, learning_track_id: track_id
  end

  def expired?
    updated_at + EXPIRE_TIME < DateTime.current
  end

end
