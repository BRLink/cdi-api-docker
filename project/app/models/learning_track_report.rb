class LearningTrackReport < ActiveRecord::Base

  include Accessable
  include Statusable

  include ReportSQL::LearningTracks::Enrollments
  include ReportSQL::LearningTracks::Reviews
  include ReportSQL::LearningTracks::StudentReports

  STATUS = {
    generated: 'generated',
    updating: 'updating'
  }

  VALID_REPORTS = [
    :enrollment_reports,
    :reviews_reports,
    :students_reports
  ]

  status_configure default_status: :updating,
                   status_hash: STATUS,
                   validate_status_inclusion: false

  belongs_to :learning_track
  belongs_to :student_class


  validates :student_class_id, :learning_track_id, presence: true

  alias :track :learning_track

  attr_accessor :init_report_columns

  after_create :refresh_all_columns

  delegate :user_id, to: :learning_track

  def report_key_for(key, force_refresh = false)
    raise "Invalid #{key}" if VALID_REPORTS.exclude?(key.to_sym)

    self.send(:refresh, key) if force_refresh || expired_key?(key)

    self_data = self.send(key)

    return self_data['report_data'] if self_data.is_a?(Hash)

    JSON.parse self_data.try(:[], 'report_data')
  end

  def refreshable_column?(key)
    raise "Invalid #{key}" if VALID_REPORTS.exclude?(key.to_sym)

    no_content = self.send(key).blank?
    return no_content || expired?
  end

  def expired?
    return status_changed_at && Time.zone.now >= (status_changed_at + CDI::Config.report_cache_time.to_i.seconds)
  end

  def expired_key?(key)
    raise "Invalid #{key}" if VALID_REPORTS.exclude?(key.to_sym)

    updated_at = report_key_updated_at(key)

    return true if updated_at.blank?

    (Time.zone.now >= (updated_at + CDI::Config.report_cache_time.to_i.seconds))
  end

  def report_key_updated_at(key)
    raise "Invalid #{key}" if VALID_REPORTS.exclude?(key.to_sym)

    parsed_data = self.send(key)
    parsed_data = JSON.parse parsed_data.to_json unless parsed_data.is_a?(Hash)

    report_data = parsed_data.try(:[], 'report_data')

    report_data = JSON.parse(report_data) unless report_data.is_a?(Hash)

    return nil if report_data.blank? || report_data['updated_at'].blank?

    Time.zone.parse(report_data['updated_at']) rescue nil
  end

  def refresh(key)
    raise "Invalid #{key}" if VALID_REPORTS.exclude?(key.to_sym)

    self.send("refresh_#{key}_column")
  end

  def refresh_all_columns
    VALID_REPORTS.map {|key| refresh(key) }
  end
end
