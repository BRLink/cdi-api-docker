class BlogArticle < ActiveRecord::Base

  include Commentable
  include Likeable
  include Categorizable
  include Searchable

  searchable_for :title, :categories_names

  validates :title, :link, :description, :pub_date, :guid, presence: true

  # TODO validate presence of :content after the rss has been updated
  validates_uniqueness_of  :guid

  # unlimited categories
  def max_categories
    categories.size
  end
end
