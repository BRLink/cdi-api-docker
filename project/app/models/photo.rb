class Photo < ActiveRecord::Base

  include Accessable
  include Imageable
  include Positionable
  include Taggable
  include PostAttachable
  include Likeable
  include Commentable

  # soft-delete
  acts_as_paranoid

  attr_accessor :create_image_versions, :slide_thumb_upload, :game_item_image_upload

  has_one :origin, as: :originable

  belongs_to :album

  belongs_to :user

  validates :album_id, :image, presence: true

  validates_integrity_of :image
  validates_processing_of :image

  before_save :set_defaults, :set_caption_length

  scope :visible, -> {
    where(visible: true)
  }

  scope :recent, -> {
    order(created_at: :desc)
  }

  MAX_CAPTION_LENGTH = 255

  def owner_user
    self.user || self.album.user
  end

  private
  def set_position
    self.position ||= (self.album.photos.maximum(:position) || 0).next
  end

  def set_caption_length
    self.caption = self.caption[0, MAX_CAPTION_LENGTH] if self.caption?
  end

  def set_defaults
    set_user_id_from_album
    self.visible = true if self.visible.nil?
  end

  def set_user_id_from_album
    self.user_id = self.album.user_id
  end
end
