class GamePoll < ActiveRecord::Base
  include Accessable
  include Statusable
  include Historiable
  include Imageable
  include GameScorable
  include GameQuestionableClonable

  amoeba do
    nullify [:user_id, :status, :status_changed_at]

    exclude_association [:origin, :edition_histories, :answers, :user_scores, :question]

    # include_association [:question]
  end

  has_one :origin, as: :originable

  belongs_to :user

  has_one :question, as: :questionable, class_name: 'GameQuestion'
  has_many :options, through: :question
  has_many :answers, through: :question

  validates :user_id, presence: true

  after_initialize :set_defaults

  MAX_OPTIONS_BY_QUESTION = (CDI::Config.game_poll_question_options_max_limit || 10).to_i
  MAX_QUESTIONS_BY_GAME   = 1

  private
  def set_defaults
  end

  def custom_data_for_histories
    self.changes.symbolize_keys.slice(:user_id, :status, :status_changed_at)
  end
end
