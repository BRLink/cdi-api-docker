class PresentationSlide < ActiveRecord::Base

  include Accessable
  include Historiable
  include Positionable

  amoeba do
    nullify :views_count
  end

  SLIDE_TYPES = {
    :content   => 'content',
    :game      => 'game',
    :dynamic   => 'dynamic',
    :course    => 'course'
  }

  SLIDE_TYPES.each do |type, value|
    scope type, -> { where(slide_type: value)  }

    define_method "#{type}_slide?" do
      self.slide_type == value.to_s
    end
  end

  ASSOCIATED_RESOURCE_TYPES = [
    'GameQuiz',
    'GamePoll',
    'GameCompleteSentence',
    'GameWordSearch',
    'GameOpenQuestion',
    'GameTrueFalse',
    'GameRelatedItem',
    'GameGroupItem',
    'LearningDynamic',
    'SupportingCourse',
    'Multimedia'
  ]

  has_one :origin, as: :originable

  belongs_to :presentation, counter_cache: :slides_count

  acts_as_list scope: :presentation

  has_one :user, through: :presentation
  has_one :learning_track, through: :presentation

  validates :slide_type, presence: true, inclusion: SLIDE_TYPES.values

  validates :layout_type, presence: true, if: :content_slide?
  validates :content, :title, presence: true, if: :content_slide?

  # validates :presentation_id, presence: true

  delegate :user_id, :learning_track_id, :presentation_type, to: :presentation

  belongs_to :associated_resource, :polymorphic => true

  validates :associated_resource_type,
            :inclusion => { :in => ASSOCIATED_RESOURCE_TYPES },
            :allow_nil => true

  def save_thumb_url(thumb_url, save = true, key = 'thumb_url')
    metadata = (self.metadata || {}).stringify_keys
    metadata[key.to_s] = thumb_url
    self.metadata = metadata

    # save to dynamics
    try_update_dynamics_thumb_url thumb_url
    save ? self.save : self
  end

  def thumbnail_url(key = 'thumb_url')
    if associated_resource_type.to_s == 'SupportingCourse'
      return supporting_course_thumbnail_url
    end
    metadata = (self.metadata || {}).stringify_keys
    metadata[key.to_s]
  end

  def supporting_course_thumbnail_url
    associated_resource.try(:thumbnail_url)
  end

  def cover_thumbnail_url
    thumbnail_url('cover_url') || thumbnail_url || student_thumbnail_url || multiplier_thumbnail_url
  end

  def multiplier_thumbnail_url
    thumbnail_url('multiplier_thumbnail_url')
  end

  def student_thumbnail_url
    thumbnail_url('student_thumbnail_url')
  end

  def delete_thumbnail(thumb_url = nil)
    basename = File.basename(thumb_url || thumbnail_url)

    return nil unless basename
    try_update_dynamics_thumb_url thumb_url
    image = Photo.where(visible: false, image: basename).last

    if image
      image.destroy
    end
  end

  def next
    self.class.unscoped.where("presentation_id = ? AND position > ?", presentation_id, position).first
  end

  def previous
    self.class.unscoped.where("presentation_id = ? AND position < ?", presentation_id, position).last
  end

  private
  def try_update_dynamics_thumb_url thumb_url
    unless self.presentation.nil? or
        !self.presentation.dynamic_content_presentation? or
        self.presentation.learning_dynamic.nil?
      self.presentation.learning_dynamic.update_column(:thumb_url, thumb_url)
    end
  end
  def custom_data_for_histories
    self.changes.symbolize_keys.slice(:title, :layout_type, :slide_type,
                                      :position, :content, :metadata)
  end

  def set_position_before_save?
    false
  end

end
