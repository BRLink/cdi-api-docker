# encoding: UTF-8
class Presentation < ActiveRecord::Base

  include Accessable
  include Statusable

  amoeba do
    nullify [:user_id, :file]

    customize(lambda { |was,now|
      now.slides_count = was.slides_count
    })

    clone :presentation_slides

    exclude_association [:visible_slides, :presentation_to_document_task, :conversion_status]

  end

  PRESENTATION_TYPES = {
    :content => 'content',
    :quiz    => 'quiz',
    :game    => 'game',
    :dynamic => 'dynamic',
    # presentation from multiplier & teacher in LearningDynamic
    :dynamic_content => 'dynamic_content'
  }

  PRESENTATION_TYPES.each do |key, value|
    scope "type_#{key}", -> { where(presentation_type: value) }

    define_method("#{key}_presentation?") do
      self.presentation_type == value
    end
  end

  PRESENTATION_TYPES.each do |key, value|
    scope "type_#{key}", -> { where(presentation_type: value) }

    define_method("#{key}_presentation?") do
      self.presentation_type == value
    end
  end

  mount_uploader :file, PresentationUploader

  belongs_to :user

  has_one :learning_track

  has_one :origin, as: :originable

  has_one :learning_dynamic, foreign_key: :multiplier_presentation_id

  has_one :challenge

  has_one :challenge_delivery

  has_one :presentation_to_document_task
  has_one :conversion_status, dependent: :destroy

  has_many :presentation_slides, dependent: :destroy

  has_many :visible_slides, -> { where.not(hidden: true) }, class_name: 'PresentationSlide'

  alias :slides :presentation_slides
  alias :slides= :presentation_slides=

  # has_many :supporting_courses
  validates :user_id, presence: true, if: :content_presentation?

  validates :presentation_type, presence: true, inclusion: PRESENTATION_TYPES.values

  delegate :id, to: :learning_track, prefix: :learning_track, allow_nil: true

  before_validation :set_defaults, :set_user_id

  def clone!(owner_or_id, options = {})
    duped_object = self.amoeba_dup
    duped_object.user_id = owner_or_id.respond_to?(:id) ? owner_or_id.id : owner_or_id
    duped_object.save

    if duped_object.errors.key?(:presentation_slides)
      duped_object.errors.delete(:presentation_slides)
      duped_object.errors.delete(:presentation_slides)
      duped_object.slides.each_with_index do |slide,index|
        slide.errors.each do |name, detail|
          duped_object.errors.add("slide_#{index}_#{name}".to_sym,detail)
        end
      end
    end

    duped_object
  end

  alias :duplicate :clone!

  private
  def set_user_id
    self.user_id ||= self.learning_track.try(:user_id)
  end

  def set_defaults
    self.presentation_type ||= PRESENTATION_TYPES[:content]
  end

end
