class CategoryResource < ActiveRecord::Base
  belongs_to :category
  belongs_to :resource, polymorphic: true

  validates :category_id, presence: true

end
