class GameWordSearch < ActiveRecord::Base

  include Accessable
  include Statusable
  include Historiable
  include GameScorable

  has_one :origin, as: :originable

  belongs_to :user

  has_many :answers, as: :game_wordable, class_name: 'GameWordAnswer'

  validates :user_id, :words, presence: true

  after_initialize :set_defaults

  before_save :set_words

  PARAMETERIZE_WORDS = true

  MAX_WORDS = (CDI::Config.max_game_word_search_words || 5).to_i

  def max_answers_count_per_user
    1
  end

  def words_match?(words = [])
    swords = self.words.map {|w| parameterize_word(w) }
    words  = words.map {|w| parameterize_word(w) }

    swords == words
  end

  def parametized_words
    self.words.map(&:parameterize)
  end

  def correct_words(words = [])
    words.select {|w| self.parametized_words.member?(parameterize_word(w)) }
  end

  def wrong_words(words = [])
    words.select {|w| self.parametized_words.exclude?(parameterize_word(w)) }
  end

  def correct?(words)
    correct_words(words).map(&:parameterize) == parametized_words
  end

  def correct_percent_for_user(user)
    answer = self.answers.find_by(user_id: user.id)

    return 0 if answer.blank?

    ((correct_words(answer.words).size * 100) / self.words.size).round(2)
  end

  alias :correct_words_by_position :correct_words
  alias :wrong_words_by_position :wrong_words

  private
  def parameterize_word(word)
    PARAMETERIZE_WORDS ? word.parameterize : word
  end

  def set_defaults
  end

  def custom_data_for_histories
    self.changes.symbolize_keys.slice(:user_id, :status, :words, :status_changed_at)
  end

  def set_words
    self.words = self.words[0, MAX_WORDS] if self.words
  end
end
