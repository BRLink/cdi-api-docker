# encoding: UTF-8
class State < ActiveRecord::Base

  has_many :cities
  has_many :educational_institutions, through: :cities

  def uf
    self.acronym
  end
end
