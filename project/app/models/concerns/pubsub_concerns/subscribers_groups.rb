module PubsubConcerns
  module SubscribersGroups

    extend ActiveSupport::Concern

    included do
      has_many :subscribers_groups
    end
  end
end
