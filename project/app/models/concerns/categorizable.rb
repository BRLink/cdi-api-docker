module Categorizable
  extend ActiveSupport::Concern

  included do
    has_many :category_resources, as: :resource
    has_many :categories, through: :category_resources
    validate :min_max_categories
  end

  def categories_names
    categories.pluck(:name)
  end

  private

    def min_max_categories
      if categories.size < min_categories
        errors.add :categories, I18n.t('cdi.errors.categories.less_than', value: min_categories)
      end

      if categories.size > max_categories
        errors.add :categories, I18n.t('cdi.errors.categories.greater_than', value: max_categories)
      end
    end

    def min_categories
      1
    end

    def max_categories
      1
    end
end
