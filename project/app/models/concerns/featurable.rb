module Featurable
  extend ActiveSupport::Concern

  included do
    has_many :featured, class_name: 'ResourceFeatured', as: :resource

    scope :only_featured, -> {
      joins("LEFT JOIN resource_featured ON resource_featured.resource_id = #{table_name}.id")
      .select("#{table_name}.*, count(resource_featured.id)")
      .where("resource_featured.resource_type = ?", name)
      .group("#{table_name}.id")
      .having("count(resource_featured.id) > ?", 0)
    }
  end

  def total_featured
    featured.count
  end
end
