# TODO: Trackable?
module Historiable
  extend ActiveSupport::Concern

  included do
    has_many :edition_histories, as: :historable, class_name: 'ResourceHistory'

    after_save :create_update_history
    before_destroy :create_delete_history, if: -> { create_history_for_destroy? }
  end

  attr_accessor :history_user

  private
  def history_attributes
    history_attributes = {}

    if self.changed?
      attrs = (attributes_for_history || default_history_attributes).map(&:to_s)

      history_attributes = self.attributes.slice(*attrs)
      history_attributes.merge!(custom_data: custom_data_for_histories)

      if self.respond_to?(:status) && status_changes = self.try(:status_change)
        history_attributes.merge!(last_status: status_changes.shift, new_status: status_changes.shift)
      end

      history_attributes.merge!(user_data: history_user_data)
      history_attributes[:action_type] = self.new_record? || created_at == updated_at ? :create : :update
    end

    # keep only valid columns
    history_attributes.slice(*ResourceHistory.column_names.map(&:to_sym))
  end

  def history_user_data
    user_data = {}
    if self.respond_to?(:history_user) && history_user = self.try(:history_user)
      if history_user && history_user.is_a?(User)
        data = history_user.as_json(only: [:email, :username, :profile_type], methods: [:fullname])
        user_data = user_data.merge!(user_id: history_user.id, user_data: data)
      end
    end

    user_data
  end

  def default_history_attributes
    []
  end

  def custom_data_for_histories
    {}
  end

  def create_history_for_destroy?
    false
  end

  def create_history_for_new_records?
    false
  end

  def create_edition_history(history_attributes)
    if history_attributes[:action_type] == :create && !create_history_for_new_records?
      return false
    end

    if history_attributes.present? && (history_attributes[:new_status]  ||
                                       history_attributes[:last_status] ||
                                       history_attributes[:custom_data].present? ||
                                       history_attributes[:user_data].present?)

      self.edition_histories.create(history_attributes)
    end
  end

  def create_update_history
    create_edition_history(history_attributes)
  end

  def create_delete_history
    history_attributes = {
      custom_data: self.attributes,
      action_type: :delete,
      user_data: history_user_data
    }

    create_edition_history(history_attributes)
  end


  alias :attributes_for_history :default_history_attributes
end
