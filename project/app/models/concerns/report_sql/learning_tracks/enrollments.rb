module ReportSQL
  module LearningTracks
    module Enrollments

      extend ActiveSupport::Concern

      # TODO: Review this query (is the same from MATERIALIZED VIEW)
      ENROLLMENT_REPORT_QUERY = <<-QUERY
        SELECT COUNT(*) as total,
               status,
               learning_track_id,
               student_class_id,
               -- Percentage for class by status
                round(
                -- Amount
                ((COUNT(*) * 100)::numeric)/
                -- Total by class/track
                (SELECT COUNT(*) FROM
                  enrolled_learning_tracks
                  WHERE student_class_id = elt.student_class_id AND
                        learning_track_id = elt.learning_track_id
                ),
                -- round
                2) as total_percent
        FROM enrolled_learning_tracks elt
        WHERE elt.learning_track_id = %s AND elt.student_class_id = %s
        GROUP BY elt.learning_track_id, elt.student_class_id, elt.status
        ORDER BY learning_track_id, student_class_id, status;
      QUERY

      ENROLLMENT_STATUS = [:enrolled, :coursing, :completed, :not_enrolled]

      included do

        def run_refresh_enrollment_reports_query
          ActiveRecord::Base.transaction do
            query = ActiveRecord::Base.send(:sanitize_sql, [
              ENROLLMENT_REPORT_QUERY, self.learning_track_id, self.student_class_id
            ], '')

            ActiveRecord::Base.connection.execute(query).to_a
          end
        end

        def all_enrollments_by_status
          create_not_enrolled_records_for_reports

          run_refresh_enrollment_reports_query
        end

        def create_not_enrolled_records_for_reports
          student_class.create_not_enrolled_records_for_reports(learning_track)
        end

        def not_enrolled_students
          student_class.not_enrolled_students(learning_track)
        end

        def all_enrollments_by_status_old
          result_data = run_refresh_enrollment_reports_query
          result_data.push(not_enrolled_students_report_data)

          result_data
        end


        def not_enrolled_students_report_data
          students = not_enrolled_students
          students_count = student_class.students_count
          not_enrolled_count = students.size
          total_percent = students_count.zero? ? 0 : ((not_enrolled_count * 100) / students_count)

          {
            status: 'not_enrolled',
            total: not_enrolled_count,
            learning_track_id: learning_track_id,
            student_class_id: student_class_id,
            total_percent: total_percent
          }.stringify_keys
        end

        def refresh_enrollment_reports_column
          result_data = all_enrollments_by_status

          ENROLLMENT_STATUS.each do |status|
            unless result_data.any? {|d| d['status'].to_s == status.to_s }

              status_data = {
                total: 0,
                status: status,
                learning_track_id: learning_track_id,
                student_class_id: student_class_id,
                total_percent: '0'
              }

              result_data.push(status_data.stringify_keys)
            end
          end

          report_data = Hash[result_data.map {|d| [d['status'], d['total_percent']] }] || {}
          report_data.merge!(updated_at: Time.zone.now)

          column_data = {
            data: result_data.to_json,
            report_data: report_data.to_json
          }

          self.migrate_to_generated
          self.update_attribute(:enrollment_reports, column_data)
        end
      end
    end
  end
end
