module ReportSQL
  module StudentClasses

    extend ActiveSupport::Concern

    AGE_RANGE_SQL = <<-QUERY
      SELECT
        u.gender,
        count(case when EXTRACT(YEAR FROM age(now(), u.birthday)) < 15 then 1 end) as range_1,
        count(case when EXTRACT(YEAR FROM age(now(), u.birthday)) BETWEEN 15 AND 17 then 1 end ) as range_2,
        count(case when EXTRACT(YEAR FROM age(now(), u.birthday)) BETWEEN 18 AND 24 then 1 end ) as range_3,
        count(case when EXTRACT(YEAR FROM age(now(), u.birthday)) BETWEEN 25 AND 34 then 1 end ) as range_4,
        count(case when EXTRACT(YEAR FROM age(now(), u.birthday)) BETWEEN 35 AND 44 then 1 end ) as range_5,
        count(case when EXTRACT(YEAR FROM age(now(), u.birthday)) BETWEEN 45 AND 54 then 1 end ) as range_6,
        count(case when EXTRACT(YEAR FROM age(now(), u.birthday)) > 54 then 1 end ) as range_7,
        COUNT(*) as total
      FROM student_classes_users scu
        INNER JOIN users u ON u.id = scu.user_id
      WHERE scu.student_class_id = %s
      GROUP BY u.gender;
    QUERY

    AGE_RANGES = {
      range_1: '< 15',
      range_2: '15-17',
      range_3: '18-24',
      range_4: '25-34',
      range_5: '35-44',
      range_6: '45-54',
      range_7: '> 54',
      total:   'Total',
      gender:  'gender'
    }

    TRACK_ENROLLMENT_QUERY = <<-QUERY
      SELECT COUNT(*) as total,
        to_char(date_trunc('day', e.created_at), 'DD/MM/YYYY') as date
      FROM enrolled_learning_tracks e
      WHERE e.status != '#{::EnrolledLearningTrack::STATUS[:not_enrolled]}' AND e.student_class_id = %s AND created_at > (now() - interval '%s')
      GROUP BY date_trunc('day', e.created_at)
      ORDER BY date_trunc('day', e.created_at) ASC;
    QUERY

    included do

      def students_tracks_enrollments_in_interval(interval = '30 days')
        query = ActiveRecord::Base.send(:sanitize_sql, [TRACK_ENROLLMENT_QUERY, self.id, interval], '')
        ActiveRecord::Base.connection.execute(query).to_a
      end

      def students_age_range
        query = ActiveRecord::Base.send(:sanitize_sql, [AGE_RANGE_SQL, self.id], '')
        ActiveRecord::Base.connection.execute(query).to_a
      end

      def formatted_students_age_range
        _method = method(:format_age_range_row)
        students_age_range.map(&_method)
      end

    end

  end
end
