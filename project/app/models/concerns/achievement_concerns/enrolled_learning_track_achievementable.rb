module AchievementConcerns
  module EnrolledLearningTrackAchievementable
    extend ActiveSupport::Concern
    include AchievementConcerns::SimpleAchievementable

    included do
      after_update :grant_on_complete_learning_track
    end

    private
    def create_enrollment
      grant_to self.track.user, :student_takes_learning_track
      grant_on_complete_learning_track
    end

    def grant_on_complete_learning_track
      grant_to self.student, :complete_learning_track
    end

  end
end



