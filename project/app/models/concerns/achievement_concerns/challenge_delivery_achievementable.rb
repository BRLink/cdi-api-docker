module AchievementConcerns
  module ChallengeDeliveryAchievementable
    extend ActiveSupport::Concern
    include AchievementConcerns::SimpleAchievementable

    private
    def create_challenge_delivery
      self.users.each do |user|
        grant_to user, :deliver_challenge
      end
    end
  end
end

