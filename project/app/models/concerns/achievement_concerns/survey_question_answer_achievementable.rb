module AchievementConcerns
  module SurveyQuestionAnswerAchievementable
    extend ActiveSupport::Concern
    include AchievementConcerns::SimpleAchievementable

    private
    def create_survey_answer
      grant_on_create :answer_survey_question
    end

  end
end




