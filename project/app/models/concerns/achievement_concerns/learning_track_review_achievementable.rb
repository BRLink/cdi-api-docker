module AchievementConcerns
  module LearningTrackReviewAchievementable
    extend ActiveSupport::Concern
    include AchievementConcerns::SimpleAchievementable

    private
    def review_learning_track
      grant_on_create :review_learning_track_score_and_140_characters if comment && comment.length >= 140
    end

  end
end


