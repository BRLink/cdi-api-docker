module ProfileCoverImageable

  extend ActiveSupport::Concern

  included do
    mount_uploader :profile_cover_image, ProfileCoverImageUploader
    mount_base64_uploader :profile_cover_image, ProfileCoverImageUploader, base_filename: :cover_profile_picture

    # create dinamic methods to handle cropping
    crop_uploaded :profile_cover_image

    process_in_background :profile_cover_image if CDI::Config.enabled?(:process_profile_cover_upload_in_background) ||
                                                  CDI::Config.enabled?(:process_upload_in_background)
  end

  def profile_cover_images
    image_versions = self.profile_cover_image.versions

    versions = image_versions.map(&:first)
    images   = image_versions.map(&:last).map(&:url)

    Hash[versions.zip(images)].presence || [self.profile_cover_image]
  end
end
