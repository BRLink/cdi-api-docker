module PostAttachable
  extend ActiveSupport::Concern
  def user_can_attach? u
    user.id == u.id
  end
end
