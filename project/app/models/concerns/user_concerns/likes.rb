module UserConcerns
  module Likes
    extend ActiveSupport::Concern

    included do
      has_many :likes
    end

    def liked? resource
      likes.where(resource: resource).exists?
    end

    def get_like_of resource
      likes.where(resource: resource).first
    end

  end
end
