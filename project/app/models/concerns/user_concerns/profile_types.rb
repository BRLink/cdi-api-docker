module UserConcerns
  module ProfileTypes
    extend ActiveSupport::Concern

    VALID_MULTIPLIER_PROFILES_TYPES = {
      :mentor          => "mentor",
      :librarian       => "librarian",
      :social_educator => "social_educator",
      :teacher         => "teacher"
    }.freeze

    VALID_PROFILES_TYPES = {
      :superuser => "superuser",
      :staff   => "staff",
      :student => "student",
    }.merge(VALID_MULTIPLIER_PROFILES_TYPES).freeze

    VALID_GENDERS = {
      :male   => "male",
      :female => "female"
    }.freeze

    included do

      # users can be banned
      enum status: { banned: -10, normal: 0 }


      VALID_PROFILES_TYPES.each do |key, value|
        scope key, -> { where(profile_type: value) }

        define_method("#{key}?") do
          self.profile_type.to_s == value.to_s
        end

        alias :"is_#{key}?" :"#{key}?"
      end

      scope :multiplier, -> { where(profile_type: VALID_MULTIPLIER_PROFILES_TYPES.values) }

      has_one :address, as: :addressable

      validates :profile_type, presence: true, inclusion: VALID_PROFILES_TYPES.values

      validates :gender, presence: true, inclusion: VALID_GENDERS.values

      validates :username, uniqueness: true, on: :create

      validates :first_name, :last_name, :birthday, presence: true
    end

    def backoffice_profile?
      return self.superuser? || self.staff? || self.admin? || self.multiplier?
    end

    def multiplier?
      return false if self.admin?
      VALID_MULTIPLIER_PROFILES_TYPES.values.member?(self.profile_type)
    end

    def admin?
      [:admin, :staff, :superuser].member?(self.profile_type.to_sym)
    end

    def banned?
      status == 'banned'
    end

    def normal?
      status == 'normal'
    end

    def formatted_phone_number
      "(#{phone_area_code.try(:squish)}) #{phone_number.try(:squish)}"
    end
  end
end
