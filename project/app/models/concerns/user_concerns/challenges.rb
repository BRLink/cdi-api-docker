module UserConcerns
  module Challenges
    extend ActiveSupport::Concern

    included do
      has_many :challenge_subscriptions
      has_many :challenges, through: :challenge_subscriptions
      has_and_belongs_to_many :challenge_deliveries
    end
  end
end
