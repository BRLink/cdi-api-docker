module UserConcerns
  module Notifications

    extend ActiveSupport::Concern

    included do
      has_many :notifications, foreign_key: :receiver_user_id
      has_many :notification_configs
    end

    def notify(notification_data, method = :create)
      raise 'Invalid method' unless [:create, :new, :build].member?(method.to_sym)

      self.notifications.send(method, notification_data)
    end

    def notifications_status
      @notifications_status ||= {
        read: self.notifications.read.count,
        unread: self.notifications.unread.count
      }
    end

    def notification_config(type)
      notification_configs.find_or_create_by notification_type: type
    end

  end
end
