module UserConcerns
  module Discussions
    extend ActiveSupport::Concern

    included do
      has_many :discussions
    end
  end
end
