module UserConcerns
  module Games
    extend ActiveSupport::Concern

    GAMES_WITH_QUESTIONS = [
      :game_polls,
      :game_quizzes,
      :game_open_questions,
      :game_true_false
    ]

    included do

      has_many :game_question_answers

      has_many :game_complete_sentences
      has_many :game_complete_sentences_answers, through: :game_complete_sentences, source: :answers

      has_many :game_word_searches

      has_many :game_related_items
      has_many :game_related_items_items, through: :game_related_items, source: :items
      has_many :game_related_items_answers, through: :game_related_items, source: :answers

      has_many :game_group_items

      has_many :game_group_items_items, through: :game_group_items, source: :items
      has_many :game_group_items_answers, through: :game_group_items, source: :answers

      has_many :games_scores,  class_name: 'GameUserScore'

      GAMES_WITH_QUESTIONS.each do |game_association|
        # created games
        has_many game_association

        source = game_association == :game_polls ? :question : :questions

        # questions of game
        has_many :"#{game_association}_questions", through: game_association, source: source

        # answers(student answers) of game
        has_many :"#{game_association}_answers", through: game_association, source: :answers
      end

    end

    def last_response_for_game(game)
      return nil if game.blank?

      all_responses_for_game(game).last
    end

    def all_responses_for_game(game)
      game.answers.where(user_id: self.id)
    end

  end
end
