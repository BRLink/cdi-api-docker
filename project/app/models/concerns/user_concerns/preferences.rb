module UserConcerns
  module Preferences
    extend ActiveSupport::Concern

    included do
      has_many :user_preferences
      alias :preferences :user_preferences
    end
  end
end
