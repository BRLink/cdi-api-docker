module UserConcerns
  module Auth
    extend ActiveSupport::Concern

    EMAIL_REGEXP = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

    included do
      has_secure_password

      has_many :authorizations

      has_one :manager

      # validate password for new user or revalidate password if changed
      validates :password, :password_confirmation, :length => {
        :minimum => 5
      }, if: :needs_password_validation?

      validates :password, confirmation: true, if: :needs_password_validation?

      validates :email, presence: true
      validates :email, uniqueness: true, on: [:create, :update]

      validates :email, format: { with: EMAIL_REGEXP }, if: -> {
        self.email.present?
      }

      validates :oauth_provider_uid, uniqueness: {
        scope: [:oauth_provider]
      }, on: [:create], if: :from_oauth_provider?

      validates :tof_accepted_at, presence: true, on: [:create]


      before_save :set_password_updated_at_before_save, if: -> { self.password_digest_changed? }

      before_create :set_tof_defaults

      after_initialize :set_tof_defaults

      def self.authenticate(email, password)
        return false unless email && password

        user = User.normal.find_by(email: email)
        user.try(:authenticate, password)
      end

    end

    def reset_password!
      self.reset_password_token = [self.id, SecureRandom.hex].join
      self.reset_password_sent_at = Time.zone.now
      self.save!
    end

    def update_password(new_password, save = true, delete_authorizations = true)
      self.password = self.password_confirmation = new_password
      self.password_reseted_at = self.password_updated_at = Time.zone.now
      self.reset_password_token = nil
      self.authorizations.delete_all if save && delete_authorizations
      self.save if save
    end

    def from_oauth_provider?
      [oauth_provider_uid, oauth_provider].all?
    end

    def reset_oauth_provider!
      self.update_attributes(oauth_provider_uid: nil, oauth_provider: nil)
    end

    def tof_accepted?
      self.tof_accepted_at.present?
    end
    alias :tof_accepted :tof_accepted?

    def tof_accepted=(value)
      self.tof_accepted_at ||= Time.zone.now if value_to_boolean(value)
    end

    private
    def value_to_boolean(value)
      ['true', true].include?(value)
    end

    def set_tof_defaults
      self.tof_accepted_at ||= Time.zone.now if self.new_record?
    end

    def needs_password_validation?
      return true if self.new_record? || self.password_digest_changed?

      return self.password_digest_changed? && self.password.blank?
    end

    def set_password_updated_at_before_save
      self.password_updated_at = Time.zone.now
    end
  end
end
