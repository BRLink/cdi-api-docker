module UserConcerns
  module StudentLearningTracks
    extend ActiveSupport::Concern

    TRACK_GAMES = [
      :game_polls,
      :game_quizzes,
      :game_complete_sentences,
      :game_open_questions,
      :game_word_searches,
      :game_true_false,
      :game_related_items,
      :game_group_items
    ]

    included do
      # student related
      has_many :enrolled_learning_tracks

      # 'my tracks' for student
      has_many :enrolled_tracks, through: :enrolled_learning_tracks, source: :learning_track
      has_many :enrolled_cycles, through: :enrolled_tracks, source: :learning_cycle

      # tracking events of watched/played slides of track
      has_many :tracking_events, through: :enrolled_learning_tracks

      # tracks shared in student_classes who user is student
      has_many :classes_tracks, through: :student_classes, source: :learning_tracks
      has_many :classes_cycles, through: :classes_tracks, source: :learning_cycle

      has_many :tracks_game_slides, through: :enrolled_tracks, source: :game_slides

      TRACK_GAMES.each do |game_association|
        has_many :"tracks_#{game_association}", through: :enrolled_tracks, source: game_association
      end

      has_many :student_deliveries

      has_and_belongs_to_many :group_students_deliveries, class_name: 'StudentDelivery', join_table: 'student_delivery_groups'

      # Multiplier review
      has_many :sent_deliveries_reviews, class_name: 'StudentDeliveryReview', foreign_key: :multiplier_user_id

      # Student review
      has_many :sent_tracks_reviews, class_name: 'LearningTrackReview' #, foreign_key: :user_id

    end

    def student_tracks_recommendations(limit = 4)
      recommendations = classes_tracks
                        .published_in_classes
                        .where.not(id: enrolled_tracks)
                        .order('created_at DESC')
                        .limit(limit)
                        .all

      return recommendations if recommendations.size == limit

      recommendations + enrolled_tracks
                        .published_in_classes
                        .order('created_at DESC')
                        .limit(limit - recommendations.size)
                        .where.not(id: recommendations.map(&:id))
    end
  end
end
