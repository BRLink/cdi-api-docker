module UserConcerns
  module Albums

    extend ActiveSupport::Concern

    included do
      # after_save  :create_default_album
      has_many :albums
      has_many :photos, through: :albums
    end

    def find_album_or_use_default(album_data = {})
      return default_album if album_data.values.compact.blank?

      self.albums.where(album_data).first || default_album
    end

    def default_gallery_album
      self.albums.gallery.first_or_create(title: Album.default_gallery_album_title)
    end

    def default_album
      self.albums.default.first_or_create(title: Album.default_album_title)
    end

    def default_album=(album)
      default_album.update_attributes(album_type: Album::TYPES[:standard])
      album.update_attributes(album_type: Album::TYPES[:default])
    end
  end
end
