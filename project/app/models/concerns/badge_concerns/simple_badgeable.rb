module BadgeConcerns
  module SimpleBadgeable
    extend ActiveSupport::Concern

    ACTION_MAP = {
      #"ChallengeDelivery" => :award_on_challenge_delivery, # after_commit, delivery <> users relation is only created after
      "Comment" => :award_on_comment_created,
      "Discussion" => :award_on_discussion_created,
      "Post" => :award_on_post_created
    }

    included do
      after_create do
        action = ACTION_MAP[self.class.to_s]
        send action if action
      end
    end

    private
    def award_on_create badge_action, quantity
      award_to self.user, badge_action, quantity
    end

    def award_on_receive badge_action, quantity
      award_to self.resource.user, badge_action, quantity
    end

    def award_to user, badge_action, quantity = nil
      if quantity.nil? || Badge::GRANTABLE_QUANTITIES.include?(quantity)
        badge_action = (badge_action.to_s % quantity)
        badge = Badge.find_by(action: badge_action)

        return unless badge

        user.user_badges.create(
          badge: badge
        )
      end
    end

  end
end

