module BadgeConcerns
  module ChallengeDeliveryBadgeable
    extend ActiveSupport::Concern
    include BadgeConcerns::SimpleBadgeable

    included do
      after_commit do
        award_on_challenge_delivery
      end
    end

    private
    def award_on_challenge_delivery
      self.users.each do |user|
        quantity = user.challenge_deliveries.count
        award_to user, "delivered_%d_challenges", quantity
      end
    end
  end
end

