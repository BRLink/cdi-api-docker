module BadgeConcerns
  module LearningTrackBadgeable
    extend ActiveSupport::Concern
    include BadgeConcerns::SimpleBadgeable

    included do
      after_update :award_on_track_created
    end

    private
    def award_on_track_created
      if self.changed.include?("status") && self.status_published?
        if self.parent.nil?
          track_quantity = self.user.learning_tracks.published.count
          award_on_create "created_%d_tracks", track_quantity

          self.moderators.each do |co_author|
            co_authored_quantities = co_author.moderate_learning_tracks.count
            award_to co_author, "co_created_%d_tracks", co_authored_quantities
          end
        else #  cloned
          cloned_quantity = self.user.learning_tracks.published.cloned.count
          award_on_create "cloned_%d_tracks", cloned_quantity

          other_user = self.parent.user
          cloned_quantity = other_user.learning_tracks.published.with_cloned_tracks.count
          award_to other_user, "had_%d_tracks_cloned", cloned_quantity
        end
      end
    end

  end
end

