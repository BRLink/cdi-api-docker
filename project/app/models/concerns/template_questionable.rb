module TemplateQuestionable

  extend ActiveSupport::Concern

  included do

    # # amoeba do
    # #   clone [:questions]
    # end

    has_many :questions, as: :template_questionable

    has_many :answers, through: :questions

    has_many :participating_users, through: :questions

    define_method :question_templates do
      QuestionTemplate.questions_for(self.class.name)
    end

    after_save :generate_default_questions, if: -> {
      CDI::Config.disabled?(:create_template_questionable_records_questions_async)
    }
  end

  def generate_default_questions
    return self if self.persisted? && self.questions.any?

    self.question_templates.each do |question_template|
      self.questions.create(question_template_id: question_template.id)
    end
  end

end
