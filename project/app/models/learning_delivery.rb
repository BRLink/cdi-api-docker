class LearningDelivery < ActiveRecord::Base

  include Accessable

  belongs_to :learning_dynamic

  has_one :user, through: :learning_dynamic
  has_one :learning_track, through: :learning_dynamic

  has_one :origin, as: :originable

  has_many :student_deliveries

  VALID_FORMAT_TYPES = {
    text: [:doc, :txt],
    image: [:jpeg, :jpg, :png],
    video: [:avi, :mp3],
    presentation: [:ppt, :pptx, :pdf]
  }

  DELIVERY_KINDS = {
    1 => :individual,
    2 => :group
  }

  MAX_GROUP_SIZE = 6

  validates :format_type,
            inclusion: VALID_FORMAT_TYPES.values.flatten.map(&:to_s),
            presence: true

  validates :delivery_kind,
            inclusion: DELIVERY_KINDS.keys,
            presence: true

  validates :delivery_date, :learning_dynamic, :group_size, presence: true


  validate :validates_delivery_date_greather_than_now

  validates :group_size, numericality: {
    only_integer: true,
    greater_than: 0,
    less_than_or_equal_to: MAX_GROUP_SIZE
  }

  validates :group_size, numericality: {
    greater_than: 1
  }, if: :group_delivery?

  delegate :user_id, to: :learning_dynamic

  before_validation :set_defaults

  def group_delivery?
    self.delivery_kind == 2
  end

  private

  def set_defaults
    self.group_size = 1 if self.delivery_kind == 1
  end

  def validates_delivery_date_greather_than_now
    if self.delivery_date && self.delivery_date < Time.zone.now
      self.errors.add(:delivery_date, 'Must be greather than now')
    end
  end

end
