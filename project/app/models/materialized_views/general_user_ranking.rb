module MaterializedViews
  class GeneralUserRanking < ActiveRecord::Base

    belongs_to :user
    has_many :institution_users, through: :user
    has_many :student_classes, through: :user

    self.table_name = "general_user_ranking_matview"

    default_scope { order(:position) }

    # FIXME use arel to get the max position and user position, so we can use only 1 query
    scope :around_user, -> (user) {
      # if user is 1st, second, last or second-to-last
      position = user.general_ranking.position

      pivot = [3, position].max
      pivot = [pivot, MaterializedViews::GeneralUserRanking.maximum(:position) - 2].min
      range = (pivot - 2)..(pivot + 2)

      ranking = self.arel_table

      where(ranking[:position].in(range))
        .order(:position)
    }

    scope :by_institution, -> (institution) {
      institution_table = InstitutionUser.arel_table

      joins(:user)
        .joins(:institution_users)
        .where(institution_table[:educational_institution_id].eq(institution.id))
        .order(:position)
    }

    scope :by_student_class, -> (klass) {
      class_table = StudentClass.arel_table

      joins(:user)
        .joins(:student_classes)
        .where(class_table[:id].eq(klass.id))
        .order(:position)
    }

    scope :override_position, -> {
        select(<<-SELECT
        ROW_NUMBER() OVER(ORDER BY position) as position,
        general_user_ranking_matview.user_id,
        default_score
        SELECT
        ).order(:position)
    }
    def self.refresh
      connection.execute("REFRESH MATERIALIZED VIEW CONCURRENTLY general_user_ranking_matview;")
    end

    def readonly?
      true
    end

  end
end

