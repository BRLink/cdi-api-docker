module MaterializedViews
  class BaseMaterializedView < ActiveRecord::Base
    def self.refresh
      connection.execute("REFRESH MATERIALIZED VIEW #{table_name}")
    end

    # Cant change MATERIALIZED VIEW
    def readonly
      true
    end

  end
end
