# encoding: UTF-8
class QuestionUserAnswer < ActiveRecord::Base

  belongs_to :question
  belongs_to :user

  has_one :origin, as: :originable

  validates :question_id, :user_id, :answer_text, presence: true

  validate :answer_index_presence

  private
  def answer_index_presence
    if self.question.with_choice?
      if self.answer_index.blank?
        errors.add(:answer_index, 'Must be provided when question have choices')
      elsif !(0..self.question.choices_count).member?(self.answer_index)
        errors.add(:ansert_index, 'Index must be in valid range')
      end
    end
  end
end
