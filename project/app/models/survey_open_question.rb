class SurveyOpenQuestion < ActiveRecord::Base

  include Accessable
  include Attachable
  include ResourceVisibility

  belongs_to :user

  has_many :questions, as: :questionable, class_name: 'SurveyQuestion', dependent: :destroy

  has_many :subscriber_surveys, as: :survey, dependent: :destroy

  has_many :answers, through: :questions

  has_many :photos, through: :attachments, source: :attachment, source_type: 'Photo'

  validates :user_id, :title, presence: true

  validates :video_link, presence: true, format: { with: URI.regexp }, if: -> {
    self.video_link.present?
  }

  def image_url
    photos.with_deleted.first.image.url if photos.with_deleted.exists?
  end
end
