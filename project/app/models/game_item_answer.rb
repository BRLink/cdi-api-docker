class GameItemAnswer < ActiveRecord::Base

  include Accessable

  has_one :origin, as: :originable

  belongs_to :game_item
  belongs_to :user

  has_one :game_related_item, through: :game_item

  scope :correct, -> { where(correct: true) }
  scope :wrong, -> { where.not(correct: true) }

  validates :game_item, :user, :reply_word, presence: true
  # validates :game_item, uniqueness: { scope: [:user_id] }

  before_save :set_correct_flag

  private
  def set_correct_flag
    self.correct = correct_word?(self.reply_word)

    true
  end

  def correct_word?(word)
    return false if word.blank? || self.game_item.blank?

    self.game_item.related_word.to_s.downcase == word.to_s.downcase
  end

end
