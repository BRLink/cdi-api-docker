class SurveyPoll < ActiveRecord::Base

  include Accessable
  include Attachable
  include ResourceVisibility

  belongs_to :user

  has_one :question, as: :questionable, class_name: 'SurveyQuestion', dependent: :destroy

  delegate :title, to: :question

  has_many :options, through: :question

  has_many :answers, through: :question

  has_many :photos, through: :attachments, source: :attachment, source_type: 'Photo'

  has_many :subscriber_surveys, as: :survey, dependent: :destroy

  validates :user_id, presence: true

  MAX_OPTIONS_BY_QUESTION = (CDI::Config.game_poll_question_options_max_limit || 10).to_i

  def image_url
    photos.with_deleted.first.image.url if photos.with_deleted.exists?
  end
end
