class NotificationConfig < ActiveRecord::Base
  belongs_to :user

  EXCLUDED_NOTIFICATIONS_TYPES = %w(welcome password_updated)

  TYPES = Notification::TYPES - EXCLUDED_NOTIFICATIONS_TYPES

  validates :notification_type, inclusion: TYPES
  validates :user, presence: true
  validates :notification_type, uniqueness: { scope: [:user_id] }

  alias_attribute :email, :can_email
  alias_attribute :can_email?, :can_email
  alias_attribute :notification, :can_notification
  alias_attribute :can_notification?, :can_notification

  def user_can_update?(user)
    self.user_id == user.id
  end

end
