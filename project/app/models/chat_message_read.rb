# encoding: UTF-8

class ChatMessageRead < ActiveRecord::Base

  belongs_to :user
  belongs_to :chat_message

  validates :user, :chat_message, presence: true
  validates :user_id, uniqueness: { scope: [:chat_message_id] }, on: :create

end
