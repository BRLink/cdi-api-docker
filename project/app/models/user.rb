class User < ActiveRecord::Base

  acts_as_paranoid

  has_one :origin, as: :originable

  # Generic Concerns
  include Historiable
  include ProfileImageable
  include Usernamed

  # Specific for users Concerns: validations, methods, separated by group
  include AchievementConcerns::UserAchievementable
  include BadgeConcerns::UserBadgeable
  include PubsubConcerns::Publisher
  include PubsubConcerns::Subscriber
  include PubsubConcerns::SubscribersGroups
  include ResourceVisibilitable
  include UserConcerns::Achievements
  include UserConcerns::Albums
  include UserConcerns::Auth
  include UserConcerns::Badges
  include UserConcerns::Challenges
  include UserConcerns::Chat
  include UserConcerns::Comments
  include UserConcerns::Discussions
  include UserConcerns::Games
  include UserConcerns::Likes
  include UserConcerns::Multimedias
  include UserConcerns::MultiplierLearningTracks
  include UserConcerns::Notifications
  include UserConcerns::Posts
  include UserConcerns::Preferences
  include UserConcerns::ProfileTypes
  include UserConcerns::ResourcesFeatured
  include UserConcerns::Search
  include UserConcerns::StudentClasses
  include UserConcerns::StudentLearningTracks
  include UserConcerns::Surveys

  def user_can_update?(user)
    self.admin? || user.id == self.id
  end

  alias :name :fullname

  private
    def custom_data_for_histories
      self.changes.symbolize_keys.slice(:first_name, :last_name, :email, :username, :profile_type, :gender, :birthday)
    end

    def profile_complete?
      [username, about, profile_image, phone_number, phone_area_code].all?
    end
end
