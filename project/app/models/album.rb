# encoding: UTF-8
class Album < ActiveRecord::Base

  TYPES = {
    gallery: 'gallery',
    standard: 'standard',
    default: 'default'
  }

  include Accessable
  include Activable
  include Positionable
  include Taggable

  belongs_to :user

  has_many :photos

  validates :user_id, :title, presence: true
  validates :album_type, inclusion: TYPES.values

  before_save :set_description_length

  scope :default, ->{ where(album_type: TYPES[:default]) }

  scope :gallery, ->{ where(album_type: TYPES[:gallery]) }

  scope :visible, -> { where(album_type: [TYPES[:default], TYPES[:standard]]) }

  MAX_DESCRIPTION_LENGTH = 255

  def self.default_album_title
    I18n.t('app.albums.default_album_title')
  end

  def self.default_gallery_album_title
    I18n.t('app.albums.default_gallery_album_title')
  end

  def cover_image_url
    self.photos.first.try(:image_url)
  end

  def cover_images_url
    self.photos.first.try(:images_url)
  end

  private
  def set_description_length
    self.description = self.description[0, MAX_DESCRIPTION_LENGTH] if self.description?
  end
end
