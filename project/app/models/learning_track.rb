# encoding: UTF-8

class LearningTrack < ActiveRecord::Base

  include Accessable
  include Statusable
  include Taggable
  include TemplateQuestionable
  include Likeable
  include Commentable
  include Categorizable
  include Searchable
  include AchievementConcerns::LearningTrackAchievementable
  include BadgeConcerns::LearningTrackBadgeable

  CLONABLE_ASSOCIATED_RESOURCE_TYPES = [
    'GameQuiz'.freeze,
    'GamePoll'.freeze,
    'GameGroupItem'.freeze,
    'GameRelatedItem'.freeze,
    'GameCompleteSentence'.freeze,
    'GameOpenQuestion'.freeze,
    'GameTrueFalse'.freeze,
    'GameWordSearch'.freeze,
    'LearningDynamic'.freeze
  ]

  searchable_for :name, :tags, :categories_names, if: ->(track) { track.status_published? }

  scope :published_in_classes, -> {
    published.joins(:student_classes)
  }

  scope :with_slides, -> {
    joins(:presentation_slides)
  }

  scope :with_presentation_slides, -> {
    joins(:presentation).where('presentations.slides_count > 1')
  }

  scope :with_visible_slides, -> {
    joins(:visible_slides)
  }

  scope :cloned, -> {
    joins(:parent)
  }

  scope :with_cloned_tracks, -> {
    joins(:cloned)
  }
  extend ActsAsTree::TreeView

  acts_as_tree order: :id

  amoeba do
    recognize [:has_and_belongs_to_many]

    include_association [:skills]

    nullify [:learning_cycle_id, :status, :status_changed_at, :parent_id]

    exclude_association [:edition_histories, :moderators, :student_classes]
  end

  belongs_to :learning_cycle

  # Alias
  alias :cycle :learning_cycle
  alias :cycle= :learning_cycle=

  has_many :cloned, class_name: "LearningTrack", foreign_key: :parent_id

  has_many :category_resources, as: :resource

  has_many :categories, through: :category_resources, source: :category

  has_one :user, through: :learning_cycle

  validates :name, :learning_cycle_id, presence: true
  validate :can_add_category

  has_and_belongs_to_many :skills

  has_and_belongs_to_many :moderators,
                           join_table: :learning_track_moderators,
                           class_name: 'User'

  alias :authors :moderators

  has_one :origin, as: :originable

  belongs_to :presentation

  has_many :presentation_slides, through: :presentation
  has_many :visible_slides, through: :presentation

  alias :slides :presentation_slides

  has_many :game_slides, -> {
    where("presentation_slides.slide_type = ?", PresentationSlide::SLIDE_TYPES[:game])
  }, through: :presentation, source: :presentation_slides

  ::UserConcerns::StudentLearningTracks::TRACK_GAMES.each do |game_association|
    has_many game_association, through: :game_slides,
                               source:  :associated_resource,
                               source_type: game_association.to_s.classify
  end

  # student related
  has_many :enrolled_learning_tracks
  has_many :enrolled_users, through: :enrolled_learning_tracks, source: :user

  has_many :student_deliveries

  # student review
  has_many :reviews, class_name: 'LearningTrackReview'

  # review related to multiplier
  has_many :student_delivery_reviews,
            through: :student_deliveries,
            source: :review,
            class_name: 'StudentDeliveryReview'

  has_many :student_delivery_reviewer_students,
           through: :student_delivery_reviews,
           source: :student_user


  has_many :matview_enroll_reports, class_name: 'MaterializedViews::TrackEnrollReport'
  has_many :matview_review_reports, class_name: 'MaterializedViews::TrackReviewReport'

  has_many :reports, class_name: 'LearningTrackReport'

  has_and_belongs_to_many :student_classes, join_table: :student_classes_tracks
  has_many :students,
           through: :student_classes,
           source: 'users'

  before_save :set_skills

  after_create :create_presentation_for_track

  delegate :user_id, to: :learning_cycle

  MAX_SKILLS_COUNT = 3

  def clone!(cycle)
    owner = cycle.user
    duped_object = self.amoeba_dup
    duped_object.learning_cycle = cycle

    duped_object.presentation = self.presentation.duplicate owner

    # temp clean children ans questions to avoid validation errors
    duped_object.children = []
    duped_object.questions = []
    duped_object.save

    # save parent and questions duplicated
    duped_object.parent_id = self.id
    duped_object.category_ids = [cycle.category_ids.first]
    duped_object.save

    Presentation.reset_counters(duped_object.presentation_id, :presentation_slides)
    duped_object.save

    # clone the associated resources of each slide
    duped_object.presentation.presentation_slides.each do |slide|
      next if slide.associated_resource.blank?
      # only clone withlisted resources
      next if CLONABLE_ASSOCIATED_RESOURCE_TYPES.exclude?(slide.associated_resource_type)

      if associated_resource = slide.associated_resource

        if associated_resource.respond_to?(:clone!)
          slide.associated_resource = associated_resource.clone!(owner.id)
        else
          slide.associated_resource = slide.associated_resource.amoeba_dup
          slide.associated_resource.user = owner if slide.associated_resource.respond_to?('user=')
        end

        slide.save
      end
    end

    duped_object
  end

  alias :duplicate :clone!

  # TODO: Think about `cover_image` column in table
  def cover_image_url
    @_first_slide ||= self.slides.first
    @_first_slide.try(:cover_thumbnail_url)
  end

  def has_learning_dynamic?
    return false if presentation.blank?

    return learning_dynamic.present?
  end

  def learning_dynamic_slide
    return nil if self.presentation.blank?

    presentation.slides.where(slide_type: ::PresentationSlide::SLIDE_TYPES[:dynamic]).last
  end

  def learning_dynamic
    learning_dynamic_slide.try(:associated_resource)
  end

  def learning_delivery
    learning_dynamic.try(:learning_delivery)
  end

  def published_in_class?(student_class)
    self.student_classes.where(id: student_class.id).exists?
  end

  def user_can_clone?(user)
    user.backoffice_profile? && user.id != self.user_id
  end

  alias :user_can_duplicate? :user_can_clone?

  def user_can_read?(user)
    return true if user.backoffice_profile?
    # check if user is student from class or if user is coursing track
    super || user.student? && (user.classes_tracks.exists?(id: self.id) || user.enrolled_tracks.exists?(id: self.id))
  end

  def user_can_read_authors?(user)
    user && (user.admin? || self.user_id == user.id) || user.moderate_learning_tracks.exists?(id: self.id)
  end

  def user_can_update?(user)
    super || self.moderators.exists?(id: user.id)
  end

  def can_add_category
    categories.each do |category|
      unless learning_cycle.category_ids.include? category.id
        errors.add :categories, I18n.t('cdi.errors.categories.not_found', id: category.id)
      end
    end
  end

  private
  def set_skills
    self.skill_ids = self.skill_ids[0, MAX_SKILLS_COUNT] if self.skill_ids.any?
  end

  def create_presentation_for_track
    self.create_presentation if self.presentation_id.blank?
  end

  def max_categories
    4
  end

  ## FIXME add to counter_cache
  def presentation_slides_count
    presentation_slides.count
  end
end
