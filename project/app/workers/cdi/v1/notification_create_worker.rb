module CDI
  module V1
    class NotificationCreateWorker

      include Sidekiq::Worker

      sidekiq_options :retry => 5, queue: :system_notifications

      sidekiq_retry_in { |count| count * 60 }

      def perform(user_id, notification_data={})
        user = User.find_by(id: user_id)

        return false unless user

        service = CDI::V1::Notifications::CreateService.new(user, notification_data)
        service.execute
      end

    end
  end
end
