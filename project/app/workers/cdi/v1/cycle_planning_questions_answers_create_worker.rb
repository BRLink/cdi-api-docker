module CDI
  module V1
    class CyclePlanningQuestionsAnswersCreateWorker

      include Sidekiq::Worker

      sidekiq_options :retry => 5, queue: :cycle_planning

      sidekiq_retry_in { |count| count * 60 }

      def perform(cycle_planning_id, user_id, answers)

        cycle_planning = cycle_planning_id.is_a?(::CyclePlanning) ?
                         cycle_planning_id :
                         ::CyclePlanning.find_by(id: cycle_planning_id)

        user = user_id.is_a?(::User) ? user_id : ::User.find_by(id: user_id)

        if cycle_planning && user
          options = { answers: answers }

          service = CDI::V1::QuestionsAnswersCreateService.new(user, cycle_planning, options)
          service.execute
        end

      end
    end
  end
end
