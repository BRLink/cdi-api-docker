module CDI
  module V1
    class TrackEnrollProgressUpdateWorker
      include Sidekiq::Worker

      sidekiq_options :retry => 5, queue: :track_enroll

      sidekiq_retry_in { |count| count * 60 }

      def perform(enrolled_track_id)
        enroll = EnrolledLearningTrack.find_by(id: enrolled_track_id)

        enroll.try(:update_progress)
      end
    end
  end
end
