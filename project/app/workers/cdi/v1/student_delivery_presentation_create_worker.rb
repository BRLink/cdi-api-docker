module CDI
  module V1
    class StudentDeliveryPresentationCreateWorker
      include Sidekiq::Worker

      sidekiq_options :retry => false, queue: :converter

      def perform(student_delivery_id, retries)
        delivery = StudentDelivery.find_by(id: student_delivery_id)
        file_path=delivery.file.current_path
        url = "#{CDI::Config.converter_url}status"
        token = CDI::Config.converter_token
        uri = URI.parse(url)
        uri.query = URI.encode_www_form({"token" => token, "path" => file_path})
        reply = Net::HTTP.get_response(uri)
        body = JSON.parse(reply.body)

        if body['status'] == 'done' and delivery.presentation.blank?
          presentation = Presentation.create(user: delivery.user)
          delivery.update(presentation: presentation)
          slides_info=body['slides']
          slides_info.each do |item|
            p=PresentationSlide.new
            p.presentation_id=presentation.id
            p.title="Sem título"
            p.layout_type="cover"
            p.slide_type="content"
            content=[{"type"=>"img",
              "url"=>"http://#{CDI::Config.aws_bucket_name}.s3.amazonaws.com/#{item['path']}",
              "top"=>0,
              "left"=>0,
              "width"=>800,
              "height"=>600,
              "location"=>"aws"
              }]
            p.content=JSON.dump(content)
            p.position=item['order']
            p.metadata=JSON.dump({"thumb_url"=>"http://#{CDI::Config.aws_bucket_name}.s3.amazonaws.com/#{item['path']}"})
            p.save
          end # loop
        elsif retries > 0
          self.class.perform_in 10.seconds ,student_delivery_id, retries-1
        end
        body
      end
    end
  end
end
