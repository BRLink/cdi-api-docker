module CDI
  module V1
    class AvailableVisibilityTypesPresenter < BasePresenter

      def initialize(current_user)
        @user = current_user
      end

      def available_types
        (default_visibility_types + subscribers_groups + student_classes)
      end

      private
        def default_visibility_types
          [
            {id: 'all', type: 'all', label: 'Todos' },
            {id: 'students', type: 'students', label: 'Alunos' },
            {id: 'multipliers', type: 'multipliers', label: 'Multiplicadores' }
          ]
        end

        def subscribers_groups
          @user.subscribers_groups.map { |group| {id: group.id, type: group.class.to_s, label: group.name } }
        end

        def student_classes
          ret = map_student_classes(@user.student_classes)
          if @user.backoffice_profile?
            ret = (ret + map_student_classes(@user.administered_student_classes)).uniq
          end
          ret
        end

        def map_student_classes(student_classes)
          student_classes.map { |cls| {id: cls.id, type: cls.class.to_s, label: cls.name } }
        end
    end
  end
end
