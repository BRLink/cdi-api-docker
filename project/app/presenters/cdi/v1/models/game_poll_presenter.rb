module CDI
  module V1
    module Models
      class GamePollPresenter < BaseGamePresenter

        def answers_sql
          {
            game_question_id: question.fetch(:id, nil)
          }
        end

        def answers
          GameQuestionAnswer.where(answers_sql)
        end

        def options
          question.fetch(:options, [])
        end

        def serializer_name
          'game_poll'
        end

        def game_class
          GamePoll
        end

      end
    end
  end
end
