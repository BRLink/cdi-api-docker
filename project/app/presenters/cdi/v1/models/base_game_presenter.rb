module CDI
  module V1
    module Models
      class BaseGamePresenter < BasePresenter

        attr_reader :attributes, :options

        def initialize(attributes = {}, options = {})
          @attributes = (attributes || {}).as_json.deep_symbolize_keys
          @options = options
        end

        def answers_sql
          {
            game_question_id: (questions || {}).map {|q| q[:id] }
          }
        end

        def answers
          GameQuestionAnswer.where(answers_sql)
        end

        def options
          questions.fetch(:options, [])
        end


        def method_missing(method_name, *args, &block)
          if @attributes.key?(method_name.to_sym)
            return @attributes[method_name.to_sym]
          end

          super(method_name, *args, &block)
        end

        def game_class
          raise('Must be implemented in children class')
        end

      end
    end
  end
end
