module CDI
  module V1
    class UserPreferencesPresenter < BasePresenter

      def initialize(user)
        @user = user
      end

      def music
        @user.preferences.music
      end

      def movie
        @user.preferences.movie
      end

      def book
        @user.preferences.book
      end

      def tv_show
        @user.preferences.tv_show
      end
    end
  end
end
