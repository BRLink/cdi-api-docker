module CDI
  module V1
    module Home
      class BaseHomePresenter < BasePresenter

        def initialize(user)
          @user = user
        end

        def latest_blog_post
          BlogArticle.order(:pub_date).last
        end

        def challenge
          Challenge.latest.first
        end

        def gamefication
          'not_implemented_yet'
        end

        def ranking
          'not_implemented_yet'
        end

        def categories_discussions
          Category.unscoped
            .joins(:category_resources)
            .where('category_resources.resource_type = ?', 'Discussion')
            .select("categories.id, categories.name, COUNT(categories.id) AS categories_count")
            .group("categories.id")
            .order('categories_count DESC')
        end
      end
    end
  end
end
