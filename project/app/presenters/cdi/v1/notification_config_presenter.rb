module CDI
  module V1
    class NotificationConfigPresenter < BasePresenter

      def initialize(current_user)
        @current_user = current_user
      end

      def to_json
        json = []
        NotificationConfig::TYPES.each do |type|
          notification_config = @current_user.notification_config(type)
          json << {
            name: notification_config.notification_type,
            label: I18n.t(notification_config.notification_type, scope: 'cdi.notification_configs'),
            notification: notification_config.can_notification?,
            email: notification_config.can_email?
          }
        end
        return json
      end

    end
  end
end
