module CDI
  module V1
    class StudentDeliveryReviewSerializer < SimpleStudentDeliveryReviewSerializer

      has_one :student_delivery, serializer: ::CDI::V1::SimpleStudentDeliverySerializer

    end
  end
end
