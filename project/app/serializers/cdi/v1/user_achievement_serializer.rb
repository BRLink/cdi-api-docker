module CDI
  module V1
    class UserAchievementSerializer < SimpleUserAchievementSerializer

      #has_one    :user, serializer: SimpleUserSerializer
      has_one    :achievement, serializer: AchievementSerializer

    end
  end
end




