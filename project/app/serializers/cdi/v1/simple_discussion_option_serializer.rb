module CDI
  module V1
    class SimpleDiscussionOptionSerializer < ActiveModel::Serializer

      root false

      attributes :id, :text
    end
  end
end
