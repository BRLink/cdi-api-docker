module CDI
  module V1
    class SimpleQuestionUserAnswerSerializer < ActiveModel::Serializer

      root false

      attributes :id, :question_id, :user_id,
                 :answer_index, :answer_text,
                 :created_at, :updated_at

    end
  end
end
