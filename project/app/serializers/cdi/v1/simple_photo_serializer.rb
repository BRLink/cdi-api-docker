module CDI
  module V1
    class SimplePhotoSerializer < ActiveModel::Serializer

      root false

      attributes :id, :album_id, :user_id, :visible,
                 :position, :caption, :tags,
                 :image_url, :images_url,
                 :created_at, :updated_at

    end
  end
end
