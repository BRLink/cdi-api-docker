module CDI
  module V1
    class GameGroupItemSerializer < SimpleGameGroupItemSerializer

      attributes :items_by_title, :shuffled_group_items

      has_many :groups, serializer: SimpleGameGroupSerializer

      has_one :user, serializer: SimpleUserSerializer

      def filter(keys)
        keys_blacklist = scope.try(:backoffice_profile?) ? [] : [:items_by_title]

        keys - keys_blacklist
      end

    end
  end
end
