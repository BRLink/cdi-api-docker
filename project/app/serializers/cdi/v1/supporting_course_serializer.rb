module CDI
  module V1
    class SupportingCourseSerializer < SimpleSupportingCourseSerializer

      has_many :skills, serializer: SimpleSkillSerializer

      has_one :user, serializer: SimpleUserSerializer

      has_many :categories, serializer: SimpleCategorySerializer

      has_one :presentation, serializer: SimplePresentationSerializer
    end
  end
end
