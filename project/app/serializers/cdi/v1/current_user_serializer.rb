module CDI
  module V1
    class CurrentUserSerializer < SimpleUserSerializer

      attributes :first_name,
                 :last_name,
                 :about,
                 :email,
                 :birthday,
                 :password_reseted_at,
                 :password_updated_at,
                 :oauth_provider,
                 :facebook_oauth_provider_linked,
                 :phone_area_code,
                 :phone_number,
                 :formatted_phone_number,
                 :multiplier?,
                 :notifications_status,
                 :tof_accepted?,
                 :learning_cycles_count,
                 :learning_tracks_count,
                 :completed_enrolled_learning_tracks_count,
                 :pending_enrolled_learning_tracks_count,
                 :pending_student_deliveries_count,
                 :discussions_count,
                 :posts_count,
                 :multimedias_count,
                 :challenges_deliveries_count

      has_one  :address, serializer: AddressSerializer

      has_many :educational_institutions, serializer: SimpleEducationalInstitutionSerializer

      has_many :photos, serializer: SimplePhotoSerializer

      has_many :student_classes, serializer: SimpleStudentClassSerializer

      has_many :administered_student_classes, serializer: SimpleStudentClassSerializer

      has_one  :general_ranking, serializer: SimpleRankingSerializer

      include CDI::V1::SerializerConcerns::FollowingFollowersSerializer

      def administered_student_classes
        return [] unless scope.backoffice_profile?

        object.administered_student_classes
      end

      def filter(keys)
        if scope.backoffice_profile?
          keys
        else
          keys - [:administered_student_classes]
        end
      end

      def learning_tracks_count
        object.learning_tracks.count
      end

      def learning_cycles_count
        object.learning_cycles.count
      end

      def completed_enrolled_learning_tracks_count
        object.enrolled_learning_tracks.completed.count
      end

      def pending_enrolled_learning_tracks_count
        object.enrolled_learning_tracks.pending.count
      end

      ## FIXME precisamos revisar este método, pois para entregas em grupo
      ## pode ocorrer alguma duplicidade.
      def pending_student_deliveries_count
        if object.student?
          object.student_deliveries.where(corrected_at: nil).count +
          object.group_students_deliveries.where(corrected_at: nil).count
        else
          object.received_student_deliveries.where(corrected_at: nil).count
        end
      end

      def discussions_count
        object.discussions.count
      end

      def posts_count
        object.posts.count
      end

      def multimedias_count
        object.multimedias.count
      end

      ## Disabilitado para a v1.2.0
      def challenges_deliveries_count
        object.challenge_deliveries.count
      end

      def photos
        # no querys
        return [] if [0, nil].member?(CDI::Config.user_profile_photos_limit)

        object.photos.limit(CDI::Config.user_profile_photos_limit || 20)
      end

      def facebook_oauth_provider_linked
        object.from_oauth_provider?
      end


      def filter keys
        if object.student?
          blacklisted_keys = [:multimedias_count]
        else
          blacklisted_keys = [:completed_enrolled_learning_tracks_count, :pending_enrolled_learning_tracks_count]
        end
        keys - blacklisted_keys
      end
    end
  end
end
