module CDI
  module V1
    class SimpleBlogArticleSerializer < ActiveModel::Serializer
      root false

      attributes :id,
                 :title,
                 :link,
                 :description,
                 :content,
                 :pub_date
    end
  end
end
