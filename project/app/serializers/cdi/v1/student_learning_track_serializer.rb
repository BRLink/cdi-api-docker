module CDI
  module V1
    class StudentLearningTrackSerializer < SimpleLearningTrackSerializer

      has_many :categories,
              serializer: SimpleCategorySerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      has_many :skills,
              serializer: SimpleSkillSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

    end
  end
end
