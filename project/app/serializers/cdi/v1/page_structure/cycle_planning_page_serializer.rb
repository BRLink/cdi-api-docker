module CDI
  module V1
    module PageStructure
      class CyclePlanningPageSerializer < BaseSerializer

        has_one :cycle_planning, serializer: ModelSerializer::CyclePlanningSerializer

        has_many :questions_templates, serializer: ModelSerializer::QuestionTemplateSerializer

      end
    end
  end
end