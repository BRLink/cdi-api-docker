module CDI
  module V1
    # Usado pelo embed_comment_serializer, quando o comment é
    # embedado diretamente em seu resource.
    class NestedCommentSerializer < SimpleNestedCommentSerializer

      has_one :user, serializer: SimpleUserSerializer

    end
  end
end
