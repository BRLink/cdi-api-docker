module CDI
  module V1
    class SimpleStudentDeliverySerializer < ActiveModel::Serializer

      root false

      attributes :id, :learning_delivery_id, :user_id, :student_class_id,
                 :learning_track_id, :file_url, :link, :format_type,
                 :created_at, :updated_at, :presentation_id, :correct, :corrected_at

    end
  end
end
