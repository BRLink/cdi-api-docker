module CDI
  module V1
    class SimpleGameOpenQuestionSerializer < ActiveModel::Serializer

      root false

      attributes :id, :title, :status, :status_changed_at, :user_id, :video_link, :image_url,
                 :questions_count, :created_at, :updated_at


      def image_url
        object.image
      end

    end
  end
end
