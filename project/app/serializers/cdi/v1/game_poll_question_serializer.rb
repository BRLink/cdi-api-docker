module CDI
  module V1
    class GamePollQuestionSerializer < SimpleGameQuestionSerializer

      has_many :options, serializer: SimpleGameQuestionOptionSerializer

      attributes :options_answers_percent

    end
  end
end
