module CDI
  module V1
    class PhotosAlbumSerializer < ActiveModel::Serializer

      root false

      attributes :id,
                 :title,
                 :description,
                 :total_photos

      has_many :photos, serializer: PhotoSerializer

      def photos
        object.photos.limit(4)
      end

      # FIXME implementar counter_cache para esta coluna
      def total_photos
        object.photos.count
      end
    end
  end
end
