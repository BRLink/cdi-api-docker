module CDI
  module V1
    module SerializerConcerns
      module PublisherSerializer

        extend ActiveSupport::Concern

        included do
          attributes :is_following
        end

        def is_following
          scope && scope.follow?(object)
        end
      end
    end
  end
end
