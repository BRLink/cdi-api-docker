module CDI
  module V1
    module SerializerConcerns
      module FollowingFollowersSerializer

        extend ActiveSupport::Concern

        included do

          attributes :following_count, :followers_count

          has_many :recent_following, serializer: SimpleUserSerializer
          has_many :recent_followers, serializer: SimpleUserSerializer
        end

        def recent_following
          object.users_subscribed.where('user_subscriptions.id != ?', object.self_user_subscription_id).order('user_subscriptions.created_at DESC').limit(6)
        end

        def recent_followers
          object.subscribers_users.where.not(id: object.id).order('user_subscriptions.created_at DESC').limit(6)
        end

        # FIXME Migrar para counter_cache
        def following_count
          object.users_subscribed.where('user_subscriptions.id != ?', object.self_user_subscription_id).count
        end

        # FIXME Migrar para counter_cache
        def followers_count
          object.subscribers_users.where.not(id: object.id).count
        end
      end
    end
  end
end
