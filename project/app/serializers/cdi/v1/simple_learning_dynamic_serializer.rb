module CDI
  module V1
    class SimpleLearningDynamicSerializer < ActiveModel::Serializer

      root false

      attributes :id, :title, :user_id,
                 :student_presentation_id, :multiplier_presentation_id,
                 :perspectives, :perspectives_texts,
                 # this will generate SQL with joins (not a problem for now)
                 # :thumb_url
                 :student_thumbnail_url, :multiplier_thumbnail_url,
                 :status, :parent_id, :created_at, :updated_at

    end
  end
end
