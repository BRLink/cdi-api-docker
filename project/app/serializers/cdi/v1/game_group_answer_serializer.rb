module CDI
  module V1
    class GameGroupAnswerSerializer < ActiveModel::Serializer

      root false

      attributes :id, :user_id, :items, :group_id, :correct?,
                 :correct_items, :wrong_items, :created_at, :updated_at


      def group_id
        object.game_id
      end

      def items
        object.words
      end

      def correct_items
        object.correct_words
      end

      def wrong_items
        object.wrong_words
      end

    end
  end
end
