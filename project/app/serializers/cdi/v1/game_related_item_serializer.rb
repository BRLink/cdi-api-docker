module CDI
  module V1
    class GameRelatedItemSerializer < SimpleGameRelatedItemSerializer

      attributes :random_game_grid

      has_one :user, serializer: SimpleUserSerializer

      has_many :items, serializer: SimpleGameItemSerializer

      def random_game_grid
        object.random_game_grid_for_user(scope)
      end

    end
  end
end
