module CDI
  module V1
    class SimpleStudentDeliveryReviewSerializer < ActiveModel::Serializer

      root false

      attributes :id, :student_delivery_id, :correct, :comment,
                 :multiplier_user_id, :created_at, :updated_at

    end
  end
end
