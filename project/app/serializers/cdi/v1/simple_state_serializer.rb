module CDI
  module V1
    class SimpleStateSerializer < ActiveModel::Serializer

      root false

      attributes :id, :name, :uf
    end
  end
end
