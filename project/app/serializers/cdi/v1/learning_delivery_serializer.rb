module CDI
  module V1
    class LearningDeliverySerializer < SimpleLearningDeliverySerializer

      has_one :learning_dynamic, serializer: SimpleLearningDynamicSerializer

    end
  end
end
