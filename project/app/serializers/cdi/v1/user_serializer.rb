module CDI
  module V1
    class UserSerializer < SimpleUserSerializer

      attributes :first_name, :last_name
      attributes :profile_images
    end
  end
end
