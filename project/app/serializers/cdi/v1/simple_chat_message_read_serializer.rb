module CDI
  module V1
    class SimpleChatMessageReadSerializer < ActiveModel::Serializer

      root false
      attributes :id, :user_id, :chat_message_id
    end
  end
end
