module CDI
  module V1
    class AddressSerializer < SimpleAddressSerializer

      has_one :city, serializer: SimpleCitySerializer

    end
  end
end
