module CDI
  module V1
    class SimpleLearningDeliverySerializer < ActiveModel::Serializer

      root false

      attributes :id, :format_type, :description,
                 :delivery_date, :delivery_kind,
                 :group_size, :learning_dynamic_id

    end
  end
end
