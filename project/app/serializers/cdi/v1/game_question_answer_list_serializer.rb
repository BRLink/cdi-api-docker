module CDI
  module V1
    class GameQuestionAnswerListSerializer < SimpleGameQuestionAnswerSerializer

      has_one :user, serializer: SimpleUserSerializer

      has_one :game,
              serializer: SimpleGameSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      has_one :game_question,
              serializer: SimpleGameQuestionSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

    end
  end
end
