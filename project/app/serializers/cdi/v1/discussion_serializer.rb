module CDI
  module V1
    class DiscussionSerializer < SimpleDiscussionSerializer

      attributes :answered, :answered_option_id, :answers_count, :viewed, :views_count

      has_many :options, serializer: DiscussionOptionAnsweredSerializer

      has_one :user, serializer: SimpleUserSerializer

      has_many :categories, serializer: SimpleCategorySerializer

      has_one :best_comment, serializer: EmbedCommentSerializer

      def answered
        @answered ||= scope && object.answers.exists?(user_id: scope.id)
      end

      def answered_option_id
        return nil unless answered
        object.answers.find_by(user_id: scope.id).option_id
      end

      def answers_count
        object.answers.count
      end

      def viewed
        scope && object.views.exists?(user: scope)
      end

      include CDI::V1::SerializerConcerns::ResourceVisibilitySerializer
      include CDI::V1::SerializerConcerns::CommentableSerializer
      include CDI::V1::SerializerConcerns::LikeableSerializer
      include CDI::V1::SerializerConcerns::ResourceFeaturedSerializer

      def filter(keys)
        blacklisted_keys = object.poll? ? [] : [:options, :answered, :answered_option_id, :answers_count]
        return keys - blacklisted_keys
      end
    end
  end
end
