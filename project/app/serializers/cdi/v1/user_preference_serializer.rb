module CDI
  module V1
    class UserPreferenceSerializer < ActiveModel::Serializer

      root false

      attributes :id, :type, :title, :author, :link

      def type
        object.preference_type
      end

    end
  end
end
