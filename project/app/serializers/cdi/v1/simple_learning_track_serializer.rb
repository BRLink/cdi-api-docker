module CDI
  module V1
    class SimpleLearningTrackSerializer < ActiveModel::Serializer

      root false

      attributes :id,
                 :name,
                 :tags,
                 :category_ids,
                 :average_score,
                 :status,
                 :status_changed_at,
                 :created_at,
                 :updated_at

      attributes :cover_image_url

      def cover_image_url
        return nil if @meta && @meta[:include_cover_image_url] == false
        object.try(:cover_image_url)
      end

    end
  end
end
