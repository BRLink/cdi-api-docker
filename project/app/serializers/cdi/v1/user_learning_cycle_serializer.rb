module CDI
  module V1
    class UserLearningCycleSerializer < SimpleLearningCycleSerializer

      has_many :learning_tracks,
              serializer: SimpleLearningTrackSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      has_one :user,
              serializer: SimpleUserSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      has_many :categories,
              serializer: SimpleCategorySerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

    end
  end
end
