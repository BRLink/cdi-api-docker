module CDI
  module V1
    class SimplePresentationSerializer < ActiveModel::Serializer
      root false
      attributes :id, :presentation_type, :status,
                 :slides_count, :created_at, :updated_at
    end
  end
end
