module CDI
  module V1
    class SimpleGameQuestionOptionSerializer < ActiveModel::Serializer

      root false

      # to avoid two key for same resource in cache, `correct` flag is
      # filtered before response rendering depending in `current_user`
      attributes :id, :game_question_id, :text, :position, :created_at, :updated_at, :correct
    end
  end
end
