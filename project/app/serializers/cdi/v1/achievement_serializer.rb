module CDI
  module V1
    class AchievementSerializer < SimpleAchievementSerializer

      attributes :name
      has_many :custom_scores, serializer: SimplePointSerializer

    end
  end
end


