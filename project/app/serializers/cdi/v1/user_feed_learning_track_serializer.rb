module CDI
  module V1
    class UserFeedLearningTrackSerializer < SimpleLearningTrackSerializer
      attributes :presentation_slides_count
    end
  end
end
