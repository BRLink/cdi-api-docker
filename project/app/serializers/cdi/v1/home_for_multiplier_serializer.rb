module CDI
  module V1
    class HomeForMultiplierSerializer < SimpleHomeSerializer

      attributes :learning_tracks_reports

      has_many :recent_library_items, serializer: LibrarySerializer
      has_many :learning_tracks_recommendations, serializer: LearningTrackHomeSerializer
    end
  end
end
