module CDI
  module V1
    class SimpleGameQuestionSerializer < ActiveModel::Serializer

      root false

      attributes :id, :title, :input_type, :position,
                 :allow_multiples_answers, :created_at, :updated_at
    end
  end
end
