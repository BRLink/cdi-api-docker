module CDI
  module V1
    class GameQuestionSerializer < SimpleGameQuestionSerializer

      # TODO: Review this
      has_one :questionable, serializer: SimpleGameSerializer

      has_many :options, serializer: SimpleGameQuestionOptionSerializer
    end
  end
end
