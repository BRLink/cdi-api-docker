module CDI
  module V1
    class PresentationSlideSerializer < SimplePresentationSlideSerializer

      has_one :presentation, serializer: SimplePresentationSerializer

    end
  end
end
