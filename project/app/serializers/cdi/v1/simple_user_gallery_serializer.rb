module CDI
  module V1
    class SimpleUserGallerySerializer < ActiveModel::Serializer

      root false

      attributes :id, :user_id, :active,
                 :description, :tags,
                 :cover_image_url, :cover_images_url,
                 :created_at, :updated_at
    end
  end
end
