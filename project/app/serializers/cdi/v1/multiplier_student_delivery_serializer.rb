module CDI
  module V1
    class MultiplierStudentDeliverySerializer < SimpleStudentDeliverySerializer

      has_one :learning_delivery,
              serializer: SimpleLearningDeliverySerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      has_one :learning_track,
              serializer: SimpleLearningTrackSerializer,
              meta: {
                include_cover_image_url: false
              },
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      has_one :student_class,
              serializer: SimpleStudentClassSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      has_one :user,
              serializer: SimpleUserSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      has_many :students,
               each_serializer: SimpleUserSerializer,
               embed: :ids,
               embed_in_root_key: :linked_data,
               embed_in_root: true


      has_one :review# serializer: SimpleStudentDeliveryReviewSerializer

    end
  end
end
