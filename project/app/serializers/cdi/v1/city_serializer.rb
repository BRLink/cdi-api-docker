module CDI
  module V1
    class CitySerializer < SimpleCitySerializer

      attributes :educational_institutions_count

      has_many :educational_institutions, serializer: SimpleEducationalInstitutionSerializer

      def educational_institutions_count
        educational_institutions.size
      end

      def educational_institutions
        @educational_institutions ||= object.educational_institutions
      end

    end
  end
end
