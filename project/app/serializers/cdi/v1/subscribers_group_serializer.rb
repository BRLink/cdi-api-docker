module CDI
  module V1
    class SubscribersGroupSerializer < ActiveModel::Serializer

      root false

      attributes :id, :name, :total_members

    end
  end
end
