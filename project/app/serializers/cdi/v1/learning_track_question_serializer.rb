module CDI
  module V1
    class LearningTrackQuestionSerializer < ActiveModel::Serializer

      root false

      attributes :id, :question_template_id ,:title, :subtitle,
                 :question_type, :instructions, :choices

      has_one :learning_track,
              serializer: SimpleLearningTrackSerializer,
              meta: {
                include_cover_image_url: false
              },
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      has_many :answers,
              serializer: SimpleQuestionUserAnswerSerializer

      def answer
        # avoiding to use .last to not execute a NEW SQL Query
        object.answers[-1]
      end

      def learning_track
        object.template_questionable
      end

    end
  end
end

