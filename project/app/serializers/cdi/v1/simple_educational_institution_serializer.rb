module CDI
  module V1
    class SimpleEducationalInstitutionSerializer < ActiveModel::Serializer

      root false

      attributes :id,
                 :name,
                 :about,
                 :profile_image_url,
                 :profile_cover_image_url,
                 :profile_images,
                 :profile_cover_images,
                 :site,
                 :telephone,
                 :email

      include CDI::V1::SerializerConcerns::PublisherSerializer
    end
  end
end
