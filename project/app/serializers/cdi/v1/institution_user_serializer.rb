module CDI
  module V1
    class InstitutionUserSerializer < SimpleInstitutionUserSerializer

      attributes :user
      def user
        SimpleUserSerializer.new(object.user)
      end
    end
  end
end
