module CDI
  module V1
    class UserPreferencesSerializer < ActiveModel::Serializer

      has_many :music, serializer: UserPreferenceSerializer
      has_many :movie, serializer: UserPreferenceSerializer
      has_many :book, serializer: UserPreferenceSerializer
      has_many :tv_show, serializer: UserPreferenceSerializer
    end
  end
end
