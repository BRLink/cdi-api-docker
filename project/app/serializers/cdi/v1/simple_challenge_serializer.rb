module CDI
  module V1
    class SimpleChallengeSerializer < ActiveModel::Serializer

      root false

      attributes :id,
                 :title,
                 :description,
                 :format_type,
                 :delivery_title,
                 :delivery_date,
                 :delivery_kind,
                 :group_size,
                 :cover_image_url,
                 :created_at,
                 :updated_at,
                 :presentation_id,
                 :tags,
                 :status,
                 :status_changed_at
    end
  end
end
