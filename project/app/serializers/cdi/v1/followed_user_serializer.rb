module CDI
  module V1
    class FollowedUserSerializer < SimpleUserSerializer

      include CDI::V1::SerializerConcerns::PublisherSerializer

    end
  end
end
