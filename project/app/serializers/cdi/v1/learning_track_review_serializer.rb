module CDI
  module V1
    class LearningTrackReviewSerializer < SimpleLearningTrackReviewSerializer

      has_one :learning_track, serializer: SimpleLearningTrackSerializer

    end
  end
end
