module CDI
  module V1
    class MultimediaSerializer < SimpleMultimediaSerializer

      has_one :user, serializer: SimpleUserSerializer
      has_many :categories, serializer: SimpleCategorySerializer

    end
  end
end
