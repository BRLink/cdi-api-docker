module CDI
  module V1

    ## Serializer utilizado nos métodos de 'commentable'
    class EmbedCommentSerializer < SimpleCommentSerializer

      root false

      attributes :id,
                 :content,
                 :total_children_comments

      has_one :user,
              serializer: SimpleUserSerializer

      has_many :nested_comments,
               serializer: NestedCommentSerializer


      include CDI::V1::SerializerConcerns::LikeableSerializer

      def nested_comments
        object.nested_comments.includes(:user).limit(3)
      end
    end
  end
end
