module CDI
  module V1
    class SimpleUserAchievementSerializer < ActiveModel::Serializer

      attributes :id,
                 :history_message,
                 :resource_type,
                 :resource_id,
                 :created_at

    end
  end
end



