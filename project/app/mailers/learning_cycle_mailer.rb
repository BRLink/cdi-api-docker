class LearningCycleMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.learning_cycle_mailer.new_cycle.subject
  #
  def new_cycle(learning_cycle)
    @learning_cycle = learning_cycle
    @user           = @learning_cycle.user

    mail to: @user.email
  end
end
