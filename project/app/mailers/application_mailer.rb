class ApplicationMailer < ActionMailer::Base
  default from: CDI::Config.default_from_emails
  layout 'mailer'
end
