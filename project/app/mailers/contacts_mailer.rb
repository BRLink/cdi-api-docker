class ContactsMailer < ApplicationMailer

  DESTINY_EMAILS = (CDI::Config.contact_emails || CDI::Config.admin_email)
                   .split(/\,|\;/)
                   .map(&:squish)
                   .freeze

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.contacts_mailer.contact_form.subject
  #
  def contact_form(contact_form, to_mails = nil)
    @contact_form = contact_form

    subject = default_i18n_subject(subject: contact_form.subject)

    mail to: to_mails || DESTINY_EMAILS, subject: subject, from: contact_form.email
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.contacts_mailer.feedback_contact_form.subject
  #
  def feedback_contact_form(feedback_contact, to_mails = nil)
    @feedback_contact = feedback_contact

    mail to: to_mails || DESTINY_EMAILS
  end
end
