require 'rails_helper'

RSpec.describe LearningDynamic, type: :model do
  it 'creates dynamics' do
    dynamic = build(:learning_dynamic)
    expect(dynamic).to be_valid
  end

  describe 'clone dynamics' do
    before(:each) do
      @orig = build(:learning_dynamic)
      @orig.set_status(:published)
    end
    it 'can clone valid dynamic' do
      dup = @orig.duplicate(@orig.user)
      expect(dup).to be_valid
      expect(dup.student_presentation.slides.size).to eq 1
      expect(dup.multiplier_presentation.slides.size).to eq 1
      expect(dup.status).to eq 'draft'
    end
    it 'can capture presentation error' do
      @orig.student_presentation.update_column(:presentation_type, 'nothing')
      dup = @orig.duplicate(@orig.user)
      expect(dup.errors.blank?).to be false
      expect(dup.errors.key?(:student_presentation_presentation_type)).to be true
    end
    it 'can capture slide error' do
      @orig.student_presentation.slides.first.update_column(:content, nil)
      dup = @orig.duplicate(@orig.user)
      expect(dup.errors.key?(:student_presentation_slide_0_content)).to be true
    end
  end

end
