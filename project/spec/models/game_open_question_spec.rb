require 'rails_helper'

RSpec.describe GameOpenQuestion, type: :model do
  before(:each) do
    @question = create(:game_open_question)
  end

  it 'must be valid' do
    question = build(:game_open_question)
    expect(question).to be_valid
  end

  it 'must have column indexes' do
    expect(subject).to have_db_index(:status)
    expect(subject).to have_db_index(:title)
    expect(subject).to have_db_index(:user_id)
  end

  it 'must belongs to user' do
    expect(@question).to belong_to(:user)
  end

  it 'has many questions and answers' do
    expect(@question).to have_many(:questions)
    expect(@question).to have_many(:answers)
  end

  it 'must change the status after reply' do
  end

  it 'only can be flaged as correct/false by owner teacher' do
  end

  it 'has many comments by teacher'
  it 'must update questions counter cache correctly'
  it 'must be invalid'
  it 'must limit the number of questions'
  it 'respond to alias for user'

end
