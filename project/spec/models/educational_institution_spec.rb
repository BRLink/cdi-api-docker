require 'rails_helper'

RSpec.describe EducationalInstitution, type: :model do
  before(:each) do
    @educational_institution = create(:educational_institution)
    @educational_institution.valid?
  end

  it 'has a valid username' do
    institution = create(:educational_institution, name: 'UserNamE-InstiTution')
    expect(institution).to be_valid

    expect(institution.errors[:username]).to eq([])
    expect(institution.username).to eq('username_institution')
  end

  it 'has a valid normalized username' do
    institution = create(:educational_institution, username: 'User Name Educational Institution')
    expect(institution.username).to eq('user_name_educational_institution')

    institution = create(:educational_institution, username: 'User Name Educational Institution')
    expect(institution.username).to eq('user_name_educational_institution2')

    institution = create(:educational_institution, username: 'User Name Educational Institution')
    expect(institution.username).to eq('user_name_educational_institution3')

    institution = create(:educational_institution, username: 'User Name Educational Institution')
    expect(institution.username).to eq('user_name_educational_institution4')

    institution = create(:educational_institution, name: 'My Awesome Institution')
    expect(institution.username).to eq('my_awesome_institution')

    institution = create(:educational_institution, name: 'My Awesome Institution')
    expect(institution.username).to eq('my_awesome_institution2')
  end

  it 'belongs to user' do
    expect(@educational_institution.admin_user).not_to be_nil
    expect(@educational_institution).to belong_to(:admin_user)
  end

  it 'has columns indexes' do
    expect(subject).to have_db_index(:name)
    expect(subject).to have_db_index(:username)
    expect(subject).to have_db_index(:admin_user_id)
  end

  it 'must be invalid' do
    institution = build(:educational_institution, name: nil)

    expect(institution).to_not be_valid
    expect(institution.errors[:name]).to_not be eq([])
  end

end
