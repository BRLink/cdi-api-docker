require 'rails_helper'

RSpec.describe PresentationSlide, type: :model do
  it 'can create content slide with content' do
    slide = build(:presentation_slide)
    expect(slide).to be_valid
  end

  it 'can not create content slide without content' do
    slide = build(:slide_without_content)
    expect(slide).not_to be_valid
  end

  it 'can create linked slide without content' do
    slide = build(:slide_linked_to_res)
    expect(slide).to be_valid
  end
  it 'can create dynamic slide' do
    slide = build(:dynamic_slide)
    expect(slide.slide_type).to eq('dynamic')
    expect(slide).to be_valid
  end
  it 'uses amoeba' do
    slide = build(:presentation_slide)
    slide.views_count=1
    slide.save!
    dup = slide.amoeba_dup
    expect(dup.views_count).to be nil
  end
end
