require 'rails_helper'

RSpec.describe SupportingCourse, type: :model do
	describe "SupportingCourse validations" do
		it { should validate_presence_of(:title)}
		it { should validate_presence_of(:category)}
		it { should validate_presence_of(:user)}
	end
	describe "SupportingCourse associations" do
		it { belong_to(:category) }
		it { belong_to(:user) }
		it { belong_to(:presentation) }
	end

  describe 'clone supporting course' do
    before(:each) do
      @orig = create(:supporting_course)
      @orig.set_status(:published)
    end
    it 'can clone valid suppcourse' do
      dup = @orig.duplicate(@orig.user)
      expect(dup).to be_valid
      expect(dup.presentation.slides.size).to be(5)
      expect(dup.status).to eq 'draft'
    end
    it 'can capture presentation error' do
      @orig.presentation.update_column(:presentation_type, 'nothing')
      dup = @orig.duplicate(@orig.user)
      expect(dup.errors.blank?).to be false
      expect(dup.errors.key?(:presentation_presentation_type)).to be true
    end
    it 'can capture slide error' do
      @orig.presentation.slides.first.update_column(:content, nil)
      dup = @orig.duplicate(@orig.user)
      expect(dup.errors.key?(:presentation_slide_0_content)).to be true
    end
  end


end
