FactoryGirl.define do

  factory :survey_open_question do
    sequence(:title) { |n| "Survey Open Question #{n}" }
    association :user, factory: :staff_user

    trait :with_answers do
      transient do
        answers_count 5
      end
      after(:create) do |survey_open_question, evaluator|
        evaluator.answers_count.times do
          survey_open_question.answers << create(:answer)
        end
      end
    end

    trait :with_survey_questions do
      transient do
        survey_questions_count 3
      end
      after(:create) do |survey_open_question, evaluator|
        evaluator.survey_questions_count.times do
          survey_open_question.questions << create(:survey_question)
        end
      end
    end
  end
end


