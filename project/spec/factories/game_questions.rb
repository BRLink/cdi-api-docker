FactoryGirl.define do
  factory :game_question do
    questionable nil
    input_type "MyString"
    position 1
    question_text "MyString"
    correct false
    image ""
  end

end
