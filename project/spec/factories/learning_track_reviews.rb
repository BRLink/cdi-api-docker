FactoryGirl.define do
  factory :learning_track_review do
    learning_track nil
    user nil
    comment "MyText"
    review_type 1
  end

end
