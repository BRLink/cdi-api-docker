FactoryGirl.define do
  factory :game_question_option do
    game_question nil
    position 1
    text "MyString"
    description "MyString"
    correct false
  end

end
