FactoryGirl.define do
  factory :game_item do
    game_related_item nil
    word "MyString"
    related_word "MyString"
    image "MyString"
  end

end
