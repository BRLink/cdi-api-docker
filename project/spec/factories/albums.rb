FactoryGirl.define do
  factory :album do
    user nil
    title "MyString"
    description "MyString"
    active true
    position 1
    tags ["MyString"]
  end

end
