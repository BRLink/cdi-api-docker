FactoryGirl.define do
  factory :game_open_question do
    association :user, factory: :teacher_user_without_origin
    title "Por que podemos entender que o resultado da Revolução Inglesa foi de suma importância para que a Inglaterra assumisse a dianteira na Revolução Industrial?"
    questions_count 1
    image nil
    status nil
    status_changed_at nil
    # image {Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, '/spec/fixtures/library/test.png')))}

    factory :game_open_question_with_questions do
      transient do
        questions_count 5
      end

      after(:create) do |question, evaluator|
        create_list(:game_question, evaluator.questions_count, questionable: question)
      end
    end
  end
end
