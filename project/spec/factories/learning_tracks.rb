FactoryGirl.define do
  factory :learning_track do
    name { Faker::Commerce.product_name }
    categories { [ create(:category) ] }
    learning_cycle

    factory :learning_track_with_tags do
      transient do
        tags_count 5
      end

      before(:create) do |track, evaluator|
        tags = Faker::Commerce.department(evaluator.tags_count, true).split(/\,|\&/).map(&:squish)
        track.tags = tags
      end
    end

    factory :learning_track_with_skills do
      transient do
        skills_count 3
      end

      before(:create) do |track, evaluator|
        evaluator.skills_count.times {
          track.skills << create(:skill)
        }
      end
    end
  end

end
