FactoryGirl.define do
  factory :question do
    question_template
    association :template_questionable, factory: :learning_track
    categories { [create(:category)] }

    factory :question_with_answers do
      transient do
        answers_count 5
      end

      after(:create) do |question, evaluator|
        create_list(:question_user_answer, evaluator.answers_count, question: question)
      end
    end

  end

  factory :question_without_template, class: Question do
    question_template nil
    association :template_questionable, factory: :learning_track
  end
end
