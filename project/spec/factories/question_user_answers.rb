FactoryGirl.define do
  factory :question_user_answer do
    question
    association :user, factory: :student_user
    answer_text { Faker::Lorem.sentence }
    answer_index { 1 }
  end
end
