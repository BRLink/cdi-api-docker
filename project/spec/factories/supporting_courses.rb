FactoryGirl.define do
  factory :supporting_course do
    title "math"
  	association :user, factory: :admin
    association :category
  	tags ["atag"]
  	association :presentation, factory: :presentation_with_slides
  end

end
