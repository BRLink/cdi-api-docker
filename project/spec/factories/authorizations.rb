FactoryGirl.define do
  factory :authorization do |f|
    association :user, factory: :student_user
    provider 'faker'
  end
end
