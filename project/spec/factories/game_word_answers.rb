FactoryGirl.define do
  factory :game_word_answer do
    association :user, factory: :student_user_without_origin
    association :game_wordable, factory: :game_complete_sentence
    correct false
    words []
  end

end
