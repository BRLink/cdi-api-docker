class CreateDiscussionViews < ActiveRecord::Migration
  def change
    create_table :discussion_views do |t|
      t.timestamps null: false
      t.belongs_to :user, null: false
      t.belongs_to :discussion, null: false
    end
    add_foreign_key :discussion_views, :users, on_delete: :cascade
    add_foreign_key :discussion_views, :discussions, on_delete: :cascade
    add_index :discussion_views, [:discussion_id, :user_id], unique: true
  end
end
