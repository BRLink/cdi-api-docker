class AddVideoLinkToGameOpenQuestions < ActiveRecord::Migration
  def change
    add_column :game_open_questions, :video_link, :string
  end
end
