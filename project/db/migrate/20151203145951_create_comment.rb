class CreateComment < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.references :user, index: true
      t.string :resource_type, null: false
      t.integer :resource_id, null: false, index: true
      t.string :content, null: false
      t.integer :parent_id, null: true, index: true
      t.timestamps null: false
    end
    add_foreign_key :comments, :users
  end
end
