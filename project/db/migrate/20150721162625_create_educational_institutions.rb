class CreateEducationalInstitutions < ActiveRecord::Migration
  def change
    create_table :educational_institutions do |t|
      t.string :name
      t.string :username
      t.text :about
      t.string :profile_image
      t.string :profile_cover_image
      t.integer :admin_user_id

      t.timestamps null: false
    end
    add_index :educational_institutions, :name
    add_index :educational_institutions, :username
    add_index :educational_institutions, :admin_user_id
  end
end
