class CreateTrackReviewReportMaterializedView < ActiveRecord::Migration
  def up
    execute 'DROP MATERIALIZED VIEW IF EXISTS track_review_reports_matview'
    execute <<-SQL
      CREATE MATERIALIZED VIEW track_review_reports_matview AS
      SELECT
       ltr.learning_track_id,
       ltr.review_type,
       ltr.student_class_id,
       -- Total by review_type column
       COUNT(*) as total_reviews_by_type,
       -- Percentage for class by review_type
       round(
        -- Amount
        ((COUNT(*) * 100)::numeric)/
        (SELECT COUNT(*)
          FROM learning_track_reviews
          WHERE learning_track_id = ltr.learning_track_id AND
                student_class_id = ltr.student_class_id
       ), 2) as total_percent
       FROM learning_track_reviews ltr
      GROUP BY ltr.review_type, ltr.learning_track_id, ltr.student_class_id;
    SQL
  end

  def down
    execute 'DROP MATERIALIZED VIEW IF EXISTS track_review_reports_matview'
  end
end
