class CreateChatMessageReads < ActiveRecord::Migration
  def change
    create_table :chat_message_reads do |t|
      t.integer :user_id, null: false
      t.integer :chat_message_id, null: false
      t.timestamps null: false
    end
  end
end
