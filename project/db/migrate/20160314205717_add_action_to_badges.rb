class AddActionToBadges < ActiveRecord::Migration
  def change
    add_column :badges, :action, :text
    add_index  :badges, :action, unique: true
  end
end
