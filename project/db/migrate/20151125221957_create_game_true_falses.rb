class CreateGameTrueFalses < ActiveRecord::Migration
  def change
    create_table :game_true_false do |t|
      t.references :user, index: true
      t.integer :questions_count
      t.string :status
      t.datetime :status_changed_at

      t.timestamps null: false
    end

    add_index :game_true_false, :status
    add_foreign_key :game_true_false, :users
  end
end
