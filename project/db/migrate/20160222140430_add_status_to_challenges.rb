class AddStatusToChallenges < ActiveRecord::Migration
  def change
    add_column :challenges, :status, :string, null: false, default: 'draft'
    add_column :challenges, :status_changed_at, :timestamp
  end
end
