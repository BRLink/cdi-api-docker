class AddPhoneNumberAndPhoneAreaCodeToUser < ActiveRecord::Migration
  def change
    add_column :users, :phone_number, :string
    add_column :users, :phone_area_code, :string
  end
end
