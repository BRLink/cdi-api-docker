class CreateCyclePlannings < ActiveRecord::Migration
  def change
    create_table :cycle_plannings do |t|
      t.references :learning_cycle, index: true

      t.timestamps null: false
    end
    add_foreign_key :cycle_plannings, :learning_cycles
  end
end
