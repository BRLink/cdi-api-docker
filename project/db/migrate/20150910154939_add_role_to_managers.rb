class AddRoleToManagers < ActiveRecord::Migration
  def change
  	add_column :managers, :role, :integer, default: 10
  end
end
