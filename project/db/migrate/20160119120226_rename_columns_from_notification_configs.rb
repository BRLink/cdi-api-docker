class RenameColumnsFromNotificationConfigs < ActiveRecord::Migration
  def change
    rename_column :notification_configs, :receive_email, :can_email
    rename_column :notification_configs, :receive_notification, :can_notification
  end
end
