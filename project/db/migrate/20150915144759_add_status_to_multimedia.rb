class AddStatusToMultimedia < ActiveRecord::Migration
  def change
  	add_column :multimedia, :status, :string
  	add_column :multimedia, :status_changed_at, :datetime
  	add_index  :multimedia, :status
  end
end
