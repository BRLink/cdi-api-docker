# https://github.com/plentz/lol_dba/
class AddMissingIndexes < ActiveRecord::Migration
  def change
    add_index :educational_institutions_users, :educational_institution_id, name: 'index_instititution_join_table_institution_id'
    add_index :educational_institutions_users, [:educational_institution_id, :user_id], name: 'index_educational_institutions_user_id_institution_id', unique: true
    add_index :educational_institutions_users, :user_id, name: 'index_instititution_join_table_user_id'

    add_index :supporting_courses_skills, :skill_id
    add_index :supporting_courses_skills, :supporting_course_id

    add_index :roles, ["resource_id", "resource_type"]

    add_index :questions, ["template_questionable_id", "template_questionable_type"], name: 'index_questions_questionable_id_questionable_type'

    add_index :learning_dynamics_skills, :skill_id
    add_index :learning_dynamics_skills, [:learning_dynamic_id, :skill_id], unique: true, name: 'unique_index_learning_dynamics_skills'
    add_index :learning_dynamics_skills, :learning_dynamic_id

    add_index :supporting_courses_skills, [:skill_id, :supporting_course_id], unique: true, name: 'unique_index_supporting_courses_skills'

    add_index :learning_dynamics, :student_presentation_id
    add_index :learning_dynamics, :multiplier_presentation_id

    add_index :resource_histories, ["historable_id", "historable_type"]


    add_index :origins, ["originable_id", "originable_type"]

    add_index :learning_tracks_skills, :learning_track_id
    add_index :learning_tracks_skills, :skill_id
    add_index :learning_tracks_skills, [:learning_track_id, :skill_id], unique: true, name: 'unique_index_learning_tracks_skills'


    add_index :addresses, ["addressable_id", "addressable_type"]

    add_index :learning_tracks, :parent_id
    add_index :learning_tracks, :presentation_id

    add_index :presentation_slides, ["associated_resource_id", "associated_resource_type"], name: 'index_presentation_slides_associated_resource'

    add_index :game_questions, ["questionable_id", "questionable_type"]

    add_index :notifications, ["notificable_id", "notificable_type"]
  end
end
