class AddAssetMetadataToPhoto < ActiveRecord::Migration
  def change
    add_column :photos, :assets_metadata, :json
  end
end
