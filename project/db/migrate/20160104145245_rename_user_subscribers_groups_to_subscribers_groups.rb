class RenameUserSubscribersGroupsToSubscribersGroups < ActiveRecord::Migration
  def change
    rename_table :user_subscribers_groups, :subscribers_groups
    add_foreign_key :subscribers_groups, :users, on_delete: :cascade
  end
end
