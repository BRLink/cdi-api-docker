class DropTableBlogMedia < ActiveRecord::Migration
  def change
  	drop_table :blog_media, if_exists: true
  end
end
