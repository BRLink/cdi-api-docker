class CreateChannels < ActiveRecord::Migration
  def change
    create_table :channels do |t|
      t.references :student_class
      t.references :learning_track

      t.timestamp
    end
    add_foreign_key :channels, :student_classes
    add_foreign_key :channels, :learning_tracks
    add_index :channels, [:student_class_id, :learning_track_id], unique: true
  end
end
