class AddParentIdToSupportingCourses < ActiveRecord::Migration
  def change
    add_column :supporting_courses, :parent_id, :integer
  end
end
