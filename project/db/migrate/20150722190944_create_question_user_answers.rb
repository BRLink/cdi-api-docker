class CreateQuestionUserAnswers < ActiveRecord::Migration
  def change
    create_table :question_user_answers do |t|
      t.references :question, index: true
      t.references :user, index: true
      t.integer :answer_index, index: true
      t.string :answer_text

      t.timestamps null: false
    end
    add_foreign_key :question_user_answers, :questions
  end
end
