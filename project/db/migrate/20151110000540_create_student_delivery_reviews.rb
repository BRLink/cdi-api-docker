class CreateStudentDeliveryReviews < ActiveRecord::Migration
  def change
    create_table :student_delivery_reviews do |t|
      t.references :student_delivery, index: true
      t.boolean :correct
      t.text :comment
      t.integer :multiplier_user_id

      t.timestamps null: false
    end

    add_index :student_delivery_reviews, :correct
    add_index :student_delivery_reviews, :multiplier_user_id
    add_foreign_key :student_delivery_reviews, :student_deliveries
  end
end
