class RemoveResourceFromUserBadge < ActiveRecord::Migration
  def change
    remove_column :user_badges, :resource_type
    remove_column :user_badges, :resource_id
  end
end
