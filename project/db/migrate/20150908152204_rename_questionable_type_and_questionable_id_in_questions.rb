class RenameQuestionableTypeAndQuestionableIdInQuestions < ActiveRecord::Migration
  def change
    rename_column :questions, :questionable_type, :template_questionable_type
    rename_column :questions, :questionable_id, :template_questionable_id
  end
end
