class RemoveLearningTrackIdFromPresentation < ActiveRecord::Migration
  def change
    remove_column :presentations, :learning_track_id, :integer, index: true
    add_column :learning_tracks, :presentation_id, :integer
  end
end
