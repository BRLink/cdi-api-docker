class RenameGameOpenQuestionsToBaseOpenQuestions < ActiveRecord::Migration
  def change
    rename_table :game_open_questions, :base_open_questions
    add_column :base_open_questions, :type, :string, index: true
  end
end
