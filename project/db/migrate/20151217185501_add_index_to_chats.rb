class AddIndexToChats < ActiveRecord::Migration
  def change
    add_index :chat_room_users, [:user_id, :chat_room_id], unique: true
    add_index :chat_message_reads, [:user_id, :chat_message_id], unique: true
  end
end
