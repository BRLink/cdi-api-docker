class AlterSubscriberDiscussions < ActiveRecord::Migration
  def change
    drop_table :subscriber_discussions
    create_table :subscriber_discussions do |t|
      t.belongs_to :user_subscription, null: false
      t.belongs_to :discussion, null: false
      t.timestamps
    end
    add_foreign_key :subscriber_discussions, :user_subscriptions, on_delete: :cascade
    add_foreign_key :subscriber_discussions, :discussions, on_delete: :cascade
    add_index :subscriber_discussions, [:user_subscription_id, :discussion_id], unique: true, name: 'idx_subscriber_discussions_user_subscription_discussion_uniq'
  end
end
