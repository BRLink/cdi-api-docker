class AddPresentationToStudentDeliveries < ActiveRecord::Migration
  def change
    add_column :student_deliveries, :presentation_id, :integer
  end
end
