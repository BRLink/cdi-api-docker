class AddLinkToStudentDelivery < ActiveRecord::Migration
  def change
    add_column :student_deliveries, :link, :string
    add_index :student_deliveries, :link
  end
end
