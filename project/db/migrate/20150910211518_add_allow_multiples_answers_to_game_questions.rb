class AddAllowMultiplesAnswersToGameQuestions < ActiveRecord::Migration
  def change
    add_column :game_questions, :allow_multiples_answers, :boolean, default: false
  end
end
