class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.references :city, index: true
      t.integer :addressable_id, index: true
      t.string  :addressable_type, index: true
      t.string :street
      t.integer :number
      t.string :district
      t.string :complement
      t.string :zipcode

      t.timestamps null: false
    end
    add_index :addresses, :street
    add_foreign_key :addresses, :cities
  end
end
