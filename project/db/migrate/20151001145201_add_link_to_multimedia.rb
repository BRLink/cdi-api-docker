class AddLinkToMultimedia < ActiveRecord::Migration
  def change
    add_column :multimedia, :link, :string
  end
end
