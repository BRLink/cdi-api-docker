class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.references :album, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.string :image
      t.string :original_filename
      t.string :content_type
      t.integer :file_size
      t.boolean :active
      t.integer :position
      t.string :caption
      t.string :tags, array: true

      t.timestamps null: false
    end

    add_index :photos, :tags, using: 'gin'
    add_index :photos, :active
  end
end
