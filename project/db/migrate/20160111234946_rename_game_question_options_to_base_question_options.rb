class RenameGameQuestionOptionsToBaseQuestionOptions < ActiveRecord::Migration
  def change
    rename_table :game_question_options, :base_question_options
    add_column :base_question_options, :type, :string, index: true
    rename_column :base_question_options, :game_question_id, :base_question_id
  end
end
