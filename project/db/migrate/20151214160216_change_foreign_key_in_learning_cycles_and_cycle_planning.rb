class ChangeForeignKeyInLearningCyclesAndCyclePlanning < ActiveRecord::Migration
  def up
    remove_foreign_key :cycle_plannings, :learning_cycles
    remove_foreign_key :learning_tracks, :learning_cycles
    remove_foreign_key :learning_track_reviews, :learning_tracks
    remove_foreign_key :student_classes_tracks, :learning_tracks

    add_foreign_key :cycle_plannings, :learning_cycles, on_delete: :cascade
    add_foreign_key :learning_tracks, :learning_cycles, on_delete: :cascade

    add_foreign_key :learning_track_reviews, :learning_tracks, on_delete: :cascade
    add_foreign_key :student_classes_tracks, :learning_tracks, on_delete: :cascade
  end

  def down
    remove_foreign_key :cycle_plannings, :learning_cycles
    remove_foreign_key :learning_tracks, :learning_cycles
    remove_foreign_key :learning_track_reviews, :learning_tracks
    remove_foreign_key :student_classes_tracks, :learning_tracks


    add_foreign_key :cycle_plannings, :learning_cycles
    add_foreign_key :learning_tracks, :learning_cycles
    add_foreign_key :learning_track_reviews, :learning_tracks
    add_foreign_key :student_classes_tracks, :learning_tracks
  end

end
