class RenameBlogColumn < ActiveRecord::Migration
  def change
    rename_column :blog_articles, :pubDate, :pub_date
  end
end
