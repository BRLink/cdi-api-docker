class AddMetadataToSlidePresentations < ActiveRecord::Migration
  def change
    add_column :presentation_slides, :metadata, :json
  end
end
