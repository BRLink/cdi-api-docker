class AddBestCommentToDiscussions < ActiveRecord::Migration
  def change
    add_column :discussions, :best_comment_id, :integer, null: true, index: true
  end
end
