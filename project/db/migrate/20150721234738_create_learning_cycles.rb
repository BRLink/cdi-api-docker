class CreateLearningCycles < ActiveRecord::Migration
  def change
    create_table :learning_cycles do |t|
      t.string :name
      t.text :objective_text

      t.references :category, index: true
      t.references :user, index: true

      t.timestamps null: false
    end

    add_index :learning_cycles, :name
    add_foreign_key :learning_cycles, :categories
    add_foreign_key :learning_cycles, :users
  end
end
