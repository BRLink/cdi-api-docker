class CreateLearningTracksStudentClasses < ActiveRecord::Migration
  def change
    create_table :learning_tracks_student_classes, id: false do |t|
      t.references :student_class, index: true
      t.references :learning_track, index: true
    end
    add_foreign_key :learning_tracks_student_classes, :student_classes
    add_foreign_key :learning_tracks_student_classes, :learning_tracks

    add_index :learning_tracks_student_classes, [:student_class_id, :learning_track_id], unique: true, name: 'index_track_class_on_student_class_id_and_learning_track_id'
  end
end
