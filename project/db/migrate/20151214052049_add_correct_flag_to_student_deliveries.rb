class AddCorrectFlagToStudentDeliveries < ActiveRecord::Migration
  def change
    add_column :student_deliveries, :correct, :boolean
    add_index :student_deliveries, :correct
  end
end
