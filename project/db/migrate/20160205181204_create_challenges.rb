class CreateChallenges < ActiveRecord::Migration
  def change
    create_table :challenges do |t|
      t.string :title, null: false
      t.text :about, null: false
      t.integer :delivery_format, null: false
      t.string :delivery_type, null: false
      t.datetime :end_date, null: false
      t.string :featured_image
      t.string :tags, array: true
      t.references :presentation

      t.timestamps null: false
    end

    add_foreign_key :challenges, :presentations, on_delete: :cascade
    add_index :challenges, :tags, using: 'gin'

  end
end
