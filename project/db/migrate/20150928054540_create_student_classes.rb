class CreateStudentClasses < ActiveRecord::Migration
  def change
    create_table :student_classes do |t|
      t.string :name
      t.integer :admin_user_id, index: true
      t.string :code
      t.datetime :code_expiration_date
      t.string :status
      t.datetime :status_changed_at

      t.timestamps null: false
    end

    add_index :student_classes, :name
    add_index :student_classes, :code
    add_index :student_classes, :code_expiration_date

    add_foreign_key :student_classes, :users, column: :admin_user_id
  end
end
