class CreateGameCompleteSentences < ActiveRecord::Migration
  def change
    create_table :game_complete_sentences do |t|
      t.references :user, index: true
      t.text :sentence
      t.string :status
      t.string :words, array: true
      t.datetime :status_changed_at

      t.timestamps null: false
    end

    add_index :game_complete_sentences, :status
    add_index :game_complete_sentences, :words, using: 'gin'

    add_foreign_key :game_complete_sentences, :users
  end
end
