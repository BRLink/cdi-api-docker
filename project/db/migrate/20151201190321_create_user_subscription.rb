class CreateUserSubscription < ActiveRecord::Migration
  def change
    create_table :user_subscriptions do |t|
      t.references :user, index: true
      t.references :user
      t.integer :publisher_id
      t.string  :publisher_type
      t.timestamps null: false
    end
    add_index :user_subscriptions, :publisher_id
    add_foreign_key :user_subscriptions, :users
  end
end
