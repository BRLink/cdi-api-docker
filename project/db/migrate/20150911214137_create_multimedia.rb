class CreateMultimedia < ActiveRecord::Migration
  def change
    create_table :multimedia do |t|
    	t.string 		:file
    	t.references  	:category, index: true,  foreign_key: true
    	t.string 		:tags, array: true
    	t.references  	:user, index: true,  foreign_key: true
    end
    add_index :multimedia, :tags, using: 'gin'
  end
end
