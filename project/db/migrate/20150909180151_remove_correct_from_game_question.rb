class RemoveCorrectFromGameQuestion < ActiveRecord::Migration
  def change
    remove_column :game_questions, :correct, :boolean
  end
end
