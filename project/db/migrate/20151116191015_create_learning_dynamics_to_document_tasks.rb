class CreateLearningDynamicsToDocumentTasks < ActiveRecord::Migration
  def change
    create_table :learning_dynamics_to_document_tasks do |t|
      t.references :learning_dynamic
      t.integer :status, default: 0
      t.integer :retries, default: 0
      t.string :ins, array: true
    end
    add_index :learning_dynamics_to_document_tasks, [:learning_dynamic_id], unique: true, name: 'index_dynamic_to_document_task_dynamic'
  end
end
