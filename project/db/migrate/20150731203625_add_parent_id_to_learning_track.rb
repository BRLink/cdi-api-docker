class AddParentIdToLearningTrack < ActiveRecord::Migration
  def change
    add_column :learning_tracks, :parent_id, :integer
  end
end
