class ChangeColumnNameFromAchievement < ActiveRecord::Migration
  def change
    rename_column :achievements, :points, :default_score
  end
end
