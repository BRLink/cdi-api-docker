class CreateBlogCategories < ActiveRecord::Migration
  def change
    create_table :blog_categories do |t|
      t.string :name, null: false, index: true
      t.integer :position

      t.timestamps null: false
    end
  end
end
