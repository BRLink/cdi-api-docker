class CreateLike < ActiveRecord::Migration
  def change
    create_table :likes do |t|
      t.references :user, index: true
      t.string :resource_type, null: false
      t.integer :resource_id, null: false, index: true
      t.timestamps null: false
    end
    add_foreign_key :likes, :users
  end
end
