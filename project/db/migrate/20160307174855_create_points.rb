class CreatePoints < ActiveRecord::Migration
  def change
    create_table :points do |t|
      t.references :educational_institution, index: true, foreign_key: true, null: false
      t.references :achievement,             index: true, foreign_key: true, null: false
      t.integer :score,                                                      null: false

      t.timestamps null: false
    end
  end
end
