class CreateLearningDeliveries < ActiveRecord::Migration
  def change
    create_table :learning_deliveries do |t|
      t.string :format_type
      t.text :description
      t.datetime :delivery_date
      t.integer :delivery_kind
      t.references :learning_dynamic, index: true

      t.timestamps null: false
    end
    add_foreign_key :learning_deliveries, :learning_dynamics
  end
end
