class CreateGameOpenQuestions < ActiveRecord::Migration
  def change
    create_table :game_open_questions do |t|
      t.references :user, index: true
      t.string :title
      t.integer :questions_count
      t.string :image
      t.string :status
      t.datetime :status_changed_at

      t.timestamps null: false
    end

    add_index :game_open_questions, :title
    add_index :game_open_questions, :status

    add_foreign_key :game_open_questions, :users
  end
end
