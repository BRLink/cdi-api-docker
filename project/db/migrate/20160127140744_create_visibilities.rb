class CreateVisibilities < ActiveRecord::Migration
  def change
    create_table :visibilities do |t|
      t.belongs_to :resource, polymorphic: true
      t.string :visibility_type, null: false
      t.string :visibility_id
    end
  end
end
