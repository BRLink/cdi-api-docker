class AddResourceOnPosts < ActiveRecord::Migration
  def change
    add_column :posts, :resource_id, :integer, null: false, index: true
    add_column :posts, :resource_type, :string, null: false, index: true
  end
end
