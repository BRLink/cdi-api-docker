class FixForeignKeyOnPostFeatured < ActiveRecord::Migration
  def change
    remove_foreign_key :post_featured, :posts
    remove_foreign_key :post_featured, :users
    add_foreign_key :post_featured, :posts, on_delete: :cascade
    add_foreign_key :post_featured, :users, on_delete: :cascade
    add_index :post_featured, [:post_id, :user_id], unique: true, name: 'post_featured_post_id_user_id_unique_index'
  end
end
