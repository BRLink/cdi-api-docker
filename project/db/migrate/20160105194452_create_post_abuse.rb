class CreatePostAbuse < ActiveRecord::Migration
  def change
    create_table :post_abuses do |t|
      t.timestamps null: false
      t.belongs_to :user
      t.belongs_to :post
    end
  end
end
