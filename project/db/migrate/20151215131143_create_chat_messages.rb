class CreateChatMessages < ActiveRecord::Migration
  def change
    create_table :chat_messages do |t|
      t.integer    :user_id, null: false
      t.integer    :chat_room_id, null: false
      t.string     :content

      t.integer    :status, default: 0, null: false
      t.datetime   :status_changed_at
      t.timestamps null: false

    end
  end
end
