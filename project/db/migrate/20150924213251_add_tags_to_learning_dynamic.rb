class AddTagsToLearningDynamic < ActiveRecord::Migration
  def change
    change_table :learning_dynamics do |t|
      t.string :tags, array: true
    end
    add_index :learning_dynamics, :tags, using: 'gin'
  end
end
