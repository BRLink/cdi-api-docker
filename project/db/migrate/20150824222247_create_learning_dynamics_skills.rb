class CreateLearningDynamicsSkills < ActiveRecord::Migration
  def change
    create_table :learning_dynamics_skills, id: false do |t|
      t.integer :learning_dynamic_id
      t.integer :skill_id
    end
  end
end
