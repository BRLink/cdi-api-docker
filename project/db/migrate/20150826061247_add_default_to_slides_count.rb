class AddDefaultToSlidesCount < ActiveRecord::Migration
  def change
    change_column_default(:presentations, :slides_count, 0)
    change_column_default(:presentation_slides, :views_count, 0)
  end
end
