class AddResourcesToUserBadge < ActiveRecord::Migration
  def change
    add_column :user_badges, :resource_id,  :integer, null: false, index: true
    add_column :user_badges, :resource_type, :string, null: false, index: true
  end
end
