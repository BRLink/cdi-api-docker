class RemoveVisibilityColumnsFromPosts < ActiveRecord::Migration
  def change
    remove_column :posts, :visibility_type
    remove_column :posts, :visibility_id
  end
end
