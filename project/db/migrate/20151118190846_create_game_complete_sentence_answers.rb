class CreateGameCompleteSentenceAnswers < ActiveRecord::Migration
  def change
    create_table :game_complete_sentence_answers do |t|
      t.references :user, index: true
      t.references :game_complete_sentence
      t.string :words, array: true
      t.boolean :correct

      t.timestamps null: false
    end

    add_index :game_complete_sentence_answers, :game_complete_sentence_id, name: 'index_game_complete_sentence_answers_on_game_id'
    add_index :game_complete_sentence_answers, :correct
    add_index :game_complete_sentence_answers, [:user_id, :game_complete_sentence_id], unique: true, name: 'uniq_index_game_sentence_answer_on_user_id_game_id'
    add_index :game_complete_sentence_answers, :words, using: 'gin'

    add_foreign_key :game_complete_sentence_answers, :users
    add_foreign_key :game_complete_sentence_answers, :game_complete_sentences
  end
end
