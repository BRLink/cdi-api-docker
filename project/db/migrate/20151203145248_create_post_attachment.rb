class CreatePostAttachment < ActiveRecord::Migration
  def change
    create_table :post_attachments do |t|
      t.references :post, index: true
      t.string :attachment_type, null: false
      t.integer :attachment_id, null: false, index: true
      t.timestamps null: false
    end
    add_foreign_key :post_attachments, :posts
  end
end
