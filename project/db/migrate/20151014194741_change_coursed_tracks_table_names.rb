class ChangeCoursedTracksTableNames < ActiveRecord::Migration
  def change
    rename_table :coursed_learning_tracks, :enrolled_learning_tracks

    remove_column :coursed_track_events, :coursed_learning_track_id, :integer
    add_reference :coursed_track_events, :enrolled_learning_track, index: true

    rename_table :coursed_track_events, :tracking_events
  end
end
