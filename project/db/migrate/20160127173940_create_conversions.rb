class CreateConversions < ActiveRecord::Migration
  def change
    create_table :conversions do |t|
      t.integer :status, default: 0
      t.integer :slide_count
      t.integer :image_count
      t.json :slides
      t.references :user
      t.references :presentation

      t.timestamps null: false
    end
    add_foreign_key :conversions, :users
    add_foreign_key :conversions, :presentations

  end
end
