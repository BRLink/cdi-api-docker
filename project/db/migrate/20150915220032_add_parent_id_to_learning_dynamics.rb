class AddParentIdToLearningDynamics < ActiveRecord::Migration
  def change
    add_column :learning_dynamics, :parent_id, :integer
    add_index :learning_dynamics, :parent_id
  end
end
