class CreateSubscribersGroupMember < ActiveRecord::Migration
  def change
    create_table :subscribers_group_members do |t|
      t.references :user_subscribers_group, index: true
      t.references :user_subscription, index: true
      t.timestamps null: false
    end
  end
end
