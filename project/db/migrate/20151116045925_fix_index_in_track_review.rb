class FixIndexInTrackReview < ActiveRecord::Migration
  def change
    remove_index :learning_track_reviews, name: "index_learning_track_reviews_on_learning_track_id_and_user_id", column: [:user_id, :learning_track_id]
    add_index :learning_track_reviews, [:user_id, :learning_track_id, :student_class_id], name: "index_tracks_reviews_on_user_id_track_id_class_id", unique: true
  end
end
