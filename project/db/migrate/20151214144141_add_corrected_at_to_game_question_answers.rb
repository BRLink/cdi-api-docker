class AddCorrectedAtToGameQuestionAnswers < ActiveRecord::Migration
  def change
    add_column :game_question_answers, :corrected_at, :datetime
  end
end
