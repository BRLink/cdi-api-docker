class CreateOrigins < ActiveRecord::Migration
  def change
    create_table :origins do |t|
      t.integer :originable_id
      t.string :originable_type
      t.string :ip
      t.string :provider
      t.string :user_agent
      t.string :locale

      t.timestamps null: false
    end
    add_index :origins, :originable_id
    add_index :origins, :originable_type
  end
end
