class AddRelatedImageToGameItem < ActiveRecord::Migration
  def change
    add_column :game_items, :related_image, :string
  end
end
