class ChangeForeignKeysInLearningDeliveries < ActiveRecord::Migration

  def change_foreign_key_2015100921212(table, column, options = {})
    options = options.reverse_merge(on_delete: :cascade)

    remove_foreign_key table, column
    add_foreign_key table, column, options
  end

  def down_change_foreign_key_2015100921212(table, column, options = {})
    change_foreign_key_2015100921212(table, column, on_delete: nil)
  end

  def up
    change_foreign_key_2015100921212 :learning_deliveries, :learning_dynamics
    change_foreign_key_2015100921212 :authorizations, :users
    change_foreign_key_2015100921212 :game_question_options, :game_questions
  end

  def down
    down_change_foreign_key_2015100921212 :learning_deliveries, :learning_dynamics
    down_change_foreign_key_2015100921212 :authorizations, :users
    down_change_foreign_key_2015100921212 :game_question_options, :game_questions
  end
end
