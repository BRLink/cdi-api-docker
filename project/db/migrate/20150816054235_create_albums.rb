class CreateAlbums < ActiveRecord::Migration
  def change
    create_table :albums do |t|
      t.references :user, index: true
      t.string :title
      t.string :description
      t.boolean :active
      t.integer :position
      t.string :tags, array: true

      t.timestamps null: false
    end

    add_index :albums, :tags, using: 'gin'
    add_index :albums, :active
  end
end
