if [:development, :test].exclude?(Rails.env.to_sym)
  Rack::Timeout.timeout = 300 # 5 minutos (testes)
end
