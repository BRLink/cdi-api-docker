# encoding: UTF-8

require 'carrierwave'

CarrierWave.configure do |config|

  config.fog_credentials = {
    :provider               => 'AWS',
    :aws_access_key_id      => CDI::Config.aws_access_key_id,
    :aws_secret_access_key  => CDI::Config.aws_secret_access_key,
    :region                 => CDI::Config.aws_region
  }

  config.fog_use_ssl_for_aws = CDI::Config.enabled?(:aws_use_ssl)

  config.fog_directory  = CDI::Config.aws_bucket_name
  config.fog_public     = true
  config.fog_attributes = {'Cache-Control'=>"max-age=#{CDI::Config.aws_cache_max_age}"}

  if Rails.env.test?
    config.enable_processing = false
  end

  if Rails.env.development?
    config.asset_host = 'http://localhost:9090'
  end
end
