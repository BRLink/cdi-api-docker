module Grape
  module Exceptions
    class Validation < Grape::Exceptions::Base

      def initialize(args = {})
        fail 'Params are missing:' unless args.key? :params
        @params = args[:params]
        args[:message] = translate_message(args[:message_key], args.slice(:values) ) if args.key? :message_key
        super
      end
    end
  end
end

module Grape
  module Validations
    class ValuesValidator < Base
      def validate_param!(attr_name, params)
        return unless params[attr_name] || required_for_root_scope?

        values = @values.is_a?(Proc) ? @values.call : @values
        param_array = params[attr_name].nil? ? [nil] : Array.wrap(params[attr_name])
        unless param_array.all? { |param| values.include?(param) }
          fail Grape::Exceptions::Validation,
            params: [@scope.full_name(attr_name)],
            message_key: :values,
            values: @values
        end
      end
    end
  end
end

module Grape
  module Exceptions
    class Base < StandardError

      alias_method :_translate_message, :translate_message

      def translate_message(key, options = {})
        if options[:values] && options[:values].kind_of?(Array)
          options[:values] = options[:values].join(', ')
        end

        _translate_message(key, options)
      end
    end
  end
end