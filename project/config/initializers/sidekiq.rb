redis_url = CDI::Config.redis_url.presence || ENV['REDISTOGO_URL'] || ENV['REDIS_URL']

Sidekiq.configure_server do |config|
  config.redis = { url: redis_url }
end

Sidekiq.configure_client do |config|
  config.redis = { url: redis_url }
end
